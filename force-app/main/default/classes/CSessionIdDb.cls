public without sharing class CSessionIdDb 
{
    public static boolean ValidateSessionIdTime(SessionID__c theSessionId)
    {
        if (datetime.now() > theSessionId.LestActivityTime__c.addHours(2))
        {
            delete theSessionId;
            DeleteSessionsIdWithLastTime();
            return false;
        }
        else
        {
            theSessionId.LestActivityTime__c = datetime.now();
            update theSessionId;
            DeleteSessionsIdWithLastTime();
            return true;
        }
    }
    
    public static void DeleteSessionsIdWithLastTime()
    {
        SessionID__c[] sessionsId = [select name, id, InTime__c, TutorName__c, Family__c, LestActivityTime__c, SupporterName__c, 
                                            TutorName__r.name , Family__r.Name
                                        from SessionID__c where LestActivityTime__c < :datetime.now().addHours(-2)];
        if (sessionsId.size() > 0)  
            delete sessionsId;
    }
    
    public static SessionID__c GetActiveSession()
    {
        SessionID__c[] sessionsId = [select name, id, InTime__c, TutorName__c, Family__c, LestActivityTime__c, SupporterName__c, 
                                    TutorName__r.name , Family__r.Name
                                      from SessionID__c where LestActivityTime__c > :datetime.now().addHours(-2)];
        if (sessionsId.size() > 0)  
            return sessionsId[0];
        return null;
    }
    
    public static SessionID__c GetSessionIDByName(string name)
    {
        SessionID__c[] sessionsId = [select name, id, InTime__c, TutorName__c, Family__c, LestActivityTime__c, SupporterName__c, 
                                        TutorName__r.name , Family__r.Name
                                      from SessionID__c where name = :name];
        if (sessionsId.size() > 0)  
            return sessionsId[0];
        return null;
    }
    
    public static SessionID__c GetSessionIDById(string id)
    {
        SessionID__c[] sessionsId = [select name, id, InTime__c, TutorName__c, Family__c, LestActivityTime__c, SupporterName__c, ActivityApproval__c ,
                                    TutorName__r.name , Family__r.Name
                                        from SessionID__c where id = :id];
        if (sessionsId.size() > 0)  
            return sessionsId[0];
        return null;
    }
    
    public static list<SessionID__c> GetSessionsIDList()
    {
        return [select name, id, InTime__c, TutorName__c, Family__c, LestActivityTime__c, SupporterName__c,
                         TutorName__r.name  , Family__r.Name from SessionID__c];
    }
    
    public static map<string, SessionID__c> GetSessionsIDMap()
    {
        SessionID__c[] sessionsId = CSessionIdDb.GetSessionsIDList();
        map<string, SessionID__c> ret = new map<string, SessionID__c>();
        for (SessionID__c sid : sessionsId)
        {
            ret.put(sid.id, sid);   
        }
        return ret;
    }
}