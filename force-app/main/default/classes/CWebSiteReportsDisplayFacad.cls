public without sharing class CWebSiteReportsDisplayFacad 
{
	private Tutor__c tutor { get; set; }
	public Tutor__c pTutor { 
		get{
			if (tutor == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null){
						ActivityApproval__c[] myTutor = [SELECT Id, Name, TutorName__c FROM ActivityApproval__c WHERE id = :sessionId.ActivityApproval__c];
						String tutorId;
						if (myTutor.size() > 0){
							tutorId = myTutor[0].TutorName__c;
						}
						tutor = CTutorDb.GetTutorById(tutorId);
					}
				}
			}
			return tutor;
		} 
	}
	
	private Family__c family { get; set; }
    public Family__c pfamily { 
        get{
            if (family == null) {
                if (ApexPages.currentPage().getCookies().get('sessionId') != null){
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null){
                        family = CFamilyDb.GetFamilyById(sessionId.Family__c);
					}
                }
            }
            return family;
        } 
    }
    
    private ActivityApproval__c approval { get; set; }
	public ActivityApproval__c pApproval{ 
		get{
			if (approval == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null){
						approval = CActivityApprovalDb.GetActivityApprovalById(sessionId.ActivityApproval__c);
					}
				}
			}
			return approval;
		} 
	}
	
	private CWebSiteReportsDisplayDb webSiteReportsDisplayDb;
	
	public CWebSiteReportsDisplayFacad()
	{
		webSiteReportsDisplayDb = new CWebSiteReportsDisplayDb();
		fromMonthM = string.valueOf(datetime.now().month());
		toMonthM = string.valueOf(datetime.now().month());
		if (string.valueOf(datetime.now().month()).length() < 2)
		{
			fromMonthM = '0' + string.valueOf(datetime.now().month());
			toMonthM = '0' + string.valueOf(datetime.now().month());
		}
		fromMonthY = string.valueOf(datetime.now().year());
		toMonthY = string.valueOf(datetime.now().year());
	}
	
	public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId)){
				return null;
			}
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
	
	public PageReference Refresh()
	{
		return null;
	}
	
	public list<CActivityReportInReportsWebSite> pActivityReportsList{ get{ return webSiteReportsDisplayDb.GetActivityReportsLines(); } }
	public list<CActivityReportInReportsWebSite> pActivityReportsListTutors{ get{ return webSiteReportsDisplayDb.GetActivityReportsLinesTutors(); } }
	public list<selectOption> monthesOptions{ get{ return CObjectNames.monthesOptions; } }
	public list<selectOption> yearsOptions{ get{ return CObjectNames.yearsOptions; } }
	public string fromMonthM{ get{ return webSiteReportsDisplayDb.fromMonthM; } set{ webSiteReportsDisplayDb.fromMonthM = value; } }
	public string fromMonthY{ get{ return webSiteReportsDisplayDb.fromMonthY; } set{ webSiteReportsDisplayDb.fromMonthY = value; } }
	public string toMonthM{ get{ return webSiteReportsDisplayDb.toMonthM; } set{ webSiteReportsDisplayDb.toMonthM = value; } }
	public string toMonthY{ get{ return webSiteReportsDisplayDb.toMonthY; } set{ webSiteReportsDisplayDb.toMonthY = value; } }
}