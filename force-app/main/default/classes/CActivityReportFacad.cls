public with sharing class CActivityReportFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	
	public boolean bAmountAndReceipt { get; set; }
	public boolean bDaysList { get; set; }
	public boolean bReasonForDecline { get; set; }
	public boolean bNotPaid { get; set; }
	public boolean bPaid { get; set; }
	
	private CActivityReportDb activityReportDb;
	
	public ActivityReport__c pActivityReport
	{
		get
		{
			return activityReportDb.GetpActivityReport();
		}
	}
	public List<CDayInActivityReportInList> pDaysInActivityReport
	{
		get
		{
			return activityReportDb.GetpDaysInActivityReport();
		}
	}
		
	public CActivityReportFacad(ApexPages.StandardController controller)
	{
    	activityReportDb = new CActivityReportDb(controller);
    	errorMessage = '';
		if (pActivityReport.id == null)
			pageTitle = 'דיווח פעילות חדש';
		else
			pageTitle = pActivityReport.name;
		OnChangeActivityReportStatus();
		SetAmountApprovedOrHoursPerWeek();
		setPaidOrNot();
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		activityReportDb.InsertpActivityReport();
		PageReference newCasePage = new ApexPages.StandardController(pActivityReport).view();
        return newCasePage;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CActivityReportValidator validator = new CActivityReportValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public void OnAddDayToList()
    {
    	activityReportDb.OnAddDayToList();
    }
    
    public void OnChangeActivityApprovalName()
    {
    	SetAmountApprovedOrHoursPerWeek();
    }
    
    public void OnChangeTutorName()
    {
    }
    
    public void OnChangeChildName()
    {
    	
    }
    
    public void OnChangeProfessionName()
    {
    	SetAmountApprovedOrHoursPerWeek();
    }
    
    public void OnChangeActivityReportStatus()
    {
    	if (pActivityReport.ActivityReportStatus__c == CObjectNames.ActivityReportStatusPostponed)
    		bReasonForDecline = true;
		else 
			bReasonForDecline = false;
    }
	
	private void SetAmountApprovedOrHoursPerWeek()
	{
		if (pActivityReport.ProfessionName__c != null)
		{
			string paymentType = CProfessionDb.GetProfessionsById(pActivityReport.ProfessionName__c)[0].ProfessionTypeName__r.PaymentType__c;
			bAmountAndReceipt = false;
			bDaysList = false;
			if (paymentType == CObjectNames.MonthlyPayment || paymentType == CObjectNames.OneTimePayment)
				bAmountAndReceipt = true;
			else
				bDaysList = true;
		}
		else
		{
			bAmountAndReceipt = false;
			bDaysList = true;
		}
	}
	
	private void setPaidOrNot()
	{
		if (pActivityReport.ActivityReportStatus__c == CObjectNames.PaymentStatusPaid || pActivityReport.ActivityReportStatus__c == CObjectNames.PaymentStatusHalfPaid)
		{
			bNotPaid = false;
			bPaid = true;
		}
		else
		{
			bNotPaid = true;
			bPaid = false;
		}
	}
    
    public PageReference OnNewActivityReport()
    {
		Schema.DescribeSObjectResult pR = ActivityReport__c.sObjectType.getDescribe();
		Pagereference pPage = new ApexPages.StandardController(pActivityReport).view();
		 
		Pagereference newpage = new Pagereference ('/' + pR.getKeyPrefix() +
		                                            '/e?CF00ND00000063kTb_lkid=' + pActivityReport.ActivityApprovalName__c + '&retURL=/' + pPage.getUrl());
		return newpage; 
    }
    
    public PageReference myDelete()
    {
    	delete pActivityReport;
    	
    	Schema.DescribeSObjectResult result = ActivityReport__c.sObjectType.getDescribe();
        PageReference pageRef = New PageReference('/' + result.getKeyPrefix() + '/o');
        
        return pageRef; 
    }
    
    public list<CObjectsHistory> pActivityReportHistories
    {
    	get
    	{
    		return activityReportDb.GetActivityReportHistories();
    	}
    }  
    
    public list<selectOption> monthesList
	{
		get
		{
			return activityReportDb.GetMonthesList();
		}
	}
	public string printLayout{ get{ return CObjectNames.getBaseUrl() + page.ActivityReportPrintLayout.getUrl() + '?ID=' + pActivityReport.id; } }
	
	
	
	public integer fromIndex { get;set; }
	public integer toIndex { get;set; }
	public void UpdateReportsToBeWithPayments()
	{
		ActivityReport__c[] reports = [select name, id, TutorName__c, ChildName__c, ChildName__r.FamilyName__c, ActivityReportStatus__c, ChildName__r.FamilyName__r.ParentName__c,
									   ChildName__r.FamilyName__r.City__c, ActivityApprovalName__r.PaymentTarget__c from ActivityReport__c where ActivityReportStatus__c = 'מאושר לתשלום' order by CreatedDate];
		set<string> families = new set<string>();
		fromIndex = fromIndex == null ? 0 : fromIndex;
		toIndex = toIndex == null || toIndex == 0 || toIndex > reports.size() ? reports.size() : toIndex;
		system.debug('toIndex = ' + toIndex);
		for (integer i = fromIndex; i < toIndex; i++)
		{
			system.debug('i = ' + i);
			families.add(reports[i].ChildName__r.FamilyName__c);
		}
		Payment__c[] payments = [select name, id, Date__c, IsDone__c, FamilyName__c, TutorName__c, FamilyOrTutor__c, Amount__c, Comment__c, Status__c, NatureOfPayment__c, 
								PaymentTarget__c from Payment__c where FamilyName__c in :families and IsDone__c != true];
		map<string, list<Payment__c>> paymentsByFamilies = new map<string, list<Payment__c>>();
		for (Payment__c payment : payments)
		{
			if (paymentsByFamilies.containsKey(payment.FamilyName__c))
				paymentsByFamilies.get(payment.FamilyName__c).add(payment);
			else
				paymentsByFamilies.put(payment.FamilyName__c, new list<Payment__c> { payment });
		}
		payments = null;
		map<string, Payment__c> paymentsByReports = new map<string, Payment__c>();
		for (integer i = fromIndex; i < toIndex; i++)
		{
			Payment__c myPayment = new Payment__c();
			myPayment.Date__c = system.now().date();
			myPayment.FamilyName__c = reports[i].ChildName__r.FamilyName__c;
			myPayment.TutorName__c = reports[i].TutorName__c;
			myPayment.ParentName__c = reports[i].ChildName__r.FamilyName__r.ParentName__c;
			myPayment.City__c = reports[i].ChildName__r.FamilyName__r.City__c;
			myPayment.Amount__c = 0;
			myPayment.IsDone__c = false;
			myPayment.name = 'temp';
			string reportPaymentTerget = reports[i].ActivityApprovalName__r.PaymentTarget__c == null ? 'משפחה' : reports[i].ActivityApprovalName__r.PaymentTarget__c;
			myPayment.PaymentTarget__c = reportPaymentTerget;
			list<Payment__c> familyPayments = paymentsByFamilies.containsKey(reports[i].ChildName__r.FamilyName__c) ? paymentsByFamilies.get(reports[i].ChildName__r.FamilyName__c) : new list<Payment__c>();
			for (Payment__c familyPayment : familyPayments)
			{
				familyPayment.PaymentTarget__c = familyPayment.PaymentTarget__c == null ? 'משפחה' : familyPayment.PaymentTarget__c;
				if (reports[i].TutorName__c == familyPayment.TutorName__c && reportPaymentTerget == familyPayment.PaymentTarget__c)
				{
					myPayment = familyPayment;
					break;
				}
			}
			paymentsByReports.put(reports[i].id, myPayment);
			//pActivityReport.PaymentName__c = myPayment.id;
		}
		if (paymentsByReports.size() > 0)
			upsert paymentsByReports.values();
		list<ActivityReport__c> toUpdate = new list<ActivityReport__c>();
		for (integer i = fromIndex; i < toIndex; i++)
		{
			reports[i].PaymentName__c = paymentsByReports.containsKey(reports[i].id) ? paymentsByReports.get(reports[i].id).id : null;
			reports[i].ActivityReportStatus__c = 'לתשלום';
			toUpdate.add(reports[i]);
			if (toUpdate.size() > 99)
			{
				update toUpdate;
				toUpdate = new list<ActivityReport__c>();
			}
		}
		if (toUpdate.size() > 0)
			update toUpdate;
	}
}