public with sharing class CFamiliesPaymentsReportDb 
{
	public list<CFamilyInPaymentReport> pFamilies {get; set;}
	public date pStartDate { get; set; }
	public date pEndDate { get; set; }
	public Children__c pChild { get; set; }
	public ActivityReport__c pActivityReport { get; set; }
	
	public decimal pSumTotalAmount()
	{
		decimal ret = 0;
		for(CFamilyInPaymentReport family : pFamilies)
			ret+= family.pTotalAmount;
		return ret;
	}
	public decimal pSumChildrenActivitiesAmount()
	{
		decimal ret = 0;
		for(CFamilyInPaymentReport family : pFamilies)
			ret+= family.pChildrenActivitiesAmount;
		return ret;
	}
	public decimal pSumFamilyActivitiesAmount()
	{
		decimal ret = 0;
		for(CFamilyInPaymentReport family : pFamilies)
			ret+= family.pFamilyActivitiesAmount;
		return ret;
	}
	public decimal pSumReportsAmount()
	{
		decimal ret = 0;
		for(CFamilyInPaymentReport family : pFamilies)
			ret+= family.pReportsAmount;
		return ret;
	}
	
	
	public CFamiliesPaymentsReportDb()
	{
		SetFamilyAndProfession();
		SetStartDate();
		SetEndDate();
		SetFamilies();
	}
	
	private void SetFamilyAndProfession()
	{
		pActivityReport = new ActivityReport__c();
		pChild = new Children__c();
		string familyId = Apexpages.currentPage().getParameters().get('familyId');
		if (familyId != null && familyId.trim() != '' && familyId.trim() != 'null')
			pChild.FamilyName__c = familyId;
		string professionId = Apexpages.currentPage().getParameters().get('professionId');
		if (professionId != null && professionId.trim() != '' && professionId.trim() != 'null')
			pActivityReport.ProfessionName__c = professionId;
	}
	
	public void searchWithFilters()
	{
		pFamilies = null;
		SetFamilies();
	}
	
	private void SetStartDate()
	{
		pStartDate = date.today();
		string startDate = Apexpages.currentPage().getParameters().get('startDate');
		if (startDate != null && startDate.trim() != '' && startDate.split('/').size() == 3)
		{
			try
			{
				string[] dateList = startDate.split('/');
				pStartDate = date.newInstance(integer.valueOf(dateList[2]), integer.valueOf(dateList[1]), integer.valueOf(dateList[0]));
			}
			catch(Exception e)
			{
				pStartDate = date.today();
			}
		}
	}
	
	private void SetEndDate()
	{
		string endDate = Apexpages.currentPage().getParameters().get('endDate');
		if (endDate != null && endDate.trim() != '' && endDate.split('/').size() == 3)
		{
			try
			{
				string[] dateList = endDate.split('/');
				pEndDate = date.newInstance(integer.valueOf(dateList[2]), integer.valueOf(dateList[1]), integer.valueOf(dateList[0]));
			}
			catch(Exception e)
			{
				pEndDate = null;
			}
		}
	}
	
	private void SetFamilies()
	{
		pFamilies = new list<CFamilyInPaymentReport>();
		string query = 'select id, name from Family__c';
		if (pChild.FamilyName__c != null)
			query += ' where id = \'' + pChild.FamilyName__c + '\'';
		list<Family__c> families = Database.query(query);
		map<string, list<CFamilyInPaymentReport.CChildInList>> children = GetChildren(families);
		map<string, decimal> activitiesPrices = GetSpecialActivitiesAmounts(families);
		for (Family__c family : families)
		{
			list<CFamilyInPaymentReport.CChildInList> childrenList = new list<CFamilyInPaymentReport.CChildInList>();
			if (children.containsKey(family.id))
				childrenList = children.get(family.id);
			CFamilyInPaymentReport newFamily = new CFamilyInPaymentReport(family, childrenList, activitiesPrices.get(family.id));
			if (newFamily.pTotalAmount > 0)
				pFamilies.add(newFamily);
		}
	}
	
	private map<string, list<CFamilyInPaymentReport.CChildInList>> GetChildren(list<Family__c> families)
	{
		map<string, list<CFamilyInPaymentReport.CChildInList>> ret = new map<string, list<CFamilyInPaymentReport.CChildInList>>();
		list<Children__c> children = [select id, name, FamilyName__c, FamilyName__r.name from Children__c where FamilyName__c != null and FamilyName__c in :families];
		map<string, decimal> activitiesPrices = GetSpecialActivitiesAmounts(children);
		map<string, decimal> activitiesReports = GetActivityReportAmounts(children);
		for (Children__c child : children)
		{
			if (((activitiesReports.get(child.id) != null ? activitiesReports.get(child.id) : 0) + (activitiesPrices.get(child.id) !=  null ? activitiesPrices.get(child.id) :0)) > 0)
			{
				if (ret.containsKey(child.FamilyName__c))
				{
					ret.get(child.FamilyName__c).add(new CFamilyInPaymentReport.CChildInList(child, activitiesReports.get(child.id), activitiesPrices.get(child.id)));
				}
				else
				{
					ret.put(child.FamilyName__c, new list<CFamilyInPaymentReport.CChildInList> { new CFamilyInPaymentReport.CChildInList(child, activitiesReports.get(child.id), activitiesPrices.get(child.id)) } );
				}
			}
		}
		return ret;
	}
	
	private map<string, decimal> GetSpecialActivitiesAmounts(list<Children__c> children)
	{							
		string query = 'select ChildName__c child, sum(PaymentAmount__c) amount from SpecialActivitiesLine__c where PaymentAmount__c != null and FamilyName__c = null and ChildName__c in' + CObjectNames.GetListToSelect(children);
		if (pStartDate != null)
			query += ' and PaymentName__r.Date__c >= ' + string.valueOf(pStartDate);
		if (pEndDate != null)
			query += ' and PaymentName__r.Date__c <= ' + string.valueOf(pEndDate);
		query += ' group by ChildName__c ';
		list<AggregateResult> prices = Database.query(query);
		map<string, decimal> ret = new map<string, decimal>();
		for (AggregateResult price : prices)
		{
			string childName = string.valueOf(price.get('child'));
			decimal amount = (decimal)price.get('amount');
			ret.put(childName, amount);
		}
		return ret;
	}
	
	private map<string, decimal> GetSpecialActivitiesAmounts(list<Family__c> families)
	{
		string query = 'select FamilyName__c family, sum(PaymentAmount__c) amount from SpecialActivitiesLine__c where PaymentAmount__c != null and ChildName__c = null and FamilyName__c in' + CObjectNames.GetListToSelect(families);
		if (pStartDate != null)
			query += ' and PaymentName__r.Date__c >= ' + string.valueOf(pStartDate);
		if (pEndDate != null)
			query += ' and PaymentName__r.Date__c <= ' + string.valueOf(pEndDate);
		query += ' group by FamilyName__c ';
		list<AggregateResult> prices = Database.query(query);
		map<string, decimal> ret = new map<string, decimal>();
		for (AggregateResult price : prices)
		{
			string childName = string.valueOf(price.get('family'));
			decimal amount = (decimal)price.get('amount');
			ret.put(childName, amount);
		}
		return ret;
	}
	
	private map<string, decimal> GetActivityReportAmounts(list<Children__c> children)
	{
		string query = 'select ChildName__c child, sum(TotalPaid__c) amount from ActivityReport__c where TotalPaid__c != null and ChildName__c in ' + CObjectNames.GetListToSelect(children);
		if (pStartDate != null)
			query += ' and MonthToFiltering__c >= ' + string.valueOf(pStartDate);
		if (pEndDate != null)
			query += ' and MonthToFiltering__c <= ' + string.valueOf(pEndDate);
		if (pActivityReport.ProfessionName__c != null)
			query += ' and ProfessionName__c =  \'' + pActivityReport.ProfessionName__c + '\'';
		query += ' group by ChildName__c ';
		list<AggregateResult> prices = Database.query(query);
		map<string, decimal> ret = new map<string, decimal>();
		for (AggregateResult price : prices)
		{
			string childName = string.valueOf(price.get('child'));
			decimal amount = (decimal)price.get('amount');
			ret.put(childName, amount);
		}
		return ret;
	}
}