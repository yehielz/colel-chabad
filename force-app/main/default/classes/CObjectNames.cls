public with sharing class CObjectNames
{
    public static string HoursPerWeek = 'שעות שבועיות';
    public static string AmountForApproval = 'סכום לאישור';
    public static string MonthlyPayment = 'תשלום חודשי';
    public static string OneTimePayment = 'תשלום חד פעמי';
    public static string Meetings = 'מפגשים';
    public static string Family = 'משפחה';
    public static string FamilyMember = 'בן משפחה';
    public static string Child = 'ילד';
    public static string Parent = 'הורה';
    public static string General = 'כללי';
    public static string Families = 'משפחות';
    public static string Children = 'בן משפחה';
    public static string Other = 'אחר';
    
    public final static string UserNameIsNotExist = 'שם משתמש אינו קיים במערכת!';
    public final static string UserNameIsNull = 'עליך להזין שם משתמש!';
    public final static string PasswordIsNotValid = 'סיסמא שגויה!';
    public final static string PasswordIsNull = 'עליך להזין סיסמא!';
    
    public static string CaseClosed = 'סגור';
    public static string ProfileSystemManager = 'מנהל מערכת';
    public static string ProfileSystemManager2 = 'מנהל המערכת';
    public static string ProfileSystemManager3 = 'מנהל מחלקה';
    public static string RoleActivityManager = 'מנהל פעילות';
    public static string RoleSecretary = 'מזכירה';
    
    public static string FamilyDiedParentDad = 'אב';
    public static string FamilyDiedParentMother = 'אם';
    public static string FamilyDiedParentBoth = 'שני ההורים';
    public static string EnglishFamilyDiedParentDad = 'Father';
    public static string EnglishFamilyDiedParentMother = 'Mother';
    public static string EnglishFamilyDiedParentBoth = 'Both parents';
    
    public static string FamilyResonOfDeathIllness = 'מחלה';
    public static string FamilyResonOfDeathHatred = 'איבה';
    public static string FamilyResonOfDeathForceDefance = 'כוחות הבטחון';
    public static string FamilyResonOfDeathSuicide = 'שי"ב';
    public static string FamilyResonOfDeathRoadAccident = 'ת. דרכים';
    public static string EnglishFamilyResonOfDeathIllness = 'Illness';
    public static string EnglishFamilyResonOfDeathHatred = 'Hatred';
    public static string EnglishFamilyResonOfDeathForceDefance = 'Defance Forces';
    public static string EnglishFamilyResonOfDeathSuicide = 'Commit Suicide';
    public static string EnglishFamilyResonOfDeathRoadAccident = 'Road Accident';
    
    public static string FamilyStateWidow = 'אלמן/ה';
    public static string FamilyStateMarried = 'נשוי/אה';
    public static string FamilyStateDivorce = 'גרוש/ה לאחר נישואין מחדש';
    public static string EnglishFamilyStateWidow = 'Widow/widower';
    public static string EnglishFamilyStateMarried = 'Re-married';
    public static string EnglishFamilyStateDivorce = 'Devorced/Divorcee after remarriage';
    
    
    public static string ExamGradeInWordExcellent = 'מצויין';
    public static string ExamGradeInWordVeryGood = 'טוב מאוד';
    public static string ExamGradeInWordAlmostVeryGood = 'כמעט טוב מאוד';
    public static string ExamGradeInWordgood = 'טוב';
    public static string ExamGradeInWordAlmostGood = 'כמעט טוב';
    public static string ExamGradeInWordEnough = 'מספיק';
    public static string ExamGradeInWordNotEnough = 'בלתי מספיק';
    
    public static decimal ExamGrade100 = 100;
    public static decimal ExamGrade95 = 95;
    public static decimal ExamGrade90 = 90;
    public static decimal ExamGrade80 = 80;
    public static decimal ExamGrade70 = 70;
    public static decimal ExamGrade60 = 60;
    public static decimal ExamGrade50 = 50;
    
    public static string CaseNew = 'CaseNew';
    public static string TutorNew = 'TutorNew';
    public static string PaymentNew = 'PaymentNew';
    public static string PaymentDelete = 'PaymentDelete';
    public static string MessagesView = 'MessagesView';
    public static string DiplomaNew = 'DiplomaNew';
    
    public static string ProfessionGroupPrivateLesson = 'שיעורים פרטיים';
    public static string ProfessionGroupClass = 'חוג';
    public static string ProfessionGroupDiagnosis = 'אבחון';
    public static string ProfessionGroupRemedialInstruction = 'הוראה מתקנת';
    public static string ProfessionGroupEmotionalCare = 'טיפול רגשי';
    public static string ProfessionGroupPsychometric = 'פסיכומטרי';
    public static string ProfessionGroupParents = 'הורים';
    public static string ProfessionGroupSummer = 'קיץ';
    
    public static string RequestForTreatmentStatusTreatment = 'בטיפול';
    public static string RequestForTreatmentStatusApproved = 'אושר';
    public static string RequestForTreatmentStatusPartialApproved = 'אושר חלקית';
    public static string RequestForTreatmentStatusNew = 'חדש';
    public static string RequestForTreatmentStatusNotApproved = 'לא אושר';
    
    public static string ActivityReportStatusNew = 'חדש';
    public static string ActivityReportStatusPostponed = 'נדחה';
    public static string ActivityReportStatusPayment = 'לתשלום';
    public static string ActivityReportStatusPaid = 'שולם';
    public static string ActReportStatusPaidByAccountingDpt = 'שולם ע"י הנה"ח';
    public static string ActReportStatusPaidByDonator = 'שולם ע"י תורם';
    public static string PaymentFamily = 'משפחה';
    public static string PaymentTutor = 'חונך';
    
    public static string ActivityApprovalPaymentTargetTutor = 'חונך';
    public static string ActivityApprovalPaymentTargetFamily = 'משפחה';
    
    public static string UserManager = 'מנהל מערכת';
    public static string UserSecretary = 'מזכירה';
    public static string UserCoordinator = 'רכז';
    
    public static map<string,string> FamilyResonOfDeathTranslate()
    {
        map<string,string> ret = new map<string,string>();
        ret.put(FamilyResonOfDeathIllness, EnglishFamilyResonOfDeathIllness);
        ret.put(FamilyResonOfDeathHatred, EnglishFamilyResonOfDeathHatred);
        ret.put(FamilyResonOfDeathForceDefance, EnglishFamilyResonOfDeathForceDefance);
        ret.put(FamilyResonOfDeathSuicide, EnglishFamilyResonOfDeathSuicide);
        ret.put(FamilyResonOfDeathRoadAccident, EnglishFamilyResonOfDeathRoadAccident);
        return ret;
    } 
    public static map<string,string> FamilyStateTranslate()
    {
        map<string,string> ret = new map<string,string>();
        ret.put(FamilyStateWidow,EnglishFamilyStateWidow);
        ret.put(FamilyStateMarried,EnglishFamilyStateMarried);
        ret.put(FamilyStateDivorce,EnglishFamilyStateDivorce);
        return ret;
    }
    
    public static string SessionId { get; set; }
    
    public static List<selectOption> GetMinutesOptions()
    {
        List<selectOption> ret = new List<selectOption>();
        for (integer i = 0; i < 60; i++)
        {
            string value = string.valueOf(i);
            if (i < 10)
                value = '0' + string.valueOf(i);
            ret.add(new selectOption(value, value));
        }
        return ret;
    }
    
    public static List<selectOption> GetHoursOptions()
    {
        List<selectOption> ret = new List<selectOption>();
        for (integer i = 0; i < 24; i++)
        {
            string value = string.valueOf(i);
            if (i < 10)
                value = '0' + string.valueOf(i);
            ret.add(new selectOption(value, value));
        }
        return ret;
    }
    
    public static string rendomNumber
    {
        get
        {
            return string.valueOf(integer.valueOf(math.random() * 100000));
        }
    }
    
    public static string getBaseUrl()
    {
        return system.Url.getSalesforceBaseUrl().toExternalForm();
    }
    
    public static string WebSitePageLogIn = getBaseUrl() + Page.WebSiteLogIn.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageMonthes = getBaseUrl() + Page.WebSiteMonthes.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageChildren = getBaseUrl() + Page.WebSiteChildren.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageReport = getBaseUrl() + Page.WebSiteReport.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageReportsDisplay = getBaseUrl() + Page.WebSiteReportsDisplay.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageReportDisplay = getBaseUrl() + Page.WebSiteReportDisplay.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageApprovals = getBaseUrl() + Page.VF02_WebSiteApprovals.getUrl() + '?a=' + rendomNumber;
    public static string WebSitePageHome = getBaseUrl() + Page.WebSiteHome.getUrl() + '?a=' + rendomNumber;
    public static string WebSiteSupporterLogIn = getBaseUrl() + Page.WebSiteSupporterLogIn.getUrl() + '?a=' + rendomNumber;
    public static string WebSiteSupporterHome = getBaseUrl() + Page.WebSiteSupporterHome.getUrl() + '?a=' + rendomNumber;
    public static string WebSiteSupporterChildren = getBaseUrl() + Page.WebSiteSupporterChildren.getUrl() + '?a=' + rendomNumber;
    public static string WebSiteSupporterChildView = getBaseUrl() + Page.WSSupporterChildView.getUrl() + '?a=' + rendomNumber;
    
    public static string WebSitePageTestLogIn = 'https://na12.salesforce.com/a0C/o';
    public static string WebSitePageTestMonthes = 'https://na12.salesforce.com/a0B/o';
    public static string WebSitePageTestChildren = 'https://na12.salesforce.com/a0G/o';
    public static string WebSitePageTestReport = 'https://na12.salesforce.com/a06/o';
    
    public static string WebSiteReportAtLeastOneDay = 'כדי לשלוח דיווח פעילות עליך למלא יום אחד לפחות בדיווח!';

    /* 22/06/2022 update */
    public static string WebSiteReportEnterDate = 'נא להזין תאריך לדיווח פעילות!';


    public static string WebSiteReportNotInThisMonthPar1 = 'התאריך - ';
    public static string WebSiteReportNotInThisMonthPar2 = ' - אינו מחודש -';
    public static string WebSiteReportStartTimeBigFromEndTimePar1 = 'בתאריך - ';
    public static string WebSiteReportStartTimeBigFromEndTimePar2 = ' - שעת סיום אינה גדולה משעת התחלה!';
    
    public static string TaskWaitForYouMale = 'ממתין עבורך ';
    public static string TaskWaitForYouFemale = 'ממתינה עבורך ';
    public static string TaskSobjectNewCase = 'עדכון חדש';
    public static string TaskSobjectNewFamily = 'משפחה חדשה';
    public static string TaskSobjectNewChild = 'בן משפחה חדש';
    public static string TaskSobjectNewExam = 'מבחן חדש';
    public static string TaskSobjectNewDiploma = 'תעודה חדשה';
    public static string TaskSobjectNewActivityApproval = 'אישור פעילות חדש';
    public static string TaskSobjectNewRequestForTreatment = 'בקשה לפעילות חדשה';
    public static string TaskSobjectNewActivityReport = 'דיווח פעילות חדש';
    public static string TaskSobjectNewDaysInReport = 'ממתין עבורך דיווח פעילות - חונך: ';
    public static string TaskSobjectClarification = 'בירור';
    public static string TaskSobjectYourRequestNotApproved = 'בקשתך לא אושרה!';
    public static string TaskSobjectYourRequestInTreatment = 'בקשתך בטיפול!';
    public static string TaskSobjectActivityReportToPayment = 'הדיווח פעילות הועבר לתשלום!';
    public static string TaskSobjectActivityReportNeedToFindOut = 'הדיווח פעילות זקוק לבירור!';
    public static string TaskStatusNew = 'חדש';
    public static string TaskStatusTreatment = 'בטיפול';
    public static string TaskStatusClosed = 'סגור';
    public static string TaskStatusPending = 'בהמתנה';
    public static string TaskStatusPostponed = 'נדחה';
    
    public static map<string, string> booleansTranslate
    {
        get
        {
            map<string, string> ret = new map<string, string>();
            ret.put('false', 'לא נבחר');
            ret.put('true', 'נבחר');
            return ret;
        }
    }
    
    public static string GetFullNumberInString(integer value)
    {
        if (value < 10)
            return '0' + value;
        else
            return string.valueOf(value);
    }
    
    public static list<selectOption> monthesOptions
    {
        get
        {
            return new list<selectOption> {
            new selectOption('01', '01'), new selectOption('02', '02'),
            new selectOption('03', '03'), new selectOption('04', '04'),
            new selectOption('05', '05'), new selectOption('06', '06'),
            new selectOption('07', '07'), new selectOption('08', '08'),
            new selectOption('09', '09'), new selectOption('10', '10'), 
            new selectOption('11', '11'), new selectOption('12', '12') };
        }
    }
    
    public static list<selectOption> yearsOptions
    {
        get
        {
            integer nowYear = datetime.now().year();
            return new list<selectOption> { 
            new selectOption(string.valueOf(nowYear - 5), string.valueOf(nowYear - 5)), 
            new selectOption(string.valueOf(nowYear - 4), string.valueOf(nowYear - 4)),
            new selectOption(string.valueOf(nowYear - 3), string.valueOf(nowYear - 3)), 
            new selectOption(string.valueOf(nowYear - 2), string.valueOf(nowYear - 2)),
            new selectOption(string.valueOf(nowYear - 1), string.valueOf(nowYear - 1)), 
            new selectOption(string.valueOf(nowYear), string.valueOf(nowYear)),
            new selectOption(string.valueOf(nowYear + 1), string.valueOf(nowYear + 1)), 
            new selectOption(string.valueOf(nowYear + 2), string.valueOf(nowYear + 2)),
            new selectOption(string.valueOf(nowYear + 3), string.valueOf(nowYear + 3)), 
            new selectOption(string.valueOf(nowYear + 4), string.valueOf(nowYear + 4)), 
            new selectOption(string.valueOf(nowYear + 5), string.valueOf(nowYear + 5)), 
            new selectOption(string.valueOf(nowYear + 6), string.valueOf(nowYear + 6)) };
        }
    }
    
    public static list<selectOption> hoursByHalfAnHourOptions
    {
        get
        {
            list<selectOption> ret = new list<selectOption>();
            for (integer i = 8; i < 24; i++)
            {
                string hour = string.valueOf(i);
                if (i < 10)
                    hour = '0' + string.valueOf(i);
                ret.add(new selectOption(hour + ':00', hour + ':00'));
                ret.add(new selectOption(hour + ':30', hour + ':30'));
            }
            return ret;
        }
    }
    
    public static string getHebrewString(string reverseString)
    {
        if (!isWithHebrew(reverseString))
            return reverseString;
        string ret = '';
        string[] stringList = reverseString.split(' ');
        string english = '';
        for (integer i = stringList.size() - 1; i >= 0; i--)
        {
            if (isWithHebrew(stringList[i]))
            {
                ret += english;
                english = '';
                string s = '';
                for (integer a = stringList[i].length() - 1; a >= 0; a--)
                {
                    s += stringList[i].substring(a, a+1);
                }
                s = s.replace('(','@@@').replace(')','(').replace('@@@',')');
                ret += s + ' ';
            }
            else
            {
                english = stringList[i] + ' ' + english + ' '; 
            }
        }
        ret += english;
        if (ret.endsWith(' ') && !reverseString.endsWith(' '))
            ret = ret.substring(0, ret.length() - 1);
        return ret;
    }
    
    public static string GetHebrewWords(string thenote, integer length)
    {
        string ret = '';
        if (thenote != null)
        {
            string note = ' ' + thenote;
            for (integer i = length; i < note.length(); i=length)
            {
                integer fromInSubS = i < length ? i: length;
                integer minus = 0;
                while(note.substring(i-minus, ((i-minus)+1)) != ' ')
                    minus++;
                i = i - minus;
                ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
                ret += '<br/>';
                note = note.substring(i, note.length());
            }
            ret += CObjectNames.getHebrewString(note);
        }
        return ret;
    }
    
    public static boolean isWithHebrew(string toCheck)
    {
        if (toCheck == '+' || toCheck == '-' || toCheck == ':' || toCheck == '/')
            return true;
        for (integer i = 0; i < toCheck.length(); i++)
        {
            string s = toCheck.substring(i, i+1);
            if (s >= 'א' && s <= 'ת')
            {
                return true;
            }
        }
        return false;
    } 
    
    public static integer GetWeekDayNumberFromDate(Date theDate)
    {
        if (theDate != null)
        {
            datetime ret = Datetime.newInstance(theDate.year(), theDate.month(), theDate.day());
            string day = ret.format('EEEE');
            if (day == 'Sunday')
                return 1;
            if (day == 'Monday')
                return 2;
            if (day == 'Tuesday')
                return 3;
            if (day == 'Wednesday')
                return 4;
            if (day == 'Thursday')
                return 5;
            if (day == 'Friday')
                return 6;
            if (day == 'Saturday')
                return 7;
        }
        return 0;
    }
    
    public static final string PaymentStatusNotPaid = 'לא שולם';
    public static final string PaymentStatusHalfPaid = 'שולם חלקית';
    public static final string PaymentStatusPaid = 'שולם';
    
    public static string GetListToSelect(list<string> theList)
    {
        string ret = '';
        for (string item : theList)
        {
            ret += ret == '' ? '\'' + item + '\'' : ',\'' + item + '\'';
        }
        return '(' + ret + ')';
    } 
    
    public static string GetListToSelect(list<sobject> theList)
    {
        string ret = '';
        for (sobject item : theList)
        {
            ret += ret == '' ? '\'' + item.id + '\'' : ',\'' + item.id + '\'';
        }
        return '(' + ret + ')';
    } 
    
    public static string GetDateStr(date theDate)
    {
        return GetFullNumberInString(theDate.day()) + '/' + GetFullNumberInString(theDate.month()) + '/' + theDate.year();
    } 
    
    public static string GetDateStr(datetime theDate)
    {
        return GetFullNumberInString(theDate.day()) + '/' + GetFullNumberInString(theDate.month()) + '/' + theDate.year() + ' ' + 
                GetFullNumberInString(theDate.hour()) + ':' + GetFullNumberInString(theDate.minute()) + ':' + GetFullNumberInString(theDate.second());
    }
}