public with sharing class CCoordinatorFacad
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	private CCoordinatorDb coordinatorDb;
	
	public Coordinator__c pCoordinator
	{
		get
		{
			return coordinatorDb.GetCoordinator();
		}
	}
		
	public CCoordinatorFacad(ApexPages.StandardController controller)
	{
		coordinatorDb = new CCoordinatorDb(controller);
		SetProperties();
	}
	
	public PageReference save()
	{
		coordinatorDb.save();
		PageReference newCoordinatorPage = new ApexPages.StandardController(pCoordinator).view();
        return newCoordinatorPage ;
	}
	
	private void SetProperties()
	{
		errorMessage = '';
		if (pCoordinator.id == null)
			pageTitle = 'רכז שטח חדש';
		else
			pageTitle = pCoordinator.name;
	}
	
	public PageReference onChangeFirstNameOrLastName()
	{
		if (pCoordinator.FirstName__c != null && pCoordinator.LastName__c == null)
		{
			User[] myUser = [select name, id from User where name = :pCoordinator.FirstName__c];
			if (myUser.size() > 0)
				pCoordinator.UserName__c = myUser[0].id;
			else
				pCoordinator.UserName__c = null;
		}
		else if (pCoordinator.FirstName__c == null && pCoordinator.LastName__c != null)
		{
			User[] myUser = [select name, id from User where name = :pCoordinator.LastName__c];
			if (myUser.size() > 0)
				pCoordinator.UserName__c = myUser[0].id;
			else
				pCoordinator.UserName__c = null;
		}
		else if (pCoordinator.FirstName__c != null && pCoordinator.LastName__c != null)
		{
			User[] myUser = [select name, id from User where name = :(pCoordinator.FirstName__c + ' ' + pCoordinator.LastName__c)];
			if (myUser.size() > 0)
				pCoordinator.UserName__c = myUser[0].id;
			else
				pCoordinator.UserName__c = null;
		}
		else if (pCoordinator.FirstName__c == null && pCoordinator.LastName__c != null)
			pCoordinator.UserName__c = null;
		return null;
	}
}