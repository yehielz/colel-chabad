@isTest
public class Test_CActivityApprovalFacad {

    @testSetup static void methodName() {
        SchoolYear__c schoolYear = new SchoolYear__c();
        schoolYear.name = '2011-2012';
        schoolYear.IsActive__c = true;
        schoolYear.StartDate__c = Date.today();
        schoolYear.HalfSchoolYear__c = system.toDay();
        schoolYear.EndOfSchoolYear__c = system.toDay().addDays(356);
        insert schoolYear;
        Profession__c p1 = new Profession__c(Name='אחר');
        insert p1;
        Profession__c p2 = new Profession__c(Name='Other');
        insert p2;
        Family__c f = new Family__c(Name = 'TEST');
        insert f;
        Children__c c = new Children__c (Name='TEST',FamilyName__c = f.Id,FamilyManType__c= 'ילד',Birthdate__c = Date.Today().addYears(-10));
        insert c;
        Case__c cas = new Case__c (ChildName__c = c.Id,WhoId__c = CObjectNames.FamilyMember);
        insert cas;
        RequestForTreatment__c requestForTreatment = new RequestForTreatment__c(CaseName__c = cas.Id,ChildName__c = c.Id);
        insert requestForTreatment;
        ActivityApproval__c  activityApproval = new ActivityApproval__c(FromDate__c = Date.Today(),ToDate__c = Date.Today().addDays(30) , RequestForTreatmentName__c = requestForTreatment.id,ChildName__c = c.Id);
        insert activityApproval;
        Tutor__c tutor = new Tutor__c();
        insert tutor;
        ActivityReport__c activityReport = new ActivityReport__c(ActivityReportStatus__c = 'TEST',ChildName__c = c.Id,TutorName__c = tutor.Id,ActivityApprovalName__c = activityApproval.Id);
        insert activityReport;
        Payment__c payment = new Payment__c();
        insert payment;
        DayInActivityReport__c day = new DayInActivityReport__c(ActivityReportName__c = activityReport.Id, Date__c = Date.TODAY());
        insert day;
        DayInActivityReport__c day2 = new DayInActivityReport__c(ActivityReportName__c = activityReport.Id, Date__c = Date.TODAY());
        insert day2;

  
        
        SpecialActivities__c specialAct = new SpecialActivities__c();
        insert specialAct;
        
        SpecialActivitiesLine__c sp = new SpecialActivitiesLine__c (SpecialActivitiesName__c = specialAct.Id,ChildName__c = c.Id);
        
        insert sp;
        
        
    }
    static testMethod void TestCActivityApprovalFacad() {
        
        ActivityApproval__c approval = [select Id,Name from ActivityApproval__c limit 1];
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(approval);
        CActivityApprovalFacad facad = new CActivityApprovalFacad(con);
        facad.save();
        facad.GoToNextRequest();
        facad.OnChangeTutor();
        facad.SaveAndGoToNextRequest();
    }
}