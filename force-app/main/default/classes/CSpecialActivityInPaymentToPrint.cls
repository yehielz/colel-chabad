public with sharing class CSpecialActivityInPaymentToPrint
{
    public SpecialActivitiesLine__c pSpecialActivityLine { get; set; }
    
    public CSpecialActivityInPaymentToPrint(SpecialActivitiesLine__c mSpecialActivityLine)
    {
        pSpecialActivityLine = mSpecialActivityLine;
    }
    
    public string AddresseeName
    {
        get
        {
            if (pSpecialActivityLine.ChildOrFamily__c == 'משפחה' && pSpecialActivityLine.FamilyName__c != null && pSpecialActivityLine.FamilyName__r.name != null)
                return CObjectNames.getHebrewString(pSpecialActivityLine.FamilyName__r.name);
            else if (pSpecialActivityLine.ChildOrFamily__c == 'בן משפחה' && pSpecialActivityLine.ChildName__c != null && pSpecialActivityLine.ChildName__r.name != null)
                return CObjectNames.getHebrewString(pSpecialActivityLine.ChildName__r.name);
            return '';
        }
    }
    
    public string ActivityName
    {
        get
        {
            if (pSpecialActivityLine.SpecialActivitiesName__c != null && pSpecialActivityLine.SpecialActivitiesName__r.name != null)
                return CObjectNames.getHebrewString(pSpecialActivityLine.SpecialActivitiesName__r.name);
            return '';
        }
    }
}