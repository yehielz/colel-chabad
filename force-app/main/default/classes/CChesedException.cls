public with sharing class CChesedException extends Exception
{
	public string pDailure { get;set; }
	public CChesedException(string failure, boolean custom)
	{
		this(failure);
		pDailure = failure;
	}
}