@isTest
private class CTest
{
    @testSetup static void methodName() {
        SchoolYear__c schoolYear = new SchoolYear__c();
        schoolYear.name = '2011-2012';
        schoolYear.IsActive__c = true;
        schoolYear.StartDate__c = Date.today();
        schoolYear.HalfSchoolYear__c = system.toDay();
        schoolYear.EndOfSchoolYear__c = system.toDay().addDays(356);
        insert schoolYear;
        Profession__c p1 = new Profession__c(Name='אחר');
        insert p1;
        Profession__c p2 = new Profession__c(Name='Other');
        insert p2;
        Profession__c p3 = new Profession__c(Name='שיעורים פרטיים');
        insert p3;
        Family__c f = new Family__c(Name = 'TEST');
        insert f;
        Children__c c = new Children__c (Name='TEST',FamilyName__c = f.Id,FamilyManType__c= 'ילד',Birthdate__c = Date.Today().addYears(-10));
        insert c;
        Case__c cas = new Case__c (ChildName__c = c.Id,WhoId__c = CObjectNames.FamilyMember);
        insert cas;
        RequestForTreatment__c requestForTreatment = new RequestForTreatment__c(CaseName__c = cas.Id,ChildName__c = c.Id,ProfessionName__c = p3.Id);
        insert requestForTreatment;
        ActivityApproval__c  activityApproval = new ActivityApproval__c(FromDate__c = Date.Today(),ToDate__c = Date.Today().addDays(30) , RequestForTreatmentName__c = requestForTreatment.id,ChildName__c = c.Id);
        insert activityApproval;
        Tutor__c tutor = new Tutor__c();
        insert tutor;
        ActivityReport__c activityReport = new ActivityReport__c(ActivityReportStatus__c = 'TEST',ChildName__c = c.Id,TutorName__c = tutor.Id,ActivityApprovalName__c = activityApproval.Id);
        insert activityReport;
        Payment__c payment = new Payment__c();
        insert payment;
        DayInActivityReport__c day = new DayInActivityReport__c(ActivityReportName__c = activityReport.Id, Date__c = Date.TODAY());
        insert day;
        DayInActivityReport__c day2 = new DayInActivityReport__c(ActivityReportName__c = activityReport.Id, Date__c = Date.TODAY());
        insert day2;
        Exam__c exam = new Exam__c(ChildName__c = c.Id);
        insert exam;
        Diploma__c diploma = new Diploma__c(ChildName__c = c.Id);
        insert diploma;
        Coordinator__c coordinator = new Coordinator__c (FirstName__c = 'TEST', LastName__c = 'TEST' );
        insert coordinator ;
        
        Task t = new Task(Subject = 'TEST');
        insert t;
        
        SpecialActivities__c specialAct = new SpecialActivities__c();
        insert specialAct;
        
        SpecialActivitiesLine__c sp = new SpecialActivitiesLine__c (SpecialActivitiesName__c = specialAct.Id,ChildName__c = c.Id, Name  = 'TEST');
        
        insert sp;
        
        Support__c sup = new Support__c ();
        insert sup;
        ChildRegistration__c cr = new ChildRegistration__c (ChildName__c = c.Id,SchoolYearName__c = schoolYear.Id);
        insert cr;
        ExpenditureExceeded__c ex = new ExpenditureExceeded__c(ActivityReportName__c = activityReport.Id);
        insert ex;
        Check__c ch = new Check__c(PaymentName__c = payment.Id);
        insert ch;
        
        MasavSetting__c mMasavSetting = new MasavSetting__c (InstitutionSubject__c= 'TEST',NameOfInstitutionSubject__c = 'TET',InstitutionSends__c=34);
        insert mMasavSetting ;
        MasavFile__c file = new MasavFile__c(PaymentDate__c = Date.Today());
        insert file;
    }
    static testMethod void SchoolYearTriggerTest()
    {
        SchoolYear__c schoolYear = new SchoolYear__c();
        schoolYear.name = '2012-2013';
        schoolYear.IsActive__c = true;
        schoolYear.HalfSchoolYear__c = system.toDay();
        schoolYear.EndOfSchoolYear__c = system.toDay();
        insert schoolYear;
        update schoolYear;
    }
    
    static testMethod void ChildRegistrationTriggerTest()
    {
        ChildRegistration__c childRegistration = new ChildRegistration__c();
        insert childRegistration;
        update childRegistration; 
        delete childRegistration;
    }
    
    static testMethod void RequestForTreatmentTriggerTest()
    {
        Case__c tempC = new Case__c();
        insert tempC;
        RequestForTreatment__c rft = new RequestForTreatment__c();
        rft.CaseName__c = tempC.id;
        insert rft;
        update rft;
        delete rft;
    }
    
    static testMethod void ActivityApprovalTriggerTest()
    {
        Case__c cas = new Case__c ();
        insert cas;
        cas.AssigneeUser__c = Userinfo.getUserId();
        update cas;
        RequestForTreatment__c  requestForTreatment = new RequestForTreatment__c(CaseName__c = cas.Id);
        insert requestForTreatment;
        ActivityApproval__c activityApproval = new ActivityApproval__c();
        activityApproval.RequestForTreatmentName__c = requestForTreatment.id;
        insert activityApproval;
        update activityApproval;
        delete activityApproval;
    }
    
    static testMethod void CoordinatorTriggerTest()
    {
        Coordinator__c coordinator = new Coordinator__c();
        insert coordinator;
        update coordinator;
        delete coordinator;
    }
    
    static testMethod void ActivityReportTriggerTest()
    {
        ActivityReport__c activityReport = new ActivityReport__c();
        insert activityReport;
        update activityReport;
        delete activityReport;
    }
    
    static testMethod void PaymentTriggerTest()
    {
        Payment__c payment = new Payment__c();
        insert payment;
        update payment;
        delete payment;
    }
    
    static testMethod void ChildrenTriggerTest()
    {
        Children__c chd = new Children__c();
        insert chd;
        update chd;
        delete chd;
    }
    /*
    static testMethod void FeedItemTriggerTest()
    {
        Case__c cs = new Case__c();
        insert cs;
        update cs;
        
        FeedItem fi = new FeedItem();
        fi.ParentId = cs.id;
        fi.Body = 'llll';
        insert fi;
        //update fi;
        
        FeedComment fc = new FeedComment();
        fc.CommentBody = 'asadfds';
        fc.FeedItemId = fi.id;
        insert fc;
        //update fc;
        delete fc;
        delete fi;
        
        delete cs;
    }
    */
    static testMethod void ProfessionTypeTriggerTest()
    {
        ProfessionType__c pt = new ProfessionType__c();
        pt.isDefault__c =true;
        insert pt;
        update pt;
        delete pt;
    }
    
    static testMethod void MasavFileTriggerTest()
    {
        MasavFile__c masavFile = new MasavFile__c();
        masavFile.FromDate__c = date.newInstance(2013, 6, 30);
        masavFile.ToDate__c = date.newInstance(2013, 6, 30);
        masavFile.PaymentDate__c = dateTime.now().date();
        masavFile.SerialNumber__c = 1;
        insert masavFile;
    }
    
    static testMethod void FamilyTriggerTest()
    {
        Family__c family = new Family__c();
        insert family;
        update family;
        delete family;
    }
    
    static testMethod void SessionIdTriggerTest()
    {
        SessionId__c sessionId = new SessionId__c();
        insert sessionId;
        update sessionId;
        delete sessionId;
    }
    
    static testMethod void ChildEventTriggerTest()
    {
        ChildEvent__c childEvent = new ChildEvent__c();
        insert childEvent;
        update childEvent;
        delete childEvent;
    }
    
    static testMethod void MessageTriggerTest()
    {
        Message__c message = new Message__c();
        insert message;
        update message;
        delete message;
    }
    
    static testMethod void TutorTriggerTest()
    {
        Tutor__c tutor = new Tutor__c();
        insert tutor;
        update tutor;
        delete tutor;
    }
    
    static testMethod void DayInActivityReportTriggerTest()
    {
        ActivityReport__c[] activitiesReport = [select name, id from ActivityReport__c where ActivityReportStatus__c != :CObjectNames.ActivityReportStatusPaid];
        DayInActivityReport__c dayInActivityReport = new DayInActivityReport__c();
        dayInActivityReport.ActivityReportName__c = activitiesReport[0].id;
        insert dayInActivityReport;
        update dayInActivityReport;
        delete dayInActivityReport;
    }
    
    static testMethod void ExamTriggerTest()
    {
        Exam__c exam = new Exam__c();
        insert exam;
        update exam;
        delete exam;
    }
    
    static testMethod void ProfessionTriggerTest()
    {
        Profession__c profession = new Profession__c();
        insert profession;
        update profession;
        delete profession;
    }
    
    static testMethod void TutorLineTriggerTest()
    {
        Tutor__c tutor = new Tutor__c ();
        insert tutor;
        TutorLine__c tutorLine = new TutorLine__c();
        tutorLine.TutorName__c = tutor.id;
        insert tutorLine;
        update tutorLine;
        delete tutorLine;
    }
    
    static testMethod void SpecialActivitiesLineTriggerTest()
    {
        SpecialActivities__c[] specialsActivities = [select name, id from SpecialActivities__c];
        SpecialActivitiesLine__c specialActivitiesLine = new SpecialActivitiesLine__c();
        specialActivitiesLine.SpecialActivitiesName__c = specialsActivities[0].id;
        specialActivitiesLine.Name = 'TEST';
        insert specialActivitiesLine;
        update specialActivitiesLine;
        delete specialActivitiesLine;
    }
    
    static testMethod void SpecialActivitiesTriggerTest()
    {
        SpecialActivities__c specialActivities = new SpecialActivities__c();
        insert specialActivities;
        update specialActivities;
        delete specialActivities;
    }
    
    static testMethod void DiplomaLineTriggerTest()
    {
        Diploma__c[] diplomas = [select name, id from Diploma__c];
        DiplomaLine__c diplomaLine = new DiplomaLine__c();
        diplomaLine.DiplomaName__c = diplomas[0].id;
        insert diplomaLine;
        update diplomaLine;
        delete diplomaLine;
    }
    
    static testMethod void CheckTriggerTest()
    {
        Payment__c[] payments = [select name, id from Payment__c];
        Check__c check = new Check__c();
        check.PaymentName__c = payments[0].id;
        insert check;
        update check;
        delete check;
    }
    
    static testMethod void SupportTriggerTest()
    {
        Support__c support = new Support__c();
        insert support;
        update support;
        delete support;
    }
    
    static testMethod void DiplomaTriggerTest()
    {
        Family__c f = new Family__c(Name = 'TEST');
        insert f;
        Children__c c = new Children__c (Name='TEST',FamilyName__c = f.Id,FamilyManType__c= 'ילד',Birthdate__c = Date.Today().addYears(-10));
        insert c;
        Diploma__c diploma = new Diploma__c(ChildName__c = c.Id);
        insert diploma;
        update diploma;
        delete diploma;
    }
    
    static testMethod void NoteTriggerTest()
    {
        Note n = new Note();
        Case__c c1 = new Case__c();
        insert c1;
        n.ParentId = c1.id;
        n.Title = 'fff';
        insert n;
        update n;
        delete n;
    }
    
    static testMethod void ExpenditureExceededTriggerTest()
    {
        ActivityReport__c[] activityReports = [select name, id from ActivityReport__c where ActivityReportStatus__c != :CObjectNames.ActivityReportStatusPaid];
        ExpenditureExceeded__c expenditureExceeded = new ExpenditureExceeded__c();
        expenditureExceeded.ActivityReportName__c = activityReports[0].id;
        insert expenditureExceeded;
        update expenditureExceeded;
        delete expenditureExceeded;
    }
    
    static testMethod void UserTriggerTest()
    {   
        User u = [select name, id from User limit 1];
        update u;
    }
    
    static testMethod void CChildDbTest()
    {
        Family__c[] family = [select name, id, CoordinatorUserName__c,OwnerId, City__c from Family__c];
        Children__c[] children = [select name, id, FamilyName__c from Children__c];
        SchoolYear__c[] schoolYear = [select name, id from SchoolYear__c];
        CChildDb childDb = new CChildDb();
        CChildDb.GetAllChildren();
        childDb.GetChildAgeRelativeToChoolYear(children[0].id, schoolYear[0].id);
        childDb.getChildByChildId(children[0].id);
        childDb.GetChildrenByFamily(family[0].id);
        childDb.GetFamilyIdByChildName(children[0].id);
        childDb.OnBirthDate();
        childDb.updateChildrenAfterUpdateFamily(family[0]);
        CChildRegistrationDb childRegistrationDb = new CChildRegistrationDb();
        childDb = new CChildDb(true);
        //childDb.updateTutorsList(childDb.getChildByChildId(children[0].id));
        childDb = new CChildDb(1);
        Children__c child = CChildDb.GetChildById(children[0].id);
        map<string, Children__c> childrenMap = CChildDb.GetChildrenMap();
        //childDb.CreateTaskAfterInsertChild(children[0]);
        //childDb.UpdateChildIsActive(children[0].id);
    }
    
    static testMethod void ChildRegistrationFacadTest()
    {
        ChildRegistration__c [] childRegistration = [select name, ChildName__c from ChildRegistration__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(childRegistration[0]);
        CChildRegistrationFacad childRegistrationFacad = new CChildRegistrationFacad(controller);
        childRegistrationFacad.save();
    }
    
    static testMethod void ChildRegistrationDBTest()
    {
        CChildRegistrationDb childRegistrationDb = new CChildRegistrationDb();
        childRegistrationDb.GetChildRegistration();
    }
    
    static testMethod void ClassComputeTest()
    {
        for (integer age = 5; age <= 17; age++)
            CClassCompute.GetClassByAge(age);   
    }
    
    static testMethod void SchoolYearDbTest()
    {
        CSchoolYearDb.GetActiveYearId();
        //CSchoolYearDb.GetStartSchoolYear();
    }
    
    static testMethod void FamilyDbTest()
    {
         CFamilyDb FamilyDb = new CFamilyDb();
         ChildRegistration__c [] childRegistration = [select name, id, ChildName__c, SchoolYearName__c from ChildRegistration__c];
         SchoolYear__c [] sy = [select name from SchoolYear__c where id = :childRegistration[0].SchoolYearName__c];
         FamilyDb.UpdateNumberOfActivtyChildren(sy[0]);
         FamilyDb.save();
         //FamilyDb.IncreaseNumberOfActivtyChildren(childRegistration[0]);
         FamilyDb.DecreaseNumberOfActivtyChildren(childRegistration[0]);
         childRegistration[0].SchoolYearName__c = CSchoolYearDb.GetActiveYearId();
         //FamilyDb.IncreaseNumberOfActivtyChildren(childRegistration[0]);
         FamilyDb.DecreaseNumberOfActivtyChildren(childRegistration[0]);
         childRegistration[0].ChildName__c = null;
         //FamilyDb.IncreaseNumberOfActivtyChildren(childRegistration[0]);
         FamilyDb.DecreaseNumberOfActivtyChildren(childRegistration[0]);
         FamilyDb.getAllUsersMap();
         FamilyDb.ForTest();
         Coordinator__c[] coordinator = [select name, id, UserName__c from Coordinator__c];
         //FamilyDb.updateFamiliesAfterUpdateCoordinator(coordinator[0].id, coordinator[0].UserName__c);
         Family__c [] family = [select id from Family__c];
         CFamilyDb.GetFamilyNameById(family[0].id);
    }
    
    static testMethod void ChildFacadTest()
    {
        Children__c [] child = [select name, id, BirthDate__c from Children__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(child[0]);
        CChildFacad childFacad = new CChildFacad(controller);
        childFacad.save();
        childFacad.OnNewChild();
        list<CObjectsHistory> pChildHistories = childFacad.pChildHistories;
        Children__c pChild = childFacad.pChild;
        CTutorInChild[] pTutorsByChild = childFacad.pTutorsByChild;
        Family__c[] familys = [select name, id from Family__c];
        child[0] = new Children__c();
        child[0].FamilyName__c = familys[0].id;
        controller = new ApexPages.StandardController(child[0]);
        childFacad = new CChildFacad(controller);
        childFacad.save();
        childFacad.OnBirthDate();
    }
    
    static testMethod void CaseFacadTest()
    {
        Case__c [] tCase = [select id, name, WhoId__c, FamilyName__c, ChildName__c, Origin__c, Status__c,
                            Type__c, Priority__c, Subject__c, Description__c, Comments__c, AssigneeUser__c,
                            OwnerName__c, SendEmail__c
                            from Case__c];
        
        ApexPages.StandardController controller = new ApexPages.StandardController(tCase[tCase.size()-1]);
        CCaseFacad caseFacad = new CCaseFacad(controller);
        caseFacad.save();
        caseFacad.UsersIDidNotChoose = 'yechiel chanzin';
        caseFacad.OnPastWhoChose();
        caseFacad.OnPastWhoIDidNotChoose();
        //caseFacad.OnAddRequestToList();
        caseFacad.OnChangeFamilyOrChild();
        list<selectoption> selects = caseFacad.OptionsChildrenByFamily;
        Case__c mycase = caseFacad.pCase;
       // list<RequestForTreatment__c> requestForTreatments = caseFacad.pRequestForTreatmentList;
        list<CObjectsHistory> histories = caseFacad.pCaseHistories;
    }
    
    static testMethod void CCoordinatorFacadTest()
    {
        Coordinator__c [] coordinator = [select name, id, FirstName__c, LastName__c from Coordinator__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(coordinator[0]);
        CCoordinatorFacad coordinatorFacad = new CCoordinatorFacad(controller);
        coordinatorFacad.save();
        coordinatorFacad.onChangeFirstNameOrLastName();
        string name = coordinatorFacad.pCoordinator.FirstName__c;
        coordinatorFacad.pCoordinator.FirstName__c = null;
        coordinatorFacad.onChangeFirstNameOrLastName();
        coordinatorFacad.pCoordinator.FirstName__c = name;
        coordinatorFacad.pCoordinator.LastName__c = null;
        coordinatorFacad.onChangeFirstNameOrLastName();
    }
    
    static testMethod void CTutorFacadTest()
    {
        Tutor__c [] tutor = [select name, Phone__c, MobilePhone__c, 
                                ApartmentNumber__c, City__c, Street__c,
                                ProfessionsList__c, LastName__c, FirstName__c, POB__c
                                from Tutor__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(tutor[0]);
        CTutorFacad tutorFacad = new CTutorFacad(controller);
        tutorFacad.save();
        CChildInTutor[] pTutorsByChild = tutorFacad.pTutorsByChild;
        Tutor__c pTutor = tutorFacad.pTutor;
        List<TutorLine__c> pTutorLines = tutorFacad.pTutorLines;
        tutorFacad.SaveFromPopup();
    }
        
    static testMethod void RequestForTreatmentFacadTest()
    {
        RequestForTreatment__c [] requestForTreatment = [select name, ProfessionsList__c, Comment__c, id, TeacherReviews__c, Reviews__c, ManagerReviews__c, FamilyChildren__c,  
                                                         CaseName__c, FromDate__c, ToDate__c, TutorName__c, ChildName__c, ProfessionName__c, WeeklyHours__c from RequestForTreatment__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(requestForTreatment[0]);
        CRequestForTreatmentFacad requestForTreatmentFacad = new CRequestForTreatmentFacad(controller);
        requestForTreatmentFacad.save();
    }
    
    static testMethod void ActivityApprovalFacadTest()
    {
        Case__c cas = new Case__c ();
        insert cas;
        RequestForTreatment__c requestForTreatment = new RequestForTreatment__c(CaseName__c = cas.Id);
        insert requestForTreatment;
        ActivityApproval__c  activityApproval = new ActivityApproval__c(RequestForTreatmentName__c = requestForTreatment.id);
        insert activityApproval;
        ApexPages.StandardController controller = new ApexPages.StandardController(activityApproval);
        CActivityApprovalFacad activityApprovalFacad = new CActivityApprovalFacad(controller);
        activityApprovalFacad.save();
    }
    
    static testMethod void ActivityApprovalDbTest()
    {
        Case__c cas = new Case__c ();
        insert cas;
        Children__c[] children = [select name, id, CoordinatorUserName__c from Children__c];
        list<children__c> chil = new list<children__c>();
        RequestForTreatment__c requestForTreatment = new RequestForTreatment__c(CaseName__c = cas.Id);
        insert requestForTreatment;
        ActivityApproval__c[] activityApproval = new ActivityApproval__c[] { new ActivityApproval__c() };
        activityApproval[0].RequestForTreatmentName__c = requestForTreatment.id;
        ApexPages.StandardController controller = new ApexPages.StandardController(activityApproval[0]);
        CActivityApprovalDb activityApprovalDb = new CActivityApprovalDb(controller);
        activityApprovalDb = new CActivityApprovalDb(true);
        activityApprovalDb = new CActivityApprovalDb();
        activityApprovalDb.setActivityApprovalByChildNameMap(chil);
     //   activityApprovalDb.updateActivityApprovalAfterUpdateRequestForTreatment(requestForTreatment[0]);
        activityApproval = [select CreatedById, ChildName__c, TutorName__c, name, PaymentTarget__c, ToDate__c, 
                            WeeklyHours__c, AmountApproved__c, FromDate__c, CoordinatorUserName__c from ActivityApproval__c];
        //activityApprovalDb.createMessageFromActivityApproval(activityApproval[0], activityApproval[0]);
        //activityApprovalDb.createMessageFromActivityApproval(activityApproval[0], null);
        //activityApprovalDb.UpdateActivityApprovalsAfterUpdateChild(children[0].id, children[0].CoordinatorUserName__c);
    }
    
    static testMethod void CExamFacadTest()
    {
        Exam__c [] exam = [select name from Exam__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(exam[0]);
        CExamFacad examFacad = new CExamFacad(controller);
        examFacad.save();
        examFacad.OnChangeGrade();
        examFacad.NewExam();
        examFacad.SetbIsScholarship();
        examFacad.Refersh();
        list<selectOption> ChildRegistrationList = examFacad.ChildRegistrationList;
        Exam__c pExam = examFacad.pExam;
    }
    
    static testMethod void CExamDbTest()
    {
        Exam__c exam = new Exam__c();
        Children__c[] children = [select name, id from Children__c];
        exam.ChildName__c = children[0].id;
        Profession__c[] profession = [select name, id from Profession__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(exam);
        CExamDb examDb = new CExamDb(controller,'');
        examDb.pExam.ChildName__c = children[0].id;
        examDb.pExam.ProfessionName__c = profession[0].id;
        examDb.pExam.Date__c = date.newinstance(2012, 05, 01);
        examDb.save();
        examDb = new CExamDb();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordExcellent;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordVeryGood;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordAlmostVeryGood;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordgood;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordAlmostGood;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordEnough;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = CObjectNames.ExamGradeInWordNotEnough;
        examDb.CheckGradeWords();
        examDb.pExam.GradeInWords__c = '';
        examDb.CheckGradeWords();
        examDb.GetpExam();
        Exam__c[] exams = [select name, id, ChildName__c from Exam__c];
        //examDb.CreateTaskAfterInsertExam(exams[0]);
    }
    
    static testMethod void FamilyFacadTest()
    {
        Family__c [] family = [select name, id, ParentId__c from Family__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(family[0]);
        CFamilyFacad familyFacad = new CFamilyFacad(controller);
        familyFacad.save();
        familyFacad.pFamily.DiedParent__c = CObjectNames.FamilyDiedParentDad;
        familyFacad.OnChangeDiedParent();
        familyFacad.pFamily.DiedParent__c = CObjectNames.FamilyDiedParentMother;
        familyFacad.OnChangeDiedParent();
        familyFacad.pFamily.DiedParent__c = CObjectNames.FamilyDiedParentBoth;
        familyFacad.OnChangeDiedParent();
        familyFacad.OnNewFamily();
        familyFacad.TranslationReasonOfDeath();
        familyFacad.TranslationFamilyState();
    }
    
    static testMethod void DiplomaFacadTest()
    {
        Diploma__c [] diploma = [select name from Diploma__c];
        
            ApexPages.StandardController controller = new ApexPages.StandardController(diploma[0]);
            CDiplomaFacad diplomaFacad = new CDiplomaFacad(controller);
            diplomaFacad.save();
        
    }
    
    static testMethod void DiplomaLineDbTest()
    {
        Diploma__c diploma = new Diploma__c();
        CDiplomaLineDb diplomaLineDb = new CDiplomaLineDb(diploma, true);
        Diploma__c[] diplomas = [select name, id from Diploma__c];
        diplomaLineDb = new CDiplomaLineDb(diplomas[0], true);
        diplomaLineDb.InsertpDiplomaLines();
        diplomaLineDb = new CDiplomaLineDb(diploma, false);
    }
    
    static testMethod void CMyDateTest()
    {
        CMyDate myDate = new CMyDate(1999, 4, 2);
    }
    
    static testMethod void CSupportFacadTest()
    {
        Support__c [] support = [select name from Support__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(support[0]);
        CSupportFacad supportFacad = new CSupportFacad(controller);
        supportFacad.save();
    }
    
    static testMethod void DateConverterTest()
    {
        DateConverter dateConverter = new DateConverter();
        dateConverter.civ2heb(25, 12, 1994);
        dateConverter.DoAllDays();
        dateConverter.DoAllMonths();
        dateConverter.DoAllYears();
    }
    
    static testMethod void CActivityReportFacadTest()
    {
        ActivityReport__c [] activityReport = [select name from ActivityReport__c where ActivityApprovalName__c != null];
        ApexPages.StandardController controller = new ApexPages.StandardController(activityReport[0]);
        CActivityReportFacad activityReportFacad = new CActivityReportFacad(controller);
        activityReportFacad.save();
        controller = new ApexPages.StandardController(activityReport[0]);
        activityReportFacad = new CActivityReportFacad(controller);
        activityReportFacad.save();
        activityReportFacad.OnAddDayToList();
        activityReportFacad.OnChangeActivityApprovalName();
        activityReportFacad.OnChangeTutorName();
        activityReportFacad.OnChangeChildName();
        activityReportFacad.OnChangeProfessionName();
        activityReport[0] = new ActivityReport__c();
        controller = new ApexPages.StandardController(activityReport[0]);
        activityReportFacad = new CActivityReportFacad(controller);
        activityReportFacad.save();
    }
    
    static testMethod void CSpecialActivitiesFacadTest()
    {
        SpecialActivities__c [] specialActivities = [select name from SpecialActivities__c];
        ApexPages.StandardController controller = new ApexPages.StandardController(specialActivities[0]);
        CSpecialActivitiesFacad specialActivitiesFacad = new CSpecialActivitiesFacad(controller);
        specialActivitiesFacad.save();
        specialActivitiesFacad.onChangeList();
        list<selectoption> selects = specialActivitiesFacad.OptionsOperator1;
        selects = specialActivitiesFacad.OptionsOperator2;
        selects = specialActivitiesFacad.OptionsOperator3;
        selects = specialActivitiesFacad.OptionsOperator4;
        selects = specialActivitiesFacad.OptionsOperator5;
        selects = specialActivitiesFacad.OptionsOperator6;
        selects = specialActivitiesFacad.OptionsOperator7;
        selects = specialActivitiesFacad.OptionsField1;
        selects = specialActivitiesFacad.OptionsField2;
        selects = specialActivitiesFacad.OptionsField3;
        selects = specialActivitiesFacad.OptionsField4;
        selects = specialActivitiesFacad.OptionsField5;
        selects = specialActivitiesFacad.OptionsField6;
        selects = specialActivitiesFacad.OptionsField7;
    }
    
    static testMethod void CFilterFamilysTest()
    {
        SpecialActivities__c specialActivities = new SpecialActivities__c();
        CFilterFamilys filterFamilys = new CFilterFamilys();
        List<SelectOption> oFields = filterFamilys.GetOptionsFields();
        List<SelectOption> oOperators = filterFamilys.GetOptionsOperators();
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = 'כפר חבד';
        specialActivities.Field1ToFilter__c = 'City__c';
        Family__c[] familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = 'כפר חבד';
        specialActivities.Field1ToFilter__c = 'ChildrenNumberInFamily__c';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '>';
        specialActivities.Value1ToFilter__c = 'כפר חבד';
        specialActivities.Field1ToFilter__c = 'City__c';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'like %';
        specialActivities.Value1ToFilter__c = 'כפר חבד';
        specialActivities.Field1ToFilter__c = 'City__c';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'like %%';
        specialActivities.Value1ToFilter__c = 'כפר חבד';
        specialActivities.Field1ToFilter__c = 'City__c';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'not like';
        specialActivities.Value1ToFilter__c = 'כפר חבד';
        specialActivities.Field1ToFilter__c = 'City__c';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'not like';
        specialActivities.Value1ToFilter__c = '5';
        specialActivities.Field1ToFilter__c = 'City__c';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '!=';
        specialActivities.Value1ToFilter__c = '30';
        specialActivities.Field1ToFilter__c = 'ParentAge';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '<';
        specialActivities.Value1ToFilter__c = '30';
        specialActivities.Field1ToFilter__c = 'ParentAge';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '>=';
        specialActivities.Value1ToFilter__c = '30';
        specialActivities.Field1ToFilter__c = 'ParentAge';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '>';
        specialActivities.Value1ToFilter__c = '30';
        specialActivities.Field1ToFilter__c = 'ParentAge';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '<=';
        specialActivities.Value1ToFilter__c = '30';
        specialActivities.Field1ToFilter__c = 'ParentAge';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = '30';
        specialActivities.Field1ToFilter__c = 'ParentAge';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = '12/05/1999';
        specialActivities.Field1ToFilter__c = 'ParentDeathDate';
        familys =  filterFamilys.GetFamiliesByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = '1';
        specialActivities.Field1ToFilter__c = 'ChildrenNumberInFamily__c';
        familys = filterFamilys.GetFamiliesByFilter(specialActivities);
    }
    
    static testMethod void CFilterChildrenTest()
    {
        SpecialActivities__c specialActivities = new SpecialActivities__c();
        CFilterChildren filterChildren = new CFilterChildren();
        List<SelectOption> oFields = filterChildren.GetOptionsFields();
        List<SelectOption> oOperators = filterChildren.GetOptionsOperators();
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = 'נועם דניאל';
        specialActivities.Field1ToFilter__c = 'name';
        Children__c[] children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '>';
        specialActivities.Value1ToFilter__c = 'נועם דניאל';
        specialActivities.Field1ToFilter__c = 'name';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'like %';
        specialActivities.Value1ToFilter__c = 'נועם דניאל';
        specialActivities.Field1ToFilter__c = 'name';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'like %%';
        specialActivities.Value1ToFilter__c = 'נועם דניאל';
        specialActivities.Field1ToFilter__c = 'name';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'not like';
        specialActivities.Value1ToFilter__c = 'נועם דניאל';
        specialActivities.Field1ToFilter__c = 'name';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = 'not like';
        specialActivities.Value1ToFilter__c = 'fff';
        specialActivities.Field1ToFilter__c = 'name';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '!=';
        specialActivities.Value1ToFilter__c = '9';
        specialActivities.Field1ToFilter__c = 'ChildAge';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '<';
        specialActivities.Value1ToFilter__c = '9';
        specialActivities.Field1ToFilter__c = 'ChildAge';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '>=';
        specialActivities.Value1ToFilter__c = '9';
        specialActivities.Field1ToFilter__c = 'ChildAge';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '>';
        specialActivities.Value1ToFilter__c = '9';
        specialActivities.Field1ToFilter__c = 'ChildAge';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '<=';
        specialActivities.Value1ToFilter__c = '9';
        specialActivities.Field1ToFilter__c = 'ChildAge';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
        specialActivities.Operator1ToFilter__c = '=';
        specialActivities.Value1ToFilter__c = '9';
        specialActivities.Field1ToFilter__c = 'ChildAge';
        children =  filterChildren.GetChildrenByFilter(specialActivities);
    }
    
    
}