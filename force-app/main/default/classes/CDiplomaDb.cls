public with sharing class CDiplomaDb 
{
	private Diploma__c pDiploma { get; set; }
	private CDiplomaLineDb diplomaLineDb { get; set; }
	
	public CDiplomaDb()
	{
		pDiploma = new Diploma__c();
	}
	
	public CDiplomaDb(ApexPages.StandardController controller, boolean IsNewPage)
	{
		pDiploma = (Diploma__c)controller.getRecord();
		if (pDiploma.id != null)
			SetpDiplomaBypDiplomaId(pDiploma.id);
		else
		{
			pDiploma.SchoolYearName__c = CSchoolYearDb.GetActiveYearId();
			OnChangeYear();
		}
		diplomaLineDb = new CDiplomaLineDb(pDiploma, IsNewPage);
	}
	
	private void SetpDiplomaBypDiplomaId(string id)
	{
		pDiploma = [select name, id, Half__c, Average__c, ChildName__c, DiplomaImage__c, Grade__c,Activity_Evaluation__c,
						LastModifiedBy.Name,CreatedBy.Name,LastModifiedDate,CreatedDate,
					ChildRegistrationName__c, Date__c, SchoolYearName__c, Note__c from Diploma__c where id = :id];
	}
	
	public void AddDiplomaLine()
	{
		diplomaLineDb.AddDiplomaLine();
	}
	
	
	public void InsertpDiploma()
	{
		//diplomaLineDb.CheckGradeWords();
		GetpDiplomaName();
		//GetGeneralAverage();
		if (pDiploma.id == null)
			insert pDiploma;
		else
			update pDiploma;	
		diplomaLineDb.InsertpDiplomaLines();
	}
	/*
	private void GetGeneralAverage()
	{
		List<CDiplomaLineInList> pDiplomaLines = GetpDiplomaLines();
		integer tosplit = 0;
		decimal res = 0;
		for (integer i = 0; i < pDiplomaLines.size(); i++)
		{
			if (pDiplomaLines[i].pDiplomaLine.IsToSave__c)
			{
				tosplit++;
				if (pDiplomaLines[i].pDiplomaLine.Grade__c != null)
					res += pDiplomaLines[i].pDiplomaLine.Grade__c;
			}
		}
		if (res != 0 && tosplit != 0)
			pDiploma.GeneralAverage__c = res / tosplit;
		else
			pDiploma.GeneralAverage__c = 0;
	}
	*/
	private void GetpDiplomaName()
	{
		SchoolYear__c schoolYear = [select name, id from SchoolYear__c where id = :pDiploma.SchoolYearName__c];
		Children__c child = [select name, id from Children__c where id = :pDiploma.ChildName__c];
		pDiploma.name = child.name + ' - ' + schoolYear.name + ' ' + pDiploma.Half__c;
	}
	
	public Diploma__c GetpDiploma()
	{
		return pDiploma;
	}
	
	public List<CDiplomaLineInList> GetpDiplomaLines()
	{
		return diplomaLineDb.GetpDiplomaLines();
	}
	
	/*public void OnChangeGrade()
	{
		diplomaLineDb.CheckGradeWords();
	}*/
	
	public void OnChangeYear()
	{
		if (pDiploma.SchoolYearName__c != null && pDiploma.ChildName__c != null)
		{
			ChildRegistration__c[] childRegistration = [select name, id from ChildRegistration__c where ChildName__c = :pDiploma.ChildName__c 
														and SchoolYearName__c = :pDiploma.SchoolYearName__c];
			if (childRegistration.size() > 0)
				pDiploma.ChildRegistrationName__c = childRegistration[0].id;	
		}
	}
	
	public static Diploma__c GetDiplomaById(string id)
	{
		Diploma__c[] diploma = [select name, id, Half__c, Average__c, ChildName__c, DiplomaImage__c, ChildName__r.name, Grade__c,
										LastModifiedBy.Name,CreatedBy.Name,LastModifiedDate,CreatedDate,
										ChildRegistrationName__c, Date__c, SchoolYearName__c, SchoolYearName__r.name, Note__c from Diploma__c where id = :id];
		if (diploma.size() > 0)
			return diploma[0];
		return null;
	}
	
	/*public void CreateTaskAfterInsertDiploma(Diploma__c newDiploma)
    {
    	list<Task> listToInsert = new list<Task>();
		list<string> ownersId = GetUsersIdToOwners();
		Children__c child = getChild(newDiploma);
		for (integer i = 0; i < ownersId.size(); i++)
		{
			//Task newTask = new Task();
			//newTask.Description = CObjectNames.TaskWaitForYouFemale + CObjectNames.TaskSobjectNewDiploma;
			//newTask.ChildName__c = child.name;
			//newTask.FamilyName__c = child.FamilyName__r.name;
			//newTask.City__c = child.FamilyName__r.City__c;
			//newTask.CoordinatorUserName__c = system.Userinfo.getName();
			//newTask.Subject = CObjectNames.TaskSobjectNewDiploma;
			///newTask.WhatId = newDiploma.id;
			//newTask.OwnerId = ownersId[i];
			//newTask.ActivityDate = datetime.now().date().addDays(7);
			//listToInsert.add(newTask);
		}
		if (listToInsert.size() > 0)
			insert listToInsert;
    }*/
    
    /*public Children__c getChild(Diploma__c newDiploma)
	{
		if (newDiploma.ChildName__c != null)
		{
			Children__c[] child = [select name, id, FamilyName__r.name, FamilyName__r.City__c from Children__c where id = :newDiploma.ChildName__c];
			if (child.size() > 0)
			{
				return child[0];
			}
		}
		return null;
	}*/
    
    /*private string[] GetUsersIdToOwners()
    {
    	UserRole[] userRoles = [select name, id from UserRole where name = :CObjectNames.RoleActivityManager];
		User[] managers = [select name, id from User where UserRoleId = :userRoles[0].id];
		list<string> ret = new list<string>();
		for (integer i = 0; i < managers.size(); i++)
			ret.add(managers[i].id);
		return ret;
    }*/
    
    private list<selectOption> professionsOptions;
	public list<selectOption> pProfessionsOptions
	{
		get
		{
			if (professionsOptions == null)
			{
			 	professionsOptions = new list<selectOption>();
			 	professionsOptions.add(new selectOption('', '--ללא--'));
			 	list<Profession__c> professionslist = [select name, id, IsTheoretical__c from Profession__c where ForDiploma__c = :true order by name];
			 	for(Profession__c profession : professionslist)
			 	{
			 		professionsOptions.add(new selectOption(profession.id, profession.name));
			 	}
			}
			return professionsOptions;
		}
	}
	
}