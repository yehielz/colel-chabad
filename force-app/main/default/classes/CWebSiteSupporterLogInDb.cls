public with sharing class CWebSiteSupporterLogInDb 
{
	public string UserNameWarning { get; set; }
	public string PasswordWarning { get; set; }
	public string pUserName { get; set; }
    public string pPassword { get; set; }
    public SessionID__c pSessionId { get; set; }
    public Support__c pSupporter { get; set; }
    
    public CWebSiteSupporterLogInDb()
    {
    	pSessionId = new SessionID__c();
		SetUserName();
    	SetPassword();
    }
    
    private void SetUserName()
    {
    	pUserName = Apexpages.currentPage().getParameters().get('UserName');
    }
    
    private void SetPassword()
    {
    	pPassword = Apexpages.currentPage().getParameters().get('Password');
    }
    
	public boolean IfExist()
    {
    	UserNameWarning = '';
    	PasswordWarning = '';
    	boolean ret = validateUserName();
    	if (ret)
    		ret = validatePassword();
		pSupporter = CSupportDb.GetSupporterByLoginInf(pUserName, pPassword);
		return ret;
    }
    
    private boolean validateUserName()
    {
    	if (pUserName == null || pUserName == '')
    	{
    		UserNameWarning = CObjectNames.UserNameIsNull;
			return false;
    	}
    	else if (!CSupportDb.IsUserNameExist(pUserName))
    	{
    		UserNameWarning = CObjectNames.UserNameIsNotExist;
			return false;
    	}
    	return true;
    }
    
    private boolean validatePassword()
    {
    	if (pPassword == null || pPassword == '')
    	{
    		PasswordWarning = CObjectNames.PasswordIsNull;
			return false;
    	}
    	else if (!CSupportDb.IsUserNameAndPasswordExist(pUserName, pPassword))
    	{
    		PasswordWarning = CObjectNames.PasswordIsNotValid;
			return false;
    	}
    	return true;
    }
    
    public void SetCookie()
	{
		Cookie sessionId = new Cookie('sessionId', insertSessionId(), null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { sessionId } );
	}
	
	private string InsertSessionId()
	{
		if (pSupporter != null)
		{
			pSessionId = new SessionID__c();
			pSessionId.name = string.valueOf(Math.random());
			pSessionId.SupporterName__c = pSupporter.id;
			pSessionId.InTime__c = datetime.now();
			pSessionId.LestActivityTime__c = datetime.now();
			insert pSessionId;
			return pSessionId.id;
		}
		return '';
	}
	
	public void forTest()
	{
		pUserName = '';
		ValidateUserName();
		pUserName = 'l';
		ValidateUserName();
		pUserName = 'levi';
		ValidateUserName();
		pPassword = '';
		ValidatePassword();
		pPassword = 'q';
		ValidatePassword();
		pPassword = '123';
		SetCookie();
		IfExist();
		ValidatePassword();
	}
}