public with sharing class CWebSiteLogInFacad 
{
	private CWebSiteLogInDb webSiteLogInDb { get; set; }
    
    public boolean pIsError{ get{ return (UserNameWarning != null && UserNameWarning.trim() != '') || (PasswordWarning != null && PasswordWarning.trim() != '') || (approvalCodeWarning != null && approvalCodeWarning.trim() != ''); } }
    public string pRetUrl{ 
    	get{
    		if(webSiteLogInDb.approval != null)  return CObjectNames.WebSitePageMonthes;
    		return /*(webSiteLogInDb.pTutor != null) ||*/ (webSiteLogInDb.pFamily != null) ? CObjectNames.WebSitePageHome :  CObjectNames.WebSiteSupporterHome  ; 
    	} 
    }
    public SessionID__c pSessionId{ get{ return webSiteLogInDb.pSessionId; } }
    /*public string pUserName{ get{ return webSiteLogInDb.pUserName; } set{ webSiteLogInDb.pUserName = value; } } 
    public string pPassword{ get{ return webSiteLogInDb.pPassword; } set{ webSiteLogInDb.pPassword = value; } }*/
    public string response{ 
    	get{ 
    		return '{"UNError":" ' + UserNameWarning + ' ","PError":" ' + PasswordWarning + ' ","AError":" ' + approvalCodeWarning + ' ","IsError":' + pIsError + ',"sessionId":"' + 
    				pSessionId.id + '","RetUrl":"' + pRetUrl + '"}'; 
    	} 
    }
    
    public CWebSiteLogInFacad(){
    	webSiteLogInDb = new CWebSiteLogInDb();
    }
    
    public string UserNameWarning{ get{ return webSiteLogInDb.UserNameWarning; } }
	public string PasswordWarning{ get{ return webSiteLogInDb.PasswordWarning; } }
	public string approvalCodeWarning{ get{ return webSiteLogInDb.approvalCodeWarning; } }
    
    public void LogIn(){
    	if (webSiteLogInDb.IfExist()){
			webSiteLogInDb.SetCookie();
		}
    }
    
    public void LogInApprovalCode(){
    	if (webSiteLogInDb.IfExistApprovalCode()){
			webSiteLogInDb.SetCookie();
		}
    }
}