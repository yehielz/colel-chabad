public with sharing class CTabsCtl 
{
	public PageReference GoToTab()
	{
		System.debug(String.valueOf(Apexpages.currentPage()).contains('lightning'));
		if (UserInfo.getUiTheme() == 'Theme4d'||String.valueOf(Apexpages.currentPage()).contains('lightning'))
			return new PageReference(CObjectNames.GetBaseUrl() + '/lightning/o/'+ getobjectName() +'/home?nooverride=1');
		else
		{
			return new PageReference(CObjectNames.GetBaseUrl() + '/' + pTabUrl);
		}
	}
	
	private string mTabUrl;
    public string pTabUrl
    {
    	get
    	{
    		if (mTabUrl == null)
    		{
    			string page = GetPage();
    			if (page != null)
    			{
    				if (page.contains('ChildrenTab'))
    					mTabUrl = Children__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('CasesTab'))
    					mTabUrl = Case__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('CoordinatorTab'))
    					mTabUrl = Coordinator__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('TutorTab'))
    					mTabUrl = Tutor__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('SpecialActivitiesTab'))
    					mTabUrl = SpecialActivities__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('ProfessionTab'))
    					mTabUrl = Profession__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('PaymentTab'))
    					mTabUrl = Payment__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('FamilyTab'))
    					mTabUrl = Family__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('ActivityReportTab'))
    					mTabUrl = ActivityReport__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('ActivityApprovalTab'))
    					mTabUrl = ActivityApproval__c.sObjectType.getDescribe().getKeyPrefix();
    				else if (page.contains('RequestForTreatmentTab'))
    					mTabUrl = RequestForTreatment__c.sObjectType.getDescribe().getKeyPrefix();
    			}
    		}
    		return mTabUrl;
    	}
    	set
    	{
    		mTabUrl = value;
    	}
	}
	
	public string getobjectName(){
    		
		string page = GetPage();
		string objectName = 'Family__c';
		if (page != null)
		{
			if (page.contains('ChildrenTab'))
				objectName = 'Children__c';
			else if (page.contains('CasesTab'))
				objectName = 'Case__c';
			else if (page.contains('CoordinatorTab'))
				objectName = 'Coordinator__c';
			else if (page.contains('TutorTab'))
				objectName = 'Tutor__c';
			else if (page.contains('SpecialActivitiesTab'))
				objectName = 'SpecialActivities__c';
			else if (page.contains('ProfessionTab'))
				objectName = 'Profession__c';
			else if (page.contains('PaymentTab'))
				objectName = 'Payment__c';
			else if (page.contains('FamilyTab'))
				objectName = 'Family__c';
			else if (page.contains('ActivityReportTab'))
				objectName = 'ActivityReport__c';
			else if (page.contains('ActivityApprovalTab'))
				objectName = 'ActivityApproval__c';
			else if (page.contains('RequestForTreatmentTab'))
				objectName = 'RequestForTreatment__c';
		}
		return objectName;
    }
    
    public string GetPage()
    {
    	return string.valueOf(ApexPages.currentPage());
    }
}