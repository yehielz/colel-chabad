@isTest
private class Ctrl04_MassEmailPaymentTest {

    static testMethod void myUnitTest() {
    	List<Payment__c> payments = [select Id From Payment__c limit 10];
    	ApexPages.StandardSetController records = new ApexPages.StandardSetController (payments);
    	records.setSelected(payments);
        Ctrl04_MassEmailPayment ctrl = new Ctrl04_MassEmailPayment (records);
        ctrl.init();
    }
}