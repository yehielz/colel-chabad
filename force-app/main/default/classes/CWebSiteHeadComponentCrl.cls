public without sharing class CWebSiteHeadComponentCrl 
{
    public string pMonth { get; set; }
    public string pProfession { get; set; }
    public string pChild { get; set; }
     
    private Tutor__c mTutor;
    public Tutor__c pTutor{
        get{
            return mTutor;
        }
        set{
            if (value != null)
                mTutor = value;
            else
                mTutor = new Tutor__c();
        }
    }
    
    private Family__c mFamily;
    public Family__c pFamily {
        get  {
            return mFamily;
        }
        set {
            if (value != null)
                mFamily = value;
            else
                mFamily = new Family__c();
        }
    }
    
    private ActivityApproval__c mApproval;
    public ActivityApproval__c pApproval {
        get  {
            return mApproval;
        }
        set {
            if (value != null){
                mApproval = value;
            }
            else
                mApproval = new ActivityApproval__c();
        }
    }
    
    public string pSelectedPage { get; set; }
    private string NewTabs { get; set; }
    public string pNewTabs 
    { 
        get
        {
            if (NewTabs == null || NewTabs == '')
                NewTabs = '[]';
            return NewTabs;
        }
        set
        {
            NewTabs = value;
            if (NewTabs == null || NewTabs == '')
                NewTabs = '[]';
        } 
    }
    
    public string HomeUrl{ get{ return CObjectNames.WebSitePageHome; } }
    public string ReportUrl{ get{ return CObjectNames.WebSitePageMonthes; } }
    public string ViewReportsUrl{ get{ return CObjectNames.WebSitePageReportsDisplay; } }
    public string ViewApporvalsUrl{ get{ return CObjectNames.WebSitePageApprovals; } }
    
    public PageReference LogOut()
    {
        Cookie sessionIdCookie = new Cookie('sessionId', null, null, -1, false);
        ApexPages.currentPage().setCookies(new Cookie[] { sessionIdCookie } );
        CSessionIdDb.DeleteSessionsIdWithLastTime();
        return new PageReference(CObjectNames.WebSitePageLogIn);
    }
}