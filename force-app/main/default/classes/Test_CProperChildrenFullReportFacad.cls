@isTest(seeAllData=true)
private class Test_CProperChildrenFullReportFacad {

    static testMethod void TestCProperChildrenFullReportFacad() {
    	
    	String childStr = '';
    	List<Children__c> childs = [select Id from Children__c limit 10];
    	for(Children__c c :childs)
     		childStr += c.Id + '-';
     		
     	Pagereference p = new Pagereference('/apex/ProperChildrenFullReport?children=' + childStr);
     	Test.setCurrentPage(p);
     	CProperChildrenFullReportFacad ctrl = new CProperChildrenFullReportFacad();
     	ctrl.BackToChildrenTab();
     	ctrl.ContinueToPrint();
    }
}