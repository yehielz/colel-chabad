public with sharing class CFamilyRequestFormToPrintFacad 
{
	private list<CChildInFamilyRequestToPrint> pChildren { get; set; }
	public Family__c pFamily { get; set; }
	public SchoolYear__c pSchoolYear { get; set; }
	public string schoolYearName{ get{ return CObjectNames.getHebrewString(pSchoolYear.name); } }
	public string childrenHtmlToView 
	{ 
		get
		{
			return GetChildrenHtml();
		}
	}
	public string todayDate 
	{ 
		get
		{
			return CObjectNames.getHebrewString('תאריך: ' + Datetime.now().day() + ' / ' + datetime.now().month() + ' / ' + datetime.now().year());
		}
	}
	public string noteToPrintPage 
	{ 
		get
		{ 
			return GetNote(pFamily.NoteToPrintPage__c); 
		} 
	}
	
	private string GetNote(string thenote)
	{
		string ret = '';
		if (thenote != null)
		{
			string note = ' ' + thenote;
			for (integer i = 46; i < note.length(); i=55)
			{
				integer fromInSubS = i < 55 ? i: 55;
				integer minus = 0;
				while(note.substring(i-minus, ((i-minus)+1)) != ' ')
					minus++;
				i = i - minus;
				ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
				if (fromInSubS == 46)
					ret += '<label class="head"> ' + CObjectNames.getHebrewString('הערות: ') + ' </label>';
				ret += '<br/>';
				note = note.substring(i, note.length());
			}
			ret += '.' + CObjectNames.getHebrewString(note);
			if (note.length() < 47)
				ret += ' ' + CObjectNames.getHebrewString('הערות: ');
		}
		return ret;
	}
	
	public CFamilyRequestFormToPrintFacad(ApexPages.StandardController ctrl)
	{
        String familyId = ctrl.getId();
		SetSchoolYear();
		SetpFamily(familyId);
		SetpChildren();
	}
	
	private void SetpFamily(String familyID)
	{
		if (familyID != null)
			pFamily = CFamilyDb.GetFamilyById(familyID);
		if (pFamily == null)
			pFamily = new Family__c();
	}
	
	private void SetSchoolYear()
	{
		pSchoolYear = CSchoolYearDb.GetActiveYear();
		if (pSchoolYear == null)
			pSchoolYear = new SchoolYear__c();
	}
	
	
	private void SetpChildren()
	{
		pChildren = new list<CChildInFamilyRequestToPrint>();
		if (pFamily.id != null)
		{
			ChildRegistration__c[] childRegistrations = [select id, name, School__c, SchoolPhoneNumber__c, ChildName__r.IdNumber__c, Class__c,
																EducatorPhoneNumber__c,	AdditionalPhoneNumber__c, ExpectedClass__c, SocialStatus__c, 
																LearningStatus__c, EmotionalStatus__c, ChildName__c, Educator__c, IsScholarship__c, 
																ChildName__r.BirthDate__c, SupportsName__c, ChildName__r.NoteToPrintPage__c, 
																SchoolYearName__c, ChildName__r.name, ChildName__r.FamilyName__c, SchoolName__c,
																SchoolName__r.name, ChildName__r.Gender__c from ChildRegistration__c where 
																ChildName__r.FamilyName__c = :pFamily.id and SchoolYearName__c = :CSchoolYearDb.GetActiveYearId()];
			for (integer i = 0; i < childRegistrations.size(); i++)
			{
				pChildren.add(new CChildInFamilyRequestToPrint(childRegistrations[i]));
			}
		}
		
	}
	
	private string GetChildrenHtml()
	{
		string ret = '';
		ret += GetParentHtml();
		for (integer i = 0; i < pChildren.size(); i++)
		{
			ret += GetHtmlForChild(pChildren[i]);
		}
		return ret;
	}
	
	private string GetHtmlForChild(CChildInFamilyRequestToPrint child)
	{
		string ret = '<div class="childDiv">' +
							'<p>' +
								child.idNumber + ' ' + child.birthDate + ' ' + child.childName + '&nbsp;&nbsp;&nbsp;&nbsp;•' +
							'</p>' +
							'<p>' +
								child.teacherPhone + ' ' + child.teacherName + ' ' + child.classAndSchool + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
							'</p>' +
							child.noteToPrintPage +
						'</div>';
		return ret;
	}
	
	private string GetParentHtml()
	{
		string ret = '<div class="childDiv">' +
				 		'<p>' +
							parentIdNumber + ' ' + familyName + ' ' + parentName + ' - ' + CObjectNames.getHebrewString('פרטי ההורה') 
								+ '&nbsp;&nbsp;&nbsp;&nbsp;•' +
						'</p>' +
						'<p>' +
							'<div>' +
								homePhone + ' ' + residentialAddress + ' - ' +  CObjectNames.getHebrewString('פרטי משפחה') + 
								'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
							'</div>' +
							'<div>' +
								childrenNumberAtHome;
								if (pFamily.Email__c != null && pFamily.Email__c.length() < 26)
									ret += ' ' + email;
								ret += ' ' + mobilePhone + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
							'</div>';
							if (pFamily.Email__c != null && pFamily.Email__c.length() > 25)
									ret += '<div>' +
												email + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
											'</div>';
						ret += '</p>' +
						GetDiedParentDetails() +
					 '</div>';
		return ret;
	}
	
	private string GetDiedParentDetails()
	{
		string father = '<p>' +
						fatherDeathCategory + ' ' + fatherDeathDate + ' ' + diedFatherName + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
					 '</p>';
	 	string mother = '<p>' +
						motherDeathCategory + ' ' + motherDeathDate + ' ' + diedMotherName + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
					 '</p>';
	 	if (pFamily.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
	 		return father + mother;
 		else if (pFamily.DiedParent__c == CObjectNames.FamilyDiedParentDad)
 			return father;
 		else if (pFamily.DiedParent__c == CObjectNames.FamilyDiedParentMother)
 			return mother;
 		return '';
	}
	
	private string parentName
	{
		get
		{
			if (pFamily.ParentName__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.ParentName__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('שם: ') + ' </label>';
			return '';
		}
	}
	public string familyName 
	{ 
		get
		{
			if (pFamily.name != null)
				return '.' + CObjectNames.getHebrewString(pFamily.name) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('משפחה: ') + ' </label>';
			return '';
		}
	}
	public string parentIdNumber 
	{ 
		get
		{
			if (pFamily.ParentId__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.ParentId__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('ת.ז.: ') + ' </label>';
			return '';
		}
	}
	public string residentialAddress
	{ 
		get
		{
			if (pFamily.City__c != null || pFamily.Address__c != null)
			{
				string ret = '.';
				if (pFamily.City__c != null)
					ret += CObjectNames.getHebrewString(pFamily.City__c) + ' ';
				if (pFamily.Address__c != null)
					ret += ' ,' + CObjectNames.getHebrewString(pFamily.Address__c)+ ' ';
				return ret + '<label class="head"> ' + CObjectNames.getHebrewString('כתובת מגורים: ') + ' </label>';
			}
			return '';
		}
	}
	public string homePhone
	{ 
		get
		{
			if (pFamily.HomePhone__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.HomePhone__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('טלפון: ') + ' </label>';
			return '';
		}
	}
	public string mobilePhone
	{ 
		get
		{
			if (pFamily.MobilePhone__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.MobilePhone__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('טלפון נייד: ') + ' </label>';
			return '';
		}
	}
	public string email
	{ 
		get
		{
			if (pFamily.Email__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.Email__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('דוא"ל: ') + ' </label>';
			return '';
		}
	}
	public string childrenNumberAtHome
	{ 
		get
		{
			if (pFamily.ChildrenNumberAtHome__c != null)
				return '.' + CObjectNames.getHebrewString(string.valueOf(pFamily.ChildrenNumberAtHome__c)) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('מס\' בני משפחה )שאינם נשואים(: ') + ' </label>';
			return '';
		}
	}
	public string diedFatherName
	{ 
		get
		{
			if (pFamily.DiedParentName__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.DiedParentName__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('שם אב שנפטר: ') + ' </label>';
			return '';
		}
	}
	public string diedMotherName
	{ 
		get
		{
			if (pFamily.DiedMotherName__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.DiedMotherName__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('שם אם שנפטרה: ') + ' </label>';
			return '';
		}
	}
	public string fatherDeathCategory
	{ 
		get
		{
			if (pFamily.DeathCategory__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.DeathCategory__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('סיבת הפטירה: ') + ' </label>';
			return '';
		}
	}
	public string motherDeathCategory
	{ 
		get
		{
			if (pFamily.MotherDeathCategory__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.MotherDeathCategory__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('סיבת הפטירה: ') + ' </label>';
			return '';
		}
	}
	public string fatherDeathDate
	{ 
		get
		{
			if (pFamily.DeathDate__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.DeathDate__c.day() + '/' + pFamily.DeathDate__c.month() + '/' + 
						pFamily.DeathDate__c.year()) + '<label class="head"> ' + CObjectNames.getHebrewString('תאריך  הפטירה: ') + ' </label>';
			return '';
		}
	}
	public string motherDeathDate
	{ 
		get
		{
			if (pFamily.DeathMotherDate__c != null)
				return '.' + CObjectNames.getHebrewString(pFamily.DeathMotherDate__c.day() + '/' + pFamily.DeathMotherDate__c.month() + '/' + 
						pFamily.DeathMotherDate__c.year()) + '<label class="head"> ' +CObjectNames.getHebrewString('תאריך  הפטירה: ') + ' </label>';
			return '';
		}
	}
}