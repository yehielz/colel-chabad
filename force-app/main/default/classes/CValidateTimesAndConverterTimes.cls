public with sharing class CValidateTimesAndConverterTimes 
{
	public static string FillFields(string sTime)
	{
		if (sTime != null)
		{
			if (sTime.length() == 1)
			{
				string s1 = sTime.substring(0);
				return '0' + s1 + ':' + '00';
			}
			if (sTime.length() == 2)
			{
				string s1 = sTime.substring(0,1);
				string s2 = sTime.substring(1);
				
				return '0' + s1 + ':' + '0' + s2;
			}
			if (sTime.length() == 3)
			{
				string s1 = sTime.substring(0,1);
				string s2 = sTime.substring(1,2);
				string s3 = sTime.substring(2);
				if (s2 == ':')
					return '0' + s1 + ':0' + s3;
				return '0' + s1 + ':' + s2 + s3;
			}
			else if (sTime.length() == 4)
			{
				string s1 = sTime.substring(0,1);
				string s2 = sTime.substring(1,2);
				string s3 = sTime.substring(2,3);
				string s4 = sTime.substring(3);
				if (s2 == ':')
					return '0' + s1 + ':' + s3 + s4;
				else if (s3 == ':')
					return s1 + s2 + ':0' + s4;
				else
					return s1 + s2 + ':' + s3 + s4;
			}
			else if (sTime.length() == 5)
				return sTime;
		}
		return null;
	}
	
	public static integer GetMinuteFromTime(string sTime)
	{
		string s;
		
		integer Minute;
		if (sTime != null)
		{
			if (sTime.length() == 3)
			{	
				s = sTime.subString(1);
				Minute = integer.valueOf(s);
				return Minute;
			}
			else if (sTime.length() == 4)
			{	
				s = sTime.subString(2);
				Minute = integer.valueOf(s);
				return Minute;
			}
			else 
			{
				s = sTime.subString(3);	
				Minute = integer.valueOf(s);
				return Minute;
			}
		}
		return 0;
	}
	
	public static integer GetHoursFromTime(string sTime)
	{
		string s;
		integer Hours;
		if (sTime != null)
		{
			if (sTime.length() == 3)
			{
				s = sTime.subString(0,1);
				Hours = integer.valueOf(s);
				return Hours;
			}
			else if (sTime.length() == 4)
			{
				string s1 = sTime.subString(1,2);
				if (s1 == ':')
				{
					s = sTime.subString(0,1);
					Hours = integer.valueOf(s);
					return Hours;
				}	
				else
				{
					s = sTime.subString(0,2);
					Hours = integer.valueOf(s);
					return Hours;
				}	
			}
			else if (sTime.length() == 5)
			{
				s = sTime.subString(0,2);
				hours = integer.valueOf(s);
				return Hours;
			}
		}
		return 0;
	}
	
	public static integer ComputeTotalHours(integer minuteStart, integer minuteAnd, integer hoursStart, integer hoursAnd)
	{
		integer res = 0;
        if (minuteAnd < minuteStart)
        { 
            res = hoursAnd - hoursStart - 1;
            return res;    
        }
        else
        {
            res = hoursAnd - hoursStart;
            return res;  
        }
	}
	
	public static integer ComputeTotalMinutes(integer minuteStart, integer minuteAnd)
	{
		integer res = 0;
        if (minuteAnd < minuteStart)
        { 
            res = 60 - (minuteStart - minuteAnd);
            return res;  
        }
        else if (minuteAnd > minuteStart)
        {
            res = minuteAnd - minuteStart;
            return res; 
        } 
        else
           return res = 00; 
	}
	
	public static decimal GetTimeToCalculate(string sTime)
	{
		if (sTime != null)
		{
			string sHours = sTime.substring(0, 2);
			string sMinute = sTime.substring(3);
			decimal Hours = decimal.valueOf(sHours);
			decimal Minute = decimal.valueOf(sMinute);
			Minute = (Minute * (100.0/6000.0));
			return Hours + Minute; 
		}
		return 0;
	}
	
	// בדיקה אם הזמן טוב...
	
	public static boolean ValidateIfTimeIsRight(string sFullTime)
	{
		string s1;
		if (sFullTime.length() == 5)
		{
			s1 = sFullTime.subString(0,1);
			if (s1 != '0' && s1 != '1' && s1 != '2' /*&& s1 != '3'*/)
				return false;
			
			if (s1 == '2')
			{
				string s2 = sFullTime.subString(1,2);
				if (s2 != '0' && s2 != '1' && s2 != '2' && s2 != '3')
					return false;
			}
					
			s1 = sFullTime.subString(1,2);
			if (s1 != '0' && s1 != '1' && s1 != '2' && s1 != '3' && s1 != '4' && s1 != '5' && s1 != '6' && s1 != '7' && s1 != '8' && s1 != '9')
				return false;
			
			s1 = sFullTime.subString(2,3);
			if (s1 != ':')
				return false;
				
			s1 = sFullTime.subString(3,4);
			if (s1 != '0' && s1 != '1' && s1 != '2' && s1 != '3' && s1 != '4' && s1 != '5')
				return false;
			
			s1 = sFullTime.subString(4);
			if (s1 != '0' && s1 != '1' && s1 != '2' && s1 != '3' && s1 != '4' && s1 != '5' && s1 != '6' && s1 != '7' && s1 != '8' && s1 != '9')
				return false;
		}
		return true;
	}
}