//Controller used for the AutoComplete Enhancement

public with sharing class AutoCompleteController{

	//Constructors
	
    public AutoCompleteController() {}
    
    public AutoCompleteController(ApexPages.StandardController controller) {}

	//Attributes
	
	private List<String> resultsname = new List<String>();
	private Boolean hasparams = false;
	private Boolean hasnoresults = false;
        
    //Getters and Setters
    
 	public Boolean getHasparams(){

 		return hasparams;
 		
 	}
 	
 	public void clearValues(){
 		
 		hasparams = false;
 		
 	}
 	
 	public Boolean getHasnoresults(){
 	
 		return hasnoresults;	
 	}

	public void avoidRefresh(){
		
	}
 	
     public PageReference searchSuggestions() {

		//Initalize variables, hasparams just indicates that a search has started
        resultsname.clear();   
        hasparams = true;
        hasnoresults = false;

		//Obtain current parameters
        String sobjectname = System.currentPageReference().getParameters().get('objectname');
        String sfieldname = System.currentPageReference().getParameters().get('fieldname');
        String displayfields = System.currentPageReference().getParameters().get('displayfields');
        String additionalfilter = System.currentPageReference().getParameters().get('additionalfilter');
        String stext = '%'+String.escapeSingleQuotes(System.currentPageReference().getParameters().get('aname').trim())+'%';
        //Limit Suggestions to 10 Results
        Integer iLimit = 10;
        
     //Validate if there's an input and get results
     
     if(stext.length() > 2){

		try{
			system.debug('\n\n\n additionalfilter = ' + additionalfilter);
			  String sql = 'select ' + displayfields + ' from ' + sobjectname + ' where ' + sfieldname + ' like \''+stext+'\''; 
			  if(additionalfilter != null && additionalfilter != '')
			  	sql+= ' and ' +  additionalfilter+ ' ';
			  sql += 'limit '+ iLimit;
		         	system.debug('sql = ' + sql);
		         	for(sobject x : Database.query(sql))
		         	{
		        		string[] fieldsArr = displayfields.split(',');
		        		String s = '';
		        		for (string s1 : fieldsArr) 	
		        		{
		        			if (s.length() > 0)
		        				s += ' | ';
		        			s  += String.escapeSingleQuotes(((String)(x.get(s1))));
		        		}
		        		resultsname.add(s);
		        	}
		
			
		}catch(Exception e){
			
			resultsname.add('Unexpected Error, please contact support');	
		}


     }
    
       return null;
  }
  
	  
	  
	 public List<String> getResultsname(){
	  	  //Make sure to clear past values
	      clearValues();
	      if(resultsname.isEmpty()){
			hasnoresults = true;
			resultsname.add('No Results');
	      }
	      return resultsname;
	  }
}