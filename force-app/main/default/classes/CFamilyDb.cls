public with sharing class CFamilyDb
{
    public static boolean IsAfterNumberOfChildren { get; set; }
    
    private Family__c pFamily { get; set; }
    //private Map<string, List<Family__c>> familiesByCoordinatorsMap;
    
    public CFamilyDb(boolean IsForTask) { }
    
    public CFamilyDb()
    {
        pFamily = new Family__c();
        //setFamiliesByCoordinators();
    }
    
    public CFamilyDb(ApexPages.StandardController controller)
    {
        pFamily = (Family__c)controller.getRecord();
        if (pFamily.id != null)
        {
            SetpFamilyBypFamilyId(pFamily.id);
        }
        else
        {
            SetRegistrationDate();
            //SetCoordinatorName();
        }
    }
    
    private void SetpFamilyBypFamilyId(string id)
    {
        pFamily = [select Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, Address__c, ParentPicture__c,OwnerId,
                   HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, DeathDadDateTime__c,
                   ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, DeathMotherDateTime__c,
                   DeathStory__c, DeathCategory__c, CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c, CoordinatorUserName__c,
                   ParentName__c, DiedParentName__c, WorkName__c, ParentId__c, ParentBirthdate__c, OldestBirthdate__c, ZipCode__c,
                   YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, FamilyPicture__c, DeathMotherStory__c, IsActive__c,
                   MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c, HebrewBirthdate__c,
                   EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c, FaxNumber__c,
                   EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, EnglishDiedFatherName__c, ParentBirthdateTime__c,
                   EnglishDiedMotherName__c, EnglishParentName__c, EnglishFamilyName__c, InnerNumber__c, NoteToPrintPage__c,
                   PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c, Username__c, Password__c, Bank_Code_List__c from Family__c where id = :id];
    }
    /*
    private void setFamiliesByCoordinators()
    {
        Family__c[] families = [select Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, Address__c, IsActive__c,
                               HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, DeathDadDateTime__c,
                               ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, DeathMotherDateTime__c,
                               DeathStory__c,  CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c, CoordinatorUserName__c, ZipCode__c,
                               ParentName__c, DiedParentName__c, WorkName__c, ParentId__c, ParentBirthdate__c, OldestBirthdate__c, ParentPicture__c,
                               YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, FamilyPicture__c, DeathMotherStory__c, NoteToPrintPage__c,
                               MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c,
                               EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c, FaxNumber__c,
                               EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, EnglishDiedFatherName__c, DeathCategory__c,
                               ParentBirthdateTime__c, EnglishDiedMotherName__c, EnglishParentName__c, EnglishFamilyName__c, InnerNumber__c,
                               PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c from Family__c];
        familiesByCoordinatorsMap = new Map<string, List<Family__c>>();
        for (integer i = 0; i < families.size(); i++)
        {
            if (familiesByCoordinatorsMap.containsKey(families[i].CoordinatorName__c))
                familiesByCoordinatorsMap.get(families[i].CoordinatorName__c).add(families[i]);
            else
                familiesByCoordinatorsMap.put(families[i].CoordinatorName__c, new List<Family__c> { families[i] });
        }
    }*/
    
    public void save()
    {
        //setCoordinatorUserName();
        if (pFamily.ParentBirthdate__c != null)
           OnBirthDate();

        setTimeToDates();

        if(pFamily.Bank_Code_List__c != null)
            pfamily.BankCode__c = Decimal.valueOf(pFamily.Bank_Code_List__c);

        if (pFamily.id == null)
            insert pFamily;
        else
            update pFamily;
    }
    /*
    private void setCoordinatorUserName()
    {
        if (pFamily.CoordinatorName__c != null)
        {
            Coordinator__c[] coordinator = [select name, id, UserName__r.name from Coordinator__c where id = :pFamily.CoordinatorName__c];
            if (coordinator.size() > 0)
                pFamily.CoordinatorUserName__c = coordinator[0].UserName__r.name;
        }
    }
    */
    private void setTimeToDates()
    {
        pFamily.DeathDadDateTime__c = setTimeToDateTime(pFamily.DeathDate__c);
        pFamily.DeathMotherDateTime__c = setTimeToDateTime(pFamily.DeathMotherDate__c);
        pFamily.ParentBirthdateTime__c = setTimeToDateTime(pFamily.ParentBirthdate__c);
    }
    
    public void OnBirthDate()
    {
    
        date Birthdate = pFamily.ParentBirthdate__c;
        if (pFamily.IsAfterSunset__c)
            Birthdate = pFamily.ParentBirthdate__c.addDays(1);
        DateConverter dateConverter = new DateConverter();
        CMyDate dateH = dateConverter.civ2heb(Birthdate.day(), Birthdate.month(), Birthdate.year());
        pFamily.HebrewBirthYear__c = dateConverter.GetHebrewYearInHebrew(dateH.Year);
        pFamily.HebrewBirthDay__c = dateConverter.GetHebrewDayInHebrew(dateH.Day);
        pFamily.HebrewBirthMonth__c = dateConverter.GetHebrewMonthInHebrew(dateH.Month);
        // pFamily.HebrewBirthYear__c = dateConverter.GetHebrewYearInHebrew(Birthdate.year());
        // pFamily.HebrewBirthDay__c = dateConverter.GetHebrewDayInHebrew(Birthdate.day());
        // pFamily.HebrewBirthMonth__c = dateConverter.GetHebrewMonthInHebrew(Birthdate.month());
        
    }
    

    private integer setTimeToDateTime(Date pDate)
    {
        if (pDate == null)
            return null;
        else
        {
            datetime retTime = date.newInstance(pDate.year(), pDate.month(), pDate.day());
            return getIntFromLongTime(retTime.getTime());
        }
    }
    
    private integer getIntFromLongTime(long fullTime)
    {
        string str = string.valueOf(fullTime);
        if (str.length() < 6)
            return integer.valueOf(str);
        string ret = '';
        for (integer i = 0; i < (str.length() - 5); i++)
        {
            ret += str.substring(i, i + 1); 
        }
        return integer.valueOf(ret);
    }     
    
    public Family__c GetpFamily()
    {
        return pFamily;
    }
    /*
    private void SetCoordinatorName()
    {
        string userProfileId = system.Userinfo.getProfileId();
        Profile pCoordinator = [select name, id from Profile where name = :CObjectNames.UserCoordinator];
        if (userProfileId == pCoordinator.id)
        {
            Coordinator__c[] coordinators = [select name, id, UserName__c from Coordinator__c where UserName__c = :system.Userinfo.getUserId()];
            if (coordinators.size() > 0)
                pFamily.CoordinatorName__c = coordinators[0].id;
        }
        //not necessary
        //pFamily.OwnerId = Userinfo.getUserId();
    }*/
    
    private void SetRegistrationDate()
    {
        pFamily.RegistrationDate__c = system.now().date();
    }
    
    public static string GetFamilyNameById(string familyId)
    {
        Family__c family = [select name from Family__c where id = :familyId];
        return family.name;
    }
    /*
    // update number of childre activity in family - after insert new ChildRegistration to DB
    public void IncreaseNumberOfActivtyChildren(ChildRegistration__c childRegistration)
    {
        if (childRegistration.ChildName__c == null)
            return;
        
        if (childRegistration.SchoolYearName__c != CSchoolYearDb.GetActiveYearId())
            return;
        
        CChildDb childDb = new CChildDb();  
        Family__c family = [select name, ChildrenNumberActive__c from Family__c
                                where id = :childDb.GetFamilyIdByChildName(childRegistration.ChildName__c)];
        family.ChildrenNumberActive__c = family.ChildrenNumberActive__c + 1;
        update family;
    }*/
    
    
    //NEW VERSION
    public static void IncreaseNumberOfActiveChildren(List<Children__c> activeChildren)
    {
        Set<string> familyIds = new Set<string>();
        for(Children__c child :activeChildren)
            familyIds.add(child.FamilyName__c);
        
        
        
        List<Family__c> families = [select name, ChildrenNumberActive__c from Family__c
                                where id in :familyIds];
        for(Family__c f :families){
            f.ChildrenNumberActive__c = f.ChildrenNumberActive__c + 1;
            f.IsActive__c = true;
        }
        update families;
    }
    
    public void DecreaseNumberOfActivtyChildren(ChildRegistration__c childRegistration)
    {
        if (childRegistration.ChildName__c == null)
            return;
        
        if (childRegistration.SchoolYearName__c != CSchoolYearDb.GetActiveYearId())
            return;
        
        CChildDb childDb = new CChildDb();  
        Family__c family = [select name, ChildrenNumberActive__c from Family__c
                                where id = :childDb.GetFamilyIdByChildName(childRegistration.ChildName__c)];
        family.ChildrenNumberActive__c = family.ChildrenNumberActive__c - 1;
        update family;
    }
    
    public void UpdateNumberOfActivtyChildren(SchoolYear__c schoolYear)
    {   
        list<Family__c> familiesToUpdate = new list<Family__c>();
        map<string, list<ChildRegistration__c>> childrenRegistrationByFamiliesMap = CChildRegistrationDb.GetChildrenRegistrationsByFamily(schoolYear.id);
        list<Family__c> familiesList = GetAllFamilies();
        for (Family__c family : familiesList)
        {
            family.ChildrenNumberActive__c = 0;
            if (childrenRegistrationByFamiliesMap.containsKey(family.id))
                family.ChildrenNumberActive__c = childrenRegistrationByFamiliesMap.get(family.id).size();
            familiesToUpdate.add(family);
            /*if (familiesToUpdate.size() > 90)
            {
                CFamilyDb.IsAfterNumberOfChildren = true;
                update familiesToUpdate;    
                familiesToUpdate = new list<Family__c>();
            }*/
        }
        if (familiesToUpdate.size() > 0)
        {
            CFamilyDb.IsAfterNumberOfChildren = true;
            update familiesToUpdate;    
        }
    }
    
    private Map<string, Family__c> GetAllFamilysMap()
    {
        Family__c[] allFamilys = [select Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, Address__c, ParentPicture__c,
                                  HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, NoteToPrintPage__c,
                                  ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, IsActive__c,
                                  DeathStory__c, DeathCategory__c, CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c,
                                  ParentName__c, DiedParentName__c, WorkName__c, ParentId__c, ParentBirthdate__c, OldestBirthdate__c,
                                  YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, FamilyPicture__c, DeathMotherStory__c,
                                  MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c,
                                  EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c,
                                  EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, EnglishDiedFatherName__c, FaxNumber__c,
                                  CoordinatorUserName__c, EnglishDiedMotherName__c, EnglishParentName__c, EnglishFamilyName__c,
                                  PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c from Family__c];
        Map<string, Family__c> allFamilysMap = new Map<string, Family__c>();
        for (integer i = 0; i < allFamilys.size(); i++)
        {
            allFamilys[i].ChildrenNumberActive__c = 0;
            allFamilysMap.put(allFamilys[i].id, allFamilys[i]);
        }
        return allFamilysMap;
    }
    
    public Family__c GetFamilyByFamilyId(string familyId)
    {
        Family__c family = [select Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, EnglishParentName__c, NoteToPrintPage__c,
                            HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, IsActive__c,
                            ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, FaxNumber__c,
                            DeathStory__c, DeathCategory__c, CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c, ParentPicture__c,
                            ParentName__c, DiedParentName__c, WorkName__c, ParentId__c, ParentBirthdate__c, OldestBirthdate__c,
                            YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, FamilyPicture__c, DeathMotherStory__c,
                            MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c,
                            EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c,
                            CoordinatorUserName__c, EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, Address__c,
                            EnglishDiedFatherName__c,EnglishDiedMotherName__c,  EnglishFamilyName__c, ZipCode__c,
                            PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c from Family__c where id = :familyId];
        return family;
    }
    /*
    public void updateFamiliesAfterUpdateCoordinator(string coordinatorId, string coordinatorUserName)
    {
        if (familiesByCoordinatorsMap.containsKey(coordinatorId))
        {
            list<Family__c> families = familiesByCoordinatorsMap.get(coordinatorId);
            list<Children__c> children = [select name, id, CoordinatorUserName__c from Children__c where FamilyName__c in :families];
            list<Case__c> cases = [select name, id, CoordinatorUserName__c from Case__c where FamilyName__c in :families or ChildName__c in :children];
            list<ActivityApproval__c> activityApprovals = [select name, id, CoordinatorUserName__c from ActivityApproval__c where ChildName__c in :children];
            list<RequestForTreatment__c> requestForTreatments = [select name, id, CoordinatorUserName__c from RequestForTreatment__c where ChildName__c in :children];
            list<ActivityReport__c> activityReports = [select name, id, CoordinatorUserName__c from ActivityReport__c where ChildName__c in :children];
            UpdateFamilies(families, coordinatorUserName);
            UpdateChildren(children, coordinatorUserName);
            UpdateCases(cases, coordinatorUserName);
            UpdateActivityApprovals(activityApprovals, coordinatorUserName);
            UpdateRequestForTreatments(requestForTreatments, coordinatorUserName);
            UpdateActivityReports(activityReports, coordinatorUserName);
        }
    }
    */
    /*
    private void UpdateActivityReports(List<ActivityReport__c> activityReports, string coordinatorUserName)
    {
        list<ActivityReport__c> toUpdate = new list<ActivityReport__c>();
        for (ActivityReport__c report : activityReports)
        {
            report.CoordinatorUserName__c = coordinatorUserName;
            toUpdate.add(report);
            /*if (toUpdate.size() > 100)
            {
                CActivityReportDb.IsAfterPaymentOrChangeCoordinator = true;
                update toUpdate;
                toUpdate = new list<ActivityReport__c>();
            }*//*
        }
        if (toUpdate.size() > 0)
        {
            CActivityReportDb.IsAfterPaymentOrChangeCoordinator = true;
            update toUpdate;
        }
    }*/
    /*
    private void UpdateRequestForTreatments(List<RequestForTreatment__c> requestForTreatments, string coordinatorUserName)
    {
        list<RequestForTreatment__c> toUpdate = new list<RequestForTreatment__c>();
        for (RequestForTreatment__c request : requestForTreatments)
        {
            request.CoordinatorUserName__c = coordinatorUserName;
            toUpdate.add(request);
            /*if (toUpdate.size() > 100)
            {
                CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
                update toUpdate;
                toUpdate = new list<RequestForTreatment__c>();
            }*//*
        }
        if (toUpdate.size() > 0)
        {
            CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
            update toUpdate;
        }
    }*/
    /*
    private void UpdateActivityApprovals(List<ActivityApproval__c> activityApprovals, string coordinatorUserName)
    {
        list<ActivityApproval__c> toUpdate = new list<ActivityApproval__c>();
        for (ActivityApproval__c approval : activityApprovals)
        {
            approval.CoordinatorUserName__c = coordinatorUserName;
            toUpdate.add(approval);
            /*if (toUpdate.size() > 100)
            {
                CActivityApprovalDb.IsAfterChangeCoordinator = true;
                update toUpdate;
                toUpdate = new list<ActivityApproval__c>();
            }*//*
        }
        if (toUpdate.size() > 0)
        {
            CActivityApprovalDb.IsAfterChangeCoordinator = true;
            update toUpdate;
        }
    }*/
    /*
    private void UpdateCases(List<Case__c> cases, string coordinatorUserName)
    {
        for (Case__c thecase : cases)
        {
            thecase.CoordinatorUserName__c = coordinatorUserName;
        }
        if (cases.size() > 0)
            update cases;
    }*/
    /*
    private void UpdateChildren(List<Children__c> children, string coordinatorUserName)
    {
        list<Children__c> toUpdate = new list<Children__c>();
        for (Children__c child : children)
        {
            child.CoordinatorUserName__c = coordinatorUserName;
            toUpdate.add(child);
            /*if (toUpdate.size() > 100)
            {
                CChildDb.IsAfterUpdateTutorsOrActive = true;
                update toUpdate;
                toUpdate = new list<Children__c>();
            }*//*
        }
        if (toUpdate.size() > 0)
        {
            CChildDb.IsAfterUpdateTutorsOrActive = true;
            update toUpdate;
        }
    }
    */
    /*
    private void UpdateFamilies(List<Family__c> families, string coordinatorUserName)
    {
        list<Family__c> toUpdate = new list<Family__c>();
        for (Family__c family : families)
        {
            family.CoordinatorUserName__c = coordinatorUserName;
            toUpdate.add(family);
            /*if (toUpdate.size() > 100)
            {
                CFamilyDb.IsAfterNumberOfChildren = true;
                update toUpdate;
                toUpdate = new list<Family__c>();
            }*//*
        }
        if (toUpdate.size() > 0)
        {
            CFamilyDb.IsAfterNumberOfChildren = true;
            update toUpdate;
        }
    }*/
    
    public Map<string, User> getAllUsersMap()
    {
        User[] allUsers = [select name, id from User];
        Map<string, User> ret = new Map<string, User>();
        for (integer i = 0; i < allUsers.size(); i++)
        {
            ret.put(allUsers[i].id, allUsers[i]);
        }
        return ret;
    }
    
    public void ForTest()
    {
        setTimeToDateTime(datetime.now().date());
    }
    
    /*public void CreateTaskAfterInsertFamily(Family__c newFamily)
    {
        //list<Task> listToInsert = new list<Task>();
        list<string> ownersId = GetUsersIdToOwners();
        for (integer i = 0; i < ownersId.size(); i++)
        {
            //Task newTask = new Task();
            //newTask.Description = CObjectNames.TaskWaitForYouFemale + CObjectNames.TaskSobjectNewFamily;
            //newTask.FamilyName__c = newFamily.name;
            //newTask.City__c = newFamily.City__c;
            //newTask.CoordinatorUserName__c = system.Userinfo.getName();
            //newTask.Subject = CObjectNames.TaskSobjectNewFamily;
            //newTask.WhatId = newFamily.id;
            //newTask.OwnerId = ownersId[i];
            //newTask.ActivityDate = datetime.now().date().addDays(7);
            //listToInsert.add(newTask);
        }
        if (listToInsert.size() > 0)
            insert listToInsert;
    }*/
    
    /*private string[] GetUsersIdToOwners()
    {
        list<string> ret = new list<string>();
        UserRole[] userRoles = [select name, id from UserRole where name = :CObjectNames.RoleActivityManager];
        if (userRoles.size() > 0)
        {
            User[] managers = [select name, id from User where UserRoleId = :userRoles[0].id];
            for (integer i = 0; i < managers.size(); i++)
                ret.add(managers[i].id);
        }
        return ret;
    }*/
    
    public list<CObjectsHistory> GetFamilyHistories()
    {
        Family__History[] caseHistories = [select ParentId, CreatedDate, CreatedBy.Name, CreatedById, Field, NewValue, OldValue from 
                                                Family__History where ParentId = :pFamily.id order by CreatedDate desc];
        list<CObjectsHistory> ret = new list<CObjectsHistory>();
        for (integer i = 0; i < caseHistories.size(); i++)
        {
            ret.add(new CObjectsHistory(caseHistories[i].Field, caseHistories[i].CreatedBy.Name, caseHistories[i].OldValue,
                                        caseHistories[i].NewValue, caseHistories[i].CreatedDate, Schema.SObjectType.Family__c.fields.getMap()));
        }
        return ret;
    }
    
    public static Family__c GetFamilyById(string id)
    {
        Family__c[] families = [select ParentId__c, Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, Address__c, ZipCode__c,
                                       HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, DeathDadDateTime__c,
                                       ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, DeathMotherDateTime__c,
                                       DeathCategory__c, CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c, CoordinatorUserName__c,
                                       DeathStory__c, ParentName__c, DiedParentName__c, WorkName__c, ParentBirthdate__c, OldestBirthdate__c,
                                       YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, FamilyPicture__c, DeathMotherStory__c, IsActive__c,
                                       MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c,
                                       EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c, FaxNumber__c,
                                       EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, EnglishDiedFatherName__c, ParentBirthdateTime__c,
                                       EnglishDiedMotherName__c, EnglishParentName__c, EnglishFamilyName__c, InnerNumber__c, NoteToPrintPage__c,TermsAgreement__c ,
                                       ParentPicture__c, PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c from Family__c where id = :id];
        if (families.size() > 0)
            return families[0];
        return null;
    }
    
    public static list<Family__c> GetAllFamilies()
    {
        return [select ParentId__c, Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, Address__c,
               HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, DeathDadDateTime__c,
               ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, DeathMotherDateTime__c,
               DeathCategory__c, CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c, CoordinatorUserName__c, ZipCode__c,
               DeathStory__c, ParentName__c, DiedParentName__c, WorkName__c, ParentBirthdate__c, OldestBirthdate__c, ParentPicture__c,
               YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, FamilyPicture__c, DeathMotherStory__c, IsActive__c,
               MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c,
               EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c, FaxNumber__c,
               EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, EnglishDiedFatherName__c, ParentBirthdateTime__c,
               EnglishDiedMotherName__c, EnglishParentName__c, EnglishFamilyName__c, InnerNumber__c, NoteToPrintPage__c, 
               CoordinatorName__r.name, PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c from Family__c];
    }
    
    public static list<Family__c> GetAllFamiliesWithOutPhoto()
    {
        return [select ParentId__c, Region__c, DiedParent__c, Comments__c, WorkPhone__c, MobilePhone__c, Address__c,
               HomePhone__c, id, name, Email__c,Email2__c,Email3__c, ChildrenNumberInFamily__c, ChildrenNumberActive__c, DeathDadDateTime__c,
               ChildrenNumberAtHome__c, EconomicStatus__c, MaritalStatus__c, LeadSource__c, City__c, DeathMotherDateTime__c,
               DeathCategory__c, CoordinatorName__c, ReligiousLevel__c, GeneralBackground__c, CoordinatorUserName__c,
               DeathStory__c, ParentName__c, DiedParentName__c, WorkName__c, ParentBirthdate__c, OldestBirthdate__c, 
               YoungestBirthdate__c, DeathDate__c, RegistrationDate__c, DeathMotherStory__c, IsActive__c, ZipCode__c,
               MotherDeathCategory__c, DiedMotherName__c, DeathMotherDate__c, EnglishDiedParent__c, EnglishAddress__c,
               EnglishMaritalStatus__c, EnglishDeathFatherStory__c, EnglishDeathMotherStory__c, EnglishCity__c, FaxNumber__c,
               EnglishFatherDeathCategory__c, EnglishMotherDeathCategory__c, EnglishDiedFatherName__c, ParentBirthdateTime__c,
               EnglishDiedMotherName__c, EnglishParentName__c, EnglishFamilyName__c, InnerNumber__c, NoteToPrintPage__c, 
               CoordinatorName__r.name, PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c from Family__c];
    }
    
    public static map<string, Family__c> GetAllFamiliesMap()
    {
        list<Family__c> families = CFamilyDb.GetAllFamilies();
        map<string, Family__c> ret = new map<string, Family__c>();
        for (Family__c family : families)
        {
            ret.put(family.id, family);
        }
        return ret;
    }
    
    public static map<string, Family__c> GetAllFamiliesMapWithOutPhoto()
    {
        list<Family__c> families = CFamilyDb.GetAllFamiliesWithOutPhoto();
        map<string, Family__c> ret = new map<string, Family__c>();
        for (Family__c family : families)
        {
            ret.put(family.id, family);
        }
        return ret;
    }
    
    public static boolean IsUserNameExist(string userName){
        Family__c[] families = [select name, id from Family__c where UserName__c != null and UserName__c = :userName];
        if (families.size() > 0)
            return true;
        return false;
    }
    
    public static boolean IsUserNameAndPasswordExist(string userName, string password) {
        Family__c[] families = [select name, id from Family__c where UserName__c != null and UserName__c = :userName 
                                 and Password__c != null and Password__c = :password];
        if (families.size() > 0)
            return true;
        return false;
    }
    public static Family__c GetFamilyByLoginInf(string userName, string password) {
        Family__c[] families = [select name, id from Family__c where UserName__c != null and UserName__c = :userName and Password__c != null 
                                   and Password__c = :password];
        if (families.size() > 0)
            return families[0];
        return null;
    } 
}