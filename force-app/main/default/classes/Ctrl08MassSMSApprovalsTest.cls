@isTest(SeeAllData=true)
private class Ctrl08MassSMSApprovalsTest {

    static testMethod void myUnitTest() {
    	List<ActivityApproval__c> approvals = [select Id From ActivityApproval__c limit 10];
    	ApexPages.StandardSetController records = new ApexPages.StandardSetController (approvals);
    	records.setSelected(approvals);
        Ctrl08MassSMSApprovals ctrl = new Ctrl08MassSMSApprovals (records);
        ctrl.init();
        ctrl.sendSMS();
        ctrl.getFieldsToDisplay();
        
    }
}