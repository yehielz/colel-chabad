public with sharing class CTutorLineDb 
{
	private Tutor__c pTutor { get; set; }
	private List<TutorLine__c> pAllTutorLines { get; set; }
	private List<TutorLine__c> pTutorLinesView { get; set; }
	public string pSelectedProfessionTypes { get; set; }
	
	public CTutorLineDb(Tutor__c extTutor)
	{
		pTutor = extTutor;
		pSelectedProfessionTypes = 'all';
		FillpAllTutorLines();
		if (pTutor.id != null)
		{
			SetpAllTutorLinesBypTutorId(pTutor.id);
		}
		SetFilterdAndSortedList();
	}
	
	private void FillpAllTutorLines()
	{
		pAllTutorLines = new List<TutorLine__c>();
		Profession__c[] professions = [select name, id, MinRate__c from Profession__c order by name];
		for (integer i = 0; i < professions.size(); i++)
		{
			TutorLine__c tempTutorLine = new TutorLine__c();
			tempTutorLine.ProfessionName__c = professions[i].id;
			tempTutorLine.Rate__c = professions[i].MinRate__c;
			pAllTutorLines.add(tempTutorLine);
		}
	}
	
	private void SetpAllTutorLinesBypTutorId(string id)
	{
		List<TutorLine__c> tempAllTutorLines = [select name, id, TutorName__c, ProfessionName__c, IsSelected__c, 
											 	Rate__c from TutorLine__c where TutorName__c = :id order by ProfessionName__r.name];
 		Map<string, TutorLine__c> tempAllTutorLinesMap = GetMapFromList(tempAllTutorLines);
 		for (integer i = 0; i < pAllTutorLines.size(); i++)
 		{
 			if (tempAllTutorLinesMap.containsKey(pAllTutorLines[i].ProfessionName__c))
 			{
 				pAllTutorLines.set(i, tempAllTutorLinesMap.get(pAllTutorLines[i].ProfessionName__c));
 			}
 		}
	}
	
	private Map<string, TutorLine__c> GetMapFromList(List<TutorLine__c> tutorLines)
	{
		map<string, TutorLine__c> ret = new map<string, TutorLine__c>();
		for (integer i = 0; i < tutorLines.size(); i++)
		{
			ret.put(tutorLines[i].ProfessionName__c, tutorLines[i]);
		}
		return ret;
	}
	
	private Map<string, Profession__c> GetMapFromList(List<Profession__c> professions)
	{
		Map<string, Profession__c> ret = new Map<string, Profession__c>();
		for (integer i = 0; i < professions.size(); i++)
		{
			ret.put(professions[i].id, professions[i]);
		}
		return ret;
	}
	
	public void InsertTutorLines()
	{
		pAllTutorLines = sendToDeleteNotSelected(pAllTutorLines);
		Map<string, Profession__c> allProfessions = GetAllProfessionsMap();
		List<TutorLine__c> tutorLinesToInsert = new List<TutorLine__c>();
		List<TutorLine__c> tutorLinesToUpdate = new List<TutorLine__c>();
		for (integer i = 0; i < pAllTutorLines.size(); i++)
		{
			pAllTutorLines[i].name = pTutor.name + ' - ' + allProfessions.get(pAllTutorLines[i].ProfessionName__c).name;
			if (pAllTutorLines[i].id == null)
			{
				pAllTutorLines[i].TutorName__c = pTutor.id;
				tutorLinesToInsert.add(pAllTutorLines[i]);
			}
			else
				tutorLinesToUpdate.add(pAllTutorLines[i]);
		}
		if (tutorLinesToInsert.size() > 0)
			insert tutorLinesToInsert;
		else if (tutorLinesToUpdate.size() > 0)
			update tutorLinesToUpdate;
	}
	
	private List<TutorLine__c> sendToDeleteNotSelected(List<TutorLine__c> tutorLines)
	{
		List<TutorLine__c> toReturn = new List<TutorLine__c>();
		List<TutorLine__c> toDelete = new List<TutorLine__c>();
		for (integer i = 0; i < tutorLines.size(); i++)
		{
			integer rezolt = RemoveOrDeleteFromList(tutorLines, i);
			if (rezolt == 1)
				toReturn.add(tutorLines[i]);
			else if (rezolt == -1)
				toDelete.add(tutorLines[i]);
		}
		if (toDelete.size() > 0)
			delete toDelete;
		
		return toReturn;
	}
	
	private integer RemoveOrDeleteFromList(List<TutorLine__c> tutorLines, integer i)
	{
		if (tutorLines[i].IsSelected__c)
			return 1;
		if (tutorLines[i].id == null)
			return 0;
		else 
			return -1;
	}
	
	private Map<string, Profession__c> GetAllProfessionsMap()
	{
		List<Profession__c> allProfessions = [select name, id from Profession__c];
		return GetMapFromList(allProfessions);
	}
	
	public List<TutorLine__c> GetTutorLines()
	{
		return pTutorLinesView;
	}
	
	public static map<string, TutorLine__c> GetTutorLinesMapByTutorAndProfession()
	{
		TutorLine__c[] tutorLines = [select name, id, TutorName__c, ProfessionName__c, IsSelected__c, Rate__c from TutorLine__c];
		map<string, TutorLine__c> ret = new map<string, TutorLine__c>();
		for (TutorLine__c tutorLine : tutorLines)
		{
			string ids = string.valueOf(tutorLine.TutorName__c) + tutorLine.ProfessionName__c;
			ret.put(ids, tutorLine);
		}
		return ret;
	}
	
	public void SetFilterdAndSortedList()
	{
		map<string, list<TutorLine__c>> mapForSort = new map<string, list<TutorLine__c>>();
		for (TutorLine__c line : pAllTutorLines)
		{
			if (ValidateLineIsToAdd(line))
			{
				if (mapForSort.containsKey(line.name))
					mapForSort.get(line.name).add(line);
				else
					mapForSort.put(line.name, new list<TutorLine__c> { line });
			}
		}
		pTutorLinesView = new list<TutorLine__c>();
		list<string> keiesList = new list<string>();
        keiesList.addAll(mapForSort.keyset());
        keiesList.sort();
        for (integer i = keiesList.size() - 1; i >= 0; i--)
        {
        	pTutorLinesView.addAll(mapForSort.get(keiesList[i]));
        }
	}
	
	private boolean ValidateLineIsToAdd(TutorLine__c line)
	{
		return (pSelectedProfessionTypes.contains('all') || (pProfessionWithType.containsKey(line.ProfessionName__c) 
				&& pProfessionWithType.get(line.ProfessionName__c).ProfessionTypeName__c != null &&
				pSelectedProfessionTypes.contains(pProfessionWithType.get(line.ProfessionName__c).ProfessionTypeName__c)));
	}
	
	private list<Profession__c> professions;
	private list<Profession__c> pProfessions
	{
		get
		{
			if (professions == null)
				professions = [select name, id, ProfessionTypeName__c from Profession__c];
			return professions;
		}
	}
	
	private map<string, Profession__c> professionWithType;
	private map<string, Profession__c> pProfessionWithType
	{
		get
		{
			if (professionWithType == null)
			{
				professionWithType = new map<string, Profession__c>();
				for (Profession__c profession : pProfessions)
				{
					professionWithType.put(profession.id, profession);
				}
			}
			return professionWithType;
		}
	}
}