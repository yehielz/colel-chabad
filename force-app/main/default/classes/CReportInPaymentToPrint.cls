public with sharing class CReportInPaymentToPrint
{
    public ActivityReport__c pActivityReport { get; set; }
    
    public CReportInPaymentToPrint(ActivityReport__c mActivityReport)
    {
        pActivityReport = mActivityReport;
    }
    
    public string ProfessionName
    {
        get
        {
            if (pActivityReport.ProfessionName__c != null && pActivityReport.ProfessionName__r.name != null)
                return CObjectNames.getHebrewString(pActivityReport.ProfessionName__r.name);
            return '';
        }
    }
    
    public string TutorName
    {
        get
        {
            if (pActivityReport.TutorName__c != null && pActivityReport.TutorName__r.name != null)
                return CObjectNames.getHebrewString(pActivityReport.TutorName__r.name);
            return '';
        }
    }
    
    public string ChildName
    {
        get
        {
            if (pActivityReport.ChildName__c != null && pActivityReport.ChildName__r.name != null)
                return CObjectNames.getHebrewString(pActivityReport.ChildName__r.name);
            return '';
        }
    }
}