public with sharing class CFamiliesPaymentsReportFacad 
{
	private CFamiliesPaymentsReportDb familiesPaymentsReportDb { get; set; }
	public decimal pSumChildrenActivitiesAmount { get{ return familiesPaymentsReportDb.pSumChildrenActivitiesAmount(); } }
	public decimal pSumFamilyActivitiesAmount { get{ return familiesPaymentsReportDb.pSumFamilyActivitiesAmount(); } }
	public decimal pSumReportsAmount { get{ return familiesPaymentsReportDb.pSumReportsAmount(); } }
	public decimal pSumTotalAmount { get{ return familiesPaymentsReportDb.pSumTotalAmount(); } }
	public string pHtml { get;set; }
	
	public string sStartDate
	{ 
		get
		{ 
			if (familiesPaymentsReportDb.pStartDate != null)
				return CObjectNames.GetFullNumberInString(familiesPaymentsReportDb.pStartDate.day()) + '/' + CObjectNames.GetFullNumberInString(familiesPaymentsReportDb.pStartDate.month()) + '/' + familiesPaymentsReportDb.pStartDate.year();
			return '';
		} 
		set
		{
			if (value != null && value.trim() != '' && value.split('/').size() == 3)
			{
				try
				{
					string[] dateList = value.split('/');
					familiesPaymentsReportDb.pStartDate = date.newInstance(integer.valueOf(dateList[2]), integer.valueOf(dateList[1]), integer.valueOf(dateList[0]));
				}
				catch(Exception e)
				{
					familiesPaymentsReportDb.pStartDate = null;
				}
			}
		}
	}
	public string sEndDate
	{ 
		get
		{ 
			if (familiesPaymentsReportDb.pEndDate != null)
				return CObjectNames.GetFullNumberInString(familiesPaymentsReportDb.pEndDate.day()) + '/' + CObjectNames.GetFullNumberInString(familiesPaymentsReportDb.pEndDate.month()) + '/' + familiesPaymentsReportDb.pEndDate.year();
			return '';
		} 
		set
		{ 
			if (value != null && value.trim() != '' && value.split('/').size() == 3)
			{
				try
				{
					string[] dateList = value.split('/');
					familiesPaymentsReportDb.pEndDate = date.newInstance(integer.valueOf(dateList[2]), integer.valueOf(dateList[1]), integer.valueOf(dateList[0]));
				}
				catch(Exception e)
				{
					familiesPaymentsReportDb.pEndDate = null;
				}
			}
		}
	}
	
	public list<CFamilyInPaymentReport> pFamilies{ get{ return familiesPaymentsReportDb.pFamilies; } }
	public ActivityReport__c pActivityReport { get{ return familiesPaymentsReportDb.pActivityReport; } set { familiesPaymentsReportDb.pActivityReport = value; } }
	public Children__c pChild { get{ return familiesPaymentsReportDb.pChild; } set { familiesPaymentsReportDb.pChild = value; } }
	public string pExcelUrl { get; set; }
	
	public CFamiliesPaymentsReportFacad()
	{
		familiesPaymentsReportDb = new CFamiliesPaymentsReportDb();
	}
	public void searchWithFilters()
	{
		familiesPaymentsReportDb.searchWithFilters();
	}
	
	public void DownloadExcel()
	{
		Document d = GetExcelDocument('excel report without children');
		d.body = GetExcelPageBlob(false);
		d.ContentType = 'application/vnd.ms-excel; charset=utf-8';
		d.Name = 'דוח תשלום למשפחות ' + system.today();
		d.FolderId = GetFolderId();
		d.Type = 'xls';
		upsert d;
		pExcelUrl = '/servlet/servlet.FileDownload?file=' + d.id;
	}
	public void DownloadExcelWithChildren()
	{
		Document d = GetExcelDocument('excel report');
		d.body = GetExcelPageBlob(true);
		d.ContentType = 'application/vnd.ms-excel; charset=utf-8';
		d.Name = 'דוח תשלום למשפחות כולל ילדים ' + system.today();
		d.FolderId = GetFolderId();
		d.Type = 'xls';
		upsert d;
		pExcelUrl = '/servlet/servlet.FileDownload?file=' + d.id;
	}
	
	private Document GetExcelDocument(string name)
	{
		Document[] d = [select name, id, Url, Body, ContentType, Type from Document where name = :name];
		if (d.size() > 0)
			return d[0];
		else
			return new Document();
	}
	
	private string GetFolderId()
	{
		Folder[] f = [select name, id from Folder where Type = 'Document' limit 1];
		if (f.size() > 0)
			return f[0].id;
		return null;
	}
	
	public string GetFormat(decimal num)
	{
		if (num != null)
		{
			string numStr = num.format();
			system.debug('numStr = '+ numStr);
			if (numStr.contains('.'))
			{
				string afterPoint = numStr.split('\\.')[1];
				if (afterPoint.length() < 2)
					return numStr + '0';
				else
					return numStr + afterPoint.substring(0, 2);
			}
			else
				return numStr + '.00';
		}
		return '0.00';
	}
	
	private blob GetExcelPageBlob(boolean isWithChildren)
	{
		/*string url = CObjectNames.getBaseUrl() + page.FamiliesPaymentsReportEXEL.getUrl() + '?startDate=' + sStartDate + '&endDate=' + sEndDate + '&familyId=' + pChild.FamilyName__c + '&professionId=' + pActivityReport.ProfessionName__c + '&isdtp=vw';
		system.debug('blob url = ' + url);
		PageReference p = new PageReference(url);
		blob ret = p.getContent();
		system.debug('ret.tostring() = ' + ret.toString());*/
		string html = '';
		if (isWithChildren)
		{
			html = '<html><head><style type="text/css">.FamiliesTable{width: 100%;border-collapse: collapse;}.FamiliesTable th, .FamiliesTable tfoot td{width: auto;background-color: #70ABAB;' + 
					  'color: white; padding: 6px;vertical-align: middle;text-align: center;border: 1px solid white;font-size: 10pt;}.FamiliesTable tbody tr td{width: auto;' +
					  'padding: 8px;vertical-align: middle;text-align: center;border: 1px solid white;}.FamiliesTable tbody tr.family td{background-color: #DDEDED;}' + 
					  '.FamiliesTable tr.child td{background-color: #C4DEEC;}</style>' + 
					  '</head><body>' + getTableWithChildrenHtmlStr() + '</body></html>';
		}
		else
		{
			html = '<html><head><style type="text/css">.FamiliesTable{width: 100%;border-collapse: collapse;}.FamiliesTable th, .FamiliesTable tfoot td{width: auto;background-color: #70ABAB;' + 
					  'color: white; padding: 6px;vertical-align: middle;text-align: center;border: 1px solid white;font-size: 10pt;}.FamiliesTable tbody tr td{width: auto;' +
					  'padding: 8px;vertical-align: middle;text-align: center;border: 1px solid white;background-color: #DDEDED;}</style>' + 
					  '</head><body>' + getTableHtmlStr() + '</body></html>';
		}
		pHtml = html;
		blob ret = blob.valueOf(html);
		system.debug('ret.tostring() = ' + ret.toString());
		return ret;
	}
	
	public string getTableHtmlStr()
	{
		string ret = '<table class="FamiliesTable">';
		ret += '<thead>' +
					'<tr>' +
						'<th>שם</th>' +
						'<th>סכום דיווחי פעילות</th>' +
						'<th>סכום פעילוית משפחה מיוחדת</th>' +
						'<th>סכום פעילויות ילדים מיוחדות</th>' +
						'<th>סה"כ</th>' +
					'</tr>' +
			   '</thead>' +
			   '<tbody>';
		for (integer i = 248; i < (pFamilies.size() < 300 ? pFamilies.size() : 300); i++)
		{
			ret += '<tr class="family">' +
				'<td>' + 
					' <a href="' + CObjectNames.GetBaseUrl() + '/' + pFamilies[i].pFamily.id + '">' + pFamilies[i].pFamily.name + '</a>' +  
				'</td>' + 
				'<td>' + 
					GetFormat(pFamilies[i].pReportsAmount) +  
				'</td>' +
				'<td>' +
					GetFormat(pFamilies[i].pFamilyActivitiesAmount) +
				'</td>' +
				'<td>' +
					GetFormat(pFamilies[i].pChildrenActivitiesAmount) +
				'</td>' +
				'<td>' +
					GetFormat(pFamilies[i].pTotalAmount) +
				'</td>' +
			'</tr>';
		}
		ret += '</tbody>' +	
				'<tfoot>' +
					'<tr>' +
						'<td >' + 
						'סיכום' +  
						'</td>' + 
						'<td>' + 
							GetFormat(pSumReportsAmount) +  
						'</td>' + 
						'<td>' + 
							GetFormat(pSumFamilyActivitiesAmount) +  
						'</td>' +
						'</td>' + 
						'<td>' + 
							GetFormat(pSumChildrenActivitiesAmount) +  
						'</td>' +
						'<td>' +
							GetFormat(pSumTotalAmount) +
						'</td>' +
					'</tr>' +
			   '</tfoot>' +	
				'</table>';
		return ret;
	}
	
	public string getTableWithChildrenHtmlStr()
	{ 
		string ret = '<table class="FamiliesTable">';
		ret += '<thead>' +
					'<tr>' +
						'<th>שם</th>' +
						'<th>סכום דיווחי פעילות</th>' +
						'<th>סכום פעילוית משפחה מיוחדת</th>' +
						'<th>סכום פעילויות ילדים מיוחדות</th>' +
						'<th>סה"כ</th>' +
					'</tr>' +
			   '</thead>' +
			   '<tbody>';
		for (CFamilyInPaymentReport family :pFamilies)
		{
			ret += '<tr class="family">' +
				'<td>' + 
					'<a href="' + CObjectNames.GetBaseUrl() + '/' + family.pFamily.id + '">' + family.pFamily.name + '</a>' +  
				'</td>' + 
				'<td>' + 
					'-' +  
				'</td>' +
				'<td>' +
					GetFormat(family.pFamilyActivitiesAmount) +
				'</td>' + 
				'<td>' + 
					'-' +  
				'</td>' +
				'<td>' +
					GetFormat(family.pTotalAmountWithoutChildren) +
				'</td>' +
			'</tr>';
			for (CFamilyInPaymentReport.CChildInList child : family.pChildrenList)
			{
				ret += '<tr class="' + family.pFamily.id + ' child">' +
					'<td>' + 
						'<a href="' + CObjectNames.GetBaseUrl() + '/' + child.id + '">' + child.ChildName + '</a>' +  
					'</td>' + 
					'<td>' + 
						GetFormat(child.ReportsAmount) +  
					'</td>' +
					'<td>' +
					'</td>' + 
					'<td>' + 
						GetFormat(child.ActivitiesAmount) +  
					'</td>' +
					'<td>' +
						GetFormat(child.TotalAmount) +
					'</td>' +
				'</tr>';
			}
		}
		ret += '</tbody>' +	
				'<tfoot>' +
					'<tr>' +
						'<td >' + 
						'סיכום' +  
						'</td>' + 
						'<td>' + 
							GetFormat(pSumReportsAmount) +  
						'</td>' + 
						'<td>' + 
							GetFormat(pSumFamilyActivitiesAmount) +  
						'</td>' +
						'</td>' + 
						'<td>' + 
							GetFormat(pSumChildrenActivitiesAmount) +  
						'</td>' +
						'<td>' +
							GetFormat(pSumTotalAmount) +
						'</td>' +
					'</tr>' +
			   '</tfoot>' +	
				'</table>';
		return ret;
	}
}