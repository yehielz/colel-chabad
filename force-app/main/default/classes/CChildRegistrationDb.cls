public with sharing class CChildRegistrationDb 
{
	private ChildRegistration__c pRegistrationChild { get; set; }
	
	public CChildRegistrationDb()
	{
		pRegistrationChild = new ChildRegistration__c();
	}
	
	public CChildRegistrationDb(ApexPages.StandardController controller)
	{
		pRegistrationChild = (ChildRegistration__c)controller.getRecord();
		if (pRegistrationChild.Id != null)
		{
			pRegistrationChild = [select name, School__c, SchoolPhoneNumber__c, SchoolName__c, SchoolName__r.SchoolPhone__c,
								  SchoolName__r.NotherPhone__c, EducatorPhoneNumber__c,	AdditionalPhoneNumber__c, Class__c,
								  ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c,	ChildName__c, 
								  Educator__c, IsScholarship__c, SchoolYearName__c, SupportsName__c from ChildRegistration__c 
								  where id = :pRegistrationChild.Id];
		}
		else
		{
			SetSchoolYearName();
			SetClass();
		}
	}
	
	public void save()
	{
		if (pRegistrationChild.Id != null)
		{
			update pRegistrationChild;
		}
		else
		{
			insert pRegistrationChild;
		}
	}
	
	public ChildRegistration__c GetChildRegistration()
	{
		return pRegistrationChild;
	}
	
	private void SetSchoolYearName()
	{
		pRegistrationChild.SchoolYearName__c = CSchoolYearDb.GetActiveYearId();
	}
	
	public void SetClass()
	{
		CChildDb childDb = new CChildDb();
		pRegistrationChild.Class__c = childDb.GetChildAgeRelativeToChoolYear(pRegistrationChild.ChildName__c, pRegistrationChild.SchoolYearName__c);
		pRegistrationChild.ExpectedClass__c = childDb.GetChildAgeRelativeToChoolYear(pRegistrationChild.ChildName__c, pRegistrationChild.SchoolYearName__c);
	}
	
	public static map<string, list<ChildRegistration__c>> GetChildrenRegistrationsByFamily(string schoolYearId)
    {
        ChildRegistration__c[] childrenRegistration = [select name, SchoolPhoneNumber__c, id, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, SchoolName__c,
        													  IsScholarship__c, Class__c, ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c, 
        													  ChildName__c, School__c, ChildName__r.FamilyName__c, Educator__c, SchoolYearName__c, SupportsName__c 
        													  from ChildRegistration__c where SchoolYearName__c = :schoolYearId];
     	map<string, list<ChildRegistration__c>> ret = new map<string, list<ChildRegistration__c>>();
     	for (ChildRegistration__c registration : childrenRegistration)
     	{
     		if (ret.containsKey(registration.ChildName__r.FamilyName__c))
     			ret.get(registration.ChildName__r.FamilyName__c).add(registration);
     		else
     			ret.put(registration.ChildName__r.FamilyName__c, new list<ChildRegistration__c> { registration });
     	}
        return ret;
    }
    
    public static ChildRegistration__c GetChildRegistrationByChildAndYear(string schoolYearId, string childId)
    {
        ChildRegistration__c[] registration = [select name, SchoolPhoneNumber__c, id, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, 
        											  SchoolName__c, IsScholarship__c, Class__c, ExpectedClass__c, SocialStatus__c, LearningStatus__c, 
        											  EmotionalStatus__c, ChildName__c, School__c, ChildName__r.FamilyName__c, Educator__c, 
        											  SchoolYearName__c, SupportsName__c, SchoolName__r.name, SchoolYearName__r.StartDate__c,
        											  SchoolYearName__r.IsActive__c from ChildRegistration__c 
        											  where SchoolYearName__c = :schoolYearId and ChildName__c = :childId];
     	if (registration.size() > 0)
     		return registration[0];
        return null;
    }
    
    public static list<ChildRegistration__c> GetChildRegistrationByChildrenAndYear(string schoolYearId, list<string> childrenIds)
    {
        return [select name, SchoolPhoneNumber__c, id, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, SchoolName__c,
			    IsScholarship__c, Class__c, ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c, 
			    ChildName__c, School__c, ChildName__r.FamilyName__c, Educator__c, SchoolYearName__c, SupportsName__c,
			    SchoolYearName__r.StartDate__c, SchoolYearName__r.IsActive__c from ChildRegistration__c 
			    where SchoolYearName__c = :schoolYearId and ChildName__c in :childrenIds];
    }
    
    public static list<ChildRegistration__c> GetChildRegistrationByChild(string childId)
    {
        return [select name, SchoolPhoneNumber__c, id, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, SchoolName__c,
			    IsScholarship__c, Class__c, ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c, 
                ChildName__c, School__c, ChildName__r.FamilyName__c, Educator__c, SchoolYearName__c, SupportsName__c, 
				SchoolName__r.name, SchoolYearName__r.StartDate__c, SchoolYearName__r.IsActive__c 
				from ChildRegistration__c where ChildName__c = :childId];
    }
    
    public static string GetActiveChildRegistrationIdByChild(string childId)
    {
    	if (childId != null && childId != '')
    	{
	        try
	        {
	        	ChildRegistration__c [] registrations = [select id from ChildRegistration__c where ChildName__c = :childId AND SchoolYearName__r.IsActive__c = true limit 1];
				system.debug('registrations = ' + registrations);
				if (registrations != null && registrations.size() > 0)
					return registrations[0].id;
	        }
	        catch(Exception ex)
	        {
	        	system.debug('ex = ' + ex);
	        }
    	}
		return null;
		
    }
    
    public static list<ChildRegistration__c> GetChildRegistrationBySupporter(string supporterId)
    {
        return [select name, SchoolPhoneNumber__c, id, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, SchoolName__c,
			    IsScholarship__c, Class__c, ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c, 
                ChildName__c, School__c, ChildName__r.FamilyName__c, Educator__c, SchoolYearName__c, SupportsName__c,
				SchoolYearName__r.StartDate__c, SchoolYearName__r.IsActive__c, SchoolName__r.name, SchoolYearName__r.EndOfSchoolYear__c from ChildRegistration__c 
				where SupportsName__c = :supporterId];
    }
    
    public static list<ChildRegistration__c> GetChildRegistrationByFilter(string filter)
    {
    	string query = ' select name, SchoolPhoneNumber__c, id, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, SchoolName__c, ' +
			    	   ' IsScholarship__c, Class__c, ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c, ' +  
            		   ' ChildName__c, School__c, ChildName__r.FamilyName__c, Educator__c, SchoolYearName__c, SupportsName__c, ' + 
            		   ' SchoolYearName__r.StartDate__c, SchoolName__r.name, SchoolYearName__r.EndOfSchoolYear__c, SchoolYearName__r.IsActive__c from ChildRegistration__c  ';
        if (filter != null && filter.trim() != '')
        	query += ' where ' + filter;
    	system.debug('query = ' + query);
        return Database.query(query);
    }
    
    public list<CObjectsHistory> GetChildRegistrationHistories()
	{
		ChildRegistration__History[] childRegistrationHistories = [select ParentId, CreatedDate, CreatedBy.Name, CreatedById, Field, NewValue, OldValue from 
        																  ChildRegistration__History where ParentId = :pRegistrationChild.id order by CreatedDate desc];
	  	list<CObjectsHistory> ret = new list<CObjectsHistory>();
	  	for (integer i = 0; i < childRegistrationHistories.size(); i++)
	  	{
	  		ret.add(new CObjectsHistory(childRegistrationHistories[i].Field, childRegistrationHistories[i].CreatedBy.Name, childRegistrationHistories[i].OldValue,
	  									childRegistrationHistories[i].NewValue, childRegistrationHistories[i].CreatedDate, Schema.SObjectType.ActivityApproval__c.fields.getMap()));
	  	}
  		return ret;
	}
}