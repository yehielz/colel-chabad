public without sharing class Ctrl05_SiteFamilyUpdate {
	
	public Case currentCase {get;set;}
	
    public Ctrl05_SiteFamilyUpdate(){
    	currentCase = new Case();
    }
    
    public void submitCase(){
    	
    	List<Family__c> families = [select Id,ownerId From Family__c where Email__c =:currentCase.Email__c or ParentId__c =:currentCase.Id_Number__c];
    	if(families.size()  > 0 ){
    		currentCase.ownerId = families[0].OwnerId;
    		currentCase.FamilyName__c = families[0].Id;
    	}
    	currentCase.Origin = 'אתר חסד מ"מ';
    	currentCase.Contact_Name__c = currentCase.FirstName__c + ' ' + currentCase.LastName__c;
    	insert currentCase;	
    }
    
}