public with sharing class CPaymentPrintCtrl
{
    public list<CPaymentToPrint> pPayments { get; set; }
    public String paymentId {
    	get;
    	set{
    		pPayments = new list<CPaymentToPrint>();
	        string paymentsIds = value;
	        if (paymentsIds != null && paymentsIds.trim() != '')
	        {
	            Payment__c[] payments = GetPayments(paymentsIds.trim());
	            for (Payment__c payment : payments)
	            {
	                pPayments.add(new CPaymentToPrint(payment));
	            }
	        }
    	}
    }
    /*
    public CPaymentPrintCtrl()   {
    	SetPayments();
    }*/
    
    
    public void SetPayments() {
        pPayments = new list<CPaymentToPrint>();
        string paymentsIds = Apexpages.currentPage().getParameters().get('ids');
        if (paymentId != null)     paymentsIds = paymentId;
        if (paymentsIds != null && paymentsIds.trim() != '')
        {
            Payment__c[] payments = GetPayments(paymentsIds.trim());
            for (Payment__c payment : payments)
            {
                pPayments.add(new CPaymentToPrint(payment));
            }
        }
    }
    
    private list<Payment__c> GetPayments(string paymentsIds)
    {
        while(paymentsIds.startsWith('-'))
        {
            paymentsIds = paymentsIds.substring(1);
        }
        while(paymentsIds.endsWith('-'))
        {
            paymentsIds = paymentsIds.substring(0, paymentsIds.length() - 1);
        }
        return [select name, id, IsDone__c, FamilyOrTutor__c, Comment__c, Debt__c, TutorName__c, Balance__c, NatureOfPayment__c, Status__c, FamilyName__c, Amount__c, 
                AmountPaidByMasav__c, MasavFile__c, ParentName__c, City__c, Date__c, FamilyName__r.name, TutorName__r.name, FamilyName__r.ParentName__c, 
                FamilyName__r.Address__c, FamilyName__r.City__c, FamilyName__r.HomePhone__c, FamilyName__r.AccountNumber__c, FamilyName__r.AffiliateNumber__c,
                TutorName__r.FirstName__c, TutorName__r.LastName__c, TutorName__r.ApartmentNumber__c, TutorName__r.Street__c, TutorName__r.City__c,
                TutorName__r.Phone__c, TutorName__r.AffiliateNumber__c, TutorName__r.AccountNumber__c, PaymentTarget__c,
                                     
                (select name, id, ActivityApprovalName__c, ProgressOrProblemsWithChild__c, Note__c, ClarificationReason__c, CommentsForPayment__c, ReportMonth__c, 
                 MonthToFiltering__c, TutorName__c, ChildName__c, IsForFindOut__c, ActivityReportStatus__c, ProfessionName__c, FromDateClass__c, TotalPaid__c, 
                 TotalReportHours__c, TalkTotalHours__c, TalkTotalHoursToCalculate__c, TotalPayment__c, TalkType__c, ReasonForDecline__c, PaymentAmountClass__c, 
                 ReceiptSum__c, ToDateClass__c, TalkWithDirectorOrTeacherOrParent__c, ReceiptDetails__c, ChildLastName__c, CoordinatorUserName__c, ChildFirstName__c, 
                 TalkStartTime__c, TalkEndTime__c, ReceiptDate__c, ReceivingImageClass__c, PaymentName__c, ChildName__r.name, TutorName__r.name, ProfessionName__r.name
                 from ActivityReportList__r
                 WHERE TotalPayment__c > 0
                 order by ChildName__r.name),
                                      
                (select name, id, ChildName__c, ChildOrFamily__c, IsInActivity__c, Note__c, FamilyName__c, PaymentAmount__c, SpecialActivitiesName__c, paid__c,
                 PaymentName__c, SpecialActivitiesName__r.Date__c, FamilyName__r.name, ChildName__r.name, SpecialActivitiesName__r.name 
                 from SpecialActivitiesPayment__r 
                 WHERE PaymentAmount__c > 0
                 order by FamilyName__r.name, ChildName__r.name)
                 
                from Payment__c 
                where id in :paymentsIds.split('-') 
                //AND Amount__c > 0
                order by FamilyName__r.name, Date__c];
    }
}