public with sharing class CBottomBorderInWebSiteCtl 
{
	private Support__c mSupporter;
	public Support__c pSupporter
	{
		get
		{
			return mSupporter;
		}
		set
		{
			if (value != null)
				mSupporter = value;
			else
				mSupporter = new Support__c();
		}
	}
	public boolean IsEnglish
	{ 
		get
		{ 
			boolean ret = false;
			if (pSupporter != null && pSupporter.LanguageForWS__c != null && pSupporter.LanguageForWS__c.contains('English'))
				ret = true;
			string lang = Apexpages.currentPage().getParameters().get('lang');
	    	if (lang != null && lang == 'en')
	    		ret = true;
	    	if (lang != null && lang == 'he')
	    		ret = false;
    		return ret; 
		} 
	}
}