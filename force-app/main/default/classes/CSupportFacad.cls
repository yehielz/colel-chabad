public with sharing class CSupportFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	private CSupportDb supportDb;
	
	public Support__c pSupport{ get{return supportDb.GetpSupport(); } }
	
	public list<CChildRegistrationInsupporter> pChildRegistrationInSupporter{ get{ return supportDb.pChildRegistrationInSupporter; } }
		
	public CSupportFacad(ApexPages.StandardController controller)
	{
		supportDb = new CSupportDb(controller);
		errorMessage = '';
		if (pSupport.id == null)
			pageTitle = 'תומך חדש';
		else
			pageTitle = pSupport.name;
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		supportDb.save();
		PageReference newCasePage = new ApexPages.StandardController(pSupport).view();
        return newCasePage;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CSupportValidator validator = new CSupportValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public integer PageNumber{ get{ return supportDb.PageNumber; } set{ supportDb.PageNumber = value; } }
	public integer PageSize{ get{ return supportDb.PageSize; } set{ supportDb.PageSize = value; } }
	public boolean IsAllPage{ get{ return supportDb.IsAllPage; } set{ supportDb.IsAllPage = value; } }
	public boolean IsFirstPage{ get{ return supportDb.IsFirstPage; } set{ supportDb.IsFirstPage = value; } }
	public boolean IsLastPage{ get{ return supportDb.IsLastPage; } set{ supportDb.IsLastPage = value; } } 
	
	public void OnChangePage()
	{
		supportDb.OnChangePage();
	}
	
	public void SetRegistrationsByPreviousPage()
	{
		PageNumber--;
		OnChangePage();
	}
	
	public void SetRegistrationsByNextPage()
	{
		PageNumber++;
		OnChangePage();
	}
}