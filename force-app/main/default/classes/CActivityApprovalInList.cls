public with sharing class CActivityApprovalInList 
{
	public RequestForTreatment__c pRequestForTreatment { get; set; }
	public ActivityApproval__c pActivityApproval { get;set; }
	public map<string, TutorLine__c> pTutorLinesMap { get; set; }
	public integer pIndex { get; set; }
	
	public CActivityApprovalInList(ActivityApproval__c mActivityApproval, RequestForTreatment__c mRequestForTreatment, map<string, TutorLine__c> mTutorLinesMap, integer index)
	{
		pRequestForTreatment = mRequestForTreatment;
		pActivityApproval = mActivityApproval;
		pTutorLinesMap = mTutorLinesMap;
		pIndex = index;
	}
	
	public void SetRateByTutor()
	{
		if (pActivityApproval.TutorName__c != null && pActivityApproval.ProfessionName__c != null)
		{
			string s = pActivityApproval.TutorName__c + '-' + pActivityApproval.ProfessionName__c;
			if (pTutorLinesMap.containsKey(s))
				pActivityApproval.Rate__c = pTutorLinesMap.get(s).Rate__c;
		}
	}
}