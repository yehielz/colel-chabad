public with sharing class CChildValidator 
{
	private CChildFacad cChildFacad { get; set; }
	public string ErrorMessage;
	
	public CChildValidator(CChildFacad ExtChildFacad)
	{
		ErrorMessage = '';
		cChildFacad = ExtChildFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	    
	    if (ret) 
	    	ret = ValidateFamilyName();
	    if (ret) 
	    	ret = ValidateChildFirstName();
	    if (ret) 
	    	ret = ValidateChildBirthdate();
    	if (ret) 
	    	ret = ValidateIdNumber();
	  	    
	 	return ret;
	}
	
	private boolean ValidateFamilyName()
	{
		if (cChildFacad.pChild.FamilyName__c == null)
		{
			ErrorMessage = CErrorMessages.ChildFamilyName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateChildFirstName()
	{
		if (cChildFacad.pChild.FirstName__c == null)
		{
			ErrorMessage = CErrorMessages.ChildFirstName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateChildBirthdate()
	{
		if (cChildFacad.pChild.Birthdate__c == null)
		{
			ErrorMessage = CErrorMessages.ChildBirthdate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateIdNumber()
	{
		if (cChildFacad.pChild.IdNumber__c != null)
		{
			if (isIdExist())
				return false;
			integer i = ValidateID(cChildFacad.pChild.IdNumber__c);
			if (i == -1)
			{
				ErrorMessage = CErrorMessages.ChildIdNumberElegalInPut;
				return false;
			}
			else if (i == -2)
			{
				ErrorMessage = CErrorMessages.ChildIdNumberNotValid;
				return false;
			}
		}
		return true;
	}
	
	private boolean isIdExist()
	{
		Children__c[] children = [select name, id, IdNumber__c from Children__c where IdNumber__c = :cChildFacad.pChild.IdNumber__c and id != :cChildFacad.pChild.id];
		if (children.size() > 0)
		{
			ErrorMessage = CErrorMessages.ChildIdNumberExist + CErrorMessages.ChildIdNumberChild + children[0].name + '!';
			return true;
		}
		Family__c[] families = [select name, id, ParentId__c from Family__c where ParentId__c = :cChildFacad.pChild.IdNumber__c];
		if (families.size() > 0)
		{
			ErrorMessage = CErrorMessages.ChildIdNumberExist + CErrorMessages.ChildIdNumberFamily + families[0].name + '!';
			return true;
		}
		return false;
	}
	
    private integer ValidateID(string str)
	{
		// DEFINE RETURN VALUES
		integer R_ELEGAL_INPUT = -1;
		integer R_NOT_VALID = -2;
		integer R_VALID = 1;
		//INPUT VALIDATION

		// Just in case -> convert to string
		string IDnum = str;
		// Validate correct input
		if ((IDnum.length() > 9) || (IDnum.length() < 5))
			return R_ELEGAL_INPUT;
		if (isNaN(IDnum))
			return R_ELEGAL_INPUT;
		// The number is too short - add leading 0000
		if (IDnum.length() < 9)
		{
			while (IDnum.length() < 9)
			{
				IDnum = '0' + IDnum;
			}
		}
		// CHECK THE ID NUMBER
		integer mone = 0, incNum;
		for (integer i=0; i < 9; i++)
		{
			incNum = integer.valueOf(IDnum.substring(i, i+1));
			incNum *= math.mod(i, 2) + 1;
			if (incNum > 9)
				incNum -= 9;
			mone += incNum;
		}
		if (math.mod(mone, 10) == 0)
			return R_VALID;
		else
			return R_NOT_VALID;
	}
	
	private boolean isNaN(string str)
	{
		for (integer i = 0; i < str.length(); i++)
		{
			string s = str.substring(i, i+1);
			if (s != '0' && s != '1' && s != '2' && s != '3' && s != '4' && s != '5' && s != '6' && s != '7' && s != '8' && s != '9')
				return true;
		}
		return false;
	}
	
	
	
	//  **********************   for tests *********************************
	
	public void testAll()
	{
		ValidateChildFirstName();
		ValidateChildBirthdate();
		ValidateIdNumber();
		cChildFacad.pChild.IdNumber__c = '017335770';
		ValidateIdNumber();
		cChildFacad.pChild.IdNumber__c = '2';
		ValidateIdNumber();
	}
}