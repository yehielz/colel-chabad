public with sharing class CErrorMessages 
{
	public static string CaseManagerOrCreator = 'כדי לסגור מקרה עליך להיות מנהל או יוצר המקרה!';
	public static string CaseAssigneeUser = 'כדי להמשיך בשמירה עליך להזין נמען!';
	public static string CaseChild = 'כדי להמשיך בשמירה עליך להזין בן משפחה!';
	public static string CaseFamily = 'כדי להמשיך בשמירה עליך להזין משפחה!';
	public static string CaseOrigin = 'כדי להמשיך בשמירה עליך לבחור מקור מקרה!';
	public static string CaseStatus = 'כדי להמשיך בשמירה עליך לבחור מצב מקרה!';
	public static string CaseType = 'כדי להמשיך בשמירה עליך לבחור סוג מקרה!';
	public static string CasePriority = 'כדי להמשיך בשמירה עליך לבחור עדיפות מקרה!';
	public static string CaseSubject = 'כדי להמשיך בשמירה עליך להזין נושא!';
	public static string CaseDescription = 'כדי להמשיך בשמירה עליך להזין תיאור!';
	public static string CaseComments = 'כדי להמשיך בשמירה עליך להזין הערות פנימיות!';
	public static string CaseDate = 'כדי להמשיך בשמירה עליך להזין תאריך!';
	public static string CaseStartTime = 'כדי להמשיך בשמירה עליך להזין שעת התחלה!';
	public static string CaseStartTimeNotRight = 'שעת ההתחלה שגויה!';
	public static string CaseEndTime = 'כדי להמשיך בשמירה עליך להזין שעת סיום!';
	public static string CaseEndTimeNotRight = 'שעת הסיום שגויה!';
	public static string CaseStartTimeBigFromEndTime = 'שעת התחלה גדולה משעת סיום!';
	public static string CaseInRequestsListHasToBeChild = 'ברשימת בקשות חייב להיות בן משפחה בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseInRequestsListHasChildNotFromFamily = 'ברשימת בקשות היה בן משפחה שלא מהמשפחה הבחורה!';
	public static string CaseRequestsListFromDate = 'כדי להמשיך בשמירה עליך להזין מתאריך בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseRequestsListToDate = 'כדי להמשיך בשמירה עליך להזין עד תאריך בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseRequestsListWeeklyHours = 'כדי להמשיך בשמירה עליך להזין שעות שבועיות בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseRequestsListApprovalAmount = 'כדי להמשיך בשמירה עליך להזין סכום לאישור בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseRequestsListProfessionType = 'כדי להמשיך בשמירה עליך להזין סוג מקצוע בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseRequestsListProfessionName = 'כדי להמשיך בשמירה עליך להזין מקצוע בכל בקשה (שהנך מעוניין לשמור)!';
	public static string CaseRequestsListProfessionWithTutor = 'באחד הבקשות ישנו / היה מקצוע שהחונך אינו מכיל!';
	
	public static string RequestForTreatmentChildName = 'כדי להמשיך בשמירה עליך להזין שם בן משפחה!';
	public static string RequestForTreatmentProfessionWithTutor = 'חונך נוכחי אינו מכיל מקצוע זה!';
	public static string RequestForTreatmentTutor = 'כדי להמשיך בשמירה עליך להזין חונך!';
	public static string RequestForTreatmentWeeklyHours = 'כדי להמשיך בשמירה עליך להזין שעות שבועיות!';
	public static string RequestForTreatmentMeetings = 'כדי להמשיך בשמירה עליך להזין מספר מפגשים!';
	public static string RequestForTreatmentApprovalAmount = 'כדי להמשיך בשמירה עליך להזין סכום הבקשה!';
	public static string RequestForTreatmentFromDate = 'כדי להמשיך בשמירה עליך להזין "מתאריך"!';
	public static string RequestForTreatmentProfessionName = 'כדי להמשיך בשמירה עליך להזין מקצוע!';
	public static string RequestForTreatmentProfessionType = 'כדי להמשיך בשמירה עליך להזין סוג מקצוע!';
	public static string RequestForTreatmentToDate = 'כדי להמשיך בשמירה עליך להזין "עד תאריך"!';
	public static string RequestForTreatmentWorningDataLossPar1 = 'לתשומת ליבך: מקצוע זה הינו סוג מקצוע אחר מהמקצוע הקודם - ';
	public static string RequestForTreatmentWorningDataLossPar2 = ' - ולכן ייתכן וייאבדו נתונים בזמן השמירה באישורי הפעילות של בקשה זו!';
	public static string RequestForTreatmentFromDateSmallThanToDate = 'כדי להמשיך בשמירה "עד תאריך" צריך להיות אחרי "מתאריך"';
	public static string RequestForTreatmentStayReason = 'כדי להמשיך בשמירה עליך להזין סיבת השהיה!';
	
	public static string TutorPhoneOrModilePhone = 'כדי להמשיך בשמירה עליך להזין טלפון או טלפון נייד!';
	public static string TutorProfession = 'כדי להמשיך בשמירה עליך לבחור לפחות מקצוע אחד!';
	public static string TutorFirstName = 'כדי להמשיך בשמירה עליך להזין שם פרטי!';
	public static string TutorCurrentJob = 'כדי להמשיך בשמירה עליך להזין מקום עבודה נוכחי!';
	public static string TutorFieldOfStudy = 'כדי להמשיך בשמירה עליך להזין תחום לימודים!';
	public static string TutorEducationDescription = 'כדי להמשיך בשמירה עליך להזין דרגת ההשכלה!';
	public static string TutorLastName = 'כדי להמשיך בשמירה עליך להזין שם משפחה!';
	public static string TutorCity = 'כדי להמשיך בשמירה עליך להזין עיר!';
	public static string TutorStreet = 'כדי להמשיך בשמירה עליך להזין רחוב!';
	public static string TutorApartmentNumber = 'כדי להמשיך בשמירה עליך להזין מס\' בית!';
	public static string TutorUserNameExist = 'שם משתמש לאתר קיים אצל - ';
	
	public static string DiplomaSchoolYearName = 'כדי להמשיך בשמירה עליך להזין שנת לימודים!';
	public static string DiplomaChildName = 'כדי להמשיך בשמירה עליך להזין שם בן משפחה!';
	public static string DiplomaHalf = 'כדי להמשיך בשמירה עליך לבחור מחצית!';
	public static string DiplomaDate = 'כדי להמשיך בשמירה עליך להזין תאריך!';
	public static string DiplomaLineHasProfessionTwice(string professionName)
	{
		return 'בטבלת המקצועות המקצוע - ' + professionName + ' - קיים פעמיים!';
		
	}
	
	public static string ExamChildRegistrationName = 'כדי להמשיך בשמירה עליך להזין שם רישום שנתי!';
	public static string ExamChildName = 'כדי להמשיך בשמירה עליך להזין שם בן משפחה!';
	public static string ExamGrade = 'כדי להמשיך בשמירה עליך להזין איזשהו ציון!';
	public static string ExamDate = 'כדי להמשיך בשמירה עליך להזין תאריך!';
	public static string ExamProfessionName = 'כדי להמשיך בשמירה עליך להזין שם מקצוע!';
	public static string ExamExamType = 'כדי להמשיך בשמירה עליך להזין סוג מבחן!';
	public static string ExamOnlyOneExamPar1 = 'לבן משפחה ';
	public static string ExamOnlyOneExamPar2 = ' כבר קיים מבחן למקצוע ';
	public static string ExamOnlyOneExamPar3 = ' ותאריך ';
	
	public static string ChildRegistrationExistAlreadyForThisYear = 'לבן משפחה זה כבר קיים רישות שנתי לשנה זו!';
	
	public static string FamilyParentIdElegalInPut = 'ת.ז. אינה חוקית!';
	public static string FamilyParentIdNotValid = 'ת.ז. אינה תקיפה!';
	public static string FamilyParentIdExist = 'ת.ז. קיימת במאגר אצל ה';
	public static string FamilyParentIdChild = 'בן משפחה - ';
	public static string FamilyParentIdFamily = 'משפחה - ';
	public static string FamilyFamilyName = 'כדי להמשיך בשמירה עליך להזין שם משפחה!';
	public static string FamilyCoordinatorName = 'כדי להמשיך בשמירה עליך להזין רכז שטח!';
	public static string FamilyParentName = 'כדי להמשיך בשמירה עליך להזין שם ההורה!';
	
	public static string SupportLestOrFirstName = 'כדי להמשיך בשמירה עליך להזין  "שם פרטי" או "שם משפחה"!';
	
	public static string ChildIdNumberElegalInPut = 'ת.ז. אינה חוקית!';
	public static string ChildIdNumberNotValid = 'ת.ז. אינה תקיפה!';
	public static string ChildIdNumberExist = 'ת.ז. קיימת במאגר אצל ה';
	public static string ChildIdNumberChild = 'בן משפחה - ';
	public static string ChildIdNumberFamily = 'משפחה - ';
	public static string ChildBirthdate = 'כדי להמשיך בשמירה עליך להזין תאריך לידה!';
	public static string ChildFirstName = 'כדי להמשיך בשמירה עליך להזין שם פרטי!';
	public static string ChildFamilyName = 'כדי להמשיך בשמירה עליך להזין שם משפחה!';
	
	public static string SpecialActivitiesDateOrActivityDescription = 'כדי להמשיך בשמירה עליך להזין תאריך או תיאור לפעילות!';
	public static string SpecialActivitiesActivityType = 'כדי להמשיך בשמירה עליך להזין סוג פעילות!';
	
	public static string SpecialActivitiesLineChildOrFamily = 'כדי להמשיך בשמירה עליך להזין "בחר משפחה / בן משפחה"!';
	public static string SpecialActivitiesLineFamilyName = 'כדי להמשיך בשמירה עליך להזין משפחה!';
	public static string SpecialActivitiesLineChildName = 'כדי להמשיך בשמירה עליך להזין בן משפחה!';
	public static string SpecialActivitiesLineSpecialActivitiesName = 'כדי להמשיך בשמירה עליך להזין פעילות מיוחדת!';
	
	public static string ActivityReportOneOfThetimesNotWriteParA = 'השעת התחלה או שעת סיום ביום - ';
	public static string ActivityReportOneOfThetimesNotWriteParB = ' - שגויה!';
	public static string ActivityReportDateInAllFulls = 'עליך להזין תאריך בכל שורה שיש בה פרטים אחרים (אם אינך רוצה לשמור שורה זו רוקן אותה מכל פרטיה)!';
	public static string ActivityReportTimesInAllDaysParA = 'עליך להזין "שעת התחלה" ו"שעת סיום" ביום - ';
	public static string ActivityReportTimesInAllDaysParB = ' - (אם אינך רוצה לשמור יום זה רוקן אותו מכל פרטיו)!';
	public static string ActivityReportHolidayParA = 'התאריך - ';
	public static string ActivityReportHolidayParB = ' - הינו חג - ';
	public static string ActivityReportToActivityApproval = 'לאישור פעילות - ';
	public static string ActivityReportHasReportToTheMonth = ' - כבר יש דיווח לחודש - ';
	public static string ActivityReportExclamationMark = '!';
	public static string ActivityReportChildWasInOtherPlaceParA = 'בן המשפחה - ';
	public static string ActivityReportChildWasInOtherPlaceParB = ' - למד בתאריך - ';
	public static string ActivityReportChildWasInOtherPlaceParC = ' - בשעות - ';
	public static string ActivityReportChildWasInOtherPlaceParD = ' - אצל החונך - ';
	public static string ActivityReportChildWasInOtherPlaceParE = ' - כמפורט בדיווח פעילות מס\' - ';
	public static string ActivityReportStartTimeBigFromEndTimeParA = 'ביום - ';
	public static string ActivityReportStartTimeBigFromEndTimeParB = ' - שעת ההתחלה גדולה משעת הסיום!';
	public static string ActivityReportTutorWasInOtherPlaceParA = 'החונך - ';
	public static string ActivityReportTutorWasInOtherPlaceParB = ' - לימד בתאריך - ';
	public static string ActivityReportTutorWasInOtherPlaceParC = ' - בשעות - ';
	public static string ActivityReportTutorWasInOtherPlaceParD = ' - את בן המשפחה - ';
	public static string ActivityReportTutorWasInOtherPlaceParE = ' - כמפורט בדיווח פעילות מס\' - ';
	public static string ActivityReportFromDateClass = 'כדי להמשיך בשמירה עליך להזין \'מתאריך\'!';
	public static string ActivityReportToDateClass = 'כדי להמשיך בשמירה עליך להזין \'עד תאריך\'!';
	public static string ActivityReportPaymentAmountClass = 'כדי להמשיך בשמירה עליך להזין \'סכום לתשלום\'!';
	public static string ActivityReportFromDateNotBigFromToDate = 'אין אפשרות ש\'מתאריך\' יהיה גדול מ-\'עד תאריך\'!';
	public static string ActivityReportPassedMinutesInSequence = 'סה"כ השעות עברו את סך הדקות ברצף שהוקצבו לאישור פעילות זה!';
	public static string ActivityReportPassed60DaysFromProductionDayParA = 'התאריך - ';
	public static string ActivityReportPassed60DaysFromProductionDayParB = ' - קטן ב-60 יום מתאריך יצירת הדיווח - ';
	public static string ActivityReportReasonForDecline = 'כדי להמשיך בשמירה עליך להזין סיבת דחייה!';
	public static string ActivityReportActivityApprovalName = 'כדי להמשיך בשמירה עליך להזין אישור פעילות!';
	public static string ActivityReportAtLeast10WordsInAllDays = 'אנא מלא תוכן פעילות בן 10 אותיות בכל יום!';
	public static string ActivityReportNeedTalkToPay = 'כדי שהמצב יהיה \'לתשלום\' עליך למלא את "שיחה עם מנהל / מחנך / הורה"!';
	public static string ActivityReportNeedTalkStartTime = 'כאשר ישנה "שיחה עם מנהל /..." עליך למלא את "שעת התחלת שיחה"!';
	public static string ActivityReportNeedTalkEndTime = 'כאשר ישנה "שיחה עם מנהל /..." עליך למלא את "שעת סיום שיחה"!';
	public static string ActivityReportNeedTalkType = 'כאשר ישנה "שיחה עם מנהל /..." עליך למלא את "סוג שיחה"!';
	public static string ActivityReportNeedTalkWithStartTime = 'כאשר ישנה "שעת התחלת שיחה" עליך למלא את "שיחה עם מנהל /..."!';
	public static string ActivityReportNeedTalkWithEndTime = 'כאשר ישנה "שעת סיום שיחה" עליך למלא את "שיחה עם מנהל /..."!';
	public static string ActivityReportTalkEndTimeNotRight = 'שעת סיום שיחה אינה תקינה!';
	public static string ActivityReportTalkStartTimeNotRight = 'שעת התחלת שיחה אינה תקינה!';
	public static string ActivityReportTalkStartTimeBigFromTalkEndTime = 'שעת סיום שיחה גדולה משעת התחלת שיחה!';
	public static string ActivityReportReportMonth = 'כדי להמשיך בשמירה עליך להזין חודש דיווח!';
	public static string ActivityReportLineNotInMonthReport = ' - אינו מחודש הדיווח - ';
	
	public static string PaymentFamilyName = 'כדי להמשיך בשמירה עליך להזין משפחה!';
	public static string PaymentTutorName = 'כדי להמשיך בשמירה עליך להזין חונך!';
	public static string PaymentDoubleInList = ' - נמצא כפול ברשימה!';
	public static string PaymentTheCheckNumber = 'מספר השיק - ';
	public static string PaymentMustBeNumbers = ' - חייב להיות מספרים בלבד!';
	public static string PaymentCheckAlreadyExistInSystem = ' - כבר קיים במערכת!';
	public static string PaymentDate = 'כדי להמשיך בשמירה עליך להזין תאריך!';
	public static string PaymentAmountNotLikeDebt = 'אין אפשרות לשלם שיקים אם הסכום אינו כמו החוב!';
	public static string PaymentCheckNotContainsAmountPar1 = 'שיק מספר - ';
	public static string PaymentCheckNotContainsAmountPar2 = ' - אינו מכיל סכום (אם הינך רוצה למחוק שיק זה עליך לרוקן אותו ממס\' שיק ומסכום)!';
	public static string PaymentCheckNotContainsName = 'ישנו שיק ברשימה שמכיל סכום ללא מס\' שיק (אם הינך רוצה למחוק שיק זה מהרשימה עליך לרוקן אותו ממס\' השיק והסכום)!';
	public static string PaymentCheckBranchNumber = 'כדי להמשיך בשמירה עליך להזין מספר סניף בשיק מספר - ';
	public static string PaymentCheckAccountNumber = 'כדי להמשיך בשמירה עליך להזין מספר חשבון בשיק מספר - ';
	public static string PaymentCheckDepositDate = 'כדי להמשיך בשמירה עליך להזין תאריך פירעון בשיק מספר - ';
	public static string PaymentExclamationMark = ' - (אם הינך רוצה למחוק שיק זה מהרשימה עליך לרוקן אותו ממס\' השיק והסכום)!';
	
	public static string ActivityApprovalIsInTheSameDates = 'לבקשה זו כבר יש אישור פעילות בין תאריכים אלו!';
	public static string ActivityApprovalFromDateSmallThanToDate = 'כדי להמשיך בשמירה "עד תאריך" צריך להיות אחרי "מתאריך"';
	
	public static string TaskReminderDateNotValid = 'תאריך התזכורת אינו תקף!';
	
	public static string ActiityApprovalRate = 'כדי להמשיך בשמירה עליך להזין תעריף!';
}