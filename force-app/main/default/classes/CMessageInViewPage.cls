public with sharing class CMessageInViewPage 
{
	public Message__c pMessage { get; set; }
	public string messageLink
	{
		get
		{
			if (pMessage == null || pMessage.link__c == null)
				return null;
			if (pMessage.link__c.contains('force.com'))
				return pMessage.link__c.split('force.com')[1];
			else
				return CObjectNames.getBaseUrl() + pMessage.link__c;
		}
	}
	
	public CMessageInViewPage(Message__c message)
	{
		pMessage = message;
	}
}