public with sharing class CDayInActivityReportToSite 
{
	public string startTimeMinutes { get; set; }
	public string startTimeHours { get; set; }
	public string endTimeMinutes { get; set; }
	public string endTimeHours { get; set; }
	public DayInActivityReport__c pDayInActivityReport { get; set; }
	public ActivityApproval__c pReportApproval { get;set; }
	public boolean IsHasPriceOnTravel
	{
		get
		{
			if (pDayInActivityReport != null && pDayInActivityReport.PriceOnTravel__c != null && pDayInActivityReport.PriceOnTravel__c > 0)
				return true;
			else
				return false;
		}
		set
		{
			if (value)	
				pDayInActivityReport.PriceOnTravel__c = pReportApproval.Fare__c;
			else
				pDayInActivityReport.PriceOnTravel__c = 0;
		}
	}
	
	public CDayInActivityReportToSite(DayInActivityReport__c dayInActivityReport, ActivityApproval__c approval)
	{
		pDayInActivityReport = dayInActivityReport;
		pReportApproval = approval;
	}
	
	public void setTimes()
	{
		pDayInActivityReport.StartTime__c = startTimeHours + ':' + startTimeMinutes;
		pDayInActivityReport.EndTime__c = endTimeHours + ':' + endTimeMinutes;
	}
}