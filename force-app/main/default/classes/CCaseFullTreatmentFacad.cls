public with sharing class CCaseFullTreatmentFacad 
{
	private CCaseFullTreatmentDb caseFullTreatmentDb;
	
	public Case__c pCase{ get{ return caseFullTreatmentDb.pCase; } }
	public list<CRequestForTreatmentInList> pRequestForTreatments{ get{ return caseFullTreatmentDb.pRequestForTreatments; } }
	
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	public integer pRequestIndex { get; set; }
	public integer pApprovalIndex { get; set; }
	
	public CCaseFullTreatmentFacad()
	{
		pRequestIndex = 0;
		caseFullTreatmentDb = new CCaseFullTreatmentDb();
		if (pCase.id != null)
			pageTitle = pCase.name;
		else
			pageTitle = 'עדכון חדש';
	}
	
	public PageReference save()
	{
		caseFullTreatmentDb.save();
		PageReference caseViewPage = new ApexPages.StandardController(pCase).view();
        return caseViewPage;
	}
	
	public PageReference cancel()
	{
		PageReference caseViewPage = new ApexPages.StandardController(pCase).view();
        return caseViewPage;
	}
	
	public boolean bFamily{ get{ return pCase.WhoId__c == null || pCase.WhoId__c == CObjectNames.Family; } }
	public boolean bChild{ get{ return pCase.WhoId__c != null && pCase.WhoId__c == CObjectNames.FamilyMember; } }
	
	public void AddActivityApproval()
	{
		pRequestForTreatments[pRequestIndex].AddNewActivityApproval();
	}
	
	public void AddNote()
	{
		pRequestForTreatments[pRequestIndex].AddNewNote();
	}
	
	public void DeleteApproval()
	{
		pRequestForTreatments[pRequestIndex].DeleteApproval(pApprovalIndex);
	}
	
	public void refresh()
	{
		
	}
}