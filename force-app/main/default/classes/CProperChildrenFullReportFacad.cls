public with sharing class CProperChildrenFullReportFacad 
{
	public set<string> pChildrenIds { get; private set; }
	public boolean IsCasesInView{ get; set; }
	public boolean IsRequestInView{ get; set; }
	public boolean IsApprovalsInView{ get; set; }
	public boolean IsReportsInView{ get; set; }
	public boolean IsDiplomasInView{ get; set; }
	public boolean IsExamsInView{ get; set; }
	public boolean IsChildProgressInView{ get; set; }
	public list<selectOption> choosen { get; set; }
	public list<selectOption> available { get; set; }
	
	public CProperChildrenFullReportFacad()
	{
		SetChildrenIds();
		SetOptions();
		IsCasesInView = true;
		IsRequestInView = true;
		IsApprovalsInView = true;
		IsReportsInView = true;
		IsDiplomasInView = true;
		IsExamsInView = true;
		IsChildProgressInView = true;
	}
	
	private void SetChildrenIds()
	{
		pChildrenIds = new set<string>();
		string children = Apexpages.currentPage().getParameters().get('children');
		if (children != null && children.trim() != '')
		{
			try
			{
				list<string> childrenIds = children.split('-');
				for (string cid : childrenIds)
				{
					pChildrenIds.add(cid);
				}
			}
			catch(Exception e)
			{
				pChildrenIds = new set<string>();
			}
		}
	}
	/*old version
	private void SetOptions(){
		
		choosen = new list<selectoption>();
		available = new list<selectoption>();
		datetime tday = datetime.now();
		string tdayYear = date.newInstance(tday.year(), tday.month(), tday.day()) < date.newInstance(tday.year(), 9, 1) ? ((tday.year() - 1) + '-' + tday.year()) : (tday.year() + '-' + (tday.year() + 1));
		set<string> yearsSet = new set<string>();
		for (SchoolYear__c schoolYear : [select name, id, StartDate__c from SchoolYear__c where StartDate__c != null order by StartDate__c])
		{
			string year = schoolYear.StartDate__c < date.newInstance(schoolYear.StartDate__c.year(), 9, 1) ? ((schoolYear.StartDate__c.year() - 1) + '-' + schoolYear.StartDate__c.year()) : 
							(schoolYear.StartDate__c.year() + '-' + (schoolYear.StartDate__c.year() + 1));
			if (!yearsSet.contains(year))
			{
				if (year == tdayYear)
					choosen.add(new selectoption(tdayYear, tdayYear));
				else
					available.add(new selectoption(year, year));
			}
			yearsSet.add(year);
		}
	}
	*/
	private void SetOptions(){
		choosen = new list<selectoption>();
		available = new list<selectoption>();
		set<string> yearsSet = new set<string>();
		for (SchoolYear__c schoolYear : [select name, id, StartDate__c,IsActive__c from SchoolYear__c where StartDate__c != null order by StartDate__c]){
			string year = schoolYear.StartDate__c.year() + '-' + (schoolYear.StartDate__c.year() + 1);
			if (!yearsSet.contains(year)){
				if(schoolYear.Isactive__c)
					choosen.add(new selectoption(year, year));
				else
					available.add(new selectoption(year, year));
			}
			yearsSet.add(year);
		}
	}
	
	public PageReference BackToChildrenTab()
	{
		return new PageReference(CObjectNames.getBaseUrl() + '/' + Children__c.sObjectType.getDescribe().getKeyPrefix());
	}
	public PageReference ContinueToPrint()
	{
		return new PageReference(CObjectNames.getBaseUrl() + page.ChildrenFullReport.getUrl() + '?children=' + GetChildrenStr() + '&view=' + GetToView() + '&years=' + GetYearsToView());
	}
	
	private string GetChildrenStr()
	{
		string ret = '';
		for (string cid : pChildrenIds)
		{
			ret += ret == '' ? cid : '-' + cid;
		}
		return ret;
	}
	
	private string GetToView()
	{
		if (IsCasesInView && IsRequestInView && IsApprovalsInView && IsReportsInView && IsDiplomasInView && IsExamsInView && IsChildProgressInView)
			return 'all';
		else
		{
			string ret = '';
			if (IsCasesInView)
				ret += 'case';
			if (IsRequestInView)
				ret += ret == '' ? 'request' : '-request';
			if (IsApprovalsInView)
				ret += ret == '' ? 'approval' : '-approval';
			if (IsReportsInView)
				ret += ret == '' ? 'report' : '-report';
			if (IsDiplomasInView)
				ret += ret == '' ? 'diploma' : '-diploma';
			if (IsExamsInView)
				ret += ret == '' ? 'exam' : '-exam';
			if (IsChildProgressInView)
				ret += ret == '' ? 'progress' : '-progress';
			ret += ret == '' ? 'none' : '';
			return ret;
		}
	}
	
	private string GetYearsToView()
	{
		if (choosen.size() < 1)
			return 'none';
		else if (available.size() < 1)
			return 'all';
		else
		{
			string ret = '';
			for (selectOption ch : choosen)
			{
				ret += ret == '' ? ch.getValue() : ';' + ch.getValue();
			}
			return ret;
		}
	}
}