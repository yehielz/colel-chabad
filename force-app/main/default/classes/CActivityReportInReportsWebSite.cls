public with sharing class CActivityReportInReportsWebSite 	
{
	public ActivityReport__c pActivityReport { get; set; }
	
	public CActivityReportInReportsWebSite(ActivityReport__c activityReport)
	{
		pActivityReport = activityReport;
	}
	
	public string reportPageUrl{ get{ return CObjectNames.WebSitePageReportDisplay + '&AR=' + pActivityReport.id; } }
}