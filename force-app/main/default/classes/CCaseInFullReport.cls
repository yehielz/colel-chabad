public with sharing class CCaseInFullReport 
{
	public Case__c pCase { get; set; }
	public list<CRequestForTreatmentInFullReport> pRequestForTreatments { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsCasesInView{ get { return pView.containsKey('all') || pView.containsKey('case'); } }
	
	
	public CCaseInFullReport(Case__c mCase, map<string, boolean> view)
	{
		pCase = mCase;
		pRequestForTreatments = new list<CRequestForTreatmentInFullReport>();
		pView = view;
	}
	
	public CCaseInFullReport(Case__c mCase, CRequestForTreatmentInFullReport mRequestForTreatment, map<string, boolean> view)
	{
		pCase = mCase;
		pRequestForTreatments = new list<CRequestForTreatmentInFullReport>();
		if (mRequestForTreatment != null)
		{
			pRequestForTreatments.add(mRequestForTreatment);
		}
		pView = view;
	}
	
	public CCaseInFullReport(Case__c mCase, RequestForTreatment__c mRequestForTreatment, map<string, boolean> view)
	{
		pCase = mCase;
		pView = view;
		pRequestForTreatments = new list<CRequestForTreatmentInFullReport>();
		if (mRequestForTreatment != null)
		{
			pRequestForTreatments.add(new CRequestForTreatmentInFullReport(mRequestForTreatment, pView));
		}
	}
	
	public CCaseInFullReport(Case__c mCase, list<RequestForTreatment__c> mRequestForTreatments, map<string, boolean> view)
	{
		pCase = mCase;
		pRequestForTreatments = new list<CRequestForTreatmentInFullReport>();
		if (mRequestForTreatments != null)
		{
			for (RequestForTreatment__c request : mRequestForTreatments)
			{
				pRequestForTreatments.add(new CRequestForTreatmentInFullReport(request, view));
			}
		}
		pView = view;
	}
	
	public void AddRequest(CRequestForTreatmentInFullReport request)
	{
		pRequestForTreatments.add(request);
	}
	
	public string pCaseName{ get{ return CObjectNames.getHebrewString(pCase.name); } }
	public string pCaseSubject{ get{ return CObjectNames.getHebrewString(pCase.Subject__c); } }
}