public without sharing class CWebSiteHomePageFacad 
{
    
    private Tutor__c tutor { get; set; }
    public Tutor__c pTutor 
    { 
        get
        {
            if (tutor == null)
            {
                if (ApexPages.currentPage().getCookies().get('sessionId') != null)
                {
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null)
                        tutor = CTutorDb.GetTutorById(sessionId.TutorName__c);
                }
            }
            return tutor;
        } 
    }
    
    private Family__c family { get; set; }
    public Family__c pfamily
    { 
        get
        {
            if (family == null)
            {
                if (ApexPages.currentPage().getCookies().get('sessionId') != null)
                {
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null)
                        family = CFamilyDb.GetFamilyById(sessionId.Family__c);
                }
            }
            return family;
        } 
    }
    //private CWebSiteMonthesDb webSiteMonthesDb;
    
    public CWebSiteHomePageFacad()
    {
        //ebSiteMonthesDb = new CWebSiteMonthesDb();
    }
    
    public PageReference ValidateIsOnline()
    {
        if (ApexPages.currentPage().getCookies().get('sessionId') != null)
        {
            SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
            if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
                return null;
        }
        return new PageReference(CObjectNames.WebSitePageLogIn);
    }
    
    //links
    
    
    public string homeLink{ get{ return CObjectNames.WebSitePageHome; } }
    public string monthesLink{ get{ return CObjectNames.WebSitePageMonthes; } }
    public string reportsDisplayLink{ get{ return CObjectNames.WebSitePageReportsDisplay; } }
    
    public Pagereference updateFamily(){
        update family;
        return null;
    }
    
    public Pagereference updateTutor(){
        update tutor;
        return null;
    }
    
    public PageReference LogOut()
    {
        Cookie sessionIdCookie = new Cookie('sessionId', null, null, -1, false);
        ApexPages.currentPage().setCookies(new Cookie[] { sessionIdCookie } );
        CSessionIdDb.DeleteSessionsIdWithLastTime();
        return new PageReference(CObjectNames.WebSitePageLogIn);
    }
}