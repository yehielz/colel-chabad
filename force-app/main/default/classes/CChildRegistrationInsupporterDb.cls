public with sharing class CChildRegistrationInsupporterDb
{
	public Support__c pSupport { get; set; }
	public list<CChildRegistrationInsupporter> pChildRegistrationInSupporter { get; set; }
	
	public CChildRegistrationInsupporterDb(Support__c extSupporter)
	{
		SetPageSize();
		SetPageNumber();
		pSupport = extSupporter;
		FillIChildRegistrationInSupporter();
		SetRegistrationsByPaging();
	}
        
    private void FillIChildRegistrationInSupporter()
	{
		pChildRegistrationInSupporter = new list<CChildRegistrationInsupporter>();
		for (ChildRegistration__c line : pChildRegistrations)
		{
			ChildRegistrationInSupporter__c tChildRegistrationInSupporter = new ChildRegistrationInSupporter__c();
			tChildRegistrationInSupporter.ChildRegistrationName__c = line.id;
			if (pExistChildRegistrationInSupportersMap.containsKey(line.id)) 
				tChildRegistrationInSupporter = pExistChildRegistrationInSupportersMap.get(line.id);
			pChildRegistrationInSupporter.add(new CChildRegistrationInsupporter(tChildRegistrationInSupporter, line));
		}
		existChildRegistrationInSupporter = null;
		ExistChildRegistrationInSupportersMap = null;
		childRegistrations = null;
	}
	
	public void SaveLines()
	{
		list<ChildRegistrationInSupporter__c> toInsert = new list<ChildRegistrationInSupporter__c>();
		list<ChildRegistrationInSupporter__c> toUpdate = new list<ChildRegistrationInSupporter__c>();
		list<ChildRegistrationInSupporter__c> toDelete = new list<ChildRegistrationInSupporter__c>();
		for (CChildRegistrationInsupporter line : pChildRegistrationInSupporter)
		{
			if (line.pChildRegistrationInSupporter.id == null && line.IsForSave)
			{
				line.pChildRegistrationInSupporter.SupporterName__c = pSupport.id;
            	toInsert.add(line.pChildRegistrationInSupporter);
			}
			else if (line.pChildRegistrationInSupporter.id != null && line.IsForSave)
            	toUpdate.add(line.pChildRegistrationInSupporter);
			else if (line.pChildRegistrationInSupporter.id != null)
            	toDelete.add(line.pChildRegistrationInSupporter);
		}
		if (toInsert.size() > 0)
			insert toInsert;
		if (toUpdate.size() > 0)
			update toUpdate;
		if (toDelete.size() > 0)
			delete toDelete;
	}
	
	private list<ChildRegistrationInSupporter__c> existChildRegistrationInSupporter;
	public list<ChildRegistrationInSupporter__c> pexistChildRegistrationInSupporters
	{
		get
		{
			if (existChildRegistrationInSupporter == null)
				existChildRegistrationInSupporter = [select name, id, ChildRegistrationName__c from ChildRegistrationInSupporter__c where SupporterName__c = :pSupport.Id];
			return existChildRegistrationInSupporter;
		}
	}
	
	private map<string, ChildRegistrationInSupporter__c> ExistChildRegistrationInSupportersMap;
	public map<string, ChildRegistrationInSupporter__c> pExistChildRegistrationInSupportersMap
	{
		get
		{
			if (ExistChildRegistrationInSupportersMap == null)
			{
				ExistChildRegistrationInSupportersMap = new map<string, ChildRegistrationInSupporter__c>();
				for (ChildRegistrationInSupporter__c line : pexistChildRegistrationInSupporters)
				{
					ExistChildRegistrationInSupportersMap.put(line.ChildRegistrationName__c, line);
				}
			}
			return ExistChildRegistrationInSupportersMap;
		}
	}
	
	private list<ChildRegistration__c> childRegistrations;
	public list<ChildRegistration__c> pChildRegistrations
	{
		get
		{
			if (childRegistrations == null)
				childRegistrations = [select name, id, SchoolName__c, School__c, Class__c, SchoolYearName__c, ChildName__c, ChildName__r.CoordinatorUserName__c,ChildName__r.OwnerId
										 from ChildRegistration__c where SchoolYearName__r.IsActive__c = true order by ChildName__r.name];
			return childRegistrations;
		}
	}
	
	public integer PageNumber { get;set; }
	public integer PageSize { get;set; }
	public boolean IsAllPage { get;set; }
	public boolean IsFirstPage { get;set; }
	public boolean IsLastPage { get;set; }
	
	private void SetPageSize()
	{
		IsAllPage = false;
		string size = null;
		PageSize = 50;
		if (Apexpages.currentPage().getCookies().containsKey('PSize') != null && Apexpages.currentPage().getCookies().get('PSize') != null)
		{
			size = Apexpages.currentPage().getCookies().get('PSize').getValue();
			if (size != null && size.trim() != '')
			{
				if (size == 'all')
					IsAllPage = true;
				try { PageSize = integer.valueOf(size);  }
				catch(Exception e){ PageSize = 50; }
			}
		}
		size = Apexpages.currentPage().getParameters().get('PSize');
		if (size != null && size.trim() != '')
		{
			if (size == 'all')
				IsAllPage = true;
			try { PageSize = integer.valueOf(size); }
			catch(Exception e){ PageSize = 50; }
		}
	}
	
	private void SetPageNumber()
	{
		string num = null;
		if (Apexpages.currentPage().getCookies().containsKey('PNum') != null && Apexpages.currentPage().getCookies().get('PNum') != null)
		{
			num = Apexpages.currentPage().getCookies().get('PNum').getValue();
			if (num != null && num.trim() != '')
			{
				try { PageNumber = integer.valueOf(num);  }
				catch(Exception e){ PageNumber = 1; }
			}
		}
		num = Apexpages.currentPage().getParameters().get('PNum');
		if (num != null && num.trim() != '')
		{
			try { PageNumber = integer.valueOf(num); }
			catch(Exception e){ PageNumber = 1; }
		}
		IsFirstPage = false;
		IsLastPage = false;
	}
	
	private void SetRegistrationsByPaging()
	{
		IsFirstPage = false;
		IsLastPage = false;
		PageNumber = PageNumber == null || PageNumber < 1 ? 1 : PageNumber;
		PageSize = PageSize == null || PageSize < 10 ? 10 : PageSize;
		list<CChildRegistrationInsupporter> listToRun = pChildRegistrationInSupporter;
		pChildRegistrationInSupporter = new list<CChildRegistrationInsupporter>();
		PageSize = IsAllPage ? listToRun.size() : PageSize;
		PageNumber = IsAllPage ? 1 : PageNumber;
		integer startNumber = (PageNumber * PageSize) - PageSize;
		while (startNumber >= listToRun.size())
		{
			PageNumber--;
			startNumber = (PageNumber * PageSize) - PageSize;
		}
		startNumber = startNumber < 0 ? 0 : startNumber;
		integer endNumber = startNumber + PageSize;
		if (endNumber >= listToRun.size())
		{
			endNumber = listToRun.size(); 
			IsLastPage = true;
		}	
		for (integer i = startNumber; i < endNumber; i++)
		{
			if (i < listToRun.size())
				pChildRegistrationInSupporter.add(listToRun[i]);
		}
		PageNumber = PageNumber < 1 ? 1 : PageNumber;
		if (PageNumber == 1)
			IsFirstPage = true;
		IsAllPage = false;
		SetCookies();
	}
	
	private void SetCookies()
	{
		Cookie PSizeCookie = new Cookie('PSize', string.valueOf(PageSize), null, -1, false);
    	Cookie PNumCookie = new Cookie('PNum', string.valueOf(PageNumber), null, -1, false);
    	ApexPages.currentPage().setCookies(new list<Cookie> { PSizeCookie, PNumCookie } );
	}
	
	public void OnChangePage()
	{
		SaveLines();
		FillIChildRegistrationInSupporter();
		SetRegistrationsByPaging();
	}
}