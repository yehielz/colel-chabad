public without sharing class CWebSiteReportDisplayFacad 
{
	
	public List<CustomAttachment> attachments {get;set;}
	public Document newFile {get; set;}	
	private Tutor__c tutor { get; set; }
	public Tutor__c pTutor { 
		get{
			if (tutor == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null)
						tutor = CTutorDb.GetTutorById(sessionId.TutorName__c);
				}
			}
			return tutor;
		} 
	}
	
	private Family__c family { get; set; }
    public Family__c pfamily { 
        get{
            if (family == null) {
                if (ApexPages.currentPage().getCookies().get('sessionId') != null){
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null)
                        family = CFamilyDb.GetFamilyById(sessionId.Family__c);
                }
            }
            return family;
        } 
    }
    
    private ActivityApproval__c approval { get; set; }
	public ActivityApproval__c pApproval{ 
		get{
			if (approval == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null)
						approval = CActivityApprovalDb.GetActivityApprovalById(sessionId.ActivityApproval__c);
				}
			}
			return approval;
		} 
	}
	
	
	private CWebSiteReportDisplayDb webSiteReportDisplayDb;
	
	public CWebSiteReportDisplayFacad() {
		webSiteReportDisplayDb = new CWebSiteReportDisplayDb();
		String approvalId = ApexPages.currentPage().getParameters().get('AR');
		List<Attachment> stdAttachments = [select Id, Name, CreatedDate From Attachment where ParentId = :approvalId and CreatedById = :UserInfo.getUserId() order by createdDate Desc];
		attachments = new List<CustomAttachment>();
		for(Attachment att : stdAttachments){
			attachments.add(new CustomAttachment (att,att.Id));
		}
		newFile = new Document();
	}
	
	public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
			{
				if (ApexPages.currentPage().getParameters().get('AR') != null)
				{
					if (CActivityReportDb.GetActivityReportById(ApexPages.currentPage().getParameters().get('AR')) != null){
						return null;
					}
				}
			}
		}
		return new PageReference(CObjectNames.WebSitePageReportsDisplay);
	}
	
	public ActivityReport__c pActivityReport
	{
		get
		{
			return webSiteReportDisplayDb.GetpActivityReport();
		}
	}
	public list<DayInActivityReport__c> pDaysInActivityReport
	{
		get
		{
			return webSiteReportDisplayDb.GetpDaysInActivityReport();
		}
	}
	
	public string childName
	{
		get
		{
			if (pActivityReport != null && pActivityReport.ChildName__c != null)
			{
				Children__c child = CChildDb.GetChildById(pActivityReport.ChildName__c);
				if (child != null)
					return child.name;
			}
			return '';
		}
	}
	public string professionName
	{
		get
		{
			if (pActivityReport != null && pActivityReport.ProfessionName__c != null)
			{
				Profession__c[] profession = [select name, id from Profession__c where id = :pActivityReport.ProfessionName__c];
				if (profession.size() > 0)
					return profession[0].name;
			}
			return '';
		}
	}
	
	public string IsSendChack
	{
		get
		{
			if (pActivityReport != null && pActivityReport.PaymentName__c != null)
			{
				Payment__c[] payment = [select name, id, IsDone__c, Status__c, Date__c from Payment__c where id = :pActivityReport.PaymentName__c];
				if (payment.size() > 0 && payment[0].IsDone__c)
					return 'כן';
			}
			return 'לא';
		}
	}
	
	public string PaymentDate
	{
		get
		{
			if (pActivityReport != null && pActivityReport.PaymentName__c != null)
			{
				Payment__c[] payment = [select name, id, IsDone__c, Status__c, Date__c from Payment__c where id = :pActivityReport.PaymentName__c];
				if (payment.size() > 0 && payment[0].IsDone__c)
				{
					return payment[0].Date__c.day() + ' / ' + payment[0].Date__c.month() + ' / ' + payment[0].Date__c.year();
				}
			}
			return '';
		}
	}
	
	//links
	
	
	public string homeLink 
	{
		get
		{
			return CObjectNames.WebSitePageHome;
		}
	}
	
	public string monthesLink 
	{
		get
		{
			return CObjectNames.WebSitePageMonthes;
		}
	}
	
	public string reportsDisplayLink 
	{
		get
		{
			return CObjectNames.WebSitePageReportsDisplay;
		}
	}
	
	public PageReference LogOut()
	{
		Cookie sessionIdCookie = new Cookie('sessionId', null, null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { sessionIdCookie } );
		CSessionIdDb.DeleteSessionsIdWithLastTime();
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
	
	public class CustomAttachment {
		public Attachment file {get;set;}
		public String recordId {get;set;}
		
		public CustomAttachment (Attachment file, String recordId){
			this.recordId = recordId;
			this.file = file;
		}
		
	}
}