public with sharing class CPaymentInBord 
{
	public Payment__c pPayment { get; set; }
	public list<Check__c> pChecks { get; set; }
	public list<ActivityReport__c> pActivityReports { get; set; }
	public list<SpecialActivitiesLine__c> pSpecialActivitiesLine { get; set; }
	public boolean IsHasChecks { get{ return pChecks.size() > 0; } }
	
	public CPaymentInBord(Payment__c mPayment, list<Check__c> mChecks, list<ActivityReport__c> mActivityReports, list<SpecialActivitiesLine__c> specialActivitiesLine)
	{
		pPayment = mPayment;
		pChecks = mChecks;
		pActivityReports = mActivityReports;
		pSpecialActivitiesLine = specialActivitiesLine;
	}
	
	public string sReportsProfessions
	{
		get
		{
			if ((pActivityReports != null && pActivityReports.size() > 0) ||(pSpecialActivitiesLine != null && pSpecialActivitiesLine.size() > 0))
			{
				string ret = '';
				for (ActivityReport__c report : pActivityReports)
				{
					string a = '<a href="/' + report.ProfessionName__c + '">' + report.ProfessionName__r.name + '</a>';
					ret += ret == '' ? a : ', ' + a;
				}
				for (SpecialActivitiesLine__c line : pSpecialActivitiesLine)
				{
					string a = '<a href="/' + line.SpecialActivitiesName__c + '"> ' + line.SpecialActivitiesName__r.ActivityDescription__c + '</a>';
					ret += ret == '' ? a : ', ' + a;
				}
				return ret;
			}
			return '';
		}
	}
}