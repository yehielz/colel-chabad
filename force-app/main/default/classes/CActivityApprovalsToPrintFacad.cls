public with sharing class CActivityApprovalsToPrintFacad 
{
    private list<CActivityApprovalInPrintPage> pActivityApprovals { get; set; }
    public Case__c pCase { get; set; }
    public string activityApprovalsHtmlToView 
    { 
        get
        {
            return GetActivityApprovalsHtml();
        }
    }
    public string todayDate 
    { 
        get
        {
            return CObjectNames.getHebrewString('תאריך: ' + Datetime.now().day() + ' / ' + datetime.now().month() + ' / ' + datetime.now().year());
        }
    }
    public string meetingDate { get; set; }
    public string noteToPrintPage 
    { 
        get
        { 
            return GetNote(pCase.NoteToPrintPage__c);
        } 
    }
    
    private string GetNote(string thenote)
    {
        string ret = '';
        if (thenote != null)
        {
            string note = ' ' + thenote;
            integer noteLength = note.length();
            for (integer i = 77; i < note.length(); i=85)
            {
                integer fromInSubS = i < 85 ? i: 85;
                integer minus = 0;
                while(note.substring(i-minus, ((i-minus)+1)) != ' ')
                    minus++;
                i = i - minus;
                ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
                if (fromInSubS == 77)
                    ret += ' ' + CObjectNames.getHebrewString('הערות: ');
                ret += '<br/>';
                note = note.substring(i, note.length());
            }
            ret += '.' + CObjectNames.getHebrewString(note);
            if (noteLength < 78)
                ret += ' ' + CObjectNames.getHebrewString('הערות: ');
        }
        return ret;
    }
    
    public string familyName 
    { 
        get
        {
            if (pCase.FamilyName__c != null)
                return CObjectNames.getHebrewString('משפחת ' + pCase.FamilyName__r.name);
            else
                return CObjectNames.getHebrewString('משפחת ' + pCase.ChildName__r.FamilyName__r.name);
        }
    }
    
    public string city
    { 
        get
        {
            return CObjectNames.getHebrewString('משפחת ' + pCase.ChildName__r.FamilyName__r.city__c);
        }
    }
    
    public CActivityApprovalsToPrintFacad()
    {
        SetpCase();
        SetpActivityApprovals();
    }
    
    private void SetpCase()
    {
        pCase = new Case__c();
        if (Apexpages.currentPage().getParameters().get('caseID') != null)
        {
            Case__c[] cases = [select Comments__c, AssigneeUser__c, FamilyName__r.name, Origin__c, Subject__c, Type__c, Priority__c, 
                                      name, WhoId__c, id,  EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, ChildName__c, 
                                      CoordinatorUserName__c, OwnerName__c, FamilyName__c, Description__c, SendEmail__c, NoteToPrintPage__c,
                                      TotalHoursToCalculate__c, AssigneeUsersList__c, SendEmailToManager__c, ChildName__r.FamilyName__r.name,
                                      ChildName__r.FamilyName__r.City__c,
                                      Date__c, Status__c from Case__c where id = :Apexpages.currentPage().getParameters().get('caseID')];
            if (cases.size() > 0)
                pCase = cases[0];
        }
    }
    
    private void SetpActivityApprovals()
    {
        pActivityApprovals = new list<CActivityApprovalInPrintPage>();
        if (pCase.id != null)
        {
            ActivityApproval__c[] activityApprovals = CActivityApprovalDb.GetActivityApprovalByCaseId(pCase.id);
            if (activityApprovals.size() > 0)
                meetingDate = activityApprovals[0].CreatedDate.day() + '/' + activityApprovals[0].CreatedDate.month() + '/' + activityApprovals[0].CreatedDate.year();
            for (integer i = 0; i < activityApprovals.size(); i++)
            {
                pActivityApprovals.add(new CActivityApprovalInPrintPage(activityApprovals[i]));
            }
        }
    }
    
    private string GetActivityApprovalsHtml()
    {
        string ret = '';
        for (integer i = 0; i < pActivityApprovals.size(); i++)
        {
            ret += GetHtmlForActivityApproval(pActivityApprovals[i], i+1);
        }
        return ret;
    }
    
    private string GetHtmlForActivityApproval(CActivityApprovalInPrintPage activityApproval, integer num)
    {
        string ret = '<div class="activityApprovalDiv">' + 
                            '<p>' +
                                '.' + activityApproval.hoursInSequence + activityApproval.totalScopeOrAmount + activityApproval.ScopeOrAmount + ','
                                 + activityApproval.childName +CObjectNames.getHebrewString('עבור ') + activityApproval.professionName + ' .' + num +
                            '</p>' +
                            '<p>' +
                                activityApproval.RateForHour + ' ' + CObjectNames.getHebrewString('תעריף ותנאים לפעילות זו:') + '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '</p>' +
                            '<p>' +
                                '.' + activityApproval.activityToDate + ' ' + CObjectNames.getHebrewString('עד ל:') + ' ' + 
                                activityApproval.activityFromDate + ' ' + CObjectNames.getHebrewString('הפעילות מאושרת - מ:') + 
                                '&nbsp;&nbsp;&nbsp;&nbsp;' +
                            '</p>' +
                        '</div>';
        return ret;
    }
}