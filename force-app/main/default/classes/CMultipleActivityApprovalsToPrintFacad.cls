public with sharing class CMultipleActivityApprovalsToPrintFacad
{
    public list<CActivityApprovalToPrintFacad> pApprovals { get; set; }
    public list<Family__c> families { get; set; }
    public Map<String,List<CActivityApprovalToPrintFacad>> approvalsMap { get; set; }
    
    public CMultipleActivityApprovalsToPrintFacad()    {
    	if(Apexpages.currentPage().getParameters().get('familiesView') != null){
    		buildFamiliesMap();
    	}
    	else{
        	list<ActivityApproval__c> approvals = GetActivityApprovalsByIds();
        	SetApprovalsFacads(approvals);
    	}
    }
    
    public void UpdateApprovals()
    {
        list<ActivityApproval__c> toUpdate = new list<ActivityApproval__c>();
        for (CActivityApprovalToPrintFacad approvalFacad : pApprovals)
        {
            ActivityApproval__c toAdd = approvalFacad.pActivityApproval.pActivityApproval;
            if (toAdd.IsPrinted__c != true) 
            {
                toAdd.IsPrinted__c = true;
                toUpdate.add(toAdd);
                if (toUpdate.size() > 50) 
                {
                    CActivityApprovalDb.IsAfterChangeCoordinator = true;
                    update toUpdate;
                    toUpdate = new list<ActivityApproval__c>();
                }
            }
        }
        if (toUpdate.size() > 0) 
        {
            CActivityApprovalDb.IsAfterChangeCoordinator = true;
            update toUpdate;
        }
    }
    
    
    
    private list<ActivityApproval__c> GetActivityApprovalsByIds()
    {
        list<string> ids = GetIds();
        return [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, NoteToPrintPage__c,
                ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, SendEmailToFamily__c, Rate__c,
                MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, Note__c, WeeklyHours__c, id, ProfessionName__r.name,
                RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, ChildName__r.name, name, CreatedDate, 
                ChildName__r.FamilyName__r.name, ProfessionName__r.ProfessionTypeName__r.PaymentType__c, CaseName__c, IsPrinted__c,
                ChildName__r.FamilyName__r.City__c, ChildName__r.FamilyName__r.Address__c,TutorName__r.Name
                from ActivityApproval__c where id in :ids order by ChildName__r.name];
    }
    
    private void buildFamiliesMap()
    {
    	
    	approvalsMap = new Map<String,List<CActivityApprovalToPrintFacad>>();
        list<string> ids = GetIds();
        List<Family__c> allFamilies = [select Id ,City__c, Address__c, Name from Family__c where Id in :ids];
        List<ActivityApproval__c> approvals = [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, NoteToPrintPage__c,
                ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, SendEmailToFamily__c, Rate__c,
                MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, Note__c, WeeklyHours__c, id, ProfessionName__r.name,
                RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, ChildName__r.name, name, CreatedDate, 
                ChildName__r.FamilyName__r.name, ProfessionName__r.ProfessionTypeName__r.PaymentType__c, CaseName__c, IsPrinted__c,
                ChildName__r.FamilyName__r.City__c, ChildName__r.FamilyName__r.Address__c,ChildName__r.FamilyName__c,TutorName__r.Name,
                TutorName__r.MobilePhone__c 
                from ActivityApproval__c 
                where ChildName__r.FamilyName__c  in :ids 
                and SchoolYear__c = :CSchoolYearDb.GetActiveYearId()
                order by ChildName__r.name];
                
        for (ActivityApproval__c approval : approvals){
        	if(approvalsMap.get(approval.ChildName__r.FamilyName__c)  == null)
        		approvalsMap.put(approval.ChildName__r.FamilyName__c, new List<CActivityApprovalToPrintFacad>());
        	approvalsMap.get(approval.ChildName__r.FamilyName__c).add(new CActivityApprovalToPrintFacad(approval));
        }
        
        families = new List<Family__c>();
        for(Family__c family :allFamilies){
        	if(approvalsMap.keySet().contains(family.Id)){
	        	if (family.Name  != null)family.Name = CObjectNames.getHebrewString(family.Name);
	        	if (family.Address__c  != null)family.Address__c = CObjectNames.getHebrewString(family.Address__c);
	        	if (family.City__c  != null)family.City__c = CObjectNames.getHebrewString(family.City__c);
        		families.add(family);
        	}
        }
        system.debug(approvalsMap);
    }
    
    private list<string> GetIds()
    {
        list<string> res = new list<string>();
        string idsStr = Apexpages.currentPage().getParameters().get('ids');
        if (idsStr != null && idsStr.trim() != '')
        {
            idsStr = idsStr.replace(' ', '');
            res = idsStr.split('-');
        }
        return res;
    }
    
    private void SetApprovalsFacads(list<ActivityApproval__c> approvals)
    {
        pApprovals = new list<CActivityApprovalToPrintFacad>();
        for (ActivityApproval__c approval : approvals)
        {
            pApprovals.add(new CActivityApprovalToPrintFacad(approval));
        }
    }
}