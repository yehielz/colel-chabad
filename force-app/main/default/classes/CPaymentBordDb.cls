public with sharing class CPaymentBordDb 
{
	public list<CPaymentInBord> pPayments { get; set; }
	public date pStartDate { get; set; }
	public date pEndDate { get; set; }
	
	public CPaymentBordDb()
	{
		SetStartDate();
		SetEndDate();
		SetPayments();
	}
	
	private void SetStartDate()
	{
		date d = date.newInstance(Datetime.now().year(), Datetime.now().month(), 1);
		string SDate = Apexpages.currentPage().getParameters().get('sd');
		if (SDate != null && SDate.trim() != '')
		{
			try { d = date.valueOf(SDate); }
			catch(Exception e){ d = date.newInstance(Datetime.now().year(), Datetime.now().month(), 1); }
		}
		pStartDate = d;
	}
	
	private void SetEndDate()
	{
		date d = date.newInstance(Datetime.now().year(), Datetime.now().month() + 1, 0);
		string SDate = Apexpages.currentPage().getParameters().get('ed');
		if (SDate != null && SDate.trim() != '')
		{
			try { d = date.valueOf(SDate); }
			catch(Exception e){ d = date.newInstance(Datetime.now().year(), Datetime.now().month() + 1, 0); }
		}
		pEndDate = d;
	}
	
	private void SetPayments()
	{
		pPayments = new list<CPaymentInBord>();
		Payment__c[] payments = [select name, id, Debt__c, TutorName__c, Balance__c, FamilyName__c, Amount__c, Date__c, FamilyName__r.InnerNumber__c, 
								 FamilyName__r.ParentName__c, NatureOfPayment__c, PaymentTarget__c from Payment__c where Date__c >= :pStartDate 
								 and Date__c <= :pEndDate];
		map<string, list<Check__c>> checksByPayment = GetChecksByPayment(payments);
		map<string, list<ActivityReport__c>> activityReportByPayment = GetActivityReportByPayment(payments);
		map<string, list<SpecialActivitiesLine__c>> specialActivitiesLineByPayment = GetSpecialActivitiesLineByPayment(payments);
		for (Payment__c payment : payments)
		{
			list<Check__c> checks = new list<Check__c>();
			if (checksByPayment.containsKey(payment.id))
				checks = checksByPayment.get(payment.id);
			list<ActivityReport__c> reports = new list<ActivityReport__c>();
			if (activityReportByPayment.containsKey(payment.id))
				reports = activityReportByPayment.get(payment.id);
			list<SpecialActivitiesLine__c> specialActivitiesLines = new list<SpecialActivitiesLine__c>();
			if (specialActivitiesLineByPayment.containsKey(payment.id))
				specialActivitiesLines = specialActivitiesLineByPayment.get(payment.id);
			pPayments.add(new CPaymentInBord(payment, checks, reports, specialActivitiesLines));
		}
	}
	
	private map<string, list<Check__c>> GetChecksByPayment(list<Payment__c> payments)
	{
		Check__c[] checks = [select name, id, PaymentName__c, AmountCheck__c, DepositDate__c from Check__c where PaymentName__c in :payments];		
		map<string, list<Check__c>> ret = new map<string, list<Check__c>>();
		for (Check__c check : checks)
		{
			if (ret.containsKey(check.PaymentName__c))
				ret.get(check.PaymentName__c).add(check);
			else
				ret.put(check.PaymentName__c, new list<Check__c> { check });
		}
		return ret;
	}
	
	private map<string, list<ActivityReport__c>> GetActivityReportByPayment(list<Payment__c> payments)
	{
		ActivityReport__c[] activityreports = [select name, id, PaymentName__c, ProfessionName__c, ProfessionName__r.name from ActivityReport__c 
											   where PaymentName__c in :payments];		
		map<string, list<ActivityReport__c>> ret = new map<string, list<ActivityReport__c>>();
		for (ActivityReport__c activityreport : activityreports)
		{
			if (ret.containsKey(activityreport.PaymentName__c))
				ret.get(activityreport.PaymentName__c).add(activityreport);
			else
				ret.put(activityreport.PaymentName__c, new list<ActivityReport__c> { activityreport });
		}
		return ret;
	}
	
	private map<string, list<SpecialActivitiesLine__c>> GetSpecialActivitiesLineByPayment(list<Payment__c> payments)
	{
		SpecialActivitiesLine__c[] specialActivitiesLine = [select name, id, PaymentName__c, SpecialActivitiesName__c, SpecialActivitiesName__r.name, 
															SpecialActivitiesName__r.ActivityDescription__c
														    from SpecialActivitiesLine__c where PaymentName__c in :payments];		
		map<string, list<SpecialActivitiesLine__c>> ret = new map<string, list<SpecialActivitiesLine__c>>();
		for (SpecialActivitiesLine__c line : specialActivitiesLine)
		{
			if (ret.containsKey(line.PaymentName__c))
				ret.get(line.PaymentName__c).add(line);
			else
				ret.put(line.PaymentName__c, new list<SpecialActivitiesLine__c> { line });
		}
		return ret;
	}
}