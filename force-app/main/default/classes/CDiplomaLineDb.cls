public with sharing class CDiplomaLineDb 
{
	private Diploma__c pDiploma { get; set; }
	private list<CDiplomaLineInList> pDiplomaLines { get; set; }
	private static map<string, boolean> clusteringByProfession;
	public static map<string, boolean> pClusteringByProfession 
	{ 
		get
		{
			if (clusteringByProfession == null)
			{
				list<Profession__c> professionslist = [select name, id, IsTheoretical__c from Profession__c where ForDiploma__c = :true order by name];
				clusteringByProfession = new map<string, boolean>();
				for (Profession__c profession : professionslist)
				{
					profession.IsTheoretical__c = profession.IsTheoretical__c == null ? false : profession.IsTheoretical__c;
					clusteringByProfession.put(profession.id, profession.IsTheoretical__c);
				}
			}
			return clusteringByProfession;
		}
	}
			 	
	public CDiplomaLineDb(Diploma__c extDiploma, boolean IsNewPage)
	{
		pDiploma = extDiploma;
		if (IsNewPage)
		{
			pDiplomaLines = new List<CDiplomaLineInList>{ new CDiplomaLineInList(new DiplomaLine__c(IsToSave__c = true)) };
			//FillpDiplomaLines();
			if (pDiploma.id != null)
				SetpDiplomaLinesBypDiplomaId(pDiploma.id);
		}
		else
			pDiplomaLines = GetDiplomaLinesByDiplomaId(pDiploma.id);
	}
	
	/*private void FillpDiplomaLines()
	{
		pDiplomaLines = new List<DiplomaLine__c>();
		Profession__c[] professions = [select name, id, IsTheoretical__c from Profession__c where ForDiploma__c = :true order by name];
		for (integer i = 0; i < professions.size(); i++)
		{
			DiplomaLine__c tempDiplomaLine = new DiplomaLine__c();
			tempDiplomaLine.ProfessionName__c = professions[i].id;
			pDiplomaLines.add(tempDiplomaLine);
		}
	}*/
	
	public void AddDiplomaLine()
	{
		pDiplomaLines.add(new CDiplomaLineInList(new DiplomaLine__c(IsToSave__c = true)));
	}
	
	private void SetpDiplomaLinesBypDiplomaId(string id)
	{
		pDiplomaLines = GetDiplomaLinesByDiplomaId(id);
		//SetExtLines(diplomaLines);
		//SetNotExtLines(diplomaLines);
	}
	
	private list<CDiplomaLineInList> GetDiplomaLinesByDiplomaId(string id)
	{
		list<CDiplomaLineInList> ret = new list<CDiplomaLineInList>();
		DiplomaLine__c[] linesList = [select name, id, Grade__c, GradeInWords__c, ProfessionName__c, DiplomaName__c, IsToSave__c, Note__c, Clustering__c, 
										ProfessionName__r.IsTheoretical__c from DiplomaLine__c where DiplomaName__c = :id];
		for(DiplomaLine__c line : linesList)
		{
			ret.add(new CDiplomaLineInList(line));
		}
		return ret;
	}
	
	/*private void SetNotExtLines(List<DiplomaLine__c> diplomaLines)
	{
		map<string, DiplomaLine__c> diplomaLinesMap = GetDiplomaLinesMapFromList(pDiplomaLines);
		for (integer i = 0; i < diplomaLines.size(); i++)
		{
			if (!diplomaLinesMap.containsKey(diplomaLines[i].ProfessionName__c))
			{
				diplomaLines[i].IsToSave__c = true;
				pDiplomaLines.add(diplomaLines[i]);
			}
		}
	}
	
	private void SetExtLines(List<DiplomaLine__c> diplomaLines)
	{
		map<string, DiplomaLine__c> diplomaLinesMap = GetDiplomaLinesMapFromList(diplomaLines);
		for (integer i = 0; i < pDiplomaLines.size(); i++)
		{
			if (diplomaLinesMap.containsKey(pDiplomaLines[i].ProfessionName__c))
			{
				diplomaLinesMap.get(pDiplomaLines[i].ProfessionName__c).IsToSave__c = true;
				pDiplomaLines.set(i, diplomaLinesMap.get(pDiplomaLines[i].ProfessionName__c));
			}
		}
	}*/
	
	private map<string, DiplomaLine__c> GetDiplomaLinesMapFromList(List<DiplomaLine__c> diplomaLines)
	{
		map<string, DiplomaLine__c> ret = new map<string, DiplomaLine__c>();
 		for (integer i = 0; i < diplomaLines.size(); i++)
 		{
 			ret.put(diplomaLines[i].ProfessionName__c, diplomaLines[i]);
 		}
 		return ret;
	}
	
	public void InsertpDiplomaLines()
	{
		pDiplomaLines = SendToDeleteThoseWhoNotToSave(pDiplomaLines);
		List<DiplomaLine__c> diplomaLinesToInsert = new List<DiplomaLine__c>();
		List<DiplomaLine__c> diplomaLinesToUpdate = new List<DiplomaLine__c>();
		for (CDiplomaLineInList line : pDiplomaLines)
		{
			line.pDiplomaLine.Clustering__c = line.IsTheoretical ? line.pDiplomaLine.Clustering__c : '';
			if (line.pDiplomaLine.id == null)
			{
				line.pDiplomaLine.DiplomaName__c = pDiploma.id;
				diplomaLinesToInsert.add(line.pDiplomaLine);
			}
			else
				diplomaLinesToUpdate.add(line.pDiplomaLine);
		}
		if (diplomaLinesToInsert.size() > 0)
			insert diplomaLinesToInsert;
		else if (diplomaLinesToUpdate.size() > 0)
			update diplomaLinesToUpdate;
	}
	
	private list<CDiplomaLineInList> SendToDeleteThoseWhoNotToSave(list<CDiplomaLineInList> diplomaLines)
	{
		list<CDiplomaLineInList> toreturn = new list<CDiplomaLineInList>();
		list<DiplomaLine__c> todelete = new list<DiplomaLine__c>();
		for (CDiplomaLineInList diplomaLine : diplomaLines)
		{
			integer result = GetLineResult(diplomaLine.pDiplomaLine);
			if (result == 1)
				toreturn.add(diplomaLine);
			if (result == -1)
				todelete.add(diplomaLine.pDiplomaLine);
		}
		if (todelete.size() > 0)
			delete todelete;
		return toreturn;
	}
	
	private integer GetLineResult(DiplomaLine__c diplomaLine)
	{
		if (diplomaLine.IsToSave__c)
			return 1;
		if (diplomaLine.id != null)
			return -1;
		return 0;
	}
	
	/*public void CheckGradeWords()
	{
		for (integer i = 0; i < pDiplomaLines.size(); i++)
		{
			if (pDiplomaLines[i].Grade__c == null || pDiplomaLines[i].Grade__c == 0)
			{
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordExcellent)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade100;
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordVeryGood)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade95;
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordAlmostVeryGood)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade90;
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordgood)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade80;
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordAlmostGood)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade70;
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordEnough)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade60;
				if (pDiplomaLines[i].GradeInWords__c == CObjectNames.ExamGradeInWordNotEnough)
					pDiplomaLines[i].Grade__c = CObjectNames.ExamGrade50;
			} 
		}
	}*/
	
	public List<CDiplomaLineInList> GetpDiplomaLines()
	{
		return pDiplomaLines;
	}
	
	public static list<DiplomaLine__c> GetDiplomaLineByDiplomaId(string diplomaId)
	{
		return [select name, id, Grade__c, GradeInWords__c, ProfessionName__c, DiplomaName__c,
				ProfessionName__r.name, IsToSave__c, Note__c, Clustering__c, ProfessionName__r.IsTheoretical__c 
				from DiplomaLine__c where DiplomaName__c = :diplomaId];
	}
	
	public static list<DiplomaLine__c> GetDiplomaLineByChildIdAndProfessions(string childId, list<string> professionsId)
	{
		return [select name, id, Grade__c, GradeInWords__c, ProfessionName__c, DiplomaName__c, ProfessionName__r.name, IsToSave__c, 
				DiplomaName__r.Half__c, DiplomaName__r.Date__c, DiplomaName__r.ChildName__c, Note__c, Clustering__c , ProfessionName__r.IsTheoretical__c 
				from DiplomaLine__c where DiplomaName__r.ChildName__c = :childId and ProfessionName__c in :professionsId];
	}
	
	public static list<DiplomaLine__c> GetDiplomaLineByChildrenIdAndProfessions(list<string> childrenIds, list<string> professionsId)
	{
		return [select name, id, Grade__c, GradeInWords__c, ProfessionName__c, DiplomaName__c, ProfessionName__r.name, IsToSave__c, 
				DiplomaName__r.Half__c, DiplomaName__r.Date__c, DiplomaName__r.ChildName__c, Note__c, Clustering__c, ProfessionName__r.IsTheoretical__c 
				from DiplomaLine__c where DiplomaName__r.ChildName__c in :childrenIds and ProfessionName__c in :professionsId];
	}
}