public with sharing class Ctrl07MassSMSPayments {
    private List<Payment__c> paym ;
    public List<Utils_SMS.SMS_Request> smsMessages {get;set;}
    public List<String> getFieldsToDisplay() {
         return new List<String> {'FamilyName__c','Name','Id','AmountPaidByMasav__c','NatureOfPayment__c'} ;
    }
    public Ctrl07MassSMSPayments(ApexPages.StandardSetController records){
        paym = (List<Payment__c>)records.getSelected(); 
        paym = [Select p.Status__c, p.PaymentTarget__c, p.NatureOfPayment__c, p.Name,p.MasavFile__r.PaymentDate__c,p.MasavFile__c,
                         p.Month__c, p.FamilyName__r.MobilePhone__c, p.FamilyName__c, p.Date__c, 
                         p.AmountPaidByMasav__c,p.FamilyName__r.ParentName__c, p.FamilyName__r.Name
                From Payment__c p 
                where Id in :paym];
        
    }
    
    public Pagereference init(){
        if(paym == null || paym.size() == 0 ){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING,'עליך לבחור לפחות שורה אחת.'));
            return null;
        }
        
        smsMessages = new List<Utils_SMS.SMS_Request> ();
        String prefix =  ((String)paym[0].Id ).substring(0,3);
        for(Payment__c p :paym){
            Utils_SMS.SMS_Request req = new Utils_SMS.SMS_Request ();
            
            req.message = String.format(Label.PaymentSMS, 
                                new String []{ p.FamilyName__r.ParentName__c + ' ' + p.FamilyName__r.Name, 
                                    String.valueOf(p.AmountPaidByMasav__c),
                                             Date.today().format()  ,
                                            p.NatureOfPayment__c,
                                             DateConverter.GetMonthInHebrew(Integer.valueOf( p.Month__c ) - 1)  });
            req.phoneNumber = p.FamilyName__r.MobilePhone__c;
            req.record = p;
            req.send = false;
            if(p.AmountPaidByMasav__c > 0) req.send = true;
            smsMessages.add(req);
        }
        
        //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.INFO,results));
        return null;
    
    }
    
    public Pagereference sendSMS(){
        List<String> res = Utils_SMS.sendSms(smsMessages);
        integer i = 0;
        for(Utils_SMS.SMS_Request req :smsMessages){
            if(res.size() > i && req.send == true){
                req.result = res[i];
                i++;
            }
        }
        return null;
    }
}