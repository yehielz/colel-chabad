public with sharing class CCheckToPrint 
{
	public Check__c pCheck { get; set; }
	public integer pIndex { get; set; }
	
	public CCheckToPrint(Check__c check, integer index)
	{
		pCheck = check;
		pIndex = index;
		sAmountInWords = CObjectNames.GetHebrewWords(CAmountIntoWordsConverter.Convert(pCheck.AmountCheck__c), 62);
		if (!sAmountInWords.contains('<br/>'))
			sAmountInWords += '<br/>';
	}
	
	public string sAmount { get{ return GetFormat(pCheck.AmountCheck__c); } }
	public string sAmountInWords { get; set; }
	public string sPayTo { get{ return pCheck.ToOrPayable__c != null ? CObjectNames.getHebrewString('שלמו ' + pCheck.ToOrPayable__c + ':') : CObjectNames.getHebrewString('שלמו ל:'); } }
	public string sDate { get{ return pCheck.DepositDate__c != null ? CObjectNames.GetDateStr(pCheck.DepositDate__c) : ''; } }
	
	
	public string GetFormat(decimal num)
	{
		if (num != null)
		{
			string numStr = num.format();
			system.debug('numStr = '+ numStr);
			if (numStr.contains('.'))
			{
				string afterPoint = numStr.split('\\.')[1];
				system.debug('afterPoint = '+ afterPoint);
				if (afterPoint.length() < 2)
					return numStr + '0';
				else
					return numStr;
			}
			else
				return numStr + '.00';
		}
		return '0.00';
	}
}