public with sharing class CProperForFullReportFacad
{
	public Children__c pChild { get; private set; }
	public boolean IsCasesInView{ get; set; }
	public boolean IsRequestInView{ get; set; }
	public boolean IsApprovalsInView{ get; set; }
	public boolean IsReportsInView{ get; set; }
	public boolean IsDiplomasInView{ get; set; }
	public boolean IsExamsInView{ get; set; }
	public boolean IsChildProgressInView{ get; set; }
	public list<selectOption> choosen { get; set; }
	public list<selectOption> available { get; set; }
	
	
	
	public CProperForFullReportFacad()
	{
		SetChild();
		SetOptions();
		IsCasesInView = true;
		IsRequestInView = true;
		IsApprovalsInView = true;
		IsReportsInView = true;
		IsDiplomasInView = true;
		IsExamsInView = true;
		IsChildProgressInView = true;
	}
	
	private void SetChild()
	{
		string childId = Apexpages.currentPage().getParameters().get('childId');
		if (childId != null && childId != '')
			pChild = CChildDb.GetChildById(childId);
			
		if (pChild == null)
			pChild = new Children__c();
	}
	
	private void SetOptions()
	{
		choosen = new list<selectoption>();
		datetime tday = datetime.now();
		string tdayYear = date.newInstance(tday.year(), tday.month(), tday.day()) < date.newInstance(tday.year(), 9, 1) ? ((tday.year() - 1) + '-' + tday.year()) : (tday.year() + '-' + (tday.year() + 1));
		map<string, selectOption> yearsMap = new map<string, selectOption>();	
		list<ActivityApproval__c> activityApprovals = CActivityApprovalDb.GetActivityApprovalByChildId(pChild.id);
		for (ActivityApproval__c approval : activityApprovals)
		{
			string year = approval.FromDate__c < date.newInstance(approval.FromDate__c.year(), 9, 1) ? ((approval.FromDate__c.year() - 1) + '-' + approval.FromDate__c.year()) : 
							(approval.FromDate__c.year() + '-' + (approval.FromDate__c.year() + 1));
			yearsMap.put(year, new selectOption(year, year));
		}
		if (yearsMap.containsKey(tdayYear))
			choosen.add(new selectoption(tdayYear, tdayYear));
		available = yearsMap.values();
	}
	
	public PageReference BackToChild()
	{
		return new PageReference(CObjectNames.getBaseUrl() + '/' + pChild.id);
	}
	public PageReference ContinueToPrint()
	{
		return new PageReference(CObjectNames.getBaseUrl() + page.ChildFullReport.getUrl() + '?childId=' + pChild.id + '&view=' + GetToView() + '&years=' + GetYearsToView());
	}
	
	private string GetToView()
	{
		if (IsCasesInView && IsRequestInView && IsApprovalsInView && IsReportsInView && IsDiplomasInView && IsExamsInView && IsChildProgressInView)
			return 'all';
		else
		{
			string ret = '';
			if (IsCasesInView)
				ret += 'case';
			if (IsRequestInView)
				ret += ret == '' ? 'request' : '-request';
			if (IsApprovalsInView)
				ret += ret == '' ? 'approval' : '-approval';
			if (IsReportsInView)
				ret += ret == '' ? 'report' : '-report';
			if (IsDiplomasInView)
				ret += ret == '' ? 'diploma' : '-diploma';
			if (IsExamsInView)
				ret += ret == '' ? 'exam' : '-exam';
			if (IsChildProgressInView)
				ret += ret == '' ? 'progress' : '-progress';
			ret += ret == '' ? 'none' : '';
			return ret;
		}
	}
	
	private string GetYearsToView()
	{
		if (choosen.size() < 1)
			return 'none';
		else if (available.size() < 1)
			return 'all';
		else
		{
			string ret = '';
			for (selectOption ch : choosen)
			{
				ret += ret == '' ? ch.getValue() : ';' + ch.getValue();
			}
			return ret;
		}
	}
}