public with sharing class CWebSiteChildrenDb 
{
	map<string, CProfessionByChild> professionsByChildrenMap { get; set; }
	public string childAndProfession { get; set; }
	
	public CWebSiteChildrenDb()
	{
		
	}
	
	public List<SelectOption> GetChildrenList()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null && ApexPages.currentPage().getParameters().get('month') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null)
			{
				CInterface iface = new CInterface();
				if(sessionId.TutorName__c != null){
					professionsByChildrenMap = iface.GetChildrenByMonth(sessionId.TutorName__c, ApexPages.currentPage().getParameters().get('month'), false);
				}
				else if (sessionId.Family__c != null){
					professionsByChildrenMap = iface.GetChildrenByMonth(sessionId.Family__c, ApexPages.currentPage().getParameters().get('month'), true);
				}
				List<SelectOption> ret = new List<SelectOption>();
				ret.add(new SelectOption('null', 'ללא'));
				list<CProfessionByChild> professionsByChildrenList = professionsByChildrenMap.values();
				for (integer i = 0; i < professionsByChildrenList.size(); i++)
				{
					list<Profession__c> professionsList = professionsByChildrenList[i].professions.values();
					for (integer a = 0; a < professionsList.size(); a++)
					{
						string value = professionsByChildrenList[i].child.id + '-' + professionsList[a].id;
						string label = professionsByChildrenList[i].child.name + ' - ' + professionsList[a].name;
						ret.add(new SelectOption(value, label));
					}
				}
				return ret;
			}
		}
		return new List<SelectOption> {new SelectOption('null', 'ללא')};
	}
	/*
	public void SetChildAndProfessionCookie()
	{
		if (test.isRunningTest())
			childAndProfession = GetChildAndProfessionForTest();
		string child = childAndProfession.split('-')[0];
		string profession = childAndProfession.split('-')[1];
		Cookie childCookie = new Cookie('child', child, null, -1, false);
		Cookie professionCookie = new Cookie('profession', profession, null, -1, false);
		//ApexPages.currentPage().setCookies(new Cookie[] {  professionCookie } );
		//ApexPages.currentPage().setCookies(new Cookie[] { childCookie, professionCookie } );
	}
	*/
	private string GetChildAndProfessionForTest()
	{
		ActivityApproval__c[] activityApproval = [select name, id, ChildName__c, ProfessionName__c from ActivityApproval__c];
		return activityApproval[0].ChildName__c + '-' + activityApproval[0].ProfessionName__c;
	}
}