public with sharing class CMasavFileFacad 
{
	public list<CPaymentInMasavFileList> pPayments{ get{ return masavFileDb.pPayments; } }
	public MasavFile__c pMasavFile{ get{ return masavFileDb.pMasavFile; } }
	public boolean IsShowTable{ get{ return masavFileDb.IsShowTable; } }
	public string pageTitle{ get; set; }
	private boolean IsNewPage{ get{ return masavFileDb.IsNewPage; } }
	public string temp { get; set; }
	
	private CMasavFileDb masavFileDb;
	
	public CMasavFileFacad(Apexpages.Standardcontroller controller)
	{
		masavFileDb = new CMasavFileDb(controller);
		if (pMasavFile.id != null)
			pageTitle = pMasavFile.name;
		else
			pageTitle = 'קובץ מס"ב חדש';
	}
	
	public PageReference Save()
	{
		try
		{
			masavFileDb.Save();
		}
		catch(Exception e)
		{
			//Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.pDailure == null ? e.getMessage() : e.pDailure));
			return null;
		}
		return new PageReference(pRetUrl);
	}
	
	public integer PageNumber{ get{ return masavFileDb.PageNumber; } set{ masavFileDb.PageNumber = value; } }
    public integer PageSize{ get{ return masavFileDb.PageSize; } set{ masavFileDb.PageSize = value; } }
    public boolean IsFirstPage{ get{ return masavFileDb.IsFirstPage; } set{ masavFileDb.IsFirstPage = value; } }
    public boolean IsLastPage{ get{ return masavFileDb.IsLastPage; } set{ masavFileDb.IsLastPage = value; } }
	
	public void OnChangePage()
    {
        masavFileDb.OnChangePage();
    }
    
    public void SetLinesByPreviousPage()
    {
        masavFileDb.SetLinesByPreviousPage();
    }
    
    public void SetLinesByNextPage()
    {
        masavFileDb.SetLinesByNextPage();
    }
    
    public void SetIsShowTable()
    {
        masavFileDb.SetIsShowTable();
    }
	
	public string mRetUrl;
	public string pRetUrl
	{
		get
		{
			if (mRetUrl == null)
			{
				mRetUrl = apexpages.currentpage().getParameters().get('RetUrl');
				if (mRetUrl == null || mRetUrl.trim() == '' || IsNewPage)
				{
					mRetUrl = null;
					return CObjectNames.GetBaseUrl() + '/' + pMasavFile.id;
				}
			}
			return mRetUrl;
		}
	}
}