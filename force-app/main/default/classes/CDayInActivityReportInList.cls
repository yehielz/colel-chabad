public with sharing class CDayInActivityReportInList 
{
	public DayInActivityReport__c pDay { get; set; }
	public ActivityApproval__c pReportApproval { get;set; }
	public boolean IsHasPriceOnTravel
	{
		get
		{
			if (pDay != null && pDay.PriceOnTravel__c != null && pDay.PriceOnTravel__c > 0)
				return true;
			else
				return false;
		}
		set
		{
			if (value)	
				pDay.PriceOnTravel__c = pReportApproval.Fare__c;
			else
				pDay.PriceOnTravel__c = 0;
		}
	}
	
	public CDayInActivityReportInList(DayInActivityReport__c day, ActivityApproval__c approval)
	{
		pDay = day;
		pReportApproval = approval;
	}
}