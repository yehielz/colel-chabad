public with sharing class CActivityApprovalInPayment 
{
	public ActivityApproval__c pActivityApproval { get;set; }
	
	public CActivityApprovalInPayment(ActivityApproval__c mActivityApproval)
	{
		pActivityApproval = mActivityApproval;
	}
	
	public string weeklyHoursOrAmountLabel
	{
		get
		{
			string paymentType = pActivityApproval.ProfessionName__r.ProfessionTypeName__r.PaymentType__c;
			if (paymentType == CObjectNames.MonthlyPayment || paymentType == CObjectNames.OneTimePayment)
				return 'סכום שאושר';
			if(paymentType == CObjectNames.Meetings)
				return 'מספר מפגשים';
			else
				return 'שעות שבועיות';
		}
	}
	
	public string weeklyHoursOrAmountField
	{
		get
		{
			string paymentType = pActivityApproval.ProfessionName__r.ProfessionTypeName__r.PaymentType__c;
			if (paymentType == CObjectNames.MonthlyPayment || paymentType == CObjectNames.OneTimePayment)
				return string.valueOf(pActivityApproval.AmountApproved__c);
			if(paymentType == CObjectNames.Meetings)
				return string.valueOf(pActivityApproval.MeetingsNumber__c);
			else
				return string.valueOf(pActivityApproval.WeeklyHours__c);
		}
	}
	
	public string TotalSpent
	{
		get
		{
			string paymentType = pActivityApproval.ProfessionName__r.ProfessionTypeName__r.PaymentType__c;
			if (paymentType == CObjectNames.MonthlyPayment || paymentType == CObjectNames.OneTimePayment)
				return 'סכום שנוצל <br/> ' + pActivityApproval.AmountSpent__c.setscale(2);
			else
			{
				string ret = '';
				if (paymentType == CObjectNames.Meetings)
					ret += '<div class="TdToHalf"><span class="HeadInTdInTable">מפגשים שנוצלו</span><br/>' + (pActivityApproval.TotalMeetingSpent__c == null ? '0' : 
						    string.valueOf(pActivityApproval.TotalMeetingSpent__c.setscale(2))) + '</div>';
				else
					ret += '<div class="TdToHalf"><span class="HeadInTdInTable">שעות שאושרו</span><br/>' + (pActivityApproval.TotalHours__c == null ? '0' : 
						    string.valueOf(pActivityApproval.TotalHours__c.setscale(2))) + '</div>';
				ret += '<div class="TdToHalf"><span class="HeadInTdInTable">שעות שנוצלו</span><br/>' + (pActivityApproval.TotalHoursSpent__c == null ? '0' : 
						    string.valueOf(pActivityApproval.TotalHoursSpent__c.setscale(2))) + '</div>';
				if (paymentType != CObjectNames.Meetings)
					ret += '<div class="TdToHalf"><span class="HeadInTdInTable">מפגשים</span><br/>' + (pActivityApproval.TotalMeetingSpent__c == null ? '0' : 
						    string.valueOf(pActivityApproval.TotalMeetingSpent__c.setscale(2))) + '</div>';
				return ret;
			}
		}
	}
}