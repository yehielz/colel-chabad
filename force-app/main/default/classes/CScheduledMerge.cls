global class CScheduledMerge implements Schedulable
{
	/*public List<colel__ActivityApproval__c> testList { get; set; }
	
	public CScheduledMerge()
	{
		testList = new list<colel__ActivityApproval__c>(); 
		map<string, List<ActivityApproval__c>> activityApprovalsByMonths = GetActivityApprovalsByMonth();
		if (activityApprovalsByMonths.containsKey(datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year()))
		{
			testList = GetListToCreateTask(activityApprovalsByMonths.get(datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year()));
			//SetTasksNotReport(testList);
		}
		//SetTasksReport();
	}*/
	
	global void execute(SchedulableContext ctx)
	{
		/*list<colel__ActivityApproval__c> listToCreateTask = new list<colel__ActivityApproval__c>(); 
		map<string, List<ActivityApproval__c>> activityApprovalsByMonths = GetActivityApprovalsByMonth();
		if (activityApprovalsByMonths.containsKey(datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year()))
		{
			listToCreateTask = GetListToCreateTask(activityApprovalsByMonths.get(datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year()));
			SetTasksNotReport(listToCreateTask);
		}
		SetTasksReport();*/
   	}  
   	
   	/*private void SetTasksReport()
   	{
   		list<colel__ActivityReport__c> activityReportList = GetReportedList();
   		map<string, User> usersByName = GetUsersByName();
   		User[] managers = GetUsersManagers();
   		User secretary = GetUserSecretary();
   		list<Task> tasksToInsert = new list<Task>();
   		//map<string, colel__Tutor__c> tutorsMap = GetTutorMap();
   		for (integer i = 0; i < activityReportList.size(); i++)
   		{
	   		string[] ownersId = GetUsersIdToOwners(activityReportList[i], usersByName, managers, secretary);
	   		for (integer a = 0; a < ownersId.size(); a++)
	   		{
		   		Task newTask = new Task();
				newTask.Description = 'ממתין עבורך דיווח פעילות בשם: ' + activityReportList[i].name;
				newTask.ChildName__c = activityReportList[i].ChildName__r.name;
				newTask.FamilyName__c = activityReportList[i].ChildName__r.FamilyName__r.name;
				newTask.TutorName__c = activityReportList[i].TutorName__r.name;
				newTask.City__c = activityReportList[i].ChildName__r.FamilyName__r.City__c;
				newTask.CoordinatorUserName__c = system.Userinfo.getName();
				newTask.Subject = CObjectNames.TaskSobjectNewDaysInReport;
				newTask.WhatId = activityReportList[i].id;
				newTask.OwnerId = ownersId[a];
				newTask.ActivityDate = datetime.now().date().addDays(7);
				CActivityReportDb.isTaskCreated = true;
				tasksToInsert.add(newTask);
	   		}
   		}
   		if (tasksToInsert.size() > 0)
   			insert tasksToInsert;
   	}
   	
   	
   	
   	private User[] GetUsersManagers()
   	{
   		UserRole[] userRoles = [select name, id from UserRole where name = :CObjectNames.RoleActivityManager];
		if (userRoles.size() > 0)
		{
			User[] manager = [select name, id from User where UserRoleId = :userRoles[0].id];
			return manager;
		}
		return null;
   	}
   	
   	private User GetUserSecretary()
   	{
   		UserRole[] userRoles = [select name, id from UserRole where name = :CObjectNames.RoleSecretary];
		if (userRoles.size() > 0)
		{
			User[] secretary = [select name, id from User where UserRoleId = :userRoles[0].id];
			if (secretary.size() > 0)
				return secretary[0];
		}
		return null;
   	}
   	
   	private string[] GetUsersIdToOwners(ActivityReport__c newActivityReport, map<string, User> usersByName, User[] usersManagers, User userSecretary)
	{
		if (newActivityReport.IsForFindOut__c)
		{
			list<string> ret = new list<string>();
			for (integer i = 0; i < usersManagers.size(); i++)
				ret.add(usersManagers[i].id);
			return ret;
		}
		else if (newActivityReport.ActivityReportStatus__c == CObjectNames.ActivityReportStatusNew)
		{
			if (usersByName.containsKey(newActivityReport.CoordinatorUserName__c))
				return new list<string> { usersByName.get(newActivityReport.CoordinatorUserName__c).id };
		}
		else if (newActivityReport.ActivityReportStatus__c == CObjectNames.ActivityReportStatusPayment)
			return new list<string> { userSecretary.id };
		return null;
	}
   	
   	private list<colel__ActivityReport__c> GetReportedList()
   	{
   		map<string, colel__ActivityReport__c> activityReportsMap = CActivityReportDb.GetAllActivityReportMap();
   		colel__DayInActivityReport__c[] activityReports = [select name, id, ChildName__r.name, ChildName__r.FamilyName__r.name, 
   														   ChildName__r.FamilyName__r.City__c, TutorName__r.name, ActivityReportName__c 
   														   from colel__DayInActivityReport__c where ActivityReportName__r.ReportMonth__c = 
   														   :(datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year())];
   		map<string, colel__ActivityReport__c> ret = new map<string, colel__ActivityReport__c>();
   		for (integer i = 0; i < activityReports.size(); i++)
   		{
   			if (activityReportsMap.containsKey(activityReports[i].ActivityReportName__c))
   				ret.put(activityReports[i].ActivityReportName__c, activityReportsMap.get(activityReports[i].ActivityReportName__c));
   		}
   		return ret.values();
   	}
   	
   	private void SetTasksNotReport(list<colel__ActivityApproval__c> thelist)
   	{
   		/*map<string, User> usersByName = GetUsersByName();
   		list<Task> tasksToInsert = new list<Task>();
   		map<string, colel__Tutor__c> tutorsMap = GetTutorMap();
   		for (integer i = 0; i < thelist.size(); i++)
   		{
	   		Task newTask = new Task();
			newTask.Description = 'החונך לא שלח דיווח לחודש - ' + datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year() + '!';
			if (tutorsMap.containsKey(thelist[i].TutorName__c))
				newTask.Description = 'החונך: ' + tutorsMap.get(thelist[i].TutorName__c).name + ' לא שלח דיווח לחודש - ' + datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year() + '!';
			newTask.Subject = 'העדר דיווח - ' + datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year();
			if (tutorsMap.containsKey(thelist[i].TutorName__c))
				newTask.Subject += 'חונך: ' + tutorsMap.get(thelist[i].TutorName__c).name;
			newTask.CallObject = datetime.now().addMonths(-1).month() + '-' + datetime.now().addMonths(-1).year();
			newTask.WhatId = thelist[i].id;
			newTask.OwnerId = usersByName.get(thelist[i].CoordinatorUserName__c).id;
			newTask.ActivityDate = datetime.now().date().addDays(7);
			tasksToInsert.add(newTask);
   		}
   		if (tasksToInsert.size() > 0)
   			insert tasksToInsert;
   	}
   	
   	private map<string, colel__Tutor__c> GetTutorMap()
   	{
   		colel__Tutor__c[] tutors = [select name, id from colel__Tutor__c];
   		map<string, colel__Tutor__c> ret = new map<string, colel__Tutor__c>();
   		for (integer i = 0; i < tutors.size(); i++)
   		{
   			ret.put(tutors[i].id, tutors[i]);
   		}
   		return ret;
   	}
   	
   	private map<string, User> GetUsersByName()
   	{
   		User[] users = [select name, id from User];
   		map<string, User> ret = new map<string, User>();
   		for (integer i = 0; i < users.size(); i++)
   		{
   			ret.put(users[i].name, users[i]);
   		}
   		return ret;
   	}
   	
   	private list<colel__ActivityApproval__c> GetListToCreateTask(list<colel__ActivityApproval__c> activityApprovalsLestMonth)
   	{
   		list<colel__ActivityApproval__c> ret = new list<colel__ActivityApproval__c>();
   		map<string, string> activityApprovalsWithReport = GetActivityApprovalsWithReport();
   		for (integer i = 0; i < activityApprovalsLestMonth.size(); i++)
   		{
   			if (!activityApprovalsWithReport.containsKey(activityApprovalsLestMonth[i].id))
   			{
   				ret.add(activityApprovalsLestMonth[i]);
   			}
   		}
   		return ret;
   	}
   	
   	private map<string, string> GetActivityApprovalsWithReport()
   	{
   		colel__DayInActivityReport__c[] daysInActivityReport = [select name, id, Date__c, ActivityReportName__r.ActivityApprovalName__c from colel__DayInActivityReport__c 
   																where Date__c < :date.newInstance(datetime.now().year(), datetime.now().date().month(), 1) and 
   																Date__c > :date.newInstance(datetime.now().date().addMonths(-1).year(), datetime.now().date().addMonths(-1).month(), 0)];
   		map<string, string> ret = new map<string, string>();
   		for (integer i = 0 ; i < daysInActivityReport.size(); i++)
   		{
   			ret.put(daysInActivityReport[i].ActivityReportName__r.ActivityApprovalName__c, daysInActivityReport[i].ActivityReportName__r.ActivityApprovalName__c);
   		}
   		return ret;
   	}
   	
   	private map<string, List<ActivityApproval__c>> GetActivityApprovalsByMonth()
	{
		ActivityApproval__c[] activityApprovals = [select name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, 
												   ChildName__r.FamilyName__r.name, ChildName__r.FamilyName__r.City__c, CoordinatorUserName__c, 
												   TutorName__r.name, TutorName__c, ProfessionName__c from ActivityApproval__c where ToDate__c 
												   > :datetime.now().date().addMonths(-2) and FromDate__c < :datetime.now().date()];
   		map<string, list<ActivityApproval__c>> ret = new map<string, list<ActivityApproval__c>>();
   		for (integer i = 0; i < activityApprovals.size(); i++)
   		{
   			string[] monthesList = GetMonthesListFromActivityApproval(activityApprovals[i]);
   			for (integer a = 0; a < monthesList.size(); a++)
   			{
   				if (ret.containsKey(monthesList[a]))
   					ret.get(monthesList[a]).add(activityApprovals[i]);
   				else
   					ret.put(monthesList[a], new list<ActivityApproval__c> { activityApprovals[i] });
   			}
   		}
   		return ret;
	}
	
	private string[] GetMonthesListFromActivityApproval(ActivityApproval__c activityApproval)
	{
		map<string, string> monthesList = new map<string, string>();
		integer month = activityApproval.FromDate__c.month(), year = activityApproval.FromDate__c.year();
		boolean isMonthSmall = true;
		while (isMonthSmall)
		{
			if (year < activityApproval.ToDate__c.year())
			{
				isMonthSmall = true;
				monthesList.put(month+'-'+year, month+'-'+year);
				if (month == 12)
				{
					year++;
					month = 0;
				}
				month++;
			}
			else if (month <= activityApproval.ToDate__c.month() && year == activityApproval.ToDate__c.year())
			{
				isMonthSmall = true;
				monthesList.put(month+'-'+year, month+'-'+year);
				if (month == 12)
				{
					year++;
					month = 0;
				}
				month++;
			}
			else
				isMonthSmall = false;
		}
		return monthesList.values();
	}*/
}