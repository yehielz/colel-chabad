public with sharing class CNoteInCaseFacad 
{	
	public string errorMessage { get; set; }
	
	public Case__c pCase 
	{ 
		get
		{
			return noteInCaseDb.pCase;
		}
	}
	
	public Note pNote 
	{ 
		get
		{
			return noteInCaseDb.pNote;
		}
	}
	
	public list<selectOption> usersOptions 
	{ 
		get
		{
			return noteInCaseDb.GetUsersOptions();
		}
	}
	
	private CNoteInCaseDb noteInCaseDb;
	public CNoteInCaseFacad()
	{
		noteInCaseDb = new CNoteInCaseDb();
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		noteInCaseDb.save();
		PageReference caseViewPage = new ApexPages.StandardController(pCase).view();
        return caseViewPage;
	}
	
	private boolean Validation()
	{
		if (pNote.Title == null || pNote.Title == '')
		{
			errorMessage = 'כדי להמשיך בשמירה עליך להזין כותרת';
			return false;
		}
		return true; 
	}
	
	public PageReference cancel()
	{
		PageReference caseViewPage = new ApexPages.StandardController(pCase).view();
        return caseViewPage;
	}
}