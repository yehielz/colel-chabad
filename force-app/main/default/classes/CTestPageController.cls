public with sharing class CTestPageController 
{
	public CTestPageController()
	{
		
	}
	
	public void DoAction()
	{
		string professionId = Apexpages.currentPage().getParameters().get('professionId');
		if (professionId != null && professionId != '')
		{
			Profession__c[] profdessions = [select name, id from Profession__c where id = :professionId];
			if (profdessions.size() > 0)
			{
				ActivityReport__c[] reports = [select name, id from ActivityReport__c where ProfessionName__c = :profdessions[0].id];
				list<ActivityReport__c> toUpdate = new list<ActivityReport__c>();
				for (ActivityReport__c report : reports)
				{
					toUpdate.add(report);
					if (toUpdate.size() > 90)
						update toUpdate;
				}
				if (toUpdate.size() > 0)
					update toUpdate;
			}
		}
	}
}