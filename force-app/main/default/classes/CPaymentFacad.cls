public with sharing class CPaymentFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	private CPaymentDb paymentDb;
	
	public boolean bFamily { get; set; }
	public boolean bTutor { get; set; }
	public boolean bIsDone { get; set; }
	public boolean bIsNotDone { get; set; }
	
	public Payment__c pPayment{ get{ return paymentDb.GetpPayment(); } }
	public List<Check__c> pChecks{ get{ return paymentDb.GetpChecks(); } }
	public Family__c pPaymentFamily{ get{ return paymentDb.pPaymentFamily; } }
	public Tutor__c pPaymentTutor{ get{ return paymentDb.pPaymentTutor; } }
	public list<CActivityApprovalInPayment> pActivityApprovals { get{ return paymentDb.pActivityApprovals; } }
	
	public CPaymentFacad(ApexPages.StandardController controller)
	{
		paymentDb = new CPaymentDb(controller, IsTable());
		if (!IsDelete())
		{
			errorMessage = '';
			if (pPayment.id == null)
				pageTitle = 'תשלום חדש';
			else
				pageTitle = pPayment.name;
			OnChangeFamilyOrTutor();
		}
		else
		{
			isCanDelete();
		}
	}
	
	public PageReference save()
	{
		OnChangeCheckAmount();
		
		if (!Validation())
			return null;
		
		paymentDb.save();
		PageReference newCasePage = new ApexPages.StandardController(pPayment).view();
        return newCasePage;
	}
	

	public PageReference saveAndPrintChecks()
	{
		OnChangeCheckAmount();
		
		if (!Validation())
			return null;
		
		paymentDb.save();
		return new PageReference(CObjectNames.getBaseUrl() + page.PrintChecks.getUrl() + '?PId=' + pPayment.id);
	}
	
	private boolean IsTable()
	{
		string currentPageName = string.valueOf(ApexPages.currentPage());
		return currentPageName.contains(CObjectNames.PaymentNew); 
	}
	
	private boolean IsDelete()
	{
		string currentPageName = string.valueOf(ApexPages.currentPage());
		return currentPageName.contains(CObjectNames.PaymentDelete); 
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CPaymentValidator validator = new CPaymentValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public void OnAddCheck()
    {
		paymentDb.OnAddCheck();
    }
    
    public PageReference OnChangeFamilyOrTutor()
    {
    	if (pPayment.FamilyOrTutor__c == null || pPayment.FamilyOrTutor__c == CObjectNames.PaymentFamily)
    	{
    		bFamily = true;
    		bTutor = false;
    	}
    	else if (pPayment.FamilyOrTutor__c == CObjectNames.PaymentTutor)
    	{
    		bFamily = false;
    		bTutor = true;
    	}
    	return null;
    }
    
    public void OnChangeCheckAmount()
    {
    	paymentDb.SetTotalAmount();
    }
    
    public PageReference myDelete()
    {
    	delete pPayment;
    	
    	Schema.DescribeSObjectResult result = Payment__c.sObjectType.getDescribe();
        PageReference pageRef = New PageReference('/' + result.getKeyPrefix() + '/o');
        
        return pageRef; 
    }
    
    private void isCanDelete()
    {
    	if (pPayment.IsDone__c)
    	{
    		bIsDone = true;
			bIsNotDone = false;
    	}
    	else
    	{
    		bIsDone = false;
			bIsNotDone = true;
    	}
    }
	
	public void SetChecksByPaymentsNum()
	{
		paymentDb.SetChecksByPaymentsNum();
	}
	
	public integer pPaymentsNumber{ get{ return paymentDb.pPaymentsNumber; } set{ paymentDb.pPaymentsNumber = value; } }
	public date pFirstChackDepositDate{ get{ return paymentDb.pFirstChackDepositDate; } set{ paymentDb.pFirstChackDepositDate = value; } }
	public string sFirstChackDepositDate
	{
		get
		{ 
			if (pFirstChackDepositDate != null)
				return CObjectNames.GetFullNumberInString(pFirstChackDepositDate.day()) + '/' + 
						CObjectNames.GetFullNumberInString(pFirstChackDepositDate.month()) + '/' + 
						CObjectNames.GetFullNumberInString(pFirstChackDepositDate.year());
			return ''; 
		} 
		set
		{ 
			pFirstChackDepositDate = null;
			if (value != null && value != '')
			{
				string[] sdate = value.split('/');
				if (sdate.size() == 3)
					pFirstChackDepositDate = date.newinstance(integer.valueOf(sdate[2]), integer.valueOf(sdate[1]), integer.valueOf(sdate[0]));
			}
		} 
	}
	public string pFirstChackName{ get{ return paymentDb.pFirstChackName; } set{ paymentDb.pFirstChackName = value; } }
	public string familyInnerNumber{ get{ return paymentDb.familyInnerNumber; } }
	
	public void OnChangeFamilyName()
	{
		paymentDb.SetFamilyInnerNumber();
	}
}