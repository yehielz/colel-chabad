public with sharing class CRowSpecialActivitiesLines 
{
	public SpecialActivitiesLine__c pSpecialActivitiesLine { get; set; }
	public Family__c pFamily { get; set; }
	public Children__c pChild { get; set; }
	
	public CRowSpecialActivitiesLines(SpecialActivitiesLine__c mSpecialActivitiesLine, Family__c mFamily)
	{
		pSpecialActivitiesLine = mSpecialActivitiesLine;
		pFamily = mFamily;
		pChild = null;
	}
	
	public CRowSpecialActivitiesLines(SpecialActivitiesLine__c mSpecialActivitiesLine, Children__c mChild)
	{
		pSpecialActivitiesLine = mSpecialActivitiesLine;
		pFamily = null;
		pChild = mChild;
	}
}