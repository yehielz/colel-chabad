public with sharing class CTutorInChild 
{
	public Tutor__c pTutor { get; set; }
	public string tutorLink 
	{
		get
		{
			PageReference tutorViewPage = new ApexPages.StandardController(pTutor).view();
			return CObjectNames.getBaseUrl() + tutorViewPage.getUrl();
		}
	}
	
	public CTutorInChild(Tutor__c tutor)
	{
		pTutor = tutor;
	}
}