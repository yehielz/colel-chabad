public with sharing class CRequestForTreatmentValidator 
{
	private CRequestForTreatmentFacad cRequestForTreatmentFacad { get; set; }
	public string ErrorMessage;
	
	public CRequestForTreatmentValidator(CRequestForTreatmentFacad ExtRequestForTreatmentFacad)
	{
		ErrorMessage = '';
		cRequestForTreatmentFacad = ExtRequestForTreatmentFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
	  	if (ret) 
	    	ret = ValidateChildName();
    	//if (ret)
	    //	ret = ValidateTutor();
    	if (ret)
	     	ret = ValidateProfessionType();
     	if (ret)
	     	ret = ValidateProfessionName();
    	//if (ret) 
	    //	ret = ValidateProfessionWithTutor();
    	if (ret) 
	    	ret = ValidateOtherProfession();
    	if (ret) 
	    	ret = ValidateWeeklyHours();
    	if (ret) 
	    	ret = ValidateMeetings();
    	if (ret)
    		ret = ValidateApprovalAmount();
    	if (ret) 
	    	ret = ValidateFromDate();
    	if (ret) 
	    	ret = ValidateToDate();
    	if (ret) 
	    	ret = notMistakeDates();
    	if (ret) 
	    	ret = ValidateStayReason();
    	
	  	 
	 	return ret;
	}
	
	private boolean ValidateStayReason()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.RequestStatus__c == 'מושהה' && cRequestForTreatmentFacad.pRequestForTreatment.StayReason__c == null)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentStayReason;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFromDate()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.FromDate__c == null)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentFromDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateToDate()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.ToDate__c == null)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentToDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateWeeklyHours()
	{
		if (cRequestForTreatmentFacad.bWeeklyHours)
		{
			if (cRequestForTreatmentFacad.pRequestForTreatment.WeeklyHours__c == null || cRequestForTreatmentFacad.pRequestForTreatment.WeeklyHours__c == 0)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentWeeklyHours;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateMeetings()
	{
		if (cRequestForTreatmentFacad.bMeetings)
		{
			if (cRequestForTreatmentFacad.pRequestForTreatment.MeetingsNumber__c == null || cRequestForTreatmentFacad.pRequestForTreatment.MeetingsNumber__c == 0)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentMeetings;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateChildName()
	{
		CCaseDb caseDb = new CCaseDb();
		Case__c oneCase = caseDb.GetCaseByCaseId(cRequestForTreatmentFacad.pRequestForTreatment.CaseName__c);
		if (oneCase != null && oneCase.WhoId__c == CObjectNames.Family)
		{
			if (cRequestForTreatmentFacad.pRequestForTreatment.FamilyChildren__c == null)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentChildName;
				return false;
			}
		}
		else if (oneCase != null && oneCase.WhoId__c == CObjectNames.FamilyMember)
		{
			if (cRequestForTreatmentFacad.pRequestForTreatment.ChildName__c == null)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentChildName;
				return false;
			}
		}
		
		return true;
	}
	
	private boolean ValidateProfessionWithTutor()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.ProfessionName__c == CProfessionDb.GetOtherProfession().id)
			return true;
		if (cRequestForTreatmentFacad.pRequestForTreatment.TutorName__c != null && cRequestForTreatmentFacad.pRequestForTreatment.ProfessionName__c != null)
		{
			TutorLine__c[] tutorLines = [select name, id from TutorLine__c where TutorName__c = :cRequestForTreatmentFacad.pRequestForTreatment.TutorName__c 
										  and ProfessionName__c = :cRequestForTreatmentFacad.pRequestForTreatment.ProfessionsList__c];
			if (tutorLines.size() == 0)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentProfessionWithTutor;
				return false;
			}
		}
		
		return true;
	}
	
	private boolean ValidateOtherProfession()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.ProfessionName__c == CProfessionDb.GetOtherProfession().id && 
				cRequestForTreatmentFacad.pRequestForTreatment.ProfessionOther__c == null)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentProfessionName;
			return false;
		}
		
		return true;
	}
	
	private boolean ValidateProfessionType()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.ProfessionTypeName__c == null)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentProfessionType;
			return false;
		}
		return true;
	}
	
	private boolean ValidateProfessionName()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.ProfessionName__c == null)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentProfessionName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateApprovalAmount()
	{
		if (cRequestForTreatmentFacad.bApprovalAmount)
		{
			if (cRequestForTreatmentFacad.pRequestForTreatment.ApprovalAmount__c == null || 
				cRequestForTreatmentFacad.pRequestForTreatment.ApprovalAmount__c == 0)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentApprovalAmount;
				return false;
			}
		}
		return true;
	}
	
	private boolean notMistakeDates()
	{
		if (cRequestForTreatmentFacad.pRequestForTreatment.FromDate__c > cRequestForTreatmentFacad.pRequestForTreatment.ToDate__c)
		{
			ErrorMessage = CErrorMessages.RequestForTreatmentFromDateSmallThanToDate;
			return false;
		}
		return true;
	}
}