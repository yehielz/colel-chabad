public with sharing class CExamFacad 
{
	public boolean bIsScholarship { get; set; }
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	
	private CExamDb examDb;
	
	public Exam__c pExam
	{
		get
		{
			return examDb.GetpExam();
		}
	}
		
	public CExamFacad(ApexPages.StandardController controller)	{
		
		String childId = Apexpages.currentPage().getParameters().get('childId');
		examDb = new CExamDb(controller,childId);
		errorMessage = '';
		if (pExam.id == null)
			pageTitle = 'מבחן חדש';
		else
			pageTitle = pExam.name;
		SetbIsScholarship();
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		examDb.save();
		PageReference newCasePage = new ApexPages.StandardController(pExam).view();
        return newCasePage;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CExamValidator validator = new CExamValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public PageReference OnChangeGrade()
    {
    	examDb.CheckGradeWords();
		return null;
    }
    
    public PageReference NewExam()
    {
    	Schema.DescribeSObjectResult pR = Exam__c.sObjectType.getDescribe();
		Pagereference pPage = new ApexPages.StandardController(pExam).view();
    	
    	Pagereference newpage = new Pagereference ('/' + pR.getKeyPrefix() +
		                                            '/e?CF00ND00000063kWT_lkid=' + pExam.ChildName__c + '&retURL=/' + pPage.getUrl());
    	return newpage;
    }
    
    public void SetbIsScholarship()
    {
    	bIsScholarship = false;
    	if (pExam.ChildRegistration__c != null && pExam.ChildRegistration__c != 'a02U0000003qtJ1')
    	{
    		ChildRegistration__c[] childRegistration = [select name, id, IsScholarship__c from ChildRegistration__c where id = :pExam.ChildRegistration__c];
    		if (childRegistration.size() > 0)
    		{
    			if (childRegistration[0].IsScholarship__c)
    				bIsScholarship = true;
    		}
    	}
    }
    
    public void Refersh()
    {
    }
    
    public list<SelectOption> ChildRegistrationList
    {
    	get
    	{
    		return examDb.GetChildRegistrationList();
    	}
    }
}