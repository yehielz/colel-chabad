public with sharing class CMasavFileDb 
{
	public list<CPaymentInMasavFileList> pPayments { get;set; }
	public MasavFile__c pMasavFile { get;set; }
	public boolean IsShowTable { get;set; }
	public boolean IsNewPage { get; set; }
	
	public CMasavFileDb(Apexpages.Standardcontroller controller)
	{
		pMasavFile = (MasavFile__c)controller.getRecord();
		IsNewPage = pMasavFile.id == null;
		SetPageSize();
		SetPageNumber();
		SetIsShowTable();
		SetPaymentsByPaging();
	}
	
	public void Save()
	{
		if (pMasavFile.id == null)
			insert pMasavFile;
		SaveLines();
		if (pMasavFile.id != null)
			update pMasavFile;
	}
	
	public void SaveLines()
	{
		list<Payment__c> toUpdate = new list<Payment__c>();
		for (CPaymentInMasavFileList payment : pPayments)
		{
			if (payment.pIsToSave)
				payment.pPayment.MasavFile__c = pMasavFile.id;
			else
				SetPaymentStatus(payment.pPayment);
			toUpdate.add(payment.pPayment);
		}
		if (toUpdate.size() > 0)
			update toUpdate;
		DeleteOutPayments();
	}
	
	public void DeleteOutPayments()
	{
		list<Payment__c> payments = new list<Payment__c>();
		if (pMasavFile.FromDate__c != null && pMasavFile.ToDate__c != null)
		{
			payments = [select name, id, MasavFile__c, Amount__c, AmountPaidByMasav__c, PaymentTarget__c from Payment__c where MasavFile__c = :pMasavFile.id 
						and (Date__c < :pMasavFile.FromDate__c or Date__c > :pMasavFile.ToDate__c)];
		}
		else
			payments = [select name, id, MasavFile__c, AmountPaidByMasav__c, Amount__c, PaymentTarget__c from Payment__c where MasavFile__c = :pMasavFile.id];
		for (Payment__c payment : payments)
		{
			SetPaymentStatus(payment);
		}
		if (payments.size() > 0)
			update payments;
	}
	
	private void SetPaymentStatus(Payment__c payment)
	{
		payment.MasavFile__c = null;
		payment.Amount__c = payment.Amount__c == null ? 0 : payment.Amount__c;
		payment.AmountPaidByMasav__c = payment.AmountPaidByMasav__c == null ? 0 : payment.AmountPaidByMasav__c;
		payment.Amount__c -= payment.AmountPaidByMasav__c;
		payment.Debt__c = payment.Debt__c == null ? 0 : payment.Debt__c;
		decimal balance = payment.Debt__c - payment.Amount__c;
		payment.AmountPaidByMasav__c = 0;
		integer debt = math.round(payment.Debt__c);
		integer amount = math.round(payment.Amount__c);
		payment.IsDone__c = payment.Amount__c != null && payment.Amount__c > 0;
		if (amount == null || !(amount > 0))
			payment.Status__c = CObjectNames.PaymentStatusNotPaid;
		else if (amount > 0 && amount < debt)
			payment.Status__c = CObjectNames.PaymentStatusHalfPaid;
		else if (amount > 0 && amount >= debt)
			payment.Status__c = CObjectNames.PaymentStatusPaid;
		else
			payment.Status__c = CObjectNames.PaymentStatusNotPaid;
	}
    
    public integer PageNumber { get;set; }
    public integer PageSize { get;set; }
    public boolean IsFirstPage { get;set; }
    public boolean IsLastPage { get;set; }
    
    private void SetPageSize()
    {
        string size = null;
        PageSize = 50;
        if (Apexpages.currentPage().getCookies().containsKey('PSize') != null && Apexpages.currentPage().getCookies().get('PSize') != null)
        {
            size = Apexpages.currentPage().getCookies().get('PSize').getValue();
            if (size != null && size.trim() != '')
            {
                try { PageSize = integer.valueOf(size);  }
                catch(Exception e){ PageSize = 50; }
            }
        }
        size = Apexpages.currentPage().getParameters().get('PSize');
        if (size != null && size.trim() != '')
        {
            try { PageSize = integer.valueOf(size); }
            catch(Exception e){ PageSize = 50; }
        }
    }
    
    private void SetPageNumber()
    {
        string num = null;
        if (Apexpages.currentPage().getCookies().containsKey('PNum') != null && Apexpages.currentPage().getCookies().get('PNum') != null)
        {
            num = Apexpages.currentPage().getCookies().get('PNum').getValue();
            if (num != null && num.trim() != '')
            {
                try { PageNumber = integer.valueOf(num);  }
                catch(Exception e){ PageNumber = 1; }
            }
        }
        num = Apexpages.currentPage().getParameters().get('PNum');
        if (num != null && num.trim() != '')
        {
            try { PageNumber = integer.valueOf(num); }
            catch(Exception e){ PageNumber = 1; }
        }
        IsFirstPage = false;
        IsLastPage = false;
    }
    
    private void SetPaymentsByPaging()
    {
        IsFirstPage = false;
        IsLastPage = false;
        PageNumber = PageNumber == null || PageNumber < 1 ? 1 : PageNumber;
        PageSize = PageSize == null || PageSize < 10 ? 10 : PageSize;
        integer startNumber = (PageNumber * PageSize) - PageSize;
        startNumber = startNumber < 0 ? 0 : startNumber;
        list<Payment__c> newList = GetPaymentsByRange(pMasavFile.FromDate__c, pMasavFile.ToDate__c, startNumber, PageSize);
        pPayments = new list<CPaymentInMasavFileList>();
        for (Payment__c payment : newList)
        {
        	if ((payment.MasavFile__c == null && IsNewPage) || (payment.MasavFile__c != null && !IsNewPage))
        		pPayments.add(new CPaymentInMasavFileList(payment, true));
        	else
        		pPayments.add(new CPaymentInMasavFileList(payment, false));
        }
        PageNumber = PageNumber < 1 ? 1 : PageNumber;
        if (PageNumber == 1)
            IsFirstPage = true;
        if (pPayments.size() < PageSize)
            IsLastPage = true;
        SetCookies();
    }
    
    private void SetCookies()
    {
        Cookie PSizeCookie = new Cookie('PSize', string.valueOf(PageSize), null, -1, false);
        Cookie PNumCookie = new Cookie('PNum', string.valueOf(PageNumber), null, -1, false);
        ApexPages.currentPage().setCookies(new list<Cookie> { PSizeCookie, PNumCookie } );
    }
    
    public void OnChangePage()
    {
        Save();
        SetPaymentsByPaging();
    }
    
    public void SetLinesByPreviousPage()
    {
        PageNumber--;
        OnChangePage();
    }
    
    public void SetLinesByNextPage()
    {
        PageNumber++;
        OnChangePage();
    }
    
    public void SetIsShowTable()
    {
    	if (pMasavFile.FromDate__c != null && pMasavFile.ToDate__c != null)
    		IsShowTable = true;
    	else
    		IsShowTable = false;
    	SetPaymentsByPaging();
    }
	
	private list<Payment__c> GetPaymentsByRange(date fromDate, date toDate, integer fromIndex, integer length)
	{
		if (fromDate != null && toDate != null)
		{
			return [SELECT Name, Id, MasavFile__c, IsDone__c, FamilyOrTutor__c, Comment__c, Debt__c, TutorName__c, Balance__c, NatureOfPayment__c, 
						Status__c, FamilyName__c, Amount__c, ParentName__c, City__c, Date__c, FamilyName__r.name, TutorName__r.name, MasavFile__r.name,
						AmountPaidByMasav__c, PaymentTarget__c 
					FROM Payment__c 
					WHERE Date__c >= :fromDate 
					AND Date__c <= :toDate 
					AND ((Balance__c != null AND Balance__c > 0 AND MasavFile__c = null) 
						OR (MasavFile__c != null and MasavFile__c = :pMasavFile.id)
						) 
					ORDER BY Date__c 
					LIMIT :length 
					OFFSET :fromIndex];
		}
		else
			return new list<Payment__c>();
	}
}