public with sharing class CActivityApprovalFacad 
{
    public string pageTitle { get; set; }
    public string errorMessage { get; set; }
    public boolean bWeeklyHours { get; set; }
    public boolean bMeetings { get; set; }
    public boolean bTutor { get; set; }
    public boolean bAmountApproved { get; set; }
    public boolean bOtherProfession { get; set; }
    public boolean hasSameTypeApproval {get;set;}
    
    private CActivityApprovalDb activityApprovalDb;
    
    public ActivityApproval__c pActivityApproval
    {
        get
        {
            return activityApprovalDb.GetpActivityApproval();
        }
    }
    
    public boolean IsNewApproval
    {
        get
        {
            return pActivityApproval.id == null;
        }
    }
        
    public CActivityApprovalFacad(ApexPages.StandardController controller)
    {
        activityApprovalDb = new CActivityApprovalDb(controller);
        hasSameTypeApproval = CActivityApprovalDb.GetActivityApprovalForSameRequestType(pActivityApproval, pApprovalRequest.ProfessionName__r.ProfessionTypeName__r.Id);
        errorMessage = '';
        if (pActivityApproval.id == null){
            pageTitle = 'אישור פעילות חדש';
            pActivityApproval.SendEmailToFamily__c = true;
        }
        else{
            pageTitle = pActivityApproval.name;
        }
        SetBooleans();
        
    }
    
    public PageReference save()
    {
        if (!Validation())
            return null;
        
        activityApprovalDb.save();
        PageReference newCasePage = new ApexPages.StandardController(pActivityApproval).view();
        return newCasePage;
    }

    @AuraEnabled
    public static void SMSsend(string idObj)
    {
        ActivityApproval__c act = [SELECT ProfessionName__r.Name ,ChildName__r.Name,ChildName__r.FamilyName__r.MobilePhone__c,ChildName__r.FamilyName__r.ParentName__c,ChildName__r.FamilyName__r.Name FROM ActivityApproval__c where id=:idObj];
        Utils_SMS.SMS_Request req = Utils_SMS.buildApprovalSMS(act);	
		List<String> results = Utils_SMS.sendSms( new List<Utils_SMS.SMS_Request> {req});
        system.debug(act.ChildName__r.FamilyName__r.MobilePhone__c);
    }
    
    
    public PageReference sendSMS()    {
    	// Utils_SMS.SMS_Request req = Utils_SMS.buildApprovalSMS(pActivityApproval);
    	Utils_SMS.SMS_Request req = Utils_SMS.InfosConnexionsBySMS(pActivityApproval);
		List<String> results = Utils_SMS.sendSms( new List<Utils_SMS.SMS_Request> {req});
        String noPhoneNumber = 'No valid recipients';
        String successSendSMS = 'Message accepted successfully';
        if( results[0].indexOf(noPhoneNumber) != -1){
            results[0] = 'אין מספר טלפון / חונך לשלוח את הפרטים';
        }
        if( results[0].indexOf(successSendSMS) != -1){
            results[0] = 'נשלח בהצלחה';
        }
		ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, results[0], ''));
        return null;
    }

    public PageReference sendEmail()    {
    	pActivityApproval.sendEmailButtonTutor__c = true;
    	save();
        return null;
    }
    
    private boolean Validation()
    {
        boolean ret = true;
        CActivityApprovalValidator validator = new CActivityApprovalValidator(this);
        ret = validator.Validate();
        if (!ret)
            errorMessage = validator.ErrorMessage;
        
        return ret;
    }
    
    private void SetBooleans()
    {
        RequestForTreatment__c requestForTreatment = CRequestForTreatmentDb.GetRequestForTreatmentWithProfessioNameById(pActivityApproval.RequestForTreatmentName__c);
        bWeeklyHours = true;
        bMeetings = true;
        // Boolean bTutor = true;

        System.debug('requestForTreatment.WeeklyHours__c => ' + requestForTreatment.WeeklyHours__c);
        System.debug('requestForTreatment.MeetingsNumber__c => ' + requestForTreatment.MeetingsNumber__c);
        System.debug('requestForTreatment.id => ' + requestForTreatment.id);
        if (requestForTreatment.WeeklyHours__c == null)
            bWeeklyHours = false;
        if (requestForTreatment.MeetingsNumber__c == null)
            bMeetings = false;
        bAmountApproved = !bMeetings && !bWeeklyHours;

        if(RequestForTreatment.TutorName__c == null)
            bTutor = false;
        if(RequestForTreatment.TutorName__c != null)
            bTutor = true;
        // bAmountApproved = !bTutor;

        bOtherProfession = false;
        if (requestForTreatment.ProfessionName__r.name == CObjectNames.Other)
            bOtherProfession = true;
    }
    
    public list<CObjectsHistory> pActivityApprovalHistories
    {
        get
        {
            return activityApprovalDb.GetActivityApprovalHistories();
        }
    }  
    
    public PageReference OnChangeTutor()
    {
        activityApprovalDb.SetRateByTutor();
        return null;
    }
    
    public PageReference SaveAndGoToNextRequest()
    {
        activityApprovalDb.save();
        return GoToNextRequest();
    }
    
    public PageReference GoToNextRequest()
    {
        if (pApprovalRequest != null)
        {
            RequestForTreatment__c[] requests = [select name, CaseName__c from RequestForTreatment__c where CreatedDate > :pApprovalRequest.CreatedDate
                                                 and id != :pActivityApproval.RequestForTreatmentName__c and (RequestStatus__c = 
                                                 :CObjectNames.RequestForTreatmentStatusNew or RequestStatus__c = :CObjectNames.RequestForTreatmentStatusTreatment) 
                                                 order by CreatedDate];
            if (requests.size() > 0)
            {
                PageReference requestPage = new ApexPages.StandardController(requests[0]).view();
                return requestPage;
            }
        }
        PageReference newCasePage = new ApexPages.StandardController(pActivityApproval).view();
        return newCasePage;
    }
    
    public string printUrl{ 
        get
        { 
            return CObjectNames.getBaseUrl() + page.ActivityApprovalToPrint.getUrl() + '?activityApprovalID=' + pActivityApproval.id; 
        } 
    }
    public string printLayout{ 
        get
        { 
            return CObjectNames.getBaseUrl() + page.ActivityApprovalPrintLayout.getUrl() + '?ID=' + pActivityApproval.id; 
        } 
    }
    
    public RequestForTreatment__c pApprovalRequest
    {
        get
        {
            System.debug(activityApprovalDb.pApprovalRequest);
            if(activityApprovalDb.pApprovalRequest != null)
                return activityApprovalDb.pApprovalRequest;
            else {
                return null;
            }
        }
    }
    
  /*public integer fromIndex { get; set; }
    public integer toIndex { get; set; }
    public void tempToUpdateReports()
    {
        ActivityReport__c[] reports = [select name, id from ActivityReport__c order by CreatedDate desc];
        fromIndex = fromIndex == null ? 0 : fromIndex;
        toIndex = toIndex == null ? 0 : toIndex;
        toIndex = toIndex > reports.size() ? reports.size() : toIndex;
        list<ActivityReport__c> reportsToUpdate = new list<ActivityReport__c>();
        for (integer i = fromIndex; i < toIndex; i++)
        {
            reportsToUpdate.add(reports[i]);
            if (reportsToUpdate.size() > 80)
            {
                update reportsToUpdate;
                reportsToUpdate = new list<ActivityReport__c>();
            }
        }
        if (reportsToUpdate.size() > 0)
            update reportsToUpdate;
    }*/
}