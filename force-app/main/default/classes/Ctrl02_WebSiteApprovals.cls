public without sharing class Ctrl02_WebSiteApprovals {

    private SessionID__c sessionId ;    
    public Tutor__c tutor { get; set; }
    public Family__c family { get; set; }
    public List<ActivityApproval__c> approvals {get;set;}
    /*public list<selectOption> monthesOptions{ get{ return CObjectNames.monthesOptions; } }
    public list<selectOption> yearsOptions{ get{ return CObjectNames.yearsOptions; } }
    public string fromMonthM{ get{ return webSiteReportsDisplayDb.fromMonthM; } set{ webSiteReportsDisplayDb.fromMonthM = value; } }
    public string fromMonthY{ get{ return webSiteReportsDisplayDb.fromMonthY; } set{ webSiteReportsDisplayDb.fromMonthY = value; } }
    public string toMonthM{ get{ return webSiteReportsDisplayDb.toMonthM; } set{ webSiteReportsDisplayDb.toMonthM = value; } }
    public string toMonthY{ get{ return webSiteReportsDisplayDb.toMonthY; } set{ webSiteReportsDisplayDb.toMonthY = value; } }
    
    private CWebSiteReportsDisplayDb webSiteReportsDisplayDb;*/
    
    public Ctrl02_WebSiteApprovals(){
        
        /*webSiteReportsDisplayDb = new CWebSiteReportsDisplayDb();
        fromMonthM = string.valueOf(datetime.now().month());
        toMonthM = string.valueOf(datetime.now().month());
        if (string.valueOf(datetime.now().month()).length() < 2)
        {
            fromMonthM = '0' + string.valueOf(datetime.now().month());
            toMonthM = '0' + string.valueOf(datetime.now().month());
        }
        fromMonthY = string.valueOf(datetime.now().year());
        toMonthY = string.valueOf(datetime.now().year());*/
    }
    public PageReference init() {
        if(ValidateIsOnline()){
            tutor = CTutorDb.GetTutorById(sessionId.TutorName__c);
            family = CFamilyDb.GetFamilyById(sessionId.Family__c);
            setApprovals();
            return null;
        }
        else 
            return new PageReference(CObjectNames.WebSitePageLogIn);
    }
    
    private boolean ValidateIsOnline(){
        if (ApexPages.currentPage().getCookies().get('sessionId') != null){
        	system.debug('\n\n\n ApexPages.currentPage().getCookies().get(\'sessionId\')' + ApexPages.currentPage().getCookies().get('sessionId') +'\n\n\n\n ');
            sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
            if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
                return true;
        }
        return false;
    }
    
    private void setApprovals() { 
        approvals = new list<ActivityApproval__c>();
        ActivityApproval__c[] lst = 
               [Select c.WeeklyHours__c, c.ToDate__c, c.Rate__c, c.ProfessionName__r.Name, c.Note__c, c.MeetingsNumber__c, c.FromDate__c,
                c.Fare__c, c.ChildName__r.Name, c.ApprovalDate__c, c.AmountSpent__c, c.AmountApproved__c, c.Name, c.Id ,
                ProfessionName__r.ProfessionTypeName__r.Name,HoursInSequence__c
                From ActivityApproval__c c
                where ((TutorName__c != null and TutorName__c = :sessionId.TutorName__c) or ChildName__r.FamilyName__c = :sessionId.Family__c) 
                //and SchoolYear__c = :CSchoolYearDb.GetActiveYearId()
                order by FromDate__c];
        
        for (ActivityApproval__c ap : lst)  
            if (validateIsToAdd(ap))    approvals.add(ap);
    }
    
    private boolean validateIsToAdd(ActivityApproval__c approval)   {
        /*if (fromMonthY == null || fromMonthM == null || toMonthY == null || toMonthM == null)
            return true;
        if (activityReport.ReportMonth__c == null)
            return true;
        integer month = integer.valueOf(approval.ReportMonth__c.split('-')[0]);
        integer year = integer.valueOf(approval.ReportMonth__c.split('-')[1]);
        if (year < integer.valueOf(fromMonthY) || year > integer.valueOf(toMonthY))
            return false;
        if (year == integer.valueOf(fromMonthY) && month < integer.valueOf(fromMonthM))
            return false;
        if (year == integer.valueOf(toMonthY) && month > integer.valueOf(toMonthM))
            return false;*/
        return true;
    }
    
}