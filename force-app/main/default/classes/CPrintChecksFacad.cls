public with sharing class CPrintChecksFacad 
{
	public list<CCheckToPrint> pChecks { get; set; }
	public Payment__c pPayment { get; set; }
	public integer pChecksNum{ get{ return pChecks.size(); } }
	public list<CProfessionInfoInPrintPayment> pProfessions { get; set; }
	
	public CPrintChecksFacad()
	{
		SetPayment();
		SetChecks();
		SetProfessions();
	}
	
	private void SetPayment()
	{
		string id = Apexpages.currentPage().getParameters().get('PId');
		if (id != null && id.trim() != '')
		{
			list<Payment__c> payments = CPaymentDb.GetPaymentsByIds(new list<string> { id });
			if (payments.size() > 0)
				pPayment = payments[0];
		}
		if (pPayment == null)
			pPayment = new Payment__c();
	}
	
	private void SetChecks()
	{
		pChecks = new list<CCheckToPrint>();
		if (pPayment.id != null)
		{
			Check__c[] checks = CCheckDb.GetChecksByPayment(pPayment.id);
			for (Check__c check : checks)
			{
				pChecks.add(new CCheckToPrint(check, pChecks.size() + 1));
			}
		}
	}
	
	private void SetProfessions()
	{
		pProfessions = new list<CProfessionInfoInPrintPayment>();
		if (pPayment.id != null)
		{
			map<string, decimal> professionsMap = new map<string, decimal>(); 
			professionsMap = SetProfessionsMapFromReports(professionsMap);
			professionsMap = SetProfessionsMapFromActivities(professionsMap);
			list<string> profesionsList = new list<string>();
			profesionsList.addAll(professionsMap.KeySet());
			profesionsList.sort();
			for (string profession : profesionsList)
			{
				pProfessions.add(new CProfessionInfoInPrintPayment(profession, professionsMap.get(profession)));
			}
		}
	}
	
	private map<string, decimal> SetProfessionsMapFromReports(map<string, decimal> professionsMap)
	{
		AggregateResult[] result = [select ProfessionName__r.name profession, sum(TotalPayment__c) totalPayment from ActivityReport__c where TotalPayment__c != null 
									and PaymentName__c = :pPayment.id group by ProfessionName__r.name];
		for (AggregateResult res : result)
		{
			string profession = string.valueOf(res.get('profession'));
			decimal amount = (decimal)res.get('totalPayment');
			amount = amount == null ? 0 : amount;
			if (professionsMap.containsKey(profession))
				amount += professionsMap.get(profession);
			professionsMap.put(profession, amount);
		}
		return professionsMap;
	}
	
	private map<string, decimal> SetProfessionsMapFromActivities(map<string, decimal> professionsMap)
	{
		AggregateResult[] result = [select SpecialActivitiesName__r.ActivityDescription__c description, sum(PaymentAmount__c) paymentAmount from SpecialActivitiesLine__c where PaymentAmount__c != null 
									and PaymentName__c = :pPayment.id group by SpecialActivitiesName__r.ActivityDescription__c];
		for (AggregateResult res : result)
		{
			string description = string.valueOf(res.get('description'));
			decimal amount = (decimal)res.get('paymentAmount');
			amount = amount == null ? 0 : amount;
			if (professionsMap.containsKey(description))
				amount += professionsMap.get(description);
			professionsMap.put(description, amount);
		}
		return professionsMap;
	}
	
	public string Payable
	{ 
		get
		{
			if (pPayment.PaymentTarget__c == 'חונך' && pPayment.TutorName__c != null)
				return CObjectNames.getHebrewString(pPayment.TutorName__r.name);
			else if (pPayment.FamilyName__c != null)
				return CObjectNames.getHebrewString(pPayment.FamilyName__r.ParentName__c + ' ' + pPayment.FamilyName__r.name);
			return '';
		} 
	}
	
	public string sPayemntAmount{ get{ return GetFormat(pPayment.Amount__c); } }
	public string sPayemntName{ get{ return CObjectNames.getHebrewString(pPayment.name); } }
	public string sToDayDate{ get{ return CObjectNames.GetDateStr(datetime.now().Date()); } }
	
	public string GetFormat(decimal num)
	{
		if (num != null)
		{
			string numStr = num.format();
			system.debug('numStr = '+ numStr);
			if (numStr.contains('.'))
			{
				string afterPoint = numStr.split('\\.')[1];
				system.debug('afterPoint = '+ afterPoint);
				if (afterPoint.length() < 2)
					return numStr + '0';
				else
					return numStr;
			}
			else
				return numStr + '.00';
		}
		return '0.00';
	}
}