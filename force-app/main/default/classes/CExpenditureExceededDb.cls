public with sharing class CExpenditureExceededDb 
{
	public ExpenditureExceeded__c pExpenditureExceeded { get; private set; }
	
	public CExpenditureExceededDb()
	{
		pExpenditureExceeded = new ExpenditureExceeded__c();
	}
	
	public CExpenditureExceededDb(ApexPages.StandardController controller)
	{
		pExpenditureExceeded = (ExpenditureExceeded__c)controller.getRecord();
		if (pExpenditureExceeded.id != null)
			SetExpenditureExceededById(pExpenditureExceeded.id);
	}
	
	private void SetExpenditureExceededById(string id)
	{
		pExpenditureExceeded = [select name , id, ActivityReportName__c, OneTime__c, Amount__c, ActivityReportName__r.ActivityReportStatus__c, Details__c
								from ExpenditureExceeded__c where id = :id];
	}
	
	public void save()
	{
		if (pExpenditureExceeded.id == null)
			insert pExpenditureExceeded;
		else
			update pExpenditureExceeded;
	}
}