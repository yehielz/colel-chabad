public with sharing class CMessageFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	public boolean bIsInReadBy { get; set; }
	public boolean bIsNotInReadBy { get; set; }
	
	private CMessagesDb messageDb;
	
	public string addToReadByButtonName
	{
		get
		{
			return 'קראתי';
		}
	}
	
	public string removeFromReadByButtonName
	{
		get
		{
			return 'לא קראתי';
		}
	}
	
	public Message__c pMessage
	{
		get
		{
			return messageDb.GetpMessage();
		}
	}
		
	public CMessageFacad(ApexPages.StandardController controller)
	{
		messageDb = new CMessagesDb(controller);
		errorMessage = '';
		if (pMessage.id == null)
			pageTitle = 'הודעה חדשה';
		else
			pageTitle = pMessage.name;
		SetBooleans();
	}
	
	
    
    private void SetBooleans()
    {
    	if (pMessage.ReadBy__c != null && pMessage.ReadBy__c.contains(system.userInfo.getName()))
    	{
    		bIsNotInReadBy = true;
    		bIsInReadBy = false;
    	}
    	else
    	{
    		bIsNotInReadBy = false;
    		bIsInReadBy = true;
    	}
    }
    
    public PageReference onAddToReadBy()
    {
    	messageDb.onAddToReadBy();
    	SetBooleans();
    	return null;
    }
    
    public PageReference onRemoveFromReadBy()
    {
    	messageDb.onRemoveFromReadBy();
    	SetBooleans();
    	return null;
    }
    
    //========================================== view
    
    public string notReadOrAll { get; set; }
    public List<CMessageInViewPage> pMessagesForView { get; set; }
	
	public CMessageFacad()
	{
		messageDb = new CMessagesDb();
		OnChangeList();
	}
    
    public PageReference OnChangeList()
    {
    	list<CMessageInViewPage> messagesForView = messageDb.GetpMessagesForView(notReadOrAll);
    	pMessagesForView = setIndex(messagesForView);
    	return null;
    }
    
    private list<CMessageInViewPage> setIndex(list<CMessageInViewPage> messagesForView)
    {
    	for (integer i = 0; i < messagesForView.size(); i++)
    	{
    		messagesForView[i].pMessage.index__c = i;
    	}
    	return messagesForView;
    }
    
    private integer getIndex(string paramName)
    {
    	if (!Test.isRunningTest())
    	{
    		string s = System.currentPageReference().getParameters().get(paramName);
    		return integer.valueOf(s);
    	}
    	else
    		return 0;
    }
    
    public PageReference onChangeToRead()
    {
    	integer index = getIndex('selectedLineToRead');
    	pMessagesForView[index].pMessage.ReadBy__c = messageDb.getReadByWithAddUser(pMessagesForView[index].pMessage.ReadBy__c);
    	update pMessagesForView[index].pMessage;
    	return null;
    }
    
    public PageReference onChangeToNotRead()
    {
    	integer index = getIndex('selectedLineToNotRead');
    	pMessagesForView[index].pMessage.ReadBy__c = messageDb.getReadByWithRemoveUser(pMessagesForView[index].pMessage.ReadBy__c);
    	update pMessagesForView[index].pMessage;
    	return null;
    }
    
    public List<SelectOption> notReadOrAllOptions
    {
    	get
    	{
    		return new List<SelectOption>{ new selectOption('All', 'הכל'), new selectOption('NotRead', 'לא נקרא') };
    	}
    }
}