public with sharing class CCoordinatorDb
{
	private Coordinator__c pCoordinator { get; set; }
	
	public CCoordinatorDb()
	{
		pCoordinator = new Coordinator__c();
	}
	
	public CCoordinatorDb(ApexPages.StandardController controller)
	{
		pCoordinator = (Coordinator__c)controller.getRecord();
		if (pCoordinator.id != null)
			SetpCoordinatorBypCoordinatorId(pCoordinator.id);
	}
	
	private void SetpCoordinatorBypCoordinatorId(string id)
	{
		pCoordinator = [select name, Region__c, WorkPhone__c, MobilePhone__c, Address__c, BirthDate__c, UserName__c,
						Email__c, IdNumber__c, LastName__c, FirstName__c, id from Coordinator__c where id = :id];
	}
	
	public void save()
	{
		pCoordinator.name = pCoordinator.LastName__c + ' ' + pCoordinator.FirstName__c;
		if (pCoordinator.id == null)
			insert pCoordinator;
		else
			update pCoordinator;
	}
	
	public Coordinator__c GetCoordinator()
	{
		return pCoordinator;
	}
}