public with sharing class CDiplomaToPrintFacad 
{
	public Diploma__c pDiploma { get; set; }
	public list<DiplomaLine__c> pDiplomaLines { get; set; }
	public list<Note> pNotesInDiploma { get; set; }
	public string sNotes { get; set; }
	public string sDiplomaLines { get; set; }
	
	public CDiplomaToPrintFacad()
	{
		SetpDiploma();
		SetpDiplomaLines();
		SetpNotesInDiploma();
		sNotes = GetNotesHtml();
		sDiplomaLines = GetDiplomaLinesHtml();
	} 
	
	private string GetDiplomaLinesHtml()
	{
		string ret = '';
		if (pDiplomaLines.size() > 0)
		{
			ret += '<table class="diplomaLinesTable">' +
							'<tr>' + 
								'<th style="width: 500px;">' +
									CObjectNames.getHebrewString('הערה') +
								'</th>' +
								'<th style="width: 20%;">' +
									CObjectNames.getHebrewString('ציון במילים') +
								'</th>' +
								'<th style="width: 10%;">' +
									CObjectNames.getHebrewString('ציון') +
								'</th>' +
								'<th style="width: 20%;">' +
									CObjectNames.getHebrewString('מקצוע') +
								'</th>' +
								
							'</tr>';
			for (DiplomaLine__c line : pDiplomaLines)
			{
				
				ret += '<tr>' + 
							'<td style="width: 500px;" > <span>';
							
								if (line.Note__c != null  ){
									
									system.debug('\n\n line.Note__c.length()' + line.Note__c.length() + '\n\n\n\n');
				system.debug('\n\n line.Note__c' + line.Note__c + '\n\n\n\n');
									//ret += line.Note__c;
									Integer size = line.Note__c.length() ;
										system.debug('\n\n size' + size + '\n\n\n\n');
									while (size - 30 > 0) {
										system.debug('\n\n size' + size + '\n\n\n\n');
										Integer spaceIndex = 30;
										while (line.Note__c.length() > spaceIndex && line.Note__c.charAt(spaceIndex) != 32 ) {
											spaceIndex ++;
										}
										if(spaceIndex > line.Note__c.length())
											spaceIndex = 30;
										ret += CObjectNames.getHebrewString(line.Note__c.substring(0,spaceIndex));
										ret += '<br/>';
										size = size - spaceIndex;	
										line.Note__c = line.Note__c.substring(spaceIndex,line.Note__c.length());
										if(size < 30 )
											break;
										
										system.debug('\n\n size' + size + '\n\n\n\n');
										
									}
									ret += CObjectNames.getHebrewString(line.Note__c.substring(0,size));
									
									//ret += CObjectNames.getHebrewString(line.Note__c);
								}
								
							ret += '</span></td>' + 
							'<td style="width: 20%;">';
								if (line.GradeInWords__c != null)
									ret += CObjectNames.getHebrewString(line.GradeInWords__c);
							ret += '</td>' +
							'<td style="width: 10%;">';
								if (line.Grade__c != null)
									ret += CObjectNames.getHebrewString(string.valueOf(line.Grade__c));
							ret += '</td>' +
							'<td style="width: 20%;">';
								if (line.ProfessionName__c != null && line.ProfessionName__r.name != null)
									ret += CObjectNames.getHebrewString(line.ProfessionName__r.name);
							ret += '</td>' + 
							
						'</tr>';
			}
		}
		return ret;
	}
	
	private string GetNotesHtml()
	{
		string ret = '';
		if (pNotesInDiploma.size() > 0)
		{
			ret += '<table class="NotesTable">' +
							'<tr>' + 
								'<th style="width: 65%; border-right: 1px solid black;">' +
									CObjectNames.getHebrewString('תוכן') +
								'</th>' +
								'<th>' + 
									CObjectNames.getHebrewString('נושא') +
								'</th>' +
							'</tr>';
			for (Note n : pNotesInDiploma)
			{
				ret += '<tr>' + 
							'<td style="font-size: 12pt; border-right: 1px solid black;">' +
								GetNote(n.Body) +
							'</td>' +
							'<td>' + 
								GetNote(n.Title) +
							'</td>' +
						'</tr>';
			}
		}
		return ret;
	}
	
	private void SetpDiploma()
	{
		if (Apexpages.currentPage().getParameters().get('id') != null && Apexpages.currentPage().getParameters().get('id') != '')
			pDiploma = CDiplomaDb.GetDiplomaById(Apexpages.currentPage().getParameters().get('id'));
		if (pDiploma == null)
			pDiploma = new Diploma__c();
	}
	
	private void SetpDiplomaLines()
	{
		if (pDiploma.id != null)
			pDiplomaLines = CDiplomaLineDb.GetDiplomaLineByDiplomaId(pDiploma.id);
		if (pDiplomaLines == null)
			pDiplomaLines = new list<DiplomaLine__c>();
	}
	
	private void SetpNotesInDiploma()
	{
		if (pDiploma.id != null)
			pNotesInDiploma = [select ParentId, Body, Title from Note where ParentId = :pDiploma.id];
		if (pNotesInDiploma == null)
			pNotesInDiploma = new list<Note>();
	}
	
	public string DiplomaName
	{
		get
		{
			if (pDiploma.name != null)
				return CObjectNames.getHebrewString('תעודה: ' + pDiploma.name);
			return '';
		}
	}
	
	public string Grade
	{
		get
		{
			if (pDiploma.Grade__c != null)
				return CObjectNames.getHebrewString('כיתה: ' + pDiploma.Grade__c);
			return '';
		}
	}
	
	public string ChildName
	{
		get
		{
			if (pDiploma.ChildName__c != null && pDiploma.ChildName__r.name != null)
				return CObjectNames.getHebrewString('שם ילד: ' + pDiploma.ChildName__r.name);
			return '';
		}
	}
	
	public string SchoolYearName
	{
		get
		{
			if (pDiploma.SchoolYearName__c != null && pDiploma.SchoolYearName__r.name != null)
				return CObjectNames.getHebrewString('שנת למודים: ' + pDiploma.SchoolYearName__r.name);
			return '';
		}
	}
	
	public string Half
	{
		get
		{
			if (pDiploma.Half__c != null)
				return CObjectNames.getHebrewString('מחצית: ' + pDiploma.Half__c);
			return '';
		}
	}
	
	public string GeneralAverage
	{
		get
		{
			if (pDiploma.Average__c != null)
				return CObjectNames.getHebrewString('ממוצע כללי: ' + pDiploma.Average__c);
			return '';
		}
	}
	
	public string DiplomaDate
	{
		get
		{
			if (pDiploma.Date__c != null)
				return pDiploma.Date__c.day() + '/' + pDiploma.Date__c.month() + '/' + pDiploma.Date__c.year() + ' ' + CObjectNames.getHebrewString('תאריך תעודה: ');
			return '';
		}
	}
	
	private string GetNote(string thenote)
	{
		string ret = '';
		if (thenote != null)
		{
			string note = ' ' + thenote;
			for (integer i = 53; i < note.length(); i=53)
			{
				integer fromInSubS = 53;
				integer minus = 0;
				while(note.substring(i-minus, ((i-minus)+1)) != ' ')
					minus++;
				i = i - minus;
				ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i)) + '<br/>';
				note = note.substring(i, note.length());
			}
			ret += '.' + CObjectNames.getHebrewString(note);
		}
		return ret;
	}
}