public with sharing class CAmountIntoWordsConverter 
{
	private static decimal pAmount { get;set; }
	
	public static string Convert(decimal amount)
	{
		SetAmount(amount);
		string words = GetBillionsStr();
		words += ' ' + GetMillionsStr();
		words += ' ' + GetThousandsStr();
		words += ' ' + GetHundredsStr();
		words += ' ' + GetScoresStr();
		words = SetAndInEndOfWords(words.trim());
		words += ' ש"ח';
		if (pAmount > 0)
			words += ' ' + GetCentsStr();
		words += ' בלבד';
		return words.trim();
	}
	
	private static string SetAndInEndOfWords(string words)
	{
		string[] wordsList = words.split(' ');
		integer palce = wordsList.size() - 1;
		string lastWord = wordsList[palce];
		while (palce >= 0 && (lastWord == 'מאות' || lastWord == 'אלפים' || lastWord == 'מיליון' || lastWord == 'מיליארד'))
		{
			palce--;
			lastWord = wordsList[palce];
		}
		if (palce > 0 && !lastWord.trim().startsWith('ו'))
			wordsList[palce] = 'ו' + lastWord;
		string ret = '';
		for (string word : wordsList)
		{
			ret += ' ' + word;
		}
		return ret.trim();
	}
	
	private static void SetAmount(decimal amount)
	{
		pAmount = amount;
		if (pAmount == null || pAmount < 0)
			pAmount = 0;
	}
	
	
	private static integer pQuantity { get;set; }
	private static string GetBillionsStr()
	{
		pQuantity = GetNumQuantity(1000000000);
		string ret = '';
		if (pQuantity > 0)
			ret += GetQuantityStr() + ' מיליארד';
		return ret.trim();
	}
	
	private static string GetMillionsStr()
	{
		pQuantity = GetNumQuantity(1000000);
		string ret = '';
		if (pQuantity > 0)
			ret += GetQuantityStr() + ' מיליון';
		return ret.trim();
	}
	
	private static string GetThousandsStr()
	{
		pQuantity = GetNumQuantity(1000);
		string ret = '';
		if (pQuantity > 0)
			ret += GetQuantityStrForThousands();
		return ret.trim();
	}
	
	private static string GetHundredsStr()
	{
		pQuantity = GetNumQuantity(100);
		string ret = '';
		if (pQuantity > 1)
			ret += GetQuantityStr() + ' מאות';
		else if (pQuantity == 1)
			ret += ' מאה';
		return ret.trim();
	}
	
	private static string GetScoresStr()
	{
		string ret = DoScoresInAmount();
		ret += ' ' + DoUnitsInAmount(ret);
		return ret.trim();
	}
	
	private static string GetCentsStr()
	{
		integer cents = integer.valueOf((pAmount * 100).round());
		cents = cents > 100 ? 99 : cents;
		return '+ ' + cents + ' אג\'';
	}
	
	private static string GetQuantityStr()
	{
		string ret = '';
		if (pQuantity > 1)
		{
			if (pQuantity >= 20)
			{
				ret += DoHundredsInQuantity();
				ret += ' ' + DoScoresInQuantity();
				ret += ' ' + DoUnitsInQuantity(ret.trim());
			}
			else
			{
				ret += DoUnitsInQuantity(ret.trim());
			}
		}
		return ret.trim();
	}
	
	private static string DoHundredsInQuantity()
	{
		string ret = '';
		if (pQuantity >= 900)
		{
			ret += ' תשעה מאות';
			pQuantity -= 900;
		}
		else if (pQuantity >= 800)
		{
			ret += ' שמונה מאות';
			pQuantity -= 800;
		}
		else if (pQuantity >= 700)
		{
			ret += ' שבע מאות';
			pQuantity -= 700;
		}
		else if (pQuantity >= 600)
		{
			ret += ' שש מאות';
			pQuantity -= 600;
		}
		else if (pQuantity >= 500)
		{
			ret += ' חמש מאות';
			pQuantity -= 500;
		}
		else if (pQuantity >= 400)
		{
			ret += ' ארבע מאות';
			pQuantity -= 400;
		}
		else if (pQuantity >= 300)
		{
			ret += ' שלוש מאות';
			pQuantity -= 300;
		}
		else if (pQuantity >= 200)
		{
			ret += ' מאתיים';
			pQuantity -= 200;
		}
		else if (pQuantity >= 100)
		{
			ret += ' מאה';
			pQuantity -= 100;
		}
		return ret.trim();
	}
	
	private static string DoScoresInQuantity()
	{
		string ret = '';
		if (pQuantity >= 90)
		{
			ret += ' תשעים';
			pQuantity -= 90;
		}
		else if (pQuantity >= 80)
		{
			ret += ' שמונים';
			pQuantity -= 80;
		}
		else if (pQuantity >= 70)
		{
			ret += ' שבעים';
			pQuantity -= 70;
		}
		else if (pQuantity >= 60)
		{
			ret += ' שישים';
			pQuantity -= 60;
		}
		else if (pQuantity >= 50)
		{
			ret += ' חמישים';
			pQuantity -= 50;
		}
		else if (pQuantity >= 40)
		{
			ret += ' ארבעים';
			pQuantity -= 40;
		}
		else if (pQuantity >= 30)
		{
			ret += ' שלושים';
			pQuantity -= 30;
		}
		else if (pQuantity >= 20)
		{
			ret += ' עשרים';
			pQuantity -= 20;
		}
		return ret.trim();
	}
	
	private static string DoScoresInAmount()
	{
		string ret = '';
		if (pAmount >= 90)
		{
			ret += ' תשעים';
			pAmount -= 90;
		}
		else if (pAmount >= 80)
		{
			ret += ' שמונים';
			pAmount -= 80;
		}
		else if (pAmount >= 70)
		{
			ret += ' שבעים';
			pAmount -= 70;
		}
		else if (pAmount >= 60)
		{
			ret += ' שישים';
			pAmount -= 60;
		}
		else if (pAmount >= 50)
		{
			ret += ' חמישים';
			pAmount -= 50;
		}
		else if (pAmount >= 40)
		{
			ret += ' ארבעים';
			pAmount -= 40;
		}
		else if (pAmount >= 30)
		{
			ret += ' שלושים';
			pAmount -= 30;
		}
		else if (pAmount >= 20)
		{
			ret += ' עשרים';
			pAmount -= 20;
		}
		return ret.trim();
	}
	
	private static string DoUnitsInQuantity(string privRet)
	{
		string ret = '';
		if (pQuantity > 10)
			ret += DoMoreThanTenInQuantity();
		else if (pQuantity <= 10)
			ret += DoLessThanTenInQuantity();
		ret = ret.trim();
		if (privRet != '' && ret != '')
			ret = 'ו' + ret;
		return ret;
	}
	
	private static string DoMoreThanTenInQuantity()
	{
		string ret = '';
		if (pQuantity >= 19)
		{
			ret += ' תשע-עשרה';
			pQuantity -= 19;
		}
		else if (pQuantity >= 18)
		{
			ret += ' שמונה-עשרה';
			pQuantity -= 18;
		}
		else if (pQuantity >= 17)
		{
			ret += ' שבע-עשרה';
			pQuantity -= 17;
		}
		else if (pQuantity >= 16)
		{
			ret += ' שש-עשרה';
			pQuantity -= 16;
		}
		else if (pQuantity >= 15)
		{
			ret += ' חמש-עשרה';
			pQuantity -= 15;
		}
		else if (pQuantity >= 14)
		{
			ret += ' ארבע-עשרה';
			pQuantity -= 14;
		}
		else if (pQuantity >= 13)
		{
			ret += ' שלוש-עשרה';
			pQuantity -= 13;
		}
		else if (pQuantity >= 12)
		{
			ret += ' שתים-עשרה';
			pQuantity -= 12;
		}
		else if (pQuantity >= 11)
		{
			ret += ' אחד-עשרה';
			pQuantity -= 11;
		}
		return ret.trim();
	}
	
	private static string DoLessThanTenInQuantity()
	{
		string ret = '';
		if (pQuantity >= 10)
		{
			ret += ' עשרה';
			pQuantity -= 10;
		}
		else if (pQuantity >= 9)
		{
			ret += ' תשע';
			pQuantity -= 9;
		}
		else if (pQuantity >= 8)
		{
			ret += ' שמונה';
			pQuantity -= 8;
		}
		else if (pQuantity >= 7)
		{
			ret += ' שבע';
			pQuantity -= 7;
		}
		else if (pQuantity >= 6)
		{
			ret += ' שש';
			pQuantity -= 6;
		}
		else if (pQuantity >= 5)
		{
			ret += ' חמש';
			pQuantity -= 5;
		}
		else if (pQuantity >= 4)
		{
			ret += ' ארבע';
			pQuantity -= 4;
		}
		else if (pQuantity >= 3)
		{
			ret += ' שלוש';
			pQuantity -= 3;
		}
		else if (pQuantity >= 2)
		{
			ret += ' שתיים';
			pQuantity -= 2;
		}
		else if (pQuantity >= 1)
		{
			ret += ' אחד';
			pQuantity -= 1;
		}
		return ret.trim();
	}
	
	private static string DoUnitsInAmount(string privRet)
	{
		string ret = '';
		if (pAmount > 10)
			ret += DoMoreThanTenInAmount();
		else if (pAmount <= 10)
			ret += DoLessThanTenInAmount();
		ret = ret.trim();
		if (privRet != '' && ret != '')
			ret = 'ו' + ret;
		return ret;
	}
	
	private static string DoMoreThanTenInAmount()
	{
		string ret = '';
		if (pAmount >= 19)
		{
			ret += ' תשע-עשרה';
			pAmount -= 19;
		}
		else if (pAmount >= 18)
		{
			ret += ' שמונה-עשרה';
			pAmount -= 18;
		}
		else if (pAmount >= 17)
		{
			ret += ' שבע-עשרה';
			pAmount -= 17;
		}
		else if (pAmount >= 16)
		{
			ret += ' שש-עשרה';
			pAmount -= 16;
		}
		else if (pAmount >= 15)
		{
			ret += ' חמש-עשרה';
			pAmount -= 15;
		}
		else if (pAmount >= 14)
		{
			ret += ' ארבע-עשרה';
			pAmount -= 14;
		}
		else if (pAmount >= 13)
		{
			ret += ' שלוש-עשרה';
			pAmount -= 13;
		}
		else if (pAmount >= 12)
		{
			ret += ' שתים-עשרה';
			pAmount -= 12;
		}
		else if (pAmount >= 11)
		{
			ret += ' אחד-עשרה';
			pAmount -= 11;
		}
		return ret.trim();
	}
	
	private static string DoLessThanTenInAmount()
	{
		string ret = '';
		if (pAmount >= 10)
		{
			ret += ' עשרה';
			pAmount -= 10;
		}
		else if (pAmount >= 9)
		{
			ret += ' תשע';
			pAmount -= 9;
		}
		else if (pAmount >= 8)
		{
			ret += ' שמונה';
			pAmount -= 8;
		}
		else if (pAmount >= 7)
		{
			ret += ' שבע';
			pAmount -= 7;
		}
		else if (pAmount >= 6)
		{
			ret += ' שש';
			pAmount -= 6;
		}
		else if (pAmount >= 5)
		{
			ret += ' חמש';
			pAmount -= 5;
		}
		else if (pAmount >= 4)
		{
			ret += ' ארבע';
			pAmount -= 4;
		}
		else if (pAmount >= 3)
		{
			ret += ' שלוש';
			pAmount -= 3;
		}
		else if (pAmount >= 2)
		{
			ret += ' שתיים';
			pAmount -= 2;
		}
		else if (pAmount >= 1)
		{
			ret += ' אחד';
			pAmount -= 1;
		}
		return ret.trim();
	}
	
	private static string GetQuantityStrForThousands()
	{
		string ret = '';
		if (pQuantity > 2)
		{
			string toAdd = '';
			if (pQuantity <= 10)
				toAdd = ' אלפים';
			else
				toAdd = ' אלף';
			if (pQuantity >= 20)
			{
				ret += DoHundredsInQuantity();
				ret += ' ' + DoScoresInQuantity();
				ret += ' ' + DoUnitsInQuantityForThousands(ret.trim());
			}
			else
			{
				ret += DoUnitsInQuantityForThousands(ret.trim());
			}
			ret += toAdd;
		}
		else if (pQuantity == 2)
			ret += 'אלפיים';
		else if (pQuantity == 1)
			ret += 'אלף';
		return ret.trim();
	}
	
	private static string DoUnitsInQuantityForThousands(string privRet)
	{
		string ret = '';
		if (privRet != '')
			ret += 'ו';
		if (pQuantity > 10)
			ret += DoMoreThanTenInQuantity();
		else if (pQuantity <= 10 && privRet != '')
			ret += DoLessThanTenInQuantity();
		else if (pQuantity <= 10 && privRet == '')
			ret += DoLessThanTenInQuantityForThousands();
		return ret.trim();
	}
	
	private static string DoLessThanTenInQuantityForThousands()
	{
		string ret = '';
		if (pQuantity >= 10)
		{
			ret += ' עשרת';
			pQuantity -= 10;
		}
		else if (pQuantity >= 9)
		{
			ret += ' תשעת';
			pQuantity -= 9;
		}
		else if (pQuantity >= 8)
		{
			ret += ' שמונת';
			pQuantity -= 8;
		}
		else if (pQuantity >= 7)
		{
			ret += ' שבעת';
			pQuantity -= 7;
		}
		else if (pQuantity >= 6)
		{
			ret += ' ששת';
			pQuantity -= 6;
		}
		else if (pQuantity >= 5)
		{
			ret += ' חמשת';
			pQuantity -= 5;
		}
		else if (pQuantity >= 4)
		{
			ret += ' ארבעת';
			pQuantity -= 4;
		}
		else if (pQuantity >= 3)
		{
			ret += ' שלושת';
			pQuantity -= 3;
		}
		return ret.trim();
	}
	
	private static integer GetNumQuantity(integer minNum)
	{
		integer ret = 0;
		while (pAmount >= minNum)
		{
			if (pAmount >= decimal.valueOf(100 * minNum))
			{
				ret += 100;
				pAmount -= decimal.valueOf(100 * minNum);
			}
			else if (pAmount >= decimal.valueOf(10 * minNum))
			{
				ret += 10;
				pAmount -= decimal.valueOf(10 * minNum);
			}
			else
			{
				ret += 1;
				pAmount -= minNum;
			}
		}
		return ret;
	} 
}