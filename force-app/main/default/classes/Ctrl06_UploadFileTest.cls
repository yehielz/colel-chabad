@isTest
private class Ctrl06_UploadFileTest {

    static testMethod void myUnitTest() {
    	Family__c f = new Family__c();
    	insert f;
    	Attachment a = new Attachment(parentId = f.Id,Name='Test',Body = Blob.valueOf(''));
    	insert a;
    	Ctrl06_UploadFile.getAttachments(f.Id);
        Ctrl06_UploadFile.deleteFile(a.Id, f.Id);
        Ctrl06_UploadFile.updateAttachedDocs(f.Id, 'TEST');
    }
}