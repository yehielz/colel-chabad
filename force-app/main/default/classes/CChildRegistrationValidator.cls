public with sharing class CChildRegistrationValidator 
{

    public static String Validate(ChildRegistration__c registration)    {   
        
        Children__c child = [select Id, RegistrationAllowed__c From Children__c where Id = :registration.ChildName__c];
        if(registration.Id != null || (child != null && child.RegistrationAllowed__c == true))
            return '';
        //validate it is not duplicate on the same year/child
        ChildRegistration__c[] childRegistrations = [select name, id, SchoolYearName__c, ChildName__c 
            from ChildRegistration__c 
            where SchoolYearName__c = :registration.SchoolYearName__c 
            and ChildName__c = :registration.ChildName__c ];
        if (childRegistrations.size() > 0)  {
            return CErrorMessages.ChildRegistrationExistAlreadyForThisYear;
        }
        
        
        //validate there is a diploma on last year second half
        if(registration.Class__c != 'גן' && registration.Class__c != 'א'){
            SchoolYear__c year = CSchoolYearDb.GetActiveYear();
            childRegistrations = [select name, id, SchoolYearName__c, ChildName__c 
                from ChildRegistration__c 
                where SchoolYearName__c = :year.previousYear__c 
                and ChildName__c = :registration.ChildName__c ];
            if (childRegistrations.size() > 0)  {
                List<Diploma__c> diplomas = [select Id, Half__c
                                            From Diploma__c 
                                            where ChildName__c = :registration.ChildName__c 
                                            AND   ( SchoolYearName__c = :year.Id OR (SchoolYearName__c = :childRegistrations[0].SchoolYearName__c AND Half__c = 'מחצית ב\'') )];
                if (diplomas.size() == 0)
                    return Label.Missing_Diploma;
            }
        }
        return '';
    }
}