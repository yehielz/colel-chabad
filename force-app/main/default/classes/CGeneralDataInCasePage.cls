public with sharing class CGeneralDataInCasePage
{
	public list<Profession__c> pAllProfessionsList { get; set; }
	public map<string, Profession__c> pProfessionsMap { get; set; }
	public map<string, ProfessionType__c> pProfessionTypeMap { get;set; }
	public Profession__c pOtherProfession { get; set; }
	public list<ProfessionType__c> pSortedProfessionTypes { get; set; }
	
	public CGeneralDataInCasePage()
	{
		pAllProfessionsList = CProfessionDb.GetProfessionsInUse();
		
		pOtherProfession = CProfessionDb.GetOtherProfession();
		
		ProfessionType__c[] professionTypes = [select name, id, PaymentType__c, IsDefault__c from ProfessionType__c];
 		pProfessionTypeMap = new map<string, ProfessionType__c>();
 		for (ProfessionType__c professionType : professionTypes)
 		{
 			pProfessionTypeMap.put(professionType.id, professionType);
 		}
 		
 		pProfessionsMap = new map<string, Profession__c>();
		for (Profession__c profession : pAllProfessionsList)
		{
			pProfessionsMap.put(profession.id, profession);
		}
		SetSortedProfessionTypes();
	}
	
	private void SetSortedProfessionTypes()
	{
		/*map<string, string> namesMap = new map<string, string>();
		map<string, list<ProfessionType__c>> typesByNames = new map<string, list<ProfessionType__c>>();
		list<ProfessionType__c> values = pProfessionTypeMap.values();
		for (ProfessionType__c item : values)
		{
			if (typesByNames.containsKey(item.name))
				typesByNames.get(item.name).add(item);
			else
				typesByNames.put(item.name, new list<ProfessionType__c> { item } );
			namesMap.put(item.name, item.name);
		}
		values = null;
		pSortedProfessionTypes = new list<ProfessionType__c>();
		list<string> names = namesMap.values();
		names.sort();
		namesMap = null;
		for (string name : names)
		{
			pSortedProfessionTypes.addAll(typesByNames.get(name));
		}*/
		
		ProfessionType__c defaultProfessionType;
		map<string, ProfessionType__c> professionType = new map<string, ProfessionType__c>();
		list<string> names = new list<string>();
		for (ProfessionType__c pro : pProfessionTypeMap.values())
		{
			if (pro.IsDefault__c == true)
				defaultProfessionType = pro;
			else
			{
				professionType.put(pro.name, pro);
				names.add(pro.name);
			}
		}
		pSortedProfessionTypes = new list<ProfessionType__c>();
		if (defaultProfessionType != null)
			pSortedProfessionTypes.add(defaultProfessionType);
		names.sort();
		for (string name : names)
		{
			pSortedProfessionTypes.add(professionType.get(name));
		}
	}
}