public with sharing class CRequestForTreatmentInFullReport 
{
	public RequestForTreatment__c pRequestForTreatment { get; set; }
	public list<CActivityApprovalInFullReport> pActivityApprovals { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsRequestsInView{ get { return pView.containsKey('all') || pView.containsKey('request'); } }
	
	
	public CRequestForTreatmentInFullReport(RequestForTreatment__c mRequestForTreatment, map<string, boolean> view)
	{
		pRequestForTreatment = mRequestForTreatment;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pView = view;
	}
	
	public CRequestForTreatmentInFullReport(RequestForTreatment__c mRequestForTreatment, CActivityApprovalInFullReport mActivityApproval, map<string, boolean> view)
	{
		pRequestForTreatment = mRequestForTreatment;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		if (mActivityApproval != null)
		{
			pActivityApprovals.add(mActivityApproval);
		}
		pView = view;
	}
	
	public CRequestForTreatmentInFullReport(RequestForTreatment__c mRequestForTreatment, ActivityApproval__c mActivityApproval, map<string, boolean> view)
	{
		pRequestForTreatment = mRequestForTreatment;
		pView = view;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		if (mActivityApproval != null)
		{
			pActivityApprovals.add(new CActivityApprovalInFullReport(mActivityApproval, view));
		}
	}
	
	public CRequestForTreatmentInFullReport(RequestForTreatment__c mRequestForTreatment, list<ActivityApproval__c> mActivityApprovals, map<string, boolean> view)
	{
		pRequestForTreatment = mRequestForTreatment;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		if (mActivityApprovals != null)
		{
			for (ActivityApproval__c approval : mActivityApprovals)
			{
				pActivityApprovals.add(new CActivityApprovalInFullReport(approval, view));
			}
		}
		pView = view;
	}
	
	public void AddApproval(CActivityApprovalInFullReport approval)
	{
		pActivityApprovals.add(approval);
	}
	
	public string pRequestForTreatmentName{ get{ return CObjectNames.getHebrewString(pRequestForTreatment.name); } }
	public string amount{ get{ return pRequestForTreatment.ApprovalAmount__c == null ? '' : CObjectNames.getHebrewString(' סכום: ;' + string.valueOf(math.round(pRequestForTreatment.ApprovalAmount__c))); } }
	public string weeklyHours{ get{ return pRequestForTreatment.WeeklyHours__c == null ? '' : CObjectNames.getHebrewString(' שעות שבועיות: ;' + string.valueOf(math.round(pRequestForTreatment.WeeklyHours__c))); } }
	public string ProfessionType{ get{ return pRequestForTreatment.ProfessionTypeName__c == null ? '' : CObjectNames.getHebrewString(pRequestForTreatment.ProfessionTypeName__c); } }
	//public string Coordinator{ get{ return pRequestForTreatment.CoordinatorUserName__c == null ? '' : CObjectNames.getHebrewString(pRequestForTreatment.CoordinatorUserName__c); } }
	public string Coordinator{ get{ return pRequestForTreatment.CaseName__r.Owner.Name == null ? '' : CObjectNames.getHebrewString(pRequestForTreatment.CaseName__r.Owner.Name); } }
	public string comment{ get{ return pRequestForTreatment.Comment__c == null ? '' : CObjectNames.getHebrewString(' הערה: ' + pRequestForTreatment.Comment__c); } }
	public string meeting{ get{ return pRequestForTreatment.MeetingsNumber__c == null ? '' : CObjectNames.getHebrewString(' מספר מפגשים: ;' + string.valueOf(math.round(pRequestForTreatment.MeetingsNumber__c))); } }
	
	public string amountWS{ get{ return pRequestForTreatment.ApprovalAmount__c == null ? '' : string.valueOf(math.round(pRequestForTreatment.ApprovalAmount__c)); } }
	public string weeklyHoursWS{ get{ return pRequestForTreatment.WeeklyHours__c == null ? '' : string.valueOf(math.round(pRequestForTreatment.WeeklyHours__c)); } }
	public string meetingWS{ get{ return pRequestForTreatment.MeetingsNumber__c == null ? '' : string.valueOf(math.round(pRequestForTreatment.MeetingsNumber__c)); } }
	public string commentWS{ get{ return pRequestForTreatment.Comment__c == null ? '' : pRequestForTreatment.Comment__c; } }
	
	public string request{ get{ return comment + weeklyHours + meeting + amount ; }}
	
	public string requestWS
	{ 
		get
		{ 
			string mAmount = amountWS != '' ? '<span class="TitleInContent">סכום:</span> ' + amountWS : '';
			string mWeeklyHours = weeklyHoursWS != '' ? '<span class="TitleInContent">שעות שבועיות:</span> ' + weeklyHoursWS : '';
			string mMeeting = meetingWS != '' ? '<span class="TitleInContent">מספר מפגשים:</span> ' + meetingWS : '';
			string mComment = commentWS != '' ? '<span class="TitleInContent">הערות:</span> ' + commentWS : '';
			string ret = mAmount;
			ret += ret == '' ? mWeeklyHours : (mWeeklyHours != '' ? ' | ' + mWeeklyHours : '');
			ret += ret == '' ? mMeeting : (mMeeting != '' ? ' | ' + mMeeting : '');
			ret += ret == '' ? mComment : (mComment != '' ? ' | ' + mComment : '');
			return ret; 
		}
	}
	
	public string requestWSEN
	{ 
		get
		{ 
			string mAmount = amountWS != '' ? '<span class="TitleInContent">Amount:</span> ' + amountWS : '';
			string mWeeklyHours = weeklyHoursWS != '' ? '<span class="TitleInContent">Weekly Hours:</span> ' + weeklyHoursWS : '';
			string mMeeting = meetingWS != '' ? '<span class="TitleInContent">Meetings Number:</span> ' + meetingWS : '';
			string mComment = commentWS != '' ? '<span class="TitleInContent">Comments:</span> ' + commentWS : '';
			string ret = mAmount;
			ret += ret == '' ? mWeeklyHours : (mWeeklyHours != '' ? ' | ' + mWeeklyHours : '');
			ret += ret == '' ? mMeeting : (mMeeting != '' ? ' | ' + mMeeting : '');
			ret += ret == '' ? mComment : (mComment != '' ? ' | ' + mComment : '');
			return ret; 
		}
	}
	
	public string margin
	{ 
		get 
		{ 
			if (pView.containsKey('all'))
				return '60px';
			else
			{
				integer ret = 40;
				if (pView.containsKey('case'))
					ret += 20;
				return ret + 'px';
			}
		} 
	}
}