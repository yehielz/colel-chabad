public with sharing class CNoteAndAttachment 
{
	private Attachment pAttachment { get; set; }
	private Note pNote { get; set; }
	private map<string, User> pUsersMap { get; set; }
	
	public CNoteAndAttachment(Note mNote, Attachment mAttachment, map<string, User> mUsersMap)
	{
		pAttachment = mAttachment;
		pNote = mNote;
		pUsersMap = mUsersMap;
	}
	
	public string LastModifiedDate
	{
		get
		{
			string ret = '';
			if (pAttachment != null)
				ret = GetDateStr(pAttachment.LastModifiedDate);
			if (pNote != null)
				ret = GetDateStr(pNote.LastModifiedDate);
			return ret;
		}
	}  
	
	private string GetDateStr(datetime d)
	{
		return GetFullNumberStr(d.hour()) + ':' + GetFullNumberStr(d.minute()) + ' ' + GetFullNumberStr(d.day()) + '/' + GetFullNumberStr(d.month()) + '/' + d.year();
	}
	
	private string GetFullNumberStr(integer num)
	{
		if (num < 10)
			return '0' + num;
		return string.valueOf(num);
	}
	
	public string FileType
	{
		get
		{
			string ret = '';
			if (pAttachment != null)
				ret = 'קובץ מצורף';
			if (pNote != null)
				ret = 'הערה';
			return ret;
		}
	}  
	
	public string Title
	{
		get
		{
			string ret = '';
			if (pAttachment != null)
				ret = pAttachment.name;
			if (pNote != null)
				ret = pNote.Title;
			return ret;
		}
	}
	
	public string OwnerId
	{
		get
		{
			string ret = '';
			if (pAttachment != null)
				ret = '<a href="/' + pAttachment.OwnerId + '">' + pUsersMap.get(pAttachment.OwnerId).name + '</a>';
			if (pNote != null)
				ret = '<a href="/' + pNote.OwnerId + '">' + pUsersMap.get(pNote.OwnerId).name + '</a>';
			return ret;
		}
	}
	
	public string FileId
	{
		get
		{
			string ret = '';
			if (pAttachment != null)
				ret = pAttachment.id;
			if (pNote != null)
				ret = pNote.id;
			return ret;
		}
	}
	
	public string Body
	{
		get
		{
			string ret = '';
			if (pAttachment != null)
				ret = '<a href="/servlet/servlet.FileDownload?file=' + pAttachment.id + '">פתח</a>';
			if (pNote != null)
				ret = pNote.Body;
			return ret;
		}
	}
}