public with sharing class CRequestForTreatmentInList 
{
	public RequestForTreatment__c pRequestForTreatment { get; set; }
	public CActivityApprovalInList pNewActivityApproval { get; set; }
	public Note pNewNote { get; set; }
	public list<Note> pNotes { get; set; }
	public list<CActivityApprovalInList> pActivityApprovals { get; set; }
	public map<string, ProfessionType__c> pProfessionTypeMap { get; set; }
	public map<string, TutorLine__c> pTutorLinesMap { get; set; }
	public integer pIndex { get; set; }
	
	public CRequestForTreatmentInList(RequestForTreatment__c mRequestForTreatment, list<ActivityApproval__c> mActivityApprovals, list<Note> mNotes, map<string, 
									  ProfessionType__c> mProfessionTypeMap, map<string, TutorLine__c> mTutorLinesMap, integer index, string lastDiplomaDate)
	{
		pRequestForTreatment = mRequestForTreatment;
		pActivityApprovals = new list<CActivityApprovalInList>();
		pTutorLinesMap = mTutorLinesMap;
		for (ActivityApproval__c approval : mActivityApprovals)
		{
			pActivityApprovals.add(new CActivityApprovalInList(approval, pRequestForTreatment, mTutorLinesMap, pActivityApprovals.size()));
		}
		pProfessionTypeMap = mProfessionTypeMap;
		pNewActivityApproval = GetNewApproval();
		pNotes = mNotes;
		pNewNote = new Note();
		pIndex = index;
		pLastDiplomaDate = lastDiplomaDate;
	}
	
	public void AddNewActivityApproval()
	{
		pActivityApprovals.add(pNewActivityApproval);
		pNewActivityApproval = GetNewApproval();
	}
	
	public void AddNewNote()
	{
		pNewNote.Title = pRequestForTreatment.name + ': ' + pNewNote.Title;
		pNewNote.ParentId = pRequestForTreatment.id;
		pNotes.add(pNewNote);
		pNewNote = new Note();
	}
	
	private CActivityApprovalInList GetNewApproval() 
	{
		ActivityApproval__c newOne = new ActivityApproval__c();
		newOne.RequestForTreatmentName__c = pRequestForTreatment.id;
		newOne.TutorName__c = pRequestForTreatment.TutorName__c;
		newOne.ChildName__c = pRequestForTreatment.ChildName__c;
		newOne.ProfessionName__c = pRequestForTreatment.ProfessionName__c;
		newOne.WeeklyHours__c = pRequestForTreatment.WeeklyHours__c;
		newOne.FromDate__c = pRequestForTreatment.FromDate__c;
		newOne.ToDate__c = pRequestForTreatment.ToDate__c;
		newOne.ProfessionOther__c = pRequestForTreatment.ProfessionOther__c;
		newOne.Note__c = pRequestForTreatment.Comment__c;
		CActivityApprovalInList ret = new CActivityApprovalInList(newOne, pRequestForTreatment, pTutorLinesMap, pActivityApprovals.size());
		ret.SetRateByTutor();
		return ret;
	}
	
	public void SetRateByTutor()
	{
		pNewActivityApproval.SetRateByTutor();	
	}
	
	public boolean bWeeklyHours 
	{ 
		get
		{
			boolean ret = true;
	    	if(pProfessionTypeMap.containsKey(pRequestForTreatment.ProfessionTypeName__c))
	    	{
	    		string paymentType = pProfessionTypeMap.get(pRequestForTreatment.ProfessionTypeName__c).PaymentType__c;
	    		if (paymentType != CObjectNames.HoursPerWeek)
	    			ret = false;
	    	}
	    	else
	    		ret = false;
    		return ret;
		}
	}
	public boolean bApprovalAmount{ get{ return !bWeeklyHours; } }
	
	public void DeleteApproval(integer index)
	{
		for (integer i = 0; i < pActivityApprovals.size(); i++)	
		{
			if (pActivityApprovals[i].pIndex == index)
			{
				if (pActivityApprovals[i].pActivityApproval.id != null)
					delete pActivityApprovals[i].pActivityApproval;
				pActivityApprovals.remove(i);
			}
		}
	}
 	
 	public string pLastDiplomaDate{ get; set; }
}