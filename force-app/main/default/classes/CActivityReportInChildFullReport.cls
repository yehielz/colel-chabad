public with sharing class CActivityReportInChildFullReport 
{
	public ActivityReport__c pActivityReport { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsReportsInView{ get { return pView.containsKey('all') || pView.containsKey('report'); } }
	public string margin
	{ 
		get 
		{ 
			if (pView.containsKey('all'))
				return '100px';
			else
			{
				integer ret = 40;
				if (pView.containsKey('case'))
					ret += 20;
				if (pView.containsKey('rquest'))
					ret += 20;
				if (pView.containsKey('approval'))
					ret += 20;
				return ret + 'px';
			} 
		} 
	}
	
	public CActivityReportInChildFullReport(ActivityReport__c mActivityReport, map<string, boolean> view)
	{
		pActivityReport = mActivityReport;
		pView = view;
	} 
	
	public string pActivityReportName{ get{ return CObjectNames.getHebrewString(pActivityReport.name); } }
	public string Month{ get{ return (pActivityReport.ReportMonth__c == null || pActivityReport.ReportMonth__c == '') ? '' :  'חודש דיווח: ' + GetPointInPlace(pActivityReport.ReportMonth__c) + ' '; } }
	public string TotalHours{ get{ return (pActivityReport.TotalReportHours__c == null || pActivityReport.TotalReportHours__c < 1) ? '' : 'סה"כ שעות דיווח: ' + GetPointInPlace(string.valueOf(math.round(pActivityReport.TotalReportHours__c))) + ' '; } }
	public string TalkWith{ get{ return (pActivityReport.TalkWithDirectorOrTeacherOrParent__c == null || pActivityReport.TalkWithDirectorOrTeacherOrParent__c == '') ? '' : 'שיחה עם: ' + GetPointInPlace(pActivityReport.TalkWithDirectorOrTeacherOrParent__c) + ' '; } }
	public string TotalPayment{ get{ return (pActivityReport.TotalPayment__c == null || pActivityReport.TotalPayment__c < 1) ? '' : 'סה"כ לתשלום: ' + GetPointInPlace(string.valueOf(math.round(pActivityReport.TotalPayment__c)) + ' ש"ח') + ' '; } }
	
	public string ReportDetails{ get{ string ret = Month + TotalHours + TalkWith + TotalPayment; return ret == '' ? '' : GetHebrewWordsForReport(ret, 83); } }
	
	public string MonthWS{ get{ return (pActivityReport.ReportMonth__c == null || pActivityReport.ReportMonth__c == '') ? '' : pActivityReport.ReportMonth__c; } }
	public string TotalHoursWS{ get{ return (pActivityReport.TotalReportHours__c == null || !(pActivityReport.TotalReportHours__c > 0)) ? '' : string.valueOf(math.round(pActivityReport.TotalReportHours__c)); } }
	public string TalkWithWS{ get{ return (pActivityReport.TalkWithDirectorOrTeacherOrParent__c == null || pActivityReport.TalkWithDirectorOrTeacherOrParent__c == '') ? '' : pActivityReport.TalkWithDirectorOrTeacherOrParent__c; } }
	public string TotalPaymentWS{ get{ return (pActivityReport.TotalPayment__c == null || pActivityReport.TotalPayment__c < 1) ? '' : string.valueOf(math.round(pActivityReport.TotalPayment__c)) + ' ש"ח'; } }
	public string ReportDetailsWS
	{ 
		get
		{ 
			string mMonth = MonthWS != '' ? '<span class="TitleInContent">חודש דיווח:</span> ' + MonthWS : '';
			string mTotalHours = TotalHoursWS != '' ? '<span class="TitleInContent">סה"כ שעות דיווח:</span> ' + TotalHoursWS : '';
			string mTotalPayment = TotalPaymentWS != '' ? '<span class="TitleInContent">סה"כ לתשלום:</span> ' + TotalPaymentWS : '';
			string mTalkWith = TalkWithWS != '' ? '<span class="TitleInContent">שיחה:</span> ' + TalkWithWS : '';
			string ret = mMonth;
			ret += ret == '' ? mTotalHours : (mTotalHours != '' ? '  ' + mTotalHours : '');
			ret += ret == '' ? mTotalPayment : (mTotalPayment != '' ? '  ' + mTotalPayment : '');
			ret += ret == '' ? mTalkWith : (mTalkWith != '' ? '  ' + mTalkWith : '');
			return ret.trim() != '' ? '<span class="gray">דיווח פעילות: </span>' + ret : '';
		} 
	}
	
	public string ReportDetailsWSEN
	{ 
		get
		{ 
			string mMonth = MonthWS != '' ? '<span class="TitleInContent">Report Month:</span> ' + MonthWS : '';
			string mTotalHours = TotalHoursWS != '' ? '<span class="TitleInContent">Total Report Hours:</span> ' + TotalHoursWS : '';
			string mTotalPayment = TotalPaymentWS != '' ? '<span class="TitleInContent">Total Payment:</span> ' + TotalPaymentWS : '';
			string mTalkWith = TalkWithWS != '' ? '<span class="TitleInContent">Conversation:</span> ' + TalkWithWS : '';
			string ret = mMonth;
			ret += ret == '' ? mTotalHours : (mTotalHours != '' ? '  ' + mTotalHours : '');
			ret += ret == '' ? mTotalPayment : (mTotalPayment != '' ? '  ' + mTotalPayment : '');
			ret += ret == '' ? mTalkWith : (mTalkWith != '' ? '  ' + mTalkWith : '');
			return ret.trim() != '' ? '<span class="gray">Activity report: </span>' + ret : '';
		} 
	}
	
	public string GetPointInPlace(string str)
	{
		if (CObjectNames.isWithHebrew(str.substring(str.length() - 1)))
			return str + ';';
		else
			return ';' + str;
	}
    
    public static string GetHebrewWordsForReport(string thenote, integer length)
	{
		string ret = '';
		if (thenote != null)
		{
			string note = ' ' + thenote;
			integer noteLength = note.length();
			for (integer i = (length - 14); i < note.length(); i=length)
			{
				integer fromInSubS = i;
				integer minus = 0;
				while(note.substring(i-minus, ((i-minus)+1)) != ' ')
					minus++;
				i = i - minus;
				ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
				if (fromInSubS == (length - 14))
					ret += '<span class="gray"> ' + CObjectNames.getHebrewString('דיווח פעילות:') + '</span>';
				ret += '<br/>';
				note = note.substring(i, note.length());
			}
			ret += CObjectNames.getHebrewString(note);
			if (noteLength < (length - 13)) 
				ret+= '<span class="gray"> ' + CObjectNames.getHebrewString('דיווח פעילות:') + '</span>';
		}
		return ret;
	}
}