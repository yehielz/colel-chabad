public with sharing class CWebSiteSupporterChildrenDb 
{
    public Support__c pSupporter { get; set; }
    public list<CWSChildInSupporterChildren> pChildren { get; set; }
    public boolean IsEnglish { get; set; }
    
    public CWebSiteSupporterChildrenDb()
    {
        SetFromSession();
        SetIsEnglish();
        SetPageSize();
        SetPageNumber();
        SetChildren();
    }
    
    private void SetIsEnglish() 
    {
        IsEnglish = false;
        if (pSupporter != null && pSupporter.LanguageForWS__c != null && pSupporter.LanguageForWS__c.contains('English'))
            IsEnglish = true;
        string lang = Apexpages.currentPage().getParameters().get('lang');
        if (lang != null && lang == 'en')
            IsEnglish = true;
        if (lang != null && lang == 'he')
            IsEnglish = false;
    }
    
    private void SetFromSession() 
    {
        if (ApexPages.currentPage().getCookies().get('sessionId') != null)
        {
            SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
            if (sessionId != null)
            {
                pSupporter = CSupportDb.GetSupporterById(sessionId.SupporterName__c);
            }
        }
        if (pSupporter == null)
            pSupporter = new Support__c();
    }
    
    public void SetChildren()
    {
        pChildren = new list<CWSChildInSupporterChildren>();
        map<string, ChildRegistration__c> registrationsByChildren = GetRegistrationsByChildren();
        list<string> childrenIds = new list<string>();
        childrenIds.addAll(registrationsByChildren.KeySet());
        map<string, boolean> IsHasReportsMapByChildren = GetIsHasReportsMapByChildren(childrenIds);
        Children__c[] childrenList = GetChildrenByPaging(childrenIds);//CChildDb.GetChildrenListByIds(childrenIds);
        for (Children__c child : childrenList)
        {
            ChildRegistration__c registration = new ChildRegistration__c();
            if (registrationsByChildren.containsKey(child.id))
                registration = registrationsByChildren.get(child.id);
            pChildren.add(new CWSChildInSupporterChildren(child, registration, pImgByGenderMap, IsEnglish, IsHasReportsMapByChildren.containsKey(child.id), IsBlur, IsLastName));
        } 
        
        system.debug( '\n\n\n pChildren' +  pChildren  +'bbbbbbbbbbbbbbbbbb  \n\n\\n');
    }
    
    private map<string, ChildRegistration__c> GetRegistrationsByChildren()
    {
        list<ChildRegistration__c> registrations = new list<ChildRegistration__c>();
        if (pSupporter.IsAllChildren__c)
        {
            integer num = pSupporter.YearsBack__c != null ? integer.valueof(pSupporter.YearsBack__c) : 1;
            string dateToCheck = string.valueof(system.today().addYears(-num)).substring(0, 10);
            string year = CSchoolYearDb.GetActiveYearId();
            system.debug('\n\n\\n aaaaaaaaaaaaaaaa');
            registrations = CChildRegistrationDb.GetChildRegistrationByFilter('SchoolYearName__r.EndOfSchoolYear__c > ' + dateToCheck + ' and SchoolYearName__r.IsActive__c  = true ' );
            //registrations = CChildRegistrationDb.GetChildRegistrationByFilter(year != null ? 'SchoolYearName__c = \'' + year + '\'' : 'id = null');
            
            system.debug('bbbbbbbbbbbbbbbbbb  \n\n\\n');
        }
        else
            registrations = CChildRegistrationDb.GetChildRegistrationByFilter('id in ' + GetRegistrationsIds());
        map<string, ChildRegistration__c> ret = new map<string, ChildRegistration__c>();
        for (ChildRegistration__c registration : registrations)
        {
            /*ChildRegistration__c toAdd = registration;
            if (ret.containsKey(registration.ChildName__c))
            {
                ChildRegistration__c exist = ret.get(registration.ChildName__c);
                date newStartDate = registration.SchoolYearName__r.StartDate__c ; 
                date existStartDate = exist.SchoolYearName__r.StartDate__c; 
                boolean existIsActive = exist.SchoolYearName__r.IsActive__c == null ? false : exist.SchoolYearName__r.IsActive__c;
                if (existIsActive || newStartDate == null || (existStartDate != null && existStartDate > newStartDate))
                    toAdd = exist;
            }*/
            ret.put(registration.ChildName__c, registration);
        }
        return ret;
    }
    
    private string GetRegistrationsIds()
    {

        integer num = pSupporter.YearsBack__c != null ? integer.valueof(pSupporter.YearsBack__c) : 1;
        date dateToCheck = system.today().addYears(-num);
        system.debug('dateToCheck = ' + dateToCheck);
        list<ChildRegistrationInSupporter__c> registrationsInSupporters = [select name, id, ChildRegistrationName__c from ChildRegistrationInSupporter__c 
                                                                                  //where ChildRegistrationName__r.SchoolYearName__r.IsActive__c = true 
                                                                                  where ChildRegistrationName__r.SchoolYearName__r.EndOfSchoolYear__c > :dateToCheck
                                                                                  and SupporterName__c = :pSupporter.id];
        map<string, string> registrationIdsMap = new map<string, string>();
        string registrationIds = '';
        for (ChildRegistrationInSupporter__c item : registrationsInSupporters)
        {
            if (!registrationIdsMap.containsKey(item.ChildRegistrationName__c))
            {
                system.debug('tem.ChildRegistrationName__r = ' + item.ChildRegistrationName__r.SchoolYearName__r.EndOfSchoolYear__c);
                string id = '\'' + string.valueOf(item.ChildRegistrationName__c) + '\'';
                registrationIds += registrationIds == '' ? id : ',' + id;
            }
            registrationIdsMap.put(item.ChildRegistrationName__c, item.ChildRegistrationName__c);
        }
        registrationIds = registrationIds == '' ? '\'\'' : registrationIds;
        return '(' + registrationIds + ')';
    }
    
    private map<string, string> imgByGenderMap { get; set; }
    public map<string, string> pImgByGenderMap
    { 
        get
        {
            if (imgByGenderMap == null)
            {
                imgByGenderMap = new map<string, string>();
                StaticResource[] res = [select name, id, Body, SystemModstamp, NamespacePrefix from StaticResource where name = 'GirlPicture' or name = 'BoyPicture'];
                for (StaticResource re : res)
                {
                    string img = '<img src="/resource/' + re.SystemModStamp.getTime() + '/' + (re.NamespacePrefix != null && re.NamespacePrefix != ''
                                     ? re.NamespacePrefix + '__' : '') + re.name + '"/>';
                    imgByGenderMap.put(re.name, img);
                }
            }
            return imgByGenderMap;
        }
    }
    
    public map<string, boolean> GetIsHasReportsMapByChildren(list<string> childrenIds)
    { 
        map<string, boolean> ret = new map<string, boolean>();
        date toDay = datetime.now().date();
        date satrtDate = toDay < date.newInstance(toDay.year(), 9, 1) ? date.newInstance(toDay.year() - 1, 9, 1) : date.newInstance(toDay.year(), 9, 1);
        date endDate = toDay < date.newInstance(toDay.year(), 9, 1) ? date.newInstance(toDay.year(), 9, 1) : date.newInstance(toDay.year() + 1, 9, 1);
        
        AggregateResult[] reports = [select ChildName__c from ActivityReport__c where ChildName__c in :childrenIds and 
                                    ((ActivityApprovalName__r.FromDate__c >= :satrtDate and ActivityApprovalName__r.FromDate__c < :endDate)
                                    or (ActivityApprovalName__r.ToDate__c >= :satrtDate and ActivityApprovalName__r.ToDate__c < :endDate))
                                    group by ChildName__c];
        for (AggregateResult report : reports)
        {
            string childname = string.valueOf(report.get('ChildName__c'));
            ret.put(childname, true);
        }
        return ret;
    }
    
    public boolean IsBlur 
    { 
        get
        {
            if (Apexpages.currentPage().getParameters().get('b') != null && Apexpages.currentPage().getParameters().get('b') == 'b')
                return true;
            return false;
        } 
    }
    
    public boolean IsLastName
    { 
        get
        {
            if (Apexpages.currentPage().getParameters().get('n') != null && Apexpages.currentPage().getParameters().get('n') == 'n')
                return false;
            return true;
        } 
    }
    
    public integer PageNumber { get;set; }
    public integer PageSize { get;set; }
    public boolean IsAllPage { get;set; }
    public boolean IsFirstPage { get;set; }
    public boolean IsLastPage { get;set; }
    
    private void SetPageSize()
    {
        IsAllPage = false;
        string size = null;
        if (Apexpages.currentPage().getCookies().containsKey('PSize') != null && Apexpages.currentPage().getCookies().get('PSize') != null)
        {
            size = Apexpages.currentPage().getCookies().get('PSize').getValue();
            if (size != null && size.trim() != '')
            {
                if (size == 'all')
                    IsAllPage = true;
                try 
                { 
                    PageSize = integer.valueOf(size);
                    if (PageSize > 100)
                        PageSize = 100;
                    if (PageSize < 20)
                        PageSize = 20; 
                }
                catch(Exception e){ PageSize = 20; }
            }
        }
        size = Apexpages.currentPage().getParameters().get('PSize');
        if (size != null && size.trim() != '')
        {
            if (size == 'all')
                IsAllPage = true;
            try 
            { 
                PageSize = integer.valueOf(size);
                if (PageSize > 100)
                    PageSize = 100;  
                if (PageSize < 20)
                    PageSize = 20; 
            }
            catch(Exception e){ PageSize = 20; }
        }
    }
    
    private void SetPageNumber()
    {
        string num = null;
        if (Apexpages.currentPage().getCookies().containsKey('PNum') != null && Apexpages.currentPage().getCookies().get('PNum') != null)
        {
            num = Apexpages.currentPage().getCookies().get('PNum').getValue();
            if (num != null && num.trim() != '')
            {
                try { PageNumber = integer.valueOf(num);  }
                catch(Exception e){ PageNumber = 1; }
            }
        }
        num = Apexpages.currentPage().getParameters().get('PNum');
        if (num != null && num.trim() != '')
        {
            try { PageNumber = integer.valueOf(num); }
            catch(Exception e){ PageNumber = 1; }
        }
        IsFirstPage = false;
        IsLastPage = false;
    }
    
    private list<Children__c> GetChildrenByPaging(list<string> childrenIds)
    {
        IsFirstPage = false;
        IsLastPage = false;
        PageNumber = PageNumber == null || PageNumber < 1 ? 1 : PageNumber;
        PageSize = PageSize == null || PageSize < 20 ? 20 : PageSize;
        Children__c[] childrenList = CChildDb.GetChildrenListByIds(childrenIds);
        system.debug( '\n\n\n childrenList.size() ' +  childrenList.size()  +'bbbbbbbbbbbbbbbbbb  \n\n\\n');
        PageSize = IsAllPage ? childrenList.size() : PageSize;
        PageNumber = IsAllPage ? 1 : PageNumber;
        integer startNumber = (PageNumber * PageSize) - PageSize;
        while (startNumber >= childrenList.size())
        {
            PageNumber--;
            startNumber = (PageNumber * PageSize) - PageSize;
        }
        startNumber = startNumber < 0 ? 0 : startNumber;
        integer endNumber = startNumber + PageSize;
        if (endNumber >= childrenList.size())
        {
            endNumber = childrenList.size(); 
            IsLastPage = true;
        }   
        list<Children__c> ret = new list<Children__c>();
        system.debug( '\n\n\n startNumber ' +  startNumber  +'bbbbbbbbbbbbbbbbbb  \n\n\\n');
        system.debug( '\n\n\n endNumber ' +  endNumber  +'bbbbbbbbbbbbbbbbbb  \n\n\\n');
        for (integer i = startNumber; i < endNumber; i++)
        {
            if (i < childrenList.size())
                ret.add(childrenList[i]);
        }
        PageNumber = PageNumber < 1 ? 1 : PageNumber;
        if (PageNumber == 1)
            IsFirstPage = true;
        IsAllPage = false;
        SetCookies();
        //return childrenList;
        
        system.debug( '\n\n\n ret.size()' +  ret.size()  +'bbbbbbbbbbbbbbbbbb  \n\n\\n');
        return ret;
        
    }
    
    private void SetCookies()
    {
        Cookie PSizeCookie = new Cookie('PSize', string.valueOf(PageSize), null, -1, false);
        Cookie PNumCookie = new Cookie('PNum', string.valueOf(PageNumber), null, -1, false);
        ApexPages.currentPage().setCookies(new list<Cookie> { PSizeCookie, PNumCookie } );
    }
}