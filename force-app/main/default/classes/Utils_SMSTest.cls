@isTest
private class Utils_SMSTest {

    static testMethod void testSendSMS() {
    	Utils_SMS.SMS_Request request = new Utils_SMS.SMS_Request ();
    	request.message = 'TEST';
    	request.phoneNumber = '0546521411';
    	request.send = true;
    	request.record = new Account();
        Utils_SMS.sendSms(new List<Utils_SMS.SMS_Request> {request});
    }
}