public with sharing class CMessagesDb 
{
	private Message__c pMessage { get; set; }
	
	public CMessagesDb()
	{
		pMessage = new Message__c();
	}
	
	public CMessagesDb(ApexPages.StandardController controller)
	{
		pMessage = (Message__c)controller.getRecord();
		if (pMessage.id != null)
			SetpMessageBypMessageId(pMessage.id);
	}
	
	private void SetpMessageBypMessageId(string messageId)
	{
		pMessage = [select name, FamilyOrChildName__c, CreatedDate, id, Sender__c, For__c, 
					MessageContent__c, ReadBy__c, Link__c, MessageType__c from Message__c where id = :messageId];
	}
	
	public void InsertMessage(string link, string note, string userSender, string userFor, string messageType, string familyOrChildName)
	{
		Message__c newMessage = new Message__c();
		//newMessage.name = name;
		newMessage.MessageType__c = messageType;
		newMessage.Link__c = link;
		newMessage.For__c = userFor;
		newMessage.Sender__c = userSender;
		newMessage.MessageContent__c = note;
		//newMessage.FamilyOrChildName__c = familyOrChildName;
		insert newMessage;
	}
	
	public Message__c getpMessage()
	{
		return pMessage;
	}
	
	public void onAddToReadBy()
	{
		pMessage.ReadBy__c = getReadByWithAddUser(pMessage.ReadBy__c);
		update pMessage;
	}
	
	public void onRemoveFromReadBy()
	{
		pMessage.ReadBy__c = getReadByWithRemoveUser(pMessage.ReadBy__c);
		update pMessage;
	}
	
	public list<CMessageInViewPage> GetpMessagesForView(string notReadOrAll)
	{
		if (notReadOrAll == 'NotRead')
		{
			return GetMessagesByNotReadToView();			
		}
		else
		{
			return GetAllMessagesToView();
		}
	}
	
	private list<CMessageInViewPage> GetMessagesByNotReadToView()
	{
		string userName = '%' + system.Userinfo.getName() + '%';
		List<Message__c> messages = [select name, CreatedDate, Link__c, id, ReadBy__c, Sender__c, For__c, MessageType__c, FamilyOrChildName__c,
				Read__c, MessageContent__c from Message__c where not ReadBy__c like :userName order by CreatedDate DESC];
		list<CMessageInViewPage> ret = new list<CMessageInViewPage>();
		for (integer i = 0; i < messages.size(); i++)
		{
			ret.add(new CMessageInViewPage(messages[i]));
		}	
		return ret;
	}
	
	private list<CMessageInViewPage> GetAllMessagesToView()
	{
		List<Message__c> messages = [select name, Link__c, CreatedDate, id, ReadBy__c, Sender__c, For__c, MessageType__c, 
					FamilyOrChildName__c, Read__c, MessageContent__c from Message__c order by CreatedDate DESC];
		list<CMessageInViewPage> ret = new list<CMessageInViewPage>();
		for (integer i = 0; i < messages.size(); i++)
		{
			ret.add(new CMessageInViewPage(messages[i]));
		}	
		return ret;
	}
	
	
	
	public string getReadByWithAddUser(string readBy)
	{
		if (readBy == null || !readBy.contains(system.userInfo.getName()))
		{
			if (readBy == null)
				readBy = '';
			readBy += ' ' + system.userInfo.getName() + ';';
		}
		return readBy;
	}
	
	public string getReadByWithRemoveUser(string readBy)
	{
		if (readBy != null && readBy.contains(system.userInfo.getName()))
		{
			string[] listOfReadBy= readBy.split(';');
			readBy = '';
			for (integer i = 0; i < listOfReadBy.size(); i++)
			{
				if (!listOfReadBy[i].contains(system.userInfo.getName()))
				{
					readBy += ' ' + listOfReadBy[i] + ';';
				}
			}
		}
		return readBy;
	}
}