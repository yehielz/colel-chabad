public with sharing class Ctrl08MassSMSApprovals {
    private List<ActivityApproval__c> approvals ;
    public List<Utils_SMS.SMS_Request> smsMessages {get;set;}
    public List<String> getFieldsToDisplay() {
         return new List<String> {'ChildName__r.FamilyName__c','Name','ProfessionName__r.Name','Id'} ;
    }
    public Ctrl08MassSMSApprovals(ApexPages.StandardSetController records){
        approvals = (List<ActivityApproval__c>)records.getSelected(); 
        approvals = [Select  p.Name,p.ChildName__r.FamilyName__r.MobilePhone__c, p.ChildName__r.FamilyName__c, p.ChildName__r.FamilyName__r.ParentName__c, 
        					p.ChildName__r.FamilyName__r.Name,  ProfessionName__r.Name  , ChildName__r.Name 
                	From ActivityApproval__c p 
                	where Id in :approvals];
        
    }
    
    public Pagereference init(){
        if(approvals == null || approvals.size() == 0 ){
            Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING,'עליך לבחור לפחות שורה אחת.'));
            return null;
        }
        
        smsMessages = new List<Utils_SMS.SMS_Request> ();
        String prefix =  ((String)approvals[0].Id ).substring(0,3);
        for(ActivityApproval__c app :approvals){
            Utils_SMS.SMS_Request req = Utils_SMS.buildApprovalSMS(app);
            
            smsMessages.add(req);
        }
        
        //Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.INFO,results));
        return null;
    
    }
    
    public Pagereference sendSMS(){
        List<String> res = Utils_SMS.sendSms(smsMessages);
        integer i = 0;
        for(Utils_SMS.SMS_Request req :smsMessages){
            if(res.size() > i && req.send == true){
                req.result = res[i];
                i++;
            }
        }
        return null;
    }
}