public with sharing class CWebSiteSupporterLogInFacad 
{
	private CWebSiteSupporterLogInDb webSiteSupporterLogInDb { get; set; }
    
    public boolean pIsError{ get{ return (UserNameWarning != null && UserNameWarning.trim() != '') || (PasswordWarning != null && PasswordWarning.trim() != ''); } }
    public SessionID__c pSessionId{ get{ return webSiteSupporterLogInDb.pSessionId; } }
    /*public string pUserName{ get{ return webSiteSupporterLogInDb.pUserName; } set{ webSiteSupporterLogInDb.pUserName = value; } } 
    public string pPassword{ get{ return webSiteSupporterLogInDb.pPassword; } set{ webSiteSupporterLogInDb.pPassword = value; } }*/
    public string response{ get{ return '{"UNError":" ' + UserNameWarning + ' ","PError":" ' + PasswordWarning + ' ","IsError":' + pIsError + ',"sessionId":"' + pSessionId.id + '"}'; } }
    
    public CWebSiteSupporterLogInFacad()
    {
    	webSiteSupporterLogInDb = new CWebSiteSupporterLogInDb();
    }
    
    public string UserNameWarning{ get{ return webSiteSupporterLogInDb.UserNameWarning; } }
	public string PasswordWarning{ get{ return webSiteSupporterLogInDb.PasswordWarning; } }
    
    public void LogIn()
    {
    	if (webSiteSupporterLogInDb.IfExist())
		{
			webSiteSupporterLogInDb.SetCookie();
		}
    }
}