public with sharing class Ctrl04_MassEmailPayment {
    private List<Payment__c> paym ;
    
    public Ctrl04_MassEmailPayment(ApexPages.StandardSetController records){
        paym = (List<Payment__c>)records.getSelected();
        
    }
    
    public Pagereference init(){
    	if(paym != null && paym.size() > 0 ){
	    	String prefix =  ((String)paym[0].Id ).substring(0,3);
	        for(Payment__c p :paym){
	            p.Send_Email__c = true;
	        }
	        update paym;
	        return new Pagereference('/' + prefix);
    	}
    	else {
    		Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.WARNING,'עליך לבחור לפחות שורה אחת.'));
    		return null;
    	}
        
    }
}