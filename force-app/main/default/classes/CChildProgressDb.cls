public with sharing class CChildProgressDb 
{
	public static list<ChildProgress__c> GetChildProgressById(string cID)
    {
        return [select name, ReportBy__c, Date__c, Content__c, ChildName__c, ChildName__r.id 
        		from ChildProgress__c where id = :cID  order by Date__c];
    }
    
    public static list<ChildProgress__c> GetChildProgressById(list<string> cIDs)
    {
        return [select name, ReportBy__c, Date__c, Content__c, ChildName__c, ChildName__r.id 
        		from ChildProgress__c where id in :cIDs  order by Date__c];
    }
    
    public static list<ChildProgress__c> GetChildrenProgressByChildrenAndDate(list<string> childrenIDs)
    {
        return [select name, ReportBy__c, Date__c, Content__c, ChildName__c, ChildName__r.id 
        		from ChildProgress__c where ChildName__c in :childrenIDs and Date__c != null 
        		order by Date__c];
    }
    
    public static list<ChildProgress__c> GetChildProgressByChildAndDate(string childId)
    {
        return [select name, ReportBy__c, Date__c, Content__c, ChildName__c, ChildName__r.id 
        		from ChildProgress__c where ChildName__c = :childId and Date__c != null order by Date__c];
    }
    
    public static list<ChildProgress__c> GetChildProgressList()
    {
        return [select name, ReportBy__c, Date__c, Content__c, ChildName__c, ChildName__r.id 
        		from ChildProgress__c order by Date__c];
    }
    
    public static map<string, ChildProgress__c> GetChildProgressMap()
    {
        list<ChildProgress__c> childProgressList = CChildProgressDb.GetChildProgressList();
        map<string, ChildProgress__c> ret = new map<string, ChildProgress__c>();
		for (ChildProgress__c c : childProgressList)
		{
			ret.put(c.id, c);
		}
		return ret;
    }
}