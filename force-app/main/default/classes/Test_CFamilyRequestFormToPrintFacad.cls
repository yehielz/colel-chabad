@isTest
private class Test_CFamilyRequestFormToPrintFacad {

    static testMethod void TestCFamilyRequestFormToPrintFacad() {
		SchoolYear__c schoolYear = new SchoolYear__c();
        schoolYear.name = '2011-2012';
        schoolYear.IsActive__c = true;
        schoolYear.StartDate__c = Date.today();
        schoolYear.HalfSchoolYear__c = system.toDay();
        schoolYear.EndOfSchoolYear__c = system.toDay().addDays(356);
        insert schoolYear;
		Family__c f = new Family__c(Name = 'TEST');
		insert f;
		Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(f);
		CFamilyRequestFormToPrintFacad ctrl = new CFamilyRequestFormToPrintFacad(con);
		String tmp = ctrl.childrenHtmlToView;
		tmp = ctrl.childrenNumberAtHome;
		tmp = ctrl.diedFatherName;
		tmp = ctrl.diedMotherName;
		tmp = ctrl.email;
		tmp = ctrl.familyName;
		tmp = ctrl.fatherDeathCategory;
		tmp = ctrl.fatherDeathDate;
		tmp = ctrl.homePhone;
		tmp = ctrl.mobilePhone;
    }
}