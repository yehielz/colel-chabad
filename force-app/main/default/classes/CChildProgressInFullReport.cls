public with sharing class CChildProgressInFullReport
{
	public ChildProgress__c pChildProgress { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsProgressInView{ get { return pView.containsKey('all') || pView.containsKey('progress'); } }
	
	public CChildProgressInFullReport(ChildProgress__c mChildProgress, map<string, boolean> view)
	{
		pChildProgress = mChildProgress;
		pView = view;
	}
	public boolean IsChildProgressInView{ get { return pView.containsKey('all') || pView.containsKey('childProgress'); } }	
	public string pChildProgressName{ get{ return 'מספר הדיווח במערכת: ' + pChildProgress.name + '; '; } }
	public string ReportBy{ get{ return pChildProgress.ReportBy__c == null ? '' : 'דווח ע"י: ' + pChildProgress.ReportBy__c + '; '; } }
	public string Content{ get{ return pChildProgress.Content__c == null ? '' : 'תוכן הדיווח: ' + pChildProgress.Content__c + '; '; } }
    public string ReportDate
	{ 
		get
		{ 
			return pChildProgress.Date__c == null ? '' : 'תאריך דיווח: ' + CObjectNames.GetFullNumberInString(pChildProgress.Date__c.day()) + '/' + CObjectNames.GetFullNumberInString(pChildProgress.Date__c.month()) + '/' + pChildProgress.Date__c.year(); 
		} 
	}
	
	public string childProgressDetails
	{
		get
		{
			return GetHebrewWordsForProgress(/*pChildProgressName + ' ' + */ReportDate + ' ' + ReportBy + ' ' + Content, 90);
		}
	}
	
	public string ReportByWS{ get{ return pChildProgress.ReportBy__c == null ? '' : ' ' + pChildProgress.ReportBy__c; } }
	public string ContentWS{ get{ return pChildProgress.Content__c == null ? '' : ' ' + pChildProgress.Content__c; } }
    public string ReportDateWS
	{ 
		get
		{ 
			return pChildProgress.Date__c == null ? '' : ' ' + CObjectNames.GetFullNumberInString(pChildProgress.Date__c.day()) + '/' + CObjectNames.GetFullNumberInString(pChildProgress.Date__c.month()) + '/' + pChildProgress.Date__c.year(); 
		} 
	}
	
	public string childProgressDetailsWSEN
	{
		get
		{
			string mReportBy = ReportByWS != '' ? '<span class="TitleInContent">Reported By:</span> ' + ReportByWS : '';
			string mContent = ContentWS != '' ? '<span class="TitleInContent">Report Content:</span> ' + ContentWS : '';
			string mReportDate = ReportDateWS != '' ? '<span class="TitleInContent">Report Date:</span> ' + ReportDateWS : '';
			string ret = mReportDate;
			ret += ret == '' ? mReportBy : (mReportBy != '' ? ' | ' + mReportBy : '');
			ret += ret == '' ? mContent : (mContent != '' ? ' | ' + mContent : '');
			return ret.trim() != '' ? '<span class="color300800">Child progress:</span> ' + ret : '';
		}
	}
	public string childProgressDetailsWS
	{
		get
		{
			string mReportBy = ReportByWS != '' ? '<span class="TitleInContent">דווח ע"י:</span> ' + ReportByWS : '';
			string mContent = ContentWS != '' ? '<span class="TitleInContent">תוכן הדיווח:</span> ' + ContentWS : '';
			string mReportDate = ReportDateWS != '' ? '<span class="TitleInContent">תאריך דיווח:</span> ' + ReportDateWS : '';
			string ret = mReportDate;
			ret += ret == '' ? mReportBy : (mReportBy != '' ? ' | ' + mReportBy : '');
			ret += ret == '' ? mContent : (mContent != '' ? ' | ' + mContent : '');
			return ret.trim() != '' ? '<span class="color300800">התקדמות הילד:</span> ' + ret : '';
		}
	}
	
    public static string GetHebrewWordsForProgress(string thenote, integer length)
	{
		string ret = '';
		if (thenote != null)
		{
			string note = ' ' + thenote;
			integer noteLength = note.length();
			for (integer i = (length - 13); i < note.length(); i=length)
			{
				integer fromInSubS = i;
				integer minus = 0;
				while(note.substring(i-minus, ((i-minus)+1)) != ' ')
					minus++;
				i = i - minus;
				ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
				if (fromInSubS == (length - 13))
					ret += '<span class="gray"> ' + CObjectNames.getHebrewString('התקדמות הילד:') + '</span>';
				ret += '<br/>';
				note = note.substring(i, note.length());
			}
			ret += CObjectNames.getHebrewString(note);
			if (noteLength < (length - 13)) 
				ret+= '<span class="gray"> ' + CObjectNames.getHebrewString('התקדמות הילד:') + '</span>';
		}
		return ret;
	}
}