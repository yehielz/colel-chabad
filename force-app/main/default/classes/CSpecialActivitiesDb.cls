public with sharing class CSpecialActivitiesDb 
{
	private SpecialActivities__c pSpecialActivities { get; set; }
	private CSpecialActivitiesLineDb specialActivitiesLineDb { get; set; }
	public string currentMark{ get{ return specialActivitiesLineDb.currentMark; } set{ specialActivitiesLineDb.currentMark = value; } }
	public string Field{ get{ return specialActivitiesLineDb.Field; } set{ specialActivitiesLineDb.Field = value; } }
	public boolean IsOneLinePaid{ get{ return specialActivitiesLineDb.IsOnePaid; } }
	
	public CSpecialActivitiesDb()
	{
		pSpecialActivities = new SpecialActivities__c();
	}
	
	public CSpecialActivitiesDb(ApexPages.StandardController controller)
	{
		pSpecialActivities = (SpecialActivities__c)controller.getRecord();
		if (pSpecialActivities.id != null)
			SetpSpecialActivitiesBypSpecialActivitiesId(pSpecialActivities.id);
		specialActivitiesLineDb = new CSpecialActivitiesLineDb(pSpecialActivities);
	}
	
	private void SetpSpecialActivitiesBypSpecialActivitiesId(string id)
	{
		pSpecialActivities = [select name, id, Date__c, ActivityDescription__c, Field1ToFilter__c, Field2ToFilter__c, Field3ToFilter__c, 
							  Field4ToFilter__c, Field5ToFilter__c, Field6ToFilter__c, Field7ToFilter__c, Operator1ToFilter__c, Operator2ToFilter__c, 
							  Operator3ToFilter__c, Operator4ToFilter__c, Operator5ToFilter__c, Operator6ToFilter__c, Operator7ToFilter__c, 
							  Value1ToFilter__c, Value2ToFilter__c, Value3ToFilter__c, Value4ToFilter__c, Value5ToFilter__c, Value6ToFilter__c, Is_Active__c ,
							  Cost__c, Value7ToFilter__c, ActivityType__c, PaymentTo__c from SpecialActivities__c where id = :id];
	}
	
	public void InsertpSpecialActivities()
	{
		setpSpecialActivitiesName();
		if (pSpecialActivities.id == null)
			insert pSpecialActivities;
		else
			update pSpecialActivities;	
		specialActivitiesLineDb.InsertpSpecialActivitiesLines(pSpecialActivities);
	}
	
	private void setpSpecialActivitiesName()
	{
		if (pSpecialActivities.Date__c != null && pSpecialActivities.ActivityDescription__c == null)
			pSpecialActivities.name = pSpecialActivities.Date__c.day() + '/' + pSpecialActivities.Date__c.month() + '/' + pSpecialActivities.Date__c.year();
		else if (pSpecialActivities.Date__c == null && pSpecialActivities.ActivityDescription__c != null)
			pSpecialActivities.name = pSpecialActivities.ActivityDescription__c;
		else if (pSpecialActivities.Date__c != null && pSpecialActivities.ActivityDescription__c != null)
			pSpecialActivities.name = pSpecialActivities.Date__c.day() + '/' + pSpecialActivities.Date__c.month() + '/' + pSpecialActivities.Date__c.year() + ' - ' + 
												pSpecialActivities.ActivityDescription__c;
	}
	
	public SpecialActivities__c GetpSpecialActivities()
	{
		return pSpecialActivities;
	}
	
	public List<CRowSpecialActivitiesLines> GetpSpecialActivitiesLines()
	{
		return specialActivitiesLineDb.GetpSpecialActivitiesLines();
	}
	
	public void onChangeList(List<Family__c> families, list<Children__c> children)
	{
		specialActivitiesLineDb.onChangeList(families, children, pSpecialActivities.ActivityType__c);
	}
	
	public void SortList()
	{
		specialActivitiesLineDb.SortList();
	}
	
	public void SetNewList()
	{
		specialActivitiesLineDb.SetNewList();
	}
	
	/*public void OnChangeActivityType()
	{
		specialActivitiesLineDb.OnChangeActivityType(pSpecialActivities.ActivityType__c);
	}*/
}