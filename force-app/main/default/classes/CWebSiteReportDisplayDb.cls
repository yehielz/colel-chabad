public with sharing class CWebSiteReportDisplayDb 
{
	//private ActivityReport__c pActivityReport { get; set; }
	//private list<DayInActivityReport__c> pDaysInActivityReport { get; set; }
	
	public ActivityReport__c GetpActivityReport()
	{
		if (ApexPages.currentPage().getParameters().get('AR') != null)
		{
			return CActivityReportDb.GetActivityReportById(ApexPages.currentPage().getParameters().get('AR'));
		}
		return null;
	}
	
	public list<DayInActivityReport__c> GetpDaysInActivityReport()
	{
		if (ApexPages.currentPage().getParameters().get('AR') != null)
		{
			return CActivityReportLineDb.GetDaysInActivityReportByActivityReportId(ApexPages.currentPage().getParameters().get('AR'));
		}
		return null;
	}
}