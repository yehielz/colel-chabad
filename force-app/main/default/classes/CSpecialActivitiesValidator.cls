public with sharing class CSpecialActivitiesValidator 
{
	private CSpecialActivitiesFacad cSpecialActivitiesFacad { get; set; }
	public string ErrorMessage;
	
	public CSpecialActivitiesValidator(CSpecialActivitiesFacad ExtSpecialActivitiesFacad)
	{
		ErrorMessage = '';
		cSpecialActivitiesFacad = ExtSpecialActivitiesFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateDateOrActivityDescription();
		if (ret) 
	    	ret = ValidateActivityType();
	  	    
	 	return ret;
	}
	
	/*8private boolean ValidateAtLeastOneLineSelected()
	{
		for (integer i = 0; i < cSpecialActivitiesFacad.pSpecialActivitiesLines.size(); i++)
		{
			if (cSpecialActivitiesFacad.pSpecialActivitiesLines[i].IsInActivity__c == true)
			{
				return true;
			}
		}
		ErrorMessage = CErrorMessages.SpecialActivitiesDateOrActivityDescription;
		return false;
	}*/
	
	private boolean ValidateDateOrActivityDescription()
	{
		if (cSpecialActivitiesFacad.pSpecialActivities.Date__c == null && cSpecialActivitiesFacad.pSpecialActivities.ActivityDescription__c == null)
		{
			ErrorMessage = CErrorMessages.SpecialActivitiesDateOrActivityDescription;
			return false;
		}
		return true;
	}
	
	private boolean ValidateActivityType()
	{
		if (cSpecialActivitiesFacad.pSpecialActivities.ActivityType__c == null)
		{
			ErrorMessage = CErrorMessages.SpecialActivitiesActivityType;
			return false;
		}
		return true;
	}
}