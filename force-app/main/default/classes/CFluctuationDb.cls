public with sharing class CFluctuationDb 
{
	private list<Payment__c> pPayments {get; set;}
	private MasavFile__c pMasavFile {get; set;}
	private MasavSetting__c pMasavSetting {get; set;}
	private string pLines {get; set;}
	
	public CFluctuationDb(MasavFile__c mMasavFile, MasavSetting__c mMasavSetting)
	{
		pMasavFile = mMasavFile;
		pMasavSetting = mMasavSetting;
		SetMasavFileLines();
	}
	
	private void SetMasavFileLines()
	{
		//UpdatePayments();
		pPayments = [select id, FamilyName__r.AccountNumber__c, TutorName__r.AccountNumber__c, FamilyName__r.PersonalID__c, TutorName__r.PersonalID__c,
					 FamilyName__r.AffiliateNumber__c, TutorName__r.AffiliateNumber__c, Balance__c, TutorName__r.ID__c, MasavFile__c, Amount__c,
					 Contact__r.BankCode__c, Contact__r.AffiliateNumber__c, Contact__r.AccountNumber__c, Contact__r.ID__c, Contact__r.Name, Contact__r.PersonalID__c, 
					 FamilyName__r.BankCode__c, TutorName__r.BankCode__c, FamilyName__r.Name, FamilyName__r.ParentName__c, name, TutorName__r.Name, 
					 FamilyName__r.ParentId__c, AmountPaidByMasav__c, Debt__c, IsDone__c, Status__c, PaymentTarget__c from Payment__c 
					 where MasavFile__c = :pMasavFile.id];
	}
	
	/*private void UpdatePayments()
	{
		Payment__c[] paymentsToUpdate = [select id, MasavFile__c from Payment__c where MasavFile__c = :pMasavFile.id];
		for (Payment__c payment : paymentsToUpdate)
		{
			payment.MasavFile__c = null;
		}
		if (paymentsToUpdate.size() > 0)
			update paymentsToUpdate;
	}*/
	
	public string GetLines()
	{
		pLines = '';
		pMasavFile.TotalPayments__c = 0;
		for (Payment__c line : pPayments)
		{
			line.MasavFile__c = pMasavFile.id;
			pLines += pLines == '' ? GetLine(line) : '\r\n' + GetLine(line);
			SetPaymentStatus(line);
		}
		pMasavFile.NumberOfPayments__c = pPayments.size();
		if (pMasavFile != null && pMasavFile.id != null)
		{
			CCreateFileFacad.IsAfterUpdateFields = true;
			update pMasavFile;
		}
		if (pPayments.size() > 0)
			update pPayments;
		return pLines;
	}
	
	private void SetPaymentStatus(Payment__c line)
	{
		decimal balance = line.Balance__c != null ? line.Balance__c : 0;
		line.Amount__c = line.Amount__c == null ? 0 : line.Amount__c;
		line.AmountPaidByMasav__c = line.AmountPaidByMasav__c == null ? 0 : line.AmountPaidByMasav__c;
		line.Amount__c += balance;
		line.AmountPaidByMasav__c += balance;
		pMasavFile.TotalPayments__c += line.AmountPaidByMasav__c;
		integer debt = math.round(line.Debt__c);
		integer amount = math.round(line.Amount__c);
		line.IsDone__c = line.Amount__c != null && line.Amount__c > 0;
		if (amount == null || !(amount > 0))
			line.Status__c = CObjectNames.PaymentStatusNotPaid;
		else if (amount > 0 && amount < debt)
			line.Status__c = CObjectNames.PaymentStatusHalfPaid;
		else if (amount > 0 && amount >= debt)
			line.Status__c = CObjectNames.PaymentStatusPaid;
		else
			line.Status__c = CObjectNames.PaymentStatusNotPaid;
	}
	
	private string GetLine(Payment__c payment)
	{
		string ret = '1';
		if (payment.PaymentTarget__c == 'חונך')
		{
			ValidateField(payment.TutorName__c, 'שם חונך', payment.id, payment.name, 'תשלום');
			ValidateField(pMasavSetting.InstitutionSubject__c, 'מוסד/נושא', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
			ret += pMasavSetting.InstitutionSubject__c;
			ret += GetLen('', 2, '0');
			ret += GetLen('', 6, '0');
			ValidateField(payment.TutorName__r.name, 'שם חונך', payment.TutorName__c, 'ריק', 'חונך');
			ValidateField(payment.TutorName__r.BankCode__c, 'קוד בנק', payment.TutorName__c, payment.TutorName__r.name, 'חונך');
			ret += GetLen(string.valueof(payment.TutorName__r.BankCode__c), 2, '0');
			ValidateField(payment.TutorName__r.AffiliateNumber__c, 'מספר סניף', payment.TutorName__c, payment.TutorName__r.name, 'חונך');
			ret += GetLen(string.valueof(payment.TutorName__r.AffiliateNumber__c), 3, '0');
			ret += GetLen('', 4, '0');
			ValidateField(payment.TutorName__r.AccountNumber__c, 'מספר חשבון', payment.TutorName__c, payment.TutorName__r.name, 'חונך');
			ret += GetLen(string.valueof(payment.TutorName__r.AccountNumber__c), 9, '0');
			ret += GetLen('', 1, '0');
			ret += GetLen((payment.TutorName__r.Id__c == null ? '' : string.valueof(payment.TutorName__r.Id__c)), 9, '0');
			string name = (payment.FamilyName__r.ParentName__c + ' ' + payment.FamilyName__r.Name).trim();
			name = name.length()>16 ? name.substring(0,16) : name ;
			ret += GetLen(GetOldCode(name), 16, ' ');
			ValidateField(payment.Balance__c, 'יתרה לתשלום', payment.id, payment.name, 'תשלום');
			decimal amount = payment.Balance__c != null && payment.AmountPaidByMasav__c != null ? payment.AmountPaidByMasav__c + payment.Balance__c : (payment.Balance__c == null && payment.AmountPaidByMasav__c != null ? payment.AmountPaidByMasav__c : (payment.Balance__c != null ? payment.Balance__c : 0));
			ret += GetLen(GetNumbersAfterPoint(string.valueof(amount)), 13, '0');
			ret += GetLen((payment.TutorName__r.PersonalID__c == null ? '' : payment.TutorName__r.PersonalID__c), 20, '0');
			ValidateField(pMasavFile.FromDate__c, 'לתשלומים מתאריך', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
			ValidateField(pMasavFile.ToDate__c, 'לתשלומים עד תאריך', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
			ret += GetDate(pMasavFile.FromDate__c, pMasavFile.ToDate__c);
			ret += GetLen('', 3, '0');
			ret += GetLen('6', 3, '0');
			ret += GetLen('', 18, '0');
			ret += GetLen('', 2, ' ');
		}
		else if (payment.PaymentTarget__c == 'איש קשר')
		{
			ValidateField(payment.Contact__c, 'שם איש קשר', payment.id, payment.name, 'תשלום');
			ValidateField(pMasavSetting.InstitutionSubject__c, 'מוסד/נושא', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
			ret += pMasavSetting.InstitutionSubject__c;
			ret += GetLen('', 2, '0');
			ret += GetLen('', 6, '0');
			ValidateField(payment.Contact__r.Name, 'שם איש קשר', payment.Contact__c, 'ריק', 'איש קשר');
			ValidateField(payment.Contact__r.BankCode__c, 'קוד בנק', payment.Contact__c, payment.Contact__r.name, 'איש קשר');
			ret += GetLen(string.valueof(payment.Contact__r.BankCode__c), 2, '0');
			ValidateField(payment.Contact__r.AffiliateNumber__c, 'מספר סניף', payment.Contact__c, payment.Contact__r.name, 'איש קשר');
			ret += GetLen(string.valueof(payment.Contact__r.AffiliateNumber__c), 3, '0');
			ret += GetLen('', 4, '0');
			ValidateField(payment.Contact__r.AccountNumber__c, 'מספר חשבון', payment.Contact__c, payment.Contact__r.name, 'איש קשר');
			ret += GetLen(string.valueof(payment.Contact__r.AccountNumber__c), 9, '0');
			ret += GetLen('', 1, '0');
			ret += GetLen((payment.Contact__r.Id__c == null ? '' : string.valueof(payment.Contact__r.Id__c)), 9, '0');
			string name = (payment.Contact__r.Name + ' ' + payment.Contact__r.Name).trim();
			name = name.length()>16 ? name.substring(0,16) : name ;
			ret += GetLen(GetOldCode(name), 16, ' ');
			ValidateField(payment.Balance__c, 'יתרה לתשלום', payment.id, payment.name, 'תשלום');
			decimal amount = payment.Balance__c != null && payment.AmountPaidByMasav__c != null ? payment.AmountPaidByMasav__c + payment.Balance__c : (payment.Balance__c == null && payment.AmountPaidByMasav__c != null ? payment.AmountPaidByMasav__c : (payment.Balance__c != null ? payment.Balance__c : 0));
			ret += GetLen(GetNumbersAfterPoint(string.valueof(amount)), 13, '0');
			ret += GetLen((payment.Contact__r.PersonalID__c == null ? '' : payment.Contact__r.PersonalID__c), 20, '0');
			ValidateField(pMasavFile.FromDate__c, 'לתשלומים מתאריך', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
			ValidateField(pMasavFile.ToDate__c, 'לתשלומים עד תאריך', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
			ret += GetDate(pMasavFile.FromDate__c, pMasavFile.ToDate__c);
			ret += GetLen('', 3, '0');
			ret += GetLen('6', 3, '0');
			ret += GetLen('', 18, '0');
			ret += GetLen('', 2, ' ');
		}
		else
		{
			ValidateField(payment.FamilyName__c, 'משפחה', payment.id, payment.name, 'תשלום');
			ValidateField(pMasavSetting.InstitutionSubject__c, 'מוסד/נושא', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
			ret += pMasavSetting.InstitutionSubject__c;
			ret += GetLen('', 2, '0');
			ret += GetLen('', 6, '0');
			ValidateField(payment.FamilyName__r.name, 'שם משפחה', payment.FamilyName__c, 'ריק', 'משפחה');
			ValidateField(payment.FamilyName__r.BankCode__c, 'קוד בנק', payment.FamilyName__c, payment.FamilyName__r.name, 'משפחה');
			ret += GetLen(string.valueof(payment.FamilyName__r.BankCode__c), 2, '0');
			ValidateField(payment.FamilyName__r.AffiliateNumber__c, 'מספר סניף', payment.FamilyName__c, payment.FamilyName__r.name, 'משפחה');
			ret += GetLen(string.valueof(payment.FamilyName__r.AffiliateNumber__c), 3, '0');
			ret += GetLen('', 4, '0');
			ValidateField(payment.FamilyName__r.AccountNumber__c, 'מספר חשבון', payment.FamilyName__c, payment.FamilyName__r.name, 'משפחה');
			ret += GetLen(string.valueof(payment.FamilyName__r.AccountNumber__c), 9, '0');
			ret += GetLen('', 1, '0');
			ret += GetLen((payment.FamilyName__r.ParentId__c == null ? '' : string.valueof(payment.FamilyName__r.ParentId__c)), 9, '0');
			ValidateField(payment.FamilyName__r.ParentName__c, 'שם ההורה', payment.FamilyName__c, payment.FamilyName__r.name, 'משפחה');
			string name = getHebrewString((payment.FamilyName__r.ParentName__c + ' ' + payment.FamilyName__r.Name).trim());
			name = name.length()>16 ? name.substring(0,16) : name ;
			ret += GetLen(GetOldCode(name), 16, ' ');
			ValidateField(payment.Balance__c, 'יתרה לתשלום', payment.id, payment.name, 'תשלום');
			decimal amount = payment.Balance__c != null && payment.AmountPaidByMasav__c != null ? payment.AmountPaidByMasav__c + payment.Balance__c : (payment.Balance__c == null && payment.AmountPaidByMasav__c != null ? payment.AmountPaidByMasav__c : (payment.Balance__c != null ? payment.Balance__c : 0));
			ret += GetLen(GetNumbersAfterPoint(string.valueof(amount)), 13, '0');
			ret += GetLen((payment.FamilyName__r.PersonalID__c != null ? payment.FamilyName__r.PersonalID__c : ''), 20, '0');
			ValidateField(pMasavFile.FromDate__c, 'לתשלומים מתאריך', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
			ValidateField(pMasavFile.ToDate__c, 'לתשלומים עד תאריך', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
			ret += GetDate(pMasavFile.FromDate__c, pMasavFile.ToDate__c);
			ret += GetLen('', 3, '0');
			ret += GetLen('6', 3, '0');
			ret += GetLen('', 18, '0');
			ret += GetLen('', 2, ' ');
		}
		return ret;
	}
	
	private string GetOldCode(string name)
	{
		map<string, string> chrsMap = new map<string, string>();
		chrsMap.put('א', '&');
		chrsMap.put('ב', 'A');
		chrsMap.put('ג', 'B');
		chrsMap.put('ד', 'C');
		chrsMap.put('ה', 'D');
		chrsMap.put('ו', 'E');
		chrsMap.put('ז', 'F');
		chrsMap.put('ח', 'G');
		chrsMap.put('ט', 'H');
		chrsMap.put('י', 'I');
		chrsMap.put('ך', 'J');
		chrsMap.put('כ', 'K');
		chrsMap.put('ל', 'L');
		chrsMap.put('ם', 'M');
		chrsMap.put('מ', 'N');
		chrsMap.put('ן', 'O');
		chrsMap.put('נ', 'P');
		chrsMap.put('ס', 'Q');
		chrsMap.put('ע', 'R');
		chrsMap.put('ף', 'S');
		chrsMap.put('פ', 'T');
		chrsMap.put('ץ', 'U');
		chrsMap.put('צ', 'V');
		chrsMap.put('ק', 'W');
		chrsMap.put('ר', 'X');
		chrsMap.put('ש', 'Y');
		chrsMap.put('ת', 'Z');
		chrsMap.put(' ', ' ');
		string oldCode ='';
		
		for (integer i=0; i < name.length(); i++)
		{
			oldCode += chrsMap.containskey(name.substring(i, i+1)) ? chrsMap.get(name.substring(i, i+1)) : name.substring(i, i+1);
		}
		return oldCode;
	}
	
	private string GetDate(date startDate, date endDate)
	{
		String sYear = String.valueof(startDate.year());
		string y = sYear.substring(2);
		String eYear = String.valueof(endDate.year());
		string z = eYear.substring(2);
		String sMonth = GetLen(String.valueof(startDate.month()), 2, '0');
		String eMonth = GetLen(String.valueof(endDate.month()), 2, '0');
		string tToday = y + sMonth + z + eMonth;
		return tToday;
	}
	
	private string GetLen(string field, integer len, string letter)
	{
		while (field.length() < len)
		{
			field = letter + field;
		}
		return field;
	}
	
	private string GetNumbersAfterPoint(string field)
	{
		string[] slist = field.split('\\.');
        if (slist.Size() < 2)
         	return slist[0] + '00';
        else 
        {
            string secondPart = slist[1];
            if (secondPart.Length() < 2)
                return slist[0] + secondPart + '0';
            else
            	return slist[0] + secondPart.substring(0, 2);    
        }
	}
	
	private void ValidateField(object fieldValue, string fieldName, string onRecordId, string onRecordName, string onRecordType)
	{
		if ((fieldValue == null || string.valueOf(fieldValue) == 'null' || string.valueOf(fieldValue).trim() == '') && fieldName != null && onRecordId != null && onRecordName != null && onRecordType != null)
		{
			throw new CChesedException('ישנו שדה ריק!! (שם שדה: \'' + fieldName + '\', שם אובייקט: \'' + onRecordType + '\', שם רשומה: \'' + onRecordName + '\', מזהה רשומה: \'' + onRecordId + '\')', false);
		}
	}
	public static string getHebrewString(string reverseString)
    {
    	if (!isWithHebrew(reverseString))
    		return reverseString;
    	string ret = '';
    	string[] stringList = reverseString.split(' ');
    	string english = '';
    	for (integer i = stringList.size() - 1; i >= 0; i--)
    	{
    		if (isWithHebrew(stringList[i]))
    		{
    			ret += english;
    			english = '';
    			string s = '';
    			for (integer a = stringList[i].length() - 1; a >= 0; a--)
    			{
    				s += stringList[i].substring(a, a+1);
    			}
    			ret += s + ' ';
    		}
    		else
    		{
    			english = stringList[i] + ' ' + english + ' '; 
    		}
    	}
    	ret += english;
    	if (ret.endsWith(' ') && !reverseString.endsWith(' '))
    		ret = ret.substring(0, ret.length() - 1);
    	return ret;
    }
    
    public static boolean isWithHebrew(string toCheck)
    {
    	if (toCheck == '+' || toCheck == '-' || toCheck == ':' || toCheck == '/')
    		return true;
    	for (integer i = 0; i < toCheck.length(); i++)
    	{
    		string s = toCheck.substring(i, i+1);
    		if (s >= 'א' && s <= 'ת')
    		{
    			return true;
    		}
    	}
    	return false;
    }
	
}