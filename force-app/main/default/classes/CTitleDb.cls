public with sharing class CTitleDb 
{
	private MasavFile__c pMasavFile {get; set;}
	private MasavSetting__c pMasavSetting {get; set;}
	private string pTitle {get; set;}
	
	public CTitleDb(MasavFile__c mMasavFiles, MasavSetting__c mMasavSetting)
	{
		pMasavFile = mMasavFiles;
		pMasavSetting = mMasavSetting;
	}
	
	public string GetTitle()
	{
		pTitle = 'K';
		ValidateField(pMasavSetting.InstitutionSubject__c, 'מוסד/נושא', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
		pTitle += pMasavSetting.InstitutionSubject__c;
		pTitle += GetLen('', 2, '0');
		ValidateField(pMasavFile.PaymentDate__c, 'תאריך התשלום', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
		pTitle += GetDate(pMasavFile.PaymentDate__c);
		pTitle += GetLen('', 1, '0');
		pTitle += GetLen('1', 3, '0');
		pTitle += '0';
		pTitle += GetDate(Date.today());
		ValidateField(pMasavSetting.InstitutionSends__c, 'מוסד שולח', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
		pTitle += GetLen(string.valueof(pMasavSetting.InstitutionSends__c), 5, '0');
		pTitle += GetLen('', 6, '0');
		ValidateField(pMasavSetting.NameOfInstitutionSubject__c, 'שם המוסד / נושא', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
		pTitle += GetLen(GetOldCode(getHebrewString(pMasavSetting.NameOfInstitutionSubject__c)), 30, ' ');
		pTitle += GetLen('', 56, ' ');
		pTitle += 'KOT';
		return pTitle;
	}
	private string GetOldCode(string name)
	{
		map<string, string> chrsMap = new map<string, string>();
		chrsMap.put('א', '&');
		chrsMap.put('ב', 'A');
		chrsMap.put('ג', 'B');
		chrsMap.put('ד', 'C');
		chrsMap.put('ה', 'D');
		chrsMap.put('ו', 'E');
		chrsMap.put('ז', 'F');
		chrsMap.put('ח', 'G');
		chrsMap.put('ט', 'H');
		chrsMap.put('י', 'I');
		chrsMap.put('ך', 'J');
		chrsMap.put('כ', 'K');
		chrsMap.put('ל', 'L');
		chrsMap.put('ם', 'M');
		chrsMap.put('מ', 'N');
		chrsMap.put('ן', 'O');
		chrsMap.put('נ', 'P');
		chrsMap.put('ס', 'Q');
		chrsMap.put('ע', 'R');
		chrsMap.put('ף', 'S');
		chrsMap.put('פ', 'T');
		chrsMap.put('ץ', 'U');
		chrsMap.put('צ', 'V');
		chrsMap.put('ק', 'W');
		chrsMap.put('ר', 'X');
		chrsMap.put('ש', 'Y');
		chrsMap.put('ת', 'Z');
		chrsMap.put(' ', ' ');
		string oldCode ='';
		
		for (integer i=0; i < name.length(); i++)
		{
			oldCode += chrsMap.containskey(name.substring(i, i+1)) ? chrsMap.get(name.substring(i, i+1)) : name.substring(i, i+1);
		}
		return oldCode;
	}
	private string GetDate(date dateToday)
	{
		String sYear = String.valueof(dateToday.year());
		string y = sYear.substring(2);
		String sMonth = GetLen(String.valueof(dateToday.month()), 2, '0');
		String sDay = GetLen(String.valueof(dateToday.day()), 2, '0');
		string sToday = y + sMonth + sDay;
		return sToday;
	}
	private string GetLen(string field, integer len, string letter)
	{
		for(integer i = 1; i <= len; i++)
		{
			if(field.length() < i)
				field = letter + field;
		}
		return field;
	}
	
	private void ValidateField(object fieldValue, string fieldName, string onRecordId, string onRecordName, string onRecordType)
	{
		if ((fieldValue == null || string.valueOf(fieldValue) == 'null' || string.valueOf(fieldValue).trim() == '') && fieldName != null && onRecordId != null && onRecordName != null && onRecordType != null)
		{
			throw new CChesedException('ישנו שדה ריק!! (שם שדה: \'' + fieldName + '\', שם אובייקט: \'' + onRecordType + '\', שם רשומה: \'' + onRecordName + '\', מזהה רשומה: \'' + onRecordId + '\')');
		}
	}
	public static string getHebrewString(string reverseString)
    {
    	if (!isWithHebrew(reverseString))
    		return reverseString;
    	string ret = '';
    	string[] stringList = reverseString.split(' ');
    	string english = '';
    	for (integer i = stringList.size() - 1; i >= 0; i--)
    	{
    		if (isWithHebrew(stringList[i]))
    		{
    			ret += english;
    			english = '';
    			string s = '';
    			for (integer a = stringList[i].length() - 1; a >= 0; a--)
    			{
    				s += stringList[i].substring(a, a+1);
    			}
    			ret += s + ' ';
    		}
    		else
    		{
    			english = stringList[i] + ' ' + english + ' '; 
    		}
    	}
    	ret += english;
    	if (ret.endsWith(' ') && !reverseString.endsWith(' '))
    		ret = ret.substring(0, ret.length() - 1);
    	return ret;
    }
    
    public static boolean isWithHebrew(string toCheck)
    {
    	if (toCheck == '+' || toCheck == '-' || toCheck == ':' || toCheck == '/')
    		return true;
    	for (integer i = 0; i < toCheck.length(); i++)
    	{
    		string s = toCheck.substring(i, i+1);
    		if (s >= 'א' && s <= 'ת')
    		{
    			return true;
    		}
    	}
    	return false;
    }
	
	/*private string SetSerialNum(string field)
	{
		string[] slist = field.split('\\-');
         	return slist[1];
	}*/
}