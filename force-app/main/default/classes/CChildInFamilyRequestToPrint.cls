public with sharing class CChildInFamilyRequestToPrint 
{
	private ChildRegistration__c pChildRegistration { get; set; }
	public CChildInFamilyRequestToPrint(ChildRegistration__c childRegistration)
	{
		pChildRegistration = childRegistration;
	}
	
	public string childName 
	{
		get
		{
			if (pChildRegistration.ChildName__c != null && pChildRegistration.ChildName__r.name != null)
				return '.' + CObjectNames.getHebrewString(pChildRegistration.ChildName__r.name) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('שם ' + (pChildRegistration.ChildName__r.Gender__c == 'נקבה' ? 'בת' : 'בן') + ' המשפחה:') + ' </label>';
			return '';
		}
	}
	public string birthDate 
	{
		get
		{
			if (pChildRegistration.ChildName__c != null && pChildRegistration.ChildName__r.BirthDate__c == null)
				return '';
			return '.' + CObjectNames.getHebrewString(pChildRegistration.ChildName__r.BirthDate__c.day() + '/' + 
					pChildRegistration.ChildName__r.BirthDate__c.month() + '/' + pChildRegistration.ChildName__r.BirthDate__c.year()) +
					'<label class="head"> ' + CObjectNames.getHebrewString('תאריך לידה: ') + ' </label>';
		}
	}
	public string idNumber
	{
		get
		{
			if (pChildRegistration.ChildName__c != null && pChildRegistration.ChildName__r.IdNumber__c != null)
				return '.' + CObjectNames.getHebrewString(pChildRegistration.ChildName__r.IdNumber__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('ת.ז. בן/ת המשפחה: ') + ' </label>';
			return '';
		}
	}
	
	public string classAndSchool
	{
		get
		{
			if (pChildRegistration.Class__c != null || (pChildRegistration.SchoolName__c != null && pChildRegistration.SchoolName__r.name != null))
			{
				string ret = '.';
				if (pChildRegistration.SchoolName__c != null && pChildRegistration.SchoolName__r.name != null)
					ret += CObjectNames.getHebrewString(pChildRegistration.SchoolName__r.name) + '<label class="head"> ' + CObjectNames.getHebrewString('בי"ס: ') + ' </label>';
				if (pChildRegistration.Class__c != null)
					ret +=  ' ,' + CObjectNames.getHebrewString(pChildRegistration.Class__c) + '<label class="head"> ' + CObjectNames.getHebrewString('כיתה: ') + ' </label>';
				return ret;
			}
			return '';
		}
	}
	
	public string teacherName
	{
		get
		{
			if (pChildRegistration.Educator__c != null)
				return CObjectNames.getHebrewString(pChildRegistration.Educator__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('שם המחנכ/ת: ') + ' </label>';
			return '';
		}
	}
	
	public string teacherPhone
	{
		get
		{
			if (pChildRegistration.EducatorPhoneNumber__c != null)
				return '.' + CObjectNames.getHebrewString(pChildRegistration.EducatorPhoneNumber__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('טלפון: ') + ' </label>';
			return '';
		}
	}
	
	public string noteToPrintPage 
	{ 
		get
		{ 
			if (pChildRegistration.ChildName__c != null && pChildRegistration.ChildName__r.NoteToPrintPage__c != null)
			{
				return '<p>.' + CObjectNames.getHebrewString(pChildRegistration.ChildName__r.NoteToPrintPage__c) + '<label class="head"> ' + 
						CObjectNames.getHebrewString('הערה ספציפית לבן/ת המשפחה: ') + ' </label>' + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>';
			}
			return ''; 
		} 
	}
}