public with sharing class CMultiSelectComponentController 
{
	public list<selectOption> options { get; set; }
	public list<selectOption> selectedOptions { get; set; }
	public list<selectOption> Available { get; set; }
	public list<selectOption> Chosen { get; set; }
	public string pChosenString { get; set; }
	private string mListSize;
	public string pListSize 
	{ 
		get
		{
			if (mListSize != null && IsNumber(mListSize))
				return mListSize;
			return '5';
		}
		set
		{
			mListSize = value;
		}
	}
	
	private boolean IsNumber(string s)
	{
		for (integer i = 0; i < s.length(); i++)
		{
			string s1 = s.substring(i, i + 1);
			if (s1 != '0' && s1 != '1' && s1 != '2' && s1 != '3' && s1 != '4' && s1 != '5' && s1 != '6' && s1 != '7' && s1 != '8' && s1 != '9')
				return false;
		}
		return true;
	}
	
	public string selected { get; set; }
	public string deselected { get; set; }
	
	public CMultiSelectComponentController()
	{ 
		
	}
	
	public void FillSelectedOptionsList()
	{
		if (pChosenString == null && (selectedOptions == null || selectedOptions.size() < 1))
			return;
		list<string> ChosenList = pChosenString != null ? pChosenString.split(';') : new list<string>();
		if (selectedOptions.size() > 0)
		{
			for (selectOption option : selectedOptions)
			{
				ChosenList.add(option.getValue());
			}
		}	
		selectedOptions.clear();
		for (integer i = 0; i < ChosenList.size(); i++)
		{
			for (integer a = 0; a < options.size(); a++)
			{
				if (ChosenList[i] == options[a].getValue())
				{
					selectedOptions.add(new selectOption(options[a].getValue(), options[a].getLabel()));
					options.remove(a);
				}
			}
		}
	}
	
	public void selecting()
	{
		for (integer i = 0; i < options.size(); i++)
		{
			if (selected.contains(options[i].getvalue()))
			{
				selectedOptions.add(options[i]);
			}
		}
		sendToRemoveFromOptions();
		/*for(String toSelect: selected)
		{
			for(integer i = 0; i < options.size(); i++)
			{
				if(options[i].getvalue() == toSelect.substring(0, toSelect.length()))
				  	selectedOptions.add(new SelectOption(toSelect,toSelect));
				else
					newOptionsList.add(new SelectOption(toSelect,toSelect));
				options
			}
		}*/
		
	}
	
	private void sendToRemoveFromOptions()
	{
		for (integer i = 0; i < options.size(); i++)
		{
			if (selected.contains(options[i].getvalue()))
			{
				RemoveFromOptions(i);
			}
		}
	}
	
	private void RemoveFromOptions(integer i)
	{
		options.remove(i);
		sendToRemoveFromOptions();
	}
	 
	public void deselecting()
	{ 
		for (integer i = 0; i < selectedOptions.size(); i++)
		{
			if (deselected.contains(selectedOptions[i].getvalue()))
			{
				options.add(selectedOptions[i]);
			}
		}
		sendToRemoveFromSelectedOptions();
		/*for(String toDeselect: deselected)
		{            
			for(integer i = 0; i < selectedOptions.size(); i++)
			{
				if(selectedOptions[i].getvalue()==toDeselect)
				{
					options.add(new SelectOption(toDeselect, toDeselect));
					selectedOptions.remove(i);
				}
			}
		}*/
	}
	
	private void sendToRemoveFromSelectedOptions()
	{
		for (integer i = 0; i < selectedOptions.size(); i++)
		{
			if (deselected.contains(selectedOptions[i].getvalue()))
			{
				RemoveFromSelectedOptions(i);
			}
		}
	}
	
	private void RemoveFromSelectedOptions(integer i)
	{
		selectedOptions.remove(i);
		sendToRemoveFromSelectedOptions();
	}
	
	private static string MULTIPICKLIST_SEPERATOR = ';';
	public static string combineOptions(List<SelectOption> values)
	{
		 String result = '';
		 for(SelectOption s: values)
		 {
		  	result = result == '' ? s.getValue() : result + MULTIPICKLIST_SEPERATOR + s.getValue();
		 }
		 return result.length()  > 0 ? result.substring(0, result.length()): result;
	}
}