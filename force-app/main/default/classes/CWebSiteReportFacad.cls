public without sharing class CWebSiteReportFacad 
{
	public transient blob fileBody{get;set;}
	public string fileName{get;set;}
	public ContentVersion attachment { get; set;}

	public static boolean isWithDays;
	public static boolean getisWithDays(){ return isWithDays;}
	public static void setisWithDays(boolean a) {isWithDays = a;}

	public static Id ActivityReportId;
	//public static Id getActivityReportId(){ return ActivityReportId;}
	//public static void setActivityReportId(Id a) {ActivityReportId = a;}
	
	

	public static string myProblems;
	private string mMonth { get; set; }
	public string pMonth 
	{ 
		get
		{
			if (mMonth == null)
			{
				if (ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().get('month') != null)
					mMonth = ApexPages.currentPage().getParameters().get('month');
			}
			return mMonth;
		} 
	}
	
	public static String getmyProblems(){ return myProblems;}
	public static void setmyProblems(String a) {myProblems = a;}

	private Tutor__c tutor { get; set; }
	public Tutor__c pTutor { 
		get{
			if (tutor == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null)
						tutor = CTutorDb.GetTutorById(sessionId.TutorName__c);
				}
			}
			return tutor;
		} 
	}
	
	private ActivityApproval__c approval { get; set; }
	public ActivityApproval__c pApproval{ 
		get{
			if (approval == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null)
						approval = CActivityApprovalDb.GetActivityApprovalById(sessionId.ActivityApproval__c);
				}
			}
			return approval;
		} 
	}
	
	private Family__c family { get; set; }
    public Family__c pfamily { 
        get{
            if (family == null) {
                if (ApexPages.currentPage().getCookies().get('sessionId') != null){
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null)
                        family = CFamilyDb.GetFamilyById(sessionId.Family__c);
                }
            }
            return family;
        } 
    }
	
	private string currentChild;
	private Children__c child;
	public string pCurrentChild 
	{
		get
		{
			if (currentChild == null)
			{
				String childId = ApexPages.currentPage().getParameters().get('childId');
				if (childId != null)
				{
					Children__c[] childs = [select name from Children__c where id = :childId];
					if (childs.size() > 0){
						child = childs[0];
						currentChild = childs[0].name;
					}
				}
				currentChild = currentChild == null ? '' : currentChild;
			}
			return currentChild;
		}
	}
	
	private string currentProfession;
	public string pCurrentProfession
	{
		get
		{
			if (currentProfession == null)
			{
				if (ApexPages.currentPage().getParameters().get('professionId') != null)
				{
					Profession__c[] profession = CProfessionDb.GetProfessionsById(ApexPages.currentPage().getParameters().get('professionId'));
					if (profession.size() > 0)
						currentProfession = profession[0].name;
				}
				currentProfession = currentProfession == null ? '' : currentProfession;
			}
			return currentProfession;
		}
	}
	
	private CActivityReportDb activityReportDb;
	public list<ExpenditureExceeded__c> pExpenditureExceededs { get; set; }
	
	public CWebSiteReportFacad()
	{	
		myProblems = '';
		activityReportDb = new CActivityReportDb(new ActivityReport__c());
		pExpenditureExceededs = new list<ExpenditureExceeded__c> { new ExpenditureExceeded__c() };
	}
	
	public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
			{
				if (!CInterface.ValidateMonth())
					return new PageReference(CObjectNames.WebSitePageMonthes); 
				if (!ValidateChild())
					return new PageReference(CObjectNames.WebSitePageMonthes); 
				if (!ValidateProfession())
					return new PageReference(CObjectNames.WebSitePageMonthes); 
				return null;
			}
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
	
	public static String updateFileName(String tmpName, String fname) {
		String newName = '';
		//newName = tmpName + '-' + tmpType;
		if (fname == null)
			newName = tmpName+ '.pdf';
		else
			newName = tmpName+ fname.right(4);
		return newName;
	  }

	
	public static Attachment myfile;
	public static blob file1{get;set;}
	public static blob getfile1(){ return file1;}
	public static void setfile1(blob a) {file1 = a;}
	public static string fname{get;set;}

	public String myString {get; set;}
	public PageReference myMethod(){
        return null;
    }

	public String myString2 {get; set;}
	public PageReference myMethod2(){
        return null;
    }
	public String myString3 {get; set;}
	public PageReference myMethod3(){
        return null;
    }
	public String myString4 {get; set;}
	public PageReference myMethod4(){
        return null;
    }
	public String myString5 {get; set;}
	public PageReference myMethod5(){
        return null;
    }
	public Decimal myString6 {get; set;}
	public PageReference myMethod6(){
        return null;
    }
	public Decimal myString7 {get; set;}
	public PageReference myMethod7(){
        return null;
    }

	public blob newFile{get;set;}
	public PageReference myNewFile(){
        return null;
    }

	public PageReference Savedoc() {
		myfile = new Attachment();
		Id myId = ActivityReportId;
		System.debug('ActivityReportId - ' + ActivityReportId);
		System.debug('myId - ' + myId);
		//Id myId = getActivityReportId();


		if(myString2 != null && String.isnotEmpty(myString)){
			myFile.name = myString2;
			system.debug( '***********' +  myString.split( ',' ).size());
			myString = myString.split( ',' ).size() > 1 ?  myString.split( ',' )[1] : myString;
			myFile.body =  EncodingUtil.base64Decode(myString);

			Attachment a = new Attachment(parentId = myId, name = myfile.name, body = myfile.body);
			System.debug('a - ' + a);

			/* insert the attachment */
				insert a; //parentId null
			
			
		}
		return NULL;
	}   

	public PageReference save()
	{	
		String a = '';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		a='';
		String childId = ApexPages.currentPage().getParameters().get('childId');
		String professionId = ApexPages.currentPage().getParameters().get('professionId') ;
		SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
		a='';
		ActivityApproval__c myActivityApproval = new ActivityApproval__c();
		if (sessionId != null && sessionId.ActivityApproval__c != null){
			myActivityApproval = CActivityApprovalDb.GetActivityApprovalById(sessionId.ActivityApproval__c);
		}
		else{
			myActivityApproval = [SELECT Id, Name, TutorName__c FROM ActivityApproval__c WHERE  ChildName__c = :childId AND ProfessionName__c = :professionId AND IsApprovalOnProbation__c != true LIMIT 1]; 
		}


		if (ValidateDaysInActivityReport() && myActivityApproval.TutorName__c != null)
		{
			SetProblemsCookie(true);
			ActivityReportId = pActivityReport.id;
			Savedoc();
			return null;
		}
		activityReportDb.SetPropertiesInWebSite(reportNote, childProgress, pExpenditureExceededs.size() > 0);

		Date x = Date.newinstance(1970, 1, 1);
		if(myString5 != null && myString5 != '')
			x = Date.valueOf(myString5);
		// else
		// 	x = Date.newinstance(1970, 1, 1);

		activityReportDb.SetInfosUploadFile(myString3, myString4, x, myString6, myString7);

		System.debug('Test pour upload des fichiers');
		System.debug('=====> ' + myString3); //nom donnee a la kabala
		System.debug('=====> ' + myString4); //מספר קבלה
		System.debug('=====> ' + myString5);
		System.debug('=====> ' + myString6);// סכום הקבלה
		System.debug('=====> ' + myString7); //סכום המבוקש להחזר
		System.debug('=====> ' + pActivityReport.ReceiptDate__c);
		System.debug('=====> ' + x);

		
		if (sessionId != null && sessionId.ActivityApproval__c != null)
			pActivityReport.LastModifiedActivityReportWebsite__c = 'חונך';
		else 
			pActivityReport.LastModifiedActivityReportWebsite__c = 'משפחה';


		activityReportDb.InsertpActivityReport();
		SaveExceededs();
		if (pActivityReport.IsForFindOut__c && myActivityApproval.TutorName__c != null)
		{
			SetProblemsCookie(false);
			ActivityReportId = pActivityReport.id;
			System.debug('ActivityReportId - ' + ActivityReportId);
			System.debug('pActivityReport.id - ' + pActivityReport.id);
			Savedoc();
			return null;
		}
		else{
			ActivityReportId = pActivityReport.id;
			System.debug('ActivityReportId - ' + ActivityReportId);
			System.debug('pActivityReport - ' + pActivityReport);
			Savedoc();
			return new PageReference(CObjectNames.WebSitePageReportDisplay + '&AR=' + pActivityReport.id);
		}
	}
	
	public void SetProblemsCookie(boolean isError)
	{
		string findoutReason = pActivityReport.ClarificationReason__c == null ? '' : pActivityReport.ClarificationReason__c;
		for (CDayInActivityReportToSite day : pDayInActivityReportToSite)
		{
			if (day.pDayInActivityReport.Date__c != null && day.pDayInActivityReport.IsWarning__c && day.pDayInActivityReport.WarningReport__c != null && day.pDayInActivityReport.WarningReport__c != '')
			{
				findoutReason += '\n\nבתאריך ' + day.pDayInActivityReport.Date__c.day() + '/' + day.pDayInActivityReport.Date__c.month() + '/' + day.pDayInActivityReport.Date__c.year() + ' - ';
				findoutReason += '\n' + day.pDayInActivityReport.WarningReport__c;
			}
		}
		findoutReason = findoutReason.trim().replace('\n', '\\n');
		String myVar = 'IsError='+isError+';FindoutReason='+findoutReason+';';
		setmyProblems(myVar);
	}
	
	public PageReference CompleteSave()
	{
		return new PageReference(CObjectNames.WebSitePageReportDisplay + '&AR=' + pActivityReport.id);
	}
	
	private boolean ValidateDaysInActivityReport()
	{
		CActivityReportValidator activityReportValidator = new CActivityReportValidator();
		if (!test.isRunningTest()){
			reportWarning = activityReportValidator.ValidateDaysInActivityReportInWebSite(pDayInActivityReportToSite, ApexPages.currentPage().getParameters().get('month'));
		}
		ValidateExceededs();
		return reportWarning != null;
	}
	
	private void ValidateExceededs()
	{
		list<integer> toRemove = new list<integer>();
		for (integer i = 0; i < pExpenditureExceededs.size(); i++)
		{
			if (pExpenditureExceededs[i].Amount__c != null && pExpenditureExceededs[i].Amount__c > 0)
			{
				if (pExpenditureExceededs[i].name == null)
					reportWarning = 'כדי להמשיך בשליחה עליך להזין כותרת בכל הוצאה חריגה שיש בה סכום';
			}
			else
				toRemove.add(i);
		}
		for (integer i = toRemove.size() - 1; i > -1; i--)
		{
			pExpenditureExceededs.remove(toRemove[i]);
		}
	}
	
	private void SaveExceededs()
	{
		for (ExpenditureExceeded__c exceeded : pExpenditureExceededs)
		{
			exceeded.ActivityReportName__c = pActivityReport.id;
		}
		if (pExpenditureExceededs.size() > 0)
			insert pExpenditureExceededs;
	}
	
	private boolean ValidateChild()
	{
		if (ApexPages.currentPage().getParameters().get('childId') != null)
			return true;
		else
			return false;
	}
	
	private boolean ValidateProfession()
	{
		if (ApexPages.currentPage().getParameters().get('professionId') != null)
			return true;
		else
			return false;
	}
	
	public void OnAddDayInActivityReport()
	{
		CWebSiteReportFacad.setisWithDays(true);
		activityReportDb.OnAddDayInActivityReportWebSite();
	}
	
	public void OnAddExpenditureExceeded()
	{
		pExpenditureExceededs.add(new ExpenditureExceeded__c());
	}
	
	public ActivityReport__c pActivityReport
	{
		get
		{
			return activityReportDb.GetpActivityReport();
		}
	}
	
	public List<CDayInActivityReportToSite> pDayInActivityReportToSite
	{
		get 
		{
			return activityReportDb.GetDaysInActivityReportWebSite();
		}
	}
	
	public List<DayInActivityReport__c> pDayInActivityReport
	{
		get 
		{
			return new List<DayInActivityReport__c> {new DayInActivityReport__c()};
		}
	}
	
	public List<selectOption> allMinutes
	{
		get
		{
			return CObjectNames.GetMinutesOptions();
		}
	}
	
	public List<selectOption> allHours
	{
		get
		{
			return CObjectNames.GetHoursOptions();
		}
	}
	
	public string reportWarning { get; set; }
	public string reportNote { get; set; }
	public string childProgress { get; set; }

	public static String namePersonUploadReceipt { get; set; }
	public static String receiptNumber { get; set; }
	public static Date ReceiptDate { get; set; }
	public static Decimal ReceiptSum { get; set; }
	public static Decimal AmountRequestedRemboursment { get; set; }
}