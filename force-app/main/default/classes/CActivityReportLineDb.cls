public without sharing class CActivityReportLineDb 
{
	private ActivityReport__c pActivityReport { Get; set; }
	private List<CDayInActivityReportInList> pDaysInActivityReport { Get; set; }
	public ActivityApproval__c pReportApproval { get; set; }
	
	public CActivityReportLineDb(ActivityReport__c extActivityReport, ActivityApproval__c approval)
	{
		pReportApproval = approval;
		pActivityReport = extActivityReport;
		if (pActivityReport.id == null)
		{
			FillpActivityReportLines();
		}
		else
		{
			SetpDaysInActivityReportBypActivityReportId(pActivityReport.id);
		}
	}
	
	private void FillpActivityReportLines()
	{
		pDaysInActivityReport = new List<CDayInActivityReportInList>();
		pDaysInActivityReport.add(new CDayInActivityReportInList(new DayInActivityReport__c(), pReportApproval));
	}
	
	private void SetpDaysInActivityReportBypActivityReportId(string id)
	{
		list<DayInActivityReport__c> days = [select name, id, ActivityReportName__c, StartTime__c, EndTime__c, Date__c, TotalHours__c, Note__c, 
											ChildName__c, TotalPayment__c, StartTimeToCalculate__c, EndTimeToCalculate__c, WarningReport__c, 
											TotalHoursToCalculate__c, IsWarning__c, CreatedDate, TutorName__c, ActivityReportName__r.name, 
											ActivityPlace__c, TotalHoursToCalculateToPayment__c, TotalHoursToPayment__c, PriceOnTravel__c 
											from DayInActivityReport__c where ActivityReportName__c = :id order by Date__c];
		pDaysInActivityReport = new List<CDayInActivityReportInList>();
		for (DayInActivityReport__c day : days)
		{
			pDaysInActivityReport.add(new CDayInActivityReportInList(day, pReportApproval));
		}
	}
	
	public static list<DayInActivityReport__c> GetDaysInActivityReportByActivityReportId(string id)
	{
		return [select name, id, ActivityReportName__c, StartTime__c, EndTime__c, Date__c, TotalHours__c, Note__c, ChildName__c, PriceOnTravel__c,
								TotalPayment__c, StartTimeToCalculate__c, EndTimeToCalculate__c, WarningReport__c, TotalHoursToCalculate__c, IsWarning__c,
								CreatedDate, TutorName__c, ActivityReportName__r.name, ActivityPlace__c, TotalHoursToCalculateToPayment__c, TotalHoursToPayment__c
								from DayInActivityReport__c where ActivityReportName__c = :id order by Date__c];
	}
	
	public void InsertpActivityReportLines()
	{
		list<DayInActivityReport__c> daysInActivityReportToInsert = new list<DayInActivityReport__c>();
		list<DayInActivityReport__c> daysInActivityReportToUpdate = new list<DayInActivityReport__c>();
		for (CDayInActivityReportInList day : pDaysInActivityReport)
		{
			if (day.pDay.id == null)
			{
				day.pDay.ActivityReportName__c = pActivityReport.id;
				daysInActivityReportToInsert.add(day.pDay);
			}
			else
				daysInActivityReportToUpdate.add(day.pDay);
		}
		if (daysInActivityReportToInsert.size() > 0)
		{
			insert daysInActivityReportToInsert;
			sendEmailToTutor(daysInActivityReportToInsert);
		}
		else if (daysInActivityReportToUpdate.size() > 0)
			update daysInActivityReportToUpdate;
	}
		
	public void SetpActivityReport(ActivityReport__c activityReport)
	{
		pActivityReport = activityReport;
	}
	
	private void SetpActivityReport(string id)
	{
		pActivityReport = [select ActivityReportStatus__c, ActivityApprovalName__c, TutorName__c, ToDateClass__c, id, ChildName__c, 
						  ChildName__r.name, ProfessionName__r.name, ProfessionName__c, ReasonForDecline__c, CreatedDate, ReportMonth__c, 
						  ReceivingImageClass__c, ChildLastName__c, ChildFirstName__c, TalkWithDirectorOrTeacherOrParent__c, name,  
						  PaymentAmountClass__c, IsForFindOut__c, CoordinatorUserName__c, FromDateClass__c from ActivityReport__c where id = :id];
	}
	
	private List<CDayInActivityReportInList> SendToDeleteEmptys(List<CDayInActivityReportInList> daysInActivityReport)
	{
		List<CDayInActivityReportInList> toReturn = new List<CDayInActivityReportInList>();
		List<DayInActivityReport__c> toDelete = new List<DayInActivityReport__c>();
		if (isClassProfession())
			return toReturn;
		for (CDayInActivityReportInList day : daysInActivityReport)
		{
			integer res = RemoveOrDeleteFromList(day.pDay);
			if (res == 1)
				toReturn.add(day);
			else if (res == -1)
				toDelete.add(day.pDay);
		}
		if (toDelete.size() > 0)
			delete toDelete;
		
		return toReturn;
	}
	
	private boolean isClassProfession()
	{
		Profession__c[] profession  = CProfessionDb.GetProfessionsById(pActivityReport.ProfessionName__c);
		if (profession.size() > 0 && profession[0].ProfessionTypeName__r.PaymentType__c == CObjectNames.AmountForApproval)
			return true;
		return false;
	}
	
	private integer RemoveOrDeleteFromList(DayInActivityReport__c day)
	{
		if (day.Date__c != null)
			return 1;
		if (day.id == null)
			return 0;
		else 
			return -1;
	}
	
	private void SetToFillFields(integer i)
	{
		pDaysInActivityReport[i].pDay.StartTime__c = CValidateTimesAndConverterTimes.FillFields(pDaysInActivityReport[i].pDay.StartTime__c);
		pDaysInActivityReport[i].pDay.EndTime__c = CValidateTimesAndConverterTimes.FillFields(pDaysInActivityReport[i].pDay.EndTime__c);
		if (pDaysInActivityReport[i].pDay.TotalHoursToPayment__c != null)
			pDaysInActivityReport[i].pDay.TotalHoursToPayment__c = CValidateTimesAndConverterTimes.FillFields(pDaysInActivityReport[i].pDay.TotalHoursToPayment__c);
	}
	
	public List<CDayInActivityReportInList> GetpDaysInActivityReport()
	{
		return pDaysInActivityReport;
	}
	
	public void OnAddDayToList()
	{
		pDaysInActivityReport.add(new CDayInActivityReportInList(new DayInActivityReport__c(), pReportApproval));
	}
	
	//====================================================================================
	
	
	private integer MinutesStart;
	private integer HoursStart;
	private integer MinutesEnd;
	private integer HoursEnd;
	
	private integer TotalMinutes;
	private integer TotalHours;
	
	
	public void SetListTimes()
	{
		map<string, string> monthesmap = new map<string, string>();
	
		CActivityReportValidator activityReportValidator = new CActivityReportValidator();
	
		map<Date, Holiday> holidaysmap = GetHolidaysmap();
	
		map<Date, DayInActivityReport__c> daysInActivityReportByChild = GetDaysInActivityReportmapByChild();
	
		map<Date, DayInActivityReport__c> daysInActivityReportByTutor = GetDaysInActivityReportmapByTutor();
	
		map<date, list<DayInActivityReport__c>> daysByDate = GetDaysMapByDateFromList();
	
		//map<string, Children__c> childrenMap = CChildDb.GetChildrenMapWithOutPhoto();
		//Children__c child = CChildDb.GetChildById(pActivityReport.ChildName__c);
		//map<string, Tutor__c> tutorsMap = CTutorDb.GetTutorsMap();
		TutorLine__c myTutorLine = GetmyTutorLine();
	
		pDaysInActivityReport = SendToDeleteEmptys(pDaysInActivityReport);
		
		for (integer i = 0; i < pDaysInActivityReport.size(); i++)
		{
			SetToFillFields(i);
			
			SetToFillMinutesAndHours(i);
			
			SetTotalHours(i);
			
			SetTotalHoursToCalculate(i);
			
			SetStartTimeToCalculate(i);
			
			SetEndTimeToCalculate(i);
			
			SetTotalPayment(i, myTutorLine, pReportApproval);
			
			SetTutorAndChildName(i);
			
			string holidayErorr = activityReportValidator.ValidateIsNotHoliday(i, holidaysmap, pDaysInActivityReport[i].pDay.Date__c);
			
			string childLearnAtTheSameTimeErorr = activityReportValidator.ValidateChildDidNotLearnAtTheSameTime(pDaysInActivityReport[i].pDay, daysInActivityReportByChild);
			string tutorLearnAtTheSameTimeErorr = '';//activityReportValidator.ValidateTutorDidNotLearnAtTheSameTime(pDaysInActivityReport[i].pDay, daysInActivityReportByTutor);
			string passedMinutesInSequenceErorr = '';//activityReportValidator.ValidateIsPassedMinutesInSequence(pDaysInActivityReport[i].pDay.TotalHoursToCalculate__c, pReportApproval.HoursInSequence__c);
			string Passed60DaysFromProductionDay = activityReportValidator.ValidateIsPassed60DaysFromProductionDay(pDaysInActivityReport[i].pDay.Date__c, ActivityReportCreatedDate);			
			string duplicateTimes = activityReportValidator.ValidateDuplicateTimes(daysByDate, pDaysInActivityReport[i].pDay);

			// 
			// pReportApproval is not initialized 		
			// 

			// System.debug('pReportApproval.MinutesInSequence__c : ' + pReportApproval.MinutesInSequence__c);
			string OneHourPerDay = activityReportValidator.ValidateOneHourPerDay(daysByDate, pDaysInActivityReport[i].pDay, pReportApproval.MinutesInSequence__c);
			// string OneHourPerDay = activityReportValidator.ValidateOneHourPerDay(daysByDate, pDaysInActivityReport[i].pDay, 2);
			SetWarningReport(i, (holidayErorr + childLearnAtTheSameTimeErorr + tutorLearnAtTheSameTimeErorr + passedMinutesInSequenceErorr + Passed60DaysFromProductionDay + duplicateTimes + OneHourPerDay));
			
			SetIsWarning(i);
			
			monthesmap.put(pDaysInActivityReport[i].pDay.Date__c.month() + '-' + pDaysInActivityReport[i].pDay.Date__c.year(), pDaysInActivityReport[i].pDay.Date__c.month() + '-' + pDaysInActivityReport[i].pDay.Date__c.year());
			
		}
		//SetToCloseTasks(monthesmap, activityApproval);
	}
	
	private map<date, list<DayInActivityReport__c>> GetDaysMapByDateFromList()
	{
		map<date, list<DayInActivityReport__c>> ret = new map<date, list<DayInActivityReport__c>>();
		for (CDayInActivityReportInList day : pDaysInActivityReport)
		{
			if (ret.containsKey(day.pDay.Date__c))
				ret.get(day.pDay.Date__c).add(day.pDay);
			else
				ret.put(day.pDay.Date__c, new list<DayInActivityReport__c> { day.pDay });
		}
		DayInActivityReport__c[] days = [select name, id, Date__c, StartTimeToCalculate__c, TotalHoursToCalculate__c, EndTimeToCalculate__c from DayInActivityReport__c where 
										 ActivityReportName__r.ReportMonth__c = :pActivityReport.ReportMonth__c
										 and ActivityReportName__r.ActivityApprovalName__c = :pActivityReport.ActivityApprovalName__c 
										 and ActivityReportName__c != :pActivityReport.id];
		for (DayInActivityReport__c day : days)
		{
			if (ret.containsKey(day.Date__c))
				ret.get(day.Date__c).add(day);
			else
				ret.put(day.Date__c, new list<DayInActivityReport__c> { day });
		}
		return ret;
	}
	
	private datetime ActivityReportCreatedDate
	{
		get
		{
			if (pActivityReport.id != null)
				return pActivityReport.CreatedDate;
			return datetime.now();
		}
	}
	
	private void setIsWarning(integer i)
	{
		if (pDaysInActivityReport[i].pDay.WarningReport__c != null && pDaysInActivityReport[i].pDay.WarningReport__c != '')
			pDaysInActivityReport[i].pDay.IsWarning__c = true;
	}
	
	private map<Date, DayInActivityReport__c> GetDaysInActivityReportmapByTutor()
	{
		DayInActivityReport__c[] tutorDaysInActivityReport = [select ChildName__r.name, StartTimeToCalculate__c, 
															EndTimeToCalculate__c, TutorName__r.name, IsWarning__c,
															ActivityReportName__r.name, name, id, Date__c, ActivityPlace__c,
															TotalHoursToCalculateToPayment__c, TotalHoursToPayment__c, PriceOnTravel__c
															from DayInActivityReport__c
															where TutorName__c = :pActivityReport.TutorName__c
															and ActivityReportName__c != :pActivityReport.id];
		map<Date, DayInActivityReport__c> ret = new map<Date, DayInActivityReport__c>();
		for (integer i = 0; i < tutorDaysInActivityReport.size(); i++)
		{
			ret.put(tutorDaysInActivityReport[i].Date__c, tutorDaysInActivityReport[i]);
		}
		system.debug('\n\n\n\n\n  ret  ' + ret);
		return ret;
	}
	
	private map<Date, DayInActivityReport__c> GetDaysInActivityReportmapByChild()
	{
		DayInActivityReport__c[] childDaysInActivityReport = [select ChildName__r.name, StartTimeToCalculate__c, PriceOnTravel__c,
															EndTimeToCalculate__c, TutorName__r.name, IsWarning__c,
															ActivityReportName__r.name, name, id, Date__c, ActivityPlace__c, 
															TotalHoursToCalculateToPayment__c, TotalHoursToPayment__c
															from DayInActivityReport__c where 
															ChildName__c = :pActivityReport.ChildName__c
															and ActivityReportName__c != :pActivityReport.id];
		map<Date, DayInActivityReport__c> ret = new map<Date, DayInActivityReport__c>();
		for (integer i = 0; i < childDaysInActivityReport.size(); i++)
		{
			ret.put(childDaysInActivityReport[i].Date__c, childDaysInActivityReport[i]);
		}
		system.debug('\n\n\n\n\n  ret  ' + ret);
		return ret;
	}
	
	private map<Date, Holiday> GetHolidaysmap()
	{
		Holiday[] allHolidays = [select name, id, ActivityDate from Holiday];
		map<Date, Holiday> ret = new map<Date, Holiday>();
		for (integer i = 0; i < allHolidays.size(); i++)
		{
			ret.put(allHolidays[i].ActivityDate, allHolidays[i]);
		}
		return ret;
	}
	
	private void SetTotalPayment(integer i, TutorLine__c myTutorLine, ActivityApproval__c activityApproval)
	{
		if (activityApproval != null && activityApproval.Rate__c != null)
			pDaysInActivityReport[i].pDay.TotalPayment__c = pDaysInActivityReport[i].pDay.TotalHoursToCalculateToPayment__c * activityApproval.Rate__c;
		else if (myTutorLine != null && myTutorLine.Rate__c != null)
			pDaysInActivityReport[i].pDay.TotalPayment__c = pDaysInActivityReport[i].pDay.TotalHoursToCalculateToPayment__c * myTutorLine.Rate__c;
	} 
	
	private TutorLine__c GetmyTutorLine()
	{
		TutorLine__c[] myTutorLine = [select name, id, Rate__c, TutorName__c, ProfessionName__c from TutorLine__c
									where TutorName__c = :pActivityReport.TutorName__c and ProfessionName__c = :pActivityReport.ProfessionName__c];
		if (myTutorLine.size() > 0)
			return myTutorLine[0];
		return null;
	}
	
	private void setTotalHoursToCalculate(integer i)
	{
		pDaysInActivityReport[i].pDay.TotalHoursToCalculate__c = CValidateTimesAndConverterTimes.GetTimeToCalculate(pDaysInActivityReport[i].pDay.TotalHours__c);
		pDaysInActivityReport[i].pDay.TotalHoursToCalculateToPayment__c = CValidateTimesAndConverterTimes.GetTimeToCalculate(pDaysInActivityReport[i].pDay.TotalHoursToPayment__c);
	}
	
	private void setTutorAndChildName(integer i)
	{
		pDaysInActivityReport[i].pDay.TutorName__c = pActivityReport.TutorName__c;
		pDaysInActivityReport[i].pDay.ChildName__c = pActivityReport.ChildName__c;
	}
	
	private void setStartTimeToCalculate(integer i)
	{
		pDaysInActivityReport[i].pDay.StartTimeToCalculate__c = CValidateTimesAndConverterTimes.GetTimeToCalculate(pDaysInActivityReport[i].pDay.StartTime__c);
	}
	
	private void setEndTimeToCalculate(integer i)
	{
		pDaysInActivityReport[i].pDay.EndTimeToCalculate__c = CValidateTimesAndConverterTimes.GetTimeToCalculate(pDaysInActivityReport[i].pDay.EndTime__c);
	}
	
	private void setTotalHours(integer i)
	{
		if (string.valueOf(TotalHours).Length() == 1)
  			pDaysInActivityReport[i].pDay.TotalHours__c = string.valueOf(TotalHours) + string.valueOf(TotalMinutes);
  		else
  		{
  			if (string.valueOf(TotalMinutes).Length() == 1)
  				pDaysInActivityReport[i].pDay.TotalHours__c = string.valueOf(TotalHours) + '0' + string.valueOf(TotalMinutes);
  			else
  				pDaysInActivityReport[i].pDay.TotalHours__c = string.valueOf(TotalHours) + string.valueOf(TotalMinutes);
  		}
  		pDaysInActivityReport[i].pDay.TotalHours__c = CValidateTimesAndConverterTimes.FillFields(pDaysInActivityReport[i].pDay.TotalHours__c);
  		if (pDaysInActivityReport[i].pDay.TotalHoursToPayment__c == null)
  			pDaysInActivityReport[i].pDay.TotalHoursToPayment__c = CValidateTimesAndConverterTimes.FillFields(pDaysInActivityReport[i].pDay.TotalHours__c);
	}
	
	private void setToFillMinutesAndHours(integer i)
	{
		MinutesStart = CValidateTimesAndConverterTimes.GetMinuteFromTime(pDaysInActivityReport[i].pDay.StartTime__c);
		HoursStart = CValidateTimesAndConverterTimes.GetHoursFromTime(pDaysInActivityReport[i].pDay.StartTime__c);
		MinutesEnd = CValidateTimesAndConverterTimes.GetMinuteFromTime(pDaysInActivityReport[i].pDay.EndTime__c);
		HoursEnd = CValidateTimesAndConverterTimes.GetHoursFromTime(pDaysInActivityReport[i].pDay.EndTime__c);
		TotalMinutes = CValidateTimesAndConverterTimes.ComputeTotalMinutes(MinutesStart, MinutesEnd);
		TotalHours = CValidateTimesAndConverterTimes.ComputeTotalHours(MinutesStart, MinutesEnd, HoursStart, HoursEnd);
	}
	
	private void setWarningReport(integer i, string newWarningReport)
	{
		pDaysInActivityReport[i].pDay.WarningReport__c = newWarningReport;
	}
	
	public boolean isExistWarningReport()
	{
		for (integer i = 0; i < pDaysInActivityReport.size(); i++)
		{
			if (pDaysInActivityReport[i].pDay.WarningReport__c != null && pDaysInActivityReport[i].pDay.WarningReport__c != '' && pDaysInActivityReport[i].pDay.WarningReport__c != '')
				return true;
		}
		return false;
	}
	
	public void SetDaysInActivityReportWebSite(List<CDayInActivityReportInList> newList)
	{
		pDaysInActivityReport = newList;
	}
	
	private void sendEmail(List<string> EmailsToSend, string VersionEmail)
	{
		if (EmailsToSend.size() > 0)
		{
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setToAddresses(EmailsToSend);
	        mail.setHtmlBody(VersionEmail);
	        mail.Subject = 'קבלת דיווח פעילות';
	        if(!Test.isRunningTest())
	        	Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
		}
	}
	
	private void sendEmailToTutor(list<DayInActivityReport__c> daysInActivityReport)
	{
		if (pActivityReport.TutorName__c != null)
 		{
 			Tutor__c[] tutor = [select name, id, Email__c from Tutor__c where id = :pActivityReport.TutorName__c];
 			if (tutor.size() > 0 && tutor[0].Email__c != null)
 			{
 				string VersionEmail = VersionEmail(tutor[0].name, daysInActivityReport);
			 	sendEmail(new list<string> { tutor[0].Email__c }, VersionEmail);
 			}
 		}
	}
	
	private string VersionEmail(string tutorName, list<DayInActivityReport__c> daysInActivityReport)
	{
		string htmlBody = '<div ' + GetDataStyle() + ' dir="rtl"><table style="width: 100%; text-align: center"><tr><td><b>לכבוד החונכ/ת: ' + tutorName + '.</b></td></tr><tr><td></td></tr><tr><td>אנו שמחים לבשר לכם על קבלת דיווח פעילות עבור: <b>' + 
						  pActivityReport.ChildName__r.name + '</b> מקצוע: <b>' + pActivityReport.ProfessionName__r.name + '</b></p></td></tr><tr><td></td></tr><tr><td><b>פירוט ימי הדיווח:</b></td></tr>'
						  + '<tr><td><table width="100%"><tr><td ' + GetHadStyle() + '>תאריך: </td><td ' + GetHadStyle() + '>שעת התחלה: </td><td ' + GetHadStyle() + '>שעת סיום:</td>' + 
						  '<td ' + GetHadStyle() + '>תכני פעילות:</td></tr>' + GetFullHtmlForTable(daysInActivityReport) + '</table></td></tr></table>';
	    return htmlBody;
	}
	
	private string GetHadStyle()
	{
		return 'style="background-color:#D8D8D8; text-align: Center;font-size: 22px; font-family: Arial Unicode MS;"';
	}
	
	private string GetDataStyle()
	{
		return 'style="text-align: Center; font-size: 18px; font-family: Arial Unicode MS;"';
	}
	
	private string GetFullHtmlForTable(list<DayInActivityReport__c> daysInActivityReport)
	{
		string ret = '';
		for (integer i = 0; i < daysInActivityReport.size(); i++)
		{
			ret += '<tr><td '+ GetDataStyle() + '>' + daysInActivityReport[i].Date__c.day() + '/' + daysInActivityReport[i].Date__c.month() + '/' + daysInActivityReport[i].Date__c.year()
				   + '</td><td '+ GetDataStyle() + '>' + daysInActivityReport[i].StartTime__c + '</td><td '+ GetDataStyle() + '>' + daysInActivityReport[i].EndTime__c + '</td>' + 
				   '<td '+ GetDataStyle() + '>' + daysInActivityReport[i].Note__c + '</td></tr>';
		}
		return ret;
	}
	
	public map<date, decimal> GetHoursByWeek()
	{
		map<date, decimal> ret = new map<date, decimal>();
		for (CDayInActivityReportInList day : pDaysInActivityReport)
		{
			date firstinweek = day.pDay.Date__c;
			integer weekDay = CObjectNames.GetWeekDayNumberFromDate(firstinweek);
			while(weekDay != 1)
			{
				firstinweek = firstinweek.addDays(-1);
				weekDay--;
			}
			decimal hours = day.pDay.TotalHoursToCalculate__c;
			if (ret.containsKey(firstinweek))
				hours += ret.get(firstinweek);
			ret.put(firstinweek, hours);
		}
		return ret;
	}
}