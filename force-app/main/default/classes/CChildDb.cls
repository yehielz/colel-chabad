public with sharing class CChildDb
{
    public static boolean IsAfterUpdateTutorsOrActive { get; set; }
    
    private Children__c pChild { get; set; }
    private Map<string, List<Children__c>> childrenByFamiliesMap;
    public Map<string, Children__c> childrenMap;
    private map<string, list<string>> tutorsByChildrenMap;
    private map<string, string> schoolYeasByChildrenMap;
    
    //public string pSchoolYearId { get{return schoolYear.Id; } set; }
    public SchoolYear__c schoolYear {get;set;}
    public List<SelectOption> pYearsOptions { get; set; }
    
    public CChildDb()
    {
        pChild = new Children__c();
        SetChildrenByFamiliesMap();
    }
    
    public CChildDb(boolean b)
    {
    }
    
    public CChildDb(integer wat)
    {
        if (wat == 0){}
        else if (wat == 1)
        {
            SetChildrenMap();
            SetSchoolYeasByChildrenMap();
        }
    }
    
    public CChildDb(ApexPages.StandardController controller)
    {
        pChild = (Children__c)controller.getRecord();
        if (pChild.id != null)
            SetpChildBypChildId(pChild.id);
        else 
            setpChildEnglishFamilyDetails();
        pYearsOptions = CSchoolYearDb.GetYearsOptions();
        schoolYear =  CSchoolYearDb.GetActiveYear();
    }
    
    private void setpChildEnglishFamilyDetails()
    {
        CFamilyDb familyDb = new CFamilyDb();
        if (pChild.FamilyName__c != null)
        {
            Family__c family = familyDb.GetFamilyByFamilyId(pChild.FamilyName__c);
            pChild.EnglishFamilyName__c = family.EnglishFamilyName__c;
            pChild.MobilePhone__c = family.MobilePhone__c;
        }
    }
    
    private void SetpChildBypChildId(string id)
    {
        pChild = [select Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, CoordinatorUserName__c, FamilyManType__c, Age__c,
                  Gender__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, TutorsList__c, NoteToPrintPage__c, School__c, ZipCode__c,
                  SchoolName__c, Grade__c, City__c, IsActive__c, IsAfterSunset__c, HebrewBirthYear__c, Birthdate__c, Picture__c, name, id, IsThereAnAnnualRegistration__c,
                  ReasonAnnualRegistration__c  ,OwnerId, RegistrationAllowed__c from Children__c  where id = :id];
    }
    
    private void SetChildrenByFamiliesMap()
    {
        Children__c[] allChildren = [select Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c,
                                    CoordinatorUserName__c,OwnerId, IsAfterSunset__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, ZipCode__c,
                                    NoteToPrintPage__c, IsActive__c, TutorsList__c, EnglishFirstName__c, HebrewBirthYear__c, Birthdate__c, Age__c,
                                    SchoolName__c, Grade__c, School__c, City__c, FamilyManType__c, Picture__c, name, id, IsThereAnAnnualRegistration__c,
                                    ReasonAnnualRegistration__c from Children__c];
        childrenByFamiliesMap = new Map<string, List<Children__c>>();
        for (integer i = 0; i < allChildren.size(); i++)
        {
            if (childrenByFamiliesMap.containsKey(allChildren[i].FamilyName__c))
                childrenByFamiliesMap.get(allChildren[i].FamilyName__c).add(allChildren[i]);
            else
                childrenByFamiliesMap.put(allChildren[i].FamilyName__c, new List<Children__c> { allChildren[i] });
        }
    }
    
    public void SetTutorsListsByChild(list<string> childrenIds)
    {
        map<string, Tutor__c> tutorsMap = GetTutorsMap();
        AggregateResult[] childrenAndTutorsList = [select ChildName__c, TutorName__c from ActivityApproval__c where ChildName__c in :childrenIds
                                                   and TutorName__c != null group by ChildName__c, TutorName__c];
        tutorsByChildrenMap = new map<string, list<string>>();
        for (integer i = 0; i < childrenAndTutorsList.size(); i++)
        {
            string childName = string.valueOf(childrenAndTutorsList[i].get('ChildName__c'));
            string tutorName = string.valueOf(childrenAndTutorsList[i].get('TutorName__c'));
            if (tutorsByChildrenMap.containsKey(childName))
                tutorsByChildrenMap.get(childName).add(tutorsMap.get(tutorName).name);
            else
                tutorsByChildrenMap.put(childName, new list<string> { tutorsMap.get(tutorName).name } );
        }
    }
    
    private map<string, Tutor__c> GetTutorsMap()
    {
        Tutor__c[] tutors = [select name, id, Education__c from Tutor__c];
        map<string, Tutor__c> ret = new map<string, Tutor__c>();
        for (integer i = 0; i < tutors.size(); i++)
        {
            ret.put(tutors[i].id, tutors[i]);
        }
        return ret;
    }
    
    public void SetChildrenMap()
    {
        Children__c[] allChildren = [select Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c,
                                    CoordinatorUserName__c, IsAfterSunset__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, ZipCode__c, 
                                    NoteToPrintPage__c, IsActive__c, TutorsList__c, EnglishFirstName__c, HebrewBirthYear__c, Birthdate__c, Age__c,
                                    SchoolName__c, Grade__c, School__c, City__c, Picture__c, FamilyManType__c, ReasonAnnualRegistration__c,
                                    IsThereAnAnnualRegistration__c, id, name from Children__c];
        childrenMap = new map<string, Children__c>();
        for (integer i = 0; i < allChildren.size(); i++)
        {
            childrenMap.put(allChildren[i].id, allChildren[i]);
        }
    }
    
    public void SetChildrenMap(list<Children__c> allChildren)
    {
        childrenMap = new map<string, Children__c>();
        for (integer i = 0; i < allChildren.size(); i++)
        {
            childrenMap.put(allChildren[i].id, allChildren[i]);
        }
    }
    
    private void SetSchoolYeasByChildrenMap()
    {
        ChildRegistration__c[] childRegistrations = [select name, id, SchoolYearName__c, ChildName__c from ChildRegistration__c];
        schoolYeasByChildrenMap = new map<string, string>();
        for (integer i = 0; i < childRegistrations.size(); i++)
        {
            if (schoolYeasByChildrenMap.containsKey(childRegistrations[i].ChildName__c))
                schoolYeasByChildrenMap.put(childRegistrations[i].ChildName__c, schoolYeasByChildrenMap.get(childRegistrations[i].ChildName__c) + '; ' + childRegistrations[i].SchoolYearName__c);
            else
                schoolYeasByChildrenMap.put(childRegistrations[i].ChildName__c, childRegistrations[i].SchoolYearName__c);
        }
    }
    
    public void save()
    {
        SetCoordinatorUserNameAndCity();
        SetFamilyManType();
        if (pChild.id == null)
            insert pChild;
        else
            update pChild;
    }
    
    private void SetFamilyManType()
    {
        pChild.FamilyManType__c = CObjectNames.Child;
    }
    
    private void SetCoordinatorUserNameAndCity()
    {
        if (pChild.FamilyName__c != null)
        {
            Family__c[] family = [select name, id, CoordinatorUserName__c, City__c,OwnerId from Family__c where id = :pChild.FamilyName__c];
            if (family.size() > 0)
            {
                //pChild.CoordinatorUserName__c = family[0].CoordinatorUserName__c;
                pChild.OwnerId = family[0].OwnerId;
                pChild.City__c = family[0].City__c;
            }
        }
    }
    
    public Children__c GetChild()
    {
        return pChild;
    }
    
    public void OnChangeYear()
    {
        approvalsList = null;
        requestsList = null;
        reportsList = null;
        paymentsList = null;
        notesFromStuffList = null;
        examList1 = null;
    }
    private list<CNoteAndAttachment> notesFromStuffList;
    public list<CNoteAndAttachment> pNotesFromStuffList
    {
        get
        {
            if (notesFromStuffList == null)
            {
                notesFromStuffList = new list<CNoteAndAttachment>();
                Case__c[] cases;
                ActivityReport__c[] reports;
                ActivityApproval__c[] approvals;
                if (schoolYear == null)
                {
                    cases = [select name, id from Case__c where ChildName__c = :pChild.id];
                    reports = [select name, id from ActivityReport__c where ChildName__c = :pChild.id];
                    approvals = [select name, id from ActivityApproval__c where ChildName__c = :pChild.id];
                }
                else
                {
                    cases = [select name, id from Case__c where ChildName__c = :pChild.id AND SchoolYear__c = :schoolYear.Id ];
                    reports = [select name, id from ActivityReport__c where ChildName__c = :pChild.id AND SchoolYear__c = :schoolYear.Id ];
                    approvals = [select name, id from ActivityApproval__c where ChildName__c = :pChild.id AND SchoolYear__c = :schoolYear.Id ];
                
                }
                
                
                Attachment[] attachments = [select name, id, IsPrivate, LastModifiedDate, OwnerId from Attachment where ParentId in :cases or ParentId in :reports or ParentId in :approvals order by LastModifiedDate desc];
                Note[] notes = [select id, Title, IsPrivate, LastModifiedDate, OwnerId, Body from Note where ParentId in :cases or ParentId in :reports or ParentId in :approvals order by LastModifiedDate desc];
                for (Note n : notes)
                {
                    notesFromStuffList.add(new CNoteAndAttachment(n, null, pUsersMap));
                }
                for (Attachment a : attachments)
                {
                    notesFromStuffList.add(new CNoteAndAttachment(null, a, pUsersMap));
                }
            }
            return notesFromStuffList;
        }
    }
    
    public string GetChildAgeRelativeToChoolYear(string childId, string schoolYearName)
    {
        Children__c child = [select IsActive__c, Birthdate__c from Children__c where id = :childId];
        integer age = CSchoolYearDb.GetStartSchoolYear(schoolYearName) - child.Birthdate__c.year();
        return CClassCompute.GetClassByAge(age);
    }
    
    public List<Children__c> GetChildrenByFamily(string familyId)
    {
        Children__c[] children = [select name, id from Children__c where FamilyName__c = :familyId];
        return children;
    }
        
    public string GetFamilyIdByChildName(string childName)
    {
        Children__c child = [select name, FamilyName__c from Children__c where id = :childName];
        return child.FamilyName__c;
    }
    
    public static Children__c[]  GetAllChildren()
    {
        Children__c[] allChildren = [select id, Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c, Age__c,
                                     name, TutorsList__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, City__c, ZipCode__c, 
                                     SchoolName__c, NoteToPrintPage__c, IsActive__c, CoordinatorUserName__c, HebrewBirthYear__c, Birthdate__c, Picture__c, 
                                     Grade__c, School__c, FamilyManType__c, FamilyName__r.name, FamilyName__r.ParentName__c, FamilyName__r.HomePhone__c,
                                     FamilyName__r.Address__c, FamilyName__r.DiedParent__c, FamilyName__r.DeathStory__c, FamilyName__r.DeathMotherStory__c,
                                     FamilyName__r.DiedParentName__c, FamilyName__r.DiedMotherName__c, FamilyName__r.DeathDate__c, FamilyName__r.DeathMotherDate__c,
                                     FamilyName__r.RegistrationDate__c, FamilyName__r.ChildrenNumberInFamily__c, SchoolName__r.name, IsThereAnAnnualRegistration__c,
                                     ReasonAnnualRegistration__c,Owner.Name from Children__c];
        
        return allChildren;
    }
    
    public void OnBirthDate()
    {
        if (test.isRunningTest())
            pChild.Birthdate__c = datetime.now().date();
        date Birthdate = pChild.Birthdate__c;
        if (pChild.IsAfterSunset__c)
            Birthdate = pChild.Birthdate__c.addDays(1);
        DateConverter dateConverter = new DateConverter();
        CMyDate dateH = dateConverter.civ2heb(Birthdate.day(), Birthdate.month(), Birthdate.year());
        setDateTopChildHebrewDate(dateH, dateConverter);
    }
    
    private void setDateTopChildHebrewDate(CMyDate dateH, DateConverter dateConverter)
    {
        pChild.HebrewBirthYear__c = dateConverter.GetHebrewYearInHebrew(dateH.Year);
        pChild.HebrewBirthDay__c = dateConverter.GetHebrewDayInHebrew(dateH.Day);
        pChild.HebrewBirthMonth__c = dateConverter.GetHebrewMonthInHebrew(dateH.Month);
    }
    
    public Children__c getChildByChildId(string childId)
    {
        return [select IsActive__c, name, id, FirstName__c, FamilyName__c, FamilyName__r.name from Children__c where id = :childId];
    }
    
    public static Children__c GetChildById(string childId)
    {
        Children__c[] child = [select id, Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c, ZipCode__c, 
                               name, TutorsList__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, City__c, 
                               SchoolName__c, NoteToPrintPage__c, IsActive__c, CoordinatorUserName__c, HebrewBirthYear__c, Birthdate__c, Picture__c, 
                               Grade__c, School__c, FamilyManType__c, FamilyName__r.name, FamilyName__r.ParentName__c, FamilyName__r.HomePhone__c,
                               FamilyName__r.Address__c, FamilyName__r.DiedParent__c, FamilyName__r.DeathStory__c, FamilyName__r.DeathMotherStory__c,
                               FamilyName__r.DiedParentName__c, FamilyName__r.DiedMotherName__c, FamilyName__r.DeathDate__c, FamilyName__r.DeathMotherDate__c,
                               FamilyName__r.EnglishDiedParent__c, FamilyName__r.EnglishAddress__c, FamilyName__r.EnglishDeathFatherStory__c, Age__c,
                               FamilyName__r.EnglishDiedFatherName__c, FamilyName__r.RegistrationDate__c, FamilyName__r.ChildrenNumberInFamily__c, 
                               SchoolName__r.name, FamilyName__r.EnglishDiedMotherName__c, FamilyName__r.EnglishDeathMotherStory__c, FamilyName__r.City__c,
                               FamilyName__r.EnglishCity__c, IsThereAnAnnualRegistration__c,Owner.Name,
                               ReasonAnnualRegistration__c  from Children__c where id = :childId];
                               
        if (child.size() > 0)
            return child[0];
        return null;
    }
    
    public static list<Children__c> GetChildrenList()
    {
        return [select Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c, Age__c,
                TutorsList__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, City__c, ZipCode__c, 
                SchoolName__c, NoteToPrintPage__c, IsActive__c, CoordinatorUserName__c, HebrewBirthYear__c, Birthdate__c, Picture__c, 
                Grade__c, School__c, name, FamilyManType__c, id, FamilyName__r.name, FamilyName__r.City__c, IsThereAnAnnualRegistration__c,
                ReasonAnnualRegistration__c,Owner.Name from Children__c];
    }
    
    public static list<Children__c> GetChildrenListByIds(list<string> ids)
    {
        return [select id, Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c, School__c,
                name, TutorsList__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, City__c, Age__c,
                SchoolName__c, NoteToPrintPage__c, IsActive__c, CoordinatorUserName__c, HebrewBirthYear__c, Birthdate__c, Picture__c, ZipCode__c, 
                Grade__c, LastApprovalDate__c, FamilyManType__c, FamilyName__r.name, FamilyName__r.ParentName__c, FamilyName__r.HomePhone__c,
                FamilyName__r.Address__c, FamilyName__r.DiedParent__c, FamilyName__r.DeathStory__c, FamilyName__r.DeathMotherStory__c,
                FamilyName__r.DiedParentName__c, FamilyName__r.DiedMotherName__c, FamilyName__r.DeathDate__c, FamilyName__r.DeathMotherDate__c,
                FamilyName__r.RegistrationDate__c, FamilyName__r.ChildrenNumberInFamily__c, SchoolName__r.name, IsThereAnAnnualRegistration__c,
                NumberOfRequestsAndReports__c,NumberOfRequestsAndReports2__c,ReasonAnnualRegistration__c,Owner.Name from Children__c where id in :ids
                order by NumberOfRequestsAndReports2__c desc nulls last, name];
    }
    
    public static list<Children__c> GetChildrenListWithOutPhoto()
    {
        return [select Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, Gender__c, ZipCode__c, 
                TutorsList__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, City__c, 
                SchoolName__c, NoteToPrintPage__c, IsActive__c, CoordinatorUserName__c, HebrewBirthYear__c, Birthdate__c, Age__c,
                Grade__c, School__c, name, FamilyManType__c, id, FamilyName__r.name, FamilyName__r.City__c, IsThereAnAnnualRegistration__c,
                ReasonAnnualRegistration__c,Owner.Name from Children__c];
    }
    
    public static map<string, Children__c> GetChildrenMap()
    {
        Children__c[] children = CChildDb.GetChildrenList();
        map<string, Children__c> ret = new map<string, Children__c>();
        for (integer i = 0; i < children.size(); i++)
        {
            ret.put(children[i].id, children[i]);
        }
        return ret;
    }
    
    public static map<string, Children__c> GetChildrenMapWithOutPhoto()
    {
        Children__c[] children = CChildDb.GetChildrenListWithOutPhoto();
        map<string, Children__c> ret = new map<string, Children__c>();
        for (integer i = 0; i < children.size(); i++)
        {
            ret.put(children[i].id, children[i]);
        }
        return ret;
    }
    
    public Children__c[] UpdateChildrenAfterUpdateFamily(Family__c family)
    {
        if (childrenByFamiliesMap.containsKey(family.id))
        {
            Children__c[] childrenToUpdate = childrenByFamiliesMap.get(family.id);
            for (integer i = 0; i < childrenToUpdate.size(); i++)
            {
                //childrenToUpdate[i].CoordinatorUserName__c = family.CoordinatorUserName__c;
                childrenToUpdate[i].OwnerId = family.OwnerId;
                childrenToUpdate[i].City__c = family.City__c;
            }
            if (childrenToUpdate.size() > 0)
                return childrenToUpdate;
        }
        return null;
    }
    
    public Children__c UpdateTutorsList(Children__c child)
    {
        if (tutorsByChildrenMap.containsKey(child.id))
        {
            string[] childTutors = tutorsByChildrenMap.get(child.id);
            child.TutorsList__c = '';
            for (string tutor : childTutors)
            {
                child.TutorsList__c += child.TutorsList__c == '' ? tutor : '; ' + tutor;
            }
        }
        return child;
    }
    
    public CTutorInChild[] getTutorsByChildTutorsList()
    {
        if (pChild.TutorsList__c != null)
        {
            Tutor__c[] tutors = [select name, id from Tutor__c where name in :pChild.TutorsList__c.split(';')];
            list<CTutorInChild> ret = new list<CTutorInChild>();
            for (integer i = 0; i < tutors.size(); i++)
            {
                ret.add(new CTutorInChild(tutors[i]));
            }
            return ret;
        }
        return new List<CTutorInChild>(); 
    }
    
    private string getStringToInInSelectFromList(list<string> listFrom)
    {
        string ret = '';
        for (integer i = 0; i < listFrom.size(); i++)
        {
            ret += '\'' + listFrom[i] + '\',';
        }
        if (ret.endsWith(','))
            ret = ret.substring(0, ret.length()-1);
        return '(' + ret + ')';
    }
    
    /*public void CreateTaskAfterInsertChild(Children__c newChild)
    {
        // עמרם ביקש לשנות סטטוס לנמוך או לחילופין לבטל, החלטתי לבטל
        /*list<Task> listToInsert = new list<Task>();
        list<string> ownersId = GetUsersIdToOwners();
        Children__c fullChild = getChild(newChild);
        for (integer i = 0; i < ownersId.size(); i++)
        {
            //Task newTask = new Task();
            //newTask.ChildName__c = fullChild.name;
            //newTask.FamilyName__c = fullChild.FamilyName__r.name;
            //newTask.City__c = fullChild.FamilyName__r.City__c;
            //newTask.CoordinatorUserName__c = system.Userinfo.getName();
            //newTask.Description = CObjectNames.TaskWaitForYouMale + CObjectNames.TaskSobjectNewChild;
            //newTask.Subject = CObjectNames.TaskSobjectNewChild;
            //newTask.WhatId = newChild.id;
            //newTask.OwnerId = ownersId[i];
            //newTask.ActivityDate = datetime.now().date().addDays(7);
            //listToInsert.add(newTask);
        }
        if (listToInsert.size() > 0)
            insert listToInsert;
    }*/
    
    public Children__c GetChild(Children__c child)
    {
        if (child.id != null)
        {
            Children__c[] fullChild = [select name, id, FamilyName__r.name, FamilyName__r.City__c,Owner.Name from Children__c where id = :child.id];
            if (fullChild.size() > 0)
            {
                return fullChild[0];
            }
        }
        return null;
    }
    
    /*private string[] GetUsersIdToOwners()
    {
        UserRole[] userRoles = [select name, id from UserRole where name = :CObjectNames.RoleActivityManager];
        User[] managers = [select name, id from User where UserRoleId = :userRoles[0].id];
        list<string> ret = new list<string>();
        for (integer i = 0; i < managers.size(); i++)
            ret.add(managers[i].id);
        return ret;
    }*/
    /*
    public void UpdateChildIsActive(string childId)
    {
        if (childrenMap.containsKey(childId) && schoolYeasByChildrenMap.containsKey(childId))
        {
            if (schoolYeasByChildrenMap.get(childId).contains(CSchoolYearDb.GetActiveYearId()))
                childrenMap.get(childId).IsActive__c = true;
            else
                childrenMap.get(childId).IsActive__c = false;
            CChildDb.IsAfterUpdateTutorsOrActive = true;
            update childrenMap.get(childId);
        }
    }
    */
    public list<CObjectsHistory> GetChildHistories()
    {
        Children__History[] caseHistories = [select ParentId, CreatedDate, CreatedBy.Name, CreatedById, Field, NewValue, OldValue from 
                                                Children__History where ParentId = :pChild.id order by CreatedDate desc];
        list<CObjectsHistory> ret = new list<CObjectsHistory>();
        for (integer i = 0; i < caseHistories.size(); i++)
        {
            ret.add(new CObjectsHistory(caseHistories[i].Field, caseHistories[i].CreatedBy.Name, caseHistories[i].OldValue,
                                        caseHistories[i].NewValue, caseHistories[i].CreatedDate, Schema.SObjectType.Children__c.fields.getMap()));
        }
        return ret;
    }
    
    private map<string, list<Children__c>> parentsByFamiliesMap;
    public void SetParentsByFamiliesMap()
    {
        list<Children__c> children = [select Comments__c, HebrewBirthMonth__c, MobilePhone__c, HebrewBirthDay__c, HebrewBirhMonth__c, CoordinatorUserName__c, 
                                             FamilyManType__c, Gender__c, IdNumber__c, FamilyName__c, EnglishFamilyName__c, FirstName__c, EnglishFirstName__c, 
                                             TutorsList__c, NoteToPrintPage__c, IsActive__c, IsAfterSunset__c, HebrewBirthYear__c, Birthdate__c, Picture__c, name, 
                                             SchoolName__c, Grade__c, School__c, City__c, id, IsThereAnAnnualRegistration__c, Age__c, ZipCode__c,
                                             ReasonAnnualRegistration__c,Owner.Name from Children__c where FamilyManType__c = :CObjectNames.Parent];
        parentsByFamiliesMap = new map<string, list<Children__c>>();
        for (Children__c child : children)
        {
            if (parentsByFamiliesMap.containsKey(child.FamilyName__c))
                parentsByFamiliesMap.get(child.FamilyName__c).add(child);
            else
                parentsByFamiliesMap.put(child.FamilyName__c, new list<Children__c> { child });
        }
    }
    
    /*public void UpdateOrInsertParents(Family__c family)
    {
        Children__c child = new Children__c();
        if (parentsByFamiliesMap.containsKey(family.id) && parentsByFamiliesMap.get(family.id).size() > 0)
            child = parentsByFamiliesMap.get(family.id)[0];
        child.FamilyName__c = family.id;
        child.FirstName__c = family.ParentName__c;
        child.Birthdate__c = family.ParentBirthdate__c;
        child.MaritalStatus__c = family.MaritalStatus__c;
        child.EnglishFirstName__c = family.EnglishParentName__c;
        child.IdNumber__c = family.ParentId__c;
        child.FamilyManType__c = CObjectNames.Parent;
        child.Picture__c = family.ParentPicture__c;
        upsert child;
    }*/
    //v2 
    
   
    public Children__c UpdateOrInsertParents2(Family__c family)
    {
        
        
        Children__c child = new Children__c();
        if (parentsByFamiliesMap.containsKey(family.id) && parentsByFamiliesMap.get(family.id).size() > 0)
            child = parentsByFamiliesMap.get(family.id)[0];
        child.FamilyName__c = family.id;
        child.FirstName__c = family.ParentName__c;
        child.Birthdate__c = family.ParentBirthdate__c;
        child.MaritalStatus__c = family.MaritalStatus__c;
        child.EnglishFirstName__c = family.EnglishParentName__c;
        child.IdNumber__c = family.ParentId__c;
        child.FamilyManType__c = CObjectNames.Parent;
        child.Picture__c = family.ParentPicture__c;
        
        //convertToHebrewDate
        if (test.isRunningTest())
            child.Birthdate__c = datetime.now().date();
        if(child.Birthdate__c  != null){
            DateConverter dateConverter = new DateConverter();
            CMyDate dateH = dateConverter.civ2heb(child.Birthdate__c.day(), child.Birthdate__c.month(), child.Birthdate__c.year());
            child.HebrewBirthYear__c = dateConverter.GetHebrewYearInHebrew(dateH.Year);
            child.HebrewBirthDay__c = dateConverter.GetHebrewDayInHebrew(dateH.Day);
            child.HebrewBirthMonth__c = dateConverter.GetHebrewMonthInHebrew(dateH.Month);
        }
        return child;
    }
    
    private map<string, ChildRegistration__c> activeChildRegistrationByChildren;
    public void SetActiveChildRegistrationByChildren()
    {
        ChildRegistration__c[] childRegistrations = [select name, id, School__c, SchoolPhoneNumber__c, EducatorPhoneNumber__c, AdditionalPhoneNumber__c, 
                                                            ExpectedClass__c, SocialStatus__c, LearningStatus__c, EmotionalStatus__c, IsScholarship__c, ChildName__c,
                                                            SchoolName__c, Class__c, Educator__c, SchoolYearName__c, SupportsName__c from ChildRegistration__c 
                                                            where SchoolYearName__c = :CSchoolYearDb.GetActiveYearId()];
        activeChildRegistrationByChildren = new map<string, ChildRegistration__c>();
        for (ChildRegistration__c registration : childRegistrations)
        {
            activeChildRegistrationByChildren.put(registration.ChildName__c, registration);
        }
    }
    
    public Children__c UpdateGradeAndSchool(string childId)
    {
        if (childrenMap.containsKey(childId))
        {
            Children__c child = childrenMap.get(childId);
            child.SchoolName__c = null;
            child.Grade__c = '';
            child.IsThereAnAnnualRegistration__c = false;
            if (activeChildRegistrationByChildren.containsKey(childId))
            {
                child.SchoolName__c = activeChildRegistrationByChildren.get(childId).SchoolName__c;
                child.Grade__c = activeChildRegistrationByChildren.get(childId).Class__c;
                child.IsThereAnAnnualRegistration__c = true;
            }
            if (child.id != null)
                return child;
        }
        return null;
    }
    //v2 used from Child registration
    //TODO: remove from Schoolyear trigger and do in batch or scheduled apex.
    public static void UpdateGradeAndSchoolAndAnnualRegistration(List<ChildRegistration__c> registrations)
    {
        String yearId = CSchoolYearDb.GetActiveYearId();
        Set<String> childrenIds = new Set<String>();
        for(ChildRegistration__c r :registrations)
            childrenIds.add(r.ChildName__c);
        
        Map<string,Children__c> childrenMap = new Map<string,Children__c>([select Id From Children__c where Id in :childrenIds]);
        
        List<Children__c> childrenToUpdate = new List<Children__c>();
        for(ChildRegistration__c r :registrations){
            Children__c child = childrenMap.get(r.ChildName__c);
            if(child != null){
                if(r.SchoolYearName__c == yearId){
                    //child.IsActive__c = true;
                    child.SchoolName__c = r.SchoolName__c;
                    child.Grade__c = r.Class__c;
                    child.IsThereAnAnnualRegistration__c = true;
                    childrenToUpdate.add(child);
                }
            }
        }
        if(childrenToUpdate.size() > 0)
            update childrenToUpdate;
    }
    
    //
    //New case was added to Child: update according checkbox: IsThereCase   
    public static void UpdateIsThereCase(List<Case__c> cases)
    {
        String yearId = CSchoolYearDb.GetActiveYearId();
        Set<String> childrenIds = new Set<String>();
        for(Case__c c :cases)
            childrenIds.add(c.ChildName__c);
        
        Map<string,Children__c> childrenMap = new Map<string,Children__c>([select Id From Children__c where Id in :childrenIds]);
        
        List<Children__c> childrenToUpdate = new List<Children__c>();
        for(Case__c c :cases){
            Children__c child = childrenMap.get(c.ChildName__c);
            if(child != null){
                if(c.SchoolYear__c == yearId){
                    child.IsThereCase__c = true;
                    childrenToUpdate.add(child);
                }
            }
        }
        if(childrenToUpdate.size() > 0)
            update childrenToUpdate;
    }
    
    
    private map<string, User> usersMap;
    public map<string, User> pUsersMap
    {
        get
        {
            if (usersMap == null)
            {
                User[] users = [select name, id from User];
                usersMap = new map<string, User>();
                for (User us : users)
                {
                    usersMap.put(us.id, us);
                }
            }
            return usersMap;
        }
    }
    
    private list<Payment__c> paymentsList;
    public list<Payment__c> pPaymentsList
    {
        get
        {
            if (paymentsList == null)
            {
                map<string, string> paymentsIdsMap = new map<string, string>();
                list<ActivityReport__c> reportsList;
                list<SpecialActivitiesLine__c> activitiesList;
                if(schoolYear == null ) 
                {
                    reportsList = [select name, id, PaymentName__c from ActivityReport__c  where PaymentName__c != null and ChildName__c != null and ChildName__c = :pChild.id];
                    activitiesList = [select name, id, PaymentName__c from SpecialActivitiesLine__c 
                                     where PaymentName__c != null and ChildName__c != null and ChildName__c = :pChild.id];
                }
                else
                {
                    reportsList = [select name, id, PaymentName__c from ActivityReport__c  where PaymentName__c != null and ChildName__c != null and ChildName__c = :pChild.id 
                                    AND SchoolYear__c = :schoolYear.Id];
                    activitiesList = [select name, id, PaymentName__c from SpecialActivitiesLine__c where PaymentName__c != null and ChildName__c != null 
                                        and ChildName__c = :pChild.id AND PaymentName__r.SchoolYear__c = :schoolYear.Id];
                }
                
                for (ActivityReport__c report : reportsList)
                {
                    paymentsIdsMap.put(report.PaymentName__c, report.PaymentName__c);
                }
                for (SpecialActivitiesLine__c activity : activitiesList)
                {
                    paymentsIdsMap.put(activity.PaymentName__c, activity.PaymentName__c);
                }
                paymentsList = CPaymentDb.GetPaymentsByIds(paymentsIdsMap.values());
            }
            return paymentsList;
        }
    }
    
    private List<ActivityReport__c> reportsList;
    public List<ActivityReport__c> pReportsList
    {
        get
        {
            if (reportsList == null)
            {
                if(schoolYear == null ) 
                {
                    reportsList = CActivityReportDb.GetActivityReportsByChild(pChild.id);
                }
                else
                {
                    reportsList = CActivityReportDb.GetActivityReportsByChildAndYear(pChild.id, schoolYear.Id);
                }
            }
            return reportsList;
        }
    }
    
    private List<ActivityApproval__c> approvalsList;
    public List<ActivityApproval__c> pApprovalsList
    {
        get
        {
            if (approvalsList == null)
            {
                if(schoolYear == null ) 
                {
                    approvalsList = CActivityApprovalDb.GetActivityApprovalByChildId(pChild.id);
                }
                else
                {
                    approvalsList = CActivityApprovalDb.GetActivityApprovalByChildIdAndYear(pChild.id, schoolYear.Id);
                }
            }
            return approvalsList;
        }
    }
    
    private List<RequestForTreatment__c> requestsList;
    public List<RequestForTreatment__c> pRequestsList{
        get
        {
            if (requestsList == null)
            {
                if(schoolYear == null ) 
                {
                    requestsList = CRequestForTreatmentDb.GetRequestForTreatmentByChild(pChild.id);
                }
                else
                {
                    requestsList = CRequestForTreatmentDb.GetRequestForTreatmentByChildAndYear(pChild.id, schoolYear.Id);
                }
            }
            return requestsList;
        }
    }
    
    private List<Exam__c> examList1;
    public List<Exam__c> examList{
        get{
            if (examList1 == null){
                if(schoolYear == null ) 
                    examList1 = CExamDb.GetExamsByChildren(new List<Children__c>{pChild});
                else
                    examList1 = CExamDb.GetExamsByChildrenAndYear(new List<Children__c>{pChild},schoolYear);
            }
            return examList1;
        }
    }
    
    
    
}