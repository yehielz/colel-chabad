public with sharing class CChildFullReportFacad 
{
	private CChildFullReportDb childFullReportDb;
	public Children__c pChild{ get{ return childFullReportDb.pChild; } }
	public list<CSchoolYearInFullReport> pYearsList{ get{ return childFullReportDb.pYearsList; } }
    public ChildRegistration__c pChildRegistration{ get{ return childFullReportDb.pChildRegistration; } }
	public boolean IsHasYears{ get{ return pYearsList.size() > 0; } } 
	public boolean IsEnglish{ get; set; }
	
	
	public CChildFullReportFacad()
	{
		childFullReportDb = new CChildFullReportDb();	
	}
	public CChildFullReportFacad(Children__c child, boolean m_isInglish)
	{
		childFullReportDb = new CChildFullReportDb(child);
		IsEnglish = m_isInglish;	
	}
	
	public CChildFullReportFacad(Children__c child, list<CSchoolYearInFullReport> yearsList, map<string, boolean> view, map<string, boolean> yearsToView, ChildRegistration__c childRegistration)
	{
		childFullReportDb = new CChildFullReportDb(child, yearsList, view, yearsToView, childRegistration);	
	}
	
	public string todayDate
	{ 
		get
		{ 
			return CObjectNames.GetFullNumberInString(datetime.now().day()) + '/' + CObjectNames.GetFullNumberInString(datetime.now().month()) + '/' + datetime.now().year(); 
		}
	}
	public string childIdNumber{ get{ return CObjectNames.GetHebrewWords(pChild.IdNumber__c, 30); } }
	public string childName{ get{ return CObjectNames.GetHebrewWords(pChild.name, 30); } }
	public string birthDate
	{ 
		get
		{ 
			if (pChild.BirthDate__c != null)
				return CObjectNames.GetFullNumberInString(pChild.BirthDate__c.day()) + '/' + CObjectNames.GetFullNumberInString(pChild.BirthDate__c.month()) + '/' + pChild.BirthDate__c.year();
			return ''; 
		} 
	}
	public string hebrewBirthDate{ get{ return CObjectNames.GetHebrewWords(pChild.BirthDate__c != null ? pChild.HebrewBirthDay__c + ' ' + pChild.HebrewBirthMonth__c + ' ' + pChild.HebrewBirthYear__c : '', 30); } }
	public string grade{ get{ return CObjectNames.GetHebrewWords(pChild.Grade__c, 30); } }
	public string schoolName{ get{ return CObjectNames.GetHebrewWords(pChild.SchoolName__r.name, 30); } }
	public string city{ get{ return CObjectNames.GetHebrewWords(pChild.City__c, 30); } }
	public string englishName{ get{ return CObjectNames.GetHebrewWords(IsNull(pChild.EnglishFamilyName__c), 30) + ' ' + CObjectNames.GetHebrewWords(IsNull(pChild.EnglishFirstName__c), 30); } }
	public string coordinatorUserName{ get{ return CObjectNames.GetHebrewWords(pChild.Owner.Name, 30); } }
	public string learningStatus{ get{ return CObjectNames.GetHebrewWords(pChildRegistration.LearningStatus__c, 30); } }
	public string parentName{ get{ return CObjectNames.GetHebrewWords(pChild.FamilyName__r.ParentName__c, 30); } }
	public string address{ get{ return CObjectNames.GetHebrewWords(pChild.FamilyName__r.Address__c, 30); } }
	
	public string phone
	{ 
		get
		{ 
			if (pChild.MobilePhone__c == null)
				return CObjectNames.GetHebrewWords(pChild.FamilyName__r.HomePhone__c, 30);
			return CObjectNames.GetHebrewWords(pChild.MobilePhone__c, 30); 
		} 
	}
	
	public string FamilyBackground
	{
		get
		{
			string ret = '';
			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentDad || pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
			{
				if(pChild.FamilyName__r.DeathDate__c != null)
				{
					ret += '<div>' + CObjectNames.GetHebrewWords(('בתאריך ' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathDate__c.day()) + '/' + 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathDate__c.month()) + '/' + pChild.FamilyName__r.DeathDate__c.year() +
							' אבי המשפחה ' + pChild.FamilyName__r.DiedParentName__c + ' ז"ל נפטר '), 90);
					ret += '</div><div>';
					ret += CObjectNames.GetHebrewWords(('סיפור הפטירה: ' + pChild.FamilyName__r.DeathStory__c + ' השאיר אחריו ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' ילדים.'), 90);
					ret += '</div>';
				}
 			}
 			
 			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
				ret += '<br/>';
 			
 			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentMother || pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
			{
				if(pChild.FamilyName__r.DeathMotherDate__c != null)
				{
					ret += '<div>' + CObjectNames.GetHebrewWords('בתאריך ' + (pChild.FamilyName__r.DeathMotherDate__c != null ? 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathMotherDate__c.day()) + '/' + 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathMotherDate__c.month()) + '/' + 
							pChild.FamilyName__r.DeathMotherDate__c.year() : '') + ' אם המשפחה ' + pChild.FamilyName__r.DiedMotherName__c + ' ע"ה נפטרה ', 90);
					ret += '</div><div>';
					ret += CObjectNames.GetHebrewWords(('סיפור הפטירה: ' + pChild.FamilyName__r.DeathMotherStory__c + ' השאירה אחריה ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' ילדים.'), 90);
					ret += '</div>';
				}
 			}
			return ret;
		}
	}
	
	public string FamilyBackgroundWS
	{
		get
		{
			string ret = '';
			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentDad || pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
			{
				if(pChild.FamilyName__r.DeathDate__c != null)
				{
					ret += '<div>' + ('בתאריך ' + (pChild.FamilyName__r.DeathDate__c != null ? 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathDate__c.day()) + '/' + 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathDate__c.month()) + '/' + 
							pChild.FamilyName__r.DeathDate__c.year() : '') + ' אבי המשפחה ' + pChild.FamilyName__r.DiedParentName__c + ' ז"ל נפטר ');
					ret += '</div><div>';
					ret += ('סיפור הפטירה: ' + pChild.FamilyName__r.DeathStory__c + ' השאיר אחריו ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' ילדים.');
					ret += '</div>';
				}
				else ret += ('אב המשפחה נפטר והשאיר אחריו ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' ילדים.');
				
 			}
 			
 			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
				ret += '<br/>';
 			
 			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentMother || pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
			{
				if(pChild.FamilyName__r.DeathMotherDate__c!= null)
				{
					ret += '<div>' + 'בתאריך ' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathMotherDate__c.day()) + '/' + 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathMotherDate__c.month()) + '/' + pChild.FamilyName__r.DeathMotherDate__c.year() +
							' אם המשפחה ' + pChild.FamilyName__r.DiedMotherName__c + ' ע"ה נפטרה ';
					ret += '</div><div>';
					ret += ('סיפור הפטירה: ' + pChild.FamilyName__r.DeathMotherStory__c + ' השאירה אחריה ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' ילדים.');
					ret += '</div>';
				}
				else ret += ('אם המשפחה נפטרה והשאירה אחריה ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' ילדים.');
 			}
			return ret;
		}
	}
	public string FamilyBackgroundWSEN
	{
		get
		{
			string ret = '';
			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentDad || pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
			{
				if(pChild.FamilyName__r.DeathDate__c != null)
				{
					string fatherName = pChild.FamilyName__r.EnglishDiedFatherName__c == null ? pChild.FamilyName__r.DiedParentName__c : pChild.FamilyName__r.EnglishDiedFatherName__c;
					string death = pChild.FamilyName__r.EnglishDeathFatherStory__c == null ? pChild.FamilyName__r.DeathStory__c : pChild.FamilyName__r.EnglishDeathFatherStory__c;
					ret += '<div>' + ('at ' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathDate__c.day()) + '/' + 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathDate__c.month()) + '/' + pChild.FamilyName__r.DeathDate__c.year() +
							' the father ' + fatherName + ' Passed away ');
					ret += '</div><div>';
					ret += ('Death story: ' + death + ' and left behind ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' Children.');
					ret += '</div>';
				}
				else ret += ('The mother Passed away and left behind ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' Children.');
 			}
 			
 			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
				ret += '<br/>';
 			
 			if(pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentMother || pChild.FamilyName__r.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
			{
				if(pChild.FamilyName__r.DeathMotherDate__c!= null)
				{
					string fatherName = pChild.FamilyName__r.EnglishDiedMotherName__c == null ? pChild.FamilyName__r.DiedMotherName__c : pChild.FamilyName__r.EnglishDiedMotherName__c;
					string death = pChild.FamilyName__r.EnglishDeathMotherStory__c == null ? pChild.FamilyName__r.DeathMotherStory__c : pChild.FamilyName__r.EnglishDeathMotherStory__c;
					ret += '<div>' + 'at ' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathMotherDate__c.day()) + '/' + 
							CObjectNames.GetFullNumberInString(pChild.FamilyName__r.DeathMotherDate__c.month()) + '/' + pChild.FamilyName__r.DeathMotherDate__c.year() +
							' the mother ' + pChild.FamilyName__r.EnglishDiedMotherName__c + ' Passed away ';
					ret += '</div><div>';
					ret += ('Death story: ' + death + ' and left behind ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' Children.');
					ret += '</div>';
				}
				else ret += ('The mother Passed away and left behind ' + pChild.FamilyName__r.ChildrenNumberInFamily__c + ' Children.');
				
 			}
			return ret;
		}
	}
	
	public string registrationDate
	{ 
		get
		{
			if (pChild.FamilyName__r.RegistrationDate__c != null)
			{
				string ret = '<div>' + CObjectNames.GetHebrewWords(('התחלנו פעילות עם המשפחה בתאריך ') + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.day()) + '/' + 
							  CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.month()) + '/' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.year()), 90);
				ret += '</div>';
				return ret;
			}
			return null;
		} 
	}
	
	public string registrationDateWS
	{ 
		get
		{
			if (pChild.FamilyName__r.RegistrationDate__c != null)
			{
				string ret = '<div>';
				if (IsEnglish)
				{
					ret = '<div>' + ('We started the activity with the family on ') + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.day()) + '/' + 
							  CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.month()) + '/' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.year());
				}
				else
				{
				ret = '<div>' + ('התחלנו פעילות עם המשפחה בתאריך ') + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.day()) + '/' + 
							  	CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.month()) + '/' + CObjectNames.GetFullNumberInString(pChild.FamilyName__r.RegistrationDate__c.year());
				}
				ret += '</div>';
				
				return ret;
			}
			return null;
		} 
	}
	
	private string IsNull(string val)
	{
		return val == null ? '' : val;
	}
}