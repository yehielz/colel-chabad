public without sharing class CWebSiteMonthesFacad 
{
	private Tutor__c tutor { get; set; }
	public Tutor__c pTutor { 
		get{
			if (tutor == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null)
						tutor = CTutorDb.GetTutorById(sessionId.TutorName__c);
				}
			}
			return tutor;
		} 
	}
	
	private ActivityApproval__c approval { get; set; }
	public ActivityApproval__c pApproval { 
		get{
			if (approval == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null){
						approval = CActivityApprovalDb.GetActivityApprovalById(sessionId.ActivityApproval__c);
					}
				}
			}
			return approval;
		} 
	}
	
	private Family__c family { get; set; }
    public Family__c pfamily { 
        get{
            if (family == null){
                if (ApexPages.currentPage().getCookies().get('sessionId') != null){
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null)
                        family = CFamilyDb.GetFamilyById(sessionId.Family__c);
                }
            }
            return family;
        } 
    }
	
	private CWebSiteMonthesDb webSiteMonthesDb;
	
	public CWebSiteMonthesFacad()
	{
		webSiteMonthesDb = new CWebSiteMonthesDb();
	}
	
	public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
				return null;
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
	
	public List<SelectOption> monthesList
	{
		get
		{
			return webSiteMonthesDb.GetMonthesList();
		}
	}
	
	public string month
	{
		get
		{
			return webSiteMonthesDb.month;
		}
		set
		{
			webSiteMonthesDb.month = value;
		}
	}
	
	public string monthWarning
	{
		get
		{
			return webSiteMonthesDb.monthWarning;
		}
		set
		{
			webSiteMonthesDb.monthWarning = value;
		}
	}
	
	public PageReference ShowReports()
	{
		// if (month != 'null')
		// {
			if(pApproval != null){
				List<ActivityReport__c> AllReportsDisplay = [SELECT Id FROM ActivityReport__c 
														WHERE ActivityApprovalName__c = :pApproval.Id 
														AND ProfessionName__c = :pApproval.ProfessionName__c];
				if(AllReportsDisplay.size() > 0 ){
					return new PageReference(CObjectNames.WebSitePageReportsDisplay + '&AR=' + pApproval.Id);
				}
				else{
					return new Pagereference(CObjectNames.WebSitePageReport + '&childId='+pApproval.ChildName__c+'&month='+ month +'&professionId=' + pApproval.ProfessionName__c);
				}
			}
		// }
		// monthWarning = 'עליך לבחור חודש';
		return null;
	}

	public PageReference ShowChildren()
	{
		if (month != 'null')
		{
			if(pApproval != null){
				//find existing ActivityReport__c and open it if existing otherwise open a new ActivityReport.
				List<ActivityReport__c> reports = [select Id From ActivityReport__c 
														where ActivityApprovalName__c = :pApproval.Id 
														and ProfessionName__c = :pApproval.ProfessionName__c 
														and  ActivityReportStatus__c = 'בעריכה' 
														and ReportMonth__c = :month ];
				if(reports.size() > 0 )
					return new Pagereference(CObjectNames.WebSitePageReport + '&childId='+pApproval.ChildName__c+'&month='+ month +'&professionId=' + pApproval.ProfessionName__c + '&AR=' + reports[0].Id);
				else
					return new Pagereference(CObjectNames.WebSitePageReport + '&childId='+pApproval.ChildName__c+'&month='+ month +'&professionId=' + pApproval.ProfessionName__c);
			}
			else{
				return new PageReference(CObjectNames.WebSitePageChildren + '&month=' + month);
			}
		}
		monthWarning = 'עליך לבחור חודש';
		return null;
	}
    
    public PageReference LogOut()
	{
		Cookie sessionIdCookie = new Cookie('sessionId', null, null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { sessionIdCookie } );
		CSessionIdDb.DeleteSessionsIdWithLastTime();
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
}