public with sharing class CWSSupporterChildViewDb 
{
	public Support__c pSupporter { get; set; }
	public Children__c pChild { get; set; }
	public ChildRegistration__c pChildRegistration { get; set; }
	public boolean IsEnglish { get; set; }
	
    public CWSSupporterChildViewDb()
    {
    	SetFromSession();
    	SetIsEnglish();
    	SetChild();
    	SetChildRegistration();
    }
    
    private void SetIsEnglish() 
    {
    	IsEnglish = false;
    	if (pSupporter != null && pSupporter.LanguageForWS__c != null && pSupporter.LanguageForWS__c.contains('English'))
    		IsEnglish = true;
		string lang = Apexpages.currentPage().getParameters().get('lang');
    	if (lang != null && lang == 'en')
    		IsEnglish = true;
    	if (lang != null && lang == 'he')
    		IsEnglish = false;
    }
    
    private void SetFromSession() 
    {
    	if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null)
			{
				pSupporter = CSupportDb.GetSupporterById(sessionId.SupporterName__c);
			}
		}
		if (pSupporter == null)
			pSupporter = new Support__c();
    }
    
    private void SetChild()
    {
    	string childId = Apexpages.currentPage().getParameters().get('childId');
    	if (childId != null && childId.trim() != null)
    		pChild = CChildDb.GetChildById(childId);
    	if (pChild == null)
    		pChild = new Children__c();
    }
    
    private void SetChildRegistration()
    {
    	if (pChild.id != null)
    	{
	    	list<ChildRegistration__c> registrations = CChildRegistrationDb.GetChildRegistrationByChild(pChild.Id);
	    	for (ChildRegistration__c registration : registrations)
	    	{
	    		if (pChildRegistration == null)
	    			pChildRegistration = registration;
	    		else
	    		{
		    		date newStartDate = registration.SchoolYearName__r.StartDate__c ; 
	    			boolean newIsActive = registration.SchoolYearName__r.IsActive__c == null ? false : registration.SchoolYearName__r.IsActive__c; 
	    			date existStartDate = pChildRegistration.SchoolYearName__r.StartDate__c; 
	    			if (newIsActive || existStartDate == null || (newStartDate != null && newStartDate > existStartDate))
	    				pChildRegistration = registration;
	    		}
	    	}
    	}
    	if (pChildRegistration == null)
    		pChildRegistration = new ChildRegistration__c();
    }
	
	private map<string, string> imgByGenderMap { get; set; }
	public map<string, string> pImgByGenderMap
	{ 
		get
		{
			if (imgByGenderMap == null)
			{
				imgByGenderMap = new map<string, string>();
				StaticResource[] res = [select name, id, Body, SystemModstamp, NamespacePrefix from StaticResource where name = 'GirlPicture' or name = 'BoyPicture'];
				for (StaticResource re : res)
				{
					string img = '<img src="/resource/' + re.SystemModStamp.getTime() + '/' + (re.NamespacePrefix != null && re.NamespacePrefix != ''
									 ? re.NamespacePrefix + '__' : '') + re.name + '"/>';
					imgByGenderMap.put(re.name, img);
				}
			}
			return imgByGenderMap;
		}
	}
	
	public string ChildPicture
	{
		get
		{
			if (pChild.Picture__c != null && pChild.Picture__c.contains('<img'))
				return pChild.Picture__c;
			else
			{
				if (pChild.Gender__c == 'נקבה' && pImgByGenderMap.containsKey('GirlPicture'))
					return pImgByGenderMap.get('GirlPicture');
				else if (pImgByGenderMap.containsKey('BoyPicture'))
					return pImgByGenderMap.get('BoyPicture');
				return '';
			}
		}
	}
	
	public string ChildAge
	{
		get
		{
			date toDay = datetime.now().date();
			date birthDate = (pChild != null && pChild.BirthDate__c != null ? pChild.BirthDate__c : toDay);
			integer ret = toDay.year() - birthDate.year();
			if (date.newinstance(birthDate.year(), toDay.month(), toDay.day()) <= birthDate)
				ret++;
			ret = ret < 0 ? 0 : ret;
			return string.valueOf(ret);
		}
	}
	
	public boolean IsBlur 
    { 
    	get
		{
			if (Apexpages.currentPage().getParameters().get('b') != null && Apexpages.currentPage().getParameters().get('b') == 'b')
	    		return true;
	    	return false;
		} 
	}
	
	public boolean IsLastName 
    { 
    	get
		{
			if (Apexpages.currentPage().getParameters().get('n') != null && Apexpages.currentPage().getParameters().get('n') == 'n')
	    		return true;
	    	return false;
		} 
	}
}