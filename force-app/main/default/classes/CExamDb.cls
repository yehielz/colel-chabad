public with sharing class CExamDb 
{
	private Exam__c pExam { get; set; }
	
	public CExamDb()
	{
		pExam = new Exam__c();
	}
	
	public CExamDb(ApexPages.StandardController controller,String childId)
	{
		
		pExam = (Exam__c)controller.getRecord();
		if( childId  != null && childId != '')
			pExam.ChildName__c = childId;
		if (pExam.id != null)
			SetpExamBypExamId(pExam.id);
		else
		{
			SetDate();
			SetByChild();
			SetByChildRegistration();
		}
	}
	
	private void SetpExamBypExamId(string id)
	{
		pExam = [select ExamType__c, Grade__c, ChildName__c, ChildRegistration__c, ScholarshipPoints__c,
				Date__c, ProfessionName__c, GradeInWords__c, id, name, CoordinatorName__c , OwnerId from Exam__c where id = :id];
	}
	
	public void save()
	{
		GetExamName();
		SetCoordinatorName();
		if (pExam.id == null)
			insert pExam;
		else
			update pExam;
	}
	
	private void GetExamName()
	{
		if (pExam.ChildName__c != null && pExam.ProfessionName__c != null && pExam.Date__c != null)
		{
			Children__c[] child = [select name, id from Children__c where id = :pExam.ChildName__c];
			Profession__c[] profession = [select name, id from Profession__c where id = :pExam.ProfessionName__c];
			pExam.name = child[0].name + ' - ' + profession[0].name + ' - ' + pExam.Date__c.day() + '/' + pExam.Date__c.Month() + '/' + pExam.Date__c.Year();
		} 
	}
	
	private void SetCoordinatorName()
	{
		Children__c[] children = [select name,OwnerId, id from Children__c where id = :pExam.ChildName__c];
		if (children.size() > 0){
			
			pExam.OwnerId = children[0].OwnerId;
		}
		else{
			pExam.OwnerId = null;
		}
	}
	
	public void CheckGradeWords()
	{
		if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordExcellent)
			pExam.Grade__c = CObjectNames.ExamGrade100;
		else if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordVeryGood)
			pExam.Grade__c = CObjectNames.ExamGrade95;
		else if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordAlmostVeryGood)
			pExam.Grade__c = CObjectNames.ExamGrade90;
		else if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordgood)
			pExam.Grade__c = CObjectNames.ExamGrade80;
		else if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordAlmostGood)
			pExam.Grade__c = CObjectNames.ExamGrade70;
		else if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordEnough)
			pExam.Grade__c = CObjectNames.ExamGrade60;
		else if (pExam.GradeInWords__c == CObjectNames.ExamGradeInWordNotEnough)
			pExam.Grade__c = CObjectNames.ExamGrade50; 
	}
	
	private void SetDate()
    {
        pExam.Date__c = system.now().date();
    }
    
    private void SetByChild()
    {
    	if (pExam.ChildName__c != null)	
    	{
        	string ActiveSchoolYearId = CSchoolYearDb.GetActiveYearId();
        	ChildRegistration__c[] childRegistration = [select name, id from ChildRegistration__c where ChildName__c = :pExam.ChildName__c and SchoolYearName__c = :ActiveSchoolYearId];
        	if (childRegistration.size() > 0)
        		pExam.ChildRegistration__c = childRegistration[0].id;
    	}
    }
    
    private void SetByChildRegistration()
    {
    	pExam.ChildRegistration__c = pExam.ChildRegistration__c;
    	if (pExam.ChildRegistration__c != null)	
    	{
        	ChildRegistration__c[] childRegistration = [select name, id, ChildName__c from ChildRegistration__c where id = :pExam.ChildRegistration__c];
        	if (childRegistration.size() > 0)
        		pExam.ChildName__c = childRegistration[0].ChildName__c;
    	}
    }
	
	public Exam__c GetpExam()
	{
		return pExam;
	}
	
	public list<SelectOption> GetChildRegistrationList()
	{
		list<SelectOption> ret = new list<SelectOption> { new SelectOption ('', '--ללא--') };
		if (pExam.ChildName__c != null)
		{
			ChildRegistration__c[] childRegistrations = [select name, id, SchoolYearName__r.name from ChildRegistration__c where ChildName__c = :pExam.ChildName__c];
			
			for (integer i = 0; i < childRegistrations.size(); i++)
			{
				if(childRegistrations[i].SchoolYearName__c != null)
					ret.add(new SelectOption(childRegistrations[i].id, childRegistrations[i].SchoolYearName__r.name));
			}
		}
		return ret;
	}
	
	public static list<Exam__c> GetExamByChildIdAndProfessions(string childId, list<string> professionsId){
		return [select ExamType__c, Grade__c, ChildName__c, ChildRegistration__c, ScholarshipPoints__c, Date__c, ProfessionName__c, GradeInWords__c, 
				id, name from Exam__c where ChildName__c = :childId and ProfessionName__c in :professionsId];
	}
	
	public static list<Exam__c> GetExamsByChildren(list<Children__c> children)	{
		return [select ExamType__c, Grade__c, ChildName__c, ChildRegistration__c, ScholarshipPoints__c, Date__c, 
				ProfessionName__c, GradeInWords__c, id, name 
				from Exam__c 
				where ChildName__c in :children
				order by Date__c desc];
	}
	
	public static list<Exam__c> GetExamsByChildrenAndYear(list<Children__c> children,SchoolYear__c year)	{
		return [select ExamType__c, Grade__c, ChildName__c, ChildRegistration__c, ScholarshipPoints__c, Date__c, 
				ProfessionName__c, GradeInWords__c, id, name 
				from Exam__c 
				where ChildName__c in :children 
				and Date__c >= :year.StartDate__c
				and Date__c <= :year.EndOfSchoolYear__c
				order by Date__c desc];
	}
	
	public static list<Exam__c> GetExamByChildrenIdAndProfessions(list<string> childrenIds, list<string> professionsId){
		return [select ExamType__c, Grade__c, ChildName__c, ChildRegistration__c, ScholarshipPoints__c, Date__c, ProfessionName__c, GradeInWords__c, 
				id, name from Exam__c where ChildName__c in :childrenIds and ProfessionName__c in :professionsId];
	}
	
	/*lpublic void CreateTaskAfterInsertExam(Exam__c newExam)
    {
    	//ist<Task> listToInsert = new list<Task>();
		list<string> ownersId = GetUsersIdToOwners();
		Children__c child = getChild(newExam);
		for (integer i = 0; i < ownersId.size(); i++)
		{
			//Task newTask = new Task();
			//newTask.ChildName__c = child.name;
			//newTask.FamilyName__c = child.FamilyName__r.name;
			//newTask.City__c = child.FamilyName__r.City__c;
			//newTask.CoordinatorUserName__c = system.Userinfo.getName();
			//newTask.Description = CObjectNames.TaskWaitForYouMale + CObjectNames.TaskSobjectNewExam;
			//newTask.Subject = CObjectNames.TaskSobjectNewExam;
			//newTask.WhatId = newExam.id;
			//newTask.OwnerId = ownersId[i];
			//newTask.ActivityDate = datetime.now().date().addDays(7);
			//listToInsert.add(newTask);
		}
		if (listToInsert.size() > 0)
			insert listToInsert;
    }*/
    
    /*public Children__c getChild(Exam__c newExam)
	{
		if (newExam.ChildName__c != null)
		{
			Children__c[] child = [select name, id, FamilyName__r.name, FamilyName__r.City__c from Children__c where id = :newExam.ChildName__c];
			if (child.size() > 0)
			{
				return child[0];
			}
		}
		return null;
	}*/
    
    /*private string[] GetUsersIdToOwners()
    {
    	UserRole[] userRoles = [select name, id from UserRole where name = :CObjectNames.RoleActivityManager];
		User[] managers = [select name, id from User where UserRoleId = :userRoles[0].id];
		list<string> ret = new list<string>();
		for (integer i = 0; i < managers.size(); i++)
			ret.add(managers[i].id);
		return ret;
    }*/ 
	
	private static map<string, list<Exam__c>> pExamsByChildren { get; set; }
	
	public static void SetExamsByChildren(list<Children__c> children){
		list<Exam__c> exams = CExamDb.GetExamsByChildren(children);
		pExamsByChildren = new map<string, list<Exam__c>>();
		for (Exam__c exam : exams)	{
			if (pExamsByChildren.containsKey(exam.ChildName__c))
				pExamsByChildren.get(exam.ChildName__c).add(exam);
			else
				pExamsByChildren.put(exam.ChildName__c, new list<Exam__c> { exam } );
		}
	}
	
	public static list<Exam__c> UpdateExamsByChild(string childId, string coordinatorUserName,String OwnerId)	{
		if (pExamsByChildren.containsKey(childId))	{
			List<Exam__c> listToUpdate = pExamsByChildren.get(childId);
			for (Exam__c exam : listToUpdate){
				//exam.CoordinatorName__c = coordinatorUserName;
				exam.OwnerId = OwnerId;
			}
			if (listToUpdate != null && listToUpdate.size() > 0){
				return listToUpdate;
			}
		}
		return new list<Exam__c>();
	}
}