public with sharing class CMyDate 
{
	public CMyDate(integer year_y, integer month_m, integer day_d)
	{
		Year = year_y;
		Month = month_m;
		Day = day_d;
	}
	public integer Year;
	public integer Month;
	public integer Day;
}