public without sharing class CWebSiteLogInDb 
{
    public string UserNameWarning { get; set; }
    public string PasswordWarning { get; set; }
    public string approvalCodeWarning { get; set; }
    public string pUserName { get; set; }
    public string pPassword { get; set; }
    public string approvalCode { get; set; }
    public SessionID__c pSessionId { get; set; }
    public Support__c pSupporter { get; set; }
    //public Tutor__c pTutor { get; set; }
    public Family__c pFamily { get; set; }
    public ActivityApproval__c approval { get; set; }
    
    
    public CWebSiteLogInDb() {
        pSessionId = new SessionID__c();
        pUserName = Apexpages.currentPage().getParameters().get('UserName');
        pPassword = Apexpages.currentPage().getParameters().get('Password');
        approvalCode = Apexpages.currentPage().getParameters().get('ApprovalCode');
    }
    
    
    public boolean IfExist()  {
        UserNameWarning = '';
        PasswordWarning = '';
        boolean ret = validateUserName();
        if (ret)
            ret = validatePassword();
        pSupporter = CSupportDb.GetSupporterByLoginInf(pUserName, pPassword);
        //pTutor = CTutorDb.GetTutorByLoginInf(pUserName, pPassword);
        pFamily = CFamilyDb.GetFamilyByLoginInf(pUserName, pPassword);
        return ret;
    }
    
    public boolean IfExistApprovalCode()  {
        UserNameWarning = '';
        approvalCodeWarning = '';
        boolean ret = validateUserName();
        if (ret)
            ret = validateApprovalCode();
        
        approval = CActivityApprovalDb.GetApprovalByLoginInf( pUserName, approvalCode);
        //if(approval != null) pFamily = CFamilyDb.GetFamilyById(approval.ChildName__r.FamilyName__c);
        return ret;
    }
    
    private boolean validateUserName() {
        if (pUserName == null || pUserName == '')  {
            UserNameWarning = CObjectNames.UserNameIsNull;
            return false;
        }
        else if (!CSupportDb.IsUserNameExist(pUserName) /*&& !CTutorDb.IsUserNameExist(pUserName)*/ && !CFamilyDb.IsUserNameExist(pUserName))  {
            UserNameWarning = CObjectNames.UserNameIsNotExist;
            return false;
        }
        return true;
    }
    
    private boolean validatePassword() {
        if (pPassword == null || pPassword == '')  {
            PasswordWarning = CObjectNames.PasswordIsNull;
            return false;
        } 
        else if (  !CSupportDb.IsUserNameAndPasswordExist(pUserName, pPassword) 
                //&& !CTutorDb.IsUserNameAndPasswordExist(pUserName, pPassword)
                && !CFamilyDb.IsUserNameAndPasswordExist(pUserName, pPassword))
        {
            PasswordWarning = CObjectNames.PasswordIsNotValid;
            return false;
        }
        return true;
    }
    
    private boolean validateApprovalCode() {
        if (approvalCode == null || approvalCode == '')  {
            approvalCodeWarning = CObjectNames.PasswordIsNull;
            return false;
        } 
        else if (  !CActivityApprovalDb.IsUserNameAndApprovalCodeExist(pUserName, approvalCode))
        {
            approvalCodeWarning = CObjectNames.PasswordIsNotValid;
            return false;
        }
        return true;
    }
    
    public void SetCookie() {
        Cookie sessionId = new Cookie('sessionId', insertSessionId(), null, -1, false);
        ApexPages.currentPage().setCookies(new Cookie[] { sessionId } );
    }
    
    private string InsertSessionId() {
        if (pSupporter != null /*|| pTutor != null*/ || pFamily != null || approval != null) {
            pSessionId = new SessionID__c();
            pSessionId.name = string.valueOf(Math.random());
            pSessionId.SupporterName__c = pSupporter != null ? pSupporter.id : null;
            //pSessionId.TutorName__c = pTutor != null ? pTutor.id : null;
            pSessionId.Family__c = pFamily != null ? pFamily.id : null;
            pSessionId.ActivityApproval__c = approval != null?  approval.Id:null;
            pSessionId.InTime__c = datetime.now();
            pSessionId.LestActivityTime__c = datetime.now();
            insert pSessionId;
            return pSessionId.id;
        }
        return '';
    }
    
    public void forTest() {
        pUserName = '';
        ValidateUserName();
        pUserName = 'l';
        ValidateUserName();
        pUserName = 'levi';
        ValidateUserName();
        pPassword = '';
        ValidatePassword();
        pPassword = 'q';
        ValidatePassword();
        pPassword = '123';
        SetCookie();
        IfExist();
        ValidatePassword();
    }
}