public without sharing class CWebSiteReportsDisplayDb 
{
	public string fromMonthM { get; set; }
	public string fromMonthY { get; set; }
	public string toMonthM { get; set; }
	public string toMonthY { get; set; }
	
	public list<CActivityReportInReportsWebSite> GetActivityReportsLines()
	{
		System.debug('Je suis ici 1');
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			System.debug('Je suis ici 2');
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			System.debug('sessionId ===> ' + sessionId);
			if (sessionId != null)
			{
				System.debug('Je suis ici 3');
				list<CActivityReportInReportsWebSite> ret = new list<CActivityReportInReportsWebSite>();
				ActivityReport__c[] activityReport = [select ActivityReportStatus__c, ActivityApprovalName__c, TutorName__c,  id, name, ToDateClass__c, TalkTotalHoursToCalculate__c, Note__c,
															 ChildName__c, ChildName__r.name, ProfessionName__c, ProfessionName__r.name, ReasonForDecline__c, PaymentAmountClass__c, CreatedDate, 
															 PaymentName__c, TalkTotalHours__c, ReportMonth__c, TalkType__c, ReceivingImageClass__c, ChildLastName__c, TalkWithDirectorOrTeacherOrParent__c, 
															 CoordinatorUserName__c, ChildFirstName__c, ClarificationReason__c, TalkStartTime__c, TalkEndTime__c, TotalPayment__c, IsForFindOut__c, 
															 MonthToFiltering__c, FromDateClass__c from ActivityReport__c 
															 WHERE ((TutorName__c != null and TutorName__c = :sessionId.TutorName__c) or ChildName__r.FamilyName__c = :sessionId.Family__c) and ReportMonth__c != null
															 order by MonthToFiltering__c];
				System.debug('activityReport =====> ' + activityReport);
				for (integer i = 0; i < activityReport.size(); i++)
				{
					if (validateIsToAdd(activityReport[i])){
						System.debug('Ajout =====> ' + activityReport[i]);
						ret.add(new CActivityReportInReportsWebSite(activityReport[i]));
					}
				}
				return ret;
			}
		}
		return new list<CActivityReportInReportsWebSite>();
	}

	public list<CActivityReportInReportsWebSite> GetActivityReportsLinesTutors()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null)
			{
				list<CActivityReportInReportsWebSite> ret = new list<CActivityReportInReportsWebSite>();
				ActivityReport__c[] activityReport = [SELECT  ActivityReportStatus__c, ActivityApprovalName__c, TutorName__c,  id, name, ToDateClass__c, TalkTotalHoursToCalculate__c, Note__c, ChildName__c, ChildName__r.name, ProfessionName__c, ProfessionName__r.name, ReasonForDecline__c, PaymentAmountClass__c, CreatedDate, PaymentName__c, TalkTotalHours__c, ReportMonth__c, TalkType__c, ReceivingImageClass__c, ChildLastName__c, TalkWithDirectorOrTeacherOrParent__c, CoordinatorUserName__c, ChildFirstName__c, ClarificationReason__c, TalkStartTime__c, TalkEndTime__c, TotalPayment__c, IsForFindOut__c, MonthToFiltering__c, FromDateClass__c 
														FROM ActivityReport__c 
														WHERE  ActivityApprovalName__c =: sessionId.ActivityApproval__c
														ORDER BY MonthToFiltering__c];
				System.debug('SELECT  ActivityReportStatus__c, ActivityApprovalName__c, TutorName__c,  id, name, ToDateClass__c, TalkTotalHoursToCalculate__c, Note__c, ChildName__c, ChildName__r.name, ProfessionName__c, ProfessionName__r.name, ReasonForDecline__c, PaymentAmountClass__c, CreatedDate, PaymentName__c, TalkTotalHours__c, ReportMonth__c, TalkType__c, ReceivingImageClass__c, ChildLastName__c, TalkWithDirectorOrTeacherOrParent__c, CoordinatorUserName__c, ChildFirstName__c, ClarificationReason__c, TalkStartTime__c, TalkEndTime__c, TotalPayment__c, IsForFindOut__c, MonthToFiltering__c, FromDateClass__c FROM ActivityReport__c WHERE  TutorName__c =: '+sessionId.ActivityApproval__c+' ORDER BY MonthToFiltering__c');
				System.debug('Size : ' + activityReport.size())														;
				for (integer i = 0; i < activityReport.size(); i++)
				{
					if (validateIsToAdd(activityReport[i])){
						ret.add(new CActivityReportInReportsWebSite(activityReport[i]));
					}
				}
				System.debug('ret : ' + ret);
				return ret;
			}
		}
		return new list<CActivityReportInReportsWebSite>();
	}
	
	private boolean validateIsToAdd(ActivityReport__c activityReport)
	{
		if (fromMonthY == null || fromMonthM == null || toMonthY == null || toMonthM == null)
			return true;
		if (activityReport.ReportMonth__c == null)
			return true;
		integer month = integer.valueOf(activityReport.ReportMonth__c.split('-')[0]);
		integer year = integer.valueOf(activityReport.ReportMonth__c.split('-')[1]);
		if (year < integer.valueOf(fromMonthY) || year > integer.valueOf(toMonthY))
			return false;
		if (year == integer.valueOf(fromMonthY) && month < integer.valueOf(fromMonthM))
			return false;
		if (year == integer.valueOf(toMonthY) && month > integer.valueOf(toMonthM))
			return false;
		return true;
	}
	
	public void forTest()
	{
		ActivityReport__c theReport = new ActivityReport__c();
		validateIsToAdd(theReport);
		fromMonthM = '06';
		fromMonthY = '2011';
		toMonthM = '06';
		toMonthY = '2013';
		validateIsToAdd(theReport);
		theReport.ReportMonth__c = '6-2014';
		validateIsToAdd(theReport);
		theReport.ReportMonth__c = '6-2010';
		validateIsToAdd(theReport);
		theReport.ReportMonth__c = '5-2011';
		validateIsToAdd(theReport);
		theReport.ReportMonth__c = '7-2013';
		validateIsToAdd(theReport);
		theReport.ReportMonth__c = '6-2012';
		validateIsToAdd(theReport);
	}
}