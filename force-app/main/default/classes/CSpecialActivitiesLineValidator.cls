public with sharing class CSpecialActivitiesLineValidator 
{
	private CSpecialActivitiesLineFacad cSpecialActivitiesLineFacad { get; set; }
	public string ErrorMessage;
	
	public CSpecialActivitiesLineValidator(CSpecialActivitiesLineFacad ExtSpecialActivitiesLineFacad)
	{
		ErrorMessage = '';
		cSpecialActivitiesLineFacad = ExtSpecialActivitiesLineFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateChildOrFamily();
		if (ret) 
	    	ret = ValidateFamilyName();
    	if (ret) 
	    	ret = ValidateChildName();
		if (ret) 
	    	ret = ValidateSpecialActivitiesName();
	  	    
	 	return ret;
	}
	
	private boolean ValidateChildOrFamily()
	{
		if (cSpecialActivitiesLineFacad.pSpecialActivityLine.ChildOrFamily__c == null)
		{
			ErrorMessage = CErrorMessages.SpecialActivitiesLineChildOrFamily;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFamilyName()
	{
		if (cSpecialActivitiesLineFacad.pSpecialActivityLine.ChildOrFamily__c == CObjectNames.Family && cSpecialActivitiesLineFacad.pSpecialActivityLine.FamilyName__c == null)
		{
			ErrorMessage = CErrorMessages.SpecialActivitiesLineFamilyName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateChildName()
	{
		if (cSpecialActivitiesLineFacad.pSpecialActivityLine.ChildOrFamily__c == CObjectNames.FamilyMember && cSpecialActivitiesLineFacad.pSpecialActivityLine.ChildName__c == null)
		{
			ErrorMessage = CErrorMessages.SpecialActivitiesLineChildName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateSpecialActivitiesName()
	{
		if (cSpecialActivitiesLineFacad.pSpecialActivityLine.SpecialActivitiesName__c == null)
		{
			ErrorMessage = CErrorMessages.SpecialActivitiesLineSpecialActivitiesName;
			return false;
		}
		return true;
	}
}