public with sharing class CProfessionByChild 
{
	public Children__c child { get; set; }
	public map<string, Profession__c> professions { get; set; }
	
	public CProfessionByChild(Children__c mChild, map<string, Profession__c> cProfessions)
	{
		child = mChild;
		professions = cProfessions;
	}
	
	/*public void addProfession(Profession__c newValue)
	{
		boolean isToAdd = false;
		for (integer i = 0 ; i < professions.size(); i++)
		{
			if (professions[i].id == newValue.id)
			{
				isToAdd = true;
			}
		}
		if (!isToAdd)
			professions.add(newValue);
	}*/
}