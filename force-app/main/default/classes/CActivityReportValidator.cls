public with sharing class CActivityReportValidator 
{
	private CActivityReportFacad cActivityReportFacad { get; set; }
	public string ErrorMessage;
	
	public CActivityReportValidator()
	{
		
	}
	
	public CActivityReportValidator(CActivityReportFacad ExtActivityReportFacad)
	{
		ErrorMessage = '';
		cActivityReportFacad = ExtActivityReportFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	    
    	if (ret)
    		ret = ValidateActivityApprovalName();
		if (ret)
    		ret = ValidateReportMonth();
    	if (ret && !cActivityReportFacad.bAmountAndReceipt) 
    	{
	    	ret = ValidateLines();
    	}
    	if (ret && cActivityReportFacad.bAmountAndReceipt)
    	{
			ret = ValidateReport();
    	}
    	if (ret)
    		ret = ValidateTalk();
    	   
    	   if (ret)
    		ret = ValidateChangeStatusPermission();
    	   
	 	return ret;
	}
	
	private boolean ValidateChangeStatusPermission()
	{ 
		if (cActivityReportFacad.pActivityReport.ActivityReportStatus__c == 'לתשלום')
		{
			
			Profile[] userProfile = [select name, id from Profile where (name='מנהל מערכת' or name='מנהל המערכת' or name = 'מנהל מחלקה' or name = 'System Administrator') and id=:System.Userinfo.getProfileId()]; 
			//System.debug('userProfile: '+userProfile.size());
			if(userProfile.size() > 0)
				return true;
				
			ErrorMessage = 'רק מנהל רשאי לשנות מצב דיווח לתשלום !';
			return false;
		}
		
		return true;
	}
	
	
	private boolean ValidateReportMonth()
	{
		if (cActivityReportFacad.pActivityReport.ReportMonth__c == null || cActivityReportFacad.pActivityReport.ReportMonth__c == '')
		{
			ErrorMessage = CErrorMessages.ActivityReportReportMonth;
			return false;
		}
		//ActivityReport__c[] activityReprot = [select name, id, ReportMonth__c, ActivityApprovalName__r.name from ActivityReport__c where id != :cActivityReportFacad.pActivityReport.id
		//											and ActivityApprovalName__c = :cActivityReportFacad.pActivityReport.ActivityApprovalName__c and ReportMonth__c = :cActivityReportFacad.pActivityReport.ReportMonth__c];
		//if (activityReprot.size() > 0)
		//{
		//	ErrorMessage = CErrorMessages.ActivityReportToActivityApproval + activityReprot[0].ActivityApprovalName__r.name + CErrorMessages.ActivityReportHasReportToTheMonth + cActivityReportFacad.pActivityReport.ReportMonth__c;
		//	return false;
		//}
		
		return true;
	}
	
	private boolean ValidateActivityApprovalName()
	{
		if (cActivityReportFacad.pActivityReport.ActivityApprovalName__c == null)
		{
			ErrorMessage = CErrorMessages.ActivityReportActivityApprovalName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateReport()
	{
		boolean ret = true;
		
		if (ret)
			ret = ValidateReasonForDecline();
		if (ret)
			ret = ValidateFromDate();
		if (ret)
			ret = ValidateToDate();
		if (ret)
			ret = ValidateFromDateNotBigFromToDate();
		if (ret)
			ret = ValidatePaymentAmount();
		/*if (ret)
			ret = ValidateTalkWithDirectorOrTeacherOrParent();*/
		
		return ret;
	}
	
	/*private boolean ValidateTalkWithDirectorOrTeacherOrParent()
	{
		if (cActivityReportFacad.pActivityReport.TalkWithDirectorOrTeacherOrParent__c == null && cActivityReportFacad.pActivityReport.ActivityReportStatus__c == CObjectNames.ActivityReportStatusPayment)
		{
			ErrorMessage = CErrorMessages.ActivityReportNeedTalkToPay;
			return false;
		}
		return true;
	}*/
	
	private boolean ValidateTalk()
	{
		boolean ret = true;
		
		if (ret)
			ret = ValidateTalkTalkWithDirectorOrTeacherOrParent();
		if (ret)
			ret = ValidateTalkTimes();
		if (ret)
			ret = ValidateTalkStartTime();
		if (ret)
			ret = ValidateTalkEndTime();
		if (ret)
			ret = ValidateTalkStartTimeNotBigFromTalkEndTime();
		
		return ret;
	}
	
	private boolean ValidateTalkTalkWithDirectorOrTeacherOrParent()
	{
		if (cActivityReportFacad.pActivityReport.TalkWithDirectorOrTeacherOrParent__c == null)
		{
			if (cActivityReportFacad.pActivityReport.TalkStartTime__c != null)
			{
				ErrorMessage = CErrorMessages.ActivityReportNeedTalkWithStartTime;
				return false;
			}
			if (cActivityReportFacad.pActivityReport.TalkEndTime__c != null)
			{
				ErrorMessage = CErrorMessages.ActivityReportNeedTalkWithEndTime;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateTalkTimes()
	{
		if (cActivityReportFacad.pActivityReport.TalkWithDirectorOrTeacherOrParent__c != null)
		{
			if (cActivityReportFacad.pActivityReport.TalkStartTime__c == null)
			{
				ErrorMessage = CErrorMessages.ActivityReportNeedTalkStartTime;
				return false;
			}
			if (cActivityReportFacad.pActivityReport.TalkEndTime__c == null)
			{
				ErrorMessage = CErrorMessages.ActivityReportNeedTalkEndTime;
				return false;
			}
			if (cActivityReportFacad.pActivityReport.TalkType__c == null)
			{
				ErrorMessage = CErrorMessages.ActivityReportNeedTalkType;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateTalkStartTime()
	{
		if (cActivityReportFacad.pActivityReport.TalkStartTime__c != null && !CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pActivityReport.TalkStartTime__c)))
		{
			ErrorMessage = CErrorMessages.ActivityReportTalkStartTimeNotRight;
			return false;
		}
		return true;
	}
	
	private boolean ValidateTalkEndTime()
	{
		if (cActivityReportFacad.pActivityReport.TalkEndTime__c != null && !CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pActivityReport.TalkEndTime__c)))
		{
			ErrorMessage = CErrorMessages.ActivityReportTalkEndTimeNotRight;
			return false;
		}
		return true;
	}
	
	private boolean ValidateTalkStartTimeNotBigFromTalkEndTime()
	{
		if (CValidateTimesAndConverterTimes.GetTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pActivityReport.TalkStartTime__c)) > 
			 CValidateTimesAndConverterTimes.GetTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pActivityReport.TalkEndTime__c)))
		{
			ErrorMessage = CErrorMessages.ActivityReportTalkStartTimeBigFromTalkEndTime;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFromDate()
	{
		if (cActivityReportFacad.pActivityReport.FromDateClass__c == null)
		{
			ErrorMessage = CErrorMessages.ActivityReportFromDateClass;
			return false;
		}
		return true;
	}
	
	private boolean ValidateToDate()
	{
		if (cActivityReportFacad.pActivityReport.ToDateClass__c == null)
		{
			ErrorMessage = CErrorMessages.ActivityReportToDateClass;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFromDateNotBigFromToDate()
	{
		if (cActivityReportFacad.pActivityReport.FromDateClass__c > cActivityReportFacad.pActivityReport.ToDateClass__c)
		{
			ErrorMessage = CErrorMessages.ActivityReportFromDateNotBigFromToDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidatePaymentAmount()
	{
		if (cActivityReportFacad.pActivityReport.PaymentAmountClass__c == null || cActivityReportFacad.pActivityReport.PaymentAmountClass__c == 0)
		{
			ErrorMessage = CErrorMessages.ActivityReportPaymentAmountClass;
			return false;
		}
		return true;
	}
	
	private boolean ValidateReasonForDecline()
	{
		if (cActivityReportFacad.pActivityReport.ReasonForDecline__c == null && cActivityReportFacad.pActivityReport.ActivityReportStatus__c == CObjectNames.ActivityReportStatusPostponed)
		{
			ErrorMessage = CErrorMessages.ActivityReportReasonForDecline;
			return false;
		}
		return true;
	}
	
	private boolean ValidateLines()
	{
		boolean ret = true;
	    
	    if (ret) 
	    	ret = ValidateReasonForDecline();
    	if (ret) 
	    	ret = ValidateIfAllFull();
    	if (ret) 
	    	ret = ValidateTimesInDays();
    	if (ret) 
	    	ret = ValidateStartTimeNotBigFromEndTime();
    	if (ret)
    		ret = ValidateAtLeast10WordsInNote();
		//if (ret)
		//	ret = ValidateTalkWithDirectorOrTeacherOrParent();
		if (ret)
			ret = ValidateLinesInMonthReport();
	  	    
	 	return ret;
	}
	
	private boolean ValidateLinesInMonthReport()
	{
		for (integer i = 0; i < cActivityReportFacad.pDaysInActivityReport.size(); i++)
		{
			if (cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c != null && (cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.month() + '-' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.year()) != cActivityReportFacad.pActivityReport.ReportMonth__c)
			{
				ErrorMessage = CErrorMessages.ActivityReportHolidayParA + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.year() + ' / ' + 
								cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.month() + ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.day()
								+ CErrorMessages.ActivityReportLineNotInMonthReport + cActivityReportFacad.pActivityReport.ReportMonth__c;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateAtLeast10WordsInNote()
	{
		for (integer i = 0; i < cActivityReportFacad.pDaysInActivityReport.size(); i++)
		{
			if (cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c != null)
			{
				if (cActivityReportFacad.pDaysInActivityReport[i].pDay.Note__c == null || cActivityReportFacad.pDaysInActivityReport[i].pDay.Note__c.length() < 11)
				{
					ErrorMessage = CErrorMessages.ActivityReportAtLeast10WordsInAllDays;
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean ValidateTimesInDays()
	{
		for (integer i = 0; i < cActivityReportFacad.pDaysInActivityReport.size(); i++)
		{
			boolean ret = true;
			if (ret && cActivityReportFacad.pDaysInActivityReport[i].pDay.StartTime__c != null)
				ret = CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pDaysInActivityReport[i].pDay.StartTime__c));
			if (ret && cActivityReportFacad.pDaysInActivityReport[i].pDay.EndTime__c != null)
				ret = CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pDaysInActivityReport[i].pDay.EndTime__c));
			if (ret && cActivityReportFacad.pDaysInActivityReport[i].pDay.TotalHoursToPayment__c != null)
				ret = CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pDaysInActivityReport[i].pDay.TotalHoursToPayment__c));
			if (!ret)
			{
				ErrorMessage = CErrorMessages.ActivityReportOneOfThetimesNotWriteParA;
				if (cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c != null)
					ErrorMessage += cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.year() + ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.month() + ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.day();
				else
					ErrorMessage += 'ללא יום';
				ErrorMessage += CErrorMessages.ActivityReportOneOfThetimesNotWriteParB;
				return false;
			}
		}
		
		return true;
	}
	
	private boolean ValidateIfAllFull()
	{
		for (integer i = 0; i < cActivityReportFacad.pDaysInActivityReport.size(); i++)
		{
			boolean TimesFull = cActivityReportFacad.pDaysInActivityReport[i].pDay.EndTime__c != null || cActivityReportFacad.pDaysInActivityReport[i].pDay.StartTime__c != null;
			boolean TimesEmpty = cActivityReportFacad.pDaysInActivityReport[i].pDay.EndTime__c == null || cActivityReportFacad.pDaysInActivityReport[i].pDay.StartTime__c == null;
			if (cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c == null && TimesFull)
			{
				ErrorMessage += CErrorMessages.ActivityReportDateInAllFulls;
				return false;
			}
			else if (TimesEmpty && cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c != null)
			{
				ErrorMessage = CErrorMessages.ActivityReportTimesInAllDaysParA;
				ErrorMessage += cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.year() + ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.month() + ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.day();
				ErrorMessage += CErrorMessages.ActivityReportTimesInAllDaysParB;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateStartTimeNotBigFromEndTime()
	{
		for (integer i = 0; i < cActivityReportFacad.pDaysInActivityReport.size(); i++)
		{
			decimal startTime = CValidateTimesAndConverterTimes.getTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pDaysInActivityReport[i].pDay.StartTime__c));
			decimal endTime = CValidateTimesAndConverterTimes.getTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(cActivityReportFacad.pDaysInActivityReport[i].pDay.EndTime__c));
			if (startTime > endTime)
			{
				ErrorMessage = CErrorMessages.ActivityReportStartTimeBigFromEndTimeParA;
				ErrorMessage += cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.year() + ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.month();
				ErrorMessage += ' / ' + cActivityReportFacad.pDaysInActivityReport[i].pDay.Date__c.day() + CErrorMessages.ActivityReportStartTimeBigFromEndTimeParB;
				return false;
			}
		}
		return true;
	}
	
	public string ValidateIsNotHoliday(integer i, Map<Date, Holiday> allHolidays, Date dayInActivityReportDate)
	{	
		if (allHolidays.containsKey(dayInActivityReportDate))
		{
			string ret = CErrorMessages.ActivityReportHolidayParA + dayInActivityReportDate.year();
			ret +=	' / ' + dayInActivityReportDate.month() + ' / ';
			ret += dayInActivityReportDate.day() + CErrorMessages.ActivityReportHolidayParB;
			ret += allHolidays.get(dayInActivityReportDate).name + CErrorMessages.ActivityReportExclamationMark;
			return ret + ' ';
		}
		return '';
	}//דוגמא להודעת שגיאה: התאריך - 01.02.2012 - הינו חג - חנוכה!
	
	public string ValidateChildDidNotLearnAtTheSameTime(DayInActivityReport__c pDayInActivityReport, map<Date, DayInActivityReport__c> allDaysInActivityReport)
	{
		decimal startTime = CValidateTimesAndConverterTimes.getTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.StartTime__c));
		decimal endTime = CValidateTimesAndConverterTimes.getTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.EndTime__c));
		if (allDaysInActivityReport.containsKey(pDayInActivityReport.Date__c) && allDaysInActivityReport.get(pDayInActivityReport.Date__c) != null)
		{
			DayInActivityReport__c otherDay = allDaysInActivityReport.get(pDayInActivityReport.Date__c);
			if (otherDay.StartTimeToCalculate__c > startTime	&& otherDay.StartTimeToCalculate__c < endTime)
			{
				return getErrorMessageChildWasInOtherPlace(pDayInActivityReport, otherDay);
			}
			else if (otherDay.EndTimeToCalculate__c > startTime && otherDay.EndTimeToCalculate__c < endTime)
			{
				return getErrorMessageChildWasInOtherPlace(pDayInActivityReport, otherDay);
			}
			else if (otherDay.EndTimeToCalculate__c > startTime && otherDay.StartTimeToCalculate__c < endTime)
			{
				return getErrorMessageChildWasInOtherPlace(pDayInActivityReport, otherDay);
			}
		}
		return '';
	}
	
	private string getErrorMessageChildWasInOtherPlace(DayInActivityReport__c pDayInActivityReport, DayInActivityReport__c childOtherDayInActivityReport){
		//string childName = childrenMap.containsKey(childOtherDayInActivityReport.ChildName__c) ? childrenMap.get(childOtherDayInActivityReport.ChildName__c).name : childOtherDayInActivityReport.ChildName__r.name;
		//string tutorName = tutorsMap.containsKey(childOtherDayInActivityReport.TutorName__c) ? tutorsMap.get(childOtherDayInActivityReport.TutorName__c).name : childOtherDayInActivityReport.TutorName__r.name;
		string childName = childOtherDayInActivityReport.ChildName__r.Name;
		string tutorName = childOtherDayInActivityReport.TutorName__r.Name;
		string ret = CErrorMessages.ActivityReportChildWasInOtherPlaceParA + childName;
		ret += CErrorMessages.ActivityReportChildWasInOtherPlaceParB + childOtherDayInActivityReport.Date__c.Year() + ' / ' ;
		ret += childOtherDayInActivityReport.Date__c.month() + ' / ' + childOtherDayInActivityReport.Date__c.day() + CErrorMessages.ActivityReportChildWasInOtherPlaceParC;
		ret += CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.StartTime__c) + '-' + CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.EndTime__c);
		ret += CErrorMessages.ActivityReportChildWasInOtherPlaceParD + tutorName;
		ret += CErrorMessages.ActivityReportChildWasInOtherPlaceParE + childOtherDayInActivityReport.ActivityReportName__r.name;
		return ret + ' ' + CErrorMessages.ActivityReportExclamationMark + ' ';
	}// ! PD-00000000 - 'דוגמא להודעת שגיאה: בן המשפחה - נועם דניאל - למד בתאריך - 20/02/2012 - בשעות - 16:00-17:00 - אצל החונך - לוי ריבקין - כמפורט בדיווח פעילות מס 
	
	public string ValidateTutorDidNotLearnAtTheSameTime(DayInActivityReport__c pDayInActivityReport, Map<Date, DayInActivityReport__c> tutorDaysInActivityReport)
	{
		System.debug('In the function : ValidateTutorDidNotLearnAtTheSameTime');
		decimal startTime = CValidateTimesAndConverterTimes.getTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.StartTime__c));
		System.debug('decimal startTime : ' + startTime);
		decimal endTime = CValidateTimesAndConverterTimes.getTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.EndTime__c));
		System.debug('decimal endTime : ' + endTime);
		if (tutorDaysInActivityReport.containsKey(pDayInActivityReport.Date__c) && tutorDaysInActivityReport.get(pDayInActivityReport.Date__c) != null )
		{
			System.debug('In the condition');
			DayInActivityReport__c otherDay = tutorDaysInActivityReport.get(pDayInActivityReport.Date__c);
			if (otherDay.StartTimeToCalculate__c > startTime && otherDay.StartTimeToCalculate__c < endTime)	{
				return getErrorMessageTutorWasInOtherPlace(pDayInActivityReport, otherDay);
			}
			else if (otherDay.EndTimeToCalculate__c > startTime && otherDay.EndTimeToCalculate__c < endTime){
				return getErrorMessageTutorWasInOtherPlace(pDayInActivityReport, otherDay);
			}
			else if (otherDay.EndTimeToCalculate__c > startTime && otherDay.StartTimeToCalculate__c < endTime)	{
				return getErrorMessageTutorWasInOtherPlace(pDayInActivityReport, otherDay);
			}
		}
		return '';
	}
	
	private string getErrorMessageTutorWasInOtherPlace(DayInActivityReport__c pDayInActivityReport, DayInActivityReport__c tutorOtherDayInActivityReport){
		//string childName = childrenMap.containsKey(tutorOtherDayInActivityReport.ChildName__c) ? childrenMap.get(tutorOtherDayInActivityReport.ChildName__c).name : tutorOtherDayInActivityReport.ChildName__r.name;
		//string tutorName = tutorsMap.containsKey(tutorOtherDayInActivityReport.TutorName__c) ? tutorsMap.get(tutorOtherDayInActivityReport.TutorName__c).name : tutorOtherDayInActivityReport.TutorName__r.name;
		string childName = tutorOtherDayInActivityReport.ChildName__r.Name;
		string tutorName = tutorOtherDayInActivityReport.TutorName__r.Name;
		string ret = CErrorMessages.ActivityReportTutorWasInOtherPlaceParA + tutorName;
		ret += CErrorMessages.ActivityReportTutorWasInOtherPlaceParB + tutorOtherDayInActivityReport.Date__c.Year() + ' / ' ;
		ret += tutorOtherDayInActivityReport.Date__c.month() + ' / ' + tutorOtherDayInActivityReport.Date__c.day() + CErrorMessages.ActivityReportTutorWasInOtherPlaceParC;
		ret += CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.StartTime__c) + '-' + CValidateTimesAndConverterTimes.FillFields(pDayInActivityReport.EndTime__c);
		ret += CErrorMessages.ActivityReportTutorWasInOtherPlaceParD + childName;
		ret += CErrorMessages.ActivityReportTutorWasInOtherPlaceParE + tutorOtherDayInActivityReport.ActivityReportName__r.name;
		return ret + ' ' + CErrorMessages.ActivityReportExclamationMark + ' ';
	}// ! PD-000000000 - 'דוגמא להודעת שגיאה: החונך - לוי ריבקין - לימד בתאריך - 21/02/2012 - בשעות - 16:00-17:00 - את בן המשפחה - נועם דניאל - כמפורט בדיווח פעילות מס
	
	public string ValidateIsPassedMinutesInSequence(decimal totalHoursToCalculate, decimal hoursInSequence)
	{
		if (hoursInSequence == 0)
			return '';
		if (hoursInSequence != null && totalHoursToCalculate > hoursInSequence)
		{
			return CErrorMessages.ActivityReportPassedMinutesInSequence + ' ';
		}
		return '';
	}//!דוגמא להודעת שגיאה: סה"כ השעות עברו את סך הדקות ברצף שהוקצבו לאישור פעילות זה
	
	public string ValidateDuplicateTimes(map<date, list<DayInActivityReport__c>> daysByDate, DayInActivityReport__c day)
	{
		if (daysByDate.containsKey(day.Date__c) && daysByDate.get(day.Date__c).size() > 1)
		{
			integer num = 0;
			list<DayInActivityReport__c> daysList = daysByDate.get(day.Date__c);
			for (DayInActivityReport__c dayInList : daysList)
			{
				if (day.StartTimeToCalculate__c >= dayInList.StartTimeToCalculate__c
					&& day.StartTimeToCalculate__c < dayInList.EndTimeToCalculate__c)
				{
					num++;
				}
				else if (day.EndTimeToCalculate__c > dayInList.StartTimeToCalculate__c
						&& day.EndTimeToCalculate__c <= dayInList.EndTimeToCalculate__c)
				{
					num++;
				}
			}
			if (num > 1)
			{
				return 'ישנם ערכים כפולים לתאריך - ' + day.Date__c.day() + '/' + day.Date__c.month() + '/' + day.Date__c.year() + 
					   ' - בשעות - ' + day.StartTime__c + '-' + day.EndTime__c + '!';
			}
			else
				return '';
		}
		else
			return '';
	}//!דוגמא להודעת שגיאה: ישנם ערכים כפולים לתאריך - 10/1/2012 - בשעות -17:00-18:00
	
	
	
	
	public string ValidateOneHourPerDay(map<date, list<DayInActivityReport__c>> daysByDate, DayInActivityReport__c day, decimal minutesPerDay)
	{
		if (minutesPerDay != null && daysByDate.containsKey(day.Date__c))
		{
			decimal total = 0;
			
			list<DayInActivityReport__c> daysList = daysByDate.get(day.Date__c);
			for (DayInActivityReport__c dayInList : daysList)
			{
				total += dayInList.TotalHoursToCalculate__c == null ? 0 : dayInList.TotalHoursToCalculate__c;
			}
		
			//System.Debug('a1:'+total+' '+(minutesPerDay/60.0));
			if (total > (minutesPerDay/60.0))
			{
				return 'סך הדקות לתאריך - ' + day.Date__c.day() + '/' + day.Date__c.month() + '/' + day.Date__c.year() + ' חורג מסך הדקות ליום !';
			}
			else
				return '';
		}
		else
			return '';
	}//!דוגמא להודעת שגיאה: סך הדקות  לתאריך - 10/1/2012 חורג מסך הדקות ליום
	
	
	public string ValidateIsPassed60DaysFromProductionDay(datetime dayDate, datetime createdDate)
	{
		dateTime createdDateLess60Days = createdDate.addDays(-60);
		if (dayDate != null && createdDateLess60Days.date() > dayDate.date())
		{
			string ret = CErrorMessages.ActivityReportPassed60DaysFromProductionDayParA + dayDate.date().day() + '/';
			ret += dayDate.date().month() + '/' + dayDate.date().year() + CErrorMessages.ActivityReportPassed60DaysFromProductionDayParB;
			return ret + createdDate.date().day() + '/' + createdDate.date().month() + '/' + createdDate.date().year() + '! ';
		}
		return '';
	}//!דוגמא להודעת שגיאה: התאריך - 20/06/2012 - קטן ב-60 יום מתאריך יצירת הדיווח - 21/08/2012
	
	public string ValidateDaysInActivityReportInWebSite(List<CDayInActivityReportToSite> daysInActivityReport, string month)
	{
		boolean isAtLeastOneFull = false; 
		System.debug('COLEL CHABAD 2 + daysInActivityReport.size() + ' + daysInActivityReport.size());
		for (integer i = 0; i < daysInActivityReport.size(); i++)
		{
			System.debug('COLEL CHABAD 3');
			if (!isAtLeastOneFull){
				System.debug('COLEL CHABAD 4 + daysInActivityReport[i] + ' + daysInActivityReport[i]);
				isAtLeastOneFull = ValidateDayInActivityReportAtLeastOneInWebSite(daysInActivityReport[i]);
				// 26/06/22 update
				
				if(isAtLeastOneFull == false){
					return CObjectNames.WebSiteReportEnterDate;
				} 
			}
			if (!ValidateDateInThisMonth(daysInActivityReport[i], month))
			{
				return CObjectNames.WebSiteReportNotInThisMonthPar1 + daysInActivityReport[i].pDayInActivityReport.Date__c.day() + '/' +
					   daysInActivityReport[i].pDayInActivityReport.Date__c.month() + '/' + daysInActivityReport[i].pDayInActivityReport.Date__c.year()
					    + CObjectNames.WebSiteReportNotInThisMonthPar2 + month + '!';
			}
			if (!ValidateTimes(daysInActivityReport[i]))
			{
				string dateDay = daysInActivityReport[i].pDayInActivityReport.Date__c != null ? string.valueOf(daysInActivityReport[i].pDayInActivityReport.Date__c.day()) : '';
				string dateMonth = daysInActivityReport[i].pDayInActivityReport.Date__c != null ? string.valueOf(daysInActivityReport[i].pDayInActivityReport.Date__c.month()) : '';
				string dateYear = daysInActivityReport[i].pDayInActivityReport.Date__c != null ? string.valueOf(daysInActivityReport[i].pDayInActivityReport.Date__c.year()) : '';
				System.debug('dateDay - ' + dateDay);
				System.debug('dateMonth - ' + dateMonth);
				System.debug('dateYear - ' + dateYear);
				return CObjectNames.WebSiteReportStartTimeBigFromEndTimePar1 + dateDay + '/' + dateMonth + '/' + dateYear + CObjectNames.WebSiteReportStartTimeBigFromEndTimePar2;
			}
			if (!ValidateAtLeast10WordsInNote(daysInActivityReport[i])){
				return ErrorMessage = CErrorMessages.ActivityReportAtLeast10WordsInAllDays;
			}
		}	
		if (!isAtLeastOneFull){
			System.debug('COLEL CHABAD 1');
			return CObjectNames.WebSiteReportAtLeastOneDay;
		}
		return null;
	}
	
	public boolean ValidateDayInActivityReportAtLeastOneInWebSite(CDayInActivityReportToSite daysInActivityReport)
	{
		System.debug('COLEL CHABAD 5 + daysInActivityReport + ' + daysInActivityReport);
		System.debug('COLEL CHABAD 6 + daysInActivityReport.pDayInActivityReport.Date__c + ' + daysInActivityReport.pDayInActivityReport.Date__c);
		if (daysInActivityReport.pDayInActivityReport.Date__c != null){
			System.debug('COLEL CHABAD 7');
			return true;
		}
		return false;
	}
	
	private boolean ValidateDateInThisMonth(CDayInActivityReportToSite daysInActivityReport, string monthAndYear)
	{
		integer month = integer.valueOf(monthAndYear.split('-')[0]);
		integer year = integer.valueOf(monthAndYear.split('-')[1]);
		if (daysInActivityReport.pDayInActivityReport.Date__c != null && (daysInActivityReport.pDayInActivityReport.Date__c.month() != month || daysInActivityReport.pDayInActivityReport.Date__c.year() != year))
			return false;
		return true;
	}
	
	private boolean ValidateTimes(CDayInActivityReportToSite daysInActivityReport)
	{
		if (daysInActivityReport.pDayInActivityReport.Date__c != null)
		{
			decimal startTime = CValidateTimesAndConverterTimes.GetTimeToCalculate(daysInActivityReport.startTimeHours + ':' + daysInActivityReport.startTimeMinutes);
			decimal endTime = CValidateTimesAndConverterTimes.GetTimeToCalculate(daysInActivityReport.endTimeHours + ':' + daysInActivityReport.endTimeMinutes);
			if (startTime >= endTime)
				return false;
		}
		return true;
	}
	
	private boolean ValidateAtLeast10WordsInNote(CDayInActivityReportToSite daysInActivityReport)
	{
		if (daysInActivityReport.pDayInActivityReport.Date__c != null)
		{
			if (daysInActivityReport.pDayInActivityReport.Note__c == null || daysInActivityReport.pDayInActivityReport.Note__c.length() < 11)
				return false;
		}
		return true;
	}
	
	// **************************** for teses ***************************************
	
	public void testAll()
	{
		cActivityReportFacad.pActivityReport.ReasonForDecline__c = null;
		cActivityReportFacad.pActivityReport.ActivityReportStatus__c = CObjectNames.ActivityReportStatusPostponed;
		ValidateReport();
		cActivityReportFacad.pActivityReport.ReasonForDecline__c = 'levi';
		cActivityReportFacad.pActivityReport.ActivityReportStatus__c = CObjectNames.ActivityReportStatusPayment;
		ValidateReport();
		cActivityReportFacad.pActivityReport.FromDateClass__c = date.newInstance(2012, 3, 3);
		ValidateReport();
		cActivityReportFacad.pActivityReport.ToDateClass__c = date.newInstance(2012, 3, 2);
		ValidateReport();
		cActivityReportFacad.pActivityReport.FromDateClass__c = date.newInstance(2012, 3, 1);
		ValidateReport();
		cActivityReportFacad.pActivityReport.PaymentAmountClass__c = 1;
		ValidateReport();
		list<CDayInActivityReportToSite> dayInActivityReportToSite = new list<CDayInActivityReportToSite>();
		ActivityApproval__c approval = new ActivityApproval__c();
		approval.Fare__c = 20;
		dayInActivityReportToSite.add(new CDayInActivityReportToSite(new DayInActivityReport__c(), approval));
		dayInActivityReportToSite[0].pDayInActivityReport.Date__c = date.newInstance(2012, 2, 3);
		dayInActivityReportToSite[0].endTimeHours = '10';
		dayInActivityReportToSite[0].startTimeHours = '09';
		dayInActivityReportToSite[0].endTimeMinutes = '10';
		dayInActivityReportToSite[0].startTimeMinutes = '10';
		ValidateDaysInActivityReportInWebSite(dayInActivityReportToSite, '2-2012');
		DayInActivityReport__c[] daysInActivityReport = [select name, id, StartTime__c, EndTime__c, TutorName__r.name, Date__c, ChildName__r.name, ActivityReportName__r.name, ActivityPlace__c
															from DayInActivityReport__c limit 40000];
		//map<string, Children__c> childrenMap = CChildDb.GetChildrenMapWithOutPhoto();
		map<string, Tutor__c> tutorsMap = CTutorDb.GetTutorsMap();
		getErrorMessageTutorWasInOtherPlace(daysInActivityReport[0], daysInActivityReport[1]);
		getErrorMessageChildWasInOtherPlace(daysInActivityReport[0], daysInActivityReport[1]);
		ValidateIsPassed60DaysFromProductionDay(datetime.now(), datetime.now().addYears(-1));
		cActivityReportFacad.pActivityReport.TalkStartTime__c = '1000';
		ValidateTalk();
		cActivityReportFacad.pActivityReport.TalkStartTime__c = null;
		cActivityReportFacad.pActivityReport.TalkEndTime__c = '1000';
		ValidateTalk();
		cActivityReportFacad.pActivityReport.TalkWithDirectorOrTeacherOrParent__c = 'sdrgs';
		ValidateTalk();
		cActivityReportFacad.pActivityReport.TalkStartTime__c = '1000';
		cActivityReportFacad.pActivityReport.TalkEndTime__c = null;
		ValidateTalk();
		cActivityReportFacad.pActivityReport.TalkEndTime__c = '1000';
		cActivityReportFacad.pActivityReport.TalkType__c = null;
		ValidateTalk();
		cActivityReportFacad.pActivityReport.TalkType__c = '1000';
		ValidateTalk();
	}
}