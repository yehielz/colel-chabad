public with sharing class CPaymentToPrint
{
    public Payment__c pPayment { get; set; }
    public list<CReportInPaymentToPrint> pActivityReports { get; set; }
    public boolean IsHasReports{ get{ return pActivityReports != null && pActivityReports.size() > 0; } }
    public list<CSpecialActivityInPaymentToPrint> pSpecialActivities { get; set; }
    public boolean IsHasSpecialActivities{ get{ return pSpecialActivities != null && pSpecialActivities.size() > 0; } }
    
    public CPaymentToPrint(Payment__c mPayment)
    {
        pPayment = mPayment;
        SetReports(mPayment.ActivityReportList__r);
        SetSpecialActivities(mPayment.SpecialActivitiesPayment__r);
    }
    
    public CPaymentToPrint(Payment__c mPayment, list<ActivityReport__c> mReports, list<SpecialActivitiesLine__c> mSpecialActivities)
    {
        pPayment = mPayment;
        SetReports(mReports);
        SetSpecialActivities(mSpecialActivities);
    }
    
    public CPaymentToPrint(Payment__c mPayment, list<CReportInPaymentToPrint> mActivityReports, list<CSpecialActivityInPaymentToPrint> mSpecialActivities)
    {
        pPayment = mPayment;
        pActivityReports = mActivityReports;
        pSpecialActivities = mSpecialActivities;
    }
    
    private void SetReports(list<ActivityReport__c> reports)
    {
        pActivityReports = new list<CReportInPaymentToPrint>();
        if (reports != null)
        {
            for (ActivityReport__c report : reports)
            {
                pActivityReports.add(new CReportInPaymentToPrint(report));
            }
        }
    }
    
    private void SetSpecialActivities(list<SpecialActivitiesLine__c> specialActivities)
    {
        pSpecialActivities = new list<CSpecialActivityInPaymentToPrint>();
        if (specialActivities != null)
        {
            for (SpecialActivitiesLine__c specialActivity : specialActivities)
            {
                pSpecialActivities.add(new CSpecialActivityInPaymentToPrint(specialActivity));
            }
        }
    }
    
    public string PaymentName
    {
        get
        {
            if (pPayment.name != null)
                return CObjectNames.getHebrewString(pPayment.name);
            return '';
        }
    }
    
    public string PersonName
    {
        get
        {
            string ret = '';
           if (pPayment.FamilyName__c != null)
            {
                if (pPayment.FamilyName__r.ParentName__c != null)
                    ret += CObjectNames.GetHebrewString(' ' + pPayment.FamilyName__r.ParentName__c + ' ');
                if (pPayment.FamilyName__r.name != null)
                    ret += CObjectNames.GetHebrewString(' ' + pPayment.FamilyName__r.name + ' ');
            }
            return ret.trim();
        }
    }
    
    public string Address
    {
        get
        {
            if (pPayment.PaymentTarget__c == 'חונך' && pPayment.TutorName__c != null)
            {
                string ret = '';
                if (pPayment.TutorName__r.Street__c != null)
                    ret += CObjectNames.getHebrewString(' '  + pPayment.TutorName__r.Street__c + ' ');
                if (pPayment.TutorName__r.ApartmentNumber__c != null)
                    ret += CObjectNames.getHebrewString(' '  + pPayment.TutorName__r.ApartmentNumber__c + ' ');
                return ret.trim();
            }
            if (pPayment.FamilyName__c != null && pPayment.FamilyName__r.Address__c != null)
                return CObjectNames.getHebrewString(pPayment.FamilyName__r.Address__c);
            return '';
        }
    }
    
    public string City
    {
        get
        {
            if (pPayment.PaymentTarget__c == 'חונך' && pPayment.TutorName__c != null && pPayment.TutorName__r.City__c != null)
                return CObjectNames.getHebrewString(pPayment.TutorName__r.City__c);
            if (pPayment.FamilyName__c != null && pPayment.FamilyName__r.City__c != null)
                return CObjectNames.getHebrewString(pPayment.FamilyName__r.City__c);
            return '';
        }
    }
    
    public string HomePhone
    {
        get
        {
            if (pPayment.PaymentTarget__c == 'חונך' && pPayment.TutorName__c != null && pPayment.TutorName__r.Phone__c != null)
                return CObjectNames.getHebrewString(pPayment.TutorName__r.Phone__c);
            else if (pPayment.FamilyName__c != null && pPayment.FamilyName__r.HomePhone__c != null)
                return CObjectNames.getHebrewString(pPayment.FamilyName__r.HomePhone__c);
            return '';
        }
    }
    
    public string AffiliateNumber
    {
        get
        {
            if (pPayment.PaymentTarget__c == 'חונך' && pPayment.TutorName__c != null && pPayment.TutorName__r.AffiliateNumber__c != null)
                return string.valueOf(pPayment.TutorName__r.AffiliateNumber__c);
            if (pPayment.FamilyName__c != null && pPayment.FamilyName__r.AffiliateNumber__c != null)
                return string.valueOf(pPayment.FamilyName__r.AffiliateNumber__c);
            return '';
        }
    }
    
    public string AccountNumber
    {
        get
        {
            if (pPayment.PaymentTarget__c == 'חונך' && pPayment.TutorName__c != null && pPayment.TutorName__r.AccountNumber__c != null)
                return string.valueOf(pPayment.TutorName__r.AccountNumber__c);
            if (pPayment.FamilyName__c != null && pPayment.FamilyName__r.AccountNumber__c != null)
                return string.valueOf(pPayment.FamilyName__r.AccountNumber__c);
            return '';
        }
    }
    
    public boolean IsHasComment{ get{ return pPayment.Comment__c != null; } }
    public string Comment
    {
        get
        {
            if (pPayment.Comment__c != null)
            {
                string ret = '';
                for (string row : pPayment.Comment__c.split('\n'))
                {
                    ret += ret == '' ? GetNote(row) : '<br/>' + GetNote(row);
                }
                return ret ;
            }
            return '';
        }
    }
    
    private string GetNote(string thenote)
    {
        string ret = '';
        if (thenote != null)
        {
            string note = ' ' + thenote;
            for (integer i = 80; i < note.length(); i=80)
            {
                integer fromInSubS = 80;
                integer minus = 0;
                while(note.substring(i-minus, ((i-minus)+1)) != ' ')
                    minus++;
                i = i - minus;
                ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i)) + '<br/>';
                note = note.substring(i, note.length());
            }
            ret += CObjectNames.getHebrewString(note);
        }
        return ret;
    }
}