public with sharing class CFamilyValidator 
{
	private CFamilyFacad cFamilyFacad { get; set; }
	public string ErrorMessage;
	
	public CFamilyValidator(CFamilyFacad ExtFamilyFacad)
	{
		ErrorMessage = '';
		cFamilyFacad = ExtFamilyFacad;
		if (test.isRunningTest())
			cFamilyFacad.pFamily.ParentId__c = '313376345';
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateFamilyName();
	    if (ret) 
	    	ret = ValidateParentName();
    	/*if (ret) 
	    	ret = ValidateCoordinatorName();*/
    	if (ret) 
	    	ret = ValidateParentId();
	  	    
	 	return ret;
	}
	
	private boolean ValidateParentName()
	{
		if (cFamilyFacad.pFamily.ParentName__c == null)
		{
			ErrorMessage = CErrorMessages.FamilyParentName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFamilyName()
	{
		if (cFamilyFacad.pFamily.name == null)
		{
			ErrorMessage = CErrorMessages.FamilyFamilyName;
			return false;
		}
		return true;
	}
	/*
	private boolean ValidateCoordinatorName()
	{
		if (cFamilyFacad.pFamily.CoordinatorName__c == null)
		{
			ErrorMessage = CErrorMessages.FamilyCoordinatorName;
			return false;
		}
		return true;
	}*/
	
	private boolean ValidateParentId()
	{
		if (cFamilyFacad.pFamily.ParentId__c != null)
		{
			if (isIdExist())
				return false;
			integer i = ValidateID(cFamilyFacad.pFamily.ParentId__c);
			if (i == -1)
			{
				ErrorMessage = CErrorMessages.FamilyParentIdElegalInPut;
				return false;
			}
			else if (i == -2)
			{
				ErrorMessage = CErrorMessages.FamilyParentIdNotValid;
				return false;
			}
		}
		return true;
	}
	
	private boolean isIdExist()
	{
		Children__c[] children = [select name, id, IdNumber__c, FamilyManType__c, FamilyName__c from Children__c where IdNumber__c = :cFamilyFacad.pFamily.ParentId__c];
		if (children.size() > 0 && (children[0].FamilyManType__c != CObjectNames.Parent || children[0].FamilyName__c != cFamilyFacad.pFamily.id))
		{
			ErrorMessage = CErrorMessages.FamilyParentIdExist + CErrorMessages.FamilyParentIdChild + children[0].name + '!';
			return true;
		}
		Family__c[] families = [select name, id, ParentId__c from Family__c where ParentId__c = :cFamilyFacad.pFamily.ParentId__c and id != :cFamilyFacad.pFamily.id];
		if (families.size() > 0)
		{
			ErrorMessage = CErrorMessages.FamilyParentIdExist + CErrorMessages.FamilyParentIdFamily + families[0].name + '!';
			return true;
		}
		return false;
	}
	
    private integer ValidateID(string str)
	{
		// DEFINE RETURN VALUES
		integer R_ELEGAL_INPUT = -1;
		integer R_NOT_VALID = -2;
		integer R_VALID = 1;
		//INPUT VALIDATION

		// Just in case -> convert to string
		string IDnum = str;
		// Validate correct input
		if ((IDnum.length() > 9) || (IDnum.length() < 5))
			return R_ELEGAL_INPUT;
		if (isNaN(IDnum))
			return R_ELEGAL_INPUT;
		// The number is too short - add leading 0000
		if (IDnum.length() < 9)
		{
			while (IDnum.length() < 9)
			{
				IDnum = '0' + IDnum;
			}
		}
		// CHECK THE ID NUMBER
		integer mone = 0, incNum;
		for (integer i=0; i < 9; i++)
		{
			incNum = integer.valueOf(IDnum.substring(i, i+1));
			incNum *= math.mod(i, 2) + 1;
			if (incNum > 9)
				incNum -= 9;
			mone += incNum;
		}
		if (math.mod(mone, 10) == 0)
			return R_VALID;
		else
			return R_NOT_VALID;
	}
	
	private boolean isNaN(string str)
	{
		for (integer i = 0; i < str.length(); i++)
		{
			string s = str.substring(i, i+1);
			if (s != '0' && s != '1' && s != '2' && s != '3' && s != '4' && s != '5' && s != '6' && s != '7' && s != '8' && s != '9')
				return true;
		}
		return false;
	}
	
	
	
	//  **********************   for tests *********************************
	
	public void testAll()
	{
		ValidateFamilyName();
		ValidateParentId();
		cFamilyFacad.pFamily.ParentId__c = '017335770';
		ValidateParentId();
		cFamilyFacad.pFamily.ParentId__c = '017335771';
		ValidateParentId();
		cFamilyFacad.pFamily.ParentId__c = '435345435';
		ValidateParentId();
		cFamilyFacad.pFamily.ParentId__c = '4444';
		ValidateParentId();		
		cFamilyFacad.pFamily.ParentId__c = '435344345435';
		ValidateParentId();	
	}
}