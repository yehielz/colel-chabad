public with sharing class CUserDb
{
	public List<SelectOption> GetOptionUsers()
    {
    	List<SelectOption> optionsUsers = new List<SelectOption>(); 
    	User [] usersList = GetAllUsers();
    	
    	for (User u : usersList)
    	{
    		optionsUsers.add(new SelectOption(u.id, u.name));
    	}
    	return optionsUsers;
    }
    
    public List<User> GetAllUsers()
    {
    	List<User> usersList = [select name from User];
    	return usersList;
    }
    
    public static list<User> GetAllUsersList()
    {
    	return [select id, name from User];
    }
    
    public static map<string, User> GetAllUsersMap()
    {
    	map<string, User> ret = new map<string, User>();
    	List<User> usersList = GetAllUsersList();
    	for (User u : usersList)
    	{
    		ret.put(u.id, u);
    	}
    	return ret;
    }
}