public with sharing class CFilterFamilys 
{
	public List<selectOption> GetOptionsOperators()
	{
		List<selectOption> ret = new List<selectOption>();
		ret.add(new selectOption('null', '--ללא--'));
		ret.add(new selectOption('=', 'שווה ל-'));
		ret.add(new selectOption('!=', 'לא שווה ל-'));
		
		ret.add(new selectOption('like %', 'מתחיל ב-'));
		ret.add(new selectOption('like %%', 'מכיל'));
		ret.add(new selectOption('not like', 'לא מכיל'));
		
		ret.add(new selectOption('<', 'פחות מ-'));
		ret.add(new selectOption('>', 'יותר מ-'));
		ret.add(new selectOption('<=', 'פחות או שווה ל-'));
		ret.add(new selectOption('>=', 'גדול או שווה ל-'));
		return ret;
	}
	
	public List<selectOption> GetOptionsFields()
	{
		List<selectOption> ret = new List<selectOption>();
		ret.add(new selectOption('null', '--ללא--'));
		
		ret.add(new selectOption('ParentAge', 'גיל ההורה שחי'));
		ret.add(new selectOption('ParentDeathDate', 'תאריך פטירה של ההורה שמת'));
		
		ret.add(new selectOption('ChildrenNumberInFamily__c', 'מספר בני משפחה במשפחה'));
		ret.add(new selectOption('ChildrenNumberActive__c', 'מספר בני משפחה בפעילות'));
		//ret.add(new selectOption('ChildrenAge', 'גילאי בני משפחה'));
		ret.add(new selectOption('City__c', 'עיר'));
		ret.add(new selectOption('Region__c', 'אזורי פעילות'));
		return ret;
	}
	
	public List<Family__c> GetFamiliesByFilter(SpecialActivities__c specialActivities)
	{
		string query = getQuery(specialActivities);
		string fullQuery = 'select name, id, City__c, CoordinatorName__c, CoordinatorName__r.name, Owner.Name, OwnerId from Family__c' + query;
		Family__c[] familys = Database.query(fullQuery);
		return familys;
	}
	
	private string getQuery(SpecialActivities__c specialActivities)
	{
		string ret = ' where ';
		ret += setQueryForFields(specialActivities.Operator1ToFilter__c, specialActivities.Field1ToFilter__c, specialActivities.Value1ToFilter__c);
		ret += setQueryForFields(specialActivities.Operator2ToFilter__c, specialActivities.Field2ToFilter__c, specialActivities.Value2ToFilter__c);
		ret += setQueryForFields(specialActivities.Operator3ToFilter__c, specialActivities.Field3ToFilter__c, specialActivities.Value3ToFilter__c);
		ret += setQueryForFields(specialActivities.Operator4ToFilter__c, specialActivities.Field4ToFilter__c, specialActivities.Value4ToFilter__c);
		ret += setQueryForFields(specialActivities.Operator5ToFilter__c, specialActivities.Field5ToFilter__c, specialActivities.Value5ToFilter__c);
		ret += setQueryForFields(specialActivities.Operator6ToFilter__c, specialActivities.Field6ToFilter__c, specialActivities.Value6ToFilter__c);
		ret += setQueryForFields(specialActivities.Operator7ToFilter__c, specialActivities.Field7ToFilter__c, specialActivities.Value7ToFilter__c);
		
		if (ret.length() != 7)
		{
			ret = ClearAndFromString(ret);
			return ret;
		}
		return '';
	}
	
	private string ClearAndFromString(string clearFrom)
	{
		if (clearFrom.endsWith(' and '))
		{
			return clearFrom.substring(0, clearFrom.length()-5);
		}
		return clearFrom;
	}
	
	private string setQueryForFields(string operator, string field, string value)
	{
		if (operator != 'null' && field != 'null')
		{
			value = CheckValue(value, field);
			string ret = getStringToOne(operator, field, value);
			if (ret != '')
				ret += ' and ';
			return ret;
		}
		return '';
	}
	
	//==========================================================================================================
	
	private string getStringToOne(string operator, string field, string value)
	{
		if (value.startsWith('\''))
		{
			return MessWithString(operator, field, value);
		}
		else 
		{
			return MessWithIntOrDate(operator, field, value);
		}
	}
	
	private string MessWithString(string operator, string field, string value)
	{
		if (field != 'City__c' && field != 'Region__c')
			return '';
		if (operator != '=' && operator != '!=' && operator != 'like %' && operator != 'like %%' && operator != 'not like')
			return '';
		if (operator == 'like %')
			return field + ' like ' + getValueToStartWith(value);
		if (operator == 'like %%')
			return field + ' like ' + getValueToContainsAndNotContains(value);
		if (operator == 'not like')
			return 'not ' + field + ' like ' + getValueToContainsAndNotContains(value);
		return field + ' ' + operator + ' ' + value;
	}
	
	private string getValueToContainsAndNotContains(string value)
	{
		string ret = '\'%';
		for (integer i = 0; i < value.length(); i++)
		{
			string s = value.substring(i, i+1);
			if (s != '\'')
				ret += s;			
		}
		ret += '%\'';
		return ret;
	}
	
	private string getValueToStartWith(string value)
	{
		string ret = '\'';
		for (integer i = 0; i < value.length(); i++)
		{
			string s = value.substring(i, i+1);
			if (s != '\'')
				ret += s;			
		}
		ret += '%\'';
		return ret;
	}
	
	private string MessWithIntOrDate(string operator, string field, string value)
	{
		if (field == 'City__c' || field == 'Region__c')
			return '';
		if (operator != '=' && operator != '!=' && operator != '<' && operator != '>' && operator != '<=' && operator != '>=')
			return '';
		if (field == 'ParentAge')
			return MessWithAgeDate(operator, field, value);
		if (field == 'ParentDeathDate')
			return MessWithDate(operator, field, value);
		return field + ' ' + operator + ' ' + value;
	}
	
	private string MessWithDate(string operator, string field, string value)
	{
		datetime ValueDate = getDateFromDateString(value);
		//string myOperator = getWriteOperatorToAge(operator);
		if (ValueDate == null)
			return '';
		return '(DeathDadDateTime__c ' + operator + ' ' + getIntFromLongTime(ValueDate.getTime()) + ' or DeathMotherDateTime__c ' + operator + ' ' + getIntFromLongTime(ValueDate.getTime()) + ')';
	}
	
	private datetime getDateFromDateString(string value)
	{
		string[] myDate = value.split('/', 3);
		if (myDate.size() == 3)
		{
			datetime ret = date.newinstance(integer.valueOf(myDate[2]), integer.valueOf(myDate[1]), integer.valueOf(myDate[0]));
			datetime mmm = date.newinstance(2012, 01, 10);
			return ret;
		}
		return null;
	}
	
	private string MessWithAgeDate(string operator, string field, string value)
	{
		datetime ToDayBeforeValue = getToDayBeforeValue(value);
		datetime ToDayBeforeValueAndOne = date.newinstance((ToDayBeforeValue.year() - 1), ToDayBeforeValue.month(), ToDayBeforeValue.day());
		string myOperator = getWriteOperatorToAge(operator);
		if (myOperator == '=')
			return 'ParentBirthdateTime__c <= ' + getIntFromLongTime(ToDayBeforeValue.getTime()) + ' and ParentBirthdateTime__c > ' + getIntFromLongTime(ToDayBeforeValueAndOne.getTime());
		else if (myOperator == '!=')
			return '(ParentBirthdateTime__c > ' + getIntFromLongTime(ToDayBeforeValue.getTime()) + ' or ParentBirthdateTime__c <= ' + getIntFromLongTime(ToDayBeforeValueAndOne.getTime()) + ')';
		else if (myOperator == '>')
			return 'ParentBirthdateTime__c > ' + getIntFromLongTime(ToDayBeforeValue.getTime());
		else if (myOperator == '<')
			return 'ParentBirthdateTime__c < ' + getIntFromLongTime(ToDayBeforeValueAndOne.getTime());
		else if (myOperator == '<=')
			return 'ParentBirthdateTime__c <= ' + getIntFromLongTime(ToDayBeforeValue.getTime());
		return 'ParentBirthdateTime__c >= ' + getIntFromLongTime(ToDayBeforeValueAndOne.getTime());
	}
	
	private datetime getToDayBeforeValue(string value)
	{
		integer year = system.now().date().year() - integer.valueOf(value);
		datetime ret = date.newinstance(year, system.now().date().month(), system.now().date().day());
		return ret;
	}
	
	private string getWriteOperatorToAge(string operator)
	{
		if (operator == '>')
			return '<';
		if (operator == '<')
			return '>';
		if (operator == '<=')
			return '>=';
		if (operator == '>=')
			return '<=';
		return operator;
	}
	
	private string CheckValue(string value, string field)
	{
		if (value == null)
			return '';
		if (field == 'City__c' || field == 'Region__c')
			return '\'' + value + '\'';
		boolean isString = false;
		for (integer i = 0; i < value.length(); i++)
		{
			string s = value.substring(i, i+1);
			if (s != '0' && s != '1' && s != '2'  && s != '3'  && s != '4'  && s != '5'  && s != '6'  && s != '7'  && s != '8'  && s != '9' && s != '/')
				isString = true;
		}
		if (isString)
			return '\'' + value + '\'';
		return value;
	}
	
	private integer getIntFromLongTime(long fullTime)
	{
		string str = string.valueOf(fullTime);
		if (str.length() < 6)
			return integer.valueOf(fullTime);
		string ret = '';
		for (integer i = 0; i < (str.length() - 5); i++)
		{
			ret += str.substring(i, i + 1);	
		}
		return integer.valueOf(ret);
	} 
}