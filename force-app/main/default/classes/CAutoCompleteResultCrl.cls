public with sharing class CAutoCompleteResultCrl 
{
	public string result { get; set; }
	
	public CAutoCompleteResultCrl()
	{
		
	}
	
	public void SetResult()
	{
		result = '';
		if (Apexpages.currentPage().getParameters().get('term') != null && Apexpages.currentPage().getParameters().get('term') != '')
		{
			string query = 'select name, id ';
			string[] tfields;
			if (Apexpages.currentPage().getParameters().get('fields') != null && Apexpages.currentPage().getParameters().get('fields') != '')
			{
				query = 'select id, name';
				tfields = Apexpages.currentPage().getParameters().get('fields').split('-');
				for (string s : tfields)
				{
					if (s != 'name')
						query += ', ' + s;
				}
			}
			string objectFrom = 'Family__c';
			if (Apexpages.currentPage().getParameters().get('objectFrom') != null && Apexpages.currentPage().getParameters().get('objectFrom') != '')
				objectFrom = Apexpages.currentPage().getParameters().get('objectFrom');
			query += ' from ' + objectFrom;
			string term = Apexpages.currentPage().getParameters().get('term');
			system.debug('query = ' + query);
			list<sObject> startWith = Database.query(query + ' where name like ' + GetValueToStartWith(term));
			list<sObject> contains = Database.query(query + ' where name like ' + GetValueToContains(term) + ' and (not name like ' + GetValueToStartWith(term) + ')');
			startWith.addAll(contains);
			result = GetResultStringFromList(startWith, tfields);
		}
		
	}

	private class AutoCompleteItem
	{
		public string label { get; set; }
		public string value { get; set; }
	}

	private string GetResultStringFromList(list<sObject> theList, string[] tfields)
	{
		list<AutoCompleteItem> a = new list<AutoCompleteItem>();
		for (sObject so : theList)
		{
			AutoCompleteItem item = new AutoCompleteItem();
			item.value = string.valueOf(so.get('name'));
			item.label = string.valueOf(so.get('name'));
			if (tfields != null)
			{
				for (string field : tfields)
				{
					string fieldValue = string.valueOf(so.get(field));
					fieldValue = fieldValue == null ? '' : fieldValue; 
					item.label += ', ' + fieldValue;
				}
			}
			a.add(item);
		}
		return Json.serialize(a);
	}
	
	private string GetValueToStartWith(string value)
	{
		string ret = '\'';
		for (integer i = 0; i < value.length(); i++)
		{
			string s = value.substring(i, i+1);
			if (s != '\'')
				ret += s;			
		}
		ret += '%\'';
		return ret;
	}
	
	private string GetValueToContains(string value)
	{
		string ret = '\'%';
		for (integer i = 0; i < value.length(); i++)
		{
			string s = value.substring(i, i+1);
			if (s != '\'')
				ret += s;			
		}
		ret += '%\'';
		return ret;
	}
}