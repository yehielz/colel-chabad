public with sharing class CSpecialActivitiesFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	private CFilterFamilys filterFamilys;
	private CFilterChildren filterChildren;
	private CSpecialActivitiesDb specialActivitiesDb;
	
	public boolean IsFamilyMember{ get{ return pSpecialActivities.ActivityType__c != null && pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember; } }
	public boolean IsFamily{ get{ return !IsFamilyMember; } }
	public boolean IsOneLinePaid{ get{ return specialActivitiesDb.IsOneLinePaid; } }
	
	public string currentMark{ get{ return specialActivitiesDb.currentMark; } set{ specialActivitiesDb.currentMark = value; } }
	public string Field{ get{ return specialActivitiesDb.Field; } set{ specialActivitiesDb.Field = value; } }
	
	public SpecialActivities__c pSpecialActivities
	{
		get
		{
			return specialActivitiesDb.GetpSpecialActivities();
		}
	}
	public List<CRowSpecialActivitiesLines> pSpecialActivitiesLines
	{
		get
		{
	 		return specialActivitiesDb.GetpSpecialActivitiesLines();
		}
	}
		
	public CSpecialActivitiesFacad(ApexPages.StandardController controller)
	{
		filterFamilys = new CFilterFamilys();
		filterChildren = new CFilterChildren();
		specialActivitiesDb = new CSpecialActivitiesDb(controller);
		errorMessage = '';
		if (pSpecialActivities.id == null)
			pageTitle = 'פעילות מיוחדת חדשה';
		else
			pageTitle = pSpecialActivities.name;
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		specialActivitiesDb.InsertpSpecialActivities();
		PageReference newCasePage = new ApexPages.StandardController(pSpecialActivities).view();
        return newCasePage;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CSpecialActivitiesValidator validator = new CSpecialActivitiesValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    //=============================================================================================================================================
    
    public List<selectOption> OptionsOperator1
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsOperator2
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsOperator3
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsOperator4
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsOperator5
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsOperator6
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsOperator7
	{
		get
		{
			return filterFamilys.GetOptionsOperators();
		}
	}
	public List<selectOption> OptionsField1
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	public List<selectOption> OptionsField2
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	public List<selectOption> OptionsField3
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	public List<selectOption> OptionsField4
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	public List<selectOption> OptionsField5
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	public List<selectOption> OptionsField6
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	public List<selectOption> OptionsField7
	{
		get
		{
			if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
				return filterChildren.GetOptionsFields();
			return filterFamilys.GetOptionsFields();
		}
	}
	
	public void onChangeList()
	{
		if (IsOneFull())
		{
			if (pSpecialActivities.ActivityType__c != null && pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
			{
				Field = Field == 'FamilyName' ? 'ChildName' : Field;
				List<Children__c> children = filterChildren.GetChildrenByFilter(pSpecialActivities);
				specialActivitiesDb.onChangeList(null, children);
			}
			else
			{
				Field = Field == 'ChildName' ? 'FamilyName' : Field;
				List<Family__c> families = filterFamilys.GetFamiliesByFilter(pSpecialActivities);
				specialActivitiesDb.onChangeList(families, null);
			}
		}
		else
			specialActivitiesDb.SetNewList();
		SortList();
	}
	
	private boolean IsOneFull()
	{
		if (pSpecialActivities.Field1ToFilter__c != null && pSpecialActivities.Operator1ToFilter__c != null && pSpecialActivities.Value1ToFilter__c != null)
			return true;
		if (pSpecialActivities.Field2ToFilter__c != null && pSpecialActivities.Operator2ToFilter__c != null && pSpecialActivities.Value2ToFilter__c != null)
			return true;
		if (pSpecialActivities.Field3ToFilter__c != null && pSpecialActivities.Operator3ToFilter__c != null && pSpecialActivities.Value3ToFilter__c != null)
			return true;
		if (pSpecialActivities.Field4ToFilter__c != null && pSpecialActivities.Operator4ToFilter__c != null && pSpecialActivities.Value4ToFilter__c != null)
			return true;
		if (pSpecialActivities.Field5ToFilter__c != null && pSpecialActivities.Operator5ToFilter__c != null && pSpecialActivities.Value5ToFilter__c != null)
			return true;
		if (pSpecialActivities.Field6ToFilter__c != null && pSpecialActivities.Operator6ToFilter__c != null && pSpecialActivities.Value6ToFilter__c != null)
			return true;
		if (pSpecialActivities.Field7ToFilter__c != null && pSpecialActivities.Operator7ToFilter__c != null && pSpecialActivities.Value7ToFilter__c != null)
			return true;
		return false;
	}
	
	public void SortList()
	{
		specialActivitiesDb.SortList();
	}
	
	public void refresh()
	{
		
	}
}