public with sharing class CPaymentValidator 
{
	private CPaymentFacad cPaymentFacad { get; set; }
	public string ErrorMessage;
	
	public CPaymentValidator()
	{
		
	}
	
	public CPaymentValidator(CPaymentFacad ExtPaymentFacad)
	{
		ErrorMessage = '';
		cPaymentFacad = ExtPaymentFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	    
    	if (ret)
    		ret = ValidateFamilyName();
		if (ret)
    		ret = ValidateTutorName();
    	if (ret)
    		ret = ValidateDate();  
	 	if (ret)
    		ret = ValidateChecks();
    			
	 	return ret;
	}

	private boolean ValidateFamilyName()
	{
		if (cPaymentFacad.pPayment.FamilyOrTutor__c == CObjectNames.ActivityApprovalPaymentTargetFamily && cPaymentFacad.pPayment.FamilyName__c == null)
		{
			ErrorMessage = CErrorMessages.PaymentFamilyName;
			return false;
		}
		return true;
	}	
	
	private boolean ValidateTutorName()
	{
		if (cPaymentFacad.pPayment.FamilyOrTutor__c == CObjectNames.ActivityApprovalPaymentTargetTutor && cPaymentFacad.pPayment.TutorName__c == null)
		{
			ErrorMessage = CErrorMessages.PaymentTutorName;
			return false;
		}
		return true;
	}	
	
	private boolean ValidateDate()
	{
		if (cPaymentFacad.pPayment.Date__c == null)
		{
			ErrorMessage = CErrorMessages.PaymentDate;
			return false;
		}
		return true;
	}	
	
	private boolean ValidateChecks()
	{
		boolean ret = true;
		
		map<string, list<Check__c>> pChecksMapByName = GetChecksMapByName();
		map<string, Check__c> otherChecksMapByName = GetOtherChecksMapByName();
		for (Check__c check : cPaymentFacad.pChecks)
		{
			if (ret)
				ret = ValidateChecksFull(check);
			if (ret)
				ret = ValidateCheckNumber(check, pChecksMapByName, otherChecksMapByName);
			if (ret)
				ret = ValidateBranchNumber(check);
			if (ret)
				ret = ValidateAccountNumber(check);
			if (ret)
				ret = ValidateDepositDate(check);
		}
		
		return ret;
	}
	
	private map<string, list<Check__c>> GetChecksMapByName()
	{
		map<string, list<Check__c>> ret = new map<string, list<Check__c>>();
		for (Check__c check : cPaymentFacad.pChecks)
		{
			if (ret.containsKey(check.name))
				ret.get(check.name).add(check);
			else
				ret.put(check.name, new list<Check__c> { check });
		}
		return ret;
	}
	
	private map<string, Check__c> GetOtherChecksMapByName()
	{
		Check__c[] checks = [select name, id from Check__c where id not in :cPaymentFacad.pChecks];
		map<string, Check__c> ret = new map<string, Check__c>();
		for (Check__c check : checks)
		{
			ret.put(check.name, check);
		}
		return ret;
	}
	
	private boolean ValidateChecksFull(Check__c check)
	{
		if ((check.AmountCheck__c == null ||check.AmountCheck__c == 0) && check.name != null)
		{
			ErrorMessage = CErrorMessages.PaymentCheckNotContainsAmountPar1 + check.name;
			ErrorMessage += CErrorMessages.PaymentCheckNotContainsAmountPar2;
			return false;
		}
		else if ((check.AmountCheck__c != null && check.AmountCheck__c > 0) && check.name == null)
		{
			ErrorMessage = CErrorMessages.PaymentCheckNotContainsName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateCheckNumber(Check__c check, map<string, list<Check__c>> pChecksMapByName, map<string, Check__c> otherChecksMapByName)
	{
		if (check.name != null)
		{
			if (!IsNumbers(check.name))
			{
				ErrorMessage = CErrorMessages.PaymentTheCheckNumber + check.name + CErrorMessages.PaymentMustBeNumbers;
				return false;
			}
			if (pChecksMapByName.containsKey(check.name) && pChecksMapByName.get(check.name).size() > 1)
			{
				ErrorMessage = CErrorMessages.PaymentCheckNotContainsAmountPar1 + check.name + CErrorMessages.PaymentDoubleInList;
				return false;
			}
			if (otherChecksMapByName.containsKey(check.name))
			{
				ErrorMessage = CErrorMessages.PaymentTheCheckNumber + check.name + CErrorMessages.PaymentCheckAlreadyExistInSystem;
				return false;
			}
		}
		return true;
	}
	
	private boolean IsNumbers(string str)
	{
		for (integer i = 0; i < str.length(); i++)
		{
			string s = str.substring(i, i+1);
			if (s != '0' && s != '1' && s != '2' && s != '3' && s != '4' && s != '5' && s != '6' && s != '7' && s != '8' && s != '9')
				return false;
		}
		return true;
	}
	
	private boolean ValidateBranchNumber(Check__c check)
	{
		if (check.name != null && check.BranchNumber__c == null)
		{
			ErrorMessage = CErrorMessages.PaymentCheckBranchNumber + check.name;
			ErrorMessage += CErrorMessages.PaymentExclamationMark;
			return false;
		}
		return true;
	}
	
	private boolean ValidateAccountNumber(Check__c check)
	{
		if (check.name != null && check.AccountNumber__c == null)
		{
			ErrorMessage = CErrorMessages.PaymentCheckAccountNumber + check.name;
			ErrorMessage += CErrorMessages.PaymentExclamationMark;
			return false;
		}
		return true;
	}
	
	private boolean ValidateDepositDate(Check__c check)
	{
		if (check.name != null && check.DepositDate__c == null)
		{
			ErrorMessage = CErrorMessages.PaymentCheckDepositDate + check.name;
			ErrorMessage += CErrorMessages.PaymentExclamationMark;
			return false;
		}
		return true;
	}
}