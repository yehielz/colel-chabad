public with sharing class CChildrenFullReportFacad 
{
	private CChildrenFullReportDB childrenFullReportDB;
	public list<CChildFullReportFacad> pChildren{ get{ return childrenFullReportDB.pChildren; } }
	
	public CChildrenFullReportFacad()
	{
		childrenFullReportDB = new CChildrenFullReportDB();	
	}
}