public with sharing class CTutorValidator 
{
	private CTutorFacad cTutorFacad { get; set; }
	public string ErrorMessage;
	
	public CTutorValidator(CTutorFacad ExtTutorFacad)
	{
		ErrorMessage = '';
		cTutorFacad = ExtTutorFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
	  	if (ret) 
	    	ret = ValidateFirstName();
    	if (ret) 
	    	ret = ValidateLastName();
    	if (ret) 
	    	ret = ValidatePhoneOrModilePhone();
	    if (ret) 
	    	ret = ValidateCity();
	    if (ret) 
	    	ret = ValidateStreet();
	    if (ret) 
	    	ret = ValidateApartmentNumber();
    	if (ret) 
	    	ret = ValidateProfessionsList();
    	if (ret) 
	    	ret = ValidateUserName();
	  	if (ret) 
	    	ret = ValidateCurrentJob();
	  	if (ret) 
	    	ret = ValidateEducationDescription();
	  	if (ret) 
	    	ret = ValidateFieldOfStudy();
	 	return ret;
	}
	
	private boolean ValidateFirstName()
	{
		if (cTutorFacad.pTutor.FirstName__c == null)
		{
			ErrorMessage = CErrorMessages.TutorFirstName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateCurrentJob()
	{
		if (cTutorFacad.pTutor.CurrentJob__c == null)
		{
			ErrorMessage = CErrorMessages.TutorCurrentJob;
			return false;
		}
		return true;
	}
	
	private boolean ValidateEducationDescription()
	{
		if (cTutorFacad.pTutor.EducationDescription__c == null)
		{
			ErrorMessage = CErrorMessages.TutorEducationDescription;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFieldOfStudy()
	{
		if (cTutorFacad.pTutor.FieldOfStudy__c == null)
		{
			ErrorMessage = CErrorMessages.TutorFieldOfStudy;
			return false;
		}
		return true;
	}
	
	private boolean ValidateLastName()
	{
		if (cTutorFacad.pTutor.LastName__c == null)
		{
			ErrorMessage = CErrorMessages.TutorLastName;
			return false;
		}
		return true;
	}
	
	private boolean ValidatePhoneOrModilePhone()
	{
		if (cTutorFacad.pTutor.MobilePhone__c == null && cTutorFacad.pTutor.Phone__c == null)
		{
			ErrorMessage = CErrorMessages.TutorPhoneOrModilePhone;
			return false;
		}
		return true;
	}
	
	private boolean ValidateProfessionsList()
	{
		if (cTutorFacad.pTutor.ProfessionsList__c == '[]')
		{
			ErrorMessage = CErrorMessages.TutorProfession;
			return false;
		}
		return true;
	}
	
	private boolean ValidateCity()
	{
		if (cTutorFacad.pTutor.City__c == null)
		{
			ErrorMessage = CErrorMessages.TutorCity;
			return false;
		}
		return true;
	}
	
	private boolean ValidateStreet()
	{
		if (cTutorFacad.pTutor.Street__c == null)
		{
			ErrorMessage = CErrorMessages.TutorStreet;
			return false;
		}
		return true;
	}
	
	private boolean ValidateApartmentNumber()
	{
		if (cTutorFacad.pTutor.ApartmentNumber__c == null)
		{
			ErrorMessage = CErrorMessages.TutorApartmentNumber;
			return false;
		}
		return true;
	}
	
	private boolean ValidateUserName()
	{
		if (cTutorFacad.pTutor.UserName__c != null)
		{
			Tutor__c[] tutors = [select name, id from Tutor__c where UserName__c = :cTutorFacad.pTutor.UserName__c and id != :cTutorFacad.pTutor.id];
			if (tutors.size() > 0)
			{
				ErrorMessage = CErrorMessages.TutorUserNameExist + 'חונך: ' + tutors[0].name;
				return false;
			}
			Support__c[] supporters = [select name, id from Support__c where UserName__c = :cTutorFacad.pTutor.UserName__c];
			if (supporters.size() > 0)
			{
				ErrorMessage = CErrorMessages.TutorUserNameExist + 'תומך: ' + supporters[0].name;
				return false;
			}
		}
		return true;
	}
}