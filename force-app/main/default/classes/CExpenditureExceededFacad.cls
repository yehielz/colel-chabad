public with sharing class CExpenditureExceededFacad 
{
	public string errorMessage { get; set; }
	public string pageTitle { get; set; }
	
	public ExpenditureExceeded__c pExpenditureExceeded 
	{ 
		get
		{
			return expenditureExceededDb.pExpenditureExceeded;
		}
	}
	
	private CExpenditureExceededDb expenditureExceededDb { get; set; }
	
	public CExpenditureExceededFacad(ApexPages.StandardController controller)
	{
		expenditureExceededDb = new CExpenditureExceededDb(controller);
		if (pExpenditureExceeded.id == null)
			pageTitle = 'הוצאה חריגה חדשה';
		else
			pageTitle = pExpenditureExceeded.name;
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		expenditureExceededDb.save();
		PageReference expenditureExceededViewPage = new ApexPages.StandardController(pExpenditureExceeded).view();
        return expenditureExceededViewPage;
	}
	
	private boolean Validation()
	{
		if (pExpenditureExceeded.ActivityReportName__c != null)
		{
			ActivityReport__c report = CActivityReportDb.GetActivityReportById(pExpenditureExceeded.ActivityReportName__c);
			if (report != null &&  report.ActivityReportStatus__c != null && report.ActivityReportStatus__c == CObjectNames.ActivityReportStatusPaid)
			{
				errorMessage = 'אין אפשרות לשמור הוצאה לדיווח ששולם כבר!';
				return false;
			}
		}
		return true;
	}
	
	public boolean IsPaid
	{ 
		get
		{
			if (pExpenditureExceeded.ActivityReportName__c != null && pExpenditureExceeded.ActivityReportName__r.ActivityReportStatus__c != null
				&& pExpenditureExceeded.ActivityReportName__r.ActivityReportStatus__c == CObjectNames.ActivityReportStatusPaid)
				return true;	
			return false;
		}
	}
	public boolean IsNotPaid{ get{ return !IsPaid; } }
}