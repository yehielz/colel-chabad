public with sharing class CDiplomaFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	
	private CDiplomaDb diplomaDb;
	public list<selectOption> pProfessionsOptions{ get{ return diplomaDb.pProfessionsOptions; } }
	public Diploma__c pDiploma
	{
		get
		{
			return diplomaDb.GetpDiploma();
		}
	}
	public List<CDiplomaLineInList> pDiplomaLines
	{
		get
		{
			List<CDiplomaLineInList> toReturn = diplomaDb.GetpDiplomaLines();
			Integer index = 0;
			for(CDiplomaLineInList line :toReturn){
				line.lineIndex = index;
				index++;
			}
			return toReturn;
		}
	}
		
	public CDiplomaFacad(ApexPages.StandardController controller)
	{
		diplomaDb = new CDiplomaDb(controller, IsNewPage());
		errorMessage = '';
		if (pDiploma.id == null)
			pageTitle = 'תעודה חדשה';
		else
			pageTitle = pDiploma.name;
	}
	
	private boolean IsNewPage()
	{
		string currentPageName = string.valueOf(ApexPages.currentPage());
		return currentPageName.contains(CObjectNames.DiplomaNew);
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		diplomaDb.InsertpDiploma();
		PageReference newCasePage = new ApexPages.StandardController(pDiploma).view();
        return newCasePage;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CDiplomaValidator validator = new CDiplomaValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    /*public void OnChangeGrade()
    {
    	diplomaDb.OnChangeGrade();
    }*/
    
    public void OnChangeYear()
    {
    	diplomaDb.OnChangeYear();
    }
	
	public void AddDiplomaLine()
	{
		diplomaDb.AddDiplomaLine();
	}
	
	public void refresh()
	{
		
	}
    
    public string PrintDiplomaPageUrl{ get{ return CObjectNames.getBaseUrl() + page.DiplomaToPrint.getUrl() + '?id=' + pDiploma.id; } }
    
    public string printLayout{ get{ return CObjectNames.getBaseUrl() + page.DiplomaPrintLayout.getUrl() + '?ID=' + pDiploma.id; } }
}