public without sharing class CActivityApprovalDb 
{
    public static boolean IsAfterChangeCoordinator { get; set; }
    
    private ActivityApproval__c pActivityApproval { get; set; }
    private map<string, List<ActivityApproval__c>> activityApprovalByRequestForTreatment;
    private map<string, Profession__c> professionsMap;
    private map<string, list<ActivityApproval__c>> activityApprovalByChildNameMap;
    
    public CActivityApprovalDb(boolean isForFeed)
    {
    }
    
    public CActivityApprovalDb()
    {
        pActivityApproval = new ActivityApproval__c();
    }
    
    public CActivityApprovalDb(ApexPages.StandardController controller){
        pActivityApproval = (ActivityApproval__c)controller.getRecord();
        if (pActivityApproval.id != null)
            SetpActivityApprovalBypActivityApprovalId(pActivityApproval.id);
        else
            SetPropeties();
    }
    
    private void SetPropeties(){
        
        pActivityApproval.TutorName__c = pApprovalRequest.TutorName__c;
        pActivityApproval.ChildName__c = pApprovalRequest.ChildName__c;
        pActivityApproval.ProfessionName__c = pApprovalRequest.ProfessionName__c;
        pActivityApproval.WeeklyHours__c = pApprovalRequest.WeeklyHours__c;
        pActivityApproval.FromDate__c = pApprovalRequest.FromDate__c;
        pActivityApproval.ToDate__c = pApprovalRequest.ToDate__c;
        pActivityApproval.ProfessionOther__c = pApprovalRequest.ProfessionOther__c;
        pActivityApproval.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
        SetRateByTutor();
        pActivityApproval.Note__c = pApprovalRequest.Comment__c;
    }
    
    private void SetpActivityApprovalBypActivityApprovalId(string id){
        pActivityApproval = [select id, TutorName__c, ProfessionName__c, ProfessionOther__c, RequestForTreatmentName__c, AmountApproved__c, TotalHours__c, 
                             HoursInSequence__c, SendEmailToTutor__c, SendEmailToFamily__c, Note__c, Rate__c, ChildName__c, CoordinatorUserName__c, 
                             AmountSpent__c, MinutesInSequence__c, PaymentTarget__c, FromDate__c, ToDate__c, WeeklyHours__c, name, CaseName__c, 
                             NoteToPrintPage__c, TotalHoursSpent__c, PercentApproved__c, RequestForTreatmentName__r.ApprovalAmount__c,  
                             TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, IsApprovalOnProbation__c, Fare__c, ApprovalDate__c, 
                             ChildName__r.FamilyName__r.City__c, ChildName__r.FamilyName__r.Address__c,ChildName__r.FamilyName__r.MobilePhone__c, TutorName__r.Phone__c, TutorName__r.MobilePhone__c,
                             ChildName__r.FamilyName__r.Name, ChildName__r.FamilyName__r.ParentName__c, ChildName__r.FamilyName__r.Username__c,
                             RequestForTreatmentName__r.CaseName__r.OwnerId,ChildName__r.Name,ProfessionName__r.Name,
                             IsAttachmentRequired__c, LastReportDate__c, SchoolYear__c, IsPrinted__c, ActivitySecretCode__c from ActivityApproval__c where id = :id];
    }
    
    public void save(){
        SetJob();
        SetHoursInSequence();
        //SetCoordinatorUserName();
        if (pActivityApproval.id == null)
        {
            //SetToSendEmails();
            // pActivityApproval.ActivitySecretCode__c = String.valueof(DateTime.now().getTime());
            insert pActivityApproval;
        }
        else
            update pActivityApproval;
    }
    
    public void SetJob() {
        /*try 
        {
            //string jobId = System.schedule('createTasksIn10InTheMonth', '0 0 10 10 * ?', new CScheduledMerge());
        }
        catch (Exception e) { }*/
    }
    /*
    private void SetCoordinatorUserName()
    {
        if (pActivityApproval.ChildName__c != null)
        {
            Children__c[] children=[select name, id, CoordinatorUserName__c from Children__c where id = :pActivityApproval.ChildName__c];
            if (children.size() > 0){
                pActivityApproval.CoordinatorUserName__c = children[0].CoordinatorUserName__c;
                //pActivityApproval.OwnerId = children[0].OwnerId;
            }
        }
    }*/
    
    private void SetHoursInSequence() {
        decimal minutes = pActivityApproval.MinutesInSequence__c;
        if (minutes == null) {minutes = 0;}
        decimal hours = 0;
        while (minutes > 59)
        {
            minutes = minutes - 60;
            hours++;
        }
        minutes = (minutes * (100.0/6000.0));
        pActivityApproval.HoursInSequence__c = hours + minutes;
    }
    
    public void SetRateByTutor() {
        if (pActivityApproval.TutorName__c != null && pActivityApproval.ProfessionName__c != null)
        {
            TutorLine__c[] professionInTutor = [select name, id, Rate__c from TutorLine__c where 
                                                        ProfessionName__c = :pActivityApproval.ProfessionName__c and 
                                                        TutorName__c = :pActivityApproval.TutorName__c];
            if (professionInTutor.size() > 0)
                pActivityApproval.Rate__c = professionInTutor[0].Rate__c;
        }
    }
    
    public void SetActivityApprovalByRequestForTreatment(list<RequestForTreatment__c> requests){
        ActivityApproval__c[] allActivitiesApproval = [select TutorName__c, AmountSpent__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c,  
                                                       ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, Note__c, ChildName__c, TotalHours__c,
                                                       PercentApproved__c, SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, 
                                                       TotalHoursSpent__c, NoteToPrintPage__c, WeeklyHours__c, id, CaseName__c, TreatedByCoordinator__c, 
                                                       FromDate__c, name, MeetingsNumber__c, TotalMeetingSpent__c, IsApprovalOnProbation__c, Fare__c, 
                                                       ChildName__r.FamilyName__r.City__c,
                                                       ApprovalDate__c, IsAttachmentRequired__c, SchoolYear__c, IsPrinted__c
                                                       from ActivityApproval__c where RequestForTreatmentName__c in :requests];
        activityApprovalByRequestForTreatment = new Map<string, List<ActivityApproval__c>>();
        for (integer i = 0; i < allActivitiesApproval.size(); i++)
        {
            if (activityApprovalByRequestForTreatment.containsKey(allActivitiesApproval[i].RequestForTreatmentName__c))
                activityApprovalByRequestForTreatment.get(allActivitiesApproval[i].RequestForTreatmentName__c).add(allActivitiesApproval[i]);
            else
                activityApprovalByRequestForTreatment.put(allActivitiesApproval[i].RequestForTreatmentName__c, new List<ActivityApproval__c> { allActivitiesApproval[i] });
        }       
    }
    
    public void SetActivityApprovalByChildNameMap(list<Children__c> children) 
    {
        ActivityApproval__c[] allActivitiesApproval = [select TutorName__c, AmountSpent__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c,
                                                       ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, Note__c, ChildName__c, TotalHoursSpent__c, 
                                                       PercentApproved__c, SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c,
                                                       TotalHours__c, NoteToPrintPage__c, FromDate__c, WeeklyHours__c, id, CaseName__c, TreatedByCoordinator__c, 
                                                       name, MeetingsNumber__c, TotalMeetingSpent__c, IsApprovalOnProbation__c, Fare__c, ApprovalDate__c, 
                                                       ChildName__r.FamilyName__r.City__c,
                                                       IsAttachmentRequired__c, SchoolYear__c, IsPrinted__c from ActivityApproval__c
                                                       where ChildName__c in :children];
        activityApprovalByChildNameMap = new map<string, list<ActivityApproval__c>>();
        for (integer i = 0; i < allActivitiesApproval.size(); i++)
        {
            if (activityApprovalByChildNameMap.containsKey(allActivitiesApproval[i].ChildName__c))
                activityApprovalByChildNameMap.get(allActivitiesApproval[i].ChildName__c).add(allActivitiesApproval[i]);
            else
                activityApprovalByChildNameMap.put(allActivitiesApproval[i].ChildName__c, new List<ActivityApproval__c> { allActivitiesApproval[i] });
        }
    }
    
    public void SetProfessionsMap()
    {
        Profession__c[] professions = CProfessionDb.GetProfessions();
        professionsMap = new Map<string, Profession__c>();
        for (integer i = 0; i < professions.size(); i++)
        {
            professionsMap.put(professions[i].id, professions[i]);
        }
    }
    
    public ActivityApproval__c GetpActivityApproval()
    {
        return pActivityApproval;
    }
    /*
    private void SendEmail(List<string> EmailsToSend, string VersionEmail)
    {
        if (EmailsToSend.size() > 0)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(EmailsToSend);
            mail.setHtmlBody(VersionEmail);
            mail.Subject = 'אישור פעילות';
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    private void SendEmailToTutor(string professionName)
    {
        if (pActivityApproval.TutorName__c != null && pActivityApproval.SendEmailToTutor__c)
        {
            Children__c[] child = [select name, id from Children__c where id = :pActivityApproval.ChildName__c];
            Tutor__c[] tutor = [select name, id, Email__c from Tutor__c where id = :pActivityApproval.TutorName__c];
            if (child.size() > 0 && tutor.size() > 0 && tutor[0].Email__c != null)
            {
                string VersionEmail = VersionEmail('החונך: ' + tutor[0].name, child[0].name, professionName);
                sendEmail(new list<string> { tutor[0].Email__c }, VersionEmail);
            }
        }
    }
    
    private void SendEmailToFamily(string professionName)
    {
        if (pActivityApproval.ChildName__c != null && pActivityApproval.SendEmailToFamily__c)
        {
            Children__c[] child = [select name, id, FamilyName__c, FamilyName__r.name, FamilyName__r.Email__c from Children__c where id = :pActivityApproval.ChildName__c];
            if (child.size() > 0 && child[0].FamilyName__c != null && child[0].FamilyName__r.Email__c != null)
            {
                string VersionEmail = VersionEmail('המשפחה: ' + child[0].FamilyName__r.name, child[0].name, professionName);
                sendEmail(new list<string> { child[0].FamilyName__r.Email__c }, VersionEmail);
            }
        }
    }
    
    private void SetToSendEmails()
    {
        string professionName = GetProfessionName();
        if (pActivityApproval.SendEmailToTutor__c)
            sendEmailToTutor(professionName);
        if (pActivityApproval.SendEmailToFamily__c)
            sendEmailToFamily(professionName);
    }
    
    private string GetProfessionName()
    {
        string ret = '';
        if (pActivityApproval.ProfessionName__c != null)
        {
            Profession__c[] profession = [select name, id from Profession__c where id = :pActivityApproval.ProfessionName__c];
            if (profession.size() > 0)
                ret = 'מקצוע: <b>' + profession[0].name + '</b>';
        }
        return ret;
    }
    
    private string VersionEmail(string familyOrTutor, string childName, string professionName)
    {
        string htmlBody = '<div dir="rtl"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>לכבוד ' + familyOrTutor + '.</b></p><br><br><div>' +
                          '<p/> אנו שמחים לבשר לכם על אישור פעילות עבור: <b>' + childName + '</b>&nbsp;' + professionName + '</p><br><br>בתודה<br>צוות חסד מנחם מענדל</div>';
        return htmlBody;
    }*/
    public list<ActivityApproval__c> UpdateActivityApprovalAfterUpdateRequestForTreatment(RequestForTreatment__c requestForTreatment)
    {
        list<ActivityApproval__c> listToUpdate = new list<ActivityApproval__c>();
        if (activityApprovalByRequestForTreatment.containsKey(requestForTreatment.id))
        {
            listToUpdate = activityApprovalByRequestForTreatment.get(requestForTreatment.id);
            if (professionsMap.get(requestForTreatment.ProfessionName__c).Group__c == CObjectNames.ProfessionGroupClass)
            {
                for (ActivityApproval__c approval : listToUpdate)
                {
                    approval.HoursInSequence__c = null;
                    approval.MinutesInSequence__c = null;
                    approval.WeeklyHours__c = null;
                    approval.TutorName__c = requestForTreatment.TutorName__c;
                    approval.ProfessionName__c = requestForTreatment.ProfessionName__c;
                    approval.ChildName__c = requestForTreatment.ChildName__c;
                }
            }
            else
            {
                for (ActivityApproval__c approval : listToUpdate)
                {
                    //approval.AmountApproved__c = null;
                    approval.TutorName__c = requestForTreatment.TutorName__c;
                    approval.ProfessionName__c = requestForTreatment.ProfessionName__c;
                    approval.ChildName__c = requestForTreatment.ChildName__c;
                }
            }
        }
        return listToUpdate;
    }
    /*
    public void UpdateActivityApprovalsAfterUpdateChild(string childId, string coordinatorUserName)
    {
        if (activityApprovalByChildNameMap.containsKey(childId))
        {
            list<ActivityApproval__c> activityApprovals = activityApprovalByChildNameMap.get(childId);
            for (integer i = 0; i < activityApprovals.size(); i++)
            {
                activityApprovals[i].CoordinatorUserName__c = coordinatorUserName;
            }
            if (activityApprovals.size() > 0)
            {
                CActivityApprovalDb.IsAfterChangeCoordinator = true;
                update activityApprovals;
            }
        }
    }*/
    //v2
    /*
    public void UpdateActivityApprovalsAfterUpdateChild(Set<Id> childIds, Map<Id,Children__c> triggerNewMap){
		list<ActivityApproval__c> activityApprovalsToUpdate = new list<ActivityApproval__c> ();
		for(Id childId :childIds){
			if (activityApprovalByChildNameMap.containsKey(childId)) {
	            list<ActivityApproval__c> activityApprovals = activityApprovalByChildNameMap.get(childId);
	            if(activityApprovals != null && activityApprovals.size() > 0 ){
		            for (ActivityApproval__c a :activityApprovals )  {
		                a.CoordinatorUserName__c = triggerNewMap.get(childId).CoordinatorUserName__c;
		                //a.OwnerId = triggerNewMap.get(childId).OwnerId;
		            }
		            activityApprovalsToUpdate.addAll(activityApprovals);
	            }
	        }
		}
		
		if (activityApprovalsToUpdate.size() > 0)  {
            CActivityApprovalDb.IsAfterChangeCoordinator = true;
            update activityApprovalsToUpdate;
        }
        
    }*/
    
    public list<CObjectsHistory> GetActivityApprovalHistories()
    {
        ActivityApproval__History[] activityApprovalHistories = [select ParentId, CreatedDate, CreatedBy.Name, CreatedById, Field, NewValue, OldValue from 
                                                                      ActivityApproval__History where ParentId = :pActivityApproval.id order by CreatedDate desc];
        list<CObjectsHistory> ret = new list<CObjectsHistory>();
        for (integer i = 0; i < activityApprovalHistories.size(); i++)
        {
            ret.add(new CObjectsHistory(activityApprovalHistories[i].Field, activityApprovalHistories[i].CreatedBy.Name, activityApprovalHistories[i].OldValue,
                                        activityApprovalHistories[i].NewValue, activityApprovalHistories[i].CreatedDate, Schema.SObjectType.ActivityApproval__c.fields.getMap()));
        }
        return ret;
    }
    
    public static ActivityApproval__c GetActivityApprovalById(string id)
    {
        ActivityApproval__c[] activityApproval = [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, name, Fare__c,
                                                        ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, SendEmailToFamily__c,
                                                        Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, Note__c, CaseName__c,
                                                        TotalHours__c, TotalHoursSpent__c, PercentApproved__c, NoteToPrintPage__c, WeeklyHours__c, id, 
                                                        AmountSpent__c, TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, IsApprovalOnProbation__c,
                                                        ApprovalDate__c, IsAttachmentRequired__c, SchoolYear__c, IsPrinted__c,ChildName__r.FamilyName__r.Name,
                                                        ChildName__r.FamilyName__r.TermsAgreement__c,
                                                        ChildName__r.FamilyName__r.City__c,ChildName__r.name,ProfessionName__r.name
                                                        from ActivityApproval__c where id = :id];
        if (activityApproval.size() > 0)
            return activityApproval[0];
        return null;    
    }
    
    public static ActivityApproval__c[] GetActivityApprovalByCaseId(string id)
    {
        return [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, 
                NoteToPrintPage__c, ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, 
                SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, 
                Note__c, WeeklyHours__c, id, ProfessionName__r.name, ProfessionName__r.Group__c, CaseName__c,
                RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, name, ChildName__r.name,
                ProfessionName__r.ProfessionTypeName__r.PaymentType__c, PercentApproved__c, CreatedDate, Fare__c,
                TotalHours__c, TotalHoursSpent__c, ChildName__r.FamilyName__r.name, AmountSpent__c, IsApprovalOnProbation__c,
                ChildName__r.FamilyName__r.City__c, ChildName__r.FamilyName__r.Address__c,
                ApprovalDate__c, IsAttachmentRequired__c, SchoolYear__c, IsPrinted__c from ActivityApproval__c 
                where RequestForTreatmentName__r.CaseName__c = :id order by CreatedDate];
    }
    
    public static boolean GetActivityApprovalForSameRequestType(ActivityApproval__c approval, String requestTypeId)   {
    	system.debug('\n\n yehiel childId'  + approval.ChildName__c );
    	system.debug('\n\n yehiel schoolYearId'  + approval.SchoolYear__c );
    	system.debug('\n\n yehiel requestTypeId'  + requestTypeId );
        List<ActivityApproval__c> approvals = [select Id, Name from ActivityApproval__c 
                 where ChildName__c = :approval.ChildName__c  
                 and SchoolYear__c =:approval.SchoolYear__c 
                 and ProfessionName__r.ProfessionTypeName__r.Id = :requestTypeId 
                 and Id != :approval.Id 
                 order by CreatedDate];
    
        System.debug('===> ' + approvals.size());
    	if(approvals.size()>0)
    		return true;
    	else return false;
    }
    
    public static ActivityApproval__c[] GetActivityApprovalByChildIdAndYear(string childId, string schoolYearId)
    {
    	return [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, 
                NoteToPrintPage__c, ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, 
                SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, 
                Note__c, WeeklyHours__c, id, ProfessionName__r.name, ProfessionName__r.Group__c, CaseName__c,
                RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, name, ChildName__r.Name,
                ProfessionName__r.ProfessionTypeName__r.PaymentType__c, PercentApproved__c, CreatedDate,
                TotalHours__c, TotalHoursSpent__c, ChildName__r.FamilyName__r.Name, AmountSpent__c, 
                TutorName__r.name, TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, Fare__c,
                IsApprovalOnProbation__c, ApprovalDate__c, IsAttachmentRequired__c, SchoolYear__c,
                ChildName__r.FamilyName__r.City__c,
                TutorName__r.id, IsPrinted__c from ActivityApproval__c 
                where ChildName__c = :childId and SchoolYear__c =:schoolYearId order by CreatedDate];
    }
    
    public static ActivityApproval__c[] GetActivityApprovalByChildId(string childId)
    {
        return [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, 
                NoteToPrintPage__c, ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, 
                SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, 
                Note__c, WeeklyHours__c, id, ProfessionName__r.name, ProfessionName__r.Group__c, CaseName__c,
                RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, name, ChildName__r.name,
                ProfessionName__r.ProfessionTypeName__r.PaymentType__c, PercentApproved__c, CreatedDate,
                TotalHours__c, TotalHoursSpent__c, ChildName__r.FamilyName__r.name, AmountSpent__c, 
                TutorName__r.name, TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, Fare__c,
                IsApprovalOnProbation__c, ApprovalDate__c, IsAttachmentRequired__c, SchoolYear__c,
                ChildName__r.FamilyName__r.City__c,
                TutorName__r.id, IsPrinted__c from ActivityApproval__c 
                where ChildName__c = :childId order by CreatedDate];
    }
    
    public static ActivityApproval__c[] GetActivityApprovalsByChildren(list<string> childrenIds)
    {
        return [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, 
                NoteToPrintPage__c, ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, 
                SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, 
                Note__c, WeeklyHours__c, id, ProfessionName__r.name, ProfessionName__r.Group__c, CaseName__c,
                RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, name, ChildName__r.name,
                ProfessionName__r.ProfessionTypeName__r.PaymentType__c, PercentApproved__c, CreatedDate,
                TotalHours__c, TotalHoursSpent__c, ChildName__r.FamilyName__r.name, AmountSpent__c, 
                TutorName__r.name, TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, Fare__c,
                ChildName__r.FamilyName__r.City__c,
                IsApprovalOnProbation__c, ApprovalDate__c, IsAttachmentRequired__c, SchoolYear__c, IsPrinted__c
                from ActivityApproval__c where ChildName__c in :childrenIds order by CreatedDate];
    }
    
    public static map<Id, ActivityApproval__c> GetActivityApprovalsMap(Set<String> approvalIds)
    {
        map<Id, ActivityApproval__c> activityApprovals =  new map<Id, ActivityApproval__c>([select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, name, 
                                                          ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, SendEmailToFamily__c,
                                                          Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, Note__c, CaseName__c,
                                                          TotalHours__c, TotalHoursSpent__c, PercentApproved__c, NoteToPrintPage__c, WeeklyHours__c, id,
                                                          AmountSpent__c, TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, Fare__c,
                                                          IsApprovalOnProbation__c, ApprovalDate__c, IsAttachmentRequired__c, IsPrinted__c,
                                                          ChildName__r.FamilyName__r.City__c
                                                          from ActivityApproval__c where Id in:approvalIds]);
        return activityApprovals;
    }
    
    private RequestForTreatment__c approvalRequest;
    public RequestForTreatment__c pApprovalRequest
    {
        get
        {
            if (approvalRequest == null)
                approvalRequest = CRequestForTreatmentDb.GetRequestForTreatmentById(pActivityApproval.RequestForTreatmentName__c);
            return approvalRequest;
        }
    }
    
    private map<string, ActivityApproval__c> activityApprovalMap;//this one should be replaced completely by the following one but in the meantime I kept it.
    private map<Id, ActivityApproval__c> activityApprovalMapByIds;
    private map<string, decimal> totalHoursSpentByActivityApprovalMap;
    private map<string, decimal> amountSpentByActivityApprovalMap;
    private map<string, integer> totalMeetingsByActivityApprovalMap;
    public void SetActivityApprovalMap(Set<String> approvalIds)  {
        activityApprovalMapByIds = new Map<Id,ActivityApproval__c>([select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, name, 
                                                          ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, ChildName__c, SendEmailToFamily__c,
                                                          Rate__c, MinutesInSequence__c, PaymentTarget__c, ToDate__c, FromDate__c, Note__c, CaseName__c,
                                                          TotalHours__c, TotalHoursSpent__c, PercentApproved__c, NoteToPrintPage__c, WeeklyHours__c, id,
                                                          AmountSpent__c, TreatedByCoordinator__c, MeetingsNumber__c, TotalMeetingSpent__c, Fare__c,
                                                          IsApprovalOnProbation__c, ApprovalDate__c, IsAttachmentRequired__c, IsPrinted__c,
                                                          ChildName__r.FamilyName__r.City__c
                                                          from ActivityApproval__c
                                                          where Id in :approvalIds]);
                                                          
		SetTotalHoursAndAmountSpentByActivityApprovalMap(approvalIds);
		SetTotalMeetingsByActivityApprovalMap(approvalIds);
		SetLastReportDatesByApproval(approvalIds);
		list<ActivityApproval__c> listToUpdate = new list<ActivityApproval__c>();
		for (string str : approvalIds) {
			ActivityApproval__c approval = UpdateActivityApprovalAfterReport(str);
			if (approval != null)
				listToUpdate.add(approval);
			
		}
		if (listToUpdate.size() > 0) {
			CActivityApprovalDb.IsAfterChangeCoordinator = true;
			update listToUpdate;
		}                                                          
    }
    
    public void SetTotalMeetingsByActivityApprovalMap(Set<string> approvalsIds){
        DayInActivityReport__c[] days = [select id, name, ActivityReportName__c, ActivityReportName__r.ActivityApprovalName__c 
                                         from DayInActivityReport__c where ActivityReportName__r.ActivityApprovalName__c in :approvalsIds];
        totalMeetingsByActivityApprovalMap = new map<string, integer>();
        for (DayInActivityReport__c day : days){
            integer num = 1;
            if (totalMeetingsByActivityApprovalMap.containsKey(day.ActivityReportName__r.ActivityApprovalName__c))  
                num += totalMeetingsByActivityApprovalMap.get(day.ActivityReportName__r.ActivityApprovalName__c);
            totalMeetingsByActivityApprovalMap.put(day.ActivityReportName__r.ActivityApprovalName__c, num);
        }
    }
    
    public void SetTotalHoursAndAmountSpentByActivityApprovalMap(Set<string> approvalsIds){
        AggregateResult[] activityReports = [select ActivityApprovalName__c, sum(TotalReportHours__c), sum(PaymentAmountClass__c) 
                                             from ActivityReport__c where ActivityApprovalName__c in :approvalsIds group by ActivityApprovalName__c];
        totalHoursSpentByActivityApprovalMap = new map<string, decimal>();
        amountSpentByActivityApprovalMap = new map<string, decimal>();
        for (AggregateResult agg : activityReports){
            SetTotalHoursSpentByActivityApprovalMap(agg);
            SetAmountSpentByActivityApprovalMap(agg);
        }
    }
    
    public void SetTotalHoursSpentByActivityApprovalMap(AggregateResult agg) {
        decimal total = 0;
        if (agg != null && (decimal)agg.get('expr0') != null)
            total = (decimal)agg.get('expr0');
        string activityApprovalName = string.valueOf(agg.get('ActivityApprovalName__c'));
        if (totalHoursSpentByActivityApprovalMap.containsKey(activityApprovalName)){
            decimal oldValue = totalHoursSpentByActivityApprovalMap.get(activityApprovalName);
            totalHoursSpentByActivityApprovalMap.put(activityApprovalName, oldValue + total);
        }
        else
            totalHoursSpentByActivityApprovalMap.put(activityApprovalName, total);
    }
    
    public void SetAmountSpentByActivityApprovalMap(AggregateResult agg){
        decimal total = 0;
        if (agg != null && (decimal)agg.get('expr1') != null)
            total = (decimal)agg.get('expr1');
        string activityApprovalName = string.valueOf(agg.get('ActivityApprovalName__c'));
        if (amountSpentByActivityApprovalMap.containsKey(activityApprovalName))
        {
            decimal oldValue = amountSpentByActivityApprovalMap.get(activityApprovalName);
            amountSpentByActivityApprovalMap.put(activityApprovalName, oldValue + total);
        }
        else
            amountSpentByActivityApprovalMap.put(activityApprovalName, total);
    }
    
    private map<string, datetime> lastReportDatesByApproval { get; set; }
    public void SetLastReportDatesByApproval(Set<string> approvalIds)   {
        ActivityReport__c[] reports = [select name, id, CreatedDate, ActivityApprovalName__c from ActivityReport__c 
                                        where ActivityApprovalName__c in :approvalIds order by CreatedDate desc];
        lastReportDatesByApproval = new map<string, datetime>();
        for (ActivityReport__c report : reports)    {
            if (!lastReportDatesByApproval.containsKey(report.ActivityApprovalName__c))
                lastReportDatesByApproval.put(report.ActivityApprovalName__c, report.CreatedDate);
        }
    }
    
    public ActivityApproval__c UpdateActivityApprovalAfterReport(string approvalId)  {
        if (activityApprovalMapByIds.containsKey(approvalId)) {
            ActivityApproval__c approval = activityApprovalMapByIds.get(approvalId);
            if (lastReportDatesByApproval.containsKey(approvalId))
                approval.LastReportDate__c = lastReportDatesByApproval.get(approvalId);
            
            approval.TotalHoursSpent__c = 0;
            if (totalHoursSpentByActivityApprovalMap.containsKey(approvalId))
                approval.TotalHoursSpent__c = totalHoursSpentByActivityApprovalMap.get(approvalId);
            
            approval.TotalMeetingSpent__c = 0;
            if (totalMeetingsByActivityApprovalMap.containsKey(approvalId))
                approval.TotalMeetingSpent__c = totalMeetingsByActivityApprovalMap.get(approvalId);
            
            approval.AmountSpent__c = 0;
            if (amountSpentByActivityApprovalMap.containsKey(approvalId))
                approval.AmountSpent__c = amountSpentByActivityApprovalMap.get(approvalId);
            
            if (approval.id != null)
                return approval;
        }
        return null;
    }
    
    public static ActivityApproval__c GetApprovalByLoginInf(string userName, string approvalCode) {
    	// approvalCode = 'א.פ-' + approvalCode;
        ActivityApproval__c[] approvals = [SELECT name, id ,ActivityApproval__c.ChildName__r.FamilyName__c, TutorName__c
        									FROM ActivityApproval__c 
        									WHERE ChildName__r.FamilyName__r.UserName__c != null 
        									AND ChildName__r.FamilyName__r.UserName__c = :userName 
                                   			AND ActivitySecretCode__c = :approvalCode];
        if (approvals.size() > 0)
            return approvals[0];
        return null;
    } 
    
    public static boolean IsUserNameAndApprovalCodeExist(string userName, string approvalCode) {
        // approvalCode = 'א.פ-' + approvalCode;
        ActivityApproval__c[] approvals = [select name, id 
        									FROM ActivityApproval__c 
        									WHERE ChildName__r.FamilyName__r.UserName__c != null 
        									AND ChildName__r.FamilyName__r.UserName__c = :userName 
                                            AND ActivitySecretCode__c = :approvalCode];
        if (approvals.size() > 0)
            return true;
        return false;
    }
}