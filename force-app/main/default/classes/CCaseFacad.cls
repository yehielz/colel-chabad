public without sharing class CCaseFacad
{
	public string TuotrUrl
	{
		get
		{
			Schema.DescribeSObjectResult fR = Tutor__c.sObjectType.getDescribe();
        	return CObjectNames.getBaseUrl() + '/' + fR.getKeyPrefix() + '/e';
		}
	}
	public string newNoteLink{ get{ return '/apex/NoteInCaseNew?caseId=' + pCase.id; } }
	
	public string RemoveOrAddToMyAttentionButtonText
	{
		get
		{
			if (pCase.ToMyAttention2__c != null && pCase.ToMyAttention2__c.contains(system.Userinfo.getName()))
				return 'הסר מתשומת ליבי';
			return 'הוסף לתשומת ליבי';
		}
	}
	
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	public List<SelectOption> OptionToSelectFamilyOrChild { get; set; }
	
	public boolean bFamily { get; set; }
	public boolean bChild { get; set; }
	
	public string UsersIDidNotChoose { get; set; }
	public string UsersIChoose { get; set; }
	
	public List<SelectOption> OptionToSelectUsersWhichIDidNotChoose { get; set; }
	public List<SelectOption> OptionToSelectUsersWhichIChose { get; set; }
	
	private CCaseDb caseDb;
	public List<SelectOption> professionTypesOptions 
	{ 
		get
		{
			return caseDb.GetProfessionTypesOptions();
		}
	}
	
	public List<SelectOption> OptionsChildrenByFamily 
	{ 
		get
		{
			return caseDb.OptionsChildrenByFamily();
		}
	}
	
	public Case__c pCase
	{
		get
		{
			return caseDb.GetCase();
		}
	}
	
	public list<CRequestInCase> pRequestsInCase
	{
		get
		{
			return caseDb.GetpRequestsInCase();
		}
	}
		
	public CCaseFacad(ApexPages.StandardController controller)
	{
		caseDb = new CCaseDb(controller);
		OptionToSelectFamilyOrChild = caseDb.OptionToSelectFamilyOrChild();
		
		if (IsTable())
		{
			OptionToSelectUsersWhichIDidNotChoose = caseDb.OptionToSelectUsers();
			OptionToSelectUsersWhichIChose = new List<SelectOption>();
			UsersIChoose = pCase.AssignedUsers__c;
			if (UsersIChoose == null)
				UsersIChoose = '[]';
			CheckUsersIChoose();
		}
		
		SetProperties();
	}
	
	public boolean IsTable()
	{
		string currentPageName = string.valueOf(ApexPages.currentPage());
		if (Test.isRunningTest())
			return true;
		else
			return currentPageName.contains(CObjectNames.CaseNew); 
	}
	
	public PageReference save()
	{
		if (IsTable())
		{
			SetHours();
			FillpCaseAssigneeUsersList();
		}
			
		if (!Validation())
			return null;
			
		caseDb.save(bChild); 
		PageReference caseViewPage = new ApexPages.StandardController(pCase).view();
		return caseViewPage;
	}
	
	private void SetHours()
	{
		pCase.StartTime__c = (StartHour != null && StartHour.trim() != '' ? CObjectNames.GetFullNumberInString(integer.valueOf(StartHour)) : '');
		pCase.StartTime__c += (StartMinute != null && StartMinute.trim() != '' ? CObjectNames.GetFullNumberInString(integer.valueOf(StartMinute)) : '');
		pCase.StartTime__c = CValidateTimesAndConverterTimes.FillFields(pCase.StartTime__c);
		pCase.EndTime__c = (EndHour != null && EndHour.trim() != '' ? CObjectNames.GetFullNumberInString(integer.valueOf(EndHour)) : '');
		pCase.EndTime__c += (EndMinute != null && EndMinute.trim() != '' ? CObjectNames.GetFullNumberInString(integer.valueOf(EndMinute)) : '');
		pCase.EndTime__c = CValidateTimesAndConverterTimes.FillFields(pCase.EndTime__c);
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
    	CCaseValidator validator = new CCaseValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    private void FillpCaseAssigneeUsersList()
    {
    	string toFill = '';
    	if (OptionToSelectUsersWhichIChose != null)
    	{
	    	for (integer i = 0; i < OptionToSelectUsersWhichIChose.size(); i++)
	    	{
	    		if (i == 0)
	    		{
	    			toFill = OptionToSelectUsersWhichIChose[i].getValue();
	    		}
				else 
				{
					toFill += '; ' + OptionToSelectUsersWhichIChose[i].getValue();
				}
	    	}
    	}
    	pCase.AssignedUsers__c = toFill;
    }
	
	private void SetProperties()
	{
		errorMessage = '';
		if (pCase.id == null)
		{
			pageTitle = 'עדכון חדש';
			pCase.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
		}
		else
			pageTitle = pCase.name;
		bFamily = pCase.FamilyName__c != null;
		bChild = pCase.ChildName__c != null;
		if (bFamily)
			pCase.WhoId__c = CObjectNames.Family;
		else if (bChild)
			pCase.WhoId__c = CObjectNames.FamilyMember;
		OnChangeWhoId();
	}
	
	public PageReference OnChangeWhoId()
	{
		bFamily = false;
		bChild = false;
		if (pCase.WhoId__c == null)
			pCase.WhoId__c = CObjectNames.FamilyMember;
		if (pCase.WhoId__c == CObjectNames.Family)	
		{	
			bFamily = true;
			bChild = false;
		}
		else if (pCase.WhoId__c == CObjectNames.FamilyMember)
		{
			bFamily = false;
			bChild = true;
		}
		return null;
	}
	
	public PageReference OnChangeProfessionType()
	{
		for (integer i = 0 ; i < pRequestsInCase.size(); i++)
		{
			pRequestsInCase[i].OnChangeProfessionType();
			pRequestsInCase[i].OnChangeTutorName();
		}
		return null;
	}
	
	public PageReference OnChangeTutorName()
	{
		try
		{
			integer index = integer.valueOf(System.currentPageReference().getParameters().get('tutorindex'));
			pRequestsInCase[index].OnChangeTutorName();
		}
		catch (Exception e) { }		
		return null;
	}
	
	public void OnPastWhoChose()
	{
		List<SelectOption> SelectOptionToPast = new List<SelectOption>();
		for (integer i = 0; i < OptionToSelectUsersWhichIDidNotChoose.size(); i++)
		{
			if (UsersIDidNotChoose.contains(OptionToSelectUsersWhichIDidNotChoose[i].getvalue()))
			{
				SelectOptionToPast.add(OptionToSelectUsersWhichIDidNotChoose[i]);
			}
		}
		for (integer i = 0; i < SelectOptionToPast.size(); i++)
		{
			OptionToSelectUsersWhichIChose.add(SelectOptionToPast[i]);
		}
		
		sendToRemoveFromWhichIDidNotChoose();
	}
	
	private void sendToRemoveFromWhichIDidNotChoose()
	{
		for (integer i = 0; i < OptionToSelectUsersWhichIDidNotChoose.size(); i++)
		{
			if (UsersIDidNotChoose.contains(OptionToSelectUsersWhichIDidNotChoose[i].getvalue()))
			{
				RemoveFromWhichIDidNotChoose(i);
			}
		}
	}
	
	private void RemoveFromWhichIDidNotChoose(integer i)
	{
		OptionToSelectUsersWhichIDidNotChoose.remove(i);
		sendToRemoveFromWhichIDidNotChoose();
	}
	
	public PageReference OnPastWhoIDidNotChoose()
	{
		List<SelectOption> SelectOptionToPast = new List<SelectOption>();
		for (integer i = 0; i < OptionToSelectUsersWhichIChose.size(); i++)
		{
			if (UsersIChoose.contains(OptionToSelectUsersWhichIChose[i].getvalue()))
			{
				SelectOptionToPast.add(OptionToSelectUsersWhichIChose[i]);
			}
		}
		for (integer i = 0; i < SelectOptionToPast.size(); i++)
		{
			OptionToSelectUsersWhichIDidNotChoose.add(SelectOptionToPast[i]);
		}
		sendToRemoveFromWhichIChose();
		
		pCase.AssignedUsers__c = UsersIChoose;
		return null;
	}
	
	private void sendToRemoveFromWhichIChose()
	{
		for (integer i = 0; i < OptionToSelectUsersWhichIChose.size(); i++)
		{
			if (UsersIChoose.contains(OptionToSelectUsersWhichIChose[i].getvalue()))
			{
				RemoveFromWhichIChose(i);
			}
		}
	}
	
	private void RemoveFromWhichIChose(integer i)
	{
		OptionToSelectUsersWhichIChose.remove(i);
		sendToRemoveFromWhichIChose();
	}
	
	private void CheckUsersIChoose()
	{
		List<SelectOption> SelectOptionToPast = new List<SelectOption>();
		for (integer i = 0; i < OptionToSelectUsersWhichIDidNotChoose.size(); i++)
		{
			if (UsersIChoose.contains(OptionToSelectUsersWhichIDidNotChoose[i].getvalue()))
			{
				SelectOptionToPast.add(OptionToSelectUsersWhichIDidNotChoose[i]);
			}
		}
		for (integer i = 0; i < SelectOptionToPast.size(); i++)
		{
			OptionToSelectUsersWhichIChose.add(SelectOptionToPast[i]);
		}
		
		sendToRemoveFromWhichIDidNotChoseByUsersIChoose();
	}
	
	private void sendToRemoveFromWhichIDidNotChoseByUsersIChoose()
	{
		for (integer i = 0; i < OptionToSelectUsersWhichIDidNotChoose.size(); i++)
		{
			if (UsersIChoose.contains(OptionToSelectUsersWhichIDidNotChoose[i].getvalue()))
			{
				RemoveFromWhichIDidNotChoseByUsersIChoose(i);
			}
		}
	}
	
	private void RemoveFromWhichIDidNotChoseByUsersIChoose(integer i)
	{
		OptionToSelectUsersWhichIDidNotChoose.remove(i);
		sendToRemoveFromWhichIDidNotChoseByUsersIChoose();
	}
	
	/*public PageReference OnAddRequestToList()
	{
		caseDb.OnAddRequestToList(tutorName);
		return null;
	}*/
	
	public void OnChangeFamilyOrChild()
	{
		if (pCase.WhoId__c == CObjectNames.Family)
			return;
		caseDb.FillChildInRequestsList();
	}
    
    public list<CObjectsHistory> pCaseHistories
    {
    	get
    	{
    		return caseDb.GetCaseHistories();
    	}
    }   
    
    public pagereference AddRequest()
	{
		caseDb.AddRequestToList();
		return null;
	}
	
	public void AddOrRemoveFromToMyAttention()
	{
		caseDb.AddOrRemoveFromToMyAttention();
	}
	
	public string printActivityApprovalsUrl{ get{ return CObjectNames.getBaseUrl() + page.ActivityApprovalsToPrint.getUrl() + '?caseID=' + pCase.id; } }
	public string StartMinute{ get{ return caseDb.StartMinute; } set{ caseDb.StartMinute = value; } }
	public string StartHour{ get{ return caseDb.StartHour; } set{ caseDb.StartHour = value; } }
	public string EndMinute{ get{ return caseDb.EndMinute; } set{ caseDb.EndMinute = value; } }
	public string EndHour{ get{ return caseDb.EndHour; } set{ caseDb.EndHour = value; } }
	
	
	public void tempUpdateDays()
	{
		DayInActivityReport__c[] daysInActivityReportList = [select name, id, ActivityReportName__c from DayInActivityReport__c];
		map<string, DayInActivityReport__c> daysInActivityReportMap = new map<string, DayInActivityReport__c>();
		for (DayInActivityReport__c dayInActivityReport : daysInActivityReportList)
		{
			daysInActivityReportMap.put(dayInActivityReport.ActivityReportName__c, dayInActivityReport);
		}
		daysInActivityReportList = daysInActivityReportMap.values();
		list<DayInActivityReport__c> listToUpdate = new list<DayInActivityReport__c>();
		
		for (DayInActivityReport__c dayInActivityReport : daysInActivityReportList)
		{
			listToUpdate.add(dayInActivityReport);
			if (listToUpdate.size() > 80)
			{
				CActivityReportDb.IsAfterPaymentOrChangeCoordinator = true;
				update listToUpdate;
				listToUpdate = new list<DayInActivityReport__c>();
			}
		}
		
		if (listToUpdate.size() > 0)
		{
			CActivityReportDb.IsAfterPaymentOrChangeCoordinator = true;
			update listToUpdate;
		}
	}
	
	public PageReference GoToCaseFullTreatmentPage()
	{
		return new PageReference(page.CaseFullTreatmentPage.getUrl() + '?caseId=' + pCase.id);
	}
	public string printLayout{ get{ return CObjectNames.getBaseUrl() + page.CasePrintLayout.getUrl() + '?ID=' + pCase.id; } }
	
}