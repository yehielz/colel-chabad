@isTest
private class Test_CPayment {
	
	static testMethod void testDeleteSpecialActivityFromPayment() {
        Children__c child = new Children__c(Name = 'TEstCHildren');
        insert child;
        Payment__c payment = new Payment__c();
        insert payment;
        SpecialActivities__c activity = new  SpecialActivities__c(Name = 'TEstActivity');
        insert activity;
        SpecialActivitiesLine__c special = new SpecialActivitiesLine__c(ChildName__c = child.Id,
        																		      PaymentName__c = payment.Id,
        																		      PaymentAmount__c = 100.00,
        																		      SpecialActivitiesName__c = activity.Id);
        insert special;
        delete special;
        
    }
	
    static testMethod void testCPaymentPrintCtrl() {
		Payment__c payment1 = new Payment__c();
        insert payment1;
        Payment__c payment2 = new Payment__c();
        insert payment2;
		Pagereference p = new Pagereference('/apex/PaymentPrint?ids=' + payment1.Id + '-' + payment2.Id);
		Test.setCurrentPage(p);
		Test.startTest();
		CPaymentPrintCtrl ctrl = new CPaymentPrintCtrl();
		Test.stopTest();
		
    }
}