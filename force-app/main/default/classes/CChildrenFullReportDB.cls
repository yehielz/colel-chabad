public with sharing class CChildrenFullReportDB 
{
	public list<CChildFullReportFacad> pChildren { get; set; }
	//pView = case, request, approval || all
	private map<string, boolean> pView { get; set; }
	private map<string, boolean> pYearsToView { get; set; }
	
	public CChildrenFullReportDB()
	{	
		SetView();
		SetYearsToView();
		SetChildren();
	}
	
	private void SetView()
	{
		pView = new map<string, boolean>();
		string view = Apexpages.currentPage().getParameters().get('view');
		if (view != null && view != '')
		{
			string[] keys = view.split('-');
			for (string key : keys)
			{
				pView.put(key, true);
			}
		}
		if (pView.size() == 0)
			pView.put('all', true);
	}
	
	private void SetYearsToView()
	{
		pYearsToView = new map<string, boolean>();
		string view = Apexpages.currentPage().getParameters().get('years');
		if (view != null && view != '')
		{
			string[] keys = view.split(';');
			for (string key : keys)
			{
				pYearsToView.put(key, true);
			}
		}
		if (pYearsToView.size() == 0)
			pYearsToView.put('all', true);
	}
	
	private void SetChildren()
	{
		list<string> childrenIds = GetChildrenIds();
		pChildren = new list<CChildFullReportFacad>();
		if (childrenIds.size() > 0)
		{
			list<Children__c> children = CChildDb.GetChildrenListByIds(childrenIds);
			map<string, Children__c> childrenMap = GetMapFromList(children);
			map<string, map<string, CSchoolYearInFullReport>> yearsMapsByChildren = GetYearsListByChild(childrenIds, childrenMAp);
			map<string, ChildRegistration__c> registrationsByChildren = GetChildrenRegistrationByChildren(childrenIds);
			for (Children__c child : children)
			{
				list<CSchoolYearInFullReport> yearsList = new list<CSchoolYearInFullReport>();
				if (yearsMapsByChildren.containsKey(child.id))
					yearsList = yearsMapsByChildren.get(child.id).values();
				ChildRegistration__c registration = new ChildRegistration__c();
				if (registrationsByChildren.containsKey(child.id))
					registration = registrationsByChildren.get(child.id);
				pChildren.add(new CChildFullReportFacad(child, yearsList, pView, pYearsToView, registration));
			}
		}
	}
    
    public static map<string, Children__c> GetMapFromList(list<Children__c> theList)
    {
    	map<string, Children__c> ret = new map<string, Children__c>();
    	for (Children__c item : theList)
    	{
    		ret.put(item.id, item);
    	}
    	return ret;
    }
	
	private list<string> GetChildrenIds()
	{
		list<string> ret = new list<string>();
		string childrenString = Apexpages.currentPage().getParameters().get('children');
		childrenString = childrenString != null ? childrenString.trim() : '';
		if (childrenString != null && childrenString != '')
		{
			ret.addAll(childrenString.split('-'));
		}
		return ret;
	}
	
	private map<string, map<string, CSchoolYearInFullReport>> GetYearsListByChild(list<string> childrenIds, map<string, Children__c> childrenMap)
	{
		map<string, map<string, CSchoolYearInFullReport>> ret = GetYearsWithChildProgress(childrenIds, childrenMap);
		list<ActivityApproval__c> activityApprovals = CActivityApprovalDb.GetActivityApprovalsByChildren(childrenIds);
		map<string, list<ActivityReport__c>> activityReportsByApproval = GetActivityReportsByApproval(childrenIds);	
		map<string, string> casesId = new map<string, string>();
		map<string, string> requestsId = new map<string, string>();	
		map<string, string> professionsId = new map<string, string>();	
		for (ActivityApproval__c approval: activityApprovals)
		{
			string year = approval.FromDate__c < date.newInstance(approval.FromDate__c.year(), 9, 1) ? ((approval.FromDate__c.year() - 1) + '-' + 
						  approval.FromDate__c.year()) : (approval.FromDate__c.year() + '-' + (approval.FromDate__c.year() + 1));
			if (pYearsToView.containsKey('all') || pYearsToView.containsKey(year))
			{
				casesId.put(approval.CaseName__c, approval.CaseName__c);
				requestsId.put(approval.RequestForTreatmentName__c, approval.RequestForTreatmentName__c);
				professionsId.put(approval.ProfessionName__c, approval.ProfessionName__c);
				CActivityApprovalInFullReport cApproval = GetFullApprovalFromApproval(approval, activityReportsByApproval);
				
				if (ret.containsKey(approval.ChildName__c))
				{
					if (ret.get(approval.ChildName__c).containskey(year))
						ret.get(approval.ChildName__c).get(year).AddApproval(cApproval);
					else
						ret.get(approval.ChildName__c).put(year, new CSchoolYearInFullReport(childrenMap.get(approval.ChildName__c), year, cApproval, pView));
				}
				else
				{
					map<string, CSchoolYearInFullReport> yearsMap = new map<string, CSchoolYearInFullReport>();
					yearsMap.put(year, new CSchoolYearInFullReport(childrenMap.get(approval.ChildName__c), year, cApproval, pView));
					ret.put(approval.ChildName__c, yearsMap);
				}
			}
		}
		SetDetailsToYearsList(childrenIds, ret, casesId.values(), requestsId.values(), professionsId.values());
		return ret;
	}
	
	private map<string, map<string, CSchoolYearInFullReport>> GetYearsWithChildProgress(list<string> childrenIds, map<string, Children__c> childrenMap)
	{
		map<string, map<string, CSchoolYearInFullReport>> ret = new map<string, map<string, CSchoolYearInFullReport>>();
		if (pView.containsKey('all') || pView.containsKey('progress'))
		{
			list<ChildProgress__c> childProgresses = CChildProgressDb.GetChildrenProgressByChildrenAndDate(childrenIds);
			for (ChildProgress__c progress : childProgresses)
			{
				string year = progress.Date__c < date.newInstance(progress.Date__c.year(), 9, 1) ? ((progress.Date__c.year() - 1) + '-' + progress.Date__c.year()) : 
								(progress.Date__c.year() + '-' + (progress.Date__c.year() + 1));
				if (pYearsToView.containsKey('all') || pYearsToView.containsKey(year))
				{
					CChildProgressInFullReport cProgress = new CChildProgressInFullReport(progress, pView);
					if (ret.containsKey(progress.ChildName__c))
					{
						if (ret.get(progress.ChildName__c).containskey(year))
							ret.get(progress.ChildName__c).get(year).AddProgress(cProgress);
						else
							ret.get(progress.ChildName__c).put(year, new CSchoolYearInFullReport(childrenMap.get(progress.ChildName__c), year, cProgress, pView));
					}
					else
					{
						map<string, CSchoolYearInFullReport> yearsMap = new map<string, CSchoolYearInFullReport>();
						yearsMap.put(year, new CSchoolYearInFullReport(childrenMap.get(progress.ChildName__c), year, cProgress, pView));
						ret.put(progress.ChildName__c, yearsMap);
					}
				}
			}
		}
		return ret;
	}
	
	private void SetDetailsToYearsList(list<string> childrenIds, map<string, map<string, CSchoolYearInFullReport>> yearsByChildMap, list<string> casesId, list<string> requestsId, list<string> professionsId)
	{
		map<string, Case__c> casesMap = GetCasesMapByIds(casesId);
		map<string, RequestForTreatment__c> requestsMap = GetRequestsMapByIds(requestsId);
		map<string, map<string, list<DiplomaLine__c>>> diplomaLinesMap = GetDiplomaLinesMapByIds(childrenIds, professionsId);
		map<string, map<string, list<Exam__c>>> examsMap = GetExamsMapByIds(childrenIds, professionsId);
		map<string, Profession__c> professionsMap = GetProfessionsMapByIds(professionsId);
		
		list<map<string, CSchoolYearInFullReport>> yearsMaps = yearsByChildMap.values();
		for (map<string, CSchoolYearInFullReport> yearsMap : yearsMaps)
		{
			list<CSchoolYearInFullReport> yearsList = yearsMap.values();
			for (CSchoolYearInFullReport year : yearsList)
			{	
				map<string, list<DiplomaLine__c>> diplomaLines = new map<string, list<DiplomaLine__c>>();
				if (diplomaLinesMap.containsKey(year.pChild.id))
					diplomaLines = diplomaLinesMap.get(year.pChild.id);
				map<string, list<Exam__c>> exams = new map<string, list<Exam__c>>();
				if (examsMap.containsKey(year.pChild.id))
					exams = examsMap.get(year.pChild.id);
				year.SetDetails(professionsMap, casesMap, requestsMap, diplomaLines, exams);
			}
		}
	}
	
	private CActivityApprovalInFullReport GetFullApprovalFromApproval(ActivityApproval__c approval, map<string, list<ActivityReport__c>> activityReportsByApproval)
	{
		CActivityApprovalInFullReport ret = new CActivityApprovalInFullReport(approval, pView);
		if (activityReportsByApproval.containsKey(approval.id))
			ret.AddReports(activityReportsByApproval.get(approval.id));
		return ret;
	}
	
	private map<string, list<ActivityReport__c>> GetActivityReportsByApproval(list<string> childrenIds)
	{
		map<string, list<ActivityReport__c>> ret = new map<string, list<ActivityReport__c>>();
		list<ActivityReport__c> tempList = CActivityReportDb.GetActivityReportsByChildren(childrenIds);
		for (ActivityReport__c report: tempList)
		{
			if (ret.containsKey(report.ActivityApprovalName__c))
				ret.get(report.ActivityApprovalName__c).add(report);
			else
				ret.put(report.ActivityApprovalName__c, new list<ActivityReport__c> { report });
		}
		return ret;
	}
	
	public map<string, Case__c> GetCasesMapByIds(list<string> casesId)
	{
		map<string, Case__c> ret = new map<string, Case__c>();
		list<Case__c> casesList = CCaseDb.GetCasesById(casesId);
		for (Case__c c : casesList)
		{
			ret.put(c.id, c);
		}
		return ret;
	}
	
	public map<string, RequestForTreatment__c> GetRequestsMapByIds(list<string> requestsId)
	{
		map<string, RequestForTreatment__c> ret = new map<string, RequestForTreatment__c>();
		list<RequestForTreatment__c> requestsList = CRequestForTreatmentDb.GetRequestForTreatmentsById(requestsId);
		for (RequestForTreatment__c r : requestsList)
		{
			ret.put(r.id, r);
		}
		return ret;
	}
	
	private map<string, map<string, list<DiplomaLine__c>>> GetDiplomaLinesMapByIds(list<string> childrenIds, list<string> professionsId)
	{
		map<string, map<string, list<DiplomaLine__c>>> ret = new map<string, map<string, list<DiplomaLine__c>>>();
		list<DiplomaLine__c> tempList = CDiplomaLineDb.GetDiplomaLineByChildrenIdAndProfessions(childrenIds, professionsId);
		for (DiplomaLine__c line : tempList)
		{
			if (ret.containsKey(line.DiplomaName__r.ChildName__c))
			{
				if (ret.get(line.DiplomaName__r.ChildName__c).containsKey(line.ProfessionName__c))
					ret.get(line.DiplomaName__r.ChildName__c).get(line.ProfessionName__c).add(line);
				else
					ret.get(line.DiplomaName__r.ChildName__c).put(line.ProfessionName__c, new list<DiplomaLine__c> { line });
			}
			else
			{
				map<string, list<DiplomaLine__c>> lines = new map<string, list<DiplomaLine__c>>();
				lines.put(line.ProfessionName__c, new list<DiplomaLine__c> { line });
				ret.put(line.DiplomaName__r.ChildName__c, lines);
			}
		}
		return ret;
	}
	
	private map<string, map<string, list<Exam__c>>> GetExamsMapByIds(list<string> childrenIds, list<string> professionsId)
	{
		map<string, map<string, list<Exam__c>>> ret = new map<string, map<string, list<Exam__c>>>();
		list<Exam__c> tempList = CExamDb.GetExamByChildrenIdAndProfessions(childrenIds, professionsId);
		for (Exam__c line : tempList)
		{
			if (ret.containsKey(line.ChildName__c))
			{
				if (ret.get(line.ChildName__c).containsKey(line.ProfessionName__c))
					ret.get(line.ChildName__c).get(line.ProfessionName__c).add(line);
				else
					ret.get(line.ChildName__c).put(line.ProfessionName__c, new list<Exam__c> { line });
			}
			else
			{
				map<string, list<Exam__c>> lines = new map<string, list<Exam__c>>();
				lines.put(line.ProfessionName__c, new list<Exam__c> { line });
				ret.put(line.ChildName__c, lines);
			}
		}
		return ret;
	}
	
	public map<string, Profession__c> GetProfessionsMapByIds(list<string> professionsId)
	{
		map<string, Profession__c> ret = new map<string, Profession__c>();
		list<Profession__c> professionsList = CProfessionDb.GetProfessionsById(professionsId);
		for (Profession__c p : professionsList)
		{
			ret.put(p.id, p);
		}
		return ret;
	}
	
	private map<string, ChildRegistration__c> GetChildrenRegistrationByChildren(list<string> childrenIds)
	{
		map<string, ChildRegistration__c> ret = new map<string, ChildRegistration__c>();
		string activeYearId = CSchoolYearDb.GetActiveYearId();
		list<ChildRegistration__c> registrations = CChildRegistrationDb.GetChildRegistrationByChildrenAndYear(activeYearId, childrenIds);
		for (ChildRegistration__c registration : registrations)
		{
			ret.put(registration.ChildName__c, registration);
		}
		return ret;
	}
}