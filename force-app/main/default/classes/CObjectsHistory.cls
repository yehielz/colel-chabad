public with sharing class CObjectsHistory 
{
	private datetime CreatedDate;
	private string Field;
	private object oldValue;
	private object newValue;
	private string CreatedByName;
	
	private map<string, Schema.SObjectField> fieldsMap;
	
	public CObjectsHistory(string m_field, string m_createdByName, object m_oldValue, object m_newValue, datetime m_createdDate, map<string, Schema.SObjectField> m_fieldsMap)
	{
		fieldsMap = m_fieldsMap;
		CreatedDate = m_createdDate;
		Field = m_field;
		oldValue = m_oldValue;
		newValue = m_newValue;
		CreatedByName = m_createdByName;
	}
	
	public string pFieldLabel
	{
		get
		{
			if (Field == null)
				return '';
			if (fieldsMap.containsKey(Field))
				return fieldsMap.get(Field).getDescribe().getLabel();
			else
				return Field;
		}
	}
	
	public string pOldValue
	{
		get
		{
			return GetCurrentField(oldValue);
		}
	}
	
	public string pNewValue
	{
		get
		{
			return GetCurrentField(newValue);
		}
	}
	
	public string pCreatedDate
	{
		get
		{
			return CreatedDate.Hour() + ':' + CreatedDate.Minute() + ':' + CreatedDate.second() + ' ' + CreatedDate.day() + '/' + CreatedDate.month() + '/' + CreatedDate.year();
		}
	}
	
	public string pCreatedByName
	{
		get
		{
			return CreatedByName;
		}
	}
	
	private string GetCurrentField(object myfield)
	{
		if (fieldsMap.containsKey(Field))
		{
			if (myfield != null && string.valueOf(fieldsMap.get(Field).getDescribe().getSOAPType()).contains('BOOLEAN') && CObjectNames.booleansTranslate.containsKey(string.valueOf(myfield)))
				return CObjectNames.booleansTranslate.get(string.valueOf(myfield));
			else if (myfield != null && string.valueOf(fieldsMap.get(Field).getDescribe().getSOAPType()).contains('DATE'))
				return date.valueOf(myfield).day() + '/' + date.valueOf(myfield).month()
								 + '/' + date.valueOf(myfield).year();
			else if (myfield != null && string.valueOf(fieldsMap.get(Field).getDescribe().getSOAPType()).contains('DATETIME'))
				return datetime.valueOf(myfield).day() + '/' + datetime.valueOf(myfield).month()
					   + '/' + datetime.valueOf(myfield).year() + ' ' + datetime.valueOf(myfield).Hour() + ':' + 
					   datetime.valueOf(myfield).Minute() + ':' + datetime.valueOf(myfield).second();
		}
	   	return string.valueOf(myfield);
	}
}