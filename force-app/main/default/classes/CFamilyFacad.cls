public with sharing class CFamilyFacad 
{
    
    //public Children__c parent {get;set;}
    public string pageTitle { get; set; }
    public string errorMessage { get; set; }
    
    public boolean bDiedParentDad { get; set; }
    public boolean bDiedParentMom { get; set; }
    public boolean bBoth { get; set; }
    
    private CFamilyDb familyDb;
    
    public Family__c pFamily
    {
        get
        {
            return familyDb.GetpFamily();
        }
    }
        
    public CFamilyFacad(ApexPages.StandardController controller)
    {
        familyDb = new CFamilyDb(controller);
        errorMessage = '';
        if (pFamily.id == null)
            pageTitle = 'משפחה חדשה';
        else
            pageTitle = pFamily.name;
        //getFamilyParent();
        OnChangeDiedParent();
    }
    /*
    public void getFamilyParent(){
        if(pFamily.id != null){
            List<Children__c> lst = [select Id, Name, FamilyName__c, FirstName__c, Birthdate__c,
                        MaritalStatus__c, EnglishFirstName__c, IdNumber__c, FamilyManType__c, Picture__c
                         From Children__c where FamilyName__c  =:pFamily.Id and FamilyManType__c = 'הורה' limit 1];
            if(lst.size()>0)
                parent = lst[0];
            else parent = new Children__c(FamilyName__c = pFamily.Id,FamilyManType__c = CObjectNames.Parent );
        }
        else parent = new Children__c(FamilyName__c = pFamily.Id,FamilyManType__c = CObjectNames.Parent );
    }*/
    
    public PageReference OnChangeDiedParent()
    {
        if (pFamily.DiedParent__c == null)
        {
            pFamily.EnglishDiedParent__c = null;
            bBoth = false;
            bDiedParentDad = false;
            bDiedParentMom = false;
        }
        else if (pFamily.DiedParent__c == CObjectNames.FamilyDiedParentDad)
        {
            pFamily.EnglishDiedParent__c = CObjectNames.EnglishFamilyDiedParentDad;
            bBoth = true;
            bDiedParentDad = true;
            bDiedParentMom = false;
        }
        else if (pFamily.DiedParent__c == CObjectNames.FamilyDiedParentMother)
        {
            pFamily.EnglishDiedParent__c = CObjectNames.EnglishFamilyDiedParentMother;
            bBoth = true;
            bDiedParentDad = false;
            bDiedParentMom = true;
        }
        else if (pFamily.DiedParent__c == CObjectNames.FamilyDiedParentBoth)
        {
            pFamily.EnglishDiedParent__c = CObjectNames.EnglishFamilyDiedParentBoth;
            bBoth = true;
            bDiedParentDad = true;
            bDiedParentMom = true;
        }
        return null;
    }
    /*
    public void OnBirthDate()
    {
        if (test.isRunningTest())
            pChild.Birthdate__c = datetime.now().date();
        date Birthdate = pChild.Birthdate__c;
        if (pChild.IsAfterSunset__c)
            Birthdate = pChild.Birthdate__c.addDays(1);
        DateConverter dateConverter = new DateConverter();
        CMyDate dateH = dateConverter.civ2heb(Birthdate.day(), Birthdate.month(), Birthdate.year());
        setDateTopChildHebrewDate(dateH, dateConverter);
    }
    */
    public PageReference save()
    {
        //retrieve parent info on Family
        //should be removed but we can remove the parent fields from the family so we have to keep.
        /*pFamily.ParentName__c = parent.FirstName__c;
        pFamily.ParentBirthdate__c = parent.Birthdate__c;
        pFamily.MaritalStatus__c = parent.MaritalStatus__c;
        pFamily.EnglishParentName__c = parent.EnglishFirstName__c;
        pFamily.ParentId__c = parent.IdNumber__c;
        pFamily.ParentPicture__c = parent.Picture__c ;
        */
        // if (!Validation())
        //     return null;
        
        System.debug('Hello World 9');
        try{
            familyDb.save();
        }
        catch(DMLException ex){
            // System.debug('=====================> ' + eX.getMessage());
            System.debug('Hello World 11');
            String myException = ex.getdmlMessage(0);
            String myString2 = 'duplicate value found: UserName__c duplicates value on record with id';
            Boolean result = myException.contains(myString2);
            if(result){
                myException = 'השם משתמש הזה כבר בשימוש';
            }
            
            ApexPages.addMessage(new ApexPages.Message(Apexpages.Severity.WARNING, myException ));
            // ApexPages.addMessage(new ApexPages.Message(Apexpages.Severity.WARNING, ex.getdmlMessage(0) ));
			
                
            return null;
        }
        catch(Exception ex){
            // System.debug('=====================> ' + eX.getMessage());
            System.debug('Hello World 10');
            ApexPages.addMessage(new ApexPages.Message(Apexpages.Severity.WARNING, ex.getdmlMessage(0) ));
			
                
            return null;
        }
        
        //upsert parent;
        PageReference newCasePage = new ApexPages.StandardController(pFamily).view();
        return newCasePage;
    }
    
    private boolean Validation()
    {
        boolean ret = true;
        CFamilyValidator validator = new CFamilyValidator(this);
        ret = validator.Validate();
        if (!ret)
            errorMessage = validator.ErrorMessage;
        
        return ret;
    }
    
    public PageReference OnNewFamily()
    {
        Schema.DescribeSObjectResult fR = Family__c.sObjectType.getDescribe();
        Pagereference newFamilyPage = new Pagereference ('/' + fR.getKeyPrefix() + '/e');
        return newFamilyPage; 
    }
    
    public PageReference TranslationReasonOfDeath()
    {
        if (CObjectNames.FamilyResonOfDeathTranslate().containsKey(pFamily.DeathCategory__c))
            pFamily.EnglishFatherDeathCategory__c = CObjectNames.FamilyResonOfDeathTranslate().get(pFamily.DeathCategory__c);
        else
            pFamily.EnglishFatherDeathCategory__c = NULL;
        if (CObjectNames.FamilyResonOfDeathTranslate().containsKey(pFamily.MotherDeathCategory__c))
            pFamily.EnglishMotherDeathCategory__c = CObjectNames.FamilyResonOfDeathTranslate().get(pFamily.MotherDeathCategory__c);
        else
            pFamily.EnglishMotherDeathCategory__c = NULL;
        return null; 
    }
    
    public PageReference TranslationFamilyState()
    {
        if (CObjectNames.FamilyStateTranslate().containsKey(pFamily.MaritalStatus__c))
            pFamily.EnglishMaritalStatus__c = CObjectNames.FamilyStateTranslate().get(pFamily.MaritalStatus__c);
        else
            pFamily.EnglishMaritalStatus__c = NULL;
        return null; 
    }
    
    public string printUrl{ get{ return CObjectNames.getBaseUrl() + page.FamilyRequestFormToPrint.getUrl() + '?ID=' + pFamily.id; } }
    
    public list<CObjectsHistory> pFamilyHistories
    {
        get
        {
            return familyDb.GetFamilyHistories();
        }
    }  
}