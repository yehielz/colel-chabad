public with sharing class CChildFullReportDb 
{
	public Children__c pChild { get; private set; }
	public ChildRegistration__c pChildRegistration { get; set; }
	public list<CSchoolYearInFullReport> pYearsList { get; set; }
	private map<string, boolean> pView { get; set; }
	private map<string, boolean> pYearsToView { get; set; }
	
	public CChildFullReportDb()
	{
		SetChild();
		SetView();
		SetYearsToView();
		SetYearsMap();
		SetChildRegistration();
	}
	
	public CChildFullReportDb(Children__c child)
	{
		pChild = child;
		SetView();
		pYearsToView = new map<string, boolean>();
		SetActiveYear();
		SetYearsMap();
		SetChildRegistration();
	}
	
	private void SetActiveYear()
	{
		SchoolYear__c activeYear = CSchoolYearDb.GetActiveYear();
		if (activeYear != null && activeYear.StartDate__c != null) {
			pYearsToView.put(activeYear.StartDate__c.year() + '-' + (activeYear.StartDate__c.year() + 1), true);
		}
		else {
			datetime toDay = datetime.now();
			pYearsToView.put(toDay < date.newInstance(toDay.year(), 9, 1) ? ((toDay.year() - 1) + '-' + toDay.year()) : (toDay.year() + '-' + (toDay.year() + 1)), true);
		}
	}
	
	public CChildFullReportDb(Children__c child, list<CSchoolYearInFullReport> yearsList, map<string, boolean> view, map<string, boolean> yearsToView, ChildRegistration__c childRegistration)
	{
		pChild = child;
		pView = view;
		pYearsToView = yearsToView;
		pYearsList = yearsList;
		pChildRegistration = childRegistration;
	}
	
	private void SetChild()
	{
		string childId = Apexpages.currentPage().getParameters().get('childId');
		if (childId != null && childId != '')
			pChild = CChildDb.GetChildById(childId);
		if (test.isRunningTest())
			pChild = CChildDb.GetAllChildren()[0];		
		if (pChild == null)
			pChild = new Children__c();
	}
	
	private void SetView()
	{
		pView = new map<string, boolean>();
		string view = Apexpages.currentPage().getParameters().get('view');
		if (view != null && view != '')
		{
			string[] keys = view.split('-');
			for (string key : keys)
			{
				pView.put(key, true);
			}
		}
		if (pView.size() == 0)
			pView.put('all', true);
	}
	
	private void SetYearsToView()
	{
		pYearsToView = new map<string, boolean>();
		string view = Apexpages.currentPage().getParameters().get('years');
		system.debug('view = ' + view);
		if (view != null && view != '')
		{
			string[] keys = view.split(';');
			for (string key : keys)
			{
				pYearsToView.put(key, true);
			}
		}
		if (pYearsToView.size() == 0)
			pYearsToView.put('all', true);
		system.debug('pYearsToView = ' + pYearsToView);
	}
	
	private void SetYearsMap()
	{
		pYearsList = new list<CSchoolYearInFullReport>();
		system.debug('pYearsList');
		if (pChild.id != null)
		{			
			map<string, CSchoolYearInFullReport> yearsMap = GetYearsWithChildProgress();
			list<ActivityApproval__c> activityApprovals = CActivityApprovalDb.GetActivityApprovalByChildId(pChild.id);
			map<string, string> casesId = new map<string, string>();
			map<string, string> requestsId = new map<string, string>();
			map<string, string> professionsId = new map<string, string>();
			map<string, list<ActivityReport__c>> activityReportsByApproval = GetActivityReportsByApproval();
			for (ActivityApproval__c approval: activityApprovals)
			{
				string year = approval.FromDate__c < date.newInstance(approval.FromDate__c.year(), 9, 1) ? ((approval.FromDate__c.year() - 1) + '-' + approval.FromDate__c.year()) : 
								(approval.FromDate__c.year() + '-' + (approval.FromDate__c.year() + 1));
				system.debug('year = ' + year);
				system.debug('pYearsToView = ' + pYearsToView);
				if (pYearsToView.containsKey('all') || pYearsToView.containsKey(year))
				{
					casesId.put(approval.CaseName__c, approval.CaseName__c);
					system.debug('casesId = ' + casesId);
				
					requestsId.put(approval.RequestForTreatmentName__c, approval.RequestForTreatmentName__c);
					professionsId.put(approval.ProfessionName__c, approval.ProfessionName__c);
					system.debug('professionsId = ' + professionsId);
				
					CActivityApprovalInFullReport cApproval = new CActivityApprovalInFullReport(approval, pView);
					if (activityReportsByApproval.containsKey(approval.id))
						cApproval.AddReports(activityReportsByApproval.get(approval.id));
					if (yearsMap.containskey(year))
						yearsMap.get(year).AddApproval(cApproval);
					else
						yearsMap.put(year, new CSchoolYearInFullReport(pChild, year, cApproval, pView));
				}
			}
			pYearsList = yearsMap.values();
			system.debug('pYearsList2');
			
			system.debug('casesId = ' + casesId);
			system.debug('requestsId = ' + requestsId);
			system.debug('professionsId = ' + professionsId);
			SetDetailsToYearsList(casesId.values(), requestsId.values(), professionsId.values());
			system.debug('SetDetailsToYearsList');
		}
	}
	
	private map<string, CSchoolYearInFullReport> GetYearsWithChildProgress()
	{
		map<string, CSchoolYearInFullReport> ret = new map<string, CSchoolYearInFullReport>();
		if (pView.containsKey('all') || pView.containsKey('progress'))
		{
			list<ChildProgress__c> childProgresses = CChildProgressDb.GetChildProgressByChildAndDate(pChild.id);
			for (ChildProgress__c progress: childProgresses)
			{
				string year = progress.Date__c < date.newInstance(progress.Date__c.year(), 9, 1) ? ((progress.Date__c.year() - 1) + '-' + progress.Date__c.year()) : 
								(progress.Date__c.year() + '-' + (progress.Date__c.year() + 1));
				if (pYearsToView.containsKey('all') || pYearsToView.containsKey(year))
				{
					CChildProgressInFullReport cProgress = new CChildProgressInFullReport(progress, pView);
					if (ret.containskey(year))
						ret.get(year).AddProgress(cProgress);
					else
						ret.put(year, new CSchoolYearInFullReport(pChild, year, cProgress, pView));
				}
			}
		}
		return ret;
	}
	
	private void SetDetailsToYearsList(list<string> casesId, list<string> requestsId, list<string> professionsId)
	{
		map<string, Case__c> casesMap = GetCasesMapByIds(casesId);
		map<string, RequestForTreatment__c> requestsMap = GetRequestsMapByIds(requestsId);
		map<string, list<DiplomaLine__c>> diplomaLinesMap = GetDiplomaLinesMapByIds(professionsId);
		map<string, list<Exam__c>> examsMap = GetExamsMapByIds(professionsId);
		map<string, Profession__c> professionsMap = GetProfessionsMapByIds(professionsId);
		for (CSchoolYearInFullReport year : pYearsList)
		{
			year.SetDetails(professionsMap, casesMap, requestsMap, diplomaLinesMap, examsMap);
		}
	}
	
	public map<string, Case__c> GetCasesMapByIds(list<string> casesId)
	{
		map<string, Case__c> ret = new map<string, Case__c>();
		list<Case__c> casesList = CCaseDb.GetCasesById(casesId);
		for (Case__c c : casesList)
		{
			ret.put(c.id, c);
		}
		return ret;
	}
	
	public map<string, RequestForTreatment__c> GetRequestsMapByIds(list<string> requestsId)
	{
		map<string, RequestForTreatment__c> ret = new map<string, RequestForTreatment__c>();
		list<RequestForTreatment__c> requestsList = CRequestForTreatmentDb.GetRequestForTreatmentsById(requestsId);
		for (RequestForTreatment__c r : requestsList)
		{
			ret.put(r.id, r);
		}
		return ret;
	}
	
	private map<string, list<DiplomaLine__c>> GetDiplomaLinesMapByIds(list<string> professionsId)
	{
		map<string, list<DiplomaLine__c>> ret = new map<string, list<DiplomaLine__c>>();
		if (pChild.id != null)
		{
			list<DiplomaLine__c> tempList = CDiplomaLineDb.GetDiplomaLineByChildIdAndProfessions(pChild.id, professionsId);
			for (DiplomaLine__c line: tempList)
			{
				if (ret.containsKey(line.ProfessionName__c))
					ret.get(line.ProfessionName__c).add(line);
				else
					ret.put(line.ProfessionName__c, new list<DiplomaLine__c> { line });
			}
		}
		return ret;
	}
	
	private map<string, list<Exam__c>> GetExamsMapByIds(list<string> professionsId)
	{
		map<string, list<Exam__c>> ret = new map<string, list<Exam__c>>();
		if (pChild.id != null)
		{
			list<Exam__c> tempList = CExamDb.GetExamByChildIdAndProfessions(pChild.id, professionsId);
			for (Exam__c line: tempList)
			{
				if (ret.containsKey(line.ProfessionName__c))
					ret.get(line.ProfessionName__c).add(line);
				else
					ret.put(line.ProfessionName__c, new list<Exam__c> { line });
			}
		}
		return ret;
	}
	
	public map<string, Profession__c> GetProfessionsMapByIds(list<string> professionsId)
	{
		map<string, Profession__c> ret = new map<string, Profession__c>();
		list<Profession__c> professionsList = CProfessionDb.GetProfessionsById(professionsId);
		for (Profession__c p : professionsList)
		{
			ret.put(p.id, p);
		}
		return ret;
	}
	
	private map<string, list<ActivityReport__c>> GetActivityReportsByApproval()
	{
		map<string, list<ActivityReport__c>> ret = new map<string, list<ActivityReport__c>>();
		if (pChild.id != null)
		{
			list<ActivityReport__c> tempList = CActivityReportDb.GetActivityReportsByChild(pChild.id);
			for (ActivityReport__c report: tempList)
			{
				if (ret.containsKey(report.ActivityApprovalName__c))
					ret.get(report.ActivityApprovalName__c).add(report);
				else
					ret.put(report.ActivityApprovalName__c, new list<ActivityReport__c> { report });
			}
		}
		return ret;
	}
	
	private void SetChildRegistration()
	{
		
		string activeYearId = CSchoolYearDb.GetActiveYearId();
		if (pChild.Id != null && activeYearId != null)
			pChildRegistration = CChildRegistrationDb.GetChildRegistrationByChildAndYear(activeYearId, pChild.Id);
		if (pChildRegistration == null)
			pChildRegistration = new ChildRegistration__c();
	}
}