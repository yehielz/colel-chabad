public with sharing class CDiplomaValidator 
{
	private CDiplomaFacad cDiplomaFacad { get; set; }
	public string ErrorMessage;
	
	public CDiplomaValidator(CDiplomaFacad ExtDiplomaFacad)
	{
		ErrorMessage = '';
		cDiplomaFacad = ExtDiplomaFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateChildName();
    	if (ret) 
	    	ret = ValidateHalf();
		if (ret) 
	    	ret = ValidateDate();
    	if (ret) 
	    	ret = ValidateSchoolYearName();
	    if (ret) 
	    	ret = ValidateDoubleProffession();
	  	    
	 	return ret;
	}
	
	private boolean ValidateSchoolYearName()
	{
		if (cDiplomaFacad.pDiploma.SchoolYearName__c == null)
		{
			ErrorMessage = CErrorMessages.DiplomaSchoolYearName;
			return false;
		}
		return true;
	}
	private boolean ValidateDoubleProffession()
	{
	    map<string, string> professionsNamesByIds = GetProfessionsNamesByIds();
		map<string, string> professionsMap = new map<string, string>();
		for(CDiplomaLineInList line : cDiplomaFacad.pDiplomaLines)
		{
			string professionId = line.pDiplomaLine.ProfessionName__c;
			if (professionId != null)
			{
				if (professionsMap.containsKey(professionId))
				{
					string professionName = professionsNamesByIds.containsKey(professionId) ? professionsNamesByIds.get(professionId) : '';
					ErrorMessage = CErrorMessages.DiplomaLineHasProfessionTwice(professionName);
					return false;
				}
				professionsMap.put(professionId, professionId);
			}
		}
		return true;
	}
	
	private map<string, string> GetProfessionsNamesByIds()
	{
		map<string, string> myMap = new map<string, string>();
		for(CDiplomaLineInList line : cDiplomaFacad.pDiplomaLines)
		{
			if (line.pDiplomaLine.ProfessionName__c != null)
				myMap.put(line.pDiplomaLine.ProfessionName__c, line.pDiplomaLine.ProfessionName__c);
		}
		list<Profession__c> professions = CProfessionDb.GetProfessionsById(myMap.values());
		myMap = new map<string, string>();
		for (Profession__c profession : professions)
		{
			myMap.put(profession.id, profession.name);
		}
		return myMap;
	}
	
	private boolean ValidateChildName()
	{
		if (cDiplomaFacad.pDiploma.ChildName__c == null)
		{
			ErrorMessage = CErrorMessages.DiplomaChildName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateHalf()
	{
		if (cDiplomaFacad.pDiploma.Half__c == null)
		{
			ErrorMessage = CErrorMessages.DiplomaHalf;
			return false;
		}
		return true;
	}
	
	private boolean ValidateDate()
	{
		if (cDiplomaFacad.pDiploma.Date__c == null)
		{
			ErrorMessage = CErrorMessages.DiplomaDate;
			return false;
		}
		return true;
	}
}