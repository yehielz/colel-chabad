@isTest
private class Test_CMasavFileFacad {
    
    @testSetup static void methodName() {
        SchoolYear__c schoolYear = new SchoolYear__c();
        schoolYear.name = '2011-2012';
        schoolYear.IsActive__c = true;
        schoolYear.StartDate__c = Date.today();
        schoolYear.HalfSchoolYear__c = system.toDay();
        schoolYear.EndOfSchoolYear__c = system.toDay().addDays(356);
        insert schoolYear;
        Profession__c p1 = new Profession__c(Name='אחר');
        insert p1;
        Profession__c p2 = new Profession__c(Name='Other');
        insert p2;
        Family__c f = new Family__c(Name = 'TEST');
        insert f;
        Children__c c = new Children__c (Name='TEST',FamilyName__c = f.Id,FamilyManType__c= 'ילד',Birthdate__c = Date.Today().addYears(-10));
        insert c;
        Case__c cas = new Case__c (ChildName__c = c.Id,WhoId__c = CObjectNames.FamilyMember);
        insert cas;
        RequestForTreatment__c requestForTreatment = new RequestForTreatment__c(CaseName__c = cas.Id,ChildName__c = c.Id);
        insert requestForTreatment;
        ActivityApproval__c  activityApproval = new ActivityApproval__c(FromDate__c = Date.Today(),ToDate__c = Date.Today().addDays(30) , RequestForTreatmentName__c = requestForTreatment.id,ChildName__c = c.Id);
        insert activityApproval;
        Tutor__c tutor = new Tutor__c();
        insert tutor;
        ActivityReport__c activityReport = new ActivityReport__c(ActivityReportStatus__c = 'TEST',ChildName__c = c.Id,TutorName__c = tutor.Id,ActivityApprovalName__c = activityApproval.Id);
        insert activityReport;
        Payment__c payment = new Payment__c();
        insert payment;
        DayInActivityReport__c day = new DayInActivityReport__c(ActivityReportName__c = activityReport.Id, Date__c = Date.TODAY());
        insert day;
        DayInActivityReport__c day2 = new DayInActivityReport__c(ActivityReportName__c = activityReport.Id, Date__c = Date.TODAY());
        insert day2;
        Exam__c exam = new Exam__c(ChildName__c = c.Id);
        insert exam;
        Diploma__c diploma = new Diploma__c(ChildName__c = c.Id);
        insert diploma;
        Coordinator__c coordinator = new Coordinator__c (FirstName__c = 'TEST', LastName__c = 'TEST' );
        insert coordinator ;
        
        Task t = new Task(Subject = 'TEST');
        insert t;
        
        SpecialActivities__c specialAct = new SpecialActivities__c();
        insert specialAct;
        
        SpecialActivitiesLine__c sp = new SpecialActivitiesLine__c (SpecialActivitiesName__c = specialAct.Id,ChildName__c = c.Id);
        
        insert sp;
        
        Support__c sup = new Support__c ();
        insert sup;
        ChildRegistration__c cr = new ChildRegistration__c (ChildName__c = c.Id,SchoolYearName__c = schoolYear.Id);
        insert cr;
        ExpenditureExceeded__c ex = new ExpenditureExceeded__c(ActivityReportName__c = activityReport.Id);
        insert ex;
        Check__c ch = new Check__c(PaymentName__c = payment.Id);
        insert ch;
        
        MasavSetting__c mMasavSetting = new MasavSetting__c (InstitutionSubject__c= 'TEST',NameOfInstitutionSubject__c = 'TET',InstitutionSends__c=34);
        insert mMasavSetting ;
        MasavFile__c file = new MasavFile__c(PaymentDate__c = Date.Today());
        insert file;
    }
    
    static testMethod void TestCMasavFileFacad() {
        MasavFile__c file = [select Id,Name,FromDate__c,ToDate__c from MasavFile__c limit 1];
        Apexpages.Standardcontroller con = new Apexpages.Standardcontroller(file);
        CMasavFileFacad facad = new CMasavFileFacad(con);
        facad.OnChangePage();
        facad.SetIsShowTable();
        facad.SetLinesByNextPage();
        facad.SetLinesByPreviousPage();
        facad.Save();
        String tmp = facad.mRetUrl;
        tmp = facad.pageTitle ;
        tmp = facad.pRetUrl ;
        List<CPaymentInMasavFileList> payments = facad.pPayments ;
        boolean first = facad.IsFirstPage;
        boolean last  = facad.IsLastPage;
        boolean show  = facad.IsShowTable;
        integer pagenum = facad.PageNumber;
        integer pagesize= facad.PageSize;
        
        
        
    }
}