public with sharing class CProfessionInfoInPrintPayment 
{
	public string pName { get; set; }
	public decimal pAmount { get; set; }
	public string sName{ get{ return CObjectNames.getHebrewString(pName); } }
	public string sAmount{ get{ return GetFormat(pAmount); } }
	
	public CProfessionInfoInPrintPayment(string name, decimal amount)
	{
		pName = name;
		pAmount = amount;
	}
	
	public string GetFormat(decimal num)
	{
		if (num != null)
		{
			string numStr = num.format();
			system.debug('numStr = '+ numStr);
			if (numStr.contains('.'))
			{
				string afterPoint = numStr.split('\\.')[1];
				system.debug('afterPoint = '+ afterPoint);
				if (afterPoint.length() < 2)
					return numStr + '0';
				else
					return numStr;
			}
			else
				return numStr + '.00';
		}
		return '0.00';
	}
}