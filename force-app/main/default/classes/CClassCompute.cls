public with sharing class CClassCompute
{
	public static string GetClassByAge(integer age)
	{
		string ret = '';
		if (age <= 5)
			ret = 'גן';
		if (age == 6)
			ret = 'א';
		if (age == 7)
			ret = 'ב';
		if (age == 8)
			ret = 'ג';
		if (age == 9)
			ret = 'ד';
		if (age == 10)
			ret = 'ה';
		if (age == 11)
			ret = 'ו';
		if (age == 12)
			ret = 'ז';
		if (age == 13)
			ret = 'ח';
		if (age == 14)
			ret = 'ט';
		if (age == 15)
			ret = 'י';
		if (age == 16)
			ret = 'יא';
		if (age == 17)
			ret = 'יב';
		if (age > 17)
			ret = 'על תיכונית';
		return ret;
	}
}