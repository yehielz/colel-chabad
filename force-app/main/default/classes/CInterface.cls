global without sharing class CInterface {

	public string[] GetMonthesByTutorOrFamilyId(string tutorOrFamilyId, Boolean isFamily){
		map<string, string> monthesList = GetMonthesByTutorOrFamily(tutorOrFamilyId, isFamily);
		return GetShortList(monthesList);
	}
	
	private list<string> GetShortList(map<string, string> monthesMap){
		list<string> ret = new list<string>();
		list<string> Lest3Monthes = GetLest3Monthes();
		for (integer i = 0; i < Lest3Monthes.size(); i++){
			if (monthesMap.containsKey(Lest3Monthes[i]))
				ret.add(Lest3Monthes[i]);
		}
		return ret;
	}
	
	private list<string> GetLest3Monthes(){
		list<string> ret = new list<string>();
		ret.add(datetime.now().Month() + '-' + datetime.now().year());
		ret.add(datetime.now().addMonths(-1).Month() + '-' + datetime.now().addMonths(-1).year());
		ret.add(datetime.now().addMonths(-2).Month() + '-' + datetime.now().addMonths(-2).year());

		return ret;
	}
	
	public map<string, CProfessionByChild> GetChildrenByMonth(string tutorOrFamilyId, string month, Boolean isFamily){
		map<string, list<ActivityApproval__c>> activityApprovalsByMonth = GetActivityApprovalsByMonth(tutorOrFamilyId, isFamily);
		if (activityApprovalsByMonth.containsKey(month)){
			return GetProfessionsByChildren(activityApprovalsByMonth.get(month), tutorOrFamilyId);
		}
		return null;
	}
	
	private map<string, CProfessionByChild> GetProfessionsByChildren(list<ActivityApproval__c> activityApprovalList, string tutorId){
		map<string, Profession__c> professionsMap = GetFrofessionsMap();
		map<string, Children__c> chidrenMap = GetChildrenMap(tutorId);
		map<string, CProfessionByChild> professionsByChildrenMap = new map<string, CProfessionByChild>();
		for (integer i = 0; i < activityApprovalList.size(); i++)
		{
			if (professionsByChildrenMap.containsKey(activityApprovalList[i].ChildName__c))
			{
				professionsByChildrenMap.get(activityApprovalList[i].ChildName__c).professions.put(
				activityApprovalList[i].ProfessionName__c, professionsMap.get(activityApprovalList[i].ProfessionName__c));
			}
			else
			{
				map<string, Profession__c> newProfessionMap = new map<string, Profession__c>();
				newProfessionMap.put(activityApprovalList[i].ProfessionName__c, professionsMap.get(activityApprovalList[i].ProfessionName__c));
				professionsByChildrenMap.put(activityApprovalList[i].ChildName__c, 
											new CProfessionByChild(chidrenMap.get(activityApprovalList[i].ChildName__c), newProfessionMap));
			}
		}
		return professionsByChildrenMap;
	}
	
	private map<string, Profession__c> GetFrofessionsMap()
	{
		Profession__c[] frofessions = [select name, id from Profession__c];
		map<string, Profession__c> ret = new map<string, Profession__c>();
		for (integer i = 0; i < frofessions.size(); i++)
		{
			ret.put(frofessions[i].id, frofessions[i]);
		}
		return ret;
	}
	
	private map<string, Children__c> GetChildrenMap(string tutorId)
	{
		//Tutor__c[] tutor = [select name, id from Tutor__c where id = :tutorId];
		Children__c[] children = [select name, id from Children__c /*where TutorsList__c like :('%' + tutor[0].name + '%')*/];
		map<string, Children__c> ret = new map<string, Children__c>();
		for (integer i = 0; i < children.size(); i++)
		{
			ret.put(children[i].id, children[i]);
		}
		return ret;
	}
	
	private map<string, List<ActivityApproval__c>> GetActivityApprovalsByMonth(string tutorOrFamilyId, Boolean withFamily)
	{
		List<ActivityApproval__c> activityApprovals = new List<ActivityApproval__c> ();
		if(withFamily){
			activityApprovals = [select name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, TutorName__r.name,
												   TutorName__c, ProfessionName__c 
												   FROM ActivityApproval__c 
												   WHERE (TutorName__c = :tutorOrFamilyId OR  ChildName__r.FamilyName__c = :tutorOrFamilyId)
												   AND IsApprovalOnProbation__c != true];
		}
		else {
			activityApprovals = [select name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, TutorName__r.name,
												   TutorName__c, ProfessionName__c 
												   FROM ActivityApproval__c 
												   WHERE (TutorName__c = :tutorOrFamilyId OR  ChildName__r.FamilyName__c = :tutorOrFamilyId)
												   AND IsApprovalOnProbation__c != true
												   AND TutorName__c != null];
		}
   		map<string, list<ActivityApproval__c>> ret = new map<string, list<ActivityApproval__c>>();
   		for (integer i = 0; i < activityApprovals.size(); i++)
   		{
   			string[] monthesList = GetMonthesListFromActivityApproval(activityApprovals[i]);
   			for (integer a = 0; a < monthesList.size(); a++)
   			{
   				if (ret.containsKey(monthesList[a]))
   					ret.get(monthesList[a]).add(activityApprovals[i]);
   				else
   					ret.put(monthesList[a], new list<ActivityApproval__c> { activityApprovals[i] });
   			}
   		}
   		return ret;
	}
	
	public map<string, string> GetMonthesByTutorOrFamily(string tutorOrFamilyId, Boolean withFamily){
		List<ActivityApproval__c> activityApprovals = new List<ActivityApproval__c> ();
		if(withFamily){
			activityApprovals = [SELECT name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, TutorName__r.name,
																TutorName__c, ProfessionName__c 
														FROM ActivityApproval__c 
														WHERE (TutorName__c = :tutorOrFamilyId OR  ChildName__r.FamilyName__c = :tutorOrFamilyId)
														AND IsApprovalOnProbation__c != true];
		}
		else{
			activityApprovals = [select name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, TutorName__r.name,
														TutorName__c, ProfessionName__c FROM ActivityApproval__c 
														WHERE (TutorName__c = :tutorOrFamilyId OR  ChildName__r.FamilyName__c = :tutorOrFamilyId)
														AND IsApprovalOnProbation__c != true 
														and TutorName__c != null];
		}
		
   		map<string, string> ret = new map<string, string>();
   		for (integer i = 0; i < activityApprovals.size(); i++){
   			string[] monthesList = GetMonthesListFromActivityApproval(activityApprovals[i]);
   			for (integer a = 0; a < monthesList.size(); a++){
   				ret.put(monthesList[a], monthesList[a]);
   			}
   		}
		   System.debug('ret : ' + ret);
   		return ret;
	}
	
	public string[] GetMonthesListFromActivityApprovalId(String approvalId){
		ActivityApproval__c approval = [select Id,FromDate__c,ToDate__c  From ActivityApproval__c where Id = :approvalId];
		return GetMonthesListFromActivityApproval(approval);
		
	}

	private string[] GetMonthesListFromActivityApproval(ActivityApproval__c activityApproval)
	{
		map<string, string> monthesList = new map<string, string>();
		integer month = activityApproval.FromDate__c.month(), year = activityApproval.FromDate__c.year();
		boolean isMonthSmall = true;
		while (isMonthSmall)
		{
			if (year < activityApproval.ToDate__c.year())
			{
				isMonthSmall = true;
				monthesList.put(month+'-'+year, month+'-'+year);
				if (month == 12)
				{
					year++;
					month = 0;
				}
				month++;
			}
			else if (month <= activityApproval.ToDate__c.month() && year == activityApproval.ToDate__c.year())
			{
				isMonthSmall = true;
				monthesList.put(month+'-'+year, month+'-'+year);
				if (month == 12)
				{
					year++;
					month = 0;
				}
				month++;
			}
			else
				isMonthSmall = false;
		}
		System.debug('monthesList.values() : ' + monthesList.values());
		return monthesList.values();
	}
	
	public list<ActivityApproval__c> GetActivityApprovalByMonthAndTutorAndChildAndProfession(string tutorId, string month, string childId, string professionId)
	{
		map<string, list<ActivityApproval__c>> activityApprovalsByMonth = GetActivityApprovalsByMonth(tutorId, childId, professionId);
		if (activityApprovalsByMonth.containsKey(month))
		{
			return activityApprovalsByMonth.get(month);
		}
		return null;
	}
	
	private map<string, List<ActivityApproval__c>> GetActivityApprovalsByMonth(string tutorId, string childId, string professionId)
	{
		String  myString = '';
		String  myTypePayment = '';
		SessionId__c sessionId = new SessionId__c();
		if (ApexPages.currentPage().getCookies().get('sessionId') != null){
			sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			System.debug('sessionId => ' + sessionId);
			if (sessionId != null && sessionId.ActivityApproval__c != null){
				ActivityApproval__c	approval = CActivityApprovalDb.GetActivityApprovalById(sessionId.ActivityApproval__c);
				// Id myTutor = [SELECT ID, TutorName__c, ProfessionName__r.ProfessionTypeName__r.PaymentType__c FROM ActivityApproval__c WHERE Id =: approval.id ].TutorName__c;
				ActivityApproval__c myActivity = [SELECT ID, TutorName__c, ProfessionName__r.ProfessionTypeName__r.PaymentType__c FROM ActivityApproval__c WHERE Id =: approval.id ];
				myString = String.valueOf(myActivity.TutorName__c);
				myTypePayment = String.valueOf(myActivity.ProfessionName__r.ProfessionTypeName__r.PaymentType__c);
			}
		}

		System.debug('tutorId => ' + tutorId);
		System.debug('myString => ' + myString);

		List<ActivityApproval__c> activityApprovals = new List<ActivityApproval__c>();
		if(myString == ''){
			activityApprovals = [select name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, TutorName__r.name,
												   TutorName__c, ProfessionName__c, ProfessionName__r.ProfessionTypeName__r.PaymentType__c 
												   FROM ActivityApproval__c 
												   WHERE /*TutorName__c = :tutorId 
												   AND*/ ChildName__c = :childId and ProfessionName__c = :professionId and IsApprovalOnProbation__c != true];
			if(activityApprovals[0].ProfessionName__r.ProfessionTypeName__r.PaymentType__c == 'שעות שבועיות' || activityApprovals[0].ProfessionName__r.ProfessionTypeName__r.PaymentType__c == 'מפגשים')									   
				CWebSiteReportFacad.setisWithDays(true);
			else
				CWebSiteReportFacad.setisWithDays(false);
		}
		else{
			activityApprovals = [select name, id, FromDate__c, ToDate__c, ChildName__c, ChildName__r.name, TutorName__r.name,
												   TutorName__c, ProfessionName__c, ProfessionName__r.ProfessionTypeName__r.PaymentType__c 
												   FROM ActivityApproval__c 
												   WHERE /*TutorName__c = :tutorId 
												   AND*/ ChildName__c = :childId and ProfessionName__c = :professionId and IsApprovalOnProbation__c != true AND TutorName__c =: myString];
			if(myTypePayment == 'שעות שבועיות' || myTypePayment == 'מפגשים')									   
				CWebSiteReportFacad.setisWithDays(true);
			else
				CWebSiteReportFacad.setisWithDays(false);
		}
   		map<string, list<ActivityApproval__c>> ret = new map<string, list<ActivityApproval__c>>();
   		for (integer i = 0; i < activityApprovals.size(); i++)
   		{
   			string[] monthesList = GetMonthesListFromActivityApproval(activityApprovals[i]);
   			for (integer a = 0; a < monthesList.size(); a++)
   			{
   				if (ret.containsKey(monthesList[a]))
   					ret.get(monthesList[a]).add(activityApprovals[i]);
   				else
   					ret.put(monthesList[a], new list<ActivityApproval__c> { activityApprovals[i] });
   			}
   		}
   		return ret;
	}
	
	public static boolean ValidateMonth()
	{
		if (ApexPages.currentPage().getParameters().get('month') != null)
			return true;
		else	
			return false;	
	}
}