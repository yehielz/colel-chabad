public with sharing class CSupportValidator 
{
	private CSupportFacad cSupportFacad { get; set; }
	public string ErrorMessage;
	
	public CSupportValidator(CSupportFacad ExtSupportFacad)
	{
		ErrorMessage = '';
		cSupportFacad = ExtSupportFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateName();
	    if (ret) 
	    	ret = ValidateUserName();
	  	    
	 	return ret;
	}
	
	private boolean ValidateName()
	{
		if (cSupportFacad.pSupport.FirstName__c == null && cSupportFacad.pSupport.LastName__c == null)
		{
			ErrorMessage = CErrorMessages.SupportLestOrFirstName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateUserName()
	{
		if (cSupportFacad.pSupport.UserName__c != null)
		{
			Support__c[] supporters = [select name, id from Support__c where UserName__c = :cSupportFacad.pSupport.UserName__c 
										and id != :cSupportFacad.pSupport.id];
			if (supporters.size() > 0)
			{
				ErrorMessage = CErrorMessages.TutorUserNameExist + 'תומך: ' + supporters[0].name;
				return false;
			}
			Tutor__c[] tutors = [select name, id from Tutor__c where UserName__c = :cSupportFacad.pSupport.UserName__c];
			if (tutors.size() > 0)
			{
				ErrorMessage = CErrorMessages.TutorUserNameExist + 'חונך: ' + tutors[0].name;
				return false;
			}
		}
		return true;
	}
}