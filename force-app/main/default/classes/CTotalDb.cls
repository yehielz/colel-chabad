public with sharing class CTotalDb
{
	private MasavFile__c pMasavFile {get; set;}
	private MasavSetting__c pMasavSetting {get; set;}
	private string pTotal {get; set;}
	
	public CTotalDb(){}
	
	public CTotalDb(MasavFile__c mMasavFile, MasavSetting__c mMasavSetting)
	{
		pMasavFile = mMasavFile;
		pMasavSetting = mMasavSetting;
	}
	
	public string GetTotal()
	{
		pTotal = '5';
		ValidateField(pMasavSetting.InstitutionSubject__c, 'מוסד/נושא', pMasavSetting.id, pMasavSetting.name, 'הגדרות מס"ב');
		pTotal += pMasavSetting.InstitutionSubject__c;
		pTotal += GetLen('', 2, '0');
		ValidateField(pMasavFile.PaymentDate__c, 'תאריך התשלום', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
		pTotal += GetDate(pMasavFile.PaymentDate__c);
		pTotal += GetLen('', 1, '0');
		pTotal += GetLen('1',3, '0');
		ValidateField(pMasavFile.TotalPayments__c, 'סכום התנועות', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
		pTotal += GetLen(GetNumbersAfterPoint(string.valueof(pMasavFile.TotalPayments__c)), 15, '0');
		pTotal += GetLen('', 15, '0');
		ValidateField(pMasavFile.NumberOfPayments__c, 'מספר התנועות', pMasavFile.id, pMasavFile.name, 'קובץ מס"ב');
		pTotal += GetLen(string.valueof(pMasavFile.NumberOfPayments__c),7, '0');
		pTotal += GetLen('', 7, '0');
		pTotal += GetLen('', 63, ' ');
		return pTotal;
	}
	
	private void ValidateField(object fieldValue, string fieldName, string onRecordId, string onRecordName, string onRecordType)
	{
		if ((fieldValue == null || string.valueOf(fieldValue) == 'null' || string.valueOf(fieldValue).trim() == '') && fieldName != null && onRecordId != null && onRecordName != null && onRecordType != null)
		{
			throw new CChesedException('ישנו שדה ריק!! (שם שדה: \'' + fieldName + '\', שם אובייקט: \'' + onRecordType + '\', שם רשומה: \'' + onRecordName + '\', מזהה רשומה: \'' + onRecordId + '\')');
		}
	}
	
	private string GetLen(string field, integer len, string letter)
	{
		for(integer i = 1; i <= len; i++)
		{
			if(field.length() < i)
				field = letter + field;
		}
		return field;
	}
	private string GetDate(date dateToday)
	{
		String sYear = String.valueof(dateToday.year());
		string y = sYear.substring(2);
		String sMonth = GetLen(String.valueof(dateToday.month()), 2, '0');
		String sDay = GetLen(String.valueof(dateToday.day()), 2, '0');
		string sToday = y + sMonth + sDay;
		return sToday;
	}
	
	private string GetNumbersAfterPoint(string field)
	{
		string[] slist = field.split('\\.');
        if (slist.Size() < 2)
         	return slist[0] + '00';
        else 
        {
            string secondPart = slist[1];
            if (secondPart.Length() < 2)
                return slist[0] + secondPart + '0';
            else
            	return slist[0] + secondPart;    
        }
	}
	
	/*private string SetSerialNum(string field)
	{
		string[] slist = field.split('\\-');
         	return slist[1];
	}*/
}