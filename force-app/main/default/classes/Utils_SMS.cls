public with sharing class Utils_SMS {

    @RemoteAction
    public static List<String>  sendSms(List<SMS_Request> smsMessages){
        Httprequest req = new Httprequest();
        HttpResponse res = new HttpResponse();
        Http http = new Http();
              
        req.setEndpoint('http://api.inforu.co.il/SendMessageXml.ashx');
        string body = 'InforuXML=';
        // System.debug('Here : ' + smsMessages.size());
        if (smsMessages.size() > 1) 
        	body += '<InforuRoot>';
 
		for(SMS_Request request : smsMessages){ 
 			if(request.send){
	 			String message = request.message;
	 			String phoneNumber = request.phoneNumber;
	 			//phoneNumber = '0546521411 YEHIEL;';
	        	body += '<Inforu><User><Username>ycmarton</Username><ApiToken>ad455e7b-95d5-4975-a307-a73b33ca7167</ApiToken></User><Content Type="sms"><Message>' + message +
	        			'</Message></Content><Recipients><PhoneNumber>'+ phoneNumber +'</PhoneNumber></Recipients><Settings><Sender>Chesed-MM</Sender></Settings></Inforu>';
 			}
 		}   
        if (smsMessages.size () > 1) 
        	body += '</InforuRoot>';
        // System.debug('Here is the body : ' + body);
        req.setBody(body);
        req.setHeader('Accept', 'application/xml');
        req.setMethod('POST');
        req.setHeader('Content-Length', '512'); 
        try {
            if (!test.isRunningTest())  
            res = http.send(req);
            List<String> results = parseResponse (res);
            return results ;
        }
        catch(Exception e){
            return new List<String> {'Error:' + e.getMessage()};
        }
        /*
        if(caseId.startsWith('500')){
            String Subject = '';
            Subject = ( content.length() > 40 ) ? content.substring(40) : content;
            Task sms = new Task(Description = content, WhatId = caseId, ActivityDate = Date.today(), Status = 'הסתיים' , Subject = 'מסרון');
            insert sms;
        }
         */   
        
    }
    
    
    private static List<String> parseResponse (Httpresponse res){
    	
    	List<String>  results = new List<String> ();
    	Dom.Document doc = new Dom.Document () ;
    	if(Test.isRunningTest())
    		doc.load ( '<TAG><Result><Status>1</Status ><Description>Message accepted successfully</Description><NumberOfRecipients>1</NumberOfRecipients></Result></TAG>');
    	else
    		doc.load ( '<TAG>' + res.getBody() + '</TAG>');
    	
    	//Retrieve the root element for this document.
        Dom.XMLNode response = doc.getRootElement();
 		system.debug( '\n\n\n  response   '+ response.getChildElements()  + '\n\n\n');
        for(Dom.XMLNode child : response.getChildElements()) {
        	String result = '';
        	for(Dom.XMLNode ch :child.getChildElements())
           		result += ch.getText() + ' ';
           	results.add(result);
        }

		system.debug( '\n\n\n'+ results  + '\n\n\n');
        
        return results;
    }
    
    
    public class SMS_Request{
    	public sObject record{get;set;}
    	public String phoneNumber {get;set;}
    	public String message {get;set;}
    	public String result {get;set;}
    	public boolean send {get;set;}
    }
    
    public static SMS_Request buildPaymentSMS (Payment__c payment){
    	
    	
    	Utils_SMS.SMS_Request req = new Utils_SMS.SMS_Request ();
            
        req.message = String.format(Label.PaymentSMS, 
                            new String []{ payment.FamilyName__r.ParentName__c + ' ' + payment.FamilyName__r.Name, 
                                String.valueOf(payment.AmountPaidByMasav__c),
                                         Date.today().format()  ,
                                        payment.NatureOfPayment__c,
                                         DateConverter.GetMonthInHebrew(Integer.valueOf( payment.Month__c ) - 1)  });
        req.phoneNumber = payment.FamilyName__r.MobilePhone__c;
        req.record = payment;
        req.send = false;
        return req;
           
    }
    
    public static SMS_Request buildApprovalSMS (ActivityApproval__c approval){
    	
    	
    	Utils_SMS.SMS_Request req = new Utils_SMS.SMS_Request();
    	req.phoneNumber = approval.ChildName__r.FamilyName__r.MobilePhone__c;
    	req.message = String.format(Label.Approval_SMS, new String []{ approval.ChildName__r.FamilyName__r.ParentName__c + ' ' + approval.ChildName__r.FamilyName__r.Name,
																							approval.ProfessionName__r.Name  ,
																							approval.ChildName__r.Name   	} ) ;
		req.record = approval;
		req.send = false;
		return req;
    }

    public static SMS_Request InfosConnexionsBySMS (ActivityApproval__c approval){
    	
    	
    	Utils_SMS.SMS_Request req = new Utils_SMS.SMS_Request();
    	req.phoneNumber = approval.TutorName__r.MobilePhone__c;
    	req.message = String.format(Label.Informations_Connexions_SMS, new String []{approval.ChildName__r.Name, approval.ProfessionName__r.Name, approval.ChildName__r.FamilyName__r.UserName__c, approval.ActivitySecretCode__c, '<a href="http://chesed.org.il/" >chesed.org.il</a>'} ) ;
		req.record = approval;
		req.send = true;
		return req;
    }
}