public with sharing class CCreateFileFacad
{
	public static boolean IsAfterUpdateFields {get; set;}
	private MasavFile__c pMasavFile {get; set;}
	private MasavSetting__c pMasavSetting {get; set;}
	public string pFile {get; set;}
	
	public CCreateFileFacad(string mfid)
	{
		SetMasavFiles(mfid);
		SetMasavSetting();
	}
	
	private void SetMasavFiles(string mfid)
	{
		if (mfid != null)
		{
			MasavFile__c[] masavFiles = [select id, name, TotalPayments__c, NumberOfPayments__c, PaymentDate__c, SerialNumber__c, FromDate__c, ToDate__c 
										 from MasavFile__c where id = :mfid];
			if (masavFiles.size() > 0)
				pMasavFile = masavFiles[0];
		}
		if (pMasavFile == null)
			pMasavFile = new MasavFile__c();
	}
	
	private void SetMasavSetting()
	{
		MasavSetting__c[] masavSetting = [select name, id, InstitutionSubject__c, NameOfInstitutionSubject__c, InstitutionSends__c from MasavSetting__c limit 1];
		if (masavSetting.size() > 0)
			pMasavSetting = masavSetting[0];
		if (pMasavSetting == null)
			pMasavSetting = new MasavSetting__c();
	}
	
	public string SetFile()
	{
		CTotalDb totalDb = new CTotalDb(pMasavFile, pMasavSetting);
		CFluctuationDb fluctuationDb = new CFluctuationDb(pMasavFile, pMasavSetting);
		CTitleDb titleDb = new CTitleDb(pMasavFile, pMasavSetting);
		pFile = titleDb.GetTitle() + '\r\n';
		pFile += fluctuationDb.GetLines() + '\r\n';
		pFile += totalDb.GetTotal() + '\r\n';
		pFile += '99999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999';
		return pFile;
	}
}