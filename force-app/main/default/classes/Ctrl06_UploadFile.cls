global with sharing class Ctrl06_UploadFile {
    public sObject record {get;set;}
    
    public Ctrl06_UploadFile(){
        
        record = Database.query('select Id,Name From ' + Apexpages.currentPage().getParameters().get('objectName')  + ' where Id = \'' + Apexpages.currentPage().getParameters().get('id')+ '\'');
    }
    
    @RemoteAction
    global static List<Attachment> getAttachments(String familyId){
        return [select Id, Name,ContentType,CreatedDate from Attachment where parentId =: familyId];
    }
    
    @RemoteAction
    global static void updateAttachedDocs(String objectId, String fileName){
    	if(fileName == 'טופס 1'){
	    	fileName = fileName + ';';
	    	Family__c f = [select Id,AttachedDocs__c from Family__c where Id =:objectId ];
	    	if(f.AttachedDocs__c == null ){
	    		f.AttachedDocs__c = fileName;
	    		update f;
	    	}
	    	else if( !f.AttachedDocs__c.contains(fileName)){
	    		f.AttachedDocs__c += fileName;
	    		update f;
	    	}
    	}
    	else if(fileName == 'תעודה' ){
    		Diploma__c d = [select Id,AttachedDiploma__c from Diploma__c where Id =:objectId ];
    		d.AttachedDiploma__c = true;
    		update d;
    	}
    }
    
    @RemoteAction
    global static List<Attachment> deleteFile(String fileId,String familyId){
    	
        List<Attachment> a = [select Id, Name,ContentType,CreatedDate from Attachment where Id = :fileId];
        delete a;
        return [select Id, Name,ContentType,CreatedDate from Attachment where parentId =: familyId];
    }
}