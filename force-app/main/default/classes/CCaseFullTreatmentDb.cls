public with sharing class CCaseFullTreatmentDb 
{
	public Case__c pCase { get; private set; }
	public list<CRequestForTreatmentInList> pRequestForTreatments { get; private set; }
	
	public CCaseFullTreatmentDb()
	{
		SetCase();
		SetRequestForTreatments();
	}
	
	private void SetCase()
	{
		if (Apexpages.currentPage().getParameters().get('caseId') != null && Apexpages.currentPage().getParameters().get('caseId') != '')
			pCase = CCaseDb.GetCaseById(Apexpages.currentPage().getParameters().get('caseId'));
		if (pCase == null)
			pCase = new Case__c();
	}
	
	private void SetRequestForTreatments()
	{
		pRequestForTreatments = new list<CRequestForTreatmentInList>();
		if (pCase.id != null)
		{
			RequestForTreatment__c[] requestForTreatments = CRequestForTreatmentDb.GetRequestsByCaseId(pCase.id);
			SetLastDiplomaDateByChild(requestForTreatments);
			map<string, list<ActivityApproval__c>> activityApprovals = GetActivityApprovalsByRequests(requestForTreatments);
			map<string, list<Note>> notes = GetNotesByRequests(requestForTreatments);
			for (RequestForTreatment__c request : requestForTreatments)
			{
				list<ActivityApproval__c> approvals = new list<ActivityApproval__c>();
				if (activityApprovals.containsKey(request.id))
					approvals = activityApprovals.get(request.id);
				list<Note> ns = new list<Note>();
				if (notes.containsKey(request.id))
					ns = notes.get(request.id);
				string lastDiplomaDate = '';
				system.debug('lastDiplomaDate 1 = ' + lastDiplomaDate);
				system.debug('pLastDiplomaDateByChild 1 = ' + pLastDiplomaDateByChild);
				system.debug('pLastDiplomaDateByChild.containsKey(request.ChildName__c) 1 = ' + pLastDiplomaDateByChild.containsKey(request.ChildName__c));
				if (pLastDiplomaDateByChild.containsKey(request.ChildName__c))
					lastDiplomaDate = pLastDiplomaDateByChild.get(request.ChildName__c);
				system.debug('lastDiplomaDate 2 = ' + lastDiplomaDate);
				pRequestForTreatments.add(new CRequestForTreatmentInList(request, approvals, ns, pProfessionTypeMap, pTutorLinesMap, pRequestForTreatments.size(), lastDiplomaDate));
			}
		}
	}
	
	private map<string, list<ActivityApproval__c>> GetActivityApprovalsByRequests(list<RequestForTreatment__c> requests)
	{
		ActivityApproval__c[] activityApprovals = [select id, TutorName__c, ProfessionName__c, ProfessionOther__c, RequestForTreatmentName__c, 
												   AmountApproved__c, HoursInSequence__c, SendEmailToTutor__c, SendEmailToFamily__c, Note__c, 
												   ChildName__c, CoordinatorUserName__c, TotalHours__c, AmountSpent__c, MinutesInSequence__c, 
												   PaymentTarget__c, FromDate__c, ToDate__c, WeeklyHours__c, CaseName__c, NoteToPrintPage__c,
							 					   name, Rate__c, PercentApproved__c, RequestForTreatmentName__r.ApprovalAmount__c, 
							 					   TotalHoursSpent__c from ActivityApproval__c where RequestForTreatmentName__c in :requests];
		map<string, list<ActivityApproval__c>> ret = new map<string, list<ActivityApproval__c>>();
		for (ActivityApproval__c approval : activityApprovals)
		{
			if (ret.containsKey(approval.RequestForTreatmentName__c))
				ret.get(approval.RequestForTreatmentName__c).add(approval);
			else
				ret.put(approval.RequestForTreatmentName__c, new list<ActivityApproval__c> { approval });
		}
		return ret;
	}
	
	private map<string, list<Note>> GetNotesByRequests(list<RequestForTreatment__c> requests)
	{
		Note[] notes = [select id, body, Title, IsPrivate, ParentId, CreatedById, LastModifiedDate from Note where ParentId in :requests];
		map<string, list<Note>> ret = new map<string, list<Note>>();
		for (Note n : notes)
		{
			if (ret.containsKey(n.ParentId))
				ret.get(n.ParentId).add(n);
			else
				ret.put(n.ParentId, new list<Note> { n });
		}
		return ret;
	}
	
	public void save()
	{
		list<Note> notesToInsert = new list<Note>();
		list<Note> notesToUpdate = new list<Note>();
		list<ActivityApproval__c> appeovalsToInsert = new list<ActivityApproval__c>();
		list<ActivityApproval__c> appeovalsToUpdate = new list<ActivityApproval__c>();
		for (CRequestForTreatmentInList request : pRequestForTreatments)
		{
			for (Note n : request.pNotes)
			{
				system.debug('n.Title = ' + n.Title);
				system.debug('n.body = ' + n.body);
				if (n.id == null)
				{
					system.debug('add to insert = = = = ' );
					notesToInsert.add(n);
				}
				else
				{
					system.debug('add to update = = = = ' );
					notesToUpdate.add(n);
				}
			}
			for (CActivityApprovalInList appeoval : request.pActivityApprovals)
			{
				if (appeoval.pActivityApproval.id == null)
					appeovalsToInsert.add(appeoval.pActivityApproval);
				else
					appeovalsToUpdate.add(appeoval.pActivityApproval);
			}
		}
		if (notesToInsert.size() > 0)
			insert notesToInsert;
		system.debug('notesToUpdate.size() = ' + notesToUpdate.size());
		if (notesToUpdate.size() > 0)
			update notesToUpdate;
		if (appeovalsToInsert.size() > 0)
			insert appeovalsToInsert;
		if (appeovalsToUpdate.size() > 0)
			update appeovalsToUpdate;
	}
	
	private map<string, ProfessionType__c> professionTypeMap;
 	public map<string, ProfessionType__c> pProfessionTypeMap
 	{
 		get
 		{
 			if (professionTypeMap == null)
 			{
 				ProfessionType__c[] professionTypes = [select name, id, PaymentType__c from ProfessionType__c];
 				professionTypeMap = new map<string, ProfessionType__c>();
 				for (ProfessionType__c professionType : professionTypes)
 				{
 					professionTypeMap.put(professionType.id, professionType);
 				}
 			}
 			return professionTypeMap;
 		}
 	}
 	
 	private map<string, TutorLine__c> tutorLinesMap;
 	public map<string, TutorLine__c> pTutorLinesMap
 	{
 		get
 		{
 			if (tutorLinesMap == null)
 			{
 				TutorLine__c[] lines = [select name, id, Rate__c, TutorName__c, ProfessionName__c from TutorLine__c];
 				tutorLinesMap = new map<string, TutorLine__c>();
 				for (TutorLine__c line : lines)
 				{
 					string s = line.TutorName__c + '-' +  line.ProfessionName__c;
 					tutorLinesMap.put(s, line);
 				}
 			}
 			return tutorLinesMap;
 		}
 	}
 	
 	private map<string, string> pLastDiplomaDateByChild;
    public void SetLastDiplomaDateByChild(list<RequestForTreatment__c> requests)
    {
    	pLastDiplomaDateByChild = new map<string, string>();
		Diploma__c[] diplomas = [select id, name, Date__c, ChildName__c from diploma__c where ChildName__c in :GetChildrenIdsList(requests) and Date__c != null order by Date__c desc];
		system.debug('diplomas.size() = ' + diplomas.size());
		for (Diploma__c diploma : diplomas)
		{
			if (!pLastDiplomaDateByChild.containsKey(diploma.ChildName__c))
			{
				string theDate = CObjectNames.GetFullNumberInString(diploma.Date__c.day()) + '/' + CObjectNames.GetFullNumberInString(diploma.Date__c.month()) + '/' + diploma.Date__c.year();
				pLastDiplomaDateByChild.put(diploma.ChildName__c, theDate);
			}
		}
    }
    
    private list<string> GetChildrenIdsList(list<RequestForTreatment__c> requests)
    {
    	map<string, string> ret = new map<string, string>();
    	for (RequestForTreatment__c request : requests)
		{
			if (request.ChildName__c != null)
				ret.put(request.ChildName__c, request.ChildName__c);
		}
		return ret.values();
    }
}