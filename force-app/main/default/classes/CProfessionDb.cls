public with sharing class CProfessionDb
{
    public static list<Profession__c> GetProfessions()
    {
        return [select name, ForDiploma__c, Group__c, MinRate__c, ProfessionTypeName__c, ProfessionTypeName__r.PaymentType__c from Profession__c order by name];
    }
    
    public static list<Profession__c> GetProfessionsInUse()
    {
        return [select name, ForDiploma__c, Group__c, MinRate__c, ProfessionTypeName__c, ProfessionTypeName__r.PaymentType__c from Profession__c 
        		where IsNotForUse__c != true order by name];
    }
    
    public static list<Profession__c> GetProfessionsById(string pID)
    {
        return [select name, ForDiploma__c, Group__c, MinRate__c, ProfessionTypeName__c, ProfessionTypeName__r.PaymentType__c from Profession__c 
        		where id = :pID  order by name];
    }
    
    public static list<Profession__c> GetProfessionsById(list<string> pIDs)
    {
        return [select name, ForDiploma__c, Group__c, MinRate__c, ProfessionTypeName__c, ProfessionTypeName__r.PaymentType__c from Profession__c 
        		where id in :pIDs  order by name];
    }
    
    public static Profession__c GetOtherProfession()
    {
        Profession__c[] professions = [select id, name, ProfessionTypeName__c from Profession__c where name = :CObjectNames.Other order by name];
        if (professions.size() > 0)
            return professions[0];
        return null;
    }
    
    public static list<Profession__c> GetProfessionsByType(string professionTypeId)
    {
    	return [select id, name, Group__c, ProfessionTypeName__c from Profession__c where ProfessionTypeName__c = :professionTypeId 
    			and name != :CObjectNames.Other order by name];
    }
    
    public static list<Profession__c> GetProfessionsByTypeAndForUse(string professionTypeId)
    {
    	return [select id, name, Group__c, ProfessionTypeName__c from Profession__c where ProfessionTypeName__c = :professionTypeId 
    			and name != :CObjectNames.Other and IsNotForUse__c != true order by name];
    }
    
    public static list<Profession__c> GetProfessionsForDiploma()
    {
        return [select name, ForDiploma__c, Group__c, MinRate__c, ProfessionTypeName__c from Profession__c 
        		where ForDiploma__c = :true order by name];
    }
    
    public static list<Profession__c> GetAllProfessionsList()
    {
    	return [select id, name, Group__c, ForDiploma__c, MinRate__c, ProfessionTypeName__c, ProfessionTypeName__r.PaymentType__c from Profession__c order by name];
    }
    
    public static map<string, Profession__c> GetAllProfessionsMap()
    {
    	list<Profession__c> professions = CProfessionDb.GetAllProfessionsList();
    	map<string, Profession__c> ret = new map<string, Profession__c>();
    	for (Profession__c profession : professions)
    	{
    		ret.put(profession.id, profession);
    	}
    	return ret;
    }
}