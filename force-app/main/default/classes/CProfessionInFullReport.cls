public with sharing class CProfessionInFullReport 
{
	public Profession__c pProfession { get; set; }
	public list<CCaseInFullReport> pCases { get; set; }
	public list<CDiplomaLineInFullReport> pDiplomaLines { get; set; }
	public list<CExamInFullReport> pExams { get; set; }
	private map<string, boolean> pView { get; set; }
	
	public CProfessionInFullReport(Profession__c mProfession, map<string, boolean> view)
	{
		pProfession = mProfession;
		pCases = new list<CCaseInFullReport>();
		pDiplomaLines = new list<CDiplomaLineInFullReport>();
		pExams = new list<CExamInFullReport>();
		pView = view;
	}
	
	public CProfessionInFullReport(Profession__c mProfession, CCaseInFullReport mCase, map<string, boolean> view)
	{
		pProfession = mProfession;
		pCases = new list<CCaseInFullReport>();
		if (mCase != null)
		{
			pCases.add(mCase);
		}
		pDiplomaLines = new list<CDiplomaLineInFullReport>();
		pExams = new list<CExamInFullReport>();
		pView = view;
	}
	
	public CProfessionInFullReport(Profession__c mProfession, Case__c mCase, map<string, boolean> view)
	{
		pProfession = mProfession;
		pCases = new list<CCaseInFullReport>();
		if (mCase != null)
		{
			pCases.add(new CCaseInFullReport(mCase, view));
		}
		pDiplomaLines = new list<CDiplomaLineInFullReport>();
		pExams = new list<CExamInFullReport>();
		pView = view;
	}
	
	public CProfessionInFullReport(Profession__c mProfession, list<Case__c> mCases, map<string, boolean> view)
	{
		pProfession = mProfession;
		pCases = new list<CCaseInFullReport>();
		if (mCases != null)
		{
			for (Case__c request : mCases)
			{
				pCases.add(new CCaseInFullReport(request, view));
			}
		}
		pDiplomaLines = new list<CDiplomaLineInFullReport>();
		pExams = new list<CExamInFullReport>();
		pView = view;
	}
	
	public void AddCase(CCaseInFullReport c)
	{
		pCases.add(c);
	}
	
	public void AddCases(list<CCaseInFullReport> cases)
	{
		pCases.addAll(cases);
	}
	
	public void AddDiplomaLine(CDiplomaLineInFullReport dl)
	{
		pDiplomaLines.add(dl);
	}
	
	public void AddDiplomaLines(list<CDiplomaLineInFullReport> dls)
	{
		pDiplomaLines.addAll(dls);
	}
	
	public void AddDiplomaLines(list<DiplomaLine__c> dls)
	{
		for (DiplomaLine__c line : dls)
		{
			pDiplomaLines.add(new CDiplomaLineInFullReport(line, pView));
		}
	}
	
	public void AddExam(CExamInFullReport e)
	{
		pExams.add(e);
	}
	
	public void AddExams(list<CExamInFullReport> es)
	{
		pExams.addAll(es);
	}
	
	public void AddExams(list<Exam__c> es)
	{
		for (Exam__c line : es)
		{
			pExams.add(new CExamInFullReport(line, pView));
		}
	}
	
	public string pProfessionName{ get{ return CObjectNames.getHebrewString(pProfession.name); } }
}