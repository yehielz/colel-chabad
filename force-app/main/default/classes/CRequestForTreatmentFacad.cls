public with sharing class CRequestForTreatmentFacad 
{
	public string TuotrUrl
	{
		get
		{
			Schema.DescribeSObjectResult fR = Tutor__c.sObjectType.getDescribe();
        	return CObjectNames.getBaseUrl() + '/' + fR.getKeyPrefix() + '/e';
		}
	}
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	public string errorMessageProfession { get; set; }
	public List<SelectOption> OptionsFamilyChildren { get; set; }
	public CGeneralDataInCasePage pGeneralData { get{ return requestForTreatmentDb.pGeneralData; } }
	
	
	public boolean bApproved { get; set; }
	public boolean bNotApproved { get; set; }
	
	private boolean pbManager { get; set; }
	public boolean bManager
	{
		get
		{
			if(pbManager == null) 
			{
				pbManager = false;
				Profile[] userProfile = [select name, id from Profile where (name='מנהל מערכת' or name='מנהל המערכת' or name = 'מנהל מחלקה') and id=:System.Userinfo.getProfileId()];
				if(userProfile.size() > 0)
					pbManager = true;
			}
			
			return pbManager;
		}
	}	
	
	private void setApprovedOrNot()
	{
		if (pRequestForTreatment.RequestStatus__c == 'אושר' || pRequestForTreatment.RequestStatus__c == 'אושר חלקית')
		{
			bApproved = true;
			bNotApproved = false;
		}
		else
		{
			bNotApproved = true;
			bApproved = false;
		}
	}
	
	public List<selectOption> OptionsProfessionsList 
	{ 
		get
		{
			return requestForTreatmentDb.GetProfessionsList();
		}
	}
	public List<selectOption> professionTypesOptions 
	{ 
		get
		{
			return requestForTreatmentDb.GetProfessionTypesOptions();
		}
	}
	
		
	public boolean bFamilyChildren 
	{ 
		get
		{
			return !bChild;
		}
	}
	public boolean bChild 
	{ 
		get
		{
			if (pRequestCase != null && pRequestCase.WhoId__c == CObjectNames.FamilyMember)
				return true;
			return false;
		}
	}
	public boolean bWeeklyHours 
	{ 
		get
		{
			boolean ret = true;
	    	if(pGeneralData.pProfessionTypeMap.containsKey(pRequestForTreatment.ProfessionTypeName__c))
	    	{
	    		string paymentType = pGeneralData.pProfessionTypeMap.get(pRequestForTreatment.ProfessionTypeName__c).PaymentType__c;
	    		if (paymentType != CObjectNames.HoursPerWeek)
	    			ret = false;
	    	}
	    	else
	    		ret = false;
    		return ret;
		}
	}
	public boolean bMeetings
	{ 
		get
		{
			boolean ret = true;
	    	if(pGeneralData.pProfessionTypeMap.containsKey(pRequestForTreatment.ProfessionTypeName__c))
	    	{
	    		string paymentType = pGeneralData.pProfessionTypeMap.get(pRequestForTreatment.ProfessionTypeName__c).PaymentType__c;
	    		if (paymentType != CObjectNames.Meetings)
	    			ret = false;
	    	}
	    	else
	    		ret = false;
    		return ret;
		}
	}
	public boolean bApprovalAmount{ get{ return !bWeeklyHours && !bMeetings; } }
	public boolean bOther { get; set; }
	
	private CRequestForTreatmentDb requestForTreatmentDb;
	
	public RequestForTreatment__c pRequestForTreatment
	{
		get
		{
			return requestForTreatmentDb.GetpRequestForTreatment();
		}
	}
		
	public CRequestForTreatmentFacad(ApexPages.StandardController controller)
	{
		requestForTreatmentDb = new CRequestForTreatmentDb(controller);
		OptionsFamilyChildren = requestForTreatmentDb.GetOptionsFamilyChildren();
		errorMessage = '';
		errorMessageProfession = '';
		if (pRequestForTreatment.id == null)
			pageTitle = 'בקשה לפעילות חדשה';
		else
			pageTitle = pRequestForTreatment.name;
		SetProperties();
		setApprovedOrNot();
	}
	
	private void SetProperties()
	{
		if (pRequestForTreatment.id == null)
		{
			pRequestForTreatment.ProfessionType__c = CObjectNames.ProfessionGroupPrivateLesson;
			pRequestForTreatment.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
		}
		OnChangeProfessionType();
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		requestForTreatmentDb.save();
		PageReference newCasePage = new ApexPages.StandardController(pRequestForTreatment).view();
        return newCasePage;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CRequestForTreatmentValidator validator = new CRequestForTreatmentValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public PageReference OnChangeTutorName()
    {
    	//OptionsProfessionsList = requestForTreatmentDb.getProfessionsList();
    	OnChangeProfessionType();
    	return null;
    }
    
    public void OnChangeProfession()
    {
    	bOther = false;
    	if (pRequestForTreatment.ProfessionName__c != null)
    	{
    		Profession__c[] profession = [select name, id from Profession__c where id = :pRequestForTreatment.ProfessionName__c];
    		if (profession.size() > 0 && profession[0].name == CObjectNames.Other)
    		{
    			bOther = true;
    		}
    	}
    }
    
    public PageReference OnChangeProfessionType()
    {
    	if (OptionsProfessionsList.size() == 1 && OptionsProfessionsList[0].getLabel() == CObjectNames.Other)
			pRequestForTreatment.ProfessionName__c = CProfessionDb.GetOtherProfession().id;
		OnChangeProfession();
    	return null;
    }
    
    public PageReference Validate()
    {
    	if (!Validation())
    	{
    		Cookie erorrMessage = new Cookie('erorrMessage', 'false', null, -1, false);
    		Cookie erorrMessageRezolt = new Cookie('erorrMessageRezolt', errorMessage, null, -1, false);
    		ApexPages.currentPage().setCookies(new list<Cookie> { erorrMessage, erorrMessageRezolt } );
    	}
    	else
    	{
    		Cookie erorrMessage = new Cookie('erorrMessage', 'true', null, -1, false);
    		ApexPages.currentPage().setCookies(new list<Cookie> { erorrMessage } );
    	}
    	return null;
    }
    
    public list<CObjectsHistory> pRequestForTreatmentHistories
    {
    	get
    	{
    		return requestForTreatmentDb.GetRequestForTreatmentHistories();
    	}
    }   
    
    public Case__c pRequestCase
    {
    	get
    	{
    		return requestForTreatmentDb.pRequestCase;
    	}
    }
    
    public string GoToNextRequest()
    {
		if (pRequestForTreatment != null)
		{
			RequestForTreatment__c[] request = [select name, CaseName__c from RequestForTreatment__c where CaseName__c = :pRequestForTreatment.CaseName__c
							 				   and (CreatedDate > :pRequestForTreatment.CreatedDate and id != :pRequestForTreatment.id) and 
							 				   (RequestStatus__c = :CObjectNames.RequestForTreatmentStatusNew or 
							 				   RequestStatus__c = :CObjectNames.RequestForTreatmentStatusTreatment) order by CreatedDate limit 1];
			if (request.size() > 0)
				return '/' + request[0].id;
		}
    	return '';
    }
    public string nextRequestUrl{ get{ return GoToNextRequest(); } }
    public string printLayout{ get{ return CObjectNames.getBaseUrl() + page.RequestForTreatmentPrintLayout.getUrl() + '?ID=' + pRequestForTreatment.id; } }
    public string pLastDiplomaDate{ get{ return requestForTreatmentDb.pLastDiplomaDate; } }

}