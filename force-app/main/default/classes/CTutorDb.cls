public without sharing class CTutorDb 
{
	private Tutor__c pTutor { get; set; }
	private CTutorLineDb turorLineDb { get; set; }
    public string pSelectedProfessionTypes { get{ return turorLineDb.pSelectedProfessionTypes; } set{ turorLineDb.pSelectedProfessionTypes = value; } }
	
	public CTutorDb()
	{
		pTutor = new Tutor__c();
	} 
	
	public CTutorDb(ApexPages.StandardController controller)
	{
		pTutor = (Tutor__c)controller.getRecord();
		if (pTutor.id != null)
			SetpTutorBypTutorId(pTutor.id);
		turorLineDb = new CTutorLineDb(pTutor);
	}
	
	private void SetpTutorBypTutorId(string id)
	{
		pTutor = CTutorDb.GetTutorById(id);
	}
	
	public static boolean IsUserNameExist(string userName)
	{
		Tutor__c[] tutors = [select name, id from Tutor__c where UserName__c != null and UserName__c = :userName];
		if (tutors.size() > 0)
			return true;
		return false;
	}
	
	public static boolean IsUserNameAndPasswordExist(string userName, string password)
	{
		Tutor__c[] tutors = [select name, id from Tutor__c where UserName__c != null and UserName__c = :userName 
								 and Password__c != null and Password__c = :password];
		if (tutors.size() > 0)
			return true;
		return false;
	}
	
	public static Tutor__c GetTutorByLoginInf(string userName, string password)
	{
		Tutor__c[] tutors = [select name, id from Tutor__c where UserName__c != null and UserName__c = :userName and Password__c != null 
								   and Password__c = :password];
		if (tutors.size() > 0)
			return tutors[0];
		return null;
	}
	
	public static Tutor__c GetTutorById(string tId)
	{
		Tutor__c[] tutors = [select name, id, Comment__c, Education__c, Phone__c, MobilePhone__c, Email__c, ApartmentNumber__c, Password__c, City__c, ZipCode__c, 
  							 Street__c, CoordinatorName__c, ProfessionsList__c, LastName__c, UserName__c, FirstName__c, POB__c, EducationDescription__c,
  							 WhereTutorLearn__c, ProffesionType__c, TutorType__c, TutorStartDate__c, CurrentJob__c, TeachingIn__c, FieldOfStudy__c, WorkIn__c,
  							 PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c, ID__c,Terms_Agreement__c
  							 from Tutor__c where id = :tId];
  		if (tutors.size() > 0)
  			return tutors[0];
  		return null;
	}
	
	public static list<Tutor__c> GetTutorsList()
	{
		return [select name, id, Comment__c, Education__c, Phone__c, MobilePhone__c, Email__c, ApartmentNumber__c, Password__c, City__c, Street__c, 
				CoordinatorName__c, ProfessionsList__c, LastName__c, UserName__c, FirstName__c, POB__c, EducationDescription__c, WorkIn__c, 
				WhereTutorLearn__c, ProffesionType__c, TutorType__c, TutorStartDate__c, CurrentJob__c, TeachingIn__c, FieldOfStudy__c, ZipCode__c,
				PersonalID__c, AccountNumber__c, AffiliateNumber__c, BankCode__c, ID__c,Terms_Agreement__c from Tutor__c];
	}
	
	public static map<string, Tutor__c> GetTutorsMap()
	{
		Tutor__c[] tutors = CTutorDb.GetTutorsList();
  		map<string, Tutor__c> ret = new map<string, Tutor__c>();
  		for (integer i = 0; i < tutors.size(); i++)
  		{
  			ret.put(tutors[i].id, tutors[i]);
  		}
  		return ret;
	}
	
	public void save()
	{
		pTutor.name = pTutor.FirstName__c + ' ' + pTutor.LastName__c;
		pTutor.WorkIn__c = pTutor.TutorType__c == 'עובד/ת' ? pTutor.WorkIn__c : null;
		if (pTutor.id == null)
			insert pTutor;
		else
			update pTutor;
		turorLineDb.InsertTutorLines();
	}
	
	public Tutor__c GetpTutor()
	{
		return pTutor;
	}
	
	public list<selectOption> GetProfessionsTypeList()
	{
		list<selectOption> ret = new list<selectOption>();
		ret.add(new selectOption('all', 'הכל'));
		ProfessionType__c[] professionsType = [select id, name from ProfessionType__c];
		for (ProfessionType__c PType : professionsType)
		{
			ret.add(new selectOption(PType.id, PType.name));
		}
		return ret;
	}
	
	public List<TutorLine__c> GetpTutorLines()
	{
		return turorLineDb.GetTutorLines();
	}
	
	public CChildInTutor[] getChildrenByTutorName()
    {
    	string tutorName = '%' + pTutor.name + '%';
    	Children__c[] children = [select name, id from Children__c where TutorsList__c like :tutorName];
    	list<CChildInTutor> ret = new list<CChildInTutor>();
    	for (integer i = 0; i < children.size(); i++)
    	{
    		ret.add(new CChildInTutor(children[i]));
    	}
    	return ret; 
    }
    
    public void SetFilterdAndSortedList()
    {
    	turorLineDb.SetFilterdAndSortedList();
    }
}