public with sharing class CChildInTutor 
{
	public Children__c pChild { get; set; }
	public string childLink 
	{
		get
		{
			PageReference tutorViewPage = new ApexPages.StandardController(pChild).view();
			return CObjectNames.GetBaseUrl() + tutorViewPage.getUrl();
		}
	}
	
	public CChildInTutor(Children__c child)
	{
		pChild = child;
	}
}