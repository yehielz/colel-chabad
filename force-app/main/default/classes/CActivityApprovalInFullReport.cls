public with sharing class CActivityApprovalInFullReport 
{
	
	public ActivityApproval__c pActivityApproval { get; set; }
	public list<CActivityReportInChildFullReport> pActivityReports { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsApprovalsInView{ get { return pView.containsKey('all') || pView.containsKey('approval'); } }
	public string margin
	{ 
		get 
		{ 
			if (pView.containsKey('all'))
				return '80px';
			else
			{
				integer ret = 40;
				if (pView.containsKey('case'))
					ret += 20;
				if (pView.containsKey('rquest'))
					ret += 20;
				return ret + 'px';
			} 
			return '100px';
		} 
	}
	
	public CActivityApprovalInFullReport(ActivityApproval__c mActivityApproval, map<string, boolean> view)
	{
		pActivityApproval = mActivityApproval;
		pActivityReports = new list<CActivityReportInChildFullReport>();
		pView = view;
	}
	
	public void AddReports(list<ActivityReport__c> reports)
	{
		if (reports != null)
		{
			for (ActivityReport__c report : reports)
			{
				pActivityReports.add(new CActivityReportInChildFullReport(report, pView));
			}
		}
	}
	
	public string pActivityApprovalName{ get{ return pActivityApproval.name; } }
	public string WeeklyHours{ get{ return (pActivityApproval.WeeklyHours__c == null || pActivityApproval.WeeklyHours__c < 1) ? '' : 'שעות שבועיות שאושרו: ;' + string.valueOf(math.round(pActivityApproval.WeeklyHours__c)) + ' '; } }
	public string TotalHours{ get{ return (pActivityApproval.TotalHours__c == null || pActivityApproval.TotalHours__c < 1) ? '' : 'סה"כ שעות שאושרו: ;' + string.valueOf(math.round(pActivityApproval.TotalHours__c)) + ' '; } }
	public string AmountApproved{ get{ return (pActivityApproval.AmountApproved__c == null || pActivityApproval.AmountApproved__c < 1) ? '' : 'סכום שאושר: ' + string.valueOf(math.round(pActivityApproval.AmountApproved__c)) + ' ש"ח; '; } }
	public string Tutor{ get{ return pActivityApproval.TutorName__c == null ? '' : 'שם חונך: ' + pActivityApproval.TutorName__r.name + '; '; } }
	public string Rate{ get{ return (pActivityApproval.Rate__c == null || pActivityApproval.Rate__c < 1) ? '' : 'תעריף לשעת חונך: ' + string.valueOf(math.round(pActivityApproval.Rate__c)) + ' ש"ח לשעה; '; } }
	public string MeetingsNumber{ get{ return (pActivityApproval.MeetingsNumber__c == null || pActivityApproval.MeetingsNumber__c < 1) ? '' : 'מספר מפגשים שאושרו: ' + string.valueOf(pActivityApproval.MeetingsNumber__c) + ' ; '; } }

	public string approvalDetails
	{
		get
		{
			return GetHebrewWordsForApproval(AmountApproved + WeeklyHours + TotalHours + MeetingsNumber + Tutor + Rate, 85);
		}
	}
	
	public string AmountApprovedWS{ get{ return (pActivityApproval.AmountApproved__c == null || !(pActivityApproval.AmountApproved__c > 0)) ? '' : string.valueOf(math.round(pActivityApproval.AmountApproved__c)) + ' ש"ח'; } }
	public string WeeklyHoursWS{ get{ return (pActivityApproval.WeeklyHours__c == null || pActivityApproval.WeeklyHours__c < 1) ? '' : string.valueOf(math.round(pActivityApproval.WeeklyHours__c)); } }
	public string TotalHoursWS{ get{ return (pActivityApproval.TotalHours__c == null || pActivityApproval.TotalHours__c < 1) ? '' : string.valueOf(math.round(pActivityApproval.TotalHours__c)); } }
	public string MeetingsNumberWS{ get{ return (pActivityApproval.MeetingsNumber__c == null || pActivityApproval.MeetingsNumber__c < 1) ? '' : string.valueOf(pActivityApproval.MeetingsNumber__c); } }
	public string TutorWS{ get{ return pActivityApproval.TutorName__c == null ? '' : pActivityApproval.TutorName__r.name; } }
	public string RateWS{ get{ return (pActivityApproval.Rate__c == null || pActivityApproval.Rate__c < 1) ? '' : string.valueOf(math.round(pActivityApproval.Rate__c)) + ' ש"ח לשעה'; } }
	
    public string approvalDetailsWS
	{
		get
		{
			string mAmountApproved = AmountApprovedWS != '' ? '<span class="TitleInContent">סכום שאושר:</span> ' + AmountApprovedWS : '';
			string mWeeklyHours = WeeklyHoursWS != '' ? '<span class="TitleInContent">שעות שבועיות שאושרו:</span> ' + WeeklyHoursWS : '';
			string mTotalHours = TotalHoursWS != '' ? '<span class="TitleInContent">סה"כ שעות שאושרו:</span> ' + TotalHoursWS : '';
			string mMeetingsNumber = MeetingsNumberWS != '' ? '<span class="TitleInContent">מספר מפגשים שאושרו:</span> ' + MeetingsNumberWS : '';
			string mTutor = TutorWS != '' ? '<span class="TitleInContent">שם חונך:</span> ' + TutorWS : '';
			string mRate = RateWS != '' ? '<span class="TitleInContent">תעריף לשעת חונך:</span> ' + RateWS : '';
			string ret = mAmountApproved;
			ret += ret == '' ? mWeeklyHours : (mWeeklyHours != '' ? ' | ' + mWeeklyHours : '');
			ret += ret == '' ? mTotalHours : (mTotalHours != '' ? ' | ' + mTotalHours : '');
			ret += ret == '' ? mMeetingsNumber : (mMeetingsNumber != '' ? ' | ' + mMeetingsNumber : '');
			ret += ret == '' ? mTutor : (mTutor != '' ? ' | ' + mTutor : '');
			ret += ret == '' ? mRate : (mRate != '' ? ' | ' + mRate : '');
			return ret;
		}
	}
	
    public string approvalDetailsWSEN
	{
		get
		{
			string mAmountApproved = AmountApprovedWS != '' ? '<span class="TitleInContent">Amount Approved:</span> ' + AmountApprovedWS : '';
			string mWeeklyHours = WeeklyHoursWS != '' ? '<span class="TitleInContent">Weekly Hours Approved:</span> ' + WeeklyHoursWS : '';
			string mTotalHours = TotalHoursWS != '' ? '<span class="TitleInContent">Total Hours Approved:</span> ' + TotalHoursWS : '';
			string mMeetingsNumber = MeetingsNumberWS != '' ? '<span class="TitleInContent">Meetings Number Approved:</span> ' + MeetingsNumberWS : '';
			string mTutor = TutorWS != '' ? '<span class="TitleInContent">Tutor Name:</span> ' + TutorWS : '';
			string mRate = RateWS != '' ? '<span class="TitleInContent">Rate For Tutor Hour:</span> ' + RateWS : '';
			string ret = mAmountApproved;
			ret += ret == '' ? mWeeklyHours : (mWeeklyHours != '' ? ' | ' + mWeeklyHours : '');
			ret += ret == '' ? mTotalHours : (mTotalHours != '' ? ' | ' + mTotalHours : '');
			ret += ret == '' ? mMeetingsNumber : (mMeetingsNumber != '' ? ' | ' + mMeetingsNumber : '');
			ret += ret == '' ? mTutor : (mTutor != '' ? ' | ' + mTutor : '');
			ret += ret == '' ? mRate : (mRate != '' ? ' | ' + mRate : '');
			return ret;
		}
	}
    
    public static string GetHebrewWordsForApproval(string thenote, integer length)
	{
		string ret = '';
		if (thenote != null)
		{
			string note = ' ' + thenote;
			integer noteLength = note.length();
			for (integer i = (length - 14); i < note.length(); i=length)
			{
				integer fromInSubS = i;
				integer minus = 0;
				while(note.substring(i-minus, ((i-minus)+1)) != ' ')
					minus++;
				i = i - minus;
				ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
				if (fromInSubS == (length - 14))
					ret += '<span class="gray"> ' + CObjectNames.getHebrewString('אישור פעילות:') + '</span>';
				ret += '<br/>';
				note = note.substring(i, note.length());
			}
			ret += CObjectNames.getHebrewString(note);
			if (noteLength < (length - 13)) 
				ret+= '<span class="gray"> ' + CObjectNames.getHebrewString('אישור פעילות:') + '</span>';
		}
		return ret;
	}
}