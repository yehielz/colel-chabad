public with sharing class CActivityApprovalInPrintPage 
{
    public ActivityApproval__c pActivityApproval { get; set; }
    public string professionName
    { 
        get
        { 
            if (pActivityApproval.ProfessionName__c != null && pActivityApproval.ProfessionName__r.name != null)
            {
                if (pActivityApproval.ProfessionName__r.name == CObjectNames.Other)
                    return CObjectNames.getHebrewString(pActivityApproval.ProfessionOther__c != null ? pActivityApproval.ProfessionOther__c : '');
                return CObjectNames.getHebrewString(pActivityApproval.ProfessionName__r.name);
            }
            return ''; 
        } 
    }
    public string ScopeOrAmount
    {
        get
        {
            if (!IsHoursPerWeek)
                return CObjectNames.getHebrewString('סכום שאושר: ');
            return CObjectNames.getHebrewString('היקף: ');
        }
    }
    public string totalScopeOrAmount
    {
        get
        {
            if (!IsHoursPerWeek)
                return '₪ ' + CObjectNames.getHebrewString(pActivityApproval.AmountApproved__c != null ? string.valueOf(pActivityApproval.AmountApproved__c) : '') + ' ';
            return CObjectNames.getHebrewString((pActivityApproval.WeeklyHours__c != null ? string.valueOf(pActivityApproval.WeeklyHours__c) : '') + ' שעות');
        }
    }
    
    public string tutorName
    {
        get
        {
            if (pActivityApproval.TutorName__c != null && pActivityApproval.TutorName__r.name != null)
                return CObjectNames.getHebrewString(pActivityApproval.TutorName__r.name + ' ');
            return ''; 
        }
    }
    
    public string tutorPhone
    {
        get
        {
            if (pActivityApproval.TutorName__c != null && pActivityApproval.TutorName__r.MobilePhone__c != null)
                return CObjectNames.getHebrewString(pActivityApproval.TutorName__r.MobilePhone__c + ' ');
            return ''; 
        }
    }
    
    public string hoursInSequence
    {
        get
        {
            if (!IsHoursPerWeek)
                return '';
            return '(' + CObjectNames.getHebrewString('ברצף') + ' (' + CObjectNames.getHebrewString('דקות') + ' ' + 
                    (pActivityApproval.MinutesInSequence__c != null ? string.valueOf(pActivityApproval.MinutesInSequence__c.setScale(2)) : '') + ') ' + 
                    CObjectNames.getHebrewString('שעות') + ' ' + 
                    (pActivityApproval.HoursInSequence__c != null ? string.valueOf(pActivityApproval.HoursInSequence__c.setScale(2)) : '') + 
                    CObjectNames.getHebrewString('אך לא יותר מ-') + ') ';
        }
    }
    
    public string childName
    {
        get
        {
            if (pActivityApproval.ChildName__c != null && pActivityApproval.ChildName__r.name != null)
                return CObjectNames.getHebrewString(pActivityApproval.ChildName__r.name + ' ');
            return ''; 
        }
    }
    public string RateForHour
    { 
        get
        { 
            string ret = '.' + CObjectNames.getHebrewString('לשעה');
            if (pActivityApproval.Rate__c != null)
                ret += ' ₪ ' + pActivityApproval.Rate__c;
            return ret; 
        } 
    }
    
    public string activityFromDate
    { 
        get
        { 
            if (pActivityApproval.FromDate__c != null)
            {
                return CObjectNames.getHebrewString(pActivityApproval.FromDate__c.day() + '/' + pActivityApproval.FromDate__c.month() 
                                                        + '/' + pActivityApproval.FromDate__c.year());
            }
            return '';
        } 
    }
    
    public string activityToDate
    { 
        get
        { 
            if (pActivityApproval.ToDate__c != null)
            {
                return CObjectNames.getHebrewString(pActivityApproval.ToDate__c.day() + '/' + pActivityApproval.ToDate__c.month() 
                                                    + '/' + pActivityApproval.ToDate__c.year()); 
            }
            return '';
        } 
    }
    public string activityNote
    { 
        get
        { 
            return pActivityApproval.NoteToPrintPage__c != null ? GetNote(pActivityApproval.NoteToPrintPage__c) : '';
        } 
    }
    
    private string GetNote(string thenote)
    {
        string ret = '';
        if (thenote != null)
        {
            string note = ' ' + thenote;
            integer noteLength = note.length();
            for (integer i = 77; i < note.length(); i=85)
            {
                integer fromInSubS = i < 85 ? i: 85;
                integer minus = 0;
                while(note.substring(i-minus, ((i-minus)+1)) != ' ')
                    minus++;
                i = i - minus;
                ret += CObjectNames.getHebrewString(note.substring(i-((fromInSubS-minus)-1), i));
                if (fromInSubS == 77)
                    ret += ' ' + CObjectNames.getHebrewString('הערות: ');
                ret += '<br/>';
                note = note.substring(i, note.length());
            }
            ret += '.' + CObjectNames.getHebrewString(note);
            if (noteLength < 78)
                ret += ' ' + CObjectNames.getHebrewString('הערות: ');
        }
        return ret;
    }
    
    public CActivityApprovalInPrintPage(ActivityApproval__c activityApproval)
    {
        pActivityApproval = activityApproval;
    }
    
    public CActivityApprovalInPrintPage() { }
    
    public Boolean IsHoursPerWeek
    {
        get
        {
            string paymentType = '';
            if (pActivityApproval.ProfessionName__c != null && pActivityApproval.ProfessionName__r.name == CObjectNames.Other)
            {
                if (pActivityApproval.RequestForTreatmentName__c != null && pActivityApproval.RequestForTreatmentName__r.ProfessionTypeName__c != null)
                    paymentType = pActivityApproval.RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c;
            }
            else if (pActivityApproval.ProfessionName__c != null && pActivityApproval.ProfessionName__r.ProfessionTypeName__c != null)
                paymentType = pActivityApproval.ProfessionName__r.ProfessionTypeName__r.PaymentType__c;
            
            if (paymentType == CObjectNames.MonthlyPayment || paymentType == CObjectNames.OneTimePayment)
                return false;
            else
                return true;
        }
    }
}