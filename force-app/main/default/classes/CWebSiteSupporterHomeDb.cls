public with sharing class CWebSiteSupporterHomeDb 
{
	public Support__c pSupporter { get; set; }
	public list<string> pChildrenIds { get; set; }
	public integer pNumberOfTotalChildren { get; set; }
	public integer pNumberOfChildren { get; set; }
	public integer pNumberOfRequests { get; set; }
	public integer pNumberOfApprovedRequests { get; set; }
	public integer pNumberOfReports { get; set; }
	private date pYearSatrtDate { get; set; }
	private date pYearEndDate { get; set; }
	public boolean IsEnglish { get; set; }
    
    public CWebSiteSupporterHomeDb()
    {
    	SetFromSession();
    	SetIsEnglish();
    	SetYearDates();
    	SetChildrenIds();
    	SetNumberOfChildren();
    	SetNumberOfRequests();
    	SetNumberOfReports();
    }
    
    private void SetIsEnglish() 
    {
    	IsEnglish = false;
    	if (pSupporter != null && pSupporter.LanguageForWS__c != null && pSupporter.LanguageForWS__c.contains('English'))
    		IsEnglish = true;
		string lang = Apexpages.currentPage().getParameters().get('lang');
    	if (lang != null && lang == 'en')
    		IsEnglish = true;
    	if (lang != null && lang == 'he')
    		IsEnglish = false;
    }
    
    private void SetFromSession() 
    {
    	if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null)
			{
				pSupporter = CSupportDb.GetSupporterById(sessionId.SupporterName__c);
			}
		}
		if (pSupporter == null)
			pSupporter = new Support__c();
    }
    
    private void SetYearDates()
	{
		pYearSatrtDate = date.newInstance(datetime.now().year(), 1, 1);
		pYearEndDate = date.newInstance((datetime.now().year() + 1), 1, 0);
	}
    
    private void SetChildrenIds()
    {
    	list<AggregateResult> children = new list<AggregateResult>();
    	string year = CSchoolYearDb.GetActiveYearId();
    	children = [select ChildName__c child from ChildRegistration__c where SchoolYearName__c = :year group by ChildName__c];
    	pNumberOfTotalChildren = children.size();
    	if (!pSupporter.IsAllChildren__c)
    	{
    		children = [select ChildRegistrationName__r.ChildName__c child from ChildRegistrationInSupporter__c where SupporterName__c = :pSupporter.id 
    					and ChildRegistrationName__r.SchoolYearName__r.IsActive__c = true group by ChildRegistrationName__r.ChildName__c];
    	}
    	pChildrenIds = new list<string>();
    	for (AggregateResult child : children)
    	{
    		string childName = string.valueOf(child.get('child'));
    		if (childName != null && childName.trim() != 'null')
    			pChildrenIds.add(childName);
    	}
    }
    
    private void SetNumberOfChildren()
    {
    	pNumberOfChildren = pChildrenIds.size();
    }
    
    private void SetNumberOfRequests()
    {
		RequestForTreatment__c[] requests = [select name, id, RequestStatus__c, FromDate__c, ToDate__c from RequestForTreatment__c 
											 where ChildName__c in :pChildrenIds and ((FromDate__c >= :pYearSatrtDate and 
											 FromDate__c <= :pYearEndDate) or (ToDate__c >= :pYearSatrtDate and ToDate__c <= :pYearEndDate))];
		pNumberOfRequests = 0;
		pNumberOfApprovedRequests = 0; 
		for (RequestForTreatment__c request : requests)	
		{
			pNumberOfRequests++;
			if (request.RequestStatus__c == CObjectNames.RequestForTreatmentStatusApproved)
				pNumberOfApprovedRequests++;
		}
    }
    
    private void SetNumberOfReports()
    {
		list<string> yearMonths = GetYearMonthList();
		ActivityReport__c[] reports = [select name, id from ActivityReport__c where ChildName__c in :pChildrenIds and ReportMonth__c in :yearMonths];
		pNumberOfReports = reports.size();
    }
    
    private list<string> GetYearMonthList()
    {
    	list<string> ret = new list<string>();
    	for (date activeMonth = pYearSatrtDate; activeMonth <= pYearEndDate; activeMonth = activeMonth.addMonths(1))
    	{
    		ret.add(activeMonth.month() + '-' + activeMonth.year());
    	}
    	return ret; 
    }
}