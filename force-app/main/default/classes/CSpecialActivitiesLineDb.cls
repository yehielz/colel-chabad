public with sharing class CSpecialActivitiesLineDb 
{
	public static boolean IsDontUpdatePaymentInTrigger { get; set; }
	public CSpecialActivitiesLineDb()
	{
		
	}
	
	public boolean IsOnePaid { get; set; }
	public string upMark{ get{ return '˄'; } }
	public string downMark{ get{ return '˅'; } }
	
	public string currentMark { get; set; }
	private string oldField{ get; set; }
	private string newField{ get; set; }
	public string Field
	{ 
		get
		{
			return newField;
		}
		set
		{
			oldField = newField;
			newField = value;
		} 
	}
	
	private SpecialActivitiesLine__c pSpecialActivityLine { get; set; }
	private map<string, List<SpecialActivitiesLine__c>> specialActivitiesLinesByPayments;
	
	public CSpecialActivitiesLineDb(ApexPages.StandardController controller)
	{
		pSpecialActivityLine = (SpecialActivitiesLine__c)controller.getRecord();
		if (pSpecialActivityLine.id != null)
			SetpSpecialActivityLineById(pSpecialActivityLine.id);
	}
	
	private void SetpSpecialActivityLineById(string id)
	{
		pSpecialActivityLine = [select name, id, IsInActivity__c, FamilyName__c, SpecialActivitiesName__c, ChildOrFamily__c, Note__c, PaymentStatus__c ,
									ChildName__c, FamilyName__r.City__c, FamilyName__r.CoordinatorName__c, PaymentAmount__c,PaymentName__c, Contact__c,
									paid__c from SpecialActivitiesLine__c where id = :id];
	}
	
	public void InsertpSpecialActivityLine()
	{
		SetIsInActivity();
		SetChildOrFamily();
		//SetpSpecialActivityLineName();
		SetIsPaid();
		if (pSpecialActivityLine.id == null)
			insert pSpecialActivityLine;
		else
			update pSpecialActivityLine;	
	}
	
	private void SetIsPaid()
	{
		if (pSpecialActivityLine.PaymentName__c != null)
		{
			Payment__c[] payment = [select name, id, IsDone__c from Payment__c where id = :pSpecialActivityLine.PaymentName__c];
			if (payment.size() > 0)
				pSpecialActivityLine.paid__c = payment[0].IsDone__c;
		}
		else
			pSpecialActivityLine.paid__c = false;
	}
	
	private void SetIsInActivity()
	{
		pSpecialActivityLine.IsInActivity__c = true;
	}
	
	private void SetChildOrFamily()
	{
		if (pSpecialActivityLine.ChildOrFamily__c == CObjectNames.FamilyMember)
			pSpecialActivityLine.FamilyName__c = null;
		else
			pSpecialActivityLine.ChildName__c = null;
	}
	/*
	private void SetpSpecialActivityLineName()
	{
		SpecialActivities__c[] specialActivities = [select name, id from SpecialActivities__c where id = :pSpecialActivityLine.SpecialActivitiesName__c];
		if (pSpecialActivityLine.ChildOrFamily__c == CObjectNames.FamilyMember)
		{
			pSpecialActivityLine.name = specialActivities[0].name;
			Children__c[] children = [select name, id from Children__c where id = :pSpecialActivityLine.ChildName__c];
			if (children.size() > 0)
				pSpecialActivityLine.name = children[0].name + ' - ' + specialActivities[0].name;
		}
		else if (pSpecialActivityLine.ChildOrFamily__c == CObjectNames.Family)
		{
			pSpecialActivityLine.name = specialActivities[0].name;
			Family__c[] families = [select name, id from Family__c where id = :pSpecialActivityLine.FamilyName__c];
			if (families.size() > 0)
				pSpecialActivityLine.name = families[0].name + ' - ' + specialActivities[0].name;
		}
	}*/
	
	public SpecialActivitiesLine__c GetpSpecialActivityLine()
	{
		return pSpecialActivityLine;
	}
	
	public list<selectoption> GetSpecialActivitisOptions()
	{
		if (pSpecialActivityLine.id == null)
		{
			if (pSpecialActivityLine.ChildOrFamily__c == CObjectNames.Family)
				return GetSpecialActivitiesForFamilies();
			else if (pSpecialActivityLine.ChildOrFamily__c == 'איש קשר'){
				return GetSpecialActivitiesForContacts();
			}
			else 
				return GetSpecialActivitiesForChildren();
		}
		else
		{
			SpecialActivities__c[] specialActivities = [select name, id from SpecialActivities__c where id = :pSpecialActivityLine.SpecialActivitiesName__c];
			return new list<selectoption> { new selectoption(specialActivities[0].id, specialActivities[0].name) };
		}
	}
	
	private list<selectoption> GetSpecialActivitiesForChildren(){
		SpecialActivities__c[] specialActivities = [select name, id from SpecialActivities__c where ActivityType__c = :CObjectNames.FamilyMember and Is_Active__c = true];
		list<selectoption> ret = new list<selectoption>();
		for (integer i = 0; i < specialActivities.size(); i++)
		{
			ret.add(new selectoption(specialActivities[i].id, specialActivities[i].name));
		}
		return ret;
	}
	
	private list<selectoption> GetSpecialActivitiesForFamilies(){
		SpecialActivities__c[] specialActivities = [select name, id from SpecialActivities__c where ActivityType__c = :CObjectNames.Family and Is_Active__c = true];
		list<selectoption> ret = new list<selectoption>();
		for (integer i = 0; i < specialActivities.size(); i++)
		{
			ret.add(new selectoption(specialActivities[i].id, specialActivities[i].name));
		}
		return ret;
	}

	private list<selectoption> GetSpecialActivitiesForContacts(){
		SpecialActivities__c[] specialActivities = [select name, id from SpecialActivities__c where ActivityType__c = 'איש קשר' and Is_Active__c = true];
		list<selectoption> ret = new list<selectoption>();
		for (integer i = 0; i < specialActivities.size(); i++)
		{
			ret.add(new selectoption(specialActivities[i].id, specialActivities[i].name));
		}
		return ret;
	}
	
	
	
	private SpecialActivities__c pSpecialActivities { get; set; }
	private list<CRowSpecialActivitiesLines> pSpecialActivitiesLines { get; set; }
	
	public CSpecialActivitiesLineDb(SpecialActivities__c extSpecialActivities)
	{
		IsOnePaid = false;
		pSpecialActivities = extSpecialActivities;
		pSpecialActivitiesLines = new list<CRowSpecialActivitiesLines>();
		/*FillpSpecialActivitiesLines();*/
		if (pSpecialActivities.id != null)
			SetpSpecialActivitiesLinesBypSpecialActivitiesId(pSpecialActivities.id);
		newField = (pSpecialActivities.ActivityType__c != null && pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember) ? 'ChildName' : 'FamilyName';
		SortList(); 
	}
	
	private void FillpSpecialActivitiesLines()
	{
		if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
			FillpSpecialActivitiesLinesWithChildren();
		else
			FillpSpecialActivitiesLinesWithFamilies();
	}
	
	private void FillpSpecialActivitiesLinesWithFamilies()
	{
		pSpecialActivitiesLines = new list<CRowSpecialActivitiesLines>();
		Family__c[] familys = GetFamilysToFill();
		for (integer i = 0; i < familys.size(); i++)
		{
			SpecialActivitiesLine__c tempSpecialActivitiesLine = new SpecialActivitiesLine__c();
			tempSpecialActivitiesLine.FamilyName__c = familys[i].id;
			pSpecialActivitiesLines.add(new CRowSpecialActivitiesLines(tempSpecialActivitiesLine, familys[i]));
		}
	}
	
	private void FillpSpecialActivitiesLinesWithChildren()
	{
		pSpecialActivitiesLines = new list<CRowSpecialActivitiesLines>();
		Children__c[] children = GetChildrenToFill();
		for (integer i = 0; i < children.size(); i++)
		{
			SpecialActivitiesLine__c tempSpecialActivitiesLine = new SpecialActivitiesLine__c();
			tempSpecialActivitiesLine.ChildName__c = children[i].id;
			//Family__c family = new Family__c();
			//if (pFamiliesMap.containsKey(children[i].FamilyName__c)) 
			//	family = pFamiliesMap.get(children[i].FamilyName__c);
			pSpecialActivitiesLines.add(new CRowSpecialActivitiesLines(tempSpecialActivitiesLine, /*family, */children[i]));
		}
	}
	
	private list<Family__c> GetFamilysToFill()
	{
		if (pSpecialActivities.id != null)
		{
			CFilterFamilys filterFamilys = new CFilterFamilys();
			return filterFamilys.GetFamiliesByFilter(pSpecialActivities);
		}
		else
			return [select name, id, City__c, CoordinatorName__c, CoordinatorName__r.name, Owner.Name, OwnerId from Family__c];
	}
	
	private list<Children__c> GetChildrenToFill()
	{
		if (pSpecialActivities.id != null)
		{
			CFilterChildren filterChildren = new CFilterChildren();
			return filterChildren.GetChildrenByFilter(pSpecialActivities);
		}
		else
			return [select name, id, FamilyName__c, FamilyName__r.City__c, FamilyName__r.CoordinatorName__c,Owner.Name, OwnerId from Children__c];
	}
	
	private void SetpSpecialActivitiesLinesBypSpecialActivitiesId(string id)
	{
		SpecialActivitiesLine__c[] tempSpecialActivitiesLines = [select name, id, IsInActivity__c, FamilyName__c, SpecialActivitiesName__c, PaymentStatus__c ,
																	    ChildOrFamily__c, ChildName__c, Note__c, paid__c, PaymentAmount__c,PaymentName__c
																	    from SpecialActivitiesLine__c where SpecialActivitiesName__c = :id];
 		list<CRowSpecialActivitiesLines> tempList = GetTempListInRowClass(tempSpecialActivitiesLines);
 		if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
 			pSpecialActivitiesLines = SetDetailsAndAddInActivityTolistInChildren(pSpecialActivitiesLines, tempList);
		else
			pSpecialActivitiesLines = SetDetailsAndAddInActivityTolist(pSpecialActivitiesLines, tempList);
 	}
	
	private list<CRowSpecialActivitiesLines> GetTempListInRowClass(SpecialActivitiesLine__c[] tempSpecialActivitiesLines)
	{
		list<CRowSpecialActivitiesLines> ret = new list<CRowSpecialActivitiesLines>();
		for (SpecialActivitiesLine__c line : tempSpecialActivitiesLines)
		{
			if (line.ChildName__c != null && pChildrenMap.containsKey(line.ChildName__c))
			{
				Children__c child;
			    child = pChildrenMap.get(line.ChildName__c);
				ret.add(new CRowSpecialActivitiesLines(line, child));
			}
			else if (line.FamilyName__c != null && pFamiliesMap.containsKey(line.FamilyName__c))
			{
				Family__c family;
				family = pFamiliesMap.get(line.FamilyName__c);
				ret.add(new CRowSpecialActivitiesLines(line, family));
			}
			if (line.Paid__c == true)
				IsOnePaid = true;
		}
		return ret;
	}
	
	private map<string, CRowSpecialActivitiesLines> GetMapFromlist(list<CRowSpecialActivitiesLines> specialActivitiesLines)
	{
		map<string, CRowSpecialActivitiesLines> ret = new map<string, CRowSpecialActivitiesLines>();
		for (CRowSpecialActivitiesLines line : specialActivitiesLines)
		{
			ret.put(line.pSpecialActivitiesLine.FamilyName__c, line);
		}
		return ret;
	}
	
	public void InsertpSpecialActivitiesLines(SpecialActivities__c extSpecialActivities)
	{
		pSpecialActivities = extSpecialActivities;
		pSpecialActivitiesLines = SendToDeleteNotSelected(pSpecialActivitiesLines);
		map<string, Family__c> allFamiliesMap = GetAllFamilysMap();
		map<string, Children__c> allChildrenMap = GetAllChildrenMap();
		list<SpecialActivitiesLine__c> specialActivitiesLinesToInsert = new list<SpecialActivitiesLine__c>();
		list<SpecialActivitiesLine__c> specialActivitiesLinesToUpdate = new list<SpecialActivitiesLine__c>();
		for (CRowSpecialActivitiesLines line : pSpecialActivitiesLines)
		{
			line.pSpecialActivitiesLine.ChildOrFamily__c = pSpecialActivities.ActivityType__c;
			line.pSpecialActivitiesLine.name = GetNameInlist(line.pSpecialActivitiesLine, allFamiliesMap, allChildrenMap);
			if (line.pSpecialActivitiesLine.id == null)
			{
				line.pSpecialActivitiesLine.SpecialActivitiesName__c = pSpecialActivities.id;
				specialActivitiesLinesToInsert.add(line.pSpecialActivitiesLine);
			}
			else
				specialActivitiesLinesToUpdate.add(line.pSpecialActivitiesLine);
		}
		if (specialActivitiesLinesToInsert.size() > 0)
			insert specialActivitiesLinesToInsert;
		else if (specialActivitiesLinesToUpdate.size() > 0)
			update specialActivitiesLinesToUpdate;
	}
	
	private string GetNameInlist(SpecialActivitiesLine__c specialActivitiesLine, Map<string, Family__c> allFamiliesMap, Map<string, Children__c> allChildrenMap)
	{
		if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember)
			return allChildrenMap.get(specialActivitiesLine.ChildName__c).name + ' - ' + pSpecialActivities.name;
		else
			return allFamiliesMap.get(specialActivitiesLine.FamilyName__c).name + ' - ' + pSpecialActivities.name;
	}
	
	private list<CRowSpecialActivitiesLines> SendToDeleteNotSelected(list<CRowSpecialActivitiesLines> specialActivitiesLines)
	{
		list<CRowSpecialActivitiesLines> toReturn = new list<CRowSpecialActivitiesLines>();
		list<SpecialActivitiesLine__c> toDelete = new list<SpecialActivitiesLine__c>();
		for (CRowSpecialActivitiesLines line : specialActivitiesLines)
		{
			integer result = RemoveOrDeleteFromlist(line.pSpecialActivitiesLine);
			if (result == 1)
				toReturn.add(line);
			else if (result == -1)
				toDelete.add(line.pSpecialActivitiesLine);
		}
		if (toDelete.size() > 0)
			delete toDelete;
		
		return toReturn;
	}
	
	private integer RemoveOrDeleteFromlist(SpecialActivitiesLine__c line)
	{
		if (line.IsInActivity__c && IsCurrentChildOrFamily(line))
			return 1;
		if (line.id == null)
			return 0;
		else 
			return -1;
	}
	
	private boolean IsCurrentChildOrFamily(SpecialActivitiesLine__c specialActivitiesLine)
	{
		if (pSpecialActivities.ActivityType__c == CObjectNames.FamilyMember && specialActivitiesLine.ChildName__c != null)	
			return true;
		if (pSpecialActivities.ActivityType__c == CObjectNames.Family && specialActivitiesLine.FamilyName__c != null)
			return true;
		return false;
	}
	
	private Map<string, Family__c> GetAllFamilysMap()
	{
		Family__c[] allFamilys = [select name, id from Family__c];
		Map<string, Family__c> allFmailysMap = new Map<string, Family__c>();
		for (integer i = 0; i < allFamilys.size(); i++)
		{
			allFmailysMap.put(allFamilys[i].id, allFamilys[i]);
		}
		return allFmailysMap;
	}
	
	private Map<string, Children__c> GetAllChildrenMap()
	{
		Children__c[] allChildren = [select name, id from Children__c];
		Map<string, Children__c> allChildrenMap = new Map<string, Children__c>();
		for (integer i = 0; i < allChildren.size(); i++)
		{
			allChildrenMap.put(allChildren[i].id, allChildren[i]);
		}
		return allChildrenMap;
	}
	
	public list<CRowSpecialActivitiesLines> GetpSpecialActivitiesLines()
	{
		return pSpecialActivitiesLines;
	}
	
	public void onChangeList(list<Family__c> families, list<Children__c> children, string activityType)
	{
		if (activityType == CObjectNames.FamilyMember)
			OnChangeChildrenlist(children);
		else
			OnChangeFamilylist(families);
	}
	
	public void OnChangeFamilylist(list<Family__c> families)
	{
		list<CRowSpecialActivitiesLines> activityOldsList = GetOnlyInActivityFromlist(pSpecialActivitiesLines);
		map<string, CRowSpecialActivitiesLines> activeMap = GetMapFromlist(activityOldsList);
		DeleteTheSaved();
		pSpecialActivitiesLines = new list<CRowSpecialActivitiesLines>();
		for (Family__c family : families)
		{
			SpecialActivitiesLine__c tempLine = new SpecialActivitiesLine__c();
			tempLine.FamilyName__c = family.id;
			CRowSpecialActivitiesLines row = new CRowSpecialActivitiesLines(tempLine, family);
			if (activeMap.containsKey(family.id))
				row = activeMap.get(family.id);
			pSpecialActivitiesLines.add(row);
		}
		activeMap = GetMapFromlist(pSpecialActivitiesLines);
		for (CRowSpecialActivitiesLines line : activityOldsList)
		{
			if (!activeMap.containsKey(line.pSpecialActivitiesLine.FamilyName__c))
				pSpecialActivitiesLines.add(line);
		}
	}
	
	private list<CRowSpecialActivitiesLines> SetDetailsAndAddInActivityTolist(list<CRowSpecialActivitiesLines> lines, list<CRowSpecialActivitiesLines> onlyInActivityOlds)
	{
		map<string, CRowSpecialActivitiesLines> activeMap = GetMapFromlist(onlyInActivityOlds);
		for (integer i = 0; i < lines.size(); i++)
		{
			if (activeMap.containsKey(lines[i].pSpecialActivitiesLine.FamilyName__c))
				lines[i] = activeMap.get(lines[i].pSpecialActivitiesLine.FamilyName__c);
		}
		activeMap = GetMapFromlist(lines);
		for (CRowSpecialActivitiesLines line : onlyInActivityOlds)
		{
			if (!activeMap.containsKey(line.pSpecialActivitiesLine.FamilyName__c))
				lines.add(line);
		}
		return lines;
	}
	
	private list<CRowSpecialActivitiesLines> GetOnlyInActivityFromlist(list<CRowSpecialActivitiesLines> specialActivitiesLines)
	{
		list<CRowSpecialActivitiesLines> ret = new list<CRowSpecialActivitiesLines>();
		for (CRowSpecialActivitiesLines line : specialActivitiesLines)
		{
			if (line.pSpecialActivitiesLine.IsInActivity__c)
				ret.add(line);
		}
		return ret;
	}
	
	public void OnChangeChildrenlist(list<Children__c> children)
	{
		list<CRowSpecialActivitiesLines> activityOldsList = GetOnlyInActivityFromlist(pSpecialActivitiesLines);
		map<string, CRowSpecialActivitiesLines> activeMap = GetMapFromlistByChildren(activityOldsList);
		DeleteTheSaved();
		pSpecialActivitiesLines = new list<CRowSpecialActivitiesLines>();
		for (Children__c child : children)
		{
			SpecialActivitiesLine__c tempLine = new SpecialActivitiesLine__c();
			tempLine.ChildName__c = child.id;
			CRowSpecialActivitiesLines row = new CRowSpecialActivitiesLines(tempLine, child);
			if (activeMap.containsKey(child.id))
				row = activeMap.get(child.id);
			pSpecialActivitiesLines.add(row);
		}
		activeMap = GetMapFromlistByChildren(pSpecialActivitiesLines);
		for (CRowSpecialActivitiesLines line : activityOldsList)
		{
			if (!activeMap.containsKey(line.pSpecialActivitiesLine.ChildName__c))
				pSpecialActivitiesLines.add(line);
		}
	}
	
	private void DeleteTheSaved()
	{
		list<SpecialActivitiesLine__c> toDelete = new list<SpecialActivitiesLine__c>();
		for (CRowSpecialActivitiesLines row : pSpecialActivitiesLines)
		{
			if (row.pSpecialActivitiesLine.id != null && (!row.pSpecialActivitiesLine.IsInActivity__c || !IsCurrentChildOrFamily(row.pSpecialActivitiesLine)))
				toDelete.add(row.pSpecialActivitiesLine);
		}
		if (toDelete.size() > 0)
			delete toDelete;
	}
	
	private list<CRowSpecialActivitiesLines> SetDetailsAndAddInActivityTolistInChildren(list<CRowSpecialActivitiesLines> lines, list<CRowSpecialActivitiesLines> onlyInActivityOlds)
	{
		map<string, CRowSpecialActivitiesLines> activeMap = GetMapFromlistByChildren(onlyInActivityOlds);
		for (integer i = 0; i < lines.size(); i++)
		{
			if (activeMap.containsKey(lines[i].pSpecialActivitiesLine.ChildName__c))
				lines[i] = activeMap.get(lines[i].pSpecialActivitiesLine.ChildName__c);
		}
		activeMap = GetMapFromlistByChildren(lines);
		for (CRowSpecialActivitiesLines line : onlyInActivityOlds)
		{
			if (!activeMap.containsKey(line.pSpecialActivitiesLine.ChildName__c))
				lines.add(line);
		}
		return lines;
	}
	
	private map<string, CRowSpecialActivitiesLines> GetMapFromlistByChildren(list<CRowSpecialActivitiesLines> lines)
	{
		map<string, CRowSpecialActivitiesLines> ret = new map<string, CRowSpecialActivitiesLines>();
		for (CRowSpecialActivitiesLines line : lines)
		{
			ret.put(line.pSpecialActivitiesLine.ChildName__c, line);
		}
		return ret;
	}
	
	public map<string, Family__c> familiesMap;
	public map<string, Family__c> pFamiliesMap
	{
		get
		{
			if (familiesMap == null)
			{
				familiesMap = new map<string, Family__c>();
				Family__c[] families = [select name, id, City__c, CoordinatorName__c, CoordinatorName__r.name , Owner.Name, OwnerId from Family__c];
				for (Family__c family : families)
				{
					familiesMap.put(family.id, family);
				}
			}
			return familiesMap;
		}
	}
	
	public map<string, Children__c> childrenMap;
	public map<string, Children__c> pChildrenMap
	{
		get
		{
			if (childrenMap == null)
			{
				childrenMap = new map<string, Children__c>();
				Children__c[] children = [select name, id, FamilyName__r.City__c, FamilyName__r.CoordinatorName__c, FamilyName__r.Owner.Name, FamilyName__r.OwnerId from Children__c];
				for (Children__c child : children)
				{
					childrenMap.put(child.id, child);
				}
			}
			return childrenMap;
		}
	}
	
	public void SortList()
	{
		if (newField != null)
		{
			SetCurrentMark();
			if (newField == 'FamilyName')
				SortListByFamilyName();
			if (newField == 'ChildName')
				SortListByChildName();
			if (newField == 'City' || newField == 'Coordinator')
				SortListByCityOrCoordinator();
		}
	}
	
	private void SortListByFamilyName()
	{
		map<string, list<CRowSpecialActivitiesLines>> mapForSort = new map<string, list<CRowSpecialActivitiesLines>>();
		for (CRowSpecialActivitiesLines line : pSpecialActivitiesLines)
		{
			if (mapForSort.containsKey(line.pFamily.name))
				mapForSort.get(line.pFamily.name).add(line);
			else
				mapForSort.put(line.pFamily.name, new list<CRowSpecialActivitiesLines> { line });
		}
		pSpecialActivitiesLines.clear();
		list<string> keiesList = new list<string>();
        keiesList.addAll(mapForSort.keyset());
        keiesList.sort();
        if (currentMark == downMark)
        {
	        for (string key : keiesList)
	        {
	        	pSpecialActivitiesLines.addAll(mapForSort.get(key));
	        }
        }
        else
        {
        	for (integer i = keiesList.size() - 1; i >= 0; i--)
	        {
	        	pSpecialActivitiesLines.addAll(mapForSort.get(keiesList[i]));
	        }
        }
	}
	
	private void SortListByChildName()
	{
		map<string, list<CRowSpecialActivitiesLines>> mapForSort = new map<string, list<CRowSpecialActivitiesLines>>();
		for (CRowSpecialActivitiesLines line : pSpecialActivitiesLines)
		{
			if (mapForSort.containsKey(line.pChild.name))
				mapForSort.get(line.pChild.name).add(line);
			else
				mapForSort.put(line.pChild.name, new list<CRowSpecialActivitiesLines> { line });
		}
		pSpecialActivitiesLines.clear();
		list<string> keiesList = new list<string>();
        keiesList.addAll(mapForSort.keyset());
        keiesList.sort();
        if (currentMark == downMark)
        {
	        for (string key : keiesList)
	        {
	        	pSpecialActivitiesLines.addAll(mapForSort.get(key));
	        }
        }
        else
        {
        	for (integer i = keiesList.size() - 1; i >= 0; i--)
	        {
	        	pSpecialActivitiesLines.addAll(mapForSort.get(keiesList[i]));
	        }
        }
	}
	
	private void SortListByCityOrCoordinator(){
		map<string, list<CRowSpecialActivitiesLines>> mapForSort = new map<string, list<CRowSpecialActivitiesLines>>();
		for (CRowSpecialActivitiesLines line : pSpecialActivitiesLines){
			
			string fieldName = newField == 'City' ? line.pFamily.City__c : line.pFamily.Owner.name;
			if (mapForSort.containsKey(fieldName))
				mapForSort.get(fieldName).add(line);
			else
				mapForSort.put(fieldName, new list<CRowSpecialActivitiesLines> { line });
		}
		pSpecialActivitiesLines.clear();
		list<string> keiesList = new list<string>();
        keiesList.addAll(mapForSort.keyset());
        keiesList.sort();
        if (currentMark == downMark) {
	        for (string key : keiesList){
	        	pSpecialActivitiesLines.addAll(mapForSort.get(key));
	        }
        }
        else{
        	for (integer i = keiesList.size() - 1; i >= 0; i--){
	        	pSpecialActivitiesLines.addAll(mapForSort.get(keiesList[i]));
	        }
        }
	}
	
	private void SetCurrentMark(){
		if (newField == oldField){
			if (currentMark == downMark)
				currentMark = upMark;
			else
				currentMark = downMark;
		}
		else
			currentMark = downMark;
	}
	
	public void SetSpecialActivitiesLinesByPayments(list<Payment__c> payments){
		SpecialActivitiesLine__c[] specialActivitiesLines = [select name, id, Paid__c, PaymentName__c from SpecialActivitiesLine__c where PaymentName__c in :payments];
		specialActivitiesLinesByPayments = new map<string, list<SpecialActivitiesLine__c>>();
		for (SpecialActivitiesLine__c line : specialActivitiesLines){
			if (specialActivitiesLinesByPayments.containsKey(line.PaymentName__c))
				specialActivitiesLinesByPayments.get(line.PaymentName__c).add(line);
			else
				specialActivitiesLinesByPayments.put(line.PaymentName__c, new List<SpecialActivitiesLine__c> { line } );
		}
	}
	
	public list<SpecialActivitiesLine__c> UpdateSpecialActivitiesLineAfterUpdatePayment(string paymentId, boolean isDone){
		list<SpecialActivitiesLine__c> ret = new list<SpecialActivitiesLine__c>();
		if (specialActivitiesLinesByPayments.containsKey(paymentId)){
			list<SpecialActivitiesLine__c> toUpdate = specialActivitiesLinesByPayments.get(paymentId);
			for (SpecialActivitiesLine__c line : toUpdate){
				line.Paid__c = isDone;
				if (line.Paid__c == true) line.PaymentStatus__c = 'שולם';
			}
			if (toUpdate.size() > 0)
				ret.addAll(toUpdate);
		}
		return ret;
	}
	
	public void SetNewList(){
		list<CRowSpecialActivitiesLines> listToKeep = new list<CRowSpecialActivitiesLines>();
		for (CRowSpecialActivitiesLines row : pSpecialActivitiesLines){
			if (row.pSpecialActivitiesLine.IsInActivity__c && IsCurrentChildOrFamily(row.pSpecialActivitiesLine))
				listToKeep.add(row);
		}
		pSpecialActivitiesLines.clear();
		pSpecialActivitiesLines = listToKeep;
	}
}