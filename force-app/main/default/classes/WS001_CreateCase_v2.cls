@RestResource(urlMapping='/colelCreateCase_v2/*')
global class WS001_CreateCase_v2 {

	
     @HttpPost
    global static string createCase(String childId,
                                    String subject,
                                    String description,
                                    String existingChild,                                  
                                    String firstName,
                                    String lastName,
                                    String phone,
                                    String userName,
                                    String parameter1,
                                    String parameter2,
                                    String parameter3,
                                    String parameter4,
                                    String parameter5){
		try{
			
			if(childId == null) childId =   '';
            if(subject== null) subject=   '';
            if(description == null) description=   '';
            if(existingChild== null) existingChild=   '';
            if(firstName == null) firstName =   '';
            if(lastName== null) lastName=   '';
            if(phone== null) phone=   '';
            if(userName== null) userName=   '';
            
			Case c = new Case ( Description = description + '\n',  // 'מטופל חדש ? ' + existingChild +  '\n' +
            					Subject     = subject  ,
            					Origin      = 'פניה טלפונית'  ,
            					Status      = 'חדש',
            					Phone__c    = phone,
            					Contact_Name__c = firstName + ' ' +  lastName,
            					Existing_Child__c = existingChild == 'לקוח חדש'? false: true,
            					ID_Number__c = childId);
            					
            if(childId != null && childId != ''){
		        List<Children__c> childs = [select ID,IdNumber__c,FamilyName__r.OwnerID,FamilyName__c from Children__c where IdNumber__c = :childId];
		        if(childs.size() > 0){
		     		c.OwnerId = childs[0].FamilyName__r.OwnerID;
		     		c.ChildName__c = childs[0].Id;
		     		c.FamilyName__c = childs[0].FamilyName__c;
		        }
            }
            if(c.ChildName__c == null){
            	userName = userName.replaceAll(' ','');
            	List<User> users = [select Id From User where Tel_Office_Suscriber__c = :userName];
            	if(users.size()>0)
            		c.OwnerId = users[0].Id;
            }
            
            insert c;
        	
         	return '<response>success - caseId:' + c.Id+ '</response';   
	        
		}
		catch (Exception ex){
			return '<response>' + ex.getMessage()+ '</response>';
		}
    }
    /* @HttpPost
    global static string createCase(String childId,
                                    String subject,
                                    String description,
                                    String existingChild,                                  
                                    String firstName,
                                    String lastName,
                                    String phone,
                                    String userName,
                                    String parameter1,
                                    String parameter2,
                                    String parameter3,
                                    String parameter4,
                                    String parameter5){
		try{
			
			if(childId == null) childId =   '';
            if(subject== null) subject=   '';
            if(description == null) description=   '';
            if(existingChild== null) existingChild=   '';
            if(firstName == null) firstName =   '';
            if(lastName== null) lastName=   '';
            if(phone== null) phone=   '';
            if(userName== null) userName=   '';
            
			Task t = new Task ( Description = 'תיאור: ' + description + '\n',  // 'מטופל חדש ? ' + existingChild +  '\n' +
            					Subject     = subject  ,
            					//Origin      = 'פניה טלפונית'  ,
            					Status      = 'חדש',
            					ActivityDate     = Date.Today().addDays(1));
            					
            if(childId != null && childId != ''){
		        List<Children__c> childs = [select ID,IdNumber__c,FamilyName__r.OwnerID from Children__c where IdNumber__c = :childId];
		        if(childs.size() > 0){
		     		t.OwnerId = childs[0].FamilyName__r.OwnerID;
		     		t.WhatId = childs[0].Id;
		        }
            }
            if(t.WhatId == null){
            	t.Description += 'ת.ז: ' +  childId + '\n';
            	t.Description += 'שם: ' + firstName + ' '+ lastName +  '\n' ;
            	List<User> users = [select Id From User where Name like :usersMap.get(userName)];
            	if(users.size()>0)
            		t.OwnerId = users[0].Id;
            }
            
            insert t;
        	
         	return 'success - taskId:' + t.Id;   
	        
		}
		catch (Exception ex){
			return ex.getMessage();
		}
    }*/
    
    
}