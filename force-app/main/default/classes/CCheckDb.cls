public with sharing class CCheckDb 
{
	private Payment__c pPayment { get; set; }
	private List<Check__c> pChecks { get; set; }
	public integer pPaymentsNumber { get; set; }
	public date pFirstChackDepositDate { get; set; }
	public string pFirstChackName { get; set; }
	
	public CCheckDb(){}
	
	public CCheckDb(Payment__c extPayment)
	{
		pChacksToDelete = new map<string, Check__c>();
		pPayment = extPayment;
		FillpCheck();
		if (pPayment.id != null)
			SetpChecksByPaymentId(pPayment.id);
	}
	
	private void FillpCheck()
	{
		pChecks = new List<Check__c>();
		Check__c check = new Check__c();
		check.BankCode__c = 'בנק פאג"י - 52';
		check.BranchNumber__c = '184';
		check.AccountNumber__c = '659770';
		pChecks.add(check);
	}
	
	private void SetpChecksByPaymentId(string id)
	{
		pChecks = [select name, id, AccountNumber__c, BranchNumber__c, AmountCheck__c, BankCode__c, PayeeOnly__c, ToOrPayable__c,
				   PaymentName__c, DepositDate__c from Check__c where PaymentName__c = :pPayment.id];
	}
	
	public void InsertChecks()
	{
		pChecks = sendToDeleteEmptys(pChecks);
		List<Check__c> toInsert = new List<Check__c>();
		List<Check__c> toUpdate = new List<Check__c>();
		for (integer i = 0; i < pChecks.size(); i++)
		{
			if (pChecks[i].id == null)
			{
				pChecks[i].PaymentName__c = pPayment.id;
				toInsert.add(pChecks[i]);
			}
			else
				toUpdate.add(pChecks[i]);
		}
		if (toInsert.size() > 0)
			insert toInsert;
		if (toUpdate.size() > 0)
			update toUpdate;
		if (pChacksToDelete.values().size() > 0)
			delete pChacksToDelete.values();
	}
	
	private List<Check__c> sendToDeleteEmptys(List<Check__c> checks)
	{
		List<Check__c> toReturn = new List<Check__c>();
		List<Check__c> toDelete = new List<Check__c>();
		for (integer i = 0; i < checks.size(); i++)
		{
			integer rezolt = RemoveOrDeleteFromList(checks, i);
			if (rezolt == 1)
				toReturn.add(checks[i]);
			else if (rezolt == -1)
				toDelete.add(checks[i]);
		}
		if (toDelete.size() > 0)
			delete toDelete;
		
		return toReturn;
	}
		
	private integer RemoveOrDeleteFromList(List<Check__c> checks, integer i)
	{
		if (checks[i].AmountCheck__c != null && checks[i].AmountCheck__c != 0 && checks[i].name != null)
			return 1;
		if (checks[i].id == null)
			return 0;
		else 
			return -1;
	}
	
	public List<Check__c> GetpChecks()
	{
		return pChecks;
	}
	
	public void OnAddCheck(decimal debt, decimal amount)
	{
		pChecks.add(GetCheckWithDetails(null, null, (debt - amount)));
	}
	
	private Check__c GetCheckWithDetails(string checkNum, date depositDate, decimal amount)
	{
		Check__c ret = new Check__c();
		ret.BankCode__c = 'בנק פאג"י - 52';
		ret.BranchNumber__c = '184';
		ret.AccountNumber__c = '659770';
		ret.DepositDate__c = datetime.now().date();
		ret.DepositDate__c = depositDate;
		ret.name = checkNum;
		ret.AmountCheck__c = amount;
		if (pChecks.size() > 0)
		{
			if (pChecks[pChecks.size() - 1].name != null)
				ret.name = checkNum != null && checkNum != '' ? checkNum : GetNumberOfCheck(pChecks[pChecks.size() - 1].name);
			if (pChecks[pChecks.size() - 1].DepositDate__c != null)
				ret.DepositDate__c = depositDate != null ? depositDate : pChecks[pChecks.size() - 1].DepositDate__c.AddMonths(1);
		}
		return ret;
	}
	
	private decimal GetPaidAmount()
	{
		decimal ret = 0;
		for (Check__c c : pChecks)
		{
			ret += c.AmountCheck__c != null ? c.AmountCheck__c : 0;
		}
		return ret;
	}
	
	private string GetNumberOfCheck(string oldNumber)
	{
		decimal checkNumber;
		try
		{
			checkNumber = decimal.valueOf(oldNumber);
		}
		finally
		{
			if (checkNumber == null)
				return null;
		}
		return GetFullStringWithNumber(oldNumber, string.valueOf(checkNumber + 1));
	}
	
	private string GetFullStringWithNumber(string oldFullNumber, string newNumber)
	{
		if (newNumber.length() >= oldFullNumber.length())
			return newNumber;
		string ret = oldFullNumber.substring(0, (oldFullNumber.length() - newNumber.length()));
		return ret + newNumber;
	}
	
	public decimal getTotalAmountFromChecks()
	{
		decimal ret = 0;
		for (integer i = 0; i < pChecks.size(); i++)
		{
			if (pChecks[i].AmountCheck__c != null)
				ret += pChecks[i].AmountCheck__c;	
		}
		return ret;
	}
	
	public void SetChecksByPaymentsNum(decimal ammount)
	{
		if (pPaymentsNumber != null && pPaymentsNumber > 0)
		{
			SetChacksToDelete();
			for (integer i = 0; i < pPaymentsNumber; i++)
			{
				if (i == 0)
					pChecks.add(GetCheckWithDetails(pFirstChackName, pFirstChackDepositDate, 0));
				else
					pChecks.add(GetCheckWithDetails(null, null, 0));
				pChecks[pChecks.size() - 1].AmountCheck__c = ammount != null && ammount > 0 ? ammount / pPaymentsNumber : 0; 
			}
		}
	}
	
	private void SetChacksToDelete()
	{
		for (Check__c c : pChecks)
		{
			if (c.id != null && !pChacksToDelete.containsKey(c.id))
				pChacksToDelete.put(c.id, c);
		}
		pChecks.clear();
	}
	
	public static list<Check__c> GetChecksByPayment(string id)
	{
		return [select name, id, AccountNumber__c, BranchNumber__c, AmountCheck__c, BankCode__c, PaymentName__c, DepositDate__c, 
				PayeeOnly__c, ToOrPayable__c from Check__c where PaymentName__c = :id];
	}
	
	private map<string, Check__c> pChacksToDelete;
	
	public void forTest()
	{
		string a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
		a = '';
	}
}