public without sharing class CCaseDb
{
    public static boolean IsDoNotUpdateCaseAfterApproval { get; set; }
    public string StartMinute { get; set; }
    public string StartHour { get; set; }
    public string EndMinute { get; set; }
    public string EndHour { get; set; }
    private Map<string, List<Case__c>> casesByFamilysMap;
    private Map<string, List<Case__c>> casesByChildrenMap;
    private Case__c pCase { get; set; }
    private CRequestForTreatmentDb requestForTreatmentDb { get; set; }
    
    public CCaseDb(boolean b)
    {
        
    }
    
    public CCaseDb()
    {
        pCase = new Case__c();
    }
    
    public CCaseDb(ApexPages.StandardController controller)
    {
        pCase = (Case__c)controller.getRecord();
        //SetUserName();
        if (pCase.id != null)
            SetpCaseBypCaseId(pCase.id);
        else
            pCase.Date__c = datetime.now().date();
        requestForTreatmentDb = new CRequestForTreatmentDb(pCase);
        SetHours();
    }
    
    private void SetHours()
    {
        if (pCase.StartTime__c != null && pCase.StartTime__c.split(':').size() == 2)
        {
            StartMinute = pCase.StartTime__c.split(':')[1];
            StartHour = pCase.StartTime__c.split(':')[0];
        }
        if (pCase.EndTime__c != null && pCase.EndTime__c.split(':').size() == 2)
        {
            EndMinute = pCase.EndTime__c.split(':')[1];
            EndHour = pCase.EndTime__c.split(':')[0];
        }
    }
    
    private void SetpCaseBypCaseId(string id)
    {
        pCase = [select Comments__c, TotalHoursToCalculate__c, AssigneeUser__c, Status__c, Origin__c, NoteToPrintPage__c, Type__c, 
                Subject__c, Priority__c, CoordinatorUserName__c, name, OwnerName__c, ChildName__c, FamilyName__c, ToMyAttention__c,
                PreviousAssigneeUser__c, Description__c, WhoId__c, id, SendEmail__c, AssignedUsers__c, Date__c, EndTime__c, StartTime__c, 
                ToMyAttention2__c, AssigneeUsersList__c, SendEmailToCreator__c, SendEmailToManager__c, SchoolYear__c, Child_Progress__c,
                Owner.Name,OwnerId, CreatedById  from Case__c where id = :id];
    }
    
    public void SetCasesByFamilysMap(set<string> familyIds)
    {
        Case__c[] allCases = [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Type__c, Priority__c, NoteToPrintPage__c,
                              ToMyAttention2__c, name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, 
                              CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, Description__c, SendEmail__c, ToMyAttention__c,
                              TotalHoursToCalculate__c, AssigneeUsersList__c, SendEmailToManager__c, PreviousAssigneeUser__c, SchoolYear__c from Case__c 
                              where FamilyName__c != null and FamilyName__c in :familyIds];
        casesByFamilysMap = new Map<string, List<Case__c>>();
        for (Case__c c : allCases)
        {
            if (casesByFamilysMap.containsKey(c.FamilyName__c))
                casesByFamilysMap.get(c.FamilyName__c).add(c);
            else
                casesByFamilysMap.put(c.FamilyName__c, new List<Case__c> { c } );
        }   
    }
    
    public void SetCasesByChildrenMap(list<Children__c> children)
    {
        Case__c[] allCases = [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Type__c, Priority__c, NoteToPrintPage__c,
                              name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, ToMyAttention__c,
                              ToMyAttention2__c, CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, Description__c, SendEmail__c, 
                              TotalHoursToCalculate__c, AssigneeUsersList__c, SendEmailToManager__c, PreviousAssigneeUser__c, SchoolYear__c from Case__c 
                              where ChildName__c in :children];
        casesByChildrenMap = new Map<string, List<Case__c>>();
        for (integer i = 0; i < allCases.size(); i++)
        {
            if (casesByChildrenMap.containsKey(allCases[i].ChildName__c))
                casesByChildrenMap.get(allCases[i].ChildName__c).add(allCases[i]);
            else
                casesByChildrenMap.put(allCases[i].ChildName__c, new List<Case__c> { allCases[i] } );
        }
    }
    
    public void Save(boolean isChild)
    {
        SetCoordinatorUserName();
        CheckWhoId();
        SetToFillTimes();
        FillChildInRequestsList();
        boolean mIsAfterUpdate = false;
        if (pCase.id == null)
            insert pCase;
        else
        {
            mIsAfterUpdate = true;
            update pCase;
        }
        requestForTreatmentDb.InsertpRequestsInCase(isChild);
        CheckIfSendEmail(mIsAfterUpdate);
    }
    
    public List<CRequestInCase> GetpRequestsInCase()
    {
        return requestForTreatmentDb.GetpRequestsInCase();
    }
    
    public List<selectOption> GetProfessionTypesOptions()
    {
        return requestForTreatmentDb.GetProfessionTypesOptions();
    }
    
    public void AddRequestToList()
    {
        requestForTreatmentDb.AddRequestToList();
        FillChildInRequestsList();
    }
    
    public void FillChildInRequestsList()
    {
        requestForTreatmentDb.FillChildInRequestsList(pCase.ChildName__c);
    }
    
    private void SetToFillTimes()
    {
        pCase.StartTime__c = CValidateTimesAndConverterTimes.FillFields(pCase.StartTime__c);
        pCase.EndTime__c = CValidateTimesAndConverterTimes.FillFields(pCase.EndTime__c);
        integer minutesStart = CValidateTimesAndConverterTimes.getMinuteFromTime(pCase.StartTime__c);
        integer hoursStart = CValidateTimesAndConverterTimes.getHoursFromTime(pCase.StartTime__c);
        integer minutesEnd = CValidateTimesAndConverterTimes.getMinuteFromTime(pCase.EndTime__c);
        integer hoursEnd = CValidateTimesAndConverterTimes.getHoursFromTime(pCase.EndTime__c);
        integer totalHours = CValidateTimesAndConverterTimes.ComputeTotalHours(MinutesStart, MinutesEnd, HoursStart, HoursEnd);
        integer totalMinutes = CValidateTimesAndConverterTimes.ComputeTotalMinutes(MinutesStart, MinutesEnd);
        string fullTotalHours = setTotalHours(totalHours, totalMinutes);
        pCase.TotalHoursToCalculate__c = CValidateTimesAndConverterTimes.GetTimeToCalculate(fullTotalHours);
    }
    
    private string SetTotalHours(integer totalHours, integer totalMinutes)
    {
        string ret = '';
        if (string.valueOf(TotalHours).Length() == 1)
            ret = string.valueOf(TotalHours) + string.valueOf(TotalMinutes);
        else
        {
            if (string.valueOf(TotalMinutes).Length() == 1)
                ret = string.valueOf(TotalHours) + '0' + string.valueOf(TotalMinutes);
            else
                ret = string.valueOf(TotalHours) + string.valueOf(TotalMinutes);
        }
        return CValidateTimesAndConverterTimes.FillFields(ret);
    }
    
    private void CheckWhoId()
    {
        if (pCase.WhoId__c == CObjectNames.Family)  
            pCase.ChildName__c = null;
        else if (pCase.WhoId__c == CObjectNames.FamilyMember)
            pCase.FamilyName__c = null;
    }
    
    private void SetCoordinatorUserName()
    {
        if (pCase.WhoId__c == CObjectNames.Family && pCase.FamilyName__c != null)
        {   
            Family__c[] family = [select name, id, CoordinatorUserName__c, OwnerId from Family__c where id = :pCase.FamilyName__c];
            if (family.size() > 0){
                //pCase.CoordinatorUserName__c = family[0].CoordinatorUserName__c;
                pCase.OwnerId = family[0].OwnerId;
            }
        }
        else if (pCase.WhoId__c == CObjectNames.FamilyMember && pCase.ChildName__c != null)
        {
            Children__c[] child = [select name, id, CoordinatorUserName__c, OwnerId from Children__c where id = :pCase.ChildName__c];
            if (child.size() > 0){
            	//pCase.CoordinatorUserName__c = child[0].CoordinatorUserName__c;
            	pCase.OwnerId = child[0].OwnerId;
            }
                
        }
                
    }
    
    private void CheckIfSendEmail(boolean mIsAfterUpdate)
    {
        List<User> userT = GetUsersT();
        string userTNames = GetUsersTNames(userT);
        if (userT.size() > 0)
        {
            if (pCase.SendEmail__c)
                SendEmailToUsersT(mIsAfterUpdate, userT, userTNames);
             
            list<Profile> p = [select name, id from Profile where name = :CObjectNames.ProfileSystemManager or name = :CObjectNames.ProfileSystemManager2 or name = :CObjectNames.ProfileSystemManager3 ];
            if (p.size() > 0)
            {
                User[] userM = [select name, FirstName, LastName, Email from User where ProfileId = :p[0].id];
                User[] userF = [select name, FirstName, LastName, Email from User where name = :pCase.OwnerName__c]; 
                
                
                string VersionEmail = VersionEmail(mIsAfterUpdate, userT[0].name, userTNames);
                List<string> EmailsToSend = GetEmailsToSend(userF, userM, userT);
                sendEmail(EmailsToSend, VersionEmail);
            }
        }
    }
    
    private void SendEmail(List<string> EmailsToSend, string VersionEmail)
    {
        if (EmailsToSend.size() > 0)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(EmailsToSend);
            mail.setHtmlBody(VersionEmail);
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    private List<string> GetEmailsToSend(List<User> userF, User[] userM, List<User> userT)
    {
        List<string> EmailsToSend = new List<string>();
        if (userM.size() > 0 && pCase.SendEmailToManager__c)
            EmailsToSend.add(userM[0].Email);
        if (userF.size() > 0 && pCase.SendEmailToCreator__c)
            EmailsToSend.add(userF[0].Email);
        return EmailsToSend;
    }
    
    private string GetUsersTNames(List<User> UsersT)
    {
        string UsersTNames = '';
        for (integer i = 0; i < UsersT.size(); i++)
        {
            if (i == 0)
                UsersTNames += UsersT[i].name;
            else 
                UsersTNames += ', ' + UsersT[i].name;
            if (i == UsersT.size() - 1)
                UsersTNames += '.';
        }
        return UsersTNames;
    }
    
    private List<User> GetUsersT()
    {
        List<User> userT = new List<User>();
        User[] allUsers = [select name, FirstName, LastName, Email from User]; 
        for (integer i = 0; i < allUsers.size(); i++)
        {
            if ((pCase.AssignedUsers__c != null && pCase.AssignedUsers__c.contains(allUsers[i].name)) || pCase.AssigneeUser__c == allUsers[i].id)
            {
                userT.add(allUsers[i]);
            }
        }
        return userT;
    }
    
    private void SendEmailToUsersT(boolean isAfterUpdate, List<User> UserT, string allUsersT)
    {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        Family__c family;
        Children__c child;
        if (pCase.WhoId__c == CObjectNames.Family)
            family = [select name from Family__c where id = :pCase.FamilyName__c];
        else if (pCase.WhoId__c == CObjectNames.FamilyMember)
            child = [select name from Children__c where id = :pCase.ChildName__c];
        for (integer i = 0; i < userT.size(); i++)
        {
            string VersionEmail = VersionEmail(IsAfterUpdate, userT[i].name, allUsersT, family, child);
            List<string> EmailsToSend = new List<string>();
            mail.setToAddresses(new string[] { userT[i].Email });
            mail.setHtmlBody(VersionEmail);
            if(!Test.isRunningTest())
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
    }
    
    private string VersionEmail(boolean IsAfterUpdate, string UserNameT, string allUsersT)
    {
        string Sbg1;
        string Sbg2;
        string FamilyOrChild;
        string OnlyToMOrF = '';
        if (IsAfterUpdate)
        {
            Sbg1 = 'עדכון מקרה ';
            Sbg2 = 'המקרה שעודכן ו';
        }
        else
        {
            Sbg1 = 'מקרה חדש ';
            Sbg2 = 'המקרה החדש הבא ש';
        }
        if (pCase.WhoId__c == CObjectNames.Family)
        {
            Family__c family = [select name from Family__c where id = :pCase.FamilyName__c limit 1];
            FamilyOrChild = '<b>משפחה: </b>' + family.name;
        }
        else if (pCase.WhoId__c == CObjectNames.FamilyMember)
        {
            Children__c child = [select name from Children__c where id = :pCase.ChildName__c limit 1];
            FamilyOrChild = '<b>בן משפחה: </b>' + child.name;
        } 
        string BaseURL = ApexPages.currentPage().getHeaders().get('Host');
        PageReference casePage = new ApexPages.StandardController(pCase).view();
        string CaseUrl = BaseURL + casePage.getUrl();
        string htmlBody = '<div dir="rtl"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>' + Sbg1+ '</b><br><br></p><div><b>אל: </b>' +
                          UserNameT + '<br></div><div><br><b>'+Sbg2 + 'הוקצה אליך ע"י ' + pCase.OwnerName__c + ':</b><br></div><div><br><br>' + 
                          FamilyOrChild + '<br><br><b>' + 'מקור מקרה: </b>' + pCase.Origin__c + '<br><br><b>' + 'מצב: </b>' + pCase.Status__c +
                          '<br><br><b>' + 'סוג: </b>' + pCase.Type__c + '<br><br><b>' + 'עדיפות: </b>' + pCase.Priority__c + '<br><br><b>' + 
                          'נושא: </b>' + pCase.Subject__c + '<br><br><b>' + 'תיאור: </b>' + pCase.Description__c + '<br><br><b>' + 'הערות פנימיות: </b>'
                          + pCase.Comments__c + '</div><div><br><br><b> המקרה המלא: </b><br>' + CaseUrl + '</div>';
        if (allUsersT != '')
            htmlBody += '<br><br> מקרה זה הוקצה ל: ' + allUsersT;
        return htmlBody;
    }
    
    private string VersionEmail(boolean IsAfterUpdate, string UserNameT, string allUsersT, Family__c family, Children__c child)
    {
        string Sbg1;
        string Sbg2;
        string FamilyOrChild;
        string OnlyToMOrF = '';
        if (IsAfterUpdate)
        {
            Sbg1 = 'עדכון מקרה ';
            Sbg2 = 'המקרה שעודכן ו';
        }
        else
        {
            Sbg1 = 'מקרה חדש ';
            Sbg2 = 'המקרה החדש הבא ש';
        }
        if (family != null)
            FamilyOrChild = '<b>משפחה: </b>' + family.name;
        else if (child != null)
            FamilyOrChild = '<b>בן משפחה: </b>' + child.name;
        string BaseURL = ApexPages.currentPage().getHeaders().get('Host');
        PageReference casePage = new ApexPages.StandardController(pCase).view();
        string CaseUrl = BaseURL + casePage.getUrl();
        string htmlBody = '<div dir="rtl"><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>' + Sbg1+ '</b><br><br></p><div><b>אל: </b>' +
                          UserNameT + '<br></div><div><br><b>'+Sbg2 + 'הוקצה אליך ע"י ' + pCase.OwnerName__c + ':</b><br></div><div><br><br>' + 
                          FamilyOrChild + '<br><br><b>' + 'מקור מקרה: </b>' + pCase.Origin__c + '<br><br><b>' + 'מצב: </b>' + pCase.Status__c +
                          '<br><br><b>' + 'סוג: </b>' + pCase.Type__c + '<br><br><b>' + 'עדיפות: </b>' + pCase.Priority__c + '<br><br><b>' + 
                          'נושא: </b>' + pCase.Subject__c + '<br><br><b>' + 'תיאור: </b>' + pCase.Description__c + '<br><br><b>' + 'הערות פנימיות: </b>'
                          + pCase.Comments__c + '</div><div><br><br><b> המקרה המלא: </b><br>' + CaseUrl + '</div>';
        if (allUsersT != '')
            htmlBody += '<br><br> מקרה זה הוקצה ל: ' + allUsersT;
        return htmlBody;
    }
    
    public Case__c GetCase()
    {
        return pCase;
    }
    
    private void SetUserName()
    {
        pCase.OwnerName__c = UserInfo.getName();
    }
    
    public List<SelectOption> OptionToSelectFamilyOrChild()
    {
        List<SelectOption> Options = new List<SelectOption>();
        Options.add(new Selectoption(CObjectNames.FamilyMember, CObjectNames.FamilyMember));
        Options.add(new Selectoption(CObjectNames.Family, CObjectNames.Family));
        Options.add(new Selectoption(CObjectNames.General, CObjectNames.General));
        return Options;
    }
    
    public List<SelectOption> OptionToSelectUsers()
    {
        List<SelectOption> Options = new List<SelectOption>();
        User [] users = [select name, id from User];
        for (integer i = 0; i < users.size(); i++)
        {
            Options.add(new SelectOption(users[i].name, users[i].name));
        }
        return Options;
    }
    
    public List<SelectOption> OptionsChildrenByFamily()
    {
        if (pCase.FamilyName__c != null)
        {
            List<SelectOption> Options = new List<SelectOption>();
            CChildDb childDb = new CChildDb();
            Children__c[] children = childDb.GetChildrenByFamily(pCase.FamilyName__c);
            Options.add(new SelectOption('null', 'ללא'));
            for (integer i = 0; i < children.size(); i++)
            {
                Options.add(new SelectOption(children[i].id, children[i].name));
            }
            return Options;
        }
        return new list<SelectOption> { new SelectOption('null', 'ללא') };
    }
    
    public Case__c GetCaseByCaseId(string caseId)
    {
        Case__c[] OneCase = [select name, id, FamilyName__c, ChildName__c, WhoId__c, SchoolYear__c,OwnerId from Case__c where id = :caseId];
        if (OneCase.size() > 0)
            return OneCase[0];
        return null; 
    }
    
    /*public void UpdateCasesAfterUpdateFamily(string familyId, string coordinatorUserName)
    {
        if (casesByFamilysMap.containsKey(familyId))
        {
            List<Case__c> listToUpdate = casesByFamilysMap.get(familyId);
            for (integer i = 0; i < listToUpdate.size(); i++)
            {
                listToUpdate[i].CoordinatorUserName__c = coordinatorUserName;
            }
            if (listToUpdate != null && listToUpdate.size() > 0)
                update listToUpdate;
        }
    }*/
    //v2
    public List<Case__c>UpdateCasesAfterUpdateFamily(string familyId, string coordinatorUserName, String OwnerId)
    {
        List<Case__c> listToUpdate = new List<Case__c> ();
        if (casesByFamilysMap.containsKey(familyId))
        {
            listToUpdate = casesByFamilysMap.get(familyId);
            for (Case__c c : listToUpdate )  {
                //c.CoordinatorUserName__c = coordinatorUserName;
                c.OwnerId = OwnerId;
            }
        }
        
        return listToUpdate ;
    }
    /*
    public void UpdateCasesAfterUpdateChildren(string childId, string coordinatorUserName)
    {
        if (casesByChildrenMap.containsKey(childId))
        {
            List<Case__c> listToUpdate = casesByChildrenMap.get(childId);
            for (integer i = 0; i < listToUpdate.size(); i++)
            {
                listToUpdate[i].CoordinatorUserName__c = coordinatorUserName;
            }
            if (listToUpdate != null && listToUpdate.size() > 0)
                update listToUpdate;
        }
    }*/
    
    //v2
    public void UpdateCasesAfterUpdateChildren(Set<Id> caseIdsToUpdate,Map<Id,Children__c> triggerNewMap)
    {
        List<Case__c> listToUpdate = new List<Case__c> ();
        
        //system.debug(casesByChildrenMap);
        for(String childId :caseIdsToUpdate){
            //system.debug('************************* childId ************************* '+childId );
            //system.debug('************************* casesByChildrenMap.get(childId)************************* '+casesByChildrenMap.get(childId));
            if (triggerNewMap.containsKey(childId))     {
                Children__c child = triggerNewMap.get(childId);
                List<Case__c> childCases = casesByChildrenMap.get(childId);
                if(childCases != null && childCases.size()> 0 ){
                    for (Case__c c : childCases )  {
                        //c.CoordinatorUserName__c = child.CoordinatorUserName__c ;
                        c.OwnerId = child.OwnerId ;
                    }
                    listToUpdate.addAll(childCases );
                }
            }
        }
        
        
        if ( listToUpdate.size() > 0)
            update listToUpdate;

    }
    
    public list<CObjectsHistory> GetCaseHistories()
    {
        Case__History[] caseHistories = [select ParentId, CreatedDate, CreatedBy.Name, CreatedById, Field, NewValue, OldValue from 
                                                Case__History where ParentId = :pCase.id order by CreatedDate desc];
        list<CObjectsHistory> ret = new list<CObjectsHistory>();
        for (integer i = 0; i < caseHistories.size(); i++)
        {
            ret.add(new CObjectsHistory(caseHistories[i].Field, caseHistories[i].CreatedBy.Name, caseHistories[i].OldValue,
                                        caseHistories[i].NewValue, caseHistories[i].CreatedDate, Schema.SObjectType.ActivityApproval__c.fields.getMap()));
        }
        return ret;
    }
    
    public void ForTest()
    {
        //Case__c[] cases = [select name, id, ChildName__c,FamilyName__c, Priority__c, Status__c, Date__c from Case__c];
        //getMessageType(true);
        //getMessageType(false);
        //getNowDateAndTime();
        //getMessageLink(cases[0]);
    }
    
    public static Case__c GetCaseById(string id)
    {
        //Commented because of the second SELECT
        // Case__c[] cases = [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Priority__c, NoteToPrintPage__c,
        //                           Type__c, name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, 
        //                           LastWriterNote__c, ToMyAttention2__c, CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, 
        //                           Description__c, SendEmail__c, PreviousAssigneeUser__c, TotalHoursToCalculate__c, AssigneeUsersList__c, 
        //                           SendEmailToManager__c,OwnerId,
        //                           (select IsPrivate, Title, Body, CreatedById, CreatedDate, LastModifiedById, LastModifiedDate From Notes)
        //                           from Case__c where id = :id];
        Case__c[] cases = [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Priority__c, NoteToPrintPage__c,
                                  Type__c, name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, 
                                  LastWriterNote__c, ToMyAttention2__c, CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, 
                                  Description__c, SendEmail__c, PreviousAssigneeUser__c, TotalHoursToCalculate__c, AssigneeUsersList__c, 
                                  SendEmailToManager__c,OwnerId from Case__c where id = :id];
        if (cases.size() > 0)
            return cases[0];
        return null;
    }
    
    public static list<Case__c> GetCasesById(list<string> ids)
    {
        return [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Priority__c, NoteToPrintPage__c,
                Type__c, name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, 
                LastWriterNote__c, ToMyAttention2__c, CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, 
                Description__c, SendEmail__c, PreviousAssigneeUser__c, TotalHoursToCalculate__c, AssigneeUsersList__c, OwnerId,
                SendEmailToManager__c from Case__c where id in :ids];
    }
    
    public static map<string, Case__c> GetCasesMap()
    {
        Case__c[] cases = [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Priority__c, NoteToPrintPage__c,
                                  Type__c, name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, 
                                  LastWriterNote__c, ToMyAttention2__c, CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, 
                                  Description__c, SendEmail__c, PreviousAssigneeUser__c, TotalHoursToCalculate__c, AssigneeUsersList__c, 
                                  SendEmailToManager__c from Case__c];
        map<string, Case__c> ret = new map<string, Case__c>();
        for (Case__c mycase : cases)
        {
            ret.put(mycase.id, mycase);
        }
        return ret;
    }
    
    public static map<Id, Case__c> GetCasesMap2(Set<String> casesIds)
    {
        map<Id, Case__c> cases = new map<Id, Case__c>( [select Comments__c, AssigneeUser__c, Status__c, Origin__c, Subject__c, Priority__c, NoteToPrintPage__c,
                                  Type__c, name, WhoId__c, id, Date__c, EndTime__c, StartTime__c, AssignedUsers__c, SendEmailToCreator__c, 
                                  LastWriterNote__c, ToMyAttention2__c, CoordinatorUserName__c, OwnerName__c, ChildName__c, FamilyName__c, 
                                  Description__c, SendEmail__c, PreviousAssigneeUser__c, TotalHoursToCalculate__c, AssigneeUsersList__c, 
                                  SendEmailToManager__c from Case__c where Id in: casesIds]);
        return cases;
    }
    
    public void AddOrRemoveFromToMyAttention()
    {
        list<string> attentionNewList = new list<string>();
        if (pCase.ToMyAttention2__c != null && pCase.ToMyAttention2__c.contains(system.Userinfo.getName()))
            attentionNewList = RemoveAndGetAttentionNewList();
        else
            attentionNewList = AddAndGetAttentionNewList();
        pCase.ToMyAttention2__c = '';
        for (string attentionNew : attentionNewList)
        {
            pCase.ToMyAttention2__c += attentionNew;
        }
        if (pCase.id != null)
            update pCase;
    }
    
    private list<string> AddAndGetAttentionNewList()
    {
        list<string> ret = new list<string>();
        list<string> attentionOldList = new list<string>();
        if (pCase.ToMyAttention2__c != null)
            attentionOldList = pCase.ToMyAttention2__c.split(';');
        for (string attentionOld : attentionOldList)
        {
            if (!attentionOld.contains(system.Userinfo.getName()))
                ret.add(attentionOld);
        }
        ret.add(system.Userinfo.getName() + ';');
        return ret;
    }
    
    private list<string> RemoveAndGetAttentionNewList()
    {
        list<string> ret = new list<string>();
        list<string> attentionOldList = new list<string>();
        if (pCase.ToMyAttention2__c != null)
            attentionOldList = pCase.ToMyAttention2__c.split(';');
        for (string attentionOld : attentionOldList)
        {
            if (!attentionOld.contains(system.Userinfo.getName()))
                ret.add(attentionOld);
        }
        return ret;
    }
}