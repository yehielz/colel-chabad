public with sharing class CRequestInCase 
{
	public RequestForTreatment__c pRequestForTreatment { get; set; }
	//private map<string, list<TutorLine__c>> pProfessionsByTutor { get; set; }
	private CGeneralDataInCasePage pGeneralData { get; set; }
	public list<selectOption> professionsOptions { get; set; }
	public integer index { get; set; }
	
	public CRequestInCase(RequestForTreatment__c requestForTreatment, /*map<string, list<TutorLine__c>> professionsByTutor,*/ 
						  integer i, CGeneralDataInCasePage generalData)
	{
		pGeneralData = generalData;
		pRequestForTreatment = requestForTreatment;
		//pProfessionsByTutor = professionsByTutor;
		index = i;
		if (pRequestForTreatment.ProfessionTypeName__c == null && pGeneralData.pProfessionTypeMap.values().size() > 0)
			pRequestForTreatment.ProfessionTypeName__c = pGeneralData.pProfessionTypeMap.values()[0].id;
		OnChangeTutorName();
	}
	
	public PageReference OnChangeTutorName()
	{
		professionsOptions = new list<selectOption>();
		/*if (pRequestForTreatment.TutorName__c == null)
		{*/
			SetAllProfessionsOptions();
		/*}
		else
		{
			if (pProfessionsByTutor.containsKey(pRequestForTreatment.TutorName__c))
			{
				SetProfessionsOptions(pProfessionsByTutor.get(pRequestForTreatment.TutorName__c));
			}
		}*/
		//pRequestForTreatment.ProfessionName__c = pRequestForTreatment.ProfessionsList__c;
		return null;
	}
	
	private void SetProfessionsOptions(list<TutorLine__c> professionsList)
	{
		for (TutorLine__c professionInTutor : professionsList)
		{
			if (professionInTutor.ProfessionName__r.ProfessionTypeName__c != null && 
				professionInTutor.ProfessionName__r.ProfessionTypeName__c == pRequestForTreatment.ProfessionTypeName__c)
				professionsOptions.add(new selectOption(professionInTutor.ProfessionName__c, professionInTutor.ProfessionName__r.name));
		}
		professionsOptions.add(new SelectOption(pGeneralData.pOtherProfession.id, pGeneralData.pOtherProfession.name));
	}
	
	private void SetAllProfessionsOptions()
	{
		for (Profession__c profession : pGeneralData.pAllProfessionsList)
		{
			if (profession.ProfessionTypeName__c == pRequestForTreatment.ProfessionTypeName__c)
			{
				professionsOptions.add(new selectOption(profession.id, profession.name));
			}
		}
		professionsOptions.add(new SelectOption(pGeneralData.pOtherProfession.id, pGeneralData.pOtherProfession.name));
	} 
	
	public boolean bWeeklyHours 
	{ 
		get
		{
			boolean ret = true;
	    	if(pGeneralData.pProfessionTypeMap.containsKey(pRequestForTreatment.ProfessionTypeName__c))
	    	{
	    		string paymentType = pGeneralData.pProfessionTypeMap.get(pRequestForTreatment.ProfessionTypeName__c).PaymentType__c;
	    		if (paymentType != CObjectNames.HoursPerWeek)
	    			ret = false;
	    	}
	    	else
	    		ret = false;
    		return ret;
		}
	}
	public boolean bMeetings
	{ 
		get
		{
			boolean ret = true;
	    	if(pGeneralData.pProfessionTypeMap.containsKey(pRequestForTreatment.ProfessionTypeName__c))
	    	{
	    		string paymentType = pGeneralData.pProfessionTypeMap.get(pRequestForTreatment.ProfessionTypeName__c).PaymentType__c;
	    		if (paymentType != CObjectNames.Meetings)
	    			ret = false;
	    	}
	    	else
	    		ret = false;
    		return ret;
		}
	}
	public boolean bApprovalAmount{ get{ return !bWeeklyHours && !bMeetings; } }
	public string HoursOrAmountColumnLabel 
	{
		get
		{
			if (bWeeklyHours)
				return 'שעות שבועיות';
			else if (bMeetings)
				return 'מספר מפגשים';
			else 
				return 'סכום הבקשה';
		}
	}
	public void OnChangeProfessionType() 
	{ 
		OnChangeTutorName();
		if (professionsOptions.size() == 1 && professionsOptions[0].getLabel() == CObjectNames.Other)
			pRequestForTreatment.ProfessionName__c = pGeneralData.pOtherProfession.id;
		//pRequestForTreatment.ProfessionName__c = pRequestForTreatment.ProfessionsList__c;
	}
	
	public boolean bOther 
	{ 
		get
		{
			if (pRequestForTreatment.ProfessionName__c != null && pGeneralData.pProfessionsMap.containsKey(pRequestForTreatment.ProfessionName__c))
	    	{
	    		if (pGeneralData.pProfessionsMap.get(pRequestForTreatment.ProfessionName__c).name == CObjectNames.Other)
	    			return true;
	    	}
	    	//pRequestForTreatment.ProfessionName__c = pRequestForTreatment.ProfessionName__c;
	    	return false;
		}
	}
	public void OnChangeProfession() 
	{ 
		//pRequestForTreatment.ProfessionName__c = pRequestForTreatment.ProfessionsList__c;
	}
}