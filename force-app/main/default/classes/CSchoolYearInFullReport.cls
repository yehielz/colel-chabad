public with sharing class CSchoolYearInFullReport 
{
	public Children__c pChild { get; set; }
	public list<CActivityApprovalInFullReport> pActivityApprovals { get; set; }
	public list<CProfessionInFullReport> pProfessions { get; set; }
	public list<CChildProgressInFullReport> pChildProgresses { get; set; }
	public string pYearName { get; set; }
	public date pStartYearDate { get; set; }
	public date pEndYearDate { get; set; }
	private map<string, boolean> pView { get; set; }
	
	public CSchoolYearInFullReport(Children__c child, string mYearName, CChildProgressInFullReport mChildProgress, map<string, boolean> view)
	{
		pChild = child;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pProfessions = new list<CProfessionInFullReport>();
		pChildProgresses = new list<CChildProgressInFullReport>();
		if (mChildProgress != null)
		{
			pChildProgresses.add(mChildProgress);
		}
		pYearName = mYearName;
		SetYearDates();
		pView = view;
	}
	
	public CSchoolYearInFullReport(Children__c child, string mYearName, CActivityApprovalInFullReport mActivityApprovals, map<string, boolean> view)
	{
		pChild = child;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pProfessions = new list<CProfessionInFullReport>();
		pChildProgresses = new list<CChildProgressInFullReport>();
		if (mActivityApprovals != null)
		{
			pActivityApprovals.add(mActivityApprovals);
		}
		pYearName = mYearName;
		SetYearDates();
		pView = view;
	}
	
	public CSchoolYearInFullReport(Children__c child, string mYearName, ChildProgress__c mChildProgress, map<string, boolean> view)
	{
		pChild = child;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pProfessions = new list<CProfessionInFullReport>();
		pChildProgresses = new list<CChildProgressInFullReport>();
		if (mChildProgress != null)
		{
			pChildProgresses.add(new CChildProgressInFullReport(mChildProgress, view));
		}
		pYearName = mYearName;
		SetYearDates();
		pView = view;
	}
	
	public CSchoolYearInFullReport(Children__c child, string mYearName, ActivityApproval__c mActivityApprovals, map<string, boolean> view)
	{
		pChild = child;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pProfessions = new list<CProfessionInFullReport>();
		pChildProgresses = new list<CChildProgressInFullReport>();
		if (mActivityApprovals != null)
		{
			pActivityApprovals.add(new CActivityApprovalInFullReport(mActivityApprovals, view));
		}
		pYearName = mYearName;
		SetYearDates();
		pView = view;
	}
	
	public CSchoolYearInFullReport(Children__c child, string mYearName, list<ChildProgress__c> mChildProgresses, map<string, boolean> view)
	{
		pChild = child;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pProfessions = new list<CProfessionInFullReport>();
		pChildProgresses = new list<CChildProgressInFullReport>();
		if (mChildProgresses != null)
		{
			for (ChildProgress__c progress : mChildProgresses)
			{
				pChildProgresses.add(new CChildProgressInFullReport(progress, view));
			}
		}
		pYearName = mYearName;
		SetYearDates();
		pView = view;
	}
	
	public CSchoolYearInFullReport(Children__c child, string mYearName, list<ActivityApproval__c> mActivityApprovals, map<string, boolean> view)
	{
		pChild = child;
		pActivityApprovals = new list<CActivityApprovalInFullReport>();
		pProfessions = new list<CProfessionInFullReport>();
		pChildProgresses = new list<CChildProgressInFullReport>();
		if (mActivityApprovals != null)
		{
			for (ActivityApproval__c approval : mActivityApprovals)
			{
				pActivityApprovals.add(new CActivityApprovalInFullReport(approval, view));
			}
		}
		pYearName = mYearName;
		SetYearDates();
		pView = view;
	}
	
	private void SetYearDates()
	{
		if (pYearName != null && pYearName.split('-').size() == 2)
		{
			string[] years = pYearName.split('-');
			integer yearStart = integer.valueOf(years[0]);
			integer yearEnd = integer.valueOf(years[1]); 
			pStartYearDate = date.newInstance(yearStart, 9, 1);
			pEndYearDate = date.newInstance(yearEnd, 9, 0);
		}
	}
	
	public void AddProgress(ChildProgress__c progress)
	{
		pChildProgresses.add(new CChildProgressInFullReport(progress, pView));
	}
	
	public void AddProgress(CChildProgressInFullReport progress)
	{
		pChildProgresses.add(progress);
	}
	
	public void AddApproval(ActivityApproval__c approval)
	{
		pActivityApprovals.add(new CActivityApprovalInFullReport(approval, pView));
	}
	
	public void AddApproval(CActivityApprovalInFullReport approval)
	{
		pActivityApprovals.add(approval);
	}
	
	public void AddReports(map<string, list<ActivityReport__c>> reportsByApprovals)
	{
		for (CActivityApprovalInFullReport approval : pActivityApprovals)
		{
			if (reportsByApprovals.containsKey(approval.pActivityApproval.id))
				approval.AddReports(reportsByApprovals.get(approval.pActivityApproval.id));
		}
	}
	
	public void SetDetails(map<string, Profession__c> professionsMap, map<string, Case__c> casesMap, map<string, RequestForTreatment__c> requestsMap, map<string, list<DiplomaLine__c>> diplomaLinesMap, map<string, list<Exam__c>> examsMap)
	{
		list<CRequestForTreatmentInFullReport> requestsList = GetRequestsWithApprovals(requestsMap);
		pProfessions = GetProfessionsDetails(requestsList, professionsMap, casesMap, diplomaLinesMap, examsMap);
	}
	
	private list<CProfessionInFullReport> GetProfessionsDetails(list<CRequestForTreatmentInFullReport> requestsList, map<string, Profession__c> professionsMap, map<string, Case__c> casesMap, map<string, list<DiplomaLine__c>> diplomaLinesMap, map<string, list<Exam__c>> examsMap)
	{
		map<string, map<string, CCaseInFullReport>> casesByProfessionsMap = new map<string, map<string, CCaseInFullReport>>();
		for (CRequestForTreatmentInFullReport request : requestsList)
		{
			string professionId = request.pRequestForTreatment.ProfessionName__c;
			string caseId = request.pRequestForTreatment.CaseName__c;
			if (casesByProfessionsMap.containsKey(professionId))
			{
				if (casesByProfessionsMap.get(professionId).containsKey(caseId))
					casesByProfessionsMap.get(professionId).get(caseId).AddRequest(request);
				else
					casesByProfessionsMap.get(professionId).put(caseId, new CCaseInFullReport(casesMap.get(caseId), request, pView));
			}
			else
			{
				map<string, CCaseInFullReport> newCasesMap = new map<string, CCaseInFullReport>();
				newCasesMap.put(caseId, new CCaseInFullReport(casesMap.get(caseId), request, pView));
				casesByProfessionsMap.put(professionId, newCasesMap);
			}
		}
		list<string> professionsKeys = new list<string>();
		professionsKeys.addAll(casesByProfessionsMap.KeySet());
		list<CProfessionInFullReport> ret = new list<CProfessionInFullReport>();
		for (string key : professionsKeys)
		{
			CProfessionInFullReport profession = new CProfessionInFullReport(professionsMap.get(key), pView);
			if (casesByProfessionsMap.ContainsKey(key))
				profession.AddCases(casesByProfessionsMap.get(key).values());	
			if (diplomaLinesMap.ContainsKey(key))
				profession.AddDiplomaLines(diplomaLinesMap.get(key));	
			if (examsMap.ContainsKey(key))
				profession.AddExams(examsMap.get(key));	
			ret.add(profession);
		}
		return ret;
	}
	
	private list<CRequestForTreatmentInFullReport> GetRequestsWithApprovals(map<string, RequestForTreatment__c> requestsMap)
	{
		map<string, CRequestForTreatmentInFullReport> ret = new map<string, CRequestForTreatmentInFullReport>();
		for (CActivityApprovalInFullReport approval : pActivityApprovals)
		{
			string requestId = approval.pActivityApproval.RequestForTreatmentName__c;
			if (ret.containsKey(requestId))
				ret.get(requestId).AddApproval(approval);
			else
				ret.put(requestId, new CRequestForTreatmentInFullReport(requestsMap.get(requestId), approval, pView));
		}
		return ret.values();
	}
}