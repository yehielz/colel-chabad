public with sharing class CTutorFacad 
{
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	
	private CTutorDb tutorDb;
	
	
	
	public CChildInTutor[] pTutorsByChild
	{
		get
		{
			return tutorDb.getChildrenByTutorName();
		}
	}
	
	public Tutor__c pTutor
	{
		get
		{
			return tutorDb.GetpTutor();
		}
	}
	
	public List<TutorLine__c> pTutorLines
	{
		get
		{
			return tutorDb.GetpTutorLines();
		}
	}
		
	public CTutorFacad(ApexPages.StandardController controller)
	{
		tutorDb = new CTutorDb(controller);
		errorMessage = '';
		if (pTutor.id == null)
			pageTitle = 'חונך חדש';
		else
			pageTitle = pTutor.name;
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		tutorDb.save();
		PageReference tutorViewPage = new ApexPages.StandardController(pTutor).view();
		return tutorViewPage;
	}
	
	public PageReference SaveFromPopup()
	{
		if (!Validation())
		{
			Cookie errorMessageCookie = new Cookie('errorMessage', 'false', null, -1, false);
			Cookie erorrMessageResultCookie = new Cookie('erorrMessageResult', errorMessage, null, -1, false);
			ApexPages.currentPage().setCookies(new Cookie[] { errorMessageCookie, erorrMessageResultCookie } );
			return null;
		}
		Cookie errorMessageCookie = new Cookie('errorMessage', 'true', null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { errorMessageCookie } );
		save();
		return null;
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CTutorValidator validator = new CTutorValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public void SetFilterdAndSortedList()
    {
    	tutorDb.SetFilterdAndSortedList();
    }
    
    public list<selectOption> professionsTypeList{ get{ return tutorDb.GetProfessionsTypeList(); } }
    public string pSelectedProfessionTypes { get{ return tutorDb.pSelectedProfessionTypes; } set{ tutorDb.pSelectedProfessionTypes = value; } }
}