public with sharing class CWebSiteSupporterHeadComponentCrl 
{
	private Support__c mSupporter;
	public Support__c pSupporter
	{
		get
		{
			return mSupporter;
		}
		set
		{
			if (value != null)
				mSupporter = value;
			else
				mSupporter = new Support__c();
		}
	}
	public boolean IsEnglish
	{ 
		get
		{ 
			boolean ret = false;
			if (pSupporter != null && pSupporter.LanguageForWS__c != null && pSupporter.LanguageForWS__c.contains('English'))
				ret = true;
			string lang = Apexpages.currentPage().getParameters().get('lang');
	    	if (lang != null && lang == 'en')
	    		ret = true;
	    	if (lang != null && lang == 'he')
	    		ret = false;
    		return ret; 
		} 
	}
	public string direction{ get{ return IsEnglish ? 'ltr' : 'rtl'; } }
	public string SupporterName{ get{ return IsEnglish != null && IsEnglish && pSupporter != null && pSupporter.EnglishName__c != null ? pSupporter.EnglishName__c : (pSupporter != null ? pSupporter.name : ''); } }
	
	public string pSelectedPage { get; set; }
	private string NewTabs { get; set; }
	public string pNewTabs 
	{ 
		get
		{
			if (NewTabs == null || NewTabs == '')
				NewTabs = '[]';
			return NewTabs;
		}
		set
		{
			NewTabs = value;
			if (NewTabs == null || NewTabs == '')
				NewTabs = '[]';
		} 
	}
	
	public string HomeUrl{ get{ return CObjectNames.WebSiteSupporterHome + (IsLastName ? '&n=n' : '') + (IsBlur ? '&b=b' : '') + '&lang=' + (IsEnglish ? 'en' : 'he'); } }
	public string ChildrenUrl{ get{ return CObjectNames.WebSiteSupporterChildren + (IsLastName ? '&n=n' : '') + (IsBlur ? '&b=b' : '') + '&lang=' + (IsEnglish ? 'en' : 'he'); } }
    
    public PageReference LogOut()
	{
		Cookie sessionIdCookie = new Cookie('sessionId', null, null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { sessionIdCookie } );
		CSessionIdDb.DeleteSessionsIdWithLastTime();
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
	
    public boolean IsBlur 
    { 
    	get
		{
			if (Apexpages.currentPage().getParameters().get('b') != null && Apexpages.currentPage().getParameters().get('b') == 'b')
	    		return true;
	    	return false;
		} 
	}
	
    public boolean IsLastName
    { 
    	get
		{
			if (Apexpages.currentPage().getParameters().get('n') != null && Apexpages.currentPage().getParameters().get('n') == 'n')
	    		return true;
	    	return false;
		} 
	}
}