public without sharing class CRequestForTreatmentDb 
{
	public static boolean IsAfterActivityApprovalOrChangeCoordinator { get; set; }
	private RequestForTreatment__c pRequestForTreatment { get; set; }
	private map<string, RequestForTreatment__c> allRequestForTreatmentMap;
	private map<string, decimal> weeklyHoursByRequestForTreatmentMap;
	private map<string, decimal> approvalAmountByRequestForTreatmentMap;
	private map<string, decimal> meetingsNumbersByRequestForTreatmentMap;
	private map<string, list<RequestForTreatment__c>> requestForTreatmentByChildMap;
	
	public CRequestForTreatmentDb(boolean isForFeed)
	{
		
	}
	
	public CRequestForTreatmentDb()
	{
		pRequestForTreatment = new RequestForTreatment__c();
	}
	
	public CRequestForTreatmentDb(ApexPages.StandardController controller)
	{
		pRequestForTreatment = (RequestForTreatment__c)controller.getRecord();
		if (pRequestForTreatment.id != null)
			SetRequestForTreatmentById(pRequestForTreatment.id);
		else
		{
			if (pRequestCase != null && pRequestCase.WhoId__c == CObjectNames.FamilyMember)
				pRequestForTreatment.ChildName__c = pRequestCase.ChildName__c;
			pRequestForTreatment.ProfessionName__c = GetProfessionIdByName(CObjectNames.ProfessionGroupPrivateLesson);
		}
	}
	
	private void SetRequestForTreatmentById(string id)
	{
		pRequestForTreatment = GetRequestForTreatmentById(id);
	}
	
	public void save()
	{
		Case__c oneCase = CCaseDb.GetCaseById(pRequestForTreatment.CaseName__c);
		if (oneCase.WhoId__c == CObjectNames.Family)
			pRequestForTreatment.ChildName__c = pRequestForTreatment.FamilyChildren__c;
		SetProfession();
		pRequestForTreatment.WeeklyHours__c = SetWeeklyHours(pRequestForTreatment);
		pRequestForTreatment.ApprovalAmount__c = SetApprovalAmount(pRequestForTreatment);
		pRequestForTreatment.MeetingsNumber__c = SetMeetingsNumber(pRequestForTreatment);
		//setCoordinatorUserName();
		SetStayReason();
		if (pRequestForTreatment.id == null)
			insert pRequestForTreatment;
		else
			update pRequestForTreatment;
	}
	
	private void SetStayReason()
	{
		if (pRequestForTreatment.RequestStatus__c != 'מושהה')
			pRequestForTreatment.StayReason__c = null;
	}
	/*
	private void setCoordinatorUserName()
	{
		Children__c[] children = [select name, id, CoordinatorUserName__c from Children__c where id = :pRequestForTreatment.ChildName__c];
		if (children.size() > 0)
			pRequestForTreatment.CoordinatorUserName__c = children[0].CoordinatorUserName__c;
	}*/
	
	private void SetProfession()
	{
		if (pRequestForTreatment.ProfessionName__c != CProfessionDb.GetOtherProfession().id)
			pRequestForTreatment.ProfessionOther__c = null;
	}
	
	private decimal SetWeeklyHours(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.ProfessionTypeName__c != null && pGeneralData.pProfessionTypeMap.containsKey(requestForTreatment.ProfessionTypeName__c) && 
	    	pGeneralData.pProfessionTypeMap.get(requestForTreatment.ProfessionTypeName__c).PaymentType__c != CObjectNames.HoursPerWeek)
			return null;
		return requestForTreatment.WeeklyHours__c;
	}
	
	private decimal SetMeetingsNumber(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.ProfessionTypeName__c != null && pGeneralData.pProfessionTypeMap.containsKey(requestForTreatment.ProfessionTypeName__c) && 
	    	pGeneralData.pProfessionTypeMap.get(requestForTreatment.ProfessionTypeName__c).PaymentType__c != CObjectNames.Meetings)
			return null;
		return requestForTreatment.MeetingsNumber__c;
	}
	
	private decimal SetApprovalAmount(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.ProfessionTypeName__c != null && pGeneralData.pProfessionTypeMap.containsKey(requestForTreatment.ProfessionTypeName__c) && 
	    	(pGeneralData.pProfessionTypeMap.get(requestForTreatment.ProfessionTypeName__c).PaymentType__c == CObjectNames.MonthlyPayment ||
	    	pGeneralData.pProfessionTypeMap.get(requestForTreatment.ProfessionTypeName__c).PaymentType__c == CObjectNames.OneTimePayment))
			return requestForTreatment.ApprovalAmount__c;
		return null;
	}
	
	public void SetAllRequestForTreatmentMap(list<string> requestsIds)
	{
		RequestForTreatment__c[] requestForTreatments = GetRequestForTreatmentsById(requestsIds);
		allRequestForTreatmentMap = new map<string, RequestForTreatment__c>();
		for (integer i = 0; i < requestForTreatments.size(); i++)
		{
			allRequestForTreatmentMap.put(requestForTreatments[i].id, requestForTreatments[i]);
		}
	}
	
	public void setRequestForTreatmentByChildMap(list<Children__c> children)
	{
		RequestForTreatment__c[] requestForTreatments = GetRequestForTreatmentByChildren(children);
		requestForTreatmentByChildMap = new map<string, list<RequestForTreatment__c>>();
		for (integer i = 0; i < requestForTreatments.size(); i++)
		{
			if (requestForTreatmentByChildMap.containsKey(requestForTreatments[i].ChildName__c))
				requestForTreatmentByChildMap.get(requestForTreatments[i].ChildName__c).add(requestForTreatments[i]);
			else
				requestForTreatmentByChildMap.put(requestForTreatments[i].ChildName__c, new list<RequestForTreatment__c> { requestForTreatments[i] });
		}
	}
	
	public RequestForTreatment__c GetpRequestForTreatment()
	{
		return pRequestForTreatment;
	}
	
	public List<SelectOption> GetOptionsFamilyChildren()
	{
		if (pRequestCase != null && pRequestCase.FamilyName__c != null)
			return GetOptionsChildrenByFamily();
		
		return GetAllChildrenOptions();
	}
	
	private List<SelectOption> GetOptionsChildrenByFamily()
	{
		List<SelectOption> OptionsFamilyChildren = new List<SelectOption>();
		CChildDb childDb = new CChildDb();
		Children__c[] children = childDb.GetChildrenByFamily(pRequestCase.FamilyName__c);
		for (integer i = 0; i < children.size(); i++)
		{
			OptionsFamilyChildren.add(new Selectoption(children[i].id, children[i].name));
		}
		return OptionsFamilyChildren;
	}
	
	private List<SelectOption> GetAllChildrenOptions()
	{
		List<SelectOption> childrenOptions = new List<SelectOption>();
		Children__c[] children = [select name, id from Children__c];
		for (integer i = 0; i < children.size(); i++)
		{
			childrenOptions.add(new Selectoption(children[i].id, children[i].name));
		}
		return childrenOptions;
	}
	
	public static RequestForTreatment__c GetRequestForTreatmentById(string id)
	{
		List<RequestForTreatment__c> requestForTreatment = [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													  ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													  FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													  TutorName__c, ChildName__c, ChildName__r.Picture__c,ProfessionName__r.ProfessionTypeName__r.Id,
													   ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,CaseName__r.OwnerId,
													  CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													 , SchoolYear__c from RequestForTreatment__c where id = :id];
		if(requestForTreatment.size() > 0)
			return requestForTreatment.get(0);
		return null;
	}
	
	public static list<RequestForTreatment__c> GetRequestForTreatmentsById(list<string> ids)
	{
		return [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
		  	   ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
			   FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
			   TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
			   CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c, SchoolYear__c from RequestForTreatment__c where id in :ids];
	}
	
	public static RequestForTreatment__c GetRequestForTreatmentWithProfessioNameById(string id)
	{
		RequestForTreatment__c requestForTreatment = [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													  ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													  FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													  TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
													  CreatedDate, ProfessionName__r.name, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													 , SchoolYear__c from RequestForTreatment__c where id = :id];
		return requestForTreatment; 
	}
	 
	public static List<RequestForTreatment__c> GetRequestForTreatmentList()
	{
		RequestForTreatment__c[] requestForTreatment = [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													   ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													   FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													   TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
													   CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													  , SchoolYear__c from RequestForTreatment__c];
		return requestForTreatment; 
	}
	
	public static Map<Id,RequestForTreatment__c> GetRequestForTreatmentMap(Set<String> requestIds)
	{
		Map<Id,RequestForTreatment__c>  requestForTreatment = new Map<Id,RequestForTreatment__c>([select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													   ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													   FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													   TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
													   CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													  , SchoolYear__c from RequestForTreatment__c where Id in :requestIds]);
		return requestForTreatment; 
	}
	
	public static List<RequestForTreatment__c> GetRequestForTreatmentByChildren(list<Children__c> children)
	{
		RequestForTreatment__c[] requestForTreatment = [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													   ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													   FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													   TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
													   CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													  , SchoolYear__c from RequestForTreatment__c where ChildName__c in :children];
		return requestForTreatment; 
	}
	
	public static List<RequestForTreatment__c> GetRequestForTreatmentByChildAndYear(string childId, string schoolYearId)
	{
		RequestForTreatment__c[] requestForTreatment = [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													   ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													   FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													   TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
													   CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													  , SchoolYear__c,TutorName__r.name, TutorName__r.id, CreatedByID from RequestForTreatment__c where ChildName__c = :childId and SchoolYear__c = :schoolYearId];
		return requestForTreatment; 
	}
	
	public static List<RequestForTreatment__c> GetRequestForTreatmentByChild(string childId)
	{
		RequestForTreatment__c[] requestForTreatment = [select name, id, IsOther__c, IsWeeklyHours__c, Comment__c, TeacherReviews__c, Reviews__c,
													   ManagerReviews__c, FamilyChildren__c, RequestStatus__c, ProfessionsList__c, ProfessionOther__c,
													   FromDate__c, ProfessionTypeName__c, ProfessionType__c, ApprovalAmount__c, CaseName__c, 
													   TutorName__c, ChildName__c, ChildName__r.Picture__c, ProfessionName__c, CoordinatorUserName__c, WeeklyHours__c,
													   CreatedDate, ToDate__c, MeetingsNumber__c, Fare__c, Description__c, StayReason__c
													  , SchoolYear__c,TutorName__r.name, TutorName__r.id, CreatedByID from RequestForTreatment__c where ChildName__c = :childId];
		return requestForTreatment; 
	}
	
	public List<SelectOption> GetProfessionTypesOptions()
    {
    	list<selectOption> ret = new list<selectOption>();
    	ProfessionType__c[] professionTypes = pGeneralData.pSortedProfessionTypes;
    	for (ProfessionType__c professionType : professionTypes)
    	{
    		ret.add(new selectOption(professionType.id, professionType.name));
    	}
    	return ret;
    }
	
	public List<SelectOption> GetProfessionsList()
    {
    	string professionTypeId = pRequestForTreatment.ProfessionTypeName__c;
    	if (pRequestForTreatment.ProfessionTypeName__c == null)
    		professionTypeId = GetDefaultProfessionType();
		//if (pRequestForTreatment.TutorName__c != null)
    	//	return GetProfessionsByTutor(professionTypeId);
    	return GetProfessionsOptions(professionTypeId);
    }
    
    private List<selectOption> GetProfessionsOptions(string professionTypeId)
    {
    	list<Profession__c> professions = CProfessionDb.GetProfessionsByTypeAndForUse(professionTypeId);
		list<SelectOption> ret = new list<SelectOption>();
    	for (Profession__c profession : professions)
    	{
    		ret.add(new SelectOption(profession.id, profession.name));
    	}
    	//Profession__c otherProfession = CProfessionDb.GetOtherProfession();
    	//ret.add(new SelectOption(otherProfession.id, otherProfession.name));
    	return ret;
    }
    /*
    private list<selectOption> GetProfessionsByTutor(string professionTypeId)
    {
    	TutorLine__c[] tutorLines =  [select name, id, ProfessionName__c, ProfessionName__r.name from TutorLine__c where 
    										 TutorName__c = :pRequestForTreatment.TutorName__c and ProfessionName__r.IsNotForUse__c != true and
    										 ProfessionName__r.ProfessionTypeName__c = :professionTypeId order by ProfessionName__r.name];
    	list<SelectOption> ret = new list<SelectOption>();
    	for (TutorLine__c tutorLine : tutorLines)
    	{
    		ret.add(new SelectOption(tutorLine.ProfessionName__c, tutorLine.ProfessionName__r.name));
    	}
    	Profession__c otherProfession = CProfessionDb.GetOtherProfession();
    	ret.add(new SelectOption(otherProfession.id, otherProfession.name));
    	return ret;
    }
    */
    private string GetDefaultProfessionType()
    {
    	if (pGeneralData.pProfessionTypeMap.values().size() > 0)
    		return pGeneralData.pProfessionTypeMap.values()[0].id;
    	return '';
    }
    
    public RequestForTreatment__c UpdateRequestForTreatmentStatus(string requestForTreatmentId)
    {
    	if (allRequestForTreatmentMap.containsKey(requestForTreatmentId))
    	{
    		RequestForTreatment__c request = allRequestForTreatmentMap.get(requestForTreatmentId);
	    	UpdateWeeklyHours(request);
	    	UpdateApprovalAmount(request);	
	    	UpdateTotalMeetings(request);
			return request;
    	}
    	return null;							
    }
    
    private void UpdateWeeklyHours(RequestForTreatment__c request)
    {
    	if (request.WeeklyHours__c == null)
    		return;
    	decimal weeklyHours = 0;
    	if (weeklyHoursByRequestForTreatmentMap.containsKey(request.id))
    		weeklyHours = weeklyHoursByRequestForTreatmentMap.get(request.id);
    	if (weeklyHours >= request.WeeklyHours__c)
			request.RequestStatus__c = CObjectNames.RequestForTreatmentStatusApproved;
		else
			request.RequestStatus__c = CObjectNames.RequestForTreatmentStatusPartialApproved;
    }
    
    private void UpdateApprovalAmount(RequestForTreatment__c request)
    {
    	if (request.ApprovalAmount__c == null)
    		return;
    	decimal approvalAmount = 0;
    	if (approvalAmountByRequestForTreatmentMap.containsKey(request.id))
    		approvalAmount = approvalAmountByRequestForTreatmentMap.get(request.id);
    	if (approvalAmount >= request.ApprovalAmount__c)
			request.RequestStatus__c = CObjectNames.RequestForTreatmentStatusApproved;
		else
			request.RequestStatus__c = CObjectNames.RequestForTreatmentStatusPartialApproved;
    }
    
    private void UpdateTotalMeetings(RequestForTreatment__c request)
    {
    	if (request.MeetingsNumber__c == null)
    		return;
    	decimal totalMeetings = 0;
    	if (meetingsNumbersByRequestForTreatmentMap.containsKey(request.id))
    		totalMeetings = meetingsNumbersByRequestForTreatmentMap.get(request.id);
    	if (totalMeetings >= request.MeetingsNumber__c)
			request.RequestStatus__c = CObjectNames.RequestForTreatmentStatusApproved;
		else
			request.RequestStatus__c = CObjectNames.RequestForTreatmentStatusPartialApproved;
    }
    
    public void SetSumsMaps(list<string> requestsIds)
    {
    	meetingsNumbersByRequestForTreatmentMap = new map<string, decimal>();
    	weeklyHoursByRequestForTreatmentMap = new map<string, decimal>();
    	approvalAmountByRequestForTreatmentMap = new map<string, decimal>();
    	AggregateResult [] totalSums = [select sum(WeeklyHours__c), sum(AmountApproved__c), sum(MeetingsNumber__c), RequestForTreatmentName__c from ActivityApproval__c 
    								    where RequestForTreatmentName__c in :requestsIds group by RequestForTreatmentName__c];
    	for (AggregateResult sum : totalSums)
    	{
    		weeklyHoursByRequestForTreatmentMap.put(string.valueOf(sum.get('RequestForTreatmentName__c')), (decimal)sum.get('expr0'));
    		approvalAmountByRequestForTreatmentMap.put(string.valueOf(sum.get('RequestForTreatmentName__c')), (decimal)sum.get('expr1'));
    		meetingsNumbersByRequestForTreatmentMap.put(string.valueOf(sum.get('RequestForTreatmentName__c')), (decimal)sum.get('expr2'));
    	}
    }
	
	
	//for List
	public Case__c pCase { get; set; }
	public list<CRequestInCase> pRequestsInCase { get; set; }
	public CRequestForTreatmentDb(Case__c extCase)
	{
		pCase = extCase;
		pRequestsInCase = new list<CRequestInCase>();
		SetpRequestsInCaseBypCaseId();
	}
	
	private void SetpRequestsInCaseBypCaseId()
	{
		list<RequestForTreatment__c> tempRequestsForTreatment = new list<RequestForTreatment__c>{ new RequestForTreatment__c() };
		if (pCase.id != null)
		{
			tempRequestsForTreatment = [select Comment__c, FamilyChildren__c, CaseName__c, ApprovalAmount__c,
										ChildName__c, ChildName__r.Picture__c, id, name, FromDate__c, ToDate__c, Reviews__c, ProfessionTypeName__c,
										WeeklyHours__c, RequestStatus__c, ProfessionsList__c, TeacherReviews__c, Description__c,
										ManagerReviews__c, TutorName__c, ProfessionOther__c, ProfessionType__c, Fare__c,
										ProfessionTypeName__r.PaymentType__c, IsOther__c, IsWeeklyHours__c, CreatedDate, ProfessionName__c 
										from RequestForTreatment__c where CaseName__c = :pCase.id];
		}
		FillpRequestsInCase(tempRequestsForTreatment);			
	}
	
	public static list<RequestForTreatment__c> GetRequestsByCaseId(string caseId)
	{
		list<RequestForTreatment__c> tempRequestsForTreatment = new list<RequestForTreatment__c>{ new RequestForTreatment__c() };
		if (caseId != null)
		{
			tempRequestsForTreatment = [select Comment__c, FamilyChildren__c, CaseName__c, ApprovalAmount__c, CoordinatorUserName__c,
										ChildName__c, ChildName__r.Picture__c, id, name, FromDate__c, ToDate__c, Reviews__c, ProfessionTypeName__c,
										WeeklyHours__c, RequestStatus__c, ProfessionsList__c, TeacherReviews__c, Description__c,
										ManagerReviews__c, TutorName__c, ProfessionOther__c, ProfessionType__c, Fare__c,
										ProfessionTypeName__r.PaymentType__c, IsOther__c, IsWeeklyHours__c, CreatedDate, ProfessionName__c 
										from RequestForTreatment__c where CaseName__c = :caseId];
		}
		return tempRequestsForTreatment;			
	}
	
	private void FillpRequestsInCase(list<RequestForTreatment__c> tempRequestsForTreatmen)
	{
		//map<string, list<TutorLine__c>> professionsByTutors = GetProfessionsByTutorsMap();
		SchoolYear__c[] schoolYear = [select name, id, HalfSchoolYear__c, EndOfSchoolYear__c
									  from SchoolYear__c where IsActive__c = true];
		Date toDay = dateTime.Now().Date();
		date toDate;
		if (!schoolYear.isEmpty())
		{
			if (schoolYear[0].HalfSchoolYear__c > toDay)
				toDate = schoolYear[0].HalfSchoolYear__c;
			else
				toDate = schoolYear[0].EndOfSchoolYear__c;
		}
		for (integer i = 0; i < tempRequestsForTreatmen.size(); i++)
		{
			if (tempRequestsForTreatmen[i].id == null)
			{
				tempRequestsForTreatmen[i].FromDate__c = datetime.now().date();
				tempRequestsForTreatmen[i].ToDate__c = toDate;
			}
			pRequestsInCase.add(new CRequestInCase(tempRequestsForTreatmen[i]/*, professionsByTutors*/, i, pGeneralData));
		}
	}
	/*
	private map<string, list<TutorLine__c>> GetProfessionsByTutorsMap()
	{
		TutorLine__c[] tutorLines = [select name, id, ProfessionName__c, ProfessionName__r.name, ProfessionName__r.ProfessionTypeName__c, 
											ProfessionName__r.ProfessionTypeName__r.PaymentType__c, TutorName__c from TutorLine__c];
		map<string, list<TutorLine__c>> ret = new map<string, list<TutorLine__c>>();
		for (integer i = 0; i < tutorLines.size(); i++)
		{
			if (ret.containsKey(tutorLines[i].TutorName__c))
				ret.get(tutorLines[i].TutorName__c).add(tutorLines[i]);
			else
				ret.put(tutorLines[i].TutorName__c, new list<TutorLine__c> { tutorLines[i] });
		}	
		return ret;
	}*/
	
	public List<CRequestInCase> GetpRequestsInCase()
	{
		return pRequestsInCase;
	}
	
	public void AddRequestToList()
	{
		//map<string, list<TutorLine__c>> professionsByTutors = GetProfessionsByTutorsMap();
		RequestForTreatment__c tempRequest = new RequestForTreatment__c();
		tempRequest.FromDate__c = datetime.now().date();
		SchoolYear__c[] schoolYear = [select name, id, HalfSchoolYear__c, EndOfSchoolYear__c
									  from SchoolYear__c where IsActive__c = true];
		Date toDay = dateTime.Now().Date();
		date toDate;
		if (!schoolYear.isEmpty())
		{
			if (schoolYear[0].HalfSchoolYear__c > toDay)
				toDate = schoolYear[0].HalfSchoolYear__c;
			else
				toDate = schoolYear[0].EndOfSchoolYear__c;
		}
		tempRequest.ToDate__c = toDate;
		tempRequest.RequestStatus__c = 'חדש';
		tempRequest.ProfessionName__c = GetProfessionIdByName(CObjectNames.ProfessionGroupPrivateLesson);
		pRequestsInCase.add(new CRequestInCase(tempRequest/*, professionsByTutors*/, pRequestsInCase.size(), pGeneralData));
	}
	
	private string GetProfessionIdByName(string name)
	{
		string res;
		Profession__c[] p = [select name from Profession__c where name = :name];
		if (p.size() > 0)
			return p[0].id;
		else
			return null;
	}
	
	public void InsertpRequestsInCase(boolean IsChild)
	{
		List<RequestForTreatment__c> toUpsert = new List<RequestForTreatment__c>();
		//List<RequestForTreatment__c> toDelete = new List<RequestForTreatment__c>();
		for (CRequestInCase requestInCase : pRequestsInCase)
		{
			if (requestInCase.pRequestForTreatment.FamilyChildren__c != null && requestInCase.pRequestForTreatment.FamilyChildren__c.trim() != '' && requestInCase.pRequestForTreatment.FamilyChildren__c.trim() != 'null')
				requestInCase.pRequestForTreatment.ChildName__c = requestInCase.pRequestForTreatment.FamilyChildren__c;
			//requestInCase.pRequestForTreatment.CoordinatorUserName__c = pCase.CoordinatorUserName__c;
			requestInCase.pRequestForTreatment.WeeklyHours__c = SetWeeklyHours(requestInCase.pRequestForTreatment);
			requestInCase.pRequestForTreatment.ApprovalAmount__c = SetApprovalAmount(requestInCase.pRequestForTreatment);
			requestInCase.pRequestForTreatment.Description__c = IsChild ? pCase.Description__c : requestInCase.pRequestForTreatment.Description__c;
			if ((requestInCase.pRequestForTreatment.WeeklyHours__c != null && requestInCase.pRequestForTreatment.WeeklyHours__c > 0) || 
				(requestInCase.pRequestForTreatment.ApprovalAmount__c != null && requestInCase.pRequestForTreatment.ApprovalAmount__c > 0) || 
				(requestInCase.pRequestForTreatment.MeetingsNumber__c != null && requestInCase.pRequestForTreatment.MeetingsNumber__c > 0))
			{
				if (requestInCase.pRequestForTreatment.id == null)
					requestInCase.pRequestForTreatment.CaseName__c = pCase.id;
				toUpsert.add(requestInCase.pRequestForTreatment);
			}
			//else if (requestInCase.pRequestForTreatment.id != null)
				//toDelete.add(requestInCase.pRequestForTreatment);
		}
		if (toUpsert.size() > 0)
			upsert toUpsert;
		//if (toDelete.size() > 0)
			//delete toDelete;
	}
	
	private string GetTutorId(string tutorName)
	{
		if (tutorName != null)
		{
			Tutor__c[] tutor = [select name, id from Tutor__c where name = :tutorName];
			if (tutor.size() > 0)
				return tutor[0].id;
		}
		return null;
	}
	
	public void FillChildInRequestsList(string childId)
	{
		for (integer i = 0; i < pRequestsInCase.size(); i++)
		{
			pRequestsInCase[i].pRequestForTreatment.ChildName__c = childId;
		}
	}
	/*
	public void UpdateRequestForTreatmentAfterUpdateChild(string childId, string coordinatorUserName){
		if (requestForTreatmentByChildMap.containsKey(childId))
		{
			list<RequestForTreatment__c> requestForTreatments = requestForTreatmentByChildMap.get(childId);
			for (integer i = 0; i < requestForTreatments.size(); i++)
			{
				requestForTreatments[i].CoordinatorUserName__c = coordinatorUserName;
			}
			if (requestForTreatments.size() > 0)
			{
				CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
				update requestForTreatments;
			}
		}
	}
	*/
	//v2
	/*
	public void UpdateRequestForTreatmentAfterUpdateChild(Set<Id> childIds, Map<Id,Children__c> triggerNewMap){
		list<RequestForTreatment__c> requestForTreatmentsToUpdate = new list<RequestForTreatment__c> ();
		for(Id childId :childIds){
			if (requestForTreatmentByChildMap.containsKey(childId)) {
	            list<RequestForTreatment__c> requestForTreatments = requestForTreatmentByChildMap.get(childId);
	            if(requestForTreatments != null && requestForTreatments.size() > 0 ){
		            for (RequestForTreatment__c r :requestForTreatments )  {
		                r.CoordinatorUserName__c = triggerNewMap.get(childId).CoordinatorUserName__c;
		            }
		            requestForTreatmentsToUpdate.addAll(requestForTreatments);
	            }
	            
	        }
		}
		
		if (requestForTreatmentsToUpdate.size() > 0)  {
            CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
            update requestForTreatmentsToUpdate;
        }
		
	}*/
	
	public void forTest()
	{
		GetAllChildrenOptions();
		Tutor__c[] tutor = [select name, id from Tutor__c];
		GetTutorId(tutor[0].name);
	}
	
	public list<CObjectsHistory> GetRequestForTreatmentHistories()
	{
		RequestForTreatment__History[] requestForTreatmentHistories = [select ParentId, CreatedDate, CreatedBy.Name, CreatedById, Field, NewValue, OldValue from 
        										RequestForTreatment__History where ParentId = :pRequestForTreatment.id order by CreatedDate desc];
	  	list<CObjectsHistory> ret = new list<CObjectsHistory>();
	  	for (integer i = 0; i < requestForTreatmentHistories.size(); i++)
	  	{
	  		ret.add(new CObjectsHistory(requestForTreatmentHistories[i].Field, requestForTreatmentHistories[i].CreatedBy.Name, requestForTreatmentHistories[i].OldValue,
	  									requestForTreatmentHistories[i].NewValue, requestForTreatmentHistories[i].CreatedDate, Schema.SObjectType.ActivityApproval__c.fields.getMap()));
	  	}
  		return ret;
	}
	/*
	public static map<string, RequestForTreatment__c> GetRequestForTreatmentsMap()
 	{
 		RequestForTreatment__c[] requestForTreatments = CRequestForTreatmentDb.GetRequestForTreatmentList();
		map<string, RequestForTreatment__c> ret = new map<string, RequestForTreatment__c>();
		for (RequestForTreatment__c requestForTreatment : requestForTreatments)
		{
			ret.put(requestForTreatment.id, requestForTreatment);
		}
		return ret;
 	}*/
 	
 	private CGeneralDataInCasePage generalData;
 	public CGeneralDataInCasePage pGeneralData
 	{
 		get
 		{
 			if (generalData == null)
 			{
 				generalData = new CGeneralDataInCasePage();
 			}
 			return generalData;
 		}
 	}
 	
 	private Case__c requestCase;
    public Case__c pRequestCase
    {
    	get
    	{ 
    		if (requestCase == null)
    		{
    			CCaseDb caseDb = new CCaseDb(); 
				if (pRequestForTreatment.CaseName__c == null)
					SetCaseName();
				requestCase = caseDb.GetCaseByCaseId(pRequestForTreatment.CaseName__c);
    		}
    		return requestCase;
    	}
    }
 	
 	private string lastDiplomaDate;
    public string pLastDiplomaDate
    {
    	get
    	{
    		if (lastDiplomaDate == null)
    		{
    			lastDiplomaDate = '';
    			Diploma__c[] diplomas = [select id, name, Date__c from diploma__c where ChildName__c = :pRequestForTreatment.ChildName__c and Date__c != null order by Date__c desc limit 1];
    			if (diplomas.size() > 0)
    				lastDiplomaDate = CObjectNames.GetFullNumberInString(diplomas[0].Date__c.day()) + '/' + CObjectNames.GetFullNumberInString(diplomas[0].Date__c.month()) + '/' + diplomas[0].Date__c.year();
    		}
    		return lastDiplomaDate;
    	}
    }
    
    private void SetCaseName()
    {
    	if (Apexpages.currentPage().getParameters().get('caseId') != null && Apexpages.currentPage().getParameters().get('caseId') != '')
    		pRequestForTreatment.CaseName__c = Apexpages.currentPage().getParameters().get('caseId');
    }
}