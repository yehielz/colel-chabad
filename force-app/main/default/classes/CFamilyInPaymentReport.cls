public with sharing class CFamilyInPaymentReport
{
	public Family__c pFamily { get; set; }
	public list<CChildInList> pChildrenList { get; set; }
	public string pChildrenJson{ get{ return Json.serialize(pChildrenList); } }
	public decimal pReportsAmount { get; set; }
	public decimal pFamilyActivitiesAmount { get; set; }
	public decimal pChildrenActivitiesAmount { get; set; }
	public decimal pTotalAmount { get; set; }
	
	public decimal pTotalAmountWithoutChildren { get; set; }
	
	public CFamilyInPaymentReport(Family__c mFamily, list<CChildInList> mChildren, decimal mFamilyActivitiesAmount)
	{
		
		pChildrenActivitiesAmount = 0;
		pReportsAmount = 0;
		pFamily = mFamily;
		pChildrenList = mChildren;
		pTotalAmount = mFamilyActivitiesAmount != null ? mFamilyActivitiesAmount : 0;
		pTotalAmountWithoutChildren = mFamilyActivitiesAmount != null ? mFamilyActivitiesAmount : 0;
		pFamilyActivitiesAmount = mFamilyActivitiesAmount != null ? mFamilyActivitiesAmount : 0;
		for (CChildInList child : mChildren)
		{
			pChildrenActivitiesAmount += child.ActivitiesAmount;
			pReportsAmount += child.ReportsAmount;
			pTotalAmount += child.TotalAmount;
		}
	}
	
	public class CChildInList
	{
		public string ChildName { get; set; }
		public string Id { get; set; }
		public decimal ReportsAmount { get; set; }
		public decimal ActivitiesAmount { get; set; }
		public decimal TotalAmount { get; set; }
		
		public CChildInList(Children__c mChild, decimal mReportsAmount, decimal mActivitiesAmount)
		{
			ChildName = mChild.Name;
			Id = mChild.Id;
			ReportsAmount = mReportsAmount == null ? 0 : mReportsAmount;
			ActivitiesAmount = mActivitiesAmount == null ? 0 : mActivitiesAmount;
			TotalAmount = ReportsAmount + ActivitiesAmount;
		}
	}
}