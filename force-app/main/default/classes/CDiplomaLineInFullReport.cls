public with sharing class CDiplomaLineInFullReport 
{
	public DiplomaLine__c pDiplomaLine { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsDiplomaInView{ get { return pView.containsKey('all') || pView.containsKey('diploma'); } }	
	
	
	public CDiplomaLineInFullReport(DiplomaLine__c diplomaLine, map<string, boolean> view)
	{
		pDiplomaLine = diplomaLine;
		pView = view;
	} 
	
	public string grade{ get{ return(pDiplomaLine.Grade__c == null || pDiplomaLine.Grade__c < 1) ? '' : string.valueOf(pDiplomaLine.Grade__c) + '%'; } }
	public string diplomaDate
	{ 
		get
		{ 
			if (pDiplomaLine.DiplomaName__r.Date__c != null)
				return CObjectNames.GetFullNumberInString(pDiplomaLine.DiplomaName__r.Date__c.day()) + '/' + CObjectNames.GetFullNumberInString(pDiplomaLine.DiplomaName__r.Date__c.month()) + '/' + pDiplomaLine.DiplomaName__r.Date__c.year();
			return '';
			 
		} 
	}
	public string half{ get{ return(pDiplomaLine.DiplomaName__r.Half__c == null || pDiplomaLine.DiplomaName__r.Half__c == '') ? '' : pDiplomaLine.DiplomaName__r.Half__c; } }
	
	public string diploma{ get{ return CObjectNames.GetHebrewWords((' ציון ' + grade + ' בתאריך ' + diplomaDate +' על '+ half),70);}}
	
	public string diplomaWS
	{
		get
		{ 
			string ret = '<span class="TitleInContent">ציון:</span> ' + grade;
			ret += ' | <span class="TitleInContent">תאריך:</span> ' + diplomaDate;
			ret += ' | <span class="TitleInContent">מחצית:</span> '+ half;
			return ret;
		}
	}
	
	public string diplomaWSEN
	{
		get
		{ 
			string ret = '<span class="TitleInContent">Grade:</span> ' + grade;
			ret += ' | <span class="TitleInContent">Date:</span> ' + diplomaDate;
			ret += ' | <span class="TitleInContent">Half:</span> '+ half;
			return ret;
		}
	}
}