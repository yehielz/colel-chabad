public with sharing class CCaseValidator 
{
	private CCaseFacad cCaseFacad { get; set; }
	public string ErrorMessage;
	
	public CCaseValidator(CCaseFacad ExtCaseFacad)
	{ 
		ErrorMessage = '';
		cCaseFacad = ExtCaseFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
	  	if (ret) 
	    	ret = ValidateManagerOrCreator();
    	if (ret) 
	    	ret = ValidateAssigneeUser();
	    if (ret) 
	    	ret = ValidateChildOrFamily();
    	if (ret) 
	    	ret = ValidateStatus();
    	if (ret) 
	    	ret = ValidateOrigin();
    	if (ret) 
	    	ret = ValidateType();
    	//if (ret) 
	    //	ret = ValidatePriority();
	    if (ret) 
	    	ret = ValidateSubject();
    	if (ret) 
	    	ret = ValidateDescription();
    	/*if (ret) 
	    	ret = ValidateComments();*/
    	if (ret) 
	    	ret = ValidateDate();
	    if (ret) 
	    	ret = ValidateStartTime();
	    if (ret) 
	    	ret = ValidateEndTime();
    	if (ret) 
	    	ret = ValidateStartTimeNotBigFromEndTime();
    	if (ret && cCaseFacad.IsTable()) 
	    	ret = ValidateRequestsListChildren();
    	    
	 	return ret;
	}
	
	private boolean ValidateManagerOrCreator()
	{
		string UserName = UserInfo.getName();
		Profile p = [select name from Profile where id = :UserInfo.getProfileId()];
		if (Userinfo.getUserId() != cCaseFacad.pCase.CreatedById && Userinfo.getUserId() != cCaseFacad.pCase.OwnerId && p.name != CObjectNames.ProfileSystemManager && p.name != CObjectNames.ProfileSystemManager2 && p.name != CObjectNames.ProfileSystemManager3)
		{
			if (cCaseFacad.pCase.Status__c == CObjectNames.CaseClosed)
			{
				ErrorMessage = CErrorMessages.CaseManagerOrCreator;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateAssigneeUser()
	{
		if (cCaseFacad.pCase.AssigneeUser__c == null)
		{
			ErrorMessage = CErrorMessages.CaseAssigneeUser;
			return false;
		}
		return true;
	}

	private boolean ValidateChildOrFamily()
	{
		if (cCaseFacad.pCase.ChildName__c == null && cCaseFacad.pCase.WhoId__c == CObjectNames.FamilyMember)
		{
			ErrorMessage = CErrorMessages.CaseChild;
			return false;
		}
		else if (cCaseFacad.pCase.FamilyName__c == null && cCaseFacad.pCase.WhoId__c == CObjectNames.Family)
		{
			ErrorMessage = CErrorMessages.CaseFamily;
			return false;
		}
		return true;
	}
	
	private boolean ValidateOrigin()
	{
		if (cCaseFacad.pCase.Origin__c == null)
		{
			ErrorMessage = CErrorMessages.CaseOrigin;
			return false;
		}
		return true;
	}
	
	private boolean ValidateStatus()
	{
		if (cCaseFacad.pCase.Status__c == null)
		{
			ErrorMessage = CErrorMessages.CaseStatus;
			return false;
		}
		return true;
	}
	
	private boolean ValidateType()
	{
		if (cCaseFacad.pCase.Type__c == null)
		{
			ErrorMessage = CErrorMessages.CaseType;
			return false;
		}
		return true;
	}
	
	/*private boolean ValidatePriority()
	{
		if (cCaseFacad.pCase.Priority__c == null)
		{
			ErrorMessage = CErrorMessages.CasePriority;
			return false;
		}
		return true;
	}*/
	
	private boolean ValidateSubject()
	{
		if (cCaseFacad.pCase.Subject__c == null)
		{
			ErrorMessage = CErrorMessages.CaseSubject;
			return false;
		}
		return true;
	}
	
	private boolean ValidateDescription()
	{
		if (cCaseFacad.pCase.Description__c == null)
		{
			ErrorMessage = CErrorMessages.CaseDescription;
			return false;
		}
		return true;
	}
	
	/*private boolean ValidateComments()
	{
		if (cCaseFacad.pCase.Comments__c == null)
		{
			ErrorMessage = CErrorMessages.CaseComments;
			return false;
		}
		return true;
	}*/
	
	private boolean ValidateDate()
	{
		if (cCaseFacad.pCase.Date__c == null)
		{
			ErrorMessage = CErrorMessages.CaseDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateStartTime()
	{
		string StartHour = cCaseFacad.StartHour == null ? '' : cCaseFacad.StartHour.trim();
		string StartMinute = cCaseFacad.StartMinute == null ? '' : cCaseFacad.StartMinute.trim();
		if (StartHour != '' || StartMinute != '')
		{
			if (StartHour == '' || StartMinute == '')
			{
				ErrorMessage = CErrorMessages.CaseStartTimeNotRight;
				return false;
			}
		}
		/*if (cCaseFacad.pCase.StartTime__c == null)
		{
			ErrorMessage = CErrorMessages.CaseStartTime;
			return false;
		}*/
		if (cCaseFacad.pCase.StartTime__c != null && !CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cCaseFacad.pCase.StartTime__c)))
		{
			ErrorMessage = CErrorMessages.CaseStartTimeNotRight;
			return false;
		}
		return true;
	}
	
	private boolean ValidateEndTime()
	{
		string EndHour = cCaseFacad.EndHour == null ? '' : cCaseFacad.EndHour.trim();
		string EndMinute = cCaseFacad.EndMinute == null ? '' : cCaseFacad.EndMinute.trim();
		if (EndHour != '' || EndMinute != '')
		{
			if (EndHour == '' || EndMinute == '')
			{
				ErrorMessage = CErrorMessages.CaseEndTimeNotRight;
				return false;
			}
		}
		/*if (cCaseFacad.pCase.EndTime__c == null)
		{
			ErrorMessage = CErrorMessages.CaseEndTime;
			return false;
		}*/
		if (cCaseFacad.pCase.EndTime__c != null && !CValidateTimesAndConverterTimes.ValidateIfTimeIsRight(CValidateTimesAndConverterTimes.FillFields(cCaseFacad.pCase.EndTime__c)))
		{
			ErrorMessage = CErrorMessages.CaseEndTimeNotRight;
			return false;
		}
		return true;
	}
	
	private boolean ValidateStartTimeNotBigFromEndTime()
	{
		if (CValidateTimesAndConverterTimes.GetTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(cCaseFacad.pCase.StartTime__c)) > 
			 CValidateTimesAndConverterTimes.GetTimeToCalculate(CValidateTimesAndConverterTimes.FillFields(cCaseFacad.pCase.EndTime__c)))
		{
			ErrorMessage = CErrorMessages.CaseStartTimeBigFromEndTime;
			return false;
		}
		return true;
	}
	
	private boolean ValidateRequestsListChildren()
	{
		map<string, Children__c> familyChildrenMap = null;
		if (cCaseFacad.pCase.WhoId__c == CObjectNames.Family && cCaseFacad.pCase.FamilyName__c != null)
			familyChildrenMap = GetFamilyChildrenMap(cCaseFacad.pCase.FamilyName__c);
		Profession__c otherProfession = CProfessionDb.GetOtherProfession();
		map<string, TutorLine__c> tutorLinesMapByTutorAndProfession = CTutorLineDb.GetTutorLinesMapByTutorAndProfession();
		boolean ret = true;
		for (CRequestInCase requestInCase : cCaseFacad.pRequestsInCase)
		{
			if (IsRequesToSave(requestInCase.pRequestForTreatment))
			{
				if (ret)
					ret = ValidateRequestChildName(requestInCase.pRequestForTreatment, familyChildrenMap);
				if (ret)
					ret = ValidateFromDate(requestInCase.pRequestForTreatment);
				if (ret)
					ret = ValidateToDate(requestInCase.pRequestForTreatment);
				if (ret)
					ret = ValidateWeeklyHours(requestInCase);
				if (ret)
					ret = ValidateApprovalAmount(requestInCase);
				if (ret)
					ret = ValidateProfessionType(requestInCase.pRequestForTreatment);
				if (ret)
					ret = ValidateProfessionName(requestInCase.pRequestForTreatment);
				//if (ret)
				//	ret = ValidateProfessionWithTutor(requestInCase, otherProfession, tutorLinesMapByTutorAndProfession);
				if (ret)
					ret = ValidateOtherProfession(requestInCase.pRequestForTreatment, otherProfession);
			}
			if (!ret)
				break;
		}
		return ret;
	}
	
	private boolean ValidateOtherProfession(RequestForTreatment__c requestForTreatment, Profession__c otherProfession)
	{
		if (requestForTreatment.ProfessionName__c == otherProfession.id && requestForTreatment.ProfessionOther__c == null)
		{
			ErrorMessage = CErrorMessages.CaseRequestsListProfessionName;
			return false;
		}
		
		return true;
	}
	
	private boolean ValidateProfessionWithTutor(CRequestInCase requestInCase, Profession__c otherProfession,  
												map<string, TutorLine__c> tutorLinesMapByTutorAndProfession)
	{
		if (requestInCase.pRequestForTreatment.ProfessionsList__c == otherProfession.id)
			return true;
		if (requestInCase.pRequestForTreatment.TutorName__c != null && requestInCase.pRequestForTreatment.ProfessionsList__c != null)
		{
			string ids = string.valueOf(requestInCase.pRequestForTreatment.TutorName__c) + requestInCase.pRequestForTreatment.ProfessionsList__c;
			if (!tutorLinesMapByTutorAndProfession.containsKey(ids))
			{
				ErrorMessage = CErrorMessages.CaseRequestsListProfessionWithTutor;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateProfessionName(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.ProfessionName__c == null)
		{
			ErrorMessage = CErrorMessages.CaseRequestsListProfessionName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateProfessionType(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.ProfessionTypeName__c == null)
		{
			ErrorMessage = CErrorMessages.CaseRequestsListProfessionType;
			return false;
		}
		return true;
	}
	
	private boolean ValidateApprovalAmount(CRequestInCase requestInCase)
	{
		if (requestInCase.bApprovalAmount)
		{
			if (requestInCase.pRequestForTreatment.ApprovalAmount__c == null || requestInCase.pRequestForTreatment.ApprovalAmount__c == 0)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentApprovalAmount;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateWeeklyHours(CRequestInCase requestInCase)
	{
		if (requestInCase.bWeeklyHours)
		{
			if (requestInCase.pRequestForTreatment.WeeklyHours__c == null || requestInCase.pRequestForTreatment.WeeklyHours__c == 0)
			{
				ErrorMessage = CErrorMessages.CaseRequestsListWeeklyHours;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateToDate(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.ToDate__c == null)
		{
			ErrorMessage = CErrorMessages.CaseRequestsListToDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateFromDate(RequestForTreatment__c requestForTreatment)
	{
		if (requestForTreatment.FromDate__c == null)
		{
			ErrorMessage = CErrorMessages.CaseRequestsListFromDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateRequestChildName(RequestForTreatment__c requestForTreatment, map<string, Children__c> familyChildrenMap)
	{
		if (familyChildrenMap != null)
		{
			if (requestForTreatment.FamilyChildren__c == null)
			{
				ErrorMessage = CErrorMessages.CaseInRequestsListHasToBeChild;
				return false;
			}
			if (!familyChildrenMap.containsKey(requestForTreatment.FamilyChildren__c))
			{
				ErrorMessage = CErrorMessages.CaseInRequestsListHasChildNotFromFamily;
				return false;
			}
		}
		else
		{
			if (requestForTreatment.ChildName__c == null)
			{
				ErrorMessage = CErrorMessages.CaseInRequestsListHasToBeChild;
				return false;
			}
		}
		return true;
	}
	
	private map<string, Children__c> GetFamilyChildrenMap(string familyId)
	{
		Children__c[] children = [select name, id from Children__c where FamilyName__c = :familyid];
		map<string, Children__c> ret = new map<string, Children__c>();
		for (integer i = 0; i < children.size(); i++)
		{
			ret.put(children[i].id, children[i]);
		}
		return ret;
	}
	
	private boolean IsRequesToSave(RequestForTreatment__c requestForTreatment)
	{
		if ((requestForTreatment.WeeklyHours__c != null && requestForTreatment.WeeklyHours__c > 0) || (requestForTreatment.ApprovalAmount__c != null && requestForTreatment.ApprovalAmount__c > 0))
			return true;
		return false;
	}
}