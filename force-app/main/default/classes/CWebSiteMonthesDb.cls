public with sharing class CWebSiteMonthesDb 
{
	public string month { get; set; } 
	public string monthWarning { get; set; }
	
	public CWebSiteMonthesDb(){}
	
	public List<SelectOption> GetMonthesList(){
		if (ApexPages.currentPage().getCookies().get('sessionId') != null){
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null){
				CInterface iface = new CInterface();
				string[] monthesList ;
				if(sessionId.TutorName__c != null ){
					monthesList = iface.GetMonthesByTutorOrFamilyId(sessionId.TutorName__c, false);
				}
				else if(sessionId.Family__c != null ){
					monthesList = iface.GetMonthesByTutorOrFamilyId(sessionId.Family__c, true);
				}
				else if(sessionId.ActivityApproval__c != null ){
					monthesList = iface.GetMonthesListFromActivityApprovalId(sessionId.ActivityApproval__c);
				}
				List<SelectOption> ret = new List<SelectOption>();
				ret.add(new SelectOption('null', 'ללא'));
				for (integer i = 0; i < monthesList.size(); i++)
				{
					ret.add(new SelectOption(monthesList[i], monthesList[i]));
				}
				return ret;
			}
		}
		return new List<SelectOption> {new SelectOption('null', 'ללא')};
	}
}