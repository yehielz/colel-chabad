public with sharing class CWebSiteChildrenFacad 
{
	private string mMonth { get; set; }
	public string pMonth 
	{ 
		get
		{
			if (mMonth == null)
			{
				if (ApexPages.currentPage() != null && ApexPages.currentPage().getParameters().get('month') != null)
					mMonth = ApexPages.currentPage().getParameters().get('month');
			}
			return mMonth;
		} 
	}
	private Tutor__c tutor { get; set; }
	public Tutor__c pTutor { 
		get{
			if (tutor == null){
				if (ApexPages.currentPage().getCookies().get('sessionId') != null){
					SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
					if (sessionId != null)
						tutor = CTutorDb.GetTutorById(sessionId.TutorName__c);
				}
			}
			return tutor;
		} 
	}
	
	private Family__c family { get; set; }
    public Family__c pfamily { 
        get  {
            if (family == null) {
                if (ApexPages.currentPage().getCookies().get('sessionId') != null) {
                    SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
                    if (sessionId != null)
                        family = CFamilyDb.GetFamilyById(sessionId.Family__c);
                }
            }
            return family;
        } 
    }
	
	private CWebSiteChildrenDb webSiteChildrenDb;
	
	public CWebSiteChildrenFacad()
	{
		webSiteChildrenDb = new CWebSiteChildrenDb();
	}
	
	public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage() != null && ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionId__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
			{
				if (!CInterface.ValidateMonth()){
					return new PageReference(CObjectNames.WebSitePageMonthes); 
				}
				return null;
			}
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
	
	public List<SelectOption> childrenList
	{
		get
		{
			if (webSiteChildrenDb.GetChildrenList() != null){
				return webSiteChildrenDb.GetChildrenList();
			}
			return null;
		}
	}
	
	public string childAndProfession
	{
		get 
		{
			if (webSiteChildrenDb.childAndProfession != null)
				return webSiteChildrenDb.childAndProfession;
			return null;
		}
		set
		{
			webSiteChildrenDb.childAndProfession = value;
		}
	}
	
	public string childrenWarning { get; set; }
	
	public PageReference ShowReport()
	{
		if (childAndProfession != 'null' && childAndProfession != null )
		{
			//webSiteChildrenDb.SetChildAndProfessionCookie();
			return new PageReference(CObjectNames.WebSitePageReport + '&childId='+ childAndProfession.split('-')[0]+ '&month='+ pMonth + '&professionId=' +childAndProfession.split('-')[1]);
		}
		childrenWarning = 'עליך להזין בן משפחה';
		return null;
	}
	
	//links
	
	
	public string homeLink 
	{
		get
		{
			return CObjectNames.WebSitePageHome;
		}
	}
	
	public string monthesLink 
	{
		get
		{
			return CObjectNames.WebSitePageMonthes;
		}
	}
	
	public string reportsDisplayLink 
	{
		get
		{
			return CObjectNames.WebSitePageReportsDisplay;
		}
	}
    
    public PageReference LogOut()
	{
		Cookie sessionIdCookie = new Cookie('sessionId', null, null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { sessionIdCookie } );
		CSessionIdDb.DeleteSessionsIdWithLastTime();
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
}