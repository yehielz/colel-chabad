public with sharing class CTaskValidator 
{
	private CTaskFacad cTaskFacad { get; set; }
	public string ErrorMessage;
	
	public CTaskValidator(CTaskFacad ExtTaskFacad)
	{
		ErrorMessage = '';
		cTaskFacad = ExtTaskFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateReminderDateTime();
	  	    
	 	return ret;
	}
	
	private boolean ValidateReminderDateTime()
	{
		if (cTaskFacad.pTask.IsReminderSet)
		{
			string[] datesplit = cTaskFacad.ReminderDate.split('/');
			if (datesplit.size() != 3)
			{
				ErrorMessage = CErrorMessages.TaskReminderDateNotValid;
				return false;
			}
			if (!ValidateDay(datesplit[0]) || !ValidateMonth(datesplit[1]) || !ValidateYear(datesplit[2]))
			{
				ErrorMessage = CErrorMessages.TaskReminderDateNotValid;
				return false;
			}
		}
		return true;
	}
	
	private boolean ValidateDay(string day)
	{
		if (day.length() > 2 || !ValidateIsNumber(day))
			return false;
		if (integer.valueOf(day) > 31)
			return false;
		return true;
	}
	
	private boolean ValidateMonth(string month)
	{
		if (month.length() > 2 || !ValidateIsNumber(month))
			return false;
		if (integer.valueOf(month) > 12)
			return false;
		return true;
	}
	
	private boolean ValidateYear(string year)
	{
		if (year.length() > 4 || !ValidateIsNumber(year))
			return false;
		return true;
	}
	
	private boolean ValidateIsNumber(string value)
	{
		for (integer i = 0; i < value.length(); i++)
		{
			string car = value.substring(i, i+1);
			if (car != '0' && car != '1' && car != '2' && car != '3' && car != '4' && car != '5' && car != '6' && car != '7' && car != '8' && car != '9')
				return false;
		}
		return true;
	}
}