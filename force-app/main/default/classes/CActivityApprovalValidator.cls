public with sharing class CActivityApprovalValidator 
{
	private CActivityApprovalFacad cActivityApprovalFacad { get; set; }
	public string ErrorMessage;
	
	public CActivityApprovalValidator(CActivityApprovalFacad ExtActivityApprovalFacad)
	{
		ErrorMessage = '';
		cActivityApprovalFacad = ExtActivityApprovalFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	//if (ret) 
	    //	ret = ValidateProfessionWithTutor();
    	if (ret) 
    		ret = notInTheSameDates();
		if (ret)
			ret = ValidateRate();
		if (ret)
			ret = notMistakeDates();
	  	    
	 	return ret;
	}
	
	private boolean ValidateRate()
	{
		if (!cActivityApprovalFacad.bAmountApproved && cActivityApprovalFacad.pActivityApproval.TutorName__c != null && cActivityApprovalFacad.pActivityApproval.Rate__c == null)
		{
			ErrorMessage = CErrorMessages.ActiityApprovalRate;			
			return false;
		}
		return true;
	}
	
	private boolean ValidateProfessionWithTutor()
	{
		if (cActivityApprovalFacad.pActivityApproval.ProfessionName__c == CProfessionDb.GetOtherProfession().id)
			return true;
		if (cActivityApprovalFacad.pActivityApproval.TutorName__c != null && cActivityApprovalFacad.pActivityApproval.ProfessionName__c != null)
		{
			TutorLine__c[] tutorLines = [select name, id from TutorLine__c where TutorName__c = :cActivityApprovalFacad.pActivityApproval.TutorName__c 
										  and ProfessionName__c = :cActivityApprovalFacad.pActivityApproval.ProfessionName__c];
			if (tutorLines.size() == 0)
			{
				ErrorMessage = CErrorMessages.RequestForTreatmentProfessionWithTutor;
				return false;
			}
		}
		return true;
	}
	
	private boolean notInTheSameDates()
	{
		if (cActivityApprovalFacad.pActivityApproval.FromDate__c != null && cActivityApprovalFacad.pActivityApproval.ToDate__c != null)
		{
			ActivityApproval__c[] activityApprovals = [select name, id, FromDate__c, ToDate__c from ActivityApproval__c 
													   where id != :cActivityApprovalFacad.pActivityApproval.id and RequestForTreatmentName__c
													   = :cActivityApprovalFacad.pActivityApproval.RequestForTreatmentName__c and 
													   ((FromDate__c >= :cActivityApprovalFacad.pActivityApproval.FromDate__c and 
													   FromDate__c <= :cActivityApprovalFacad.pActivityApproval.ToDate__c) or 
													   (ToDate__c >= :cActivityApprovalFacad.pActivityApproval.FromDate__c and 
													   ToDate__c <= :cActivityApprovalFacad.pActivityApproval.ToDate__c) or 
													   (FromDate__c <= :cActivityApprovalFacad.pActivityApproval.FromDate__c and 
													   ToDate__c >= :cActivityApprovalFacad.pActivityApproval.ToDate__c))];
		   	if (activityApprovals.size() > 0)
		   	{
		   		ErrorMessage = CErrorMessages.ActivityApprovalIsInTheSameDates;
				return false;
		   	}
		}
		return true;
	}
	private boolean notMistakeDates()
	{
		   	if (cActivityApprovalFacad.pActivityApproval.FromDate__c > cActivityApprovalFacad.pActivityApproval.ToDate__c)
		   	{
		   		ErrorMessage = CErrorMessages.ActivityApprovalFromDateSmallThanToDate;
				return false;
		   	}
		
		return true;
	}
}