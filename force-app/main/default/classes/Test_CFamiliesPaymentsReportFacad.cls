@isTest(seeAllData=true)
private class Test_CFamiliesPaymentsReportFacad {

    static testMethod void TestCFamiliesPaymentsReportFacad() {

		
		Pagereference p = new Pagereference('/apex/FamiliesPaymentsReport?startDate=' + Date.today());
		Test.setCurrentPage(p);
		Test.startTest();
		CFamiliesPaymentsReportFacad ctrl = new CFamiliesPaymentsReportFacad();
		ctrl.GetFormat(12);
		ctrl.getTableHtmlStr();
		ctrl.getTableWithChildrenHtmlStr();
		ctrl.DownloadExcelWithChildren();
		ctrl.DownloadExcel();
		Test.stopTest();
	
    }
}