public with sharing class CActivityApprovalToPrintFacad 
{
    public boolean isUpdate { get; set; }
    private boolean showBR;
    public void UpdateApproval()
    {
        ActivityApproval__c approval = pActivityApproval.pActivityApproval;
        if (isUpdate && approval.IsPrinted__c != true) 
        {
            approval.IsPrinted__c = true;
            update approval;
        }
    }
    
    public CActivityApprovalInPrintPage pActivityApproval { get; set; }
    public string activityApprovalsHtmlToView 
    { 
        get
        {
            return GetHtmlForActivityApproval(pActivityApproval);
        }
    }
    public string todayDate 
    { 
        get
        {
            return CObjectNames.getHebrewString('תאריך: ' + Datetime.now().day() + ' / ' + datetime.now().month() + ' / ' + datetime.now().year());
        }
    }
    public string meetingDate { get; set; }
    public string familyName 
    { 
        get
        {
            if (pActivityApproval.pActivityApproval.ChildName__c != null && pActivityApproval.pActivityApproval.ChildName__r.FamilyName__c != null &&
                pActivityApproval.pActivityApproval.ChildName__r.FamilyName__r.name != null)
                return CObjectNames.getHebrewString('משפחת ' + pActivityApproval.pActivityApproval.ChildName__r.FamilyName__r.name);
            return '';
        }
    }
    
    public string city
    { 
        get
        {
            if (pActivityApproval.pActivityApproval.ChildName__c != null && pActivityApproval.pActivityApproval.ChildName__r.FamilyName__c != null &&
                pActivityApproval.pActivityApproval.ChildName__r.FamilyName__r.City__c != null)
                return CObjectNames.getHebrewString(pActivityApproval.pActivityApproval.ChildName__r.FamilyName__r.city__c);
            return '';
        }
    }
    
    public string address
    { 
        get
        {
            if (pActivityApproval.pActivityApproval.ChildName__c != null && pActivityApproval.pActivityApproval.ChildName__r.FamilyName__c != null &&
                pActivityApproval.pActivityApproval.ChildName__r.FamilyName__r.address__c != null)
                return CObjectNames.getHebrewString(pActivityApproval.pActivityApproval.ChildName__r.FamilyName__r.address__c);
            return '';
        }
    }

    
    public CActivityApprovalToPrintFacad()
    {
        isUpdate = true;
        SetpActivityApproval();
    }
    public CActivityApprovalToPrintFacad(boolean showBR)
    {
        isUpdate = true;
        SetpActivityApproval();
    }
    
    public CActivityApprovalToPrintFacad(ActivityApproval__c approval)
    {
        isUpdate = false;
        pActivityApproval = new CActivityApprovalInPrintPage(approval);
        meetingDate = approval.CreatedDate.day() + '/' + approval.CreatedDate.month()
                        + '/' + approval.CreatedDate.year();
    }
    
    private void SetpActivityApproval()
    {
        pActivityApproval = new CActivityApprovalInPrintPage();
        if (Apexpages.currentPage().getParameters().get('activityApprovalID') != null)
        {
            ActivityApproval__c[] activityApprovals = [select TutorName__c, ProfessionName__c, RequestForTreatmentName__c, AmountApproved__c, 
                                                              NoteToPrintPage__c, ProfessionOther__c, HoursInSequence__c, SendEmailToTutor__c, 
                                                              ChildName__c, SendEmailToFamily__c, Rate__c, MinutesInSequence__c, PaymentTarget__c, 
                                                              ToDate__c, FromDate__c, Note__c, WeeklyHours__c, id, ProfessionName__r.name, 
                                                              RequestForTreatmentName__r.ProfessionTypeName__r.PaymentType__c, IsPrinted__c,
                                                              ChildName__r.name, name, CreatedDate, ChildName__r.FamilyName__r.name,
                                                              ChildName__r.FamilyName__r.City__c, ChildName__r.FamilyName__r.Address__c,
                                                              ProfessionName__r.ProfessionTypeName__r.PaymentType__c, CaseName__c from ActivityApproval__c
                                                              where id = :Apexpages.currentPage().getParameters().get('activityApprovalID')];
            if (activityApprovals.size() > 0)
            {
                pActivityApproval = new CActivityApprovalInPrintPage(activityApprovals[0]);
                meetingDate = activityApprovals[0].CreatedDate.day() + '/' + activityApprovals[0].CreatedDate.month()
                                + '/' + activityApprovals[0].CreatedDate.year();
            }
        }
    }
    
    private string GetHtmlForActivityApproval(CActivityApprovalInPrintPage activityApproval)
    {
        string ret = '<div class="activityApprovalDiv">' + 
                            '<p>' +
                                '.' + activityApproval.hoursInSequence + activityApproval.totalScopeOrAmount + activityApproval.ScopeOrAmount + ','
                                 + activityApproval.childName + CObjectNames.getHebrewString('עבור ') + activityApproval.professionName + ' .1' +
                            '</p>';
                            if (activityApproval.IsHoursPerWeek)
                            {
                                ret += '<p>' +
                                    activityApproval.RateForHour + ' ' + CObjectNames.getHebrewString('תעריף ותנאים לפעילות זו:') ;
                             ret+=   '&nbsp;&nbsp;&nbsp;&nbsp;' ;
                             ret+=   '</p>';
                            }
                            ret += '<p>' +
                                '.' + activityApproval.activityToDate + ' ' + CObjectNames.getHebrewString('עד ל:') + ' ' + 
                                activityApproval.activityFromDate + ' ' + CObjectNames.getHebrewString('הפעילות מאושרת - מ:') ;
                    ret+=   '&nbsp;&nbsp;&nbsp;&nbsp;' ;
                           ret+= '</p>' +
                        '</div>';
        return ret;
    }
    
    public string GetShortHtmlForActivityApproval ()
    { 
    	CActivityApprovalInPrintPage activityApproval = pActivityApproval;
        string ret =   '<span>' +
                                '.' + activityApproval.hoursInSequence + activityApproval.totalScopeOrAmount + activityApproval.ScopeOrAmount + ','
                                 + activityApproval.childName + CObjectNames.getHebrewString('עבור ') + activityApproval.professionName  +
                            '</span>';
                            ret += '<div>' ;
                            if (activityApproval.IsHoursPerWeek)
                            {
                                
                               ret+=      activityApproval.RateForHour + ' ' + CObjectNames.getHebrewString('תעריף ותנאים לפעילות זו:') ;
                             ret+=   '&nbsp;&nbsp;&nbsp;&nbsp;' ;
                             
                            }
                            ret+= '<span>';
                           ret+= '.' + activityApproval.activityToDate + ' ' + CObjectNames.getHebrewString('עד ל:') + ' ' + 
                                activityApproval.activityFromDate + ' ' + CObjectNames.getHebrewString('הפעילות מאושרת - מ:') ;
                                 ret+= '</span>';
                    ret+=   '&nbsp;&nbsp;&nbsp;&nbsp;' ;
                    ret += '</div><div style="margin-right: 15px;">' ;
                     ret+= '<span>';
                    if(activityApproval.tutorPhone  != '')
                    ret += activityApproval.tutorPhone + ' '+ CObjectNames.getHebrewString('טלפון: ') ;
                     ret+= '</span>';
                     ret+= '<span>';
                    if(activityApproval.TutorName  != '')
                    ret += '.' + activityApproval.TutorName + ' '+ CObjectNames.getHebrewString('שם החונך: ') ;
                     ret+= '</span>';
                    
                           ret+= '</div>' ;
                    
        return ret;
    }
}