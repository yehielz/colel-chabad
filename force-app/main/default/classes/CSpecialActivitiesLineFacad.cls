public with sharing class CSpecialActivitiesLineFacad 
{
	public boolean bIsFamily { get; set; }
	public boolean bIsChild { get; set; }
	public boolean bIsContact { get; set; }
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	private CSpecialActivitiesLineDb specialActivitiesLineDb;
	
	public SpecialActivitiesLine__c pSpecialActivityLine
	{
		get
		{
			return specialActivitiesLineDb.GetpSpecialActivityLine();
		}
	}
	
	public list<selectoption> specialActivitiesOptions
	{
		get
		{
			return specialActivitiesLineDb.GetSpecialActivitisOptions();
		}
	}
		
	public CSpecialActivitiesLineFacad(ApexPages.StandardController controller)
	{
		specialActivitiesLineDb = new CSpecialActivitiesLineDb(controller);
		errorMessage = '';
		if (pSpecialActivityLine.id == null)
			pageTitle = 'פעילות מיוחדת חדשה';
		else
			pageTitle = pSpecialActivityLine.name;
		SetChildOrFamily();
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		specialActivitiesLineDb.InsertpSpecialActivityLine();
		PageReference newCasePage = new ApexPages.StandardController(pSpecialActivityLine).view();
        return newCasePage;
	}
	
	private void SetChildOrFamily()
	{
		if (pSpecialActivityLine.id == null && pSpecialActivityLine.ChildName__c != null)
			pSpecialActivityLine.ChildOrFamily__c = CObjectNames.FamilyMember;
		if (pSpecialActivityLine.id == null && pSpecialActivityLine.FamilyName__c != null)
			pSpecialActivityLine.ChildOrFamily__c = CObjectNames.Family;
		OnChangeChildOrFamily();
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CSpecialActivitiesLineValidator validator = new CSpecialActivitiesLineValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public PageReference OnChangeChildOrFamily()
    {
    	if (pSpecialActivityLine.ChildOrFamily__c == CObjectNames.FamilyMember)
    	{
    		bIsFamily = bIsContact= false;
			bIsChild= true;
			pSpecialActivityLine.FamilyName__c = pSpecialActivityLine.Contact__c = null;
    	}
    	else if ( pSpecialActivityLine.ChildOrFamily__c == CObjectNames.Family)
    	{
    		bIsFamily = true;
			bIsChild = bIsContact = false;
    		pSpecialActivityLine.ChildName__c = pSpecialActivityLine.Contact__c = null;
    	}
    	else{
    		bIsContact = true;
			bIsChild = bIsFamily = false;
			pSpecialActivityLine.FamilyName__c = pSpecialActivityLine.ChildName__c = null;
    	}
    	return null;
    }
}