public with sharing class CChildRegistrationInsupporter
{
	public ChildRegistrationInSupporter__c pChildRegistrationInSupporter { get; set; }
	public ChildRegistration__c pChildRegistration { get; set; }
	public boolean IsForSave { get; set; }
	
	public CChildRegistrationInsupporter(ChildRegistrationInSupporter__c childRegistrationInSupporter, ChildRegistration__c childRegistration)
	{
		pChildRegistrationInSupporter = childRegistrationInSupporter;
		pChildRegistration = childRegistration;
		IsForSave = pChildRegistrationInSupporter.id != null ? true : false;
	}
}