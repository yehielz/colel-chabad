public without sharing class CSchoolYearDb
{
	public static string GetActiveYearId()
	{
		SchoolYear__c activeYear = [select name, id from SchoolYear__c where IsActive__c = true limit 1];
		return activeYear != null ? activeYear.id : null;
	}
	
	public static SchoolYear__c GetActiveYear()
	{
		SchoolYear__c[] activeYear = [select name, id, StartDate__c, EndOfSchoolYear__c, HalfSchoolYear__c, PreviousYear__c from SchoolYear__c where IsActive__c = true];
		if (activeYear.size() > 0)
			return activeYear[0];
		return null;
	}
	
	public static SchoolYear__c GetYearById(String yearId)	{
		SchoolYear__c[] years = [select name, id, StartDate__c, EndOfSchoolYear__c, HalfSchoolYear__c from SchoolYear__c where Id =:yearId];
		if (years.size() > 0)
			return years[0];
		return null;
	}
	
	public static SchoolYear__c[] GetAllYears()
	{
		return [select name, id from SchoolYear__c order by StartDate__c desc];
	}
	
	public static integer GetStartSchoolYear(string schoolYearName)
	{
		SchoolYear__c [] schoolYear = [select StartDate__c from SchoolYear__c where id = :schoolYearName];
		if (schoolYear.size() > 0)
			return schoolYear[0].StartDate__c.year();
		return -1;
	}
	public static List<SelectOption> GetYearsOptions()
	{
		List<SelectOption> Options = new List<SelectOption>();
		Options.add(new Selectoption('', '-הכל-'));
		for(SchoolYear__c year : GetAllYears())
		{
			Options.add(new Selectoption(year.id, year.name));
		}
		return Options;
	}
}