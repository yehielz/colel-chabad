public with sharing class CWSSupporterChildViewFacad 
{
	private CWSSupporterChildViewDb wsSupporterChildViewDb { get; set; }
    public CChildFullReportFacad childFullReportFacad { get; set; }
    
    public Support__c pSupporter{ get{ return wsSupporterChildViewDb.pSupporter; } }
	public Children__c pChild{ get{ return wsSupporterChildViewDb.pChild; } }
	public ChildRegistration__c pChildRegistration{ get{ return wsSupporterChildViewDb.pChildRegistration; } }
	public boolean IsEnglish{ get{ return wsSupporterChildViewDb.IsEnglish; } }
    public boolean IsBlur{ get{ return wsSupporterChildViewDb.IsBlur; } }
    public boolean IsLastName{ get{ return wsSupporterChildViewDb.IsLastName; } }
     
    public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
				return null;
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
    
    public CWSSupporterChildViewFacad()
    {
    	boolean a = wsSupporterChildViewDb != null && childFullReportFacad == null;
    	wsSupporterChildViewDb = new CWSSupporterChildViewDb();
    	childFullReportFacad = new CChildFullReportFacad(pChild, IsEnglish);
    }
    
    public PageReference GoToNextChild()
    {
    	Children__c child = GetChildInPossession(1);
    	if (child != null)
    		return new PageReference(CObjectNames.getBaseUrl() + Page.WSSupporterChildView.getUrl() + '?childId=' + child.id + '&lang=' + (IsEnglish ? 'en' : 'he') + (IsBlur ? '&b=b' : '') + (IsLastName ? '&n=n' : ''));
    	return null;
    }
    
    public PageReference GoToPreviousChild()
    {
    	Children__c child = GetChildInPossession(-1);
    	if (child != null)
    		return new PageReference(CObjectNames.getBaseUrl() + Page.WSSupporterChildView.getUrl() + '?childId=' + child.id + '&lang=' + (IsEnglish ? 'en' : 'he') + (IsBlur ? '&b=b' : '') + (IsLastName ? '&n=n' : ''));
    	return null;
    }
    
    public Children__c GetChildInPossession(integer possession)
    {
    	list<Children__c> children = GetSupporterChildren();
    	for (integer i = 0; i < children.size(); i++)
    	{
    		if (children[i].id == pChild.id && (i + possession) >= 0 && children.size() > (i + possession))
    			return children[i + possession];
    	}
    	return null;
    }
    
    private list<Children__c> GetSupporterChildren()
    {
    	if (pSupporter.IsAllChildren__c)
    	{
    		string year = CSchoolYearDb.GetActiveYearId();
    		ChildRegistration__c[] registrations = CChildRegistrationDb.GetChildRegistrationByFilter(year != null ? 'SchoolYearName__c = \'' + year + '\'' : 'id = null');
	    	map<string, string> childrenIds = new map<string, string>();
	    	for (ChildRegistration__c item : registrations)
	    	{
	    		if (item.ChildName__c != null)
	    			childrenIds.put(item.ChildName__c, item.ChildName__c);
	    	}
			return CChildDb.GetChildrenListByIds(childrenIds.values());
    	}
    	else
    	{
    		list<ChildRegistrationInSupporter__c> registrationsInSupporters = [select name, id, ChildRegistrationName__r.ChildName__c,
	    																			  ChildRegistrationName__c from ChildRegistrationInSupporter__c 
	    																			  where SupporterName__c = :pSupporter.id and
	    																			  ChildRegistrationName__r.SchoolYearName__r.IsActive__c = true];
	    	map<string, string> childrenIds = new map<string, string>();
	    	for (ChildRegistrationInSupporter__c item : registrationsInSupporters)
	    	{
	    		if (item.ChildRegistrationName__c != null && item.ChildRegistrationName__r.ChildName__c != null)
	    			childrenIds.put(item.ChildRegistrationName__r.ChildName__c, item.ChildRegistrationName__r.ChildName__c);
	    	}
			return CChildDb.GetChildrenListByIds(childrenIds.values());
    	}
    }
    
    public string ChildPicture{ get{ return wsSupporterChildViewDb.ChildPicture; } }
    public string ChildAge{ get{ return (IsEnglish != null && IsEnglish ? 'Age: ' : 'גיל: ') + '<span>' + wsSupporterChildViewDb.ChildAge + '</span>'; } }
	public string ChildName
    { 
    	get
    	{ 
    		string lastName = pChild.FamilyName__r.name != null ? pChild.FamilyName__r.name : '';
    		if (IsEnglish && pChild.EnglishFamilyName__c != null)
    			lastName = pChild.EnglishFamilyName__c;
    		string firstName = pChild.FirstName__c != null ? pChild.FirstName__c : '';
    		if (IsEnglish && pChild.EnglishFirstName__c != null)
    			firstName = pChild.EnglishFirstName__c;
    		if (IsBlur && lastName.length() > 1)
    			lastName = lastName.substring(0,1) + '.';
    		return firstName + (IsLastName ? ' ' + lastName : ''); 
    	} 
    } 
    public string ChildNameForTitle{ get{ return ChildName.replace('\'', '\\\'').replace('"', '\\"'); } }
    public string Direction{ get{ return IsEnglish ? 'ltr' : 'rtl'; } }
    public string FamilyStatus
    { 
    	get
    	{ 
    		if (IsEnglish)
    			return 'Family status: ' + (pChild.FamilyName__r.DiedParent__c != null ? '<span>Orphan from ' + pChild.FamilyName__r.EnglishDiedParent__c + '</span>' : '');
    		else
    			return 'מצב משפחתי: ' + (pChild.FamilyName__r.DiedParent__c != null ? '<span>' + (pChild.Gender__c == 'נקבה' ? 'יתומה' : 'יתום') + ' מ' + pChild.FamilyName__r.DiedParent__c + '</span>' : '');
    	}
    }
    
    public string SchoolName{ get{ return (IsEnglish ? 'School name: ' : 'בית ספר: ') + '<span>' + pChildRegistration.SchoolName__r.name + '</span>'; } }
    public string SocialStatus{ get{ return (IsEnglish ? 'Social status: ' : 'מצב חברתי: ') + (pChildRegistration.SocialStatus__c != null ? '<span>' + pChildRegistration.SocialStatus__c + '</span>' : ''); } }
    public string Educator{ get{ return (IsEnglish ? 'Educator: ' : 'מחנך: ') + (pChildRegistration.Educator__c != null ? '<span>' + pChildRegistration.Educator__c + '</span>' : ''); } }
    public string LearningStatus{ get{ return (IsEnglish ? 'Learning status: ' : 'מצב לימודי: ') + (pChildRegistration.LearningStatus__c != null ? '<span>' + pChildRegistration.LearningStatus__c + '</span>' : ''); } }
    public string ChildClass{ get{ return (IsEnglish ? 'Class: ' : 'כיתה: ') + (pChildRegistration.Class__c != null ? '<span>' + pChildRegistration.Class__c + '</span>' : ''); } }
	public string Address
    { 
    	get
    	{ 
    		if (IsEnglish)
    			return 'Address: ' + ((pChild.FamilyName__r.EnglishAddress__c != null  &&  pChild.FamilyName__r.EnglishCity__c != null) ? '<span>' + pChild.FamilyName__r.EnglishAddress__c + ', ' + pChild.FamilyName__r.EnglishCity__c + '</span>' : '<span>' + pChild.FamilyName__r.Address__c + ', ' +  pChild.FamilyName__r.City__c + '</span>');
    		else
    			return 'כתובת: ' + ((pChild.FamilyName__r.Address__c != null &&  pChild.FamilyName__r.City__c != null) ? '<span>' + pChild.FamilyName__r.Address__c + ', ' +  pChild.FamilyName__r.City__c + '</span>' : '');
    	}
    }
    public string FamilyBG{ get{ return (IsEnglish ? 'Family story: ' : 'רקע משפחתי: '); } }
    public string FamilyBackGround{ get{ return (IsEnglish ? childFullReportFacad.FamilyBackgroundWSEN : childFullReportFacad.FamilyBackgroundWS); } }
    public string RegistrationDate{ get{ return (IsEnglish ? 'Beginning of the activity: ' :'תחילת פעילות: '); } }
}