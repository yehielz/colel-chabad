public with sharing class CExamValidator 
{
	private CExamFacad cExamFacad { get; set; }
	public string ErrorMessage;
	
	public CExamValidator(CExamFacad ExtExamFacad)
	{
		ErrorMessage = '';
		cExamFacad = ExtExamFacad;
	}
	
	public boolean Validate()
	{
		boolean ret = true;
	  
    	if (ret) 
	    	ret = ValidateChildName();
    	if (ret) 
	    	ret = ValidateProfessionName();
		if (ret) 
	    	ret = ValidateDate();
    	if (ret) 
	    	ret = ValidateOnlyOneExam();
    	if (ret) 
	    	ret = ValidateChildRegistrationName();
    	if (ret) 
	    	ret = ValidateExamType();
    	if (ret) 
	    	ret = ValidateGrade();
	  	    
	 	return ret;
	}
	
	private boolean ValidateChildRegistrationName()
	{
		if (cExamFacad.pExam.ChildRegistration__c == null)
		{
			ErrorMessage = CErrorMessages.ExamChildRegistrationName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateChildName()
	{
		if (cExamFacad.pExam.ChildName__c == null)
		{
			ErrorMessage = CErrorMessages.ExamChildName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateGrade()
	{
		if ((cExamFacad.pExam.Grade__c == null || cExamFacad.pExam.Grade__c == 0) && cExamFacad.pExam.GradeInWords__c == null)
		{
			ErrorMessage = CErrorMessages.ExamGrade;
			return false;
		}
		return true;
	}
	
	private boolean ValidateDate()
	{
		if (cExamFacad.pExam.Date__c == null)
		{
			ErrorMessage = CErrorMessages.ExamDate;
			return false;
		}
		return true;
	}
	
	private boolean ValidateProfessionName()
	{
		if (cExamFacad.pExam.ProfessionName__c == null)
		{
			ErrorMessage = CErrorMessages.ExamProfessionName;
			return false;
		}
		return true;
	}
	
	private boolean ValidateExamType()
	{
		if (cExamFacad.pExam.ExamType__c == null)
		{
			ErrorMessage = CErrorMessages.ExamExamType;
			return false;
		}
		return true;
	}
	
	private boolean ValidateOnlyOneExam()
	{
		Exam__c[] allExams = [select name, id, ChildName__c, ProfessionName__c, Date__c from Exam__c where id != :cExamFacad.pExam.id];
		for (integer i = 0; i < allExams.size(); i++)
		{
			if (allExams[i].ChildName__c == cExamFacad.pExam.ChildName__c && 
				allExams[i].ProfessionName__c == cExamFacad.pExam.ProfessionName__c && 
				allExams[i].Date__c == cExamFacad.pExam.Date__c)
			{
				ErrorMessage = getErorrToOnlyOneExam();
				return false;
			}
		}
		
		return true;
	}
	
	private string getErorrToOnlyOneExam()
	{
		Children__c[] child = [select name, id from Children__c where id = :cExamFacad.pExam.ChildName__c];
		Profession__c[] profession = [select name, id from Profession__c where id = :cExamFacad.pExam.ProfessionName__c];
		string ret = CErrorMessages.ExamOnlyOneExamPar1 + child[0].name;
		ret += CErrorMessages.ExamOnlyOneExamPar2 + profession[0].name;
		ret += CErrorMessages.ExamOnlyOneExamPar3 + cExamFacad.pExam.Date__c.year() + ' / ' + cExamFacad.pExam.Date__c.month() + ' / ' + cExamFacad.pExam.Date__c.day();
		return ret;
	}
}