public with sharing class CChildRegistrationFacad
{
	public string subTitle { get; set; }
	public string title { get; set; }
	public string errorMessage { get; set; }
	public string warningMessage { get; set; }
	private CChildRegistrationDb childRegistrationDb;
	
	public ChildRegistration__c pRegistrationChild
	{
		get
		{
			return childRegistrationDb.GetChildRegistration();
		}
	}
		
	public CChildRegistrationFacad(ApexPages.StandardController controller)
	{
		childRegistrationDb = new CChildRegistrationDb(controller);
		SetProperties();
	}
	
	public PageReference save()
	{
		errorMessage = CChildRegistrationValidator.Validate(pRegistrationChild);
		if ( errorMessage != ''){
			return null; 
		}
		try{
			childRegistrationDb.save();
		}
		catch(DmlException ex){
			ApexPages.addMessage(new ApexPages.Message(Apexpages.Severity.WARNING, ex.getdmlMessage(0) ));
			return null;
		}
		PageReference newChildRegistrationPage = new ApexPages.StandardController(pRegistrationChild).view();
        return newChildRegistrationPage ;
	}
	
	
	private void SetProperties()
	{
		errorMessage = '';
		warningMessage = '';
		title = 'עריכת רישום שנתי';
		subTitle = 'רישום שנתי חדש';
		if (pRegistrationChild.Id != null)
		{	
			title = 'פירוט רישום שנתי';
			subTitle = pRegistrationChild.name;
			warningMessage = SetIsClassMatch();
		}
	}
	
	private string SetIsClassMatch()
	{
		if (pRegistrationChild.Class__c != pRegistrationChild.ExpectedClass__c)
		{
			string expectedClass = '';
			if (pRegistrationChild.ExpectedClass__c == null)
				expectedClass = 'על תיכונית';
			else
				expectedClass = pRegistrationChild.ExpectedClass__c;
			return 'שים לב, הכיתה הצפויה לבן המשפחה היא - ' + expectedClass + ' !!!';
		}
		return '';
	}
	
	public PageReference SetClass()
	{
		childRegistrationDb.SetClass();
		return null;
	}
	
	public list<CObjectsHistory> pChildRegistrationHistories
    {
    	get
    	{
    		return childRegistrationDb.GetChildRegistrationHistories();
    	}
    }   
}