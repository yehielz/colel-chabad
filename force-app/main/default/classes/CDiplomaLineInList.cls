public with sharing class CDiplomaLineInList
{
	public DiplomaLine__c pDiplomaline{get; set;}
	public Integer lineIndex{get; set;}
	
	public CDiplomaLineInList(DiplomaLine__c diplomaLine)
	{
		pDiplomaline = diplomaLine;
	}
	public boolean IsTheoretical 
	{ 
		get
		{ 
			system.debug('CDiplomaLineDb.pClusteringByProfession = '  + CDiplomaLineDb.pClusteringByProfession);
			if (pDiplomaline.ProfessionName__c != null && CDiplomaLineDb.pClusteringByProfession.containsKey(pDiplomaline.ProfessionName__c))
				return CDiplomaLineDb.pClusteringByProfession.get(pDiplomaline.ProfessionName__c);
			system.debug('222222222222222222222');
			return false; 
		} 
	}
	
}