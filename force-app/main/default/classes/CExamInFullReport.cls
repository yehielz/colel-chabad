public with sharing class CExamInFullReport
{
	public Exam__c pExam { get; set; }
	private map<string, boolean> pView { get; set; }
	public boolean IsExamInView{ get { return pView.containsKey('all') || pView.containsKey('exam'); } }	
	
	public CExamInFullReport(Exam__c exam, map<string, boolean> view)
	{
		pExam = exam;
		pView = view;
	} 
	
	public string grade{ get{ return(pExam.Grade__c == null || pExam.Grade__c < 1) ? '' : string.valueOf(math.round(pExam.Grade__c)); } }
	public string examDate
	{
		get
		{ 
			if (pExam.Date__c != null)
				return CObjectNames.GetFullNumberInString(pExam.Date__c.day()) + '/' + CObjectNames.GetFullNumberInString(pExam.Date__c.month()) + '/' + pExam.Date__c.year();
			return '';
			 
		} 
	}
	public string exam{ get{ return CObjectNames.GetHebrewWords((' ציון ' + grade + '%'  + ' בתאריך ' + examDate),70);}}
	public string examWS
	{ 
		get
		{ 
			string ret = '<span class="TitleInContent">ציון:</span> ' + grade;
			ret += ' | <span class="TitleInContent">תאריך:</span> ' + examDate;
			return ret;
		}
	}
	public string examWSEN
	{ 
		get
		{ 
			string ret = '<span class="TitleInContent">Grade:</span> ' + grade;
			ret += ' | <span class="TitleInContent">Date:</span> ' + examDate;
			return ret;
		}
	}
}