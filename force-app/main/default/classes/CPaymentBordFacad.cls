public with sharing class CPaymentBordFacad 
{
	private CPaymentBordDb paymentBordDb { get; set; }
	public list<CPaymentInBord> pPayments { get{ return paymentBordDb.pPayments; } }
	public date pStartDate { get{ return paymentBordDb.pStartDate; } set{ paymentBordDb.pStartDate = value; } }
	public date pEndDate { get{ return paymentBordDb.pEndDate; } set{ paymentBordDb.pEndDate = value; } }
	
	public CPaymentBordFacad()
	{
		paymentBordDb = new CPaymentBordDb();
	}
	
	public string sStartDate
	{
		get
		{
			return CObjectNames.GetFullNumberInString(pStartDate.day()) + '/' + CObjectNames.GetFullNumberInString(pStartDate.month())
					 + '/' + pStartDate.year();
		}
		set
		{
			pStartDate = null;
			if (value != null && value.trim() != '' && value.split('/').size() == 3)
			{
				string[] valueList = value.split('/');
				pStartDate = date.newinstance(integer.valueOf(valueList[2]), integer.valueOf(valueList[1]), integer.valueOf(valueList[0]));
			}
		}
	}
	public string sEndDate
	{
		get
		{
			return CObjectNames.GetFullNumberInString(pEndDate.day()) + '/' + CObjectNames.GetFullNumberInString(pEndDate.month())
					 + '/' + pEndDate.year();
		}
		set
		{
			pEndDate = null;
			if (value != null && value.trim() != '' && value.split('/').size() == 3)
			{
				string[] valueList = value.split('/');
				pEndDate = date.newinstance(integer.valueOf(valueList[2]), integer.valueOf(valueList[1]), integer.valueOf(valueList[0]));
			}
		}
	}
	
	public Pagereference RefreshDates()
	{
		string url = CObjectNames.GetBaseUrl() + page.PaymentBord.getUrl() + paramsInUrl;
		return new PageReference(url);
	}
	
	public string paramsInUrl{ get{ return '?sd=' + pStartDate + '&ed=' + pEndDate; } }
}