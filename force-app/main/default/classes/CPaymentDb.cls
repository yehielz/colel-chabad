public with sharing class CPaymentDb 
{
	private Payment__c pPayment { get; set; }
	public list<CActivityApprovalInPayment> pActivityApprovals { get; private set; }
	private CCheckDb checkDb { get; set; }
	private Map<string, Payment__c> allPaymentsMap;
	private Map<string, decimal> debtByPaymentsMap;
	private Map<string, map<string, string>> professionsByPaymentsMap;
	public integer pPaymentsNumber{ get{ return checkDb.pPaymentsNumber; } set{ checkDb.pPaymentsNumber = value; } }
	public date pFirstChackDepositDate{ get{ return checkDb.pFirstChackDepositDate; } set{ checkDb.pFirstChackDepositDate = value; } }
	public string pFirstChackName{ get{ return checkDb.pFirstChackName; } set{ checkDb.pFirstChackName = value; } }
	public string familyInnerNumber { get; set; }
	
	public CPaymentDb()
	{
		pPayment = new payment__c(); 
	} 
	
	public CPaymentDb(ApexPages.StandardController controller, boolean IsTable)
	{
		pPayment = (Payment__c)controller.getRecord();
		if (pPayment.id != null)
			SetpPaymentBypPaymentId(pPayment.id);
		else
			pPayment.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
		if (IsTable && pPayment.id != null)
			pPayment.Debt__c = math.round(SetDebt(pPayment));
		SetActivityApprovals();
		checkDb = new CCheckDb(pPayment);
		SetFamilyInnerNumber();
	}
	
	private void SetpPaymentBypPaymentId(string id)
	{
		pPayment = [select Debt__c, name, id, IsDone__c, FamilyOrTutor__c, TutorName__c, City__c, Balance__c, MasavFile__c, Contact__c, 
					AmountPaidByMasav__c, ParentName__c, FamilyName__c, Amount__c, Date__c, Comment__c, Status__c, NatureOfPayment__c, 
					PaymentTarget__c, SchoolYear__c from Payment__c where id = :id];
	}
	
	private void SetActivityApprovals()
	{
		if (pPayment.id != null)
		{
			pActivityApprovals = new list<CActivityApprovalInPayment>();
			list<string> activityApprovalList = GetActivityApprovalStrList();
			ActivityApproval__c[] activityApprovals = [select name, id, ProfessionName__r.ProfessionTypeName__r.PaymentType__c, ProfessionName__c, ChildName__c,
													   Note__c, PaymentTarget__c, Rate__c, WeeklyHours__c, ToDate__c, AmountSpent__c, AmountApproved__c, 
													   TotalHoursSpent__c, TotalHours__c, FromDate__c, ProfessionOther__c, TutorName__c, TotalMeetingSpent__c,
													   MeetingsNumber__c from ActivityApproval__c where id in :activityApprovalList];
			for (ActivityApproval__c approval : activityApprovals)
			{
				pActivityApprovals.add(new CActivityApprovalInPayment(approval));
			}
		}
	}
	
	private list<string> GetActivityApprovalStrList()
	{
		ActivityReport__c[] reports = [select name, id, ActivityApprovalName__c from ActivityReport__c where PaymentName__c = :pPayment.id];
		list<string> ret = new list<string>();
		for (ActivityReport__c report : reports)
		{
			ret.add(report.ActivityApprovalName__c);
		}
		return ret;
	}
	
	public void SetFamilyInnerNumber()
	{
		familyInnerNumber = null;
		if (pPayment.FamilyName__c != null)
		{		
			Family__c family = [select name, id, City__c, ParentName__c, InnerNumber__c from Family__c where id = :pPayment.FamilyName__c limit 1];
			if (family != null)
				familyInnerNumber = family.InnerNumber__c;
		}
	}
	
	public void save()
	{
		SetTotalAmount();
		SetCityAndParentName();
		//SetpPaymentName();
		if(pPayment.Contact__c == null )
			pPayment.Debt__c = math.round(SetDebt(pPayment));
		SetIsDone();
		if (pPayment.id == null)
			insert pPayment;
		else
			update pPayment;
		checkDb.InsertChecks();
	}
	
	private void RoundAmounts()
	{
		if (pPayment.Amount__c != null)
			pPayment.Amount__c = math.round(pPayment.Amount__c);
		if (pPayment.Debt__c != null)
			pPayment.Debt__c = math.round(pPayment.Debt__c);
	}
	
	private void SetCityAndParentName()
	{
		if (pPayment.FamilyName__c != null)
		{		
			Family__c[] family = [select name, id, City__c, ParentName__c from Family__c where id = :pPayment.FamilyName__c];
			if (family.size() > 0)
			{
				pPayment.City__c = family[0].City__c;
				pPayment.ParentName__c = family[0].ParentName__c;
			}
		}
	}
	
	public void SetAllPaymentsMap(Set<String> ids)
	{
		Payment__c[] allPaymentsList = [select Debt__c, name, id, IsDone__c, FamilyOrTutor__c, TutorName__c, City__c, NatureOfPayment__c, 
										AmountPaidByMasav__c, ParentName__c, FamilyName__c, Amount__c, Date__c, Comment__c, Status__c, 
										MasavFile__c, PaymentTarget__c, SchoolYear__c from Payment__c where id in :ids];
		allPaymentsMap = new Map<string, Payment__c>();
		allPaymentsMap.putAll(allPaymentsList);
	}
	
	public void SetDebtByPaymentsMap(Set<String> ids)
	{
		debtByPaymentsMap = new Map<string, decimal>();
		SetDebtsMapFromActivityReport(ids);
		SetDebtsMapFromSpecialActivities(ids);
		professionsByPaymentsMap = new Map<string, map<string, string>>();
		SetProfessionsMapFromActivityReport(ids);
		SetProfessionssMapFromSpecialActivities(ids);
	}
	
	private void SetProfessionsMapFromActivityReport(Set<String> ids)
	{
		AggregateResult[] pro = [select ProfessionName__r.name profession, PaymentName__c pname from ActivityReport__c where TotalPayment__c != null 
										and PaymentName__c in :ids group by PaymentName__c, ProfessionName__r.name];
		for (AggregateResult result : pro)
		{
			string pname = string.valueOf(result.get('pname'));
			map<string, string> peofessionsMap = new map<string, string>();
			if (professionsByPaymentsMap.containsKey(pname))
				peofessionsMap = professionsByPaymentsMap.get(pname);
			string profession = string.valueOf(result.get('profession'));
			peofessionsMap.put(profession, profession);
			professionsByPaymentsMap.put(pname, peofessionsMap);
		}
	}
	private void SetProfessionssMapFromSpecialActivities(Set<String> ids)
	{
		AggregateResult[] pro = [select SpecialActivitiesName__r.ActivityDescription__c description, PaymentName__c pname from SpecialActivitiesLine__c where PaymentAmount__c != null 
										and PaymentName__c in :ids group by PaymentName__c, SpecialActivitiesName__r.ActivityDescription__c];
		for (AggregateResult result : pro)
		{
			string pname = string.valueOf(result.get('pname'));
			string description = string.valueOf(result.get('description'));
			map<string, string> peofessionsMap = new map<string, string>();
			if (professionsByPaymentsMap.containsKey(pname))
				peofessionsMap = professionsByPaymentsMap.get(pname);
			peofessionsMap.put(description, description);
			professionsByPaymentsMap.put(pname, peofessionsMap);
		}
	}
	/*
	private void SetDebtsMapFromActivityReport(Set<String> paymentIds)
	{
		List<AggregateResult> totalDebt = [select sum(TotalPayment__c), PaymentName__c from ActivityReport__c where TotalPayment__c != null 
										and PaymentName__c in :paymentIds group by PaymentName__c];
		
		for (integer i = 0; i < totalDebt.size(); i++)
		{
			debtByPaymentsMap.put(string.valueOf(totalDebt[i].get('colel__PaymentName__c')), (decimal)totalDebt[i].get('expr0'));
		}
	}*/
	
	//new version.
	private void SetDebtsMapFromActivityReport(Set<String> paymentIds){
		
		//initialize with value 0
		for(String paymentId : paymentIds){
			debtByPaymentsMap.put(paymentId,0);
		}
		List<AggregateResult> totalDebt = [select sum(TotalPayment__c), PaymentName__c from ActivityReport__c where TotalPayment__c != null 
										and PaymentName__c in :paymentIds group by PaymentName__c];
		system.debug('\n\n\\n\n\n YEHIEL  SetDebtsMapFromActivityReport totalDebt.size();' + totalDebt.size() + '\n\n\\n\n\n');
		
		//update all values found
		//if one payment has no report, it will stay 0 (initialized before)
		for(AggregateResult result: totalDebt){
			debtByPaymentsMap.put(string.valueOf(result.get('PaymentName__c')), (decimal)result.get('expr0'));
		}
		
	}
	
	private void SetDebtsMapFromSpecialActivities(Set<String> ids)
	{
		AggregateResult[] totalDebt = [select sum(PaymentAmount__c), PaymentName__c from SpecialActivitiesLine__c where PaymentAmount__c != null 
										and PaymentName__c in :ids group by PaymentName__c];
		for (AggregateResult result : totalDebt)
		{
			string paymentId = string.valueOf(result.get('PaymentName__c'));
			decimal amount = (decimal)result.get('expr0');
			if (debtByPaymentsMap.containsKey(paymentId))
				amount += debtByPaymentsMap.get(paymentId);
			debtByPaymentsMap.put(paymentId, amount);
		}
	}
	
	private decimal SetDebt(Payment__c payment)
	{
		if(payment.Contact__c != null)
			return payment.Debt__c;
		decimal ret = 0;
		AggregateResult[] totalDebt = [select sum(TotalPayment__c) from ActivityReport__c where PaymentName__c = :payment.Id];
		if (totalDebt.size() > 0 && (decimal)totalDebt[0].get('expr0') != null)
			ret += (decimal)totalDebt[0].get('expr0');
		totalDebt = [select sum(PaymentAmount__c) from SpecialActivitiesLine__c where PaymentName__c = :payment.Id];
		if (totalDebt.size() > 0 && (decimal)totalDebt[0].get('expr0') != null)
			ret += (decimal)totalDebt[0].get('expr0');
		return ret;
	}
	
	private void SetpPaymentName()
	{
		pPayment.name = pPayment.Date__c.day() + '/' + pPayment.Date__c.month() + '/' + pPayment.Date__c.year();
		if (pPayment.TutorName__c != null)
		{
			Tutor__c[] tutor = [select name, id from Tutor__c where id = :pPayment.TutorName__c];
			if (tutor.size() > 0)
				pPayment.name += ' - ' + tutor[0].name;
		}
		if (pPayment.FamilyName__c != null)
		{
			Family__c[] family = [select name, id from Family__c where id = :pPayment.FamilyName__c];
			if (family.size() > 0)
				pPayment.name += ' - ' + family[0].name;
		}
	}
	
	private void SetIsDone()
	{
		integer debt = math.round(pPayment.Debt__c);
		integer amount = math.round(pPayment.Amount__c);
		pPayment.IsDone__c = pPayment.Amount__c != null && pPayment.Amount__c > 0;
		if(pPayment.Status__c == CObjectNames.ActReportStatusPaidByAccountingDpt 
			|| pPayment.Status__c == CObjectNames.ActReportStatusPaidByDonator){
			pPayment.Amount__c = pPayment.Debt__c;
			pPayment.IsDone__c = true;
				return;
		}
		if (amount == null || !(amount > 0))
			pPayment.Status__c = CObjectNames.PaymentStatusNotPaid;
		else if (amount > 0 && amount < debt)
			pPayment.Status__c = CObjectNames.PaymentStatusHalfPaid;
		else if (amount > 0 && amount >= debt)
			pPayment.Status__c = CObjectNames.PaymentStatusPaid;
		else
			pPayment.Status__c = CObjectNames.PaymentStatusNotPaid;
	}
	
	private Family__c paymentFamily;
	public Family__c pPaymentFamily
	{
		get
		{
			if (paymentFamily == null)
			{
				paymentFamily = new Family__c();
				if (pPayment != null && pPayment.FamilyName__c != null)
					paymentFamily = CFamilyDb.GetFamilyById(pPayment.FamilyName__c);
			}
			return paymentFamily;
		}
	}
	
	private Tutor__c paymentTutor;
	public Tutor__c pPaymentTutor
	{
		get
		{
			if (paymentTutor == null)
			{
				paymentTutor = new Tutor__c();
				if (pPayment != null && pPayment.TutorName__c != null)
					paymentTutor = CTutorDb.GetTutorById(pPayment.TutorName__c);
			}
			return paymentTutor;
		}
	}
	
	public Payment__c GetpPayment()
	{
		return pPayment;
	}
	
	public List<Check__c> GetpChecks()
	{
		return checkDb.GetpChecks();
	}
	
	public void OnAddCheck()
	{
		SetTotalAmount();
		checkDb.OnAddCheck(pPayment.Debt__c, pPayment.Amount__c);
		SetTotalAmount();
	}
	
	public void SetChecksByPaymentsNum()
	{
		SetTotalAmount();
		if (pPayment.Amount__c == null || pPayment.Amount__c == 0 || pPayment.Amount__c < 0)
		{
			checkDb.SetChecksByPaymentsNum(pPayment.Debt__c);
			SetTotalAmount();
		}
	}
	
	public void SetTotalAmount(){
		if(pPayment.Status__c == CObjectNames.ActReportStatusPaidByAccountingDpt 
			|| pPayment.Status__c == CObjectNames.ActReportStatusPaidByDonator){
				return;
		}
		if (pPayment.MasavFile__c == null)
			pPayment.AmountPaidByMasav__c = 0;
		pPayment.Amount__c = checkDb.GetTotalAmountFromChecks();
		pPayment.AmountPaidByMasav__c = pPayment.AmountPaidByMasav__c == null ? 0 : pPayment.AmountPaidByMasav__c;
		pPayment.Amount__c += pPayment.AmountPaidByMasav__c;
	}
	
	public void UpdatePaymentDebt(Set <String> paymentsId)
	{
		
		system.debug('\n\n\\n\n\n YEHIEL  UpdatePaymentDebt 11111111' + paymentsId + '\n\n\\n\n\n');
		map<string, Payment__c> mapToUpdate = new map<string, Payment__c>();
		for (string id : paymentsId)
		{
			if (id != null && allPaymentsMap.containsKey(id))
			{
				Payment__c payment = allPaymentsMap.get(id);
				if (debtByPaymentsMap.containsKey(id))
					payment.Debt__c = math.round(debtByPaymentsMap.get(id));
				if (payment.Debt__c == null)
					payment.Debt__c = 0;
				payment.NatureOfPayment__c = '';
				if (professionsByPaymentsMap.containsKey(id))
					payment.NatureOfPayment__c = GetNatureOfPayment(professionsByPaymentsMap.get(id));

				if(payment.Status__c == CObjectNames.ActReportStatusPaidByAccountingDpt 
					|| payment.Status__c == CObjectNames.ActReportStatusPaidByDonator){
						payment.IsDone__c = true;
						payment.Amount__c = payment.Debt__c;
				}
				mapToUpdate.put(payment.id, payment);
			}
		}
		system.debug('\n\n\\n\n\n YEHIEL  mapToUpdate ' + mapToUpdate + '\n\n\\n\n\n');
		if (mapToUpdate.size() > 0)
			update mapToUpdate.values();
	}
	
	private string GetNatureOfPayment(map<string, string> professionsMap)
	{
		list<string> professionsList = professionsMap.values();
		professionsList.sort();
		string ret = '';
		for (string profession : professionsList)
		{
			ret += ret == '' ? profession : '; ' + profession;
		}
		return ret;
	}
	
	public static list<Payment__c> GetPaymentsByIds(list<string> ids)
	{
		return [select name, id, IsDone__c, FamilyOrTutor__c, Comment__c, Debt__c, TutorName__c, Balance__c, FamilyName__c, Amount__c, 
				MasavFile__c, TutorName__r.name, ParentName__c, City__c, Date__c, Status__c, FamilyName__r.ParentName__c, 
				FamilyName__r.name , NatureOfPayment__c, PaymentTarget__c, AmountPaidByMasav__c, SchoolYear__c from Payment__c where id in :ids order by Date__c desc];
	}
}