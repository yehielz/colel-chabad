public with sharing class CWebSiteSupporterChildrenFacad 
{
	private CWebSiteSupporterChildrenDb webSiteSupporterChildrenDb { get; set; }
    
    public Support__c pSupporter{ get{ return webSiteSupporterChildrenDb.pSupporter; } }
	public list<CWSChildInSupporterChildren> pChildren{ get{ return webSiteSupporterChildrenDb.pChildren; } }
	public boolean IsEnglish{ get{ return webSiteSupporterChildrenDb.IsEnglish; } }
    public boolean IsBlur{ get{ return webSiteSupporterChildrenDb.IsBlur; } }
    public boolean IsLastName{ get{ return webSiteSupporterChildrenDb.IsLastName; } }
    
    public integer PageNumber{ get{ return webSiteSupporterChildrenDb.PageNumber; } set{ webSiteSupporterChildrenDb.PageNumber = value; } }
	public integer PageSize{ get{ return webSiteSupporterChildrenDb.PageSize; } set{ webSiteSupporterChildrenDb.PageSize = value; } }
	public boolean IsAllPage{ get{ return webSiteSupporterChildrenDb.IsAllPage; } set{ webSiteSupporterChildrenDb.IsAllPage = value; } }
	public boolean IsFirstPage{ get{ return webSiteSupporterChildrenDb.IsFirstPage; } }
	public boolean IsLastPage{ get{ return webSiteSupporterChildrenDb.IsLastPage; } }
    
    public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
			{
				return null;
			}
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
       
    public CWebSiteSupporterChildrenFacad()
    {
	/*	map<String, String> headers = Apexpages.currentPage().getHeaders();
    	headers.put('Pragma', 'no-cache');
    	headers.put('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0');
    */
    	webSiteSupporterChildrenDb = new CWebSiteSupporterChildrenDb();
    }
    
    public string Direction{ get{ return IsEnglish ? 'ltr' : 'rtl'; } }
    
    public void SetChildrenByAllPage()
    {
    	IsAllPage = true;
    	webSiteSupporterChildrenDb.SetChildren();
    }
    
    public void SetChildrenByNextPage()
    {
    	PageNumber++;
    	webSiteSupporterChildrenDb.SetChildren();
    }
    
    public void SetChildrenByPreviousPage()
    {
    	PageNumber--;
    	webSiteSupporterChildrenDb.SetChildren();
    }
    
    public void SetChildrenByPageNumber()
    {
    	webSiteSupporterChildrenDb.SetChildren();
    }
}