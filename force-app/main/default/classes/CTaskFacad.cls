public with sharing class CTaskFacad 
{
	public string ReminderTime { get; set; }
	public string ReminderDate { get; set; }
	
	public string pageTitle { get; set; }
	public string errorMessage { get; set; }
	
	private CTaskDb taskDb;
	
	public list<selectOption> UsersOption
	{ 
		get
		{
			return taskDb.GetUsersOptions();
		}
	}
	
	public Task pTask
	{
		get
		{
			return taskDb.GetpTask();
		}
	}
	
	public CTaskFacad()
	{
		taskDb = new CTaskDb();
	}
		
	public CTaskFacad(ApexPages.StandardController controller)
	{
		taskDb = new CTaskDb(controller);
		errorMessage = '';
		if (pTask.id == null)
			pageTitle = 'משימה חדשה';
		else
			pageTitle = pTask.Subject;
		SetHourAndDateFromReminderDateTime();
	}
	
	public PageReference save()
	{
		if (!Validation())
			return null;
		
		SetReminderDateTime();
		taskDb.save();
		PageReference newCasePage = new ApexPages.StandardController(pTask).view();
        return newCasePage;
	}
	
	private void SetReminderDateTime()
	{
		if (pTask.IsReminderSet && ReminderDate != null)
		{
			integer day = integer.valueOf(ReminderDate.split('/', 3)[0]);
			integer month = integer.valueOf(ReminderDate.split('/', 3)[1]);
			integer year = integer.valueOf(ReminderDate.split('/', 3)[2]);
			integer hour = integer.valueOf(ReminderTime.split(':', 2)[0]);
			integer minute = integer.valueOf(ReminderTime.split(':', 2)[1]);
			pTask.ReminderDateTime = datetime.newInstance(year, month, day, hour, minute, 0);
		}
		else
			pTask.ReminderDateTime = null;
	}
	
	private void SetHourAndDateFromReminderDateTime()
	{
		if (pTask.ReminderDateTime != null)
		{
			string year = CObjectNames.GetFullNumberInString(pTask.ReminderDateTime.year());
			string month = CObjectNames.GetFullNumberInString(pTask.ReminderDateTime.month());
			string day = CObjectNames.GetFullNumberInString(pTask.ReminderDateTime.day());
			ReminderDate = day + '/' + month + '/' + year;
			string hour = CObjectNames.GetFullNumberInString(pTask.ReminderDateTime.hour());
			string minute = CObjectNames.GetFullNumberInString(pTask.ReminderDateTime.minute());
			ReminderTime = hour + ':' + minute;
		}
	}
	
	private boolean Validation()
    {
    	boolean ret = true;
		CTaskValidator validator = new CTaskValidator(this);
		ret = validator.Validate();
		if (!ret)
			errorMessage = validator.ErrorMessage;
		
		return ret;
    }
    
    public list<selectOption> HoursList
    {
    	get
    	{
    		return taskDb.GetHoursOptions();
    	}
    }
}