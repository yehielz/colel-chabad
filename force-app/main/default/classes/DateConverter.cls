public with sharing class DateConverter 
{
	private double Gauss(double year) 
	{
    	double a,b,c;
    	double m;
    	double Mar;    // "day in March" on which Pesach falls (return value)

   		a = Math.floor(math.mod((12 * integer.valueOf(year) + 17), 19));
    	b = Math.floor(math.mod(integer.valueOf(year), 4));
    	m = 32.044093161144 + 1.5542417966212 * a +  b / 4.0 - 0.0031777940220923 * year;
    	if (m < 0)
       		m -= 1;
    	Mar = Math.floor(m);
    	if (m < 0)
	        m++;
	    m -= Mar;
	
	    c = Math.floor(math.mod((integer.valueOf(Mar) + 3 * integer.valueOf(year) + 5 * integer.valueOf(b) + 5), 7));
    	if(c == 0 && a > 11 && m >= 0.89772376543210 )
	        Mar++;
	    else if(c == 1 && a > 6 && m >= 0.63287037037037)
        	Mar += 2;
    	else if(c == 2 || c == 4 || c == 6)
        	Mar++;

    	Mar += Math.floor((year - 3760) / 100) - Math.floor((year - 3760) / 400) - 2;
    	return Mar;
	}

	private boolean leap(integer y) 
	{
    	return ((math.mod(y, 400) == 0) || (math.mod(y, 100) != 0 && math.mod(y, 4) == 0));
	}
	
	public CMyDate civ2heb(integer day, integer month, integer year) 
	{
    	double d = day;
    	double m = month;
    	double y = year;
    	double hy;
    	double pesach;
    	double anchor;
   		double adarType;

    	m -= 2;
    	if (m <= 0) 
    	{ // Jan or Feb
        	m += 12;
       	 	y -= 1;
    	}

    	d += Math.floor(7 * m / 12 + 30 * (m - 1)); // day in March
    	hy = y + 3760;    // get Hebrew year
    	pesach = Gauss(hy);
    	if (d <= pesach - 15) 
    	{ // before 1 Nisan
	        anchor = pesach;
        	d += 365;
        	if(leap(integer.valueOf(y)))
	            d++;
        	y -= 1;
        	hy -= 1;
        	pesach = Gauss(hy);
    	}
    	else
        	anchor = Gauss(hy + 1);

    	d -= pesach - 15;
    	anchor -= pesach - 12;
   	 	y++;
   	 	if(leap(integer.valueOf(y)))
        	anchor++;

		for(m = 0; m < 11; m++) 
		{
        	double days;
        	if(m == 7 && math.mod(integer.valueOf(anchor), 30) == 2)
            	days = 30; // Cheshvan
        	else if(m == 8 && math.mod(integer.valueOf(anchor), 30) == 0)
            	days = 29; // Kislev
        	else
            	days = 30 - math.mod(integer.valueOf(m), 2);
        	if(d <= days)
            	break;
        	d -= days;
    	}

    	adarType = 0;            // plain old Adar
    	if (m == 11 && anchor >= 30) 
    	{
        	if (d > 30) 
        	{
            	adarType = 2;    // Adar 2
            	d -= 30;
        	}
        	else
            	adarType = 1;    // Adar 1
    	}

    	if(m >= 6)        // Tishrei or after?
        	hy++;        // then bump up year

    	if(m == 11)            // Adar?
        	m += adarType;    // adjust for Adars
		
		CMyDate hebrewDate = new CMyDate(integer.valueOf(hy), integer.valueOf(m), integer.valueOf(d));
		return hebrewDate;
	}
 	
 	/* kdate.js - Kaluach Javascript Hebrew date routines
 *   Version 0.03 (beta release)
 * Copyright (C) 5760 (2000 CE), by Abu Mami and Yisrael Hersch.
 *   All Rights Reserved.
 *   All copyright notices in this script must be left intact.
 * Based on the formula by Gauss
 * Terms of use:
 *   - Permission will be granted to use this script on personal
 *     web pages. All that's required is that you please ask.
 *     (Of course if you want to send a few dollars, that's OK too :-)
 *   - Use on commercial web sites requires a $50 payment.
 * website: http://www.kaluach.net
 * email: abu-mami@kaluach.net
 */

/*function makeArray() {
    this[0] = makeArray.arguments.length;
    for (i = 0; i < makeArray.arguments.length; i = i + 1)
        this[i+1] = makeArray.arguments[i];
}

var hebMonth = new makeArray(
    'Nisan', 'Iyyar', 'Sivan', 'Tammuz', 'Av', 'Elul',
    'Tishrei', 'Cheshvan', 'Kislev', 'Tevet', 'Shevat',
    'Adar', 'Adar I', 'Adar II');

var civMonth = new makeArray(
    'January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December');

var weekDay = new makeArray(
    'Sun', 'Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Shabbat');*/

	
/*
function civMonthLength(month, year) {
    if(month == 2)
        return 28 + leap(year);
    else if(month == 4 || month == 6 || month == 9 || month == 11)
       return 30;
    else
        return 31;
}*/

	

/*function Easter(Y) {
    // based on the algorithm of Oudin
    var C = Math.floor(Y / 100);
    var N = Y - 19 * Math.floor(Y / 19);
    var K = Math.floor((C - 17) / 25);
    var I = C - Math.floor(C / 4) - Math.floor((C - K) / 3) + 19 * N + 15;
    I = I - 30*Math.floor((I / 30));
    I = I - Math.floor(I / 28) * (1 - Math.floor(I / 28) * Math.floor(29 / (I + 1)) * Math.floor((21 - N) / 11));
    var J = Y + Math.floor(Y / 4) + I + 2 - C + Math.floor(C / 4);
    J = J - 7 * Math.floor(J / 7);
    var L = I - J;
    var M = 3 + Math.floor((L + 40) / 44);
    var D = L + 28 - 31 * Math.floor(M / 4);

    var ret = new Object();
    ret[1] = M;
    ret[2] = D;
    return ret;
}

function DOW(day,month,year) {
    var a = Math.floor((14 - month)/12);
    var y = year - a;
    var m = month + 12*a - 2;
    var d = (day + y + Math.floor(y/4) - Math.floor(y/100) +
            Math.floor(y/400) + Math.floor((31*m)/12)) % 7;
    return d + 1;
}

function NthDOW(nth,weekday,month,year) {
    if (nth > 0)
        return (nth - 1) * 7 + 1 + (7 + weekday - DOW((nth - 1) * 7 + 1, month, year)) % 7;
    var days = civMonthLength(month, year);
    return days - (DOW(days, month, year) - weekday + 7) % 7;
}

function holidays(cday, cmonth, cyear) {
    // American civil holidays and some major religious holiday
    if (cmonth == 1 && cday == 1)
        return "New Year's Day";
    else if (cmonth == 2 && cday == 12)
        return "Lincoln's Birthday";
    else if (cmonth == 2 && cday == 14)
        return "Valentine's Day";
    else if (cmonth == 2 && cday == NthDOW(3, 2, 2, cyear))
        return "President's Day";
    else if (cmonth == 3 && cday == 17)
        return "St. Patrick's Day";
    else if (cmonth == 3 || cmonth == 4) {
        var e = Easter(cyear);
        if (cmonth == e[1] && cday == e[2])
            return "Easter";
    }
    else if (cmonth == 5 && cday == NthDOW(2, 1, 5, cyear))
        return "Mother's Day";
    else if (cmonth == 5 && cday == NthDOW(3, 7, 5, cyear))
        return "Armed Forces Day";
    else if (cmonth == 5 && cday == NthDOW(0, 2, 5, cyear))
        return "Memorial Day";
    else if (cmonth == 6 && cday == 14)
        return "Flag Day";
    else if (cmonth == 6 && cday == NthDOW(3, 1, 6, cyear))
        return "Father's Day";
    else if (cmonth == 7 && cday == 4)
        return "Independence Day";
    else if (cmonth == 9 && cday == NthDOW(1, 2, 9, cyear))
        return "Labor Day";
    else if (cmonth == 10 && cday == NthDOW(2, 2, 10, cyear))
        return "Columbus Day";
    else if (cmonth == 10 && cday == 31)
        return "Halloween";
    else if (cmonth == 11 && cday == 11)
        return "Veterans' Day";
    else if (cmonth == 11 && cday == NthDOW(4, 5, 11, cyear))
        return "Thanksgiving";
    else if (cmonth == 12 && cday == 25)
        return "Christmas";

    return "";
}

function moadim(cday, cmonth, cyear, hday, hmonth, dow) {
    if(hmonth == 6) {
        if(hday == 1 || hday == 2)
            return "Rosh Hashana"
        else if(hday == 3 && dow != 7)
            return "Fast of Gedalia";
        else if(hday == 4 && dow == 1)
            return "Fast of Gedalia";
        else if(hday == 10)
            return "Yom Kippur"
        else if(hday >= 15 && hday <= 22)
            return "Sukkot"
        else if(hday == 23)
            return "Sukkot (d)"
    }
    else if(hmonth == 8) {
        if(hday >= 25)
            return "Chanukkah"
    }
    else if(hmonth == 9) {
        if(hday <= 2) {
            return "Chanukkah"
        }
        else if(hday == 3) {
            // Kislev can be malei or chaser
            if(cday == 1) {
                cday = 29;
                cmonth = 11;
            }
            else if(cday == 2) {
                cday = 30;
                cmonth = 11;
            }
            else
                cday -= 3;
            var hdate = civ2heb(cday, cmonth, cyear);
            hd = eval(hdate.substring(0, hdate.indexOf(' ')));
            if(hd == 29)
                return "Chanukkah"
        }
        else if(hday == 10)
            return "Fast of Tevet"
    }
    else if(hmonth == 10) {
        if(hday==15)
            return "Tu b'Shvat"
    }
    else if(hmonth == 11 || hmonth == 13) {
        if(hday == 11 && dow == 5)
            return "Taanit Esther"
        else if(hday == 13 && dow != 7)
            return "Taanit Esther"
        else if(hday == 14)
            return "Purim"
        else if(hday == 15)
            return "Shushan Purim"
    }
    else if(hmonth == 0) {

        if(hday == 12 && dow == 5)
            return "Taanit Bechorot"
        else if(hday == 14 && dow != 7)
            return "Taanit Bechorot"
        else if(hday >= 15 && hday <= 21)
            return "Pesach"
        else if(hday == 22)
            return "Pesach (d)"
    }
    else if(hmonth == 1) {
        if(hday == 3 && dow == 5)
            return "Yom Ha'Atzmaut"
        else if(hday == 4 && dow == 5)
            return "Yom Ha'Atzmaut"
        else if(hday == 5 && dow != 6 && dow != 7)
            return "Yom Ha'Atzmaut"
        if(hday == 14)
            return "Pesah sheni"
        else if(hday == 18)
            return "Lag B'Omer"
        if(hday == 28)
            return "Yom Yerushalayim"
    }
    else if(hmonth == 2) {
        if(hday == 6)
            return "Shavuot"
        else if(hday == 7)
            return "Shavuot (d)"
    }
    else if(hmonth == 3) {
        if(hday == 17 && dow != 7)
            return "Fast of Tammuz"
        if(hday == 18 && dow == 1)
            return "Fast of Tammuz"
    }
    else if(hmonth == 4) {
        if(hday == 9 && dow != 7)
            return "Tisha B'Av"
        if(hday == 10 && dow == 1)
            return "Tisha B'Av"
        if(hday == 15)
            return "Tu B'Av"
    }

    return "";
}*/




	//*********************************************************************************************************************
 	//*********************************************************************************************************************
 	//*********************************************************************************************************************


	
	
	/*// This function converts a Gregorian date into the Hebrew date.  The
 	// function returns the hebrew month as a string in the format M/D/Y.
 	// See function HebToGreg() for the definition of the month numbers.
 	public string GregToHeb(datetime dGreg)
	{
   		decimal nYearH;
   		decimal nMonthH;
   		decimal nDateH;
   		double nOneMolad;
   		decimal nAvrgYear;
   		decimal nDays;
   		datetime dTishrei1;
   		decimal nLengthOfYear;
   		boolean bLeap;
   		boolean bHaser;
   		boolean bShalem;
   		decimal nMonthLen;
   		boolean bWhile;
   		datetime d1900 = date.newinstance(1900, 1, 1);

   		// The basic algorythm to get Hebrew date for the Gregorian date dGreg.
   		// 1) Find out how many days dGreg is after creation.
   		// 2) Based on those days, estimate the Hebrew year
   		// 3) Now that we a good estimate of the Hebrew year, use brute force to
   		//    find the Gregorian date for Tishrei 1 prior to or equal to dGreg
   		// 4) Add to Tishrei 1 the amount of days dGreg is after Tishrei 1
		decimal hefresh = 30.91666666666667;
   		// Figure out how many days are in a month.
   		// 29 days + 12 hours + 793 chalakim
		system.debug('d1900.getTime() = ' + d1900.getTime());
		nOneMolad = 29.0 + (12.0 / 24.0) + (793.0 / (1080.0 * 24.0));
   		// Figure out the average length of a year. The hebrew year has exactly
   		// 235 months over 19 years.
   		nAvrgYear = nOneMolad * (235.0 / 19.0);
   		// Get how many days dGreg is after creation. See note as to why I
   		// use 1/1/1900 and add 2067025
   		system.debug('dGreg.getTime() = ' + (dGreg.getTime()));
   		nDays = Math.round(((dGreg.getTime()) - (d1900.getTime())) / (24.0 * 60.0 * 60.0 * 1000.0));
   		nDays += 2067025.0; // 2067025 days after creation
   		
   		// Guess the Hebrew year. This should be a pretty accurate guess.
   		nYearH = math.floor(nDays / nAvrgYear) + 1.0;
   		// Use brute force to find the exact year nYearH. It is the Tishrei 1 in
   		// the year <= dGreg.
   		dTishrei1 = Tishrei1(nYearH);

   		if (SameDate(dTishrei1, dGreg)) 
   		{
     		// If we got lucky and landed on the exact date, we can stop here
     		nMonthH = 1.0;
 			nDateH = 1.0;
   		}
   		else  
   		{
     		// Here is the brute force.  Either count up or count down nYearH
     		// until Tishrei 1 is <= dGreg.
     		if (dTishrei1 < dGreg) 
 			{
   				// If Tishrei 1, nYearH is less than dGreg, count nYearH up.
   				while (Tishrei1(nYearH + 1.0) <= dGreg) 
   				{
         			nYearH += 1.0;
       			}
     		}
 			else 
 			{
       			// If Tishrei 1, nYearH is greater than dGreg, count nYearH down.
       			nYearH -= 1.0;
       			while (Tishrei1(nYearH) > dGreg) 
       			{
         			nYearH -= 1.0;
       			}
     		}
	

	
			// Subtract Tishrei 1, nYearH from dGreg. That should leave us with
     		// how many days we have to add to Tishrei 1
     		/*מכאן הבעיה ------------=================================================
     		//system.debug('nDays 111 = ' + nDays);
     		//system.debug('dGreg.getTime() = ' + dGreg.getTime());
     		//system.debug('Tishrei1(nYearH).getTime() = ' + Tishrei1(nYearH).getTime());
     		//system.debug('dGreg.getTime() - Tishrei1(nYearH).getTime() = ' + (dGreg.getTime() - Tishrei1(nYearH).getTime()));
     		//system.debug('Tishrei1(nYearH).date() = ' + Tishrei1(nYearH).day() + '/' + Tishrei1(nYearH).Month() + '/' + Tishrei1(nYearH).Year());
     		nDays = ((dGreg.getTime()) - (Tishrei1(nYearH).getTime())) / (24.0 * 60.0 * 60.0 * 1000.0);
     		//system.debug('nDays 222 = ' + nDays);
     		nDays = Math.round(nDays);
     		//nDays = nDays -1;
     		//system.debug('nDays 333 = ' + nDays);
     		/*עד כאן הבעיה --------------===================================================
     		// Find out what type of year it is so that we know the length of the
     		// months
     		nLengthOfYear = LengthOfYear(nYearH);
     		bHaser = (nLengthOfYear == 353 || nLengthOfYear == 383);
    		bShalem = (nLengthOfYear == 355 || nLengthOfYear == 385);
     		bLeap = IsLeapYear(nYearH);

     		// Add nDays to Tishrei 1.
     		nMonthH = 1;
     		do 
     		{
     			system.debug('111111111111111 = ' + nMonthH);
       			if (nMonthH == 1.0 || nMonthH == 5.0 || nMonthH == 6.0 || nMonthH == 8.0 || nMonthH == 10.0 || nMonthH == 12.0)
       			{
       				system.debug('222222222222222222 = ' + nMonthH);
       				nMonthLen = 30.0;
       			}
       			else if (nMonthH == 4.0 || nMonthH == 7.0 || nMonthH == 9.0 || nMonthH == 11.0 || nMonthH == 13.0)
       			{
       				system.debug('333333333333333333333 = ' + nMonthH);
       				nMonthLen = 29.0;
       			}
       			else if (nMonthH == 6.0)
       			{
       				system.debug('44444444444444444 = ' + nMonthH);
       				nMonthLen = 30.0;
       			}
       			else if (nMonthH == 2.0)
       			{
       				system.debug('55555555555555 = ' + nMonthH);
       				nMonthLen = (bShalem ? 30.0 : 29.0);
       			}
       			else if (nMonthH == 3.0)
       			{
       				system.debug('6666666666666666666 = ' + nMonthH);
       				nMonthLen = (bHaser ? 29.0: 30.0);
       			}
       			/*switch (nMonthH) 
       			{
         			case 1:
         			case 5:
         			case 6:
         			case 8:
         			case 10:
         			case 12:
           			
           			break
         			case 4:
         			case 7:
         			case 9:
         			case 11:
         			case 13:
           			
           			break
         			case 6: // Adar A (6) will be skipped on non-leap years
           			
       				break
         			case 2: // Cheshvan, see note above
           			
           			break
        			case 3: // Kislev, see note above
           			
       				break
   				}

   				if (nDays >= nMonthLen) 
   				{
	     			bWhile = true;
	     			if (bLeap || nMonthH != 5.0) 
	         		{
	       				nMonthH ++;
	         		}
	         		else 
	         		{
	       				// We can skip Adar A (6) if its not a leap year
	       				nMonthH += 2.0;
	         		}
	         		nDays -= nMonthLen;
   				}
   				else 
       			{
     				bWhile = false;
       			}
 			} 
 			while (bWhile);
 			//Add the remaining days to Date
			nDateH = nDays + 1.0;
   		}
   		return nMonthH + '/' + nDateH + '/' + nYearH;
 	}
 	
 	 private decimal LengthOfYear(decimal nYearH) 
 	 {
  		datetime dThisTishrei1;
  		datetime dNextTishrei1;
   		decimal diff;
		decimal hefresh = 30.91666666666667;
		
   		// subtract the date of this year from the date of next year
   		dThisTishrei1 = Tishrei1(nYearH);
   		dNextTishrei1 = Tishrei1(nYearH + 1.0);
   		// Java's dates are stored in milliseconds. To convert it into days
   		// we have to divide it by 1000 * 60 * 60 * 24
   		diff = ((dNextTishrei1.getTime()) - (dThisTishrei1.getTime())) / ( 1000.0 * 60.0 * 60.0 * 24.0);
   		return Math.round(diff);
 	}

 	private boolean SameDate(datetime d1, datetime d2) 
 	{
  		return (d1.Year() == d2.Year() &&
          		d1.Month() == d2.Month() &&
           		d1.Day() == d2.Day());
 	}
	
	private datetime Tishrei1(decimal nYearH) 
	{
   		decimal nMonthsSinceFirstMolad;
   		decimal nChalakim;
   		decimal nHours;
   		decimal nDays;
   		decimal nDayOfWeek;
   		datetime dTishrei1;

  		// We want to calculate how many days, hours and chalakim it has
   		// been from the time of 0 days, 0 hours and 0 chalakim to the
   		// molad at the beginning of year nYearH.
   		//
   		// The period between one new moon to the next is 29 days, 12
   		// hours and 793 chalakim. We must multiply that by the amount
   		// of months that transpired since the first molad. Then we add
   		// the time of the first molad (Monday, 5 hours and 204 chalakim)
   		nMonthsSinceFirstMolad = MonSinceFirstMolad(nYearH);
   		nChalakim = 793.0 * nMonthsSinceFirstMolad;
   		nChalakim += 204.0;
	   	// carry the excess Chalakim over to the hours
   		nHours = Math.floor(nChalakim / 1080.0);
   		nChalakim = math.mod((integer)nChalakim, 1080);
		
   		nHours += nMonthsSinceFirstMolad * 12.0;
   		nHours += 5.0;
		
   		// carry the excess hours over to the days
   		nDays = Math.floor(nHours / 24.0);
   		nHours = math.mod((integer)nHours, 24);
		
   		nDays += 29.0 * nMonthsSinceFirstMolad;
   		nDays += 2.0;
		
   		// figure out which day of the week the molad occurs.
   		// Sunday = 1, Moday = 2 ..., Shabbos = 0
   		nDayOfWeek = math.mod((integer)nDays, 7);
		
   		// In a perfect world, Rosh Hashanah would be on the day of the
   		// molad. The Hebrew calendar makes four exceptions where we
   		// push off Rosh Hashanah one or two days. This is done to
   		// prevent three situation. Without explaining why, the three
   		// situations are:
   		//   1) We don't want Rosh Hashanah to come out on Sunday,
   		//      Wednesday or Friday
   		//   2) We don't want Rosh Hashanah to be on the day of the
   		//      molad if the molad occurs after the beginning of 18th
   		//      hour.
   		//   3) We want to limit years to specific lengths.  For non-leap
   		//      years, we limit it to either 353, 354 or 355 days.  For
   		//      leap years, we limit it to either 383, 384 or 385 days.
   		//      If setting Rosh Hashanah to the day of the molad will
   		//      cause this year, or the previous year to fall outside
   		//      these lengths, we push off Rosh Hashanah to get the year
   		//      back to a valid length.
   		// This code handles these exceptions.
   		if (!IsLeapYear(nYearH) &&
       	nDayOfWeek == 3.0 &&
       	(nHours * 1080.0) + nChalakim >= (9.0 * 1080.0) + 204.0) 
		{
    
     		// This prevents the year from being 356 days. We have to push
     		// Rosh Hashanah off two days because if we pushed it off only
     		// one day, Rosh Hashanah would comes out on a Wednesday. Check
     		// the Hebrew year 5745 for an example.
     		nDayOfWeek = 5.0;
     		nDays += 2.0;
   		}
   		else if ( IsLeapYear(nYearH - 1.0) &&
 		nDayOfWeek == 2.0 &&
        (nHours * 1080.0) + nChalakim >= (15.0 * 1080.0) + 589.0 ) 
        {
     		// This prevents the previous year from being 382 days. Check
     		// the Hebrew Year 5766 for an example. If Rosh Hashanah was not
     		// pushed off a day then 5765 would be 382 days
     		nDayOfWeek = 3.0;
     		nDays += 1.0;
   		}
   		else 
   		{
     		// see rule 2 above. Check the Hebrew year 5765 for an example
     		if (nHours >= 18.0) 
     		{
      			nDayOfWeek += 1.0;
       			nDayOfWeek = math.mod((integer)nDayOfWeek, 7);
       			nDays += 1.0;
     		}
     		// see rule 1 above. Check the Hebrew year 5765 for an example
     		if (nDayOfWeek == 1.0 ||
         	nDayOfWeek == 4.0 ||
         	nDayOfWeek == 6.0) 
         	{
       			nDayOfWeek += 1.0;
       			nDayOfWeek = math.mod((integer)nDayOfWeek, 7);
       			nDays += 1.0;
     		}
   		}
   		
   		// Here we want to add nDays to creation
   		//    dTishrie1 = creation + nDays
   		// Unfortunately, Many languages do not handle negative years very
   		// well. I therefore picked a Random date (1/1/1900) and figured out
   		// how many days it is after the creation (2067025). Then I
  		// subtracted 2067025 from nDays.
   		nDays -= 2067025.0;
   		dTishrei1 = date.newinstance(1900, 0, 1); // 2067025 days after creation
   		//system.debug('dTishrei1 111 = ' + dTishrei1);
   		dTishrei1 = dTishrei1.date().addDays((integer)nDays);
   		//system.debug('nDays 111121212121212121212121212121212121212121212121212121212 = ' + nDays);
		//system.debug('dTishrei1 222 = ' + dTishrei1);
   		return dTishrei1;	
    }
   
    private boolean IsLeapYear(decimal nYearH) 
    {
    	decimal nYearInCycle;

   		// Find out which year we are within the cycle.  The 19th year of
   		// the cycle will return 0
   		nYearInCycle = math.mod((integer)nYearH, 19);
   		return (nYearInCycle ==  3.0 ||
            	nYearInCycle ==  6.0 ||
            	nYearInCycle ==  8.0 ||
            	nYearInCycle == 11.0 ||
            	nYearInCycle == 14.0 ||
            	nYearInCycle == 17.0 ||
            	nYearInCycle == 0.0);
 	}
   
	private decimal MonSinceFirstMolad(decimal nYearH) 
	{
   		decimal nMonSinceFirstMolad;

   		// A shortcut to this function can simply be the following formula
   		//   return Math.floor(((235 * nYearH) - 234) / 19)
   		// This formula is found in Remy Landau's website and he
   		// attributes it to Wolfgang Alexander Shochen. I will use a less
   		// optimized function which I believe shows the underlying logic
   		// better.

   		// count how many months there has been in all years up to last
   		// year. The months of this year hasn't happened yet.
   		nYearH --;

   		// In the 19 year cycle, there will always be 235 months. That
   		// would be 19 years times 12 months plus 7 extra month for the
   		// leap years. (19 * 12) + 7 = 235.

   		// Get how many 19 year cycles there has been and multiply it by
   		// 235
   		nMonSinceFirstMolad = Math.floor(nYearH / 19.0) * 235.0;
   		// Get the remaining years after the last complete 19 year cycle
   		nYearH = math.mod((integer)nYearH, 19);
   		// Add 12 months for each of those years
   		nMonSinceFirstMolad += 12.0 * nYearH;
   		// Add the extra months to account for the leap years
   		if (nYearH >= 17.0) 
   		{
     		nMonSinceFirstMolad += 6.0;
   		} 
   		else if  (nYearH >= 14.0) 
   		{
     		nMonSinceFirstMolad += 5.0;
   		} 
   		else if  (nYearH >= 11.0) 
   		{
     		nMonSinceFirstMolad += 4.0;
   		} 
   		else if  (nYearH >= 8.0) 
   		{
     		nMonSinceFirstMolad += 3.0;
   		} 
   		else if  (nYearH >= 6.0) 
   		{
     		nMonSinceFirstMolad += 2.0;
   		} 
   		else if  (nYearH >= 3.0) 
   		{
    		nMonSinceFirstMolad += 1.0;
   		}
   		return nMonSinceFirstMolad;
 	}*/
 	
 	
 	
 	//מכאן זה קוד שלי לא מועתק משום מקום!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 	
 	
 	public static string GetHebrewMonth(integer month){
 		if (month == 0) 
 			return 'ניסן';
 		else if (month == 1) 
 			return 'אייר';
 		else if (month == 2) 
 			return 'סיון';
 		else if (month == 3) 
 			return 'תמוז';
 		else if (month == 4) 
 			return 'אב';
 		else if (month == 5) 
 			return 'אלול';
 		else if (month == 6) 
 			return 'תשרי';
 		else if (month == 7) 
 			return 'חשון';
 		else if (month == 8) 
 			return 'כסלו';
 		else if (month == 9) 
 			return 'טבת';
 		else if (month == 10) 
 			return 'שבט';
 		else if (month == 11) 
 			return 'אדר';
 		else if (month == 12) 
 			return 'אדר א';
 		else if (month == 13) 
 			return 'אדר ב';
 		return null;
 		
 	}
 	
 	public string GetHebrewMonthInHebrew(integer month){
 		return GetHebrewMonth (month);
 	}
 	
 	
 	public static string GetMonthInHebrew(integer month){
 		
		if (month == 0) 
 			return 'דצבמר';
 		else if (month == 1) 
 			return 'ינואר';
 		else if (month == 2) 
 			return 'פברואר';
 		else if (month == 3) 
 			return 'מרץ';
 		else if (month == 4) 
 			return 'אפריל';
 		else if (month == 5) 
 			return 'מאי';
 		else if (month == 6) 
 			return 'יוני';
 		else if (month == 7) 
 			return 'יולי';
 		else if (month == 8) 
 			return 'אוגוסט';
 		else if (month == 9) 
 			return 'ספטמבר';
 		else if (month == 10) 
 			return 'אוקטובר';
 		else if (month == 11) 
 			return 'נובמבר';
 		else if (month == 12) 
 			return 'דצבמר';
 		return null;
 	}
 	
 	public string GetHebrewDayInHebrew(integer day)	{
 		if (day == 1) 
 			return 'א';
 		else if (day == 2) 
 			return 'ב';
 		else if (day == 3) 
 			return 'ג';
 		else if (day == 4) 
 			return 'ד';
 		else if (day == 5) 
 			return 'ה';
 		else if (day == 6) 
 			return 'ו';
 		else if (day == 7) 
 			return 'ז';
 		else if (day == 8) 
 			return 'ח';
 		else if (day == 9) 
 			return 'ט';
 		else if (day == 10) 
 			return 'י';
 		else if (day == 11) 
 			return 'יא';
 		else if (day == 12) 
 			return 'יב';
 		else if (day == 13) 
 			return 'יג';
 		else if (day == 14) 
 			return 'יד';
 		else if (day == 15) 
 			return 'טו';
 		else if (day == 16) 
 			return 'טז';
 		else if (day == 17) 
 			return 'יז';
 		else if (day == 18) 
 			return 'יח';
 		else if (day == 19) 
 			return 'יט';
 		else if (day == 20) 
 			return 'כ';
 		else if (day == 21) 
 			return 'כא';
 		else if (day == 22) 
 			return 'כב';
 		else if (day == 23) 
 			return 'כג';
 		else if (day == 24) 
 			return 'כד';
 		else if (day == 25) 
 			return 'כה';
 		else if (day == 26) 
 			return 'כו';
 		else if (day == 27) 
 			return 'כז';
 		else if (day == 28) 
 			return 'כח';
 		else if (day == 29) 
 			return 'כט';
 		else if (day == 30) 
 			return 'ל';
 		return null;
 	}
 	
 	public string GetHebrewYearInHebrew(integer year)
 	{
 		if (year >= 6000)
 			year -= 6000;
 		if (year >= 5000)
 			year -= 5000;
 		string yearInHeb = '';
 		while (year >= 400)
 		{
 			year -= 400;
 			yearInHeb += 'ת';
 		}
 		while (year >= 300)
 		{
 			year -= 300;
 			yearInHeb += 'ש';
 		}
 		while (year >= 200)
 		{
 			year -= 200;
 			yearInHeb += 'ר';
 		}
 		while (year >= 100)
 		{
 			year -= 100;
 			yearInHeb += 'ק';
 		}
 		while (year >= 90)
 		{
 			year -= 90;
 			yearInHeb += 'צ';
 		}
 		while (year >= 80)
 		{
 			year -= 80;
 			yearInHeb += 'פ';
 		}
 		while (year >= 70)
 		{
 			year -= 70;
 			yearInHeb += 'ע';
 		}
 		while (year >= 60)
 		{
 			year -= 60;
 			yearInHeb += 'ס';
 		}
 		while (year >= 50)
 		{
 			year -= 50;
 			yearInHeb += 'נ';
 		}
 		while (year >= 40)
 		{
 			year -= 40;
 			yearInHeb += 'מ';
 		}
 		while (year >= 30)
 		{
 			year -= 30;
 			yearInHeb += 'ל';
 		}
 		while (year >= 20)
 		{
 			year -= 20;
 			yearInHeb += 'כ';
 		}
 		while (year >= 10)
 		{
 			year -= 10;
 			yearInHeb += 'י';
 		}
 		if (year == 9)
 			yearInHeb += 'ט';
 		else if (year == 8)
 			yearInHeb += 'ח';
 		else if (year == 7)
 			yearInHeb += 'ז';
 		else if (year == 6)
 			yearInHeb += 'ו';
 		else if (year == 5)
 			yearInHeb += 'ה';
 		else if (year == 4)
 			yearInHeb += 'ד';
 		else if (year == 3)
 			yearInHeb += 'ג';
 		else if (year == 2)
 			yearInHeb += 'ב';
 		else if (year == 1)
 			yearInHeb += 'א';
 		string ret = yearInHeb.substring(0, yearInHeb.length() - 1) + '"' + yearInHeb.substring(yearInHeb.length() - 1);
 		return ret;
 	}
 	
 	
 	//     for   test    ************************************* for     test   ******************************  for     test ********
 	
 	
 	public void DoAllDays()
	{
		GetHebrewDayInHebrew(1);
		GetHebrewDayInHebrew(2);
		GetHebrewDayInHebrew(3);
		GetHebrewDayInHebrew(4);
		GetHebrewDayInHebrew(5);
		GetHebrewDayInHebrew(6);
		GetHebrewDayInHebrew(7);
		GetHebrewDayInHebrew(8);
		GetHebrewDayInHebrew(9);
		GetHebrewDayInHebrew(10);
		GetHebrewDayInHebrew(11);
		GetHebrewDayInHebrew(12);
		GetHebrewDayInHebrew(13);
		GetHebrewDayInHebrew(14);
		GetHebrewDayInHebrew(15);
		GetHebrewDayInHebrew(16);
		GetHebrewDayInHebrew(17);
		GetHebrewDayInHebrew(18);
		GetHebrewDayInHebrew(19);
		GetHebrewDayInHebrew(20);
		GetHebrewDayInHebrew(21);
		GetHebrewDayInHebrew(22);
		GetHebrewDayInHebrew(23);
		GetHebrewDayInHebrew(24);
		GetHebrewDayInHebrew(25);
		GetHebrewDayInHebrew(26);
		GetHebrewDayInHebrew(27);
		GetHebrewDayInHebrew(28);
		GetHebrewDayInHebrew(29);
		GetHebrewDayInHebrew(30);
	}
	
	public void DoAllMonths()
	{
		GetHebrewMonthInHebrew(0);
		GetHebrewMonthInHebrew(1);
		GetHebrewMonthInHebrew(2);
		GetHebrewMonthInHebrew(3);
		GetHebrewMonthInHebrew(4);
		GetHebrewMonthInHebrew(5);
		GetHebrewMonthInHebrew(6);
		GetHebrewMonthInHebrew(7);
		GetHebrewMonthInHebrew(8);
		GetHebrewMonthInHebrew(9);
		GetHebrewMonthInHebrew(10);
		GetHebrewMonthInHebrew(11);
		GetHebrewMonthInHebrew(12);
		GetHebrewMonthInHebrew(13);
	}
	
	public void DoAllYears()
	{
		GetHebrewYearInHebrew(6779);
		GetHebrewYearInHebrew(5688);
		GetHebrewYearInHebrew(5597);
		GetHebrewYearInHebrew(5566);
		GetHebrewYearInHebrew(5555);
		GetHebrewYearInHebrew(5544);
		GetHebrewYearInHebrew(5533);
		GetHebrewYearInHebrew(5522);
		GetHebrewYearInHebrew(5511);
	}
}