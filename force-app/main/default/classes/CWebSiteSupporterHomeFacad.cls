public with sharing class CWebSiteSupporterHomeFacad 
{
	private CWebSiteSupporterHomeDb webSiteSupporterHomeDb { get; set; }
    
    public Support__c pSupporter{ get{ return webSiteSupporterHomeDb.pSupporter; } }
	private list<string> pChildrenIds{ get{ return webSiteSupporterHomeDb.pChildrenIds; } }
	public string pChildrenIdsJson{ get{ return Json.serialize(webSiteSupporterHomeDb.pChildrenIds).replace('"', '\\"').replace('\'', '\\\''); } }
	public integer pNumberOfTotalChildren{ get{ return webSiteSupporterHomeDb.pNumberOfTotalChildren; } }
	public integer pNumberOfChildren{ get{ return webSiteSupporterHomeDb.pNumberOfChildren; } }
	public integer pNumberOfRequests{ get{ return webSiteSupporterHomeDb.pNumberOfRequests; } }
	public integer pNumberOfApprovedRequests{ get{ return webSiteSupporterHomeDb.pNumberOfApprovedRequests; } }
	public integer pNumberOfReports{ get{ return webSiteSupporterHomeDb.pNumberOfReports; } }
	public boolean IsEnglish{ get{ return webSiteSupporterHomeDb.IsEnglish; } }
     
    public PageReference ValidateIsOnline()
	{
		if (ApexPages.currentPage().getCookies().get('sessionId') != null)
		{
			SessionID__c sessionId = CSessionIdDb.GetSessionIDById(ApexPages.currentPage().getCookies().get('sessionId').getValue());
			if (sessionId != null && CSessionIdDb.ValidateSessionIdTime(sessionId))
			{
				return null;
			}
		}
		return new PageReference(CObjectNames.WebSitePageLogIn);
	}
       
    public CWebSiteSupporterHomeFacad()
    {
    	webSiteSupporterHomeDb = new CWebSiteSupporterHomeDb();
    }
    
    public string Direction{ get{ return IsEnglish ? 'ltr' : 'rtl'; } }
    
}