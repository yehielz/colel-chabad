public with sharing class CWebSiteDb 
{
	public static Tutor__c tutorOnLine { get; set; }
	
	public string UserName { get; set; }
	public string UserNameWarning { get; set; }
	public string UserNameBorderColor { get; set; }
	public string Password { get; set; }
	public string PasswordWarning { get; set; }
	public string PasswordBorderColor { get; set; }
	
	public CWebSiteDb()
	{
		UserNameBorderColor = '#ccc';
		PasswordBorderColor = '#ccc';
	}
	
	public boolean validateUserName()
	{
		if (UserName == null || UserName == '')
		{
			UserNameBorderColor = 'red';
			UserNameWarning = 'אנא מלא שם משתמש';
			PasswordBorderColor = '#ccc';
			PasswordWarning = '';
			return false;
		}
		else
		{
			UserNameBorderColor = '#ccc';
			UserNameWarning = '';
			return validateIfUserNameExist();
		}
	}
	
	private boolean validateIfUserNameExist()
	{
		Tutor__c[] tutor = [select name, id from Tutor__c where UserName__c = :UserName];
		if (tutor.size() > 0)
			return true;
		UserNameBorderColor = 'red';
		UserNameWarning = 'משתמש לא קיים';
		PasswordBorderColor = '#ccc';
		PasswordWarning = '';
		return false;
	}
	
	public boolean validatePasswoed()
	{
		if (Password == null || Password == '')
		{
			PasswordBorderColor = 'red';
			PasswordWarning = 'אנא הקש סיסמא';
			return false;
		}
		else
		{
			PasswordBorderColor = '#ccc';
			PasswordWarning = '';
			return validateIfPasswoedExist();
		}
	}
	
	private boolean validateIfPasswoedExist()
	{
		Tutor__c[] tutor = [select name, id from Tutor__c where UserName__c = :UserName and Password__c = :Password];
		if (tutor.size() > 0)
		{
			setCookie(tutor[0]);
			tutorOnLine = tutor[0];
			return true;
		}
		PasswordBorderColor = 'red';
		PasswordWarning = 'סיסמא שגויה';
		return false;
	}
	
	private void setCookie(Tutor__c tutor)
	{
		Cookie sessionId = new Cookie('sessionId', insertSessionId(tutor), null, -1, false);
		ApexPages.currentPage().setCookies(new Cookie[] { sessionId } );
	}
	
	private string insertSessionId(Tutor__c tutor)
	{
		SessionID__c sessionId = new SessionID__c();
		sessionId.name = string.valueOf(Math.random());
		sessionId.TutorName__c = tutor.id;
		sessionId.InTime__c = datetime.now();
		sessionId.LestActivityTime__c = datetime.now();
		insert sessionId;
		return sessionId.id;
	}
}