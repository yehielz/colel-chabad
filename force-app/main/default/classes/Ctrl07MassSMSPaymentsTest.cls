@isTest(SeeAllData=true)
private class Ctrl07MassSMSPaymentsTest {

    static testMethod void myUnitTest() {
    	List<Payment__c> payments = [select Id From Payment__c limit 10];
    	ApexPages.StandardSetController records = new ApexPages.StandardSetController (payments);
    	records.setSelected(payments);
        Ctrl07MassSMSPayments ctrl = new Ctrl07MassSMSPayments (records);
        ctrl.init();
        ctrl.sendSMS();
        ctrl.getFieldsToDisplay();
        
    }
}