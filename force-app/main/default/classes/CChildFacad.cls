public with sharing class CChildFacad
{
    public String currentURL {get;set;}
    public string pageTitle { get; set; }
    public string errorMessage { get; set; }
    public String examId {get;set;}
    private CChildDb childDb;
    
    public boolean IsParent { get{ return pChild.FamilyManType__c != null && pChild.FamilyManType__c == CObjectNames.Parent; } }
    public boolean IsNotParent { get{ return !IsParent; } }
    
    public string pSchoolYearId { get { if (childDb.schoolYear != null )return childDb.schoolYear.Id;else return null; } set{childDb.schoolYear = CSchoolYearDb.GetYearById(value);} }
    public ChildRegistration__c childRegistration {get;set;}
    public List<SelectOption> pYearsOptions { get{ return childDb.pYearsOptions; } }
    public List<ActivityReport__c> pReportsList { get { return childDb.pReportsList; } }
    public List<ActivityApproval__c> pApprovalsList { get { return childDb.pApprovalsList; } }
    public List<RequestForTreatment__c> pRequestsList { get { return childDb.pRequestsList; } }
    public List<Exam__c> examList {get{ return childDb.examList;}}
    public List<ChildRegistrationInSupporter__c> childSponsors {
        get{
            if(childSponsors == null)
                childSponsors = [select Id,SupporterName__c,Name,ChildRegistrationName__r.Id,ChildRegistrationName__r.Name,ChildRegistrationName__r.SchoolYearName__c 
                                         From ChildRegistrationInSupporter__c where ChildRegistrationName__r.ChildName__c = :pChild.Id AND ChildRegistrationName__r.SchoolYearName__c  = :pSchoolYearId];

            return childSponsors;
        }
        set;}
    public Children__c pChild{
        get{
            return childDb.GetChild();
        }
    }
    
    public CTutorInChild[] pTutorsByChild{
        get { 
            return childDb.getTutorsByChildTutorsList();
        }
    }
    
    
    public void initChildRegistration(){
        List<ChildRegistration__c>  registrations;
        registrations= [select Id,Name From ChildRegistration__c where ChildName__c = :pChild.Id AND SchoolYearName__c  = :pSchoolYearId];
        if(registrations.size() > 0)
            childRegistration = registrations[0];
    }
        
    public CChildFacad(ApexPages.StandardController controller) {
        childDb = new CChildDb(controller);
        SetProperties();
        initChildRegistration();
        currentURL = '/apex/ChildView?id=' + pChild.Id + '&SchoolYearId=' + pSchoolYearId;
    }
    
    public PageReference save() {
        if (!Validation())
            return null;
        
        childDb.save();
        PageReference newChildPage = new ApexPages.StandardController(pChild).view();
        return newChildPage ;
    }
    
    private void SetProperties()
    {
        errorMessage = '';
        if (pChild.id == null)
            pageTitle = 'בן משפחה חדש';
        else
            pageTitle = pChild.name;
        
        String yearId =  Apexpages.currentPage().getParameters().get('SchoolYearId');
        if (!String.isEmpty(yearId )) pSchoolYearId = yearId ;
    }
    
    private boolean Validation()
    {
        boolean ret = true;
        CChildValidator validator = new CChildValidator(this);
        ret = validator.Validate();
        if (!ret)
            errorMessage = validator.ErrorMessage;
        
        return ret;
    }
    
    public PageReference OnBirthDate()
    {
        if (pChild.Birthdate__c != null)
            childDb.OnBirthDate();
        return null;
    }
    
    public PageReference OnNewChild()
    {
        Schema.DescribeSObjectResult pR = Children__c.sObjectType.getDescribe();
        Pagereference pPage = new ApexPages.StandardController(pChild).view();
         
        Pagereference newpage = new Pagereference ('/' + pR.getKeyPrefix() +
                                                    '/e?CF00ND00000063kVN_lkid=' + pChild.FamilyName__c + '&retURL=/' + pPage.getUrl());
        return newpage; 
    }
    public PageReference deleteExam(){
        String examId = Apexpages.currentPage().getParameters().get('examId');
        system.debug(  '\n\n\n examId ' +examId + '\n\n');
        if(examId != null && examId != ''){
            List<Exam__c> exams = [select Id From Exam__c where ID = :examId];
            if(exams.size()> 0){
                try{
                    delete exams;  
                }
                catch(Exception ex){
                    Apexpages.addMessage(new Apexpages.Message(ApexPages.Severity.ERROR,ex.getMessage()));
                    return null;
                }               
            }
        }
        return new Pagereference('/'+pChild.Id);
    }
    
    public list<CObjectsHistory> pChildHistories
    {
        get
        {
            return childDb.GetChildHistories();
        }
    }
    
    public string printUrl{ get{ return CObjectNames.getBaseUrl() + page.ProperForFullReport.getUrl() + '?childID=' + pChild.id; } }
    public list<CNoteAndAttachment> pNotesFromStuffList{ get{ return childDb.pNotesFromStuffList; } }
    public list<Payment__c> pPaymentsList{ get{ return childDb.pPaymentsList; } }
}