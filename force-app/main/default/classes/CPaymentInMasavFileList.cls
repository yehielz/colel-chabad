public with sharing class CPaymentInMasavFileList 
{
	public Payment__c pPayment { get;set; }
	public boolean pIsToSave { get;set; }
	
	public CPaymentInMasavFileList()
	{
		pPayment = new Payment__c();
		pIsToSave = true;
	}
	
	public CPaymentInMasavFileList(Payment__c mPayment)
	{
		pPayment = mPayment;
		pIsToSave = true;
	}
	
	public CPaymentInMasavFileList(boolean mIsToSave)
	{
		pPayment = new Payment__c();
		pIsToSave = mIsToSave;
	}
	
	public CPaymentInMasavFileList(Payment__c mPayment, boolean mIsToSave)
	{
		pPayment = mPayment;
		pIsToSave = mIsToSave;
	}
}