public with sharing class CWSChildInSupporterChildren 
{
	public Children__c pChild { get; set; }
	public ChildRegistration__c pChildRegistration { get; set; }
	public boolean IsEnglish { get; set; }
	public boolean IsHasReports { get; set; }
	public boolean IsBlur{ get; set; }
	public boolean IsLastName{ get; set; }
	
	public CWSChildInSupporterChildren(Children__c child, boolean m_isEnglish)
	{
		pChild = child;
		pChildRegistration = new ChildRegistration__c();
		IsEnglish = m_isEnglish;
	}
	
	public CWSChildInSupporterChildren(Children__c child, ChildRegistration__c registration, boolean m_isEnglish)
	{
		pChild = child;
		pChildRegistration = registration;
		IsEnglish = m_isEnglish;
	}
	
	public CWSChildInSupporterChildren(Children__c child, ChildRegistration__c registration, map<string, string> imgByGender, boolean m_isEnglish, boolean m_IsHasReports, boolean m_IsBlur, boolean m_IsLastName)
	{
		pChild = child;
		pChildRegistration = registration;
		pImgByGenderMap = imgByGender;
		IsEnglish = m_isEnglish;
		IsHasReports = m_IsHasReports;
		IsBlur = m_IsBlur;
		IsLastName = m_IsLastName;
	}
	
	private map<string, string> imgByGenderMap { get; set; }
	public map<string, string> pImgByGenderMap
	{ 
		get
		{
			if (imgByGenderMap == null)
			{
				imgByGenderMap = new map<string, string>();
				StaticResource[] res = [select name, id, Body, SystemModstamp, NamespacePrefix from StaticResource where name = 'GirlPicture' or name = 'BoyPicture'];
				for (StaticResource re : res)
				{
					string img = '<img src="/resource/' + re.SystemModStamp.getTime() + '/' + (re.NamespacePrefix != null && re.NamespacePrefix != ''
									 ? re.NamespacePrefix + '__' : '') + re.name + '"/>';
					imgByGenderMap.put(re.name, img);
				}
			}
			return imgByGenderMap;
		}
		set
		{
			imgByGenderMap = value;
		} 
	}
	
	public string picture
	{
		get
		{
			if (pChild.Picture__c != null && pChild.Picture__c.contains('<img'))
			{
				return pChild.Picture__c.replace('<br>', '');
			}
			else
			{
				if (pChild.Gender__c == 'נקבה' && pImgByGenderMap.containsKey('GirlPicture'))
					return pImgByGenderMap.get('GirlPicture');
				else if (pImgByGenderMap.containsKey('BoyPicture'))
					return pImgByGenderMap.get('BoyPicture');
				return '';
			}
		}
	}
	
	public string Age
	{
		get
		{
			date toDay = datetime.now().date();
			date birthDate = pChild.BirthDate__c;
			integer ret = toDay.year() - birthDate.year();
			if (date.newinstance(birthDate.year(), toDay.month(), toDay.day()) <= birthDate)
				ret++;
			ret = ret < 0 ? 0 : ret;
			return string.valueOf(ret);
		}
	}
	
	public string ChildName
    { 
    	get
    	{ 
    		string lastName = pChild.FamilyName__r.name != null ? pChild.FamilyName__r.name : '';
    		if (IsEnglish && pChild.EnglishFamilyName__c != null)
    			lastName = pChild.EnglishFamilyName__c;
    		string firstName = pChild.FirstName__c != null ? pChild.FirstName__c : '';
    		if (IsEnglish && pChild.EnglishFirstName__c != null)
    			firstName = pChild.EnglishFirstName__c;
    		if (IsBlur && lastName.length() > 1)
    			lastName = lastName.substring(0,1) + '.';
    		return firstName + (IsLastName ? ' ' + lastName : ''); 
    	} 
    } 
}