public with sharing class CNoteInCaseDb 
{
	public static boolean IsToKeepAssigneeUser { get; set; }
	public Case__c pCase { get; private set; }
	public Note pNote { get; private set; }
	
	public CNoteInCaseDb()
	{
		SetpCase();
		SetpNote();
	}
	
	private void SetpCase()
	{
		if (Apexpages.currentPage().getParameters().get('caseId') != null && Apexpages.currentPage().getParameters().get('caseId') != '')
			pCase = CCaseDb.GetCaseById(Apexpages.currentPage().getParameters().get('caseId'));
		if (pCase == null)
			pCase = new Case__c();
		pCase.AssigneeUser__c = pCase.PreviousAssigneeUser__c != null ? pCase.PreviousAssigneeUser__c : pCase.AssigneeUser__c;
	}
	
	private void SetpNote()
	{
		pNote = new Note();
	}
	
	public void save()
	{
		if (pCase.id != null)
			update pCase;
		if (pNote.id == null)
		{
			pNote.ParentId = pCase.id;
			CNoteInCaseDb.IsToKeepAssigneeUser = true;
			insert pNote;
		}
	}
	
	public list<selectOption> GetUsersOptions()
	{
		list<selectOption> ret = new list<selectOption>();
		User[] users = [select name, id from User];
		for (User us : users)
		{
			ret.add(new selectOption(us.id, us.name));
		}
		return ret;
	}
}