<apex:page Title="בן משפחה: {!pageTitle}" standardController="Children__c" extensions="CChildFacad">
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
    <style> 
        .mybtn
        {
            margin: 0 3px;
            text-decoration: none;
            padding: 4px 3px !important;
        }
        .mybtn:hover
        {
            text-decoration: none;
        }
        .tutors
        {
            font-size: 1.2em; 
            color: black; 
            font-weight: bold; 
            margin: 0 2% 0 0;
            display: block; 
            padding: 0 0 8px 0;
        }
        
        .photo
        {
            width: 15%;
            float: right;
        }
        
        .photo img
        {
            width: 97% !important;
            -moz-box-shadow: 0 0 5px 2px #888;
            -webkit-box-shadow: 0 0 5px 2px #888;
            box-shadow: 0 0 5px 2px #888;
            height: auto !important;
        }

        .details
        {
            width: 83%;
            float: right;
        }
        
        .sffont
        {
            font-size: 91%; 
            font-weight: bold;
            color: #4A4A56 !Important;
        }
        
        .mybuttons
        {
            text-align: center;
        }
        
        .mybuttons input
        {
            margin: 3px;
        }
        
        .ListsHeader
        {
            font-size: 10pt;
        }
        
        .ListsNumber
        {
            font-weight: normal;
        }
            
        .editLink
        {
            color: #015BA7 !Important;
            text-decoration: none;
        }    
    </style>
    <apex:outputPanel id="allInfos">
    <script type="text/javascript">
        $(document).ready(function() {
            $('.toclose img').click();
            SetListsNumber();
        });
        
        function SetListsNumber()
        {
            $('.AllRelatedList').each(function() {
                var $this = $(this);
                var rowsNum = $this.find('.RelatedList .pbBody').find('table').find('tr.dataRow').length; 
                var fullNumber = $this.find('.RelatedList .pbBody').find('.pShowMore').find('a:contains("עבור לרשימה")').text().replace(/\D/g, '');
                var num = rowsNum;
                if (fullNumber != null && fullNumber != '')
                    num = Number(fullNumber);
                $this.find('.ListsNumber').text('(' + num.toString() + ')');
            });
        }
        
        function deleteItem(e)
        {
            if (!window.confirm("האם אתה בטוח שברצונך למחוק פריט זה?"))
            {
                e.preventDefault();
                event.stopPropagation();
            }
        }
        
        function changeYear(obj){
            window.location.href ="/apex/ChildView?id={!pChild.Id}&SchoolYearId=" + obj.value;
        }
        
        // onclick="deleteItem(event);"
    </script>
    
    <apex:form >
        
        <apex:sectionHeader subtitle="{!pageTitle}" title="בן משפחה"/>
        
        <chatter:feedWithFollowers entityId="{!pChild.Id}"/>
        <div class="mybuttons">
            <apex:commandButton action="{!edit}" value="עריכה" rendered="{!IsNotParent}"/>
            <apex:commandButton onclick="deleteItem(event);" action="{!delete}" value="מחק"/>
            <apex:commandButton action="{!OnNewChild}" value="רישום בן משפחה חדש" rendered="{!IsNotParent}"/>
            <a target="_blank" class="btn mybtn" href="{!printUrl}">הדפס בן משפחה </a> 
        </div>
        <apex:pageMessages />
        <div class="photo">
            <apex:outputField value="{!pChild.Picture__c}"/>
        </div>
        <div class="details">
            <apex:pageBlock title="" mode="maindetail">
                <apex:pageBlockSection title="פרטים אישיים" columns="2" collapsible="true">
                    <apex:outputField value="{!pChild.FamilyName__c}"/>
                    <apex:outputField value="{!pChild.EnglishFamilyName__c}"/>
                    <apex:outputField value="{!pChild.FirstName__c}"/>
                    <apex:outputField value="{!pChild.EnglishFirstName__c}"/>
                    <apex:outputField value="{!pChild.Birthdate__c}"/>
                    <apex:outputField value="{!pChild.IdNumber__c}"/>
                    <apex:outputPanel >
                        <table style="width: 100%;">
                            <tr>
                                <td class="sffont" style="width: 35.4%; padding-left: 9px; text-align: left;">
                                    תאריך לידה עברי
                                </td>
                                <td style="padding-right: 9px; text-align: right;">
                                    <apex:outputField value="{!pChild.HebrewBirthDay__c}"/>&nbsp;
                                    <apex:outputField value="{!pChild.HebrewBirthMonth__c}"/>&nbsp;
                                    <apex:outputField value="{!pChild.HebrewBirthYear__c}"/>
                                </td>
                            </tr>
                        </table>
                    </apex:outputPanel>
                    <apex:outputField value="{!pChild.OwnerId}"/>
                    <apex:outputField value="{!pChild.Gender__c}"/>
                    <apex:outputField value="{!pChild.MobilePhone__c}"/>
                    <apex:outputField value="{!pChild.FamilyManType__c}"/>
                    <apex:outputField value="{!pChild.City__c}"/>
                    <apex:outputField value="{!pChild.Grade__c}"/>
                    <apex:outputField value="{!pChild.IsThereAnAnnualRegistration__c}"/>
                    <apex:outputField value="{!pChild.SchoolName__c}"/>
                    <apex:outputField value="{!pChild.ReasonAnnualRegistration__c}" rendered="{!!pChild.IsThereAnAnnualRegistration__c}"/> 
                    <apex:outputField value="{!pChild.Age__c}"/>
                    <apex:outputField value="{!pChild.ZipCode__c}"/>
                    <apex:outputField value="{!pChild.RegistrationAllowed__c}"/>
                </apex:pageBlockSection>
                <apex:pageBlockSection title="פרטים כלליים" columns="1" collapsible="true">
                    <apex:outputField value="{!pChild.NoteToPrintPage__c}"/>
                    <apex:outputField value="{!pChild.Comments__c}"/>
                </apex:pageBlockSection>
                
                <apex:pageBlockSection title="שנת פעילות" columns="1" collapsible="true">
                    <apex:selectList value="{!pSchoolYearId}" size="1" onchange="changeYear(this)" >
                            <apex:selectOptions value="{!pYearsOptions}"/>
                        </apex:selectList>
                </apex:pageBlockSection>
            </apex:pageBlock> 
        </div> 
       
    </apex:form>
    
    <apex:pageBlock mode="maindetail">
        
        <span class="AllRelatedList">
            <apex:pageBlockSection title="" columns="1" collapsible="true">
                <apex:facet name="header">
                    <span class="ListsHeader">
                        התקדמויות הילד
                        <span class="ListsNumber"></span>
                    </span>
                </apex:facet>
                <apex:outputPanel styleclass="RelatedList">
                    <apex:relatedList list="ChildProgressList__r" title="" />
                </apex:outputPanel>
            </apex:pageBlockSection>
        </span>
        <apex:outPutPanel styleclass="toclose">
        
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            רישומים שנתיים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="ChildRegistrationList__r" title="" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            עדכונים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="CaseList__r" title=""/>
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            
            <!--  
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            בקשות לפעילות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="RequestForTreatmentList__r" title="" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            אישורי פעילות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="ActivityApprovalList__r" title="" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            דיווחי פעילות 
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="ActivityReportList__r" title="" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            -->
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            בקשות לפעילות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="note" title="">
                               <apex:pageBlockTable value="{!pRequestsList}" var="request">
                                    <apex:column headerValue="פעולה" style="width: 1%;">
                                        <a class="editLink" href="/{!request.id}/e">עריכה</a>
                                    </apex:column>
                                    <apex:column headerValue="שם">
                                        <a href="/{!request.id}">{!request.name}</a>
                                    </apex:column>
                                    <apex:column headerValue="תאריך יצירה">
                                        <apex:outputField value="{!request.CreatedDate}"/>
                                    </apex:column>
                                    <apex:column value="{!request.ProfessionName__c}"/>
                                    <apex:column headerValue="חונך">
                                        <a href="/{!request.TutorName__c}">{!request.TutorName__r.name}</a>
                                    </apex:column>
                                    <apex:column value="{!request.FromDate__c}"/>
                                    <apex:column value="{!request.ToDate__c}"/>
                                    <apex:column headerValue="תעריף נסיעה">
                                        <apex:outputField value="{!request.Fare__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="עדכון">
                                        <apex:outputField value="{!request.CaseName__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="נוצר ע״י">
                                        <apex:outputField value="{!request.CreatedByID}"/>
                                    </apex:column>
                                    <apex:column headerValue="מצב הבקשה">
                                        <apex:outputField value="{!request.RequestStatus__c}"/>
                                    </apex:column>
                                </apex:pageBlockTable>
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel> 
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            אישורי פעילות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="note" title="">
                               <apex:pageBlockTable value="{!pApprovalsList}" var="approval">
                                    <apex:column headerValue="פעולה" style="width: 1%;">
                                        <a class="editLink" href="/{!approval.id}/e">עריכה</a>
                                    </apex:column>
                                    <apex:column headerValue="שם">
                                        <a href="/{!approval.id}">{!approval.name}</a>
                                    </apex:column>
                                    <apex:column headerValue="מקצוע">
                                        <apex:outputField value="{!approval.ProfessionName__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="חונך">
                                        <a href="/{!approval.TutorName__c}">{!approval.TutorName__r.name}</a>
                                    </apex:column>
                                    <apex:column headerValue="עד תאריך">
                                        <apex:outputField value="{!approval.ToDate__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="סכום שאושר">
                                        <apex:outputField value="{!approval.AmountApproved__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="תעריף לשעת חונך">
                                        <apex:outputField value="{!approval.Rate__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="תעריף נסיעה שאושר">
                                        <apex:outputField value="{!approval.Fare__c}"/>
                                    </apex:column>
                                    <apex:column value="{!approval.WeeklyHours__c}"/>
                                    <apex:column value="{!approval.NoteToPrintPage__c}"/>
                                    <apex:column value="{!approval.ApprovalDate__c}"/>
                                </apex:pageBlockTable>
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel> 
                </apex:pageBlockSection>
            </span>
            
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            דיווחי פעילות 
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="note" title="">
                               <apex:pageBlockTable value="{!pReportsList}" var="report">
                                    <apex:column headerValue="פעולה" style="width: 1%;">
                                        <a class="editLink" href="/{!report.id}/e">עריכה</a>
                                    </apex:column>
                                    <apex:column headerValue="שם">
                                        <a href="/{!report.id}">{!report.name}</a>
                                    </apex:column>
                                    <apex:column headerValue="מקצוע">
                                        <apex:outputField value="{!report.ProfessionName__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="חונך">
                                        <a href="/{!report.TutorName__c}">{!report.TutorName__r.name}</a>
                                    </apex:column>
                                    <apex:column value="{!report.ReportMonth__c}"/>
                                    <apex:column value="{!report.ActivityReportStatus__c}"/>
                                    <apex:column value="{!report.TotalPayment__c}"/>
                                    <apex:column value="{!report.TotalPaid__c}"/>
                                    <apex:column value="{!report.CreatedDate}"/>
                                </apex:pageBlockTable>
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel> 
                </apex:pageBlockSection>
            </span>
            
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            פעילויות מיוחדות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList" rendered="{!!CONTAINS($User.ProfileId , '00eD0000001QtcS') }">
                        <apex:relatedList list="SpecialActivitiesLineList__r" title="" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            תשלומים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="note" title="">
                               <apex:pageBlockTable value="{!pPaymentsList}" var="payment">
                                    <apex:column headerValue="פעולה" style="width: 1%;">
                                        <a class="editLink" href="/{!payment.id}/e">עריכה</a>
                                    </apex:column>
                                    <apex:column headerValue="שם">
                                        <a href="/{!payment.id}">{!payment.name}</a>
                                    </apex:column>
                                    <apex:column headerValue="מהות התשלום">
                                        <apex:outputField value="{!payment.NatureOfPayment__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="חונך">
                                        <a href="/{!payment.TutorName__c}">{!payment.TutorName__r.name}</a>
                                    </apex:column>
                                    <apex:column headerValue="תאריך">
                                        <apex:outputField value="{!payment.Date__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="חוב">
                                        <apex:outputField value="{!payment.Debt__c}"/>
                                    </apex:column>
                                    <apex:column headerValue="סכום">
                                        <apex:outputField value="{!payment.Amount__c}"/>
                                    </apex:column>
                                </apex:pageBlockTable>
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel>        
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            מבחנים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="exams" title="">
                                <apex:pageBlockbuttons location="top">
                                    <a href="/{!$ObjectType.Exam__c.KeyPrefix}/e?retURL=/apex/ChildView?id={!pChild.Id}&childId={!pChild.Id}" style="text-decoration: none;">
                                        <input type="button" class="btn" value="מבחן חדש"/>
                                    </a>
                                </apex:pageBlockbuttons>
                                <apex:pageBlockTable value="{!examList}" var="exam" rendered="{!examList.size > 0}">
                                    <apex:column headerValue="פעולה" styleClass="actionColumn">
                                        <a href="/{!exam.Id}/e?retURL=/apex/ChildView?id={!pChild.Id}" class="actionLink" >Edit</a>
                                            &nbsp;|&nbsp;
                                        <a href="javascript:void(0);" class="actionLink" onclick="if(confirmDelete()){deleteExam('{!exam.Id}')};">Del</a>
                                    </apex:column>
                                    <apex:column headerValue="{!$ObjectType.Exam__c.Fields.Name.label}">
                                        <apex:outputLink value="/{!exam.Id}" >{!exam.Name}</apex:outputLink>
                                    </apex:column>
                                    <apex:column value="{!exam.ProfessionName__c}"/>
                                    <apex:column value="{!exam.ExamType__c}"/>
                                    <apex:column value="{!exam.Grade__c}"/>
                                    <apex:column value="{!exam.GradeInWords__c}"/>
                                    <apex:column value="{!exam.Date__c}"/>
                                    <apex:column value="{!exam.ChildRegistration__c}"/>
                                </apex:pageBlockTable>
                                <apex:outputPanel rendered="{!examList.size == 0}">
                                    אין נתונים להצגה
                                </apex:outputPanel>
                            </apex:pageBlock>  
                        </div>
                        <apex:form >
                            <apex:actionFunction name="deleteExam" action="{!deleteExam}" rerender="allInfos" oncomplete="$('body').animate({scrollTop:0}, '500');">
                                <apex:param name="examId"  value=""/>
                            </apex:actionFunction>
                        </apex:form> 
<!--                        <apex:relatedList list="TestList__r" title=""/> -->
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            תעודות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="DiplomaList__r" title="" />
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            חונכים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock >
                                <apex:outputText styleClass="tutors" value=""/>
                                <apex:pageBlockTable value="{!pTutorsByChild}" var="line">
                                    <apex:column >
                                    <apex:facet name="header">שם חונך</apex:facet>
                                    <a href="{!line.tutorLink}">{!line.pTutor.name}</a>
                                    </apex:column>
                                </apex:pageBlockTable> 
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection title="" columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            היסטוריית בן משפחה
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="history" title="">
                               <apex:pageBlockTable value="{!pChildHistories}" var="history">
                                    <apex:column headerValue="תאריך השינוי" >
                                        <apex:outputText value="{!history.pCreatedDate}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:facet name="header">שונה</apex:facet>
                                        <apex:outputText value="{!history.pFieldLabel}" />
                                    </apex:column>
                                    <apex:column headerValue="מ - ">
                                        <apex:outputText value="{!history.pOldValue}" />
                                    </apex:column>
                                    <apex:column headerValue="ל - ">
                                        <apex:outputText value="{!history.pNewValue}" />
                                    </apex:column>
                                    <apex:column headerValue="על ידי"   value="{!history.pCreatedByName}"/>
                                </apex:pageBlockTable>
                            </apex:pageBlock>  
                        </div>
                    </apex:outputPanel>      
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            הערות וקבצים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="NotesAndAttachments" title=""/>
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            השתתפות במלגות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
<!--                     <apex:outputPanel styleclass="RelatedList" rendered="{!!CONTAINS($User.ProfileId , '00eD0000001QtcS') }"> -->
                    <apex:outputPanel styleclass="RelatedList">
                        <apex:relatedList list="ChildGrants__r" title="מלגות" rendered="{!$ObjectType.ChildInGrant__c.accessible}"/>
                    </apex:outputPanel>
                </apex:pageBlockSection>
            </span>
        
            
            <span class="AllRelatedList">
                <apex:pageBlockSection columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            הערות וקבצים מעדכונים דיווחים ואישורי פעילות
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="note" title="">
                                <apex:pageBlockTable value="{!pNotesFromStuffList}" var="note">
                                    <apex:column headerValue="פעולה" style="width: 1%;">
                                        <a class="editLink" href="/{!note.FileId}/e">עריכה</a>
                                    </apex:column>
                                    <apex:column headerValue="סוג" >
                                        <apex:outputText value="{!note.FileType}" escape="false"/>
                                    </apex:column>
                                    <apex:column headerValue="כותרת" >
                                        <a href="/{!note.FileId}">{!note.Title}</a>
                                    </apex:column>
                                    <apex:column headerValue="השתנה לאחרונה" >
                                        <apex:outputText value="{!note.LastModifiedDate}" escape="false"/>
                                    </apex:column>
                                    <apex:column headerValue="נוצר על-ידי" >
                                        <apex:outputText value="{!note.OwnerId}" escape="false"/>
                                    </apex:column>
                                    <apex:column headerValue="גוף" >
                                        <apex:outputText escape="false" value="{!note.Body}"/>
                                    </apex:column>
                                </apex:pageBlockTable>
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel>        
                </apex:pageBlockSection>
            </span>
            
            <span class="AllRelatedList">
                <apex:pageBlockSection columns="1" collapsible="true">
                    <apex:facet name="header">
                        <span class="ListsHeader">
                            רישומים שנתיים בתומכים
                            <span class="ListsNumber"></span>
                        </span>
                    </apex:facet>
                    <apex:outputPanel styleclass="RelatedList">
                        <div class="pbBody">
                            <apex:pageBlock id="childSponsors" title="">
                                <apex:pageBlockbuttons location="top" rendered="{!NOT(ISBLANK(childRegistration))}">
                                    <a href="/{!$ObjectType.ChildRegistrationInSupporter__c.KeyPrefix}/e?CF00ND00000063kSl={!childRegistration.Name}&CF00ND00000063kSl_lkid={!childRegistration.Id}&saveURL={!currentURL}&retURL={!currentURL}" style="text-decoration: none;">
                                        <input type="button" class="btn" value="תומך חדש"/>
                                    </a>
                                </apex:pageBlockbuttons>
                                <apex:pageBlockTable value="{!childSponsors}" var="cs">
                                    <apex:column headerValue="תומך" >
                                        <apex:outputField value="{!cs.SupporterName__c}"/>
                                    </apex:column>
                                    <apex:column >
                                        <apex:outputField value="{!cs.ChildRegistrationName__r.SchoolYearName__c }"/>
                                    </apex:column>
                                </apex:pageBlockTable>
                            </apex:pageBlock>
                        </div>
                    </apex:outputPanel>        
                </apex:pageBlockSection>
            </span>
            
        </apex:outPutPanel>
    </apex:pageBlock>
    </apex:outputPanel>
    <apex:form styleclass="mybuttons">
        <apex:commandButton action="{!edit}" value="עריכה" rendered="{!IsNotParent}"/>
        <apex:commandButton onclick="deleteItem(event);" action="{!delete}" value="מחק"/>
        <apex:commandButton action="{!OnNewChild}" value="רישום בן משפחה חדש" rendered="{!IsNotParent}"/>
    </apex:form>
</apex:page>