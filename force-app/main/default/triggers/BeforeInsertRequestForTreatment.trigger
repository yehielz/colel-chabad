trigger BeforeInsertRequestForTreatment on RequestForTreatment__c (before insert) 
{
	if (!PAD.canTrigger('All Triggers'))
        return;
    for (RequestForTreatment__c req : trigger.new)
    {
        if (req.SchoolYear__c == null)
            req.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
    }
}