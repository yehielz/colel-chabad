trigger AfterInsertAndUpdateSpecialActivityLine on SpecialActivitiesLine__c (after insert, after update) {
	 
	if (CSpecialActivitiesLineDb.IsDontUpdatePaymentInTrigger != true && PAD.canTrigger('All Triggers')){
		CPaymentDb paymentDb = new CPaymentDb();
		Set<String> paymentsIdsSet = new Set<String>();
		for (SpecialActivitiesLine__c line : trigger.new){
			if ( line.PaymentName__c != null)
				paymentsIdsSet.add(line.PaymentName__c);
			if (trigger.old != null && trigger.oldMap.get(line.Id).PaymentName__c != null)
				paymentsIdsSet.add(trigger.oldMap.get(line.Id).PaymentName__c);
		}
		
		paymentDb.SetAllPaymentsMap(paymentsIdsSet);
		paymentDb.SetDebtByPaymentsMap(paymentsIdsSet);
		paymentDb.UpdatePaymentDebt(paymentsIdsSet);
	}
	else
		CSpecialActivitiesLineDb.IsDontUpdatePaymentInTrigger = false;
		
}