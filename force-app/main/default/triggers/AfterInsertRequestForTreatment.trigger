trigger AfterInsertRequestForTreatment on RequestForTreatment__c (after insert) 
{ 
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
		
	map<string, string> childrenIds = new map<string, string>();
	for (RequestForTreatment__c request : trigger.new)
	{
		childrenIds.put(request.ChildName__c, request.ChildName__c);
	}
	Children__c[] children = [select name, id, NumberOfRequestsAndReports__c from Children__c where id in :childrenIds.values()];
	date startDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year() - 1, 9, 1) : date.newInstance(datetime.now().year(), 9, 1);
	date endDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year(), 9, 1) : date.newInstance(datetime.now().year() + 1, 9, 1);
	AggregateResult[] reportsCounts = [select count(id) num, ChildName__c child from ActivityReport__c where ChildName__c in :childrenIds.keySet()
									   and ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c >= :startDate and 
									   ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c < :endDate group by ChildName__c];
	AggregateResult[] requestsCounts = [select count(id) num, ChildName__c child from RequestForTreatment__c where 
										ChildName__c in :childrenIds.keySet() and FromDate__c >= :startDate and FromDate__c < :endDate 
										group by ChildName__c];
	map<string, integer> sizesByChild = new map<string, integer>();
	for (AggregateResult item : reportsCounts)
	{
		string childName = string.valueOf(item.get('child'));
		integer num = integer.valueOf(item.get('num'));
		sizesByChild.put(childName, num);
	}	
	for (AggregateResult item : requestsCounts)
	{
		string childName = string.valueOf(item.get('child'));
		integer num = integer.valueOf(item.get('num'));
		if (sizesByChild.containsKey(childName))
			num += sizesByChild.get(childName);
		sizesByChild.put(childName, num);
	}
	list<Children__c> childrenToUpdate = new list<Children__c>();
	for (Children__c child : children)	{
		child.IsThereCase__c = true; //this field refer either to Case or Request for treatment
		child.NumberOfRequestsAndReports__c = sizesByChild.containsKey(child.id) ? sizesByChild.get(child.id) : 0;
		childrenToUpdate.add(child);
		
	}
	if (childrenToUpdate.size() > 0){
		//CChildDb.IsAfterUpdateTutorsOrActive = true;
		update childrenToUpdate;
	}
}