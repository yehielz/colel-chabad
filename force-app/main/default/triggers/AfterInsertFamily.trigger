trigger AfterInsertFamily on Family__c (after insert) 
{
    if( PAD.canTrigger('AfterInsertFamily') && !Test.isRunningTest()){
        
	    List<Children__c> parentsToUpsert = new List<Children__c>();
	    CChildDb childDb = new CChildDb();
	    childDb.SetParentsByFamiliesMap();
	    for (Family__c family : trigger.new){
	         parentsToUpsert.add( childDb.UpdateOrInsertParents2(family));
	    }
	    
	    if (parentsToUpsert.size() > 0)
	        upsert parentsToUpsert;
    }
}