trigger AftertInserUpdateAndDeleteMasavFile on MasavFile__c (after delete, after insert, after update) 
{
	if (CCreateFileFacad.IsAfterUpdateFields != true && PAD.canTrigger('All Triggers'))
	{
		if (Trigger.isDelete)
		{
			list<Payment__c> payments = [select name, id, MasavFile__c, Amount__c, AmountPaidByMasav__c from Payment__c where MasavFile__c = null
										 and AmountPaidByMasav__c != null and AmountPaidByMasav__c > 0];
			for (Payment__c payment : payments)
			{
				payment.MasavFile__c = null;
				payment.Amount__c = payment.Amount__c == null ? 0 : payment.Amount__c;
				payment.AmountPaidByMasav__c = payment.AmountPaidByMasav__c == null ? 0 : payment.AmountPaidByMasav__c;
				payment.Amount__c -= payment.AmountPaidByMasav__c;
				payment.AmountPaidByMasav__c = 0;
			}
			if (payments.size() > 0)
				update payments;
		}
		else
		{
			Attachment[] attachments = [select id from Attachment where ParentId = :trigger.new];
			if (attachments.size() > 0)
				delete attachments;
			for (MasavFile__c file : trigger.new)
			{
				try
				{
					CCreateFileFacad createFileFacad = new CCreateFileFacad(file.id);
					Attachment attach = new Attachment();
				    attach.Name = String.valueOf(Date.Today()) + '.txt';
				    attach.Body = Blob.valueOf(createFileFacad.SetFile());
				    attach.ContentType = 'application/octet-stream .txt';
				    attach.ParentID = file.id;
				    insert attach;
				}
				catch(Exception e)
				{
					file.addError(e.getMessage());
				}
			}
		}
	}
	else
		CCreateFileFacad.IsAfterUpdateFields = false;
}