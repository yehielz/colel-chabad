trigger AfterInsertActivityReport on ActivityReport__c (after insert) 
{ 
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	CPaymentDb paymentDb = new CPaymentDb();
	Set<String> paymentsIdsSet = new Set<String>();
	//CActivityReportDb activityReportDb = new CActivityReportDb();
	
	//*************************************************************************************************
	// update the field of LastReportActivity and the field of TotalHoursSpent in the Activity Approval
	//*************************************************************************************************
	Set<String> activityApprovalsToRun = new Set<String>();
	map<string, datetime> lastReportActivityByActivityApprovalsMap = new map<string, datetime>();
	for (ActivityReport__c report : trigger.new)
	{
		if (report.PaymentName__c != null)
			paymentsIdsSet.add(report.PaymentName__c);
		
		activityApprovalsToRun.add(report.ActivityApprovalName__c);
		if (lastReportActivityByActivityApprovalsMap.containsKey(report.ActivityApprovalName__c) && 
			report.CreatedDate > lastReportActivityByActivityApprovalsMap.get(report.ActivityApprovalName__c))
			lastReportActivityByActivityApprovalsMap.put(report.ActivityApprovalName__c, report.CreatedDate);
		else
			lastReportActivityByActivityApprovalsMap.put(report.ActivityApprovalName__c, report.CreatedDate);
	}
	
	//אם יוצרים דיווח שיש בו תשלום אז צריך להוסיף את סכום הדייווח לתשלום
	//במקרה זה מחשבים מחדש את כל סכום התשלום 
	if (paymentsIdsSet.size() > 0)
	{
		paymentDb.SetAllPaymentsMap(paymentsIdsSet);
		paymentDb.SetDebtByPaymentsMap(paymentsIdsSet);
		paymentDb.UpdatePaymentDebt(paymentsIdsSet);
	}
		
	CActivityApprovalDb activityApprovalDb = new CActivityApprovalDb(true);
	activityApprovalDb.SetActivityApprovalMap(activityApprovalsToRun);
	activityApprovalDb.SetTotalHoursAndAmountSpentByActivityApprovalMap(activityApprovalsToRun);
	activityApprovalDb.SetTotalMeetingsByActivityApprovalMap(activityApprovalsToRun);
	activityApprovalDb.SetLastReportDatesByApproval(activityApprovalsToRun);
	list<ActivityApproval__c> activityApprovalsToUpdate = new list<ActivityApproval__c>();
	for (string str : activityApprovalsToRun)
	{
		ActivityApproval__c approval = activityApprovalDb.UpdateActivityApprovalAfterReport(str);
		if (approval != null)
			activityApprovalsToUpdate.add(approval);
		if (activityApprovalsToUpdate.size() > 90)
		{
			CActivityApprovalDb.IsAfterChangeCoordinator = true;
			update activityApprovalsToUpdate;
			activityApprovalsToUpdate = new list<ActivityApproval__c>();
		}
	}
	if (activityApprovalsToUpdate.size() > 0)
	{
		CActivityApprovalDb.IsAfterChangeCoordinator = true;
		update activityApprovalsToUpdate;
	}
	
	//**************************************************
	// update the field of LastActivityDate in the Child
	//**************************************************
	map<string, Children__c> childrenMap = CChildDb.GetChildrenMap();
	map<string, Children__c> childrenMapToUpdate = new map<string, Children__c>();
	date startDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year() - 1, 9, 1) : date.newInstance(datetime.now().year(), 9, 1);
	date endDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year(), 9, 1) : date.newInstance(datetime.now().year() + 1, 9, 1);
	AggregateResult[] reportsCounts = [select count(id) num, ChildName__c child from ActivityReport__c where ChildName__c in :childrenMap.keySet()
									   and ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c >= :startDate and 
									   ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c < :endDate group by ChildName__c];
	AggregateResult[] requestsCounts = [select count(id) num, ChildName__c child from RequestForTreatment__c where 
										ChildName__c in :childrenMap.keySet() and FromDate__c >= :startDate and FromDate__c < :endDate 
										group by ChildName__c];
	map<string, integer> sizesByChild = new map<string, integer>();
	for (AggregateResult item : reportsCounts)
	{
		string childName = string.valueOf(item.get('child'));
		integer num = integer.valueOf(item.get('num'));
		sizesByChild.put(childName, num);
	}	
	for (AggregateResult item : requestsCounts)
	{
		string childName = string.valueOf(item.get('child'));
		integer num = integer.valueOf(item.get('num'));
		if (sizesByChild.containsKey(childName))
			num += sizesByChild.get(childName);
		sizesByChild.put(childName, num);
	}									
	for (ActivityReport__c ar : trigger.new)
	{
		if (childrenMap.containsKey(ar.ChildName__c))
		{
			Children__c child = childrenMap.get(ar.ChildName__c);
			child.LastActivityDate__c = ar.CreatedDate;
			child.NumberOfRequestsAndReports__c = sizesByChild.containsKey(child.id) ? sizesByChild.get(child.id) : 0;
			childrenMapToUpdate.put(child.id, child);
		}
	}
	list<Children__c> childrenToUpdate = childrenMapToUpdate.values();
	if (childrenToUpdate.size() > 0)
	{
		CChildDb.IsAfterUpdateTutorsOrActive = true;
		update childrenToUpdate;
	}
}