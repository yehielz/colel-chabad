trigger AfterUpdateProfession on Profession__c (after update) 
{    
    RequestForTreatment__c[] requests = [select name, id, ProfessionTypeName__c, ProfessionName__c, ProfessionName__r.ProfessionTypeName__c from RequestForTreatment__c where ProfessionName__c in :trigger.new];
    list<RequestForTreatment__c> requestsToUpdate = new list<RequestForTreatment__c>();
    for (RequestForTreatment__c request : requests)
    {
        request.ProfessionTypeName__c = request.ProfessionName__r.ProfessionTypeName__c;
        requestsToUpdate.add(request);
        if (requestsToUpdate.size() > 100)
        {
            CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
            update requestsToUpdate;
            requestsToUpdate = new list<RequestForTreatment__c>();
        }
    }    
    if (requestsToUpdate.size() > 0)
    {
        CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
        update requestsToUpdate;
    }
}