trigger SpecialActivitiesLineAfterDelete on SpecialActivitiesLine__c (after delete) {
	
	//update payments when special activity line is deleted.
	if (CSpecialActivitiesLineDb.IsDontUpdatePaymentInTrigger != true){
		CPaymentDb paymentDb = new CPaymentDb();
		Set<String> paymentsIdsSet = new Set<String>();
		for (SpecialActivitiesLine__c line : trigger.old){
			if ( line.PaymentName__c != null)
				paymentsIdsSet.add(line.PaymentName__c);
		}
		
		paymentDb.SetAllPaymentsMap(paymentsIdsSet);
		paymentDb.SetDebtByPaymentsMap(paymentsIdsSet);
		paymentDb.UpdatePaymentDebt(paymentsIdsSet);
	}
	else
		CSpecialActivitiesLineDb.IsDontUpdatePaymentInTrigger = false;
}