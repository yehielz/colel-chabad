trigger AfterInsertChildRegistration on ChildRegistration__c (after insert)
{ 
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	
	/*  OLD VERSION
	CFamilyDb familyDb = new CFamilyDb();
	CChildDb childDb = new CChildDb(1);
	childDb.SetActiveChildRegistrationByChildren();
	list<Children__c> listToUpdate = new list<Children__c>();
	for (integer i = 0; i < trigger.new.size(); i++)
	{
		childDb.UpdateChildIsActive(trigger.new[i].ChildName__c);
		familyDb.IncreaseNumberOfActivtyChildren(trigger.new[i]);
		Children__c child = childDb.UpdateGradeAndSchool(trigger.new[i].ChildName__c);
		if (child != null)
			listToUpdate.add(child);
	}
	if (listToUpdate.size() > 0)
		update listToUpdate;
		
	*/
	
	//NEW VERSION
	//CFamilyDb.IncreaseNumberOfActivtyChildren(trigger.new);
	CChildDb.UpdateGradeAndSchoolAndAnnualRegistration(trigger.new);
}