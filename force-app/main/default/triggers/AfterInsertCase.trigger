trigger AfterInsertCase on Case__c (after insert) 
{ 
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	
	CChildDb.UpdateIsThereCase(trigger.new);
	
	
	/*CCaseDb caseDb = new CCaseDb(true);
	for (integer i = 0; i < trigger.new.size(); i++)
	{
		caseDb.createMessageFromCase(trigger.new[i], null);
	}*/
}