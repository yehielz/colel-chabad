trigger AfterUpdateFamily on Family__c (after update) 
{
    if( PAD.canTrigger('AfterUpdateFamily') && !Test.isRunningTest() ){
       
	    if (CFamilyDb.IsAfterNumberOfChildren != true){
	        List<Children__c> parentsToUpsert = new List<Children__c>();
	        List<Case__c> casesToUpdate = new List<Case__c>();
	        CChildDb childDb = new CChildDb();
	        childDb.SetParentsByFamiliesMap();
	        CCaseDb caseDb = new CCaseDb();
	        set<string> familiesIds = new set<string>();
	        for (Family__c family : trigger.new)
	        {
	            familiesIds.add(family.id);
	        }
	        caseDb.SetCasesByFamilysMap(familiesIds);
	        list<Children__c> childrenToUpdate = new list<Children__c>();
	        for (Family__c family : trigger.new)
	        {
	        	Family__c oldFamily = trigger.oldMap.get(family.Id);
	        	//update childs and other things if coordinator was changed
	            if(/*family.CoordinatorUserName__c != oldFamily.CoordinatorUserName__c
	            	|| family.CoordinatorName__c != oldFamily.CoordinatorName__c 
	            	||*/ family.OwnerId != oldFamily.OwnerId ){
		            list<Children__c> children = childDb.UpdateChildrenAfterUpdateFamily(family);
		            if (children != null)
		                childrenToUpdate.addAll(children);
		            /*if (childrenToUpdate.size() > 90)
		            {
		                update childrenToUpdate;
		                childrenToUpdate = new list<Children__c>();
		            }*/
		            casesToUpdate.addAll(caseDb.UpdateCasesAfterUpdateFamily(family.id, family.CoordinatorUserName__c,family.OwnerId));
		        }
		        
		        //update parent info if they changed
		        if( family.ParentName__c != oldFamily.ParentName__c
	            	|| family.ParentBirthdate__c != oldFamily.ParentBirthdate__c 
	            	|| family.MaritalStatus__c != oldFamily.MaritalStatus__c 
	            	|| family.EnglishParentName__c != oldFamily.EnglishParentName__c 
	            	|| family.ParentId__c != oldFamily.ParentId__c 
	            	|| family.ParentPicture__c != oldFamily.ParentPicture__c  ){
	            	parentsToUpsert.add( childDb.UpdateOrInsertParents2(family));
		        }
	        }
	        if (childrenToUpdate.size() > 0)
	            update childrenToUpdate;
	        if (parentsToUpsert.size() > 0)
	            upsert parentsToUpsert;
	        if (casesToUpdate.size() > 0)
	            update casesToUpdate;
	    }
	    else    
	        CFamilyDb.IsAfterNumberOfChildren = false;
	}
}