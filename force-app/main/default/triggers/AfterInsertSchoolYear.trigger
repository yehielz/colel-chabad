trigger AfterInsertSchoolYear on SchoolYear__c (after insert)
{
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	
	if (trigger.new[0].IsActive__c)
    {
		List<SchoolYear__c> schoolYears = [select name, IsActive__c from SchoolYear__c where id != :trigger.new[0].id];
	    
	    // when set one of school years to be active, set all others to be not active.
	    for (SchoolYear__c a : schoolYears)
	    {
	        a.IsActive__c = false;
	    }
	    update schoolYears;
	    
	    CFamilyDb familyDb = new CFamilyDb();
        familyDb.UpdateNumberOfActivtyChildren(trigger.new[0]);
    
	    list<Children__c> childrenToUpdate = new list<Children__c>();
	    CChildDb childDb = new CChildDb(0);
	    childDb.SetChildrenMap();
	    list<Children__c> children = childDb.childrenMap.values();
	    childDb.SetActiveChildRegistrationByChildren();
	    for (Children__c child : children)
	    {
	    	Children__c childToUpdate = childDb.UpdateGradeAndSchool(child.id);
	    	if (childToUpdate != null)
	    		childrenToUpdate.add(childToUpdate);
	    	if (childrenToUpdate.size() > 70)
	    	{
	    		CChildDb.IsAfterUpdateTutorsOrActive = true;
	    		update childrenToUpdate;
	    		childrenToUpdate = new list<Children__c>();
	    	}
	    }
	    if (childrenToUpdate.size() > 0)
	    {
	    	CChildDb.IsAfterUpdateTutorsOrActive = true;
	    	update childrenToUpdate;
	    }
    }
}