trigger AfterUpdatePayment on Payment__c (after update) 
{
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	
	CActivityReportDb activityReportDb = new CActivityReportDb();
	activityReportDb.SetActivityReportsByPayments(trigger.new);
	
	CSpecialActivitiesLineDb specialActivitiesLineDb = new CSpecialActivitiesLineDb();
	specialActivitiesLineDb.SetSpecialActivitiesLinesByPayments(trigger.new);
	
	list<ActivityReport__c> reportsToUpdate = new list<ActivityReport__c>();
	list<SpecialActivitiesLine__c> specialActivitiesLinesToUpdate = new list<SpecialActivitiesLine__c>();
	
	//update should be done only one even if the list is bigger than 100. Need to be fixed! 
	for (Payment__c payment : trigger.new)	{
		
		reportsToUpdate.addAll(activityReportDb.updateActivityReportAfterUpdatePayment(payment.id, payment.Amount__c, payment.Status__c));
		specialActivitiesLinesToUpdate.addAll(specialActivitiesLineDb.UpdateSpecialActivitiesLineAfterUpdatePayment(payment.id, payment.IsDone__c));
		if (reportsToUpdate.size() > 100)
		{
			CActivityReportDb.isAfterPaymentOrChangeCoordinator = true;
			update reportsToUpdate;
			reportsToUpdate = new list<ActivityReport__c>();
		}
		if (specialActivitiesLinesToUpdate.size() > 100)
		{
			CSpecialActivitiesLineDb.IsDontUpdatePaymentInTrigger = true;
			update specialActivitiesLinesToUpdate;
			specialActivitiesLinesToUpdate = new list<SpecialActivitiesLine__c>();
		}
	}
	
	if (reportsToUpdate.size() > 0)	{
		
		CActivityReportDb.isAfterPaymentOrChangeCoordinator = true;
		update reportsToUpdate;
	}
	
	if (specialActivitiesLinesToUpdate.size() > 0)	{
		CSpecialActivitiesLineDb.IsDontUpdatePaymentInTrigger = true;
		update specialActivitiesLinesToUpdate;
	}
}