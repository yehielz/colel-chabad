trigger AfterUpdateSchoolYear on SchoolYear__c (after update)
{
    if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
        return;
    if(PAD.canTrigger('AfterUpdateSchoolYear')){
	    if (trigger.new[0].IsActive__c)
	    {
	    	/*
	        List<SchoolYear__c> schoolYears = [select IsActive__c from SchoolYear__c where id != :trigger.new[0].id and IsActive__c = true];
	        
	        // when set one of school years to be active, set all others to be not active.
	        for (SchoolYear__c a : schoolYears)
	        {
	            a.IsActive__c = false;
	        }
	        update schoolYears;
	        */
	        CFamilyDb familyDb = new CFamilyDb();
	        for (SchoolYear__c schoolYear : trigger.new)
	        {
	            familyDb.UpdateNumberOfActivtyChildren(schoolYear);
	        }
	        
	        list<Children__c> childrenToUpdate = new list<Children__c>();
	        CChildDb childDb = new CChildDb(0);
	        childDb.SetChildrenMap();
	        list<Children__c> children = childDb.childrenMap.values();
	        childDb.SetActiveChildRegistrationByChildren();
	        for (Children__c child : children)
	        {
	            Children__c childToUpdate = childDb.UpdateGradeAndSchool(child.id);
	            if (childToUpdate != null)
	                childrenToUpdate.add(childToUpdate);
	            /*if (childrenToUpdate.size() > 100)
	            {
	                CChildDb.IsAfterUpdateTutorsOrActive = true;
	                update childrenToUpdate;
	                childrenToUpdate = new list<Children__c>();
	            }*/
	        }
	        if (childrenToUpdate.size() > 0)
	        {
	            CChildDb.IsAfterUpdateTutorsOrActive = true;
	            update childrenToUpdate;
	        }
	    }
	}
}