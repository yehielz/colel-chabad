trigger BeforeDeleteExpenditureExceeded on ExpenditureExceeded__c (before delete) 
{
	Set<String> reportIds = new Set<String>();
	for(ExpenditureExceeded__c exp :trigger.old){
		reportIds.add(exp.ActivityReportName__c);
	}
	map<string, ActivityReport__c> activityReportsMap = CActivityReportDb.GetAllActivityReportMap(reportIds);

	for (ExpenditureExceeded__c ex : trigger.old)
	{
		if (activityReportsMap.containsKey(ex.ActivityReportName__c) && 
			activityReportsMap.get(ex.ActivityReportName__c).ActivityReportStatus__c == CObjectNames.ActivityReportStatusPaid)
		{
			throw new CChesedException('שגיאה! אין אפשרות למחוק את יום ' + ex.name + ' כיון שהדיווח אב שלו כבר שולם!');
		}
	}
}