trigger BeforeInsertAndUpdateDayInActivityReport on DayInActivityReport__c (before insert, before update) 
{
	if (test.isRunningTest() || !PAD.canTrigger('All Triggers'))
        return;
	
	Set<String> activityReportsIds = new Set<String>();
	
	for (DayInActivityReport__c day : trigger.new){
		if (day.ActivityReportName__c != null)
			activityReportsIds.add(day.ActivityReportName__c);
	}
	
	map<Id, ActivityReport__c> activityReportMap = new Map<Id,ActivityReport__c>([select Id,Name From ActivityReport__c where Id in :activityReportsIds]);
	
	for (DayInActivityReport__c day : trigger.new)
	{
		day.name = day.Date__c.day() + ' / ' + day.Date__c.month() + ' / ' + day.Date__c.year();
		if (activityReportMap.containsKey(day.ActivityReportName__c))
			day.name = activityReportMap.get(day.ActivityReportName__c).name + ' - ' + day.name;
		
	}
}