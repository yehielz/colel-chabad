trigger AfterInsertActivityApproval on ActivityApproval__c (after insert) 
{ 
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	
	//CActivityApprovalDb activityApprovalDb = new CActivityApprovalDb();
	map<string, datetime> childrenIdsMap = new map<string, datetime>();
	map<string, string> casesIdsMap = new map<string, string>();
	map<string, string> requestsIdsMap = new map<string, string>();
	for (ActivityApproval__c approval : trigger.new)
	{
		//activityApprovalDb.createMessageFromActivityApproval(approval, null);
		childrenIdsMap.put(approval.ChildName__c, approval.CreatedDate);
		casesIdsMap.put(approval.CaseName__c, approval.CaseName__c);
		requestsIdsMap.put(approval.RequestForTreatmentName__c, approval.RequestForTreatmentName__c);
	}
	
	//*******************************************************************
	// update the field of LastActivityDate in the Child
	//*******************************************************************
	list<string> childrenIds = new list<string>();
	childrenIds.addAll(childrenIdsMap.keySet()); 
	list<Children__c> children = CChildDb.GetChildrenListByIds(childrenIds);
	CChildDb childDb = new CChildDb(true);
	childDb.SetChildrenMap(children);
	childDb.SetTutorsListsByChild(childrenIds);
	list<Children__c> childrenToUpdate = new list<Children__c>();
	for (Children__c child : children)
	{
		child = childDb.updateTutorsList(child);
		child.LastApprovalDate__c = childrenIdsMap.get(child.id);
		childrenToUpdate.add(child);
		/*if (childrenToUpdate.size() > 99)
		{
			CChildDb.IsAfterUpdateTutorsOrActive = true;
			update childrenToUpdate;
			childrenToUpdate = new list<Children__c>();
		}*/
	}
	if (childrenToUpdate.size() > 0)
	{
		CChildDb.IsAfterUpdateTutorsOrActive = true;
		update childrenToUpdate;
	}
	
	//*******************************************************************
	// change the case status of this activity approval
	//*******************************************************************
	list<Case__c> cases = CCaseDb.GetCasesById(casesIdsMap.values());
	list<Case__c> casesToUpdate = new list<Case__c>();
	for (Case__c cs : cases)
	{
		if (cs.Status__c == 'חדש')
			cs.Status__c = 'בטיפול';
		if (Userinfo.getUserId() != cs.PreviousAssigneeUser__c)
			cs.AssigneeUser__c = cs.PreviousAssigneeUser__c;
		casesToUpdate.add(cs);
		if (casesToUpdate.size() > 99)
		{
			update casesToUpdate;
			casesToUpdate = new list<Case__c>();
		}
	}
	if (casesToUpdate.size() > 0)
		update casesToUpdate;
	
	
	//*******************************************************************
	// change the request details of this activity approval
	//**********************************************************	
	CRequestForTreatmentDb requestForTreatmentDb = new CRequestForTreatmentDb();
	list<string> requestsIds = requestsIdsMap.values(); 
	requestForTreatmentDb.SetAllRequestForTreatmentMap(requestsIds);
	requestForTreatmentDb.SetSumsMaps(requestsIds);
	list<RequestForTreatment__c> requestsToUpdate = new list<RequestForTreatment__c>();
	for (string rid : requestsIds)
	{
		RequestForTreatment__c toAdd = requestForTreatmentDb.UpdateRequestForTreatmentStatus(rid);
		if (toAdd != null)
			requestsToUpdate.add(toAdd);
		/*if (requestsToUpdate.size() > 99)
		{
	    	CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
			update requestsToUpdate;
			requestsToUpdate = new list<RequestForTreatment__c>();
		}*/
	}
	if (requestsToUpdate.size() > 0)
	{
		CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = true;
		update requestsToUpdate;
	}
}