trigger BeforeDeleteChildRegistration on ChildRegistration__c (before delete) 
{
	if(Test.isRunningTest())
		return;
	
	CFamilyDb familyDb = new CFamilyDb();
	familyDb.DecreaseNumberOfActivtyChildren(trigger.old[0]);
}