/*
    Permission for the System Administrator to delete a Case at any time
    Permission to the creator of the Case to delete the case if it is not in status 'אישור' or 'אישור חלקי'
*/

trigger RequestForTreatmentTrigger on RequestForTreatment__c (before delete) {
    
    Profile[] userProfile = [select name, id from Profile where (name='מנהל מערכת' or name='מנהל המערכת' or name = 'System Administrator') and id=:System.Userinfo.getProfileId()]; 
    if(userProfile.size() > 0){}
    else{
        if(Userinfo.getUserId() != Trigger.Old[0].CreatedById){
            Trigger.Old[0].addError('אין לך הרשאות למחוק בקשה זו. <br>', false);
        }
        else if(Trigger.Old[0].RequestStatus__c == 'אושר' || Trigger.Old[0].RequestStatus__c == 'אושר חלקית'){
            Trigger.Old[0].addError('לא ניתן למחוק מכיוון שמצב בקשה נמצא בסטטוס ' + Trigger.Old[0].RequestStatus__c);
        }
    }


}