trigger BeforeUpdateCase on Case__c (before update) 
{
	for (integer i = 0; i < trigger.new.size(); i++)
	{
		if (trigger.old[i].AssigneeUser__c != null && trigger.new[i].AssigneeUser__c != trigger.old[i].AssigneeUser__c)
			trigger.new[i].PreviousAssigneeUser__c = trigger.old[i].AssigneeUser__c;
	}
}