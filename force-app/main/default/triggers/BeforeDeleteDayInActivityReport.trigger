trigger BeforeDeleteDayInActivityReport on DayInActivityReport__c (before delete) {

	Set<String> reportIds = new Set<String>();
	for(DayInActivityReport__c day :trigger.old){
		reportIds.add(day.ActivityReportName__c);
	}
	map<string, ActivityReport__c> activityReportsMap = CActivityReportDb.GetAllActivityReportMap(reportIds);

	for (DayInActivityReport__c day : trigger.old)	{
		if (activityReportsMap.containsKey(day.ActivityReportName__c) && 
			activityReportsMap.get(day.ActivityReportName__c).ActivityReportStatus__c == CObjectNames.ActivityReportStatusPaid)	{
			throw new CChesedException('שגיאה! אין אפשרות למחוק את יום ' + day.name + ' כיון שהדיווח אב שלו כבר שולם!');
		}
	}
}