trigger BeforeInserCase on Case__c (before insert)
{
	if(PAD.canTrigger('All Triggers')){
	    for (Case__c mycase : trigger.new)
	    {
	        mycase.LastWriterNote__c = system.Userinfo.getName();
	        mycase.PreviousAssigneeUser__c = system.Userinfo.getUserId();
	        mycase.LastNoteDate__c = system.now();
	        mycase.LastNoteSubject__c = 'יצירה';
	        if (mycase.SchoolYear__c == null)
	            mycase.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
	    }
	}
}