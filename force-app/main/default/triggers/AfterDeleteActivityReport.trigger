trigger AfterDeleteActivityReport on ActivityReport__c (after delete) {
	if(PAD.canTrigger('AfterDeleteActivityReport')){
		Set<String> activityApprovalsToRun = new Set<String>();
		
		CPaymentDb paymentDb = new CPaymentDb();
		Set<String> paymentsIdsSet = new Set<String>();
		map<string, string> childrenIds = new map<string, string>();
		for (ActivityReport__c report : trigger.old)
		{
			if (report.PaymentName__c != null)
				paymentsIdsSet.add( report.PaymentName__c);
			activityApprovalsToRun.add(report.ActivityApprovalName__c);
			childrenIds.put(report.ChildName__c, report.ChildName__c);
		}
		
		if (paymentsIdsSet.size() > 0)
		{
			paymentDb.SetAllPaymentsMap(paymentsIdsSet);
			paymentDb.SetDebtByPaymentsMap(paymentsIdsSet);
			paymentDb.UpdatePaymentDebt(paymentsIdsSet);
		}
		
		CActivityApprovalDb activityApprovalDb = new CActivityApprovalDb(true);
		activityApprovalDb.SetActivityApprovalMap(activityApprovalsToRun);
		activityApprovalDb.SetTotalHoursAndAmountSpentByActivityApprovalMap(activityApprovalsToRun);
		activityApprovalDb.SetTotalMeetingsByActivityApprovalMap(activityApprovalsToRun);
		activityApprovalDb.SetLastReportDatesByApproval(activityApprovalsToRun);
		list<ActivityApproval__c> listToUpdate = new list<ActivityApproval__c>();
		for (string str : activityApprovalsToRun)
		{
			ActivityApproval__c approval = activityApprovalDb.UpdateActivityApprovalAfterReport(str);
			if (approval != null)
				listToUpdate.add(approval);
			/*if (listToUpdate.size() > 90)
			{
				CActivityApprovalDb.IsAfterChangeCoordinator = true;
				update listToUpdate;
				listToUpdate = new list<ActivityApproval__c>();
			}*/
		}
		if (listToUpdate.size() > 0)
		{
			CActivityApprovalDb.IsAfterChangeCoordinator = true;
			update listToUpdate;
		}
		
		Children__c[] children = [select name, id, NumberOfRequestsAndReports__c from Children__c where id in :childrenIds.values()];
		date startDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year() - 1, 9, 1) : date.newInstance(datetime.now().year(), 9, 1);
		date endDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year(), 9, 1) : date.newInstance(datetime.now().year() + 1, 9, 1);
		AggregateResult[] reportsCounts = [select count(id) num, ChildName__c child from ActivityReport__c where ChildName__c in :childrenIds.keySet()
										   and ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c >= :startDate and 
										   ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c < :endDate group by ChildName__c];
		AggregateResult[] requestsCounts = [select count(id) num, ChildName__c child from RequestForTreatment__c where 
											ChildName__c in :childrenIds.keySet() and FromDate__c >= :startDate and FromDate__c < :endDate 
											group by ChildName__c];
		map<string, integer> sizesByChild = new map<string, integer>();
		for (AggregateResult item : reportsCounts)
		{
			string childName = string.valueOf(item.get('child'));
			integer num = integer.valueOf(item.get('num'));
			sizesByChild.put(childName, num);
		}	
		for (AggregateResult item : requestsCounts)
		{
			string childName = string.valueOf(item.get('child'));
			integer num = integer.valueOf(item.get('num'));
			if (sizesByChild.containsKey(childName))
				num += sizesByChild.get(childName);
			sizesByChild.put(childName, num);
		}
		list<Children__c> childrenToUpdate = new list<Children__c>();
		for (Children__c child : children)
		{
			child.NumberOfRequestsAndReports__c = sizesByChild.containsKey(child.id) ? sizesByChild.get(child.id) : 0;
			childrenToUpdate.add(child);
			/*if (childrenToUpdate.size() > 150)
			{
				CChildDb.IsAfterUpdateTutorsOrActive = true;
				update childrenToUpdate;
				childrenToUpdate = new list<Children__c>();
			}*/
		}
		if (childrenToUpdate.size() > 0)
		{
			CChildDb.IsAfterUpdateTutorsOrActive = true;
			update childrenToUpdate;
		}
		
		map<string, decimal> activtyReportByPaymentId = new map<string, decimal>();
		for(ActivityReport__c report : trigger.old)
		{
			if (report.PaymentName__c != null)
			{
				decimal toReduse = report.PaymentAmountClass__c == null ? 0 : report.PaymentAmountClass__c;
				if (activtyReportByPaymentId.containsKey(report.PaymentName__c))	
					toReduse += activtyReportByPaymentId.get(report.PaymentName__c);
				activtyReportByPaymentId.put(report.PaymentName__c, toReduse);
			}
		}
		
		if (activtyReportByPaymentId.size() > 0)
		{
			list<Payment__c> toUpdate = new list<Payment__c>();
			list<Payment__c> toDelete = new list<Payment__c>();
			Payment__c[] payments = [select Debt__c, name, id, IsDone__c, FamilyOrTutor__c, TutorName__c, City__c, Balance__c, MasavFile__c, 
						AmountPaidByMasav__c, ParentName__c, FamilyName__c, Amount__c, Date__c, Comment__c, Status__c, NatureOfPayment__c, 
						PaymentTarget__c from Payment__c where id in :activtyReportByPaymentId.keySet()];
			for (Payment__c payment : payments)
			{
				payment.Debt__c -= activtyReportByPaymentId.get(payment.id);
				if (payment.Debt__c == 0 && payment.Status__c == 'לא שולם')
					toDelete.add(payment);
				else
					toUpdate.add(payment);
			}
			if (toUpdate.size() > 0)
				update toUpdate;
			if (toDelete.size() > 0)
				delete toDelete;
			
		}
	}
}