trigger AfterUpdateChildRegistration on ChildRegistration__c (after update) 
{
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	/*  OLD VERSION
	CChildDb childDb = new CChildDb(1);
	childDb.SetActiveChildRegistrationByChildren();
	list<Children__c> listToUpdate = new list<Children__c>();
	for (integer i = 0; i < trigger.new.size(); i++)
	{
		childDb.UpdateChildIsActive(trigger.new[i].ChildName__c);
		Children__c child = childDb.UpdateGradeAndSchool(trigger.new[i].ChildName__c);
		if (child != null)
			listToUpdate.add(child);
		//not necessary. this field is not editable.
		if (trigger.new[i].ChildName__c != trigger.old[i].ChildName__c)
		{
			childDb.UpdateChildIsActive(trigger.old[i].ChildName__c);
			child = childDb.UpdateGradeAndSchool(trigger.old[i].ChildName__c);
			if (child != null)
				listToUpdate.add(child);
		}
	}
	if (listToUpdate.size() > 0)
		update listToUpdate;
	*/
		
	//NEW VERSION
	
	List<ChildRegistration__c> registrations = new List<ChildRegistration__c>();
	for(ChildRegistration__c r :trigger.new){
		ChildRegistration__c oldR = trigger.oldMap.get(r.Id);
		if(r.SchoolName__c != oldR.SchoolName__c || r.Class__c != oldR.Class__c || r.SchoolName__c != oldR.SchoolName__c)
			registrations.add(r);
	}
	
	if(registrations.size() > 0)
		CChildDb.UpdateGradeAndSchoolAndAnnualRegistration(registrations);
}