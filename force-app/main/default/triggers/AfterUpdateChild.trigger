trigger AfterUpdateChild on Children__c (after update) 
{
    if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
        return;
    
    if (CChildDb.IsAfterUpdateTutorsOrActive != true)
    {
        Set<Id> childIds = new Set<Id>();
        List<Children__c> triggerNew  = new List<Children__c>();
        List<Children__c> activeChildren = new List<Children__c>();
        
        CExamDb.SetExamsByChildren(trigger.new);
        list<Exam__c> examsToUpdate = new list<Exam__c>();
        
        for (Children__c child : trigger.new) {    
        	Children__c oldChild = trigger.oldMap.get(child.Id);
            if(child.OwnerId != oldChild.OwnerId ){
                childIds.add(child.Id);
                triggerNew.add(child);
                //caseDb.updateCasesAfterUpdateChildren(child.id, child.CoordinatorUserName__c);
               
                //activityApprovalDb.UpdateActivityApprovalsAfterUpdateChild(child.id, child.CoordinatorUserName__c);
                //requestForTreatmentDb.UpdateRequestForTreatmentAfterUpdateChild(child.id, child.CoordinatorUserName__c);
                //activityReportDb.updateActivityReportsAfterUpdateChildren(child.id, child.CoordinatorUserName__c);
                examsToUpdate.addAll(CExamDb.UpdateExamsByChild(child.id, child.CoordinatorUserName__c,child.OwnerId));
            }
            
            if( (child.IsThereAnAnnualRegistration__c != oldChild.IsThereAnAnnualRegistration__c 
            	|| child.IsThereCase__c != oldChild.IsThereCase__c )  && child.IsThereAnAnnualRegistration__c && child.IsThereCase__c)
            	activeChildren.add(child);
        }
        
        if(childIds.size() > 0){
        	CCaseDb caseDb = new CCaseDb(true);
	        caseDb.SetCasesByChildrenMap(triggerNew);
	        caseDb.updateCasesAfterUpdateChildren(childIds,trigger.newMap);
	            
	        CActivityReportDb activityReportDb = new CActivityReportDb();
	        activityReportDb.SetActivityReportsByChildren(triggerNew);
	        activityReportDb.updateActivityReportsAfterUpdateChildren(childIds,trigger.newMap);
	        
	        
	        //No more necessary as they are master detail and we are using owner from now.
	        //CActivityApprovalDb activityApprovalDb = new CActivityApprovalDb(true);
	        //activityApprovalDb.setActivityApprovalByChildNameMap(triggerNew);
	        //activityApprovalDb.UpdateActivityApprovalsAfterUpdateChild(childIds,trigger.newMap);
	        
	        //CRequestForTreatmentDb requestForTreatmentDb = new CRequestForTreatmentDb(true);
	        //requestForTreatmentDb.setRequestForTreatmentByChildMap(triggerNew);
	        //requestForTreatmentDb.UpdateRequestForTreatmentAfterUpdateChild(childIds,trigger.newMap);
            
        }
        
        if (examsToUpdate.size() > 0)
            update examsToUpdate;
        if(activeChildren.size() > 0)
        	CFamilyDb.IncreaseNumberOfActiveChildren(activeChildren);
    }
    else
        CChildDb.IsAfterUpdateTutorsOrActive = false;
}