trigger AfterInsertDaysInActivityReport on DayInActivityReport__c (after insert) 
{ 
	if(PAD.canTrigger('All Triggers')){
		map<string, string> activityReportsMap = new map<string, string>();
		for (DayInActivityReport__c day : trigger.new)
		{
			if (day.ActivityReportName__c != null)
				activityReportsMap.put(day.ActivityReportName__c, day.ActivityReportName__c);
		}
		
		list<string> activityReports = activityReportsMap.values();
		list<ActivityReport__c> listToUpdate = new list<ActivityReport__c>();
		
		CActivityReportDb activityReportDb = new CActivityReportDb();
		activityReportDb.SetActivityReportMap(activityReports);
		activityReportDb.SetTotalPaymentByActivityReportMap(activityReports);
		
		for (string str : activityReports)
		{
			ActivityReport__c report = activityReportDb.UpdateActivityReportTotalPayment(str);
			if (report != null)
				listToUpdate.add(report);
			if (listToUpdate.size() > 70)
			{
				update listToUpdate;
				listToUpdate = new list<ActivityReport__c>();
			}
		}
		
		if (listToUpdate.size() > 0)
			update listToUpdate;
	}
}