trigger BeforeInsertSpecialActivitiesLine on SpecialActivitiesLine__c (before insert, before update) {
	
	if (!PAD.canTrigger('All Triggers'))
        return;
	
	Set<string> activtyIdsSet   = new Set<string>();
	Set<string> childrensIdsSet = new Set<string> ();
	Set<string> familiesIdsSet  = new Set<string> ();
	Set<string> contactIdsSet   = new Set<string> ();
	list<SpecialActivitiesLine__c> activitiesLines = new list<SpecialActivitiesLine__c>();
	for (SpecialActivitiesLine__c line : trigger.new) {
		if (line.PaymentAmount__c != null && line.PaymentAmount__c > 0 && line.PaymentName__c == null && line.PaymentStatus__c == 'לתשלום') {
			if (line.ChildName__c != null)
				childrensIdsSet.add(line.ChildName__c);
			if (line.FamilyName__c != null)
				familiesIdsSet.add(line.FamilyName__c);
			if (line.Contact__c != null)
				contactIdsSet.add(line.Contact__c);
			activtyIdsSet.add(line.SpecialActivitiesName__c);
			activitiesLines.add(line);
		}
		
		if(line.PaymentStatus__c != null && line.PaymentStatus__c.startsWith ('שולם')  ) line.Paid__c = true;
	}
	Map<Id,Children__c> childrenMap = new Map<Id,Children__c>();
	if (childrensIdsSet.size() > 0) {
		childrenMap = new Map<Id,Children__c> ([select name, id, FamilyName__c from Children__c where id in :childrensIdsSet]);
		
	}
	map<Id, SpecialActivities__c> specialActivitiesMap = new map<Id, SpecialActivities__c> ([select name, id , PaymentTo__c from SpecialActivities__c where id in :activtyIdsSet]);
	
	for (Children__c child :childrenMap.values()){
		familiesIdsSet.add(child.FamilyName__c);
	}
	
	map<string, Payment__c> paymentsByReceiver = new map<string, Payment__c>();
	List<Payment__c> payments = [select id, name, Contact__c, FamilyName__c  
								FROM Payment__c 
								WHERE TutorName__c = null and IsDone__c != true 
								AND Status__c != :CObjectNames.ActReportStatusPaidByAccountingDpt
								AND Status__c != :CObjectNames.ActReportStatusPaidByDonator
								AND (Contact__c in :contactIdsSet or FamilyName__c in :familiesIdsSet)];
	for (Payment__c payment : payments){
		if(payment.Contact__c != null)
			paymentsByReceiver.put(payment.Contact__c, payment);
		if(payment.FamilyName__c != null)
			paymentsByReceiver.put(payment.FamilyName__c, payment);
	}
	
	
	map<string, Payment__c> paymentsByFamilyOrContactToSave = new map<string, Payment__c>(); 
	list<Payment__c> paymentsToSave = new list<Payment__c>();
	for (string family : familiesIdsSet) {
		Payment__c payment = new Payment__c();
		payment.FamilyName__c = family;
		payment.Date__c = datetime.now().date();
		if (paymentsByReceiver.containsKey(family))	
			payment = paymentsByReceiver.get(family);
		if (payment.id == null) {
			paymentsToSave.add(payment);
			paymentsByFamilyOrContactToSave.put(family, payment);
			
			
		}
		else
			paymentsByFamilyOrContactToSave.put(family, payment);
	}
	
	for (String contactId :contactIdsSet) {
		Payment__c payment = new Payment__c();
		payment.Contact__c = contactId;
		payment.Date__c = datetime.now().date();
		if (paymentsByReceiver.containsKey(contactId))	
			payment = paymentsByReceiver.get(contactId);
		if (payment.id == null) {
			paymentsToSave.add(payment);
			paymentsByFamilyOrContactToSave.put(contactId, payment);
			
			
		}
		else
			paymentsByFamilyOrContactToSave.put(contactId, payment);
	}
	
	
	if (paymentsToSave.size() > 0)
		insert paymentsToSave;
	
	for (SpecialActivitiesLine__c line : activitiesLines) {
		string paymentTo = specialActivitiesMap.get(line.SpecialActivitiesName__c).PaymentTo__c ;
		if (paymentTo == 'חיצונית') {
			line.PaymentAmount__c = null;
			line.paid__c = null;
			line.PaymentName__c = null;
		}	
		else {
			string receiverName = '';
			if (line.FamilyName__c != null)
				receiverName = line.FamilyName__c;
			if (line.ChildName__c != null && childrenMap.containsKey(line.ChildName__c))
				receiverName = childrenMap.get(line.ChildName__c).FamilyName__c;
			if(line.Contact__c != null)
				receiverName = line.Contact__c;
			line.PaymentName__c = paymentsByFamilyOrContactToSave.containsKey(ReceiverName ) ? paymentsByFamilyOrContactToSave.get(ReceiverName ).id : null;
		}
	}
}