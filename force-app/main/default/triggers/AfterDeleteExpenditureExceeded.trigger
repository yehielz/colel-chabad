trigger AfterDeleteExpenditureExceeded on ExpenditureExceeded__c (after delete) 
{
	map<string, string> activityReportsMap = new map<string, string>();
	for (integer i = 0; i < trigger.old.size(); i++)
	{
		if (trigger.old[i].ActivityReportName__c != null)
			activityReportsMap.put(trigger.old[i].ActivityReportName__c, trigger.old[i].ActivityReportName__c);
	}
	
	list<string> activityReports = activityReportsMap.values();
	list<ActivityReport__c> listToUpdate = new list<ActivityReport__c>();
	
	CActivityReportDb activityReportDb = new CActivityReportDb();
	activityReportDb.SetActivityReportMap(activityReports);
	activityReportDb.SetTotalPaymentByActivityReportMap(activityReports);
	
	for (string str : activityReports)
	{
		ActivityReport__c report = activityReportDb.UpdateActivityReportTotalPayment(str);
		if (report != null)
			listToUpdate.add(report);
		if (listToUpdate.size() > 70)
		{
			update listToUpdate;
			listToUpdate = new list<ActivityReport__c>();
		}
	}
	
	if (listToUpdate.size() > 0)
		update listToUpdate;
}