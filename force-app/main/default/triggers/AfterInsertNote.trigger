trigger AfterInsertNote on Note (after insert)
{
	if( PAD.canTrigger('All Triggers')  ){
		Set<String> parentIds = new Set<String>();
		for (Note n : trigger.new)
			parentIds.add(n.ParentId);
		
		map<Id, ActivityApproval__c> activityApprovalsMap = CActivityApprovalDb.GetActivityApprovalsMap(parentIds);
		for(ActivityApproval__c app :activityApprovalsMap.values()) if(app.RequestForTreatmentName__c != null) parentIds.add (app.RequestForTreatmentName__c);
		map<Id, RequestForTreatment__c> requestForTreatmentsMap = CRequestForTreatmentDb.GetRequestForTreatmentMap(parentIds);
		for(RequestForTreatment__c req :requestForTreatmentsMap.values()) if(req.CaseName__c != null) parentIds.add (req.CaseName__c);
		map<Id, Case__c> casesMap = CCaseDb.GetCasesMap2(parentIds); 
		
		//************************************************************************************
		/*Case__c[] c = [select name, LastWriterNote__c, LastNoteDate__c, LastNoteSubject__c, Status__c, AssigneeUser__c 
								from Case__c where id = :triggerNote.ParentId];*/
		list<Note> notesToInsert = new list<Note>();
		map<string, Case__c> casesToInsert = new map<string, Case__c>();
		for (Note triggerNote : trigger.new)
		{
			if (casesMap.containsKey(triggerNote.ParentId))
			{
				Case__c c = casesMap.get(triggerNote.ParentId);
				User[] users = [select name, id from User where name = :c.LastWriterNote__c];
				if (users.size() > 0 && CNoteInCaseDb.IsToKeepAssigneeUser != true)
					c.AssigneeUser__c = users[0].id;
				c.LastWriterNote__c = system.Userinfo.getName();
				c.LastNoteDate__c = system.now();
				if (triggerNote.Body != null)
				{
					if (triggerNote.Body.length() < 255)
						c.LastNoteSubject__c = triggerNote.Body;
					else
						c.LastNoteSubject__c = triggerNote.Title;
				}
				c.Status__c = 'בטיפול';
				casesToInsert.put(c.id, c);
			}
			
			//************************************************************************************
			/*RequestForTreatment__c[] rft = [select name, CaseName__c from RequestForTreatment__c where id = :triggerNote.ParentId];*/
			if (requestForTreatmentsMap.containsKey(triggerNote.ParentId))
			{
				/*Case__c[] cs = [select name from Case__c where id = :rft[0].CaseName__c];*/
				if (casesMap.containsKey(requestForTreatmentsMap.get(triggerNote.ParentId).CaseName__c))
				{
					Note n = new Note();
					n.Title = triggerNote.Title;
					n.Body = triggerNote.Body;
					n.ParentId = requestForTreatmentsMap.get(triggerNote.ParentId).CaseName__c;
					notesToInsert.add(n);
				}
			}
			
			//************************************************************************************
			/*ActivityApproval__c[] aap = [select name, RequestForTreatmentName__c from ActivityApproval__c where id = :triggerNote.ParentId];*/
			if (activityApprovalsMap.containsKey(triggerNote.ParentId))
			{
				/*RequestForTreatment__c[] reqft = [select name, CaseName__c from RequestForTreatment__c where id = :aap[0].RequestForTreatmentName__c];*/
				ActivityApproval__c activityApproval = activityApprovalsMap.get(triggerNote.ParentId);
				if (requestForTreatmentsMap.containsKey(activityApproval.RequestForTreatmentName__c))
				{
					/*Case__c[] cs1 = [select name from Case__c where id = :reqft[0].CaseName__c];*/
					RequestForTreatment__c requestForTreatment = requestForTreatmentsMap.get(activityApproval.RequestForTreatmentName__c);
					if (casesMap.containsKey(requestForTreatment.CaseName__c))
					{
						Note n = new Note();
						n.Title = triggerNote.Title;
						n.Body = triggerNote.Body;
						n.ParentId = requestForTreatment.CaseName__c;
						notesToInsert.add(n);
					}
				}
			}
		}
		if (notesToInsert.size() > 0){
			insert notesToInsert;
		}
		if (casesToInsert.values().size() > 0)
			update casesToInsert.values();
		if (CNoteInCaseDb.IsToKeepAssigneeUser != null && CNoteInCaseDb.IsToKeepAssigneeUser == true)
			CNoteInCaseDb.IsToKeepAssigneeUser = false;
	}
}