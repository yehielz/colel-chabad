trigger BeforeInsertAndUpdateChild on Children__c (before insert, before update) 
{
	//map<string, Family__c> familiesMap = CFamilyDb.GetAllFamiliesMap();
	if (!PAD.canTrigger('All Triggers'))
        return;
	Set<String> familyIds = new Set<String>();
	for (Children__c child : trigger.new){
		familyIds.add(child.FamilyName__c);
	}
	
	Map<Id,Family__c> familiesMap = new Map<Id,Family__c>([select Id, Name From Family__c where Id in : familyIds ]);
	
	for (Children__c child : trigger.new)
	{
		child.name = '';
		
		if (familiesMap.containsKey(child.FamilyName__c) && familiesMap.get(child.FamilyName__c).name != null && child.FirstName__c != null)
			child.name += familiesMap.get(child.FamilyName__c).name + ' ' + child.FirstName__c;
		if ((!familiesMap.containsKey(child.FamilyName__c) || familiesMap.get(child.FamilyName__c).name == null) && child.FirstName__c != null)
			child.name += child.FirstName__c;
		if (familiesMap.containsKey(child.FamilyName__c) && familiesMap.get(child.FamilyName__c).name != null && child.FirstName__c == null)
			child.name += familiesMap.get(child.FamilyName__c).name;
		if (child.FamilyManType__c != null && child.FamilyManType__c == 'הורה')
			child.name += ' - הורה';
	}
	
}