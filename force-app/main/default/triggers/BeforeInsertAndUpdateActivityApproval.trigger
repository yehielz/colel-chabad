trigger BeforeInsertAndUpdateActivityApproval on ActivityApproval__c (before insert, before update) 
{
	if (!PAD.canTrigger('All Triggers'))
        return;
	//מקשר כל אישור פעילות חדש לעדכון הנכון
	//אישור הפעילות כבר  מקושר לבקשת הפעילות שהיא עצמה מקושרת לעדכון
	//רק מעתיקים את הקישור לעדכון לרמה של האישור גם כן
	Set<String> requestsIds = new Set<String> ();
    for (ActivityApproval__c approval : trigger.new){
    	if(approval.RequestForTreatmentName__c != null)
    		requestsIds.add(approval.RequestForTreatmentName__c);
    }
    
    Map<Id,RequestForTreatment__c> requestsMap = new Map<Id,RequestForTreatment__c>([select Id,Name,CaseName__c from RequestForTreatment__c where Id in :requestsIds]);
    
    for (ActivityApproval__c approval : trigger.new){
    	if(requestsMap.get(approval.RequestForTreatmentName__c) != null)
    		approval.CaseName__c = requestsMap.get(approval.RequestForTreatmentName__c).CaseName__c;
    }
    
    //list<RequestForTreatment__c> requestForTreatments = CRequestForTreatmentDb.GetRequestForTreatmentList();
    //map<string, RequestForTreatment__c> requestForTreatmentsMap = new map<string, RequestForTreatment__c>();
    //for (RequestForTreatment__c requestForTreatment : requestForTreatments)
    //{
        //requestForTreatmentsMap.put(requestForTreatment.id, requestForTreatment);
    //}
    
    for (ActivityApproval__c approval : trigger.new)
    {  
        //if (requestForTreatmentsMap.containsKey(approval.RequestForTreatmentName__c))
        //    approval.CaseName__c = requestForTreatmentsMap.get(approval.RequestForTreatmentName__c).CaseName__c;
            
        approval.TotalHours__c = 0;
        decimal weeklyHours = 0;
        if (approval.WeeklyHours__c != null)
            weeklyHours = approval.WeeklyHours__c;
        integer totalDays = 0;
        if (approval.FromDate__c != null && approval.ToDate__c != null)
            totalDays = approval.FromDate__c.daysBetween(approval.ToDate__c);
        decimal totalWeeks = totalDays / 7;
        approval.TotalHours__c = weeklyHours * totalWeeks;
        if (trigger.isinsert && approval.SchoolYear__c == null)
            approval.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
    }
    
    
    
    
}