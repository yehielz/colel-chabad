trigger BeforeInsertAndUpdatePayment on Payment__c (before insert, before update) 
{
	/*
	if (!PAD.canTrigger('All Triggers'))
        return;
	map<string, string> tutorsIds = new map<string, string>();
	map<string, string> familiesIds = new map<string, string>();
	for (Payment__c payment : trigger.new)
	{
		if (payment.FamilyName__c != null)
			familiesIds.put(payment.FamilyName__c, payment.FamilyName__c);
		if (payment.TutorName__c != null)
			tutorsIds.put(payment.TutorName__c, payment.TutorName__c);
	}
	Tutor__c[] tutors = [select name, id from Tutor__c where id in :tutorsIds.values()];
	Family__c[] families = [select name, id from Family__c where id in :familiesIds.values()];
	tutorsIds = null;
	familiesIds = null;
	map<string, string> tutorNamesByIds = new map<string, string>();
	map<string, string> familyNamesByIds = new map<string, string>();
	for (Tutor__c tutor : tutors)
	{
		tutorNamesByIds.put(tutor.id, tutor.name);
	}
	for (Family__c family : families)
	{
		familyNamesByIds.put(family.id, family.name);
	}
	
	for (Payment__c payment : trigger.new)
	{
		payment.name = payment.Date__c != null ? payment.Date__c.day() + '/' + payment.Date__c.month() + '/' + payment.Date__c.year() : '';
		if (payment.TutorName__c != null && tutorNamesByIds.containsKey(payment.TutorName__c))
			payment.name += ' - ' + tutorNamesByIds.get(payment.TutorName__c);
		if (payment.FamilyName__c != null && familyNamesByIds.containsKey(payment.FamilyName__c))
			payment.name += ' - ' + familyNamesByIds.get(payment.FamilyName__c);
	}*/
}