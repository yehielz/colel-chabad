trigger BeforeInsertAndUpdateProfessionType on ProfessionType__c (before insert, before update)
{
	/*ProfessionType__c[] ProfessionTypeDeafult = [select name, id, IsDefault__c from ProfessionType__c where IsDefault__c = true];
	for (ProfessionType__c professionType : trigger.new)
	{
		if (!ProfessionTypeDeafult.isEmpty() && ProfessionTypeDeafult[0].id != professionType.id && professionType.IsDefault__c == true)
		{
			ProfessionTypeDeafult[0].IsDefault__c = false;
			update ProfessionTypeDeafult[0];
		}
	}*/
	if (test.isRunningTest() || !PAD.canTrigger('All Triggers'))
        return;
	
	map<string,ProfessionType__c> toUpdate = new map<string,ProfessionType__c>();
	ProfessionType__c[] ProfessionTypeDeafult;
	if(trigger.isInsert)
		ProfessionTypeDeafult = [select name, id, IsDefault__c from ProfessionType__c];
	else
		ProfessionTypeDeafult = [select name, id, IsDefault__c from ProfessionType__c where IsDefault__c = true];
		
	for (ProfessionType__c professionType : trigger.new)
	{
		if (professionType.IsDefault__c == true && ProfessionTypeDeafult.size() > 0)
		{
			for (ProfessionType__c p : ProfessionTypeDeafult)
			{
				if (p.IsDefault__c == true && p != professionType)
				{
					p.IsDefault__c = false;
					toUpdate.put(p.id,p);
				}
			}
			professionType.IsDefault__c = true;
		}
	}
	if(toUpdate.values().size() > 0)
		update toUpdate.values();
	
}