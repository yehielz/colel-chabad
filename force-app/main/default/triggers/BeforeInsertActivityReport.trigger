trigger BeforeInsertActivityReport on ActivityReport__c (before insert) 
{
    if (test.isRunningTest() || !PAD.canTrigger('All Triggers'))
        return;
    
    map<string, Profession__c> professionsMap = CProfessionDb.GetAllProfessionsMap();
    for (ActivityReport__c report : trigger.new)
    {
    
        if (report.ReportMonth__c == null)
            report.MonthToFiltering__c = null;
        else
            report.MonthToFiltering__c = date.newInstance(integer.valueOf(report.ReportMonth__c.split('-')[1]), integer.valueOf(report.ReportMonth__c.split('-')[0]), 1);
            
        string paymentType = '';
        if (professionsMap.containsKey(report.ProfessionName__c))
            paymentType = professionsMap.get(report.ProfessionName__c).ProfessionTypeName__r.PaymentType__c;
            
        if (paymentType == CObjectNames.MonthlyPayment || paymentType == CObjectNames.OneTimePayment)
            report.TotalPayment__c = report.PaymentAmountClass__c;
        else
        {
            report.FromDateClass__c = null;
            report.ToDateClass__c = null;
            report.PaymentAmountClass__c = null;
        }
         if (report.SchoolYear__c == null)
              report.SchoolYear__c = CSchoolYearDb.GetActiveYearId();
    }
}