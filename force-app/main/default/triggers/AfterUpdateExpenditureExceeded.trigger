trigger AfterUpdateExpenditureExceeded on ExpenditureExceeded__c (after update) 
{
	if(PAD.canTrigger('All Triggers')){
		map<string, string> activityReportsMap = new map<string, string>();
		for (integer i = 0; i < trigger.new.size(); i++)
		{
			if (trigger.new[i].ActivityReportName__c != null)
				activityReportsMap.put(trigger.new[i].ActivityReportName__c, trigger.new[i].ActivityReportName__c);
			if (trigger.old[i].ActivityReportName__c != null)
				activityReportsMap.put(trigger.old[i].ActivityReportName__c, trigger.old[i].ActivityReportName__c);
		}
		
		list<string> activityReports = activityReportsMap.values();
		list<ActivityReport__c> listToUpdate = new list<ActivityReport__c>();
		
		CActivityReportDb activityReportDb = new CActivityReportDb();
		activityReportDb.SetActivityReportMap(activityReports);
		activityReportDb.SetTotalPaymentByActivityReportMap(activityReports);
		
		for (string str : activityReports)
		{
			ActivityReport__c report = activityReportDb.UpdateActivityReportTotalPayment(str);
			if (report != null)
				listToUpdate.add(report);
			if (listToUpdate.size() > 70)
			{
				update listToUpdate;
				listToUpdate = new list<ActivityReport__c>();
			}
		}
		
		if (listToUpdate.size() > 0)
			update listToUpdate;
	}
}