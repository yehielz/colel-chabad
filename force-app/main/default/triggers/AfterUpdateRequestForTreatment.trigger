trigger AfterUpdateRequestForTreatment on RequestForTreatment__c (after update) 
{
	if(Test.isRunningTest() || !PAD.canTrigger('All Triggers'))
		return;
	
	if (CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator != true)
	{
		CActivityApprovalDb activityApprovalDb = new CActivityApprovalDb();
		activityApprovalDb.SetActivityApprovalByRequestForTreatment(trigger.new);
		activityApprovalDb.SetProfessionsMap();
		list<Note> notesToInsert = new list<Note>();
		list<ActivityApproval__c> approvalsToUpdate = new list<ActivityApproval__c>();
		map<string, string> childrenIds = new map<string, string>();
		for (integer i = 0; i < trigger.new.size(); i++)
		{
			approvalsToUpdate.addAll(activityApprovalDb.UpdateActivityApprovalAfterUpdateRequestForTreatment(trigger.new[i]));
			/*if (approvalsToUpdate.size() > 90)
			{
				CCaseDb.IsDoNotUpdateCaseAfterApproval = true;
				update approvalsToUpdate;
			}*/
			if (trigger.new[i].RequestStatus__c != trigger.old[i].RequestStatus__c)
			{
				Note n = new Note();
				n.Title = 'מצב בקשה שונה';
				n.Body = 'מצב בקשה מס\' ' + trigger.new[i].name + ' שונה מ: ' + trigger.old[i].RequestStatus__c + ' ל: ' + trigger.new[i].RequestStatus__c;
				n.ParentId = trigger.new[i].CaseName__c;
				notesToInsert.add(n);
			} 
			childrenIds.put(trigger.new[i].ChildName__c, trigger.new[i].ChildName__c);
			childrenIds.put(trigger.old[i].ChildName__c, trigger.old[i].ChildName__c);
		}
		if (notesToInsert.size() > 0)
			insert notesToInsert;
		if (approvalsToUpdate.size() > 0)
		{
			CCaseDb.IsDoNotUpdateCaseAfterApproval = true;
			update approvalsToUpdate;
		}
		Children__c[] children = [select name, id, NumberOfRequestsAndReports__c from Children__c where id in :childrenIds.values()];
		date startDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year() - 1, 9, 1) : date.newInstance(datetime.now().year(), 9, 1);
		date endDate = datetime.now().date() < date.newInstance(datetime.now().year(), 9, 1) ? date.newInstance(datetime.now().year(), 9, 1) : date.newInstance(datetime.now().year() + 1, 9, 1);
		AggregateResult[] reportsCounts = [select count(id) num, ChildName__c child from ActivityReport__c where ChildName__c in :childrenIds.keySet()
										   and ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c >= :startDate and 
										   ActivityApprovalName__r.RequestForTreatmentName__r.FromDate__c < :endDate group by ChildName__c];
		AggregateResult[] requestsCounts = [select count(id) num, ChildName__c child from RequestForTreatment__c where 
											ChildName__c in :childrenIds.keySet() and FromDate__c >= :startDate and FromDate__c < :endDate 
											group by ChildName__c];
		map<string, integer> sizesByChild = new map<string, integer>();
		for (AggregateResult item : reportsCounts)
		{
			string childName = string.valueOf(item.get('child'));
			integer num = integer.valueOf(item.get('num'));
			sizesByChild.put(childName, num);
		}	
		for (AggregateResult item : requestsCounts)
		{
			string childName = string.valueOf(item.get('child'));
			integer num = integer.valueOf(item.get('num'));
			if (sizesByChild.containsKey(childName))
				num += sizesByChild.get(childName);
			sizesByChild.put(childName, num);
		}
		list<Children__c> childrenToUpdate = new list<Children__c>();
		for (Children__c child : children)
		{
			child.NumberOfRequestsAndReports__c = sizesByChild.containsKey(child.id) ? sizesByChild.get(child.id) : 0;
			childrenToUpdate.add(child);
			/*if (childrenToUpdate.size() > 150)
			{
				CChildDb.IsAfterUpdateTutorsOrActive = true;
				update childrenToUpdate;
				childrenToUpdate = new list<Children__c>();
			}*/
		}
		if (childrenToUpdate.size() > 0)
		{
			CChildDb.IsAfterUpdateTutorsOrActive = true;
			update childrenToUpdate;
		}
	}
	else
		CRequestForTreatmentDb.IsAfterActivityApprovalOrChangeCoordinator = false;
}