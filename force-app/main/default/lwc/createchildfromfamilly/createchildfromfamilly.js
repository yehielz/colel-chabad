import { LightningElement,api } from 'lwc';

export default class Createchildfromfamilly extends LightningElement
{
    @api recordId;
    @api invoke() {
        window.open('https://colelchabadcmm--dev--c.visualforce.com/apex/ChildNew?id='+this.recordId+'&save_new=1');
    }
}