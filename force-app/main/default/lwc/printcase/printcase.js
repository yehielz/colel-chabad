import { LightningElement,api } from 'lwc';

export default class Printcase extends LightningElement
{
    @api recordId;
    @api invoke() {
        window.open('/apex/caseprintlayout?ID='+this.recordId)
    }
}