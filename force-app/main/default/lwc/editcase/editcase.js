import { LightningElement,api } from 'lwc';

export default class Editcase extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/CaseNew?id=a0e3z000015AdT1&sfdc.override=1')
    }
}