import { LightningElement,api } from 'lwc';

export default class EditFamilly extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/FamilyNew?id='+this.recordId)
    }
}