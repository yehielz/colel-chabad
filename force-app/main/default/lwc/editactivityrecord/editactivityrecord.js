import { LightningElement,api } from 'lwc';

export default class Editactivityrecord extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/ActivityReportNew?id='+this.recordId)
    }
}