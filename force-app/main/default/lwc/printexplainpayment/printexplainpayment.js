import { LightningElement,api } from 'lwc';

export default class Printexplainpayment extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/PaymentPrint?ids='+this.recordId);
    }
}