import { LightningElement,api } from 'lwc';

export default class PrintFileActivityApproval extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/activityapprovaltoprint?activityApprovalID='+this.recordId);
    }

}