import { LightningElement,api } from 'lwc';

export default class LinkActionWeb extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/VF06_UploadFile?id='+ this.recordId +'&objectName=Family__c&fileTopic=טופס 1','טפסים','menubar=1,resizable=1,width=900,height=600,left=300,top=100')
    }
}