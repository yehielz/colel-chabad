import { LightningElement,api } from 'lwc';

export default class LinkActionWeb extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/properforfullreport?childID='+this.recordId);
    }
}