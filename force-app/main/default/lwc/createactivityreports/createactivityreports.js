import { LightningElement,api,wire } from 'lwc';
import { getRecord,getFieldValue  } from 'lightning/uiRecordApi';
import ActivityName from '@salesforce/schema/ActivityReport__c.ActivityApprovalName__c';

export default class Createactivityreports extends LightningElement {
    @api recordId;
    @wire(getRecord, { recordId:  '$recordId', fields: ActivityName, optionalFields: [] })
    recordname;
    
    @api invoke() {
        let valueId = getFieldValue(this.recordname.data, ActivityName);
        window.open('/apex/ActivityReportNew?CF00ND00000063kTb_lkid='+valueId+'&retURL=%2F'+this.recordId+'&save_new=1&sfdc.override=1')
    }
}