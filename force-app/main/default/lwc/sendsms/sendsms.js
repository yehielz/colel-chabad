import { LightningElement,api } from 'lwc';
import sendSms from '@salesforce/apex/CActivityApprovalFacad.SMSsend'
import { ShowToastEvent } from 'lightning/platformShowToastEvent'

export default class Sendsms extends LightningElement 
{
    @api recordId;
    @api invoke() {
       console.log('in sending');
       sendSms({idObj:this.recordId}).then(
        ()=>{
            const event = new ShowToastEvent({
                "title": "Success!",
                "message": "Success!",
                "variant":"success"
            });
            this.dispatchEvent(event);
        }
       )
    }
}