import { LightningElement,api } from 'lwc';

export default class PrintActivityAproval extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/activityapprovalprintlayout?ID='+this.recordId);
    }

}