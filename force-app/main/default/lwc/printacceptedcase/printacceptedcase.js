import { LightningElement,api } from 'lwc';

export default class Printacceptedcase extends LightningElement
{
    @api recordId;
    @api invoke() {
        window.open('/apex/activityapprovalstoprint?caseID='+this.recordId)
    }
}