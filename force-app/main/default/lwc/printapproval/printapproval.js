import { LightningElement,api } from 'lwc';

export default class Printapproval extends LightningElement 
{
    @api recordId;
    @api invoke() {
        window.open('/apex/activityreportprintlayout?ID='+this.recordId)
    }
}